# To enable ProGuard in your project, edit project.properties
# to define the proguard.config property as described in that file.
#
# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in ${sdk.dir}/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the ProGuard
# include property in project.properties.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-keepattributes SourceFile
-keepattributes LineNumberTable
-keepattributes Signature
-keepattributes *Annotation*
-keepattributes Exceptions
-keepattributes InnerClasses

-dontobfuscate
-dontoptimize
-dontskipnonpubliclibraryclassmembers
-dontnote


-keep class com.crashlytics.** { *; }
-keep class com.facebook.** { *; }
-keep class org.* { *; }
-keep class org.brickred.** { *; }
-keep class com.squareup.** { *; }
-keep class okio.** { *; }
-keep class com.linkedin.** { *; }

-keep class okhttp3.** { *; }
-keep interface okhttp3.** { *; }
-dontwarn okhttp3.**

-keep public class com.google.android.gms.ads.identifier.** { *; }
-keep class com.google.android.gms.ads.identifier.AdvertisingIdClient {
    public *;
}
-keep class com.google.android.gms.ads.identifier.AdvertisingIdClient$Info {
    public *;
}

#InMobi specific configuration
-keep class com.inmobi.** { *; }
-dontwarn com.inmobi.**

#SeventyNine specific configuration
-keep class com.seventynine.sdkadapter.** {*; }
-keep class seventynine.sdk.** {*; }
-dontwarn com.seventynine.**
-dontwarn seventynine.sdk.**

##---junit
-dontwarn android.test.**
-dontwarn android.support.test**
-dontwarn org.support.test**
-dontwarn org.junit.**

##Wasebeef Transformations
-dontwarn android.support.v8.renderscript.**
-dontwarn jp.co.cyberagent.android.gpuimage.**


-keepclassmembers class ** {
    @com.squareup.otto.Subscribe public *;
    @com.squareup.otto.Produce public *;
}


#BUTTERKNIFE RULES
#https://github.com/JakeWharton/butterknife/blob/master/butterknife/proguard-rules.txt
-keep class butterknife.*
-keep class butterknife.** { *; }
-keep class **$$ViewBinder { *; }

-dontwarn butterknife.internal.**

# Retain generated class which implement Unbinder.
-keep public class * implements butterknife.Unbinder { public <init>(...); }

# Prevent obfuscation of types which use ButterKnife annotations since the simple name
# is used to reflectively look up the generated ViewBinding.

-keepclasseswithmembernames class * { @butterknife.* <methods>; }
-keepclasseswithmembernames class * { @butterknife.* <fields>; }

#GLIDE
#-keep public class * implements com.bumptech.glide.module.GlideModule
#-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
#    **[] $VALUES;
#    public *;
#}

#Enums
-keepclassmembers class * extends java.lang.Enum {
    <fields>;
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-dontwarn android.support.v4.**
-dontwarn org.brickred.**
-dontwarn com.crashlytics.**
-dontwarn com.squareup.**
-dontwarn com.moe.pushlibrary.**
-dontwarn javax.annotation.**
-dontwarn javax.servlet.**
-dontwarn org.openid4java.**
-dontwarn org.apache.commons.logging.**
-dontwarn sun.misc.**
-dontwarn java.nio.**
-dontwarn com.facebook.BuildConfig
-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement
-dontwarn com.google.android.gms.internal.**


#App dynamics
-keep class com.appdynamics.eumagent.runtime.DontObfuscate
-keep @com.appdynamics.eumagent.runtime.DontObfuscate class * { *; }

#New Relic
-keep class com.newrelic.** { *; }
-dontwarn com.newrelic.**

##MoEngage
-dontwarn com.google.android.gms.location.**
-dontwarn com.google.android.gms.gcm.**
-dontwarn com.google.android.gms.iid.**

-keep class com.google.android.gms.gcm.** { *; }
-keep class com.google.android.gms.iid.** { *; }
-keep class com.google.android.gms.location.** { *; }

-keep class com.moe.pushlibrary.activities.** { *; }
-keep class com.moe.pushlibrary.internal.MoEService
-keep class com.moe.pushlibrary.GeofenceIntentService
-keep class com.moe.pushlibrary.InstallReceiver
-keep class com.moengage.push.MoEPushWorker
-keep class com.moe.pushlibrary.PushGcmBroadcastReceiver
-keep class com.moe.pushlibrary.providers.MoEProvider
-keep class com.moengage.receiver.MoEInstanceIDListener
-keep class com.moengage.worker.MoEGCMListenerService
-keep class com.moe.pushlibrary.models.** { *;}
-keep class com.moe.pushlibrary.internal.GeoTask
-keep class com.moengage.locationlibrarynew.LocationHandlerImpl

-dontwarn com.moengage.locationlibrarynew.LocationHandlerImpl
-dontwarn com.moe.pushlibrary.internal.GeoTask
-dontwarn com.moengage.receiver.*
-dontwarn com.moengage.worker.*
-dontwarn com.moengage.ViewEngine
-dontwarn com.facebook.drawee.**

-keep class com.delight.**  { *; }
##

#In App Billing
-keep class com.android.vending.billing.**

-assumenosideeffects class android.util.Log {
    public static *** d(...);
    public static *** w(...);
    public static *** v(...);
    public static *** i(...);
}

#Paytm
-keepclassmembers class com.paytm.pgsdk.PaytmWebView$PaytmJavaScriptInterface {
   public *;
}

#JWPlayer
#-dontwarn com.longtailvideo.jwplayer.cast.*
-dontwarn com.longtailvideo.jwplayer.**
-dontwarn com.googlecode.mp4parser.authoring.tracks.mjpeg.*

#AWS
-dontwarn com.amazonaws.util.json.*

#M4M
-keep class org.m4m.** { *; }
-keep class com.mp4parser.** { *; }
-keep class com.coremedia.iso.boxes.** { *; }
-keep class * implements com.coremedia.iso.boxes.Box { *; }
#-keep class com.googlecode.mp4parser.boxes.mp4.ESDescriptorBox { *; }
-keep class com.googlecode.mp4parser.boxes.** { *; }
