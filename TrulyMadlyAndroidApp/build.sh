#!/bin/bash

function string_replace {
    echo "${1/$2/$3}"
}

if [ "$3" = "-u" ]; then
    if [ -z "$4" ]; then
        echo "Specify the directory id after -u"
        echo "build.sh -a <assemble_command> -u <parent_directory_id>"
        exit 1
    fi
fi
if [ "$4" = "-u" ]; then
    if [ -z "$5" ]; then
        echo "Specify the directory id after -u"
        echo "build.sh -a <assemble_command> -u <parent_directory_id>"
        exit 1
    fi
fi



if [ "$1" = "-h" ]; then
	echo ""
	echo "Usage: redex [-h] [-a [assembleRule]] [-u [uploadUnderFolder]]"
	echo ""
	echo "Arguments:"
	echo "  -h            show this help message and exit"
	echo "  -a [assembleRule]"
	echo "                Allowed rules :"
	echo "                   assembleRelease"
	echo "                     assembleDevRelease"
	echo "                       assembleDevUniversalRelease , assembleDevHdpiRelease , assembleDevXhdpiRelease , assembleDevXxhdpiRelease , assembleDevXxxhdpiRelease"
	echo "                     assembleT1Release"
	echo "                       assembleT1UniversalRelease , assembleT1HdpiRelease , assembleT1XhdpiRelease , assembleT1XxhdpiRelease , assembleT1XxxhdpiRelease"
	echo "                     assembleLiveRelease"
	echo "                       assembleLiveUniversalRelease , assembleLiveHdpiRelease , assembleLiveXhdpiRelease , assembleT1XxhdpiRelease , assembleLiveXxxhdpiRelease"
	echo "                   assembleDebug"
	echo "                     assembleDevDebug"
	echo "                       assembleDevUniversalDebug , assembleDevHdpiDebug , assembleDevXhdpiDebug , assembleDevXxhdpiDebug , assembleDevXxxhdpiDebug"
	echo "                     assembleT1Debug"
	echo "                       assembleT1UniversalDebug , assembleT1HdpiDebug , assembleT1XhdpiDebug , assembleT1XxhdpiDebug , assembleT1XxxhdpiDebug"
	echo "                     assembleLiveDebug"
	echo "                       assembleLiveUniversalDebug , assembleLiveHdpiDebug , assembleLiveXhdpiDebug , assembleT1XxhdpiDebug , assembleLiveXxxhdpiDebug"
	echo "  --obfuscate"
	echo "                Obsufcate and Optimize the APK using Proguard"
	echo "                Note : Expects '-a [assembleRule]' parameters to be passed first"
	echo "  -u [uploadUnderFolder]"
	echo "                Needs a parent folder id under which the files are uploaded."
	echo "                How to get folder id : Please refer to the 'Build and Redex Readme.txt' file for the same."
	echo "                Note : Expects '-a [assembleRule]' parameters to be passed first"
	echo "  --publish"
	echo "                Publish the file to play store (Not implemented yet)"
	echo ""
	echo ""
fi

src="\.apk"
dest="-redex.apk"
key="TrulyMadlyAlpha.keystore"
sign="--sign -s $key -a android_alpha_release -p 1aficianado"
proguard_redex="proguard-rules.pro.redex"
proguard_normal="proguard-rules.pro"
normaldirname="normal"
redexdirname="redex"
toObfuscate="false"

if [ "$1" = "-a" ]; then

    if [ "$3" = "--obfuscate" ]; then
        toObfuscate="true"
    fi
    if [ "$toObfuscate" = "true" ]; then
        normaldirname="normalObfuscated"
        redexdirname="redexObfuscated"

        echo "\n--------------------BUILDING OBFUSCATED PROGUARD---------------------\n"

        if [ "$(uname)" == "Darwin" ]; then
            # Do something under Mac OS X platform
            echo "Mac OS X PLATFORM"
            sed -i '' 's/-dontobfuscate/#OBFUSCATE#-dontobfuscate/g' ${proguard_normal}
            sed -i '' 's/-dontoptimize/#OBFUSCATE#-dontoptimize/g' ${proguard_normal}
        elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
            # Do something under GNU/Linux platform
            echo "GNU/Linux PLATFORM"
            sed -i -- 's/-dontobfuscate/#OBFUSCATE#-dontobfuscate/g' ${proguard_normal}
            sed -i -- 's/-dontoptimize/#OBFUSCATE#-dontoptimize/g' ${proguard_normal}
        elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW32_NT" ]; then
            # Do something under Windows NT platform
            echo "WINDOWS NT PLATFORM: DO SOMETHING ABOUT FILENAME REPLACE HERE"
        fi

        echo "\n--------------------BUILDING OBFUSCATED PROGUARD COMPLETE---------------------\n"
    fi



	#Cleaning build folder
	echo "\n";
	echo "--------------------CLEANING PRE-BUILD--------------------"
	../gradlew -q clean
	rm ${proguard_redex}
	echo "--------------------CLEANING COMPLETE---------------------"
	echo "\n";

	#Running the build commands
	echo "\n--------------------BUILDING SIGNED APK-------------------\n"
	../gradlew $2
	echo "\n--------------------BUILDING COMPLETE---------------------\n"

	#Remove Unaligned Builds
	echo "\n"
	echo "--------------------REMOVING UNALIGNED FILES--------------"
	rm build/outputs/apk/*unaligned*.apk
	echo "--------------------REMOVING COMPLETE---------------------"
	echo "\n"

	#Redex
	echo "\n--------------------REDEXING FILES------------------------\n"
	echo "\n--------------------CREATING PROGUARD FILE----------------\n"

	cp ${proguard_normal} ${proguard_redex}
	#Commenting out proguard rules not supported by redex yet
	#sed -i '' 's/-keepattributes/#REDEX#-keepattributes/g' ${proguard_redex}  #Fixed in REDEX upgrade
	#sed -i '' 's/-dontobfuscate/#REDEX#-dontobfuscate/g' ${proguard_redex}  #Fixed in REDEX upgrade
	#sed -i '' 's/-dontoptimize/#REDEX#-dontoptimize/g' ${proguard_redex}  #Fixed in REDEX upgrade
	#sed -i '' 's/-dontwarn/#REDEX#-dontwarn/g' ${proguard_redex}  #Fixed in REDEX upgrade

	if [ "$(uname)" == "Darwin" ]; then
        # Do something under Mac OS X platform
        echo "Mac OS X PLATFORM"
        sed -i '' 's/-dontskipnonpubliclibraryclassmembers/#REDEX#-dontskipnonpubliclibraryclassmembers/g' ${proguard_redex}
	    sed -i '' 's/-dontnote/#REDEX#-dontnote/g' ${proguard_redex}
    elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
        # Do something under GNU/Linux platform
        echo "GNU/Linux PLATFORM"
        sed -i -- 's/-dontskipnonpubliclibraryclassmembers/#REDEX#-dontskipnonpubliclibraryclassmembers/g' ${proguard_redex}
	    sed -i -- 's/-dontnote/#REDEX#-dontnote/g' ${proguard_redex}
    elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW32_NT" ]; then
        # Do something under Windows NT platform
        echo "WINDOWS NT PLATFORM: DO SOMETHING ABOUT FILENAME REPLACE HERE"
    fi


	echo "\n--------------------CREATED PROGUARD FILE-----------------\n"

	#Only redexing apk files which are not "unaligned" && not "redex"
	FILES=$(find build/outputs/apk -type f \( -iname "*.apk" ! -iname "*unaligned*" ! -iname "*redex*" \))
	for f in ${FILES}
	do
	  echo "REDEXING FILE : " ${f}
	  dirname="$(dirname $f)"
	  outNormalDir="$dirname/$normaldirname"
	  outRedexDir="$dirname/$redexdirname"

      #Creating directories
	  if ! [ -d $outNormalDir ]; then
	    mkdir $outNormalDir
	  fi
	  if ! [ -d $outRedexDir ]; then
	    mkdir $outRedexDir
	  fi
	  ###

	  outFile=$(string_replace "$f" "$src" "$dest")
	  outFile=$(string_replace "$outFile" "/TM-APP" "/$redexdirname/TM-APP")
	  #echo "$outFile"

	  redexCommand="redex $sign $f -c redex.config --out $outFile --proguard-config $proguard_redex"
	  echo "$redexCommand"
	  ${redexCommand}

	  #Moving normal file to normal directory
	  mv $f $outNormalDir
	  echo "\n"
	done

	rm ${proguard_redex}
	echo "\n--------------------REDEXING COMPLETE---------------------\n"

	#Moving all build files in the current folder
	#Temporarily commenting
	#mv build/outputs/apk/* ./

    if [ "$toObfuscate" = "true" ]; then
        echo "\n--------------------BUILDING NORMAL PROGUARD---------------------\n"



        if [ "$(uname)" == "Darwin" ]; then
            # Do something under Mac OS X platform
            echo "Mac OS X PLATFORM"
            sed -i '' 's/#OBFUSCATE#-dontobfuscate/-dontobfuscate/g' ${proguard_normal}
            sed -i '' 's/#OBFUSCATE#-dontoptimize/-dontoptimize/g' ${proguard_normal}
        elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
            # Do something under GNU/Linux platform
            echo "GNU/Linux PLATFORM"
            sed -i -- 's/#OBFUSCATE#-dontobfuscate/-dontobfuscate/g' ${proguard_normal}
            sed -i -- 's/#OBFUSCATE#-dontoptimize/-dontoptimize/g' ${proguard_normal}
        elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW32_NT" ]; then
            # Do something under Windows NT platform
            echo "WINDOWS NT PLATFORM: DO SOMETHING ABOUT FILENAME REPLACE HERE"
        fi


        echo "\n--------------------BUILDING NORMAL PROGUARD COMPLETE---------------------\n"
    fi

fi

toUpload="false"
uploadUnderFolder="none"
if [ "$3" = "-u" ]; then
    toUpload="true"
    uploadUnderFolder=$4
fi
if [ "$4" = "-u" ]; then
    toUpload="true"
    uploadUnderFolder=$5
fi
if [ "$toUpload" = "true" ]; then
    echo "\n--------------------UPLOADING FILES---------------------\n"
    gdrive upload --parent $uploadUnderFolder --recursive build/outputs/apk/* --chunksize 524288
    echo "\n--------------------UPLOADING COMPLETE---------------------\n"

	#echo "\n--------------------CLEANING POST_UPLOAD--------------------\n"
	#Removing clean because in case upload fails, everything gets cleaned :(
	#../gradlew -q clean
	#echo "\n--------------------CLEANING COMPLETE---------------------\n"
fi


if [ "$3" = "--publish" ] || [ "$5" = "--publish" ] || [ "$6" = "--publish" ] || [ "$7" = "--publish" ]; then
    echo "\n--------------------UPLOADING FILES---------------------\n"
    gdrive upload --parent $4 --recursive build/outputs/apk/* --chunksize 524288
    echo "\n--------------------UPLOADING COMPLETE---------------------\n"

	echo "\n--------------------CLEANING POST_UPLOAD--------------------\n"
	../gradlew -q clean
	echo "\n--------------------CLEANING COMPLETE---------------------\n"
fi
