package seventynine.sdk.adapter;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.view.ViewGroup;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.seventynine.sdkadapter.AdapterCallback;
import com.seventynine.sdkadapter.CallBackAdapter.AdCallbackListener;
import com.seventynine.sdkadapter.CallBackAdapter.AdapterCallbackListener;
import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.ads.AdsHelper;
import com.trulymadly.android.app.ads.AdsType;
import com.trulymadly.android.app.ads.SeventyNineHandler;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;

public class AdMobInterstitialAdapter implements AdapterCallbackListener {
    AdCallbackListener adCallbackListener;
    InterstitialAd interstitial;
    ViewGroup adViewContainer;
    private Context mAppContext;

    @Override
    public void onLoad(final Context context, String Pid,
                       AdapterCallback adapterCallbackListener, final String zoneId,
                       ViewGroup view) {
        if(context != null) {
            mAppContext = context.getApplicationContext();
        }
        Log.d(SeventyNineHandler.TAG, "AdMobInterstitialAdapter : onLoad");
        try {

            adViewContainer = view;
            adCallbackListener = adapterCallbackListener;

            if (interstitial == null) {
                interstitial = new InterstitialAd(context);
                interstitial.setAdUnitId(Pid);
                interstitial.setAdListener(new AdListener() {
                    @Override
                    public void onAdLoaded() {
                        new Handler().post(new Runnable() {
                            public void run() {
                                HashMap<String, String> params = new HashMap<String, String>();
                                params.put("method", "onAdLoaded");
                                AdsHelper.addParams(params);
                                if (interstitial != null) {
                                    //Commenting out to fix the static listenr issue
//                                    adCallbackListener.onAdLoad(
//                                            "AdMobInterstitial", zoneId);
                                    interstitial.show();
                                    TrulyMadlyTrackEvent.trackEvent(context, TrulyMadlyEvent.TrulyMadlyActivities.matches,
                                            TrulyMadlyEvent.TrulyMadlyEventTypes.ad_interstitial, 0,
                                            TrulyMadlyEvent.TrulyMadlyEventStatus.ad_mediation_success_admob, params, true);
                                } else {
                                    //Commenting out to fix the static listenr issue
//                                    adCallbackListener.onAdFaild("AdMobInterstitial", adViewContainer, zoneId);
                                    TrulyMadlyTrackEvent.trackEvent(context, TrulyMadlyEvent.TrulyMadlyActivities.matches,
                                            TrulyMadlyEvent.TrulyMadlyEventTypes.ad_interstitial, 0,
                                            TrulyMadlyEvent.TrulyMadlyEventStatus.ad_mediation_failed_admob, params, true);
                                }
                                interstitial = null;
                            }
                        });
                    }

                    @Override
                    public void onAdFailedToLoad(int errorCode) {
                        //Commenting out to fix the static listenr issue
//                        adCallbackListener.onAdFaild("AdMobInterstitial",
//                                adViewContainer, zoneId);
                        interstitial = null;
                        HashMap<String, String> params = new HashMap<String, String>();
                        params.put("method", "onAdFailedToLoad");
                        params.put("error", "errorCode : " + errorCode);
                        AdsHelper.addParams(params);
                        TrulyMadlyTrackEvent.trackEvent(context, TrulyMadlyEvent.TrulyMadlyActivities.matches,
                                TrulyMadlyEvent.TrulyMadlyEventTypes.ad_interstitial, 0,
                                TrulyMadlyEvent.TrulyMadlyEventStatus.ad_mediation_failed_admob, params, true);
                    }

                    @Override
                    public void onAdClosed() {
                        //Commenting out to fix the static listenr issue
//                        adCallbackListener.onAdClose();
                    }

                    @Override
                    public void onAdLeftApplication() {
                        //Commenting out to fix the static listenr issue
//                        adCallbackListener.onClick();
                    }

                    @Override
                    public void onAdOpened() {
                        //Commenting out to fix the static listenr issue
//                        adCallbackListener.onAdShow();
                        AdsHelper.adShown(mAppContext, AdsType.MATCHES_END_INTERSTITIAL);
                        HashMap<String, String> params = new HashMap<String, String>();
                        params.put("method", "onAdOpened");
                        AdsHelper.addParams(params);
                        TrulyMadlyTrackEvent.trackEvent(context, TrulyMadlyEvent.TrulyMadlyActivities.matches,
                                TrulyMadlyEvent.TrulyMadlyEventTypes.ad_interstitial, 0,
                                TrulyMadlyEvent.TrulyMadlyEventStatus.ad_mediation_shown_admob, null, true);
                    }
                });
                AdRequest adRequest = new AdRequest.Builder().build();
                try {
                    interstitial.loadAd(adRequest);
                } catch (Throwable throwable) {
                    StringWriter sw = new StringWriter();
                    PrintWriter pw = new PrintWriter(sw, true);
                    throwable.printStackTrace(pw);
                    System.out.println(sw.getBuffer().toString());
                }
            }

        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    @Override
    public void onAdShow() {
        Log.d(SeventyNineHandler.TAG, "AdMobInterstitialAdapter : onAdShow");
        if(mAppContext != null) {
            AdsHelper.adShown(mAppContext, AdsType.MATCHES_END_INTERSTITIAL);
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("method", "onAdShow");
            TrulyMadlyTrackEvent.trackEvent(mAppContext, TrulyMadlyEvent.TrulyMadlyActivities.matches,
                    TrulyMadlyEvent.TrulyMadlyEventTypes.ad_interstitial, 0,
                    TrulyMadlyEvent.TrulyMadlyEventStatus.ad_mediation_shown_admob, null, true);
        }
    }

}
