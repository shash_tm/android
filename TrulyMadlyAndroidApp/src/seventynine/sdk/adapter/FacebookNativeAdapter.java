package seventynine.sdk.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.MediaView;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdScrollView;
import com.facebook.ads.NativeAdScrollView.AdViewProvider;
import com.facebook.ads.NativeAdView;
import com.facebook.ads.NativeAdViewAttributes;
import com.facebook.ads.NativeAdsManager;
import com.facebook.ads.NativeAdsManager.Listener;
import com.google.common.base.Preconditions;
import com.seventynine.sdkadapter.AdapterCallback;
import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.adapter.ConversationListAdapter;
import com.trulymadly.android.app.bus.AdEvent;
import com.trulymadly.android.app.utility.Utility;

import java.util.HashMap;

public class FacebookNativeAdapter implements AdListener, Listener {

    Context context;
    String adType;
    String size;
    ViewGroup nativeAdContainer;
    String zoneId;
    TextView titleLabel;
    MediaView coverImage;
    AdapterCallback adCallbackListener = new AdapterCallback();
    LayoutInflater inflater;
    String adCustomType;
    private NativeAd nativeAd;
    // private AdChoicesView adChoicesView;
    private View adView;
    private NativeAdsManager manager;
    private NativeAdScrollView scrollView;
    private NativeAdView.Type viewType;
    private NativeAdViewAttributes adViewAttributes = new NativeAdViewAttributes();

    public static void inflateAd(NativeAd nativeAd, View adView, Context context) {

        ConversationListAdapter.ConversationMessageViewHolder cmvh = new ConversationListAdapter.ConversationMessageViewHolder(adView);

        Preconditions.checkNotNull(cmvh.getProfile_pic_spark_indicator()).setVisibility(View.GONE);
        cmvh.getProfileTitleTv().setText(nativeAd.getAdTitle());
        cmvh.getProfileTitleTv().setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
//        cmvh.getConversationSelectLabel().setVisibility(View.GONE);
        cmvh.getLastMessageTv().setText(nativeAd.getAdSubtitle());
        cmvh.getConversationDateTv().setVisibility(View.GONE);
        cmvh.getSponsoredTv().setVisibility(View.VISIBLE);
        cmvh.getConversationListActionBlockIv().setVisibility(View.GONE);
        //cmvh.profileTitleTv.setTypeface(null, Typeface.NORMAL);
        cmvh.getLastMessageTv().setTypeface(null, Typeface.NORMAL);
        cmvh.getLastMessageTv().setTextColor(context.getResources().getColor(R.color.grey_text_color));
        cmvh.getFront().setBackgroundResource(R.drawable.conversation_list_item_front);
        NativeAd.Image adIcon = nativeAd.getAdIcon();
        NativeAd.downloadAndDisplayImage(adIcon, cmvh.getProfilePicIv());

        try {
//			ImageView nativeAdIcon = (ImageView) adView
//					.findViewById(R.id.nativeAdIcon);
//			TextView nativeAdTitle = (TextView) adView
//					.findViewById(R.id.nativeAdTitle);
//			TextView nativeAdBody = (TextView) adView
//					.findViewById(R.id.nativeAdBody);
//			MediaView nativeAdMedia = (MediaView) adView
//					.findViewById(R.id.nativeAdMedia);
//			TextView nativeAdSocialContext = (TextView) adView
//					.findViewById(R.id.nativeAdSocialContext);
//			Button nativeAdCallToAction = (Button) adView
//					.findViewById(R.id.nativeAdCallToAction);
//			// Setting the Text
//			nativeAdSocialContext.setText(nativeAd.getAdSocialContext());
//			nativeAdCallToAction.setText(nativeAd.getAdCallToAction());
//			nativeAdCallToAction.setVisibility(View.VISIBLE);
//			nativeAdTitle.setText(nativeAd.getAdTitle());
//			nativeAdBody.setText(nativeAd.getAdBody());
//			// Downloading and setting the ad icon.
//			NativeAd.Image adIcon = nativeAd.getAdIcon();
//			NativeAd.downloadAndDisplayImage(adIcon, nativeAdIcon);
//			// Downloading and setting the cover image.
//			NativeAd.Image adCoverImage = nativeAd.getAdCoverImage();
//			int bannerWidth = adCoverImage.getWidth();
//			int bannerHeight = adCoverImage.getHeight();
//			WindowManager wm = (WindowManager) context
//					.getSystemService(Context.WINDOW_SERVICE);
//			Display display = wm.getDefaultDisplay();
//			DisplayMetrics metrics = new DisplayMetrics();
//			display.getMetrics(metrics);
//			int screenWidth = metrics.widthPixels;
//			int screenHeight = metrics.heightPixels;
//			nativeAdMedia
//					.setLayoutParams(new LinearLayout.LayoutParams(
//							screenWidth,
//							Math.min(
//									(int) (((double) screenWidth / (double) bannerWidth) * bannerHeight),
//									screenHeight / 3)));
//			nativeAdMedia.setNativeAd(nativeAd);
            nativeAd.registerViewForInteraction(adView);
        } catch (Exception ignored) {
        }

    }

    public void showNative(Context context, ViewGroup nativeAdContainer,
                           String Pid, String adType, String size, String zoneId,
                           String adCustom) {

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        this.adType = adType;
        this.size = size;
        this.nativeAdContainer = nativeAdContainer;
        this.zoneId = zoneId;
        adCustomType = adCustom;
        if (adType.equalsIgnoreCase("simple")) {

//			if (adCustomType.equalsIgnoreCase("custom")) {
//
//				adView = (LinearLayout) inflater.inflate(
//						R.layout.ad_unit_forcustom, nativeAdContainer);
//
//			} else {
//				adView = (LinearLayout) inflater.inflate(R.layout.ad_unit,
//						nativeAdContainer);
//			}
            adView = inflater.inflate(R.layout.conversation_list_item, nativeAdContainer);
            adView.setVisibility(View.INVISIBLE);

            nativeAd = new NativeAd(context, Pid);
            nativeAd.setAdListener(FacebookNativeAdapter.this);
            nativeAd.loadAd(NativeAd.MediaCacheFlag.ALL);

        } else if (adType.equalsIgnoreCase("scroll")) {
            manager = new NativeAdsManager(context, Pid, 5);
            manager.setListener(FacebookNativeAdapter.this);
            manager.loadAds(NativeAd.MediaCacheFlag.ALL);
            manager.loadAds();

        } else if (adType.equalsIgnoreCase("template")) {

            nativeAd = new NativeAd(context, Pid);
            nativeAd.setAdListener(FacebookNativeAdapter.this);
            nativeAd.loadAd();

        }

    }

    @Override
    public void onAdClicked(Ad arg0) {
        //COmmenting out as a hack to fix static listener issue
//        adCallbackListener.onClick();
    }

    @Override
    public void onAdLoaded(Ad ad) {

        try {
            //COmmenting out as a hack to fix static listener issue
//            adCallbackListener.onAdLoad("FacebookNativeAdapter", zoneId);

            HashMap<String, String> params = new HashMap<String, String>();
            params.put("method", "onAdLoaded");
            TrulyMadlyTrackEvent.trackEvent(context, TrulyMadlyEvent.TrulyMadlyActivities.conversation_list,
                    TrulyMadlyEvent.TrulyMadlyEventTypes.ad_native, 0,
                    TrulyMadlyEvent.TrulyMadlyEventStatus.ad_mediation_success_fb, params, true);

            if (nativeAd == null || nativeAd != ad) {
                Utility.fireBusEvent(context, true, new AdEvent(AdEvent.AD_LOAD_FAILURE));
                return;
            }

            if (adType.equalsIgnoreCase("simple")) {

                adView.setVisibility(View.VISIBLE);
                nativeAd.unregisterView();
                // if (adChoicesView == null) {
                // adChoicesView = new AdChoicesView(context, nativeAd);
                // adView.addView(adChoicesView, 0);
                // }

                if (adCustomType.equalsIgnoreCase("custom")) {
                    inflateCustomAd(nativeAd, adView, context);
                } else {
                    inflateAd(nativeAd, adView, context);
                }
                Utility.fireBusEvent(context, true, new AdEvent(AdEvent.AD_LOAD_SUCCESS));

            } else if (adType.equalsIgnoreCase("template")) {

                if (nativeAd != null && nativeAd.isAdLoaded()) {
                    nativeAdContainer.removeAllViews();

                    if (size.equalsIgnoreCase("100")) {
                        viewType = NativeAdView.Type.HEIGHT_100;
                    } else if (size.equalsIgnoreCase("120")) {
                        viewType = NativeAdView.Type.HEIGHT_120;
                    } else if (size.equalsIgnoreCase("300")) {
                        viewType = NativeAdView.Type.HEIGHT_300;
                    } else if (size.equalsIgnoreCase("400")) {
                        viewType = NativeAdView.Type.HEIGHT_400;
                    } else {
                        viewType = NativeAdView.Type.HEIGHT_100;
                    }

                    if (adCustomType.equalsIgnoreCase("custom")) {
                        // adViewAttributes.setBackgroundColor(rowBackgroundColor);
                        // adViewAttributes.setTitleTextColor(titleColor);
                        // adViewAttributes.setDescriptionTextColor(contentColor);
                        // adViewAttributes.setButtonBorderColor(linkColor);
                        // adViewAttributes.setButtonTextColor(linkColor);
                    }
                    View adView = NativeAdView.render(context, nativeAd,
                            viewType, adViewAttributes);
                    // Add adView to the container showing Ads
                    nativeAdContainer.addView(adView, 0);
                    nativeAdContainer.setBackgroundColor(Color.TRANSPARENT);
                    Utility.fireBusEvent(context, true, new AdEvent(AdEvent.AD_LOAD_SUCCESS));
                }
            }

        } catch (Exception ignored) {
        }

    }

    @Override
    public void onError(Ad arg0, AdError arg1) {
        arg1.getErrorMessage();
        //COmmenting out as a hack to fix static listener issue
//        adCallbackListener.onAdFaild("FacebookNative", nativeAdContainer,
//                zoneId);

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("method", "onError : message : " + arg1.getErrorMessage());
        TrulyMadlyTrackEvent.trackEvent(context, TrulyMadlyEvent.TrulyMadlyActivities.conversation_list,
                TrulyMadlyEvent.TrulyMadlyEventTypes.ad_native, 0,
                TrulyMadlyEvent.TrulyMadlyEventStatus.ad_mediation_failed_fb, params, true);

        Log.d("FB E", arg1.getErrorMessage());
        Utility.fireBusEvent(context, true, new AdEvent(AdEvent.AD_LOAD_FAILURE));
    }

    // for scroll
    @Override
    public void onAdError(AdError arg0) {
        //COmmenting out as a hack to fix static listener issue
//        adCallbackListener.onAdFaild("FacebookNative", nativeAdContainer,
//                zoneId);
        Log.d("FB E", arg0.getErrorMessage());
        Utility.fireBusEvent(context, true, new AdEvent(AdEvent.AD_LOAD_FAILURE));

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("method", "onAdError : message : " + arg0.getErrorMessage());
        TrulyMadlyTrackEvent.trackEvent(context, TrulyMadlyEvent.TrulyMadlyActivities.conversation_list,
                TrulyMadlyEvent.TrulyMadlyEventTypes.ad_native, 0,
                TrulyMadlyEvent.TrulyMadlyEventStatus.ad_mediation_failed_fb, params, true);
    }

    @Override
    public void onAdsLoaded() {

        try {

            //COmmenting out as a hack to fix static listener issue
//            adCallbackListener.onAdLoad("FacebookNativeAdapter", zoneId);

            HashMap<String, String> params = new HashMap<String, String>();
            params.put("method", "onAdsLoaded");
            TrulyMadlyTrackEvent.trackEvent(context, TrulyMadlyEvent.TrulyMadlyActivities.conversation_list,
                    TrulyMadlyEvent.TrulyMadlyEventTypes.ad_native, 0,
                    TrulyMadlyEvent.TrulyMadlyEventStatus.ad_mediation_success_fb, params, true);

            if (scrollView != null) {
                (nativeAdContainer).removeView(scrollView);
            }

            if (size.equalsIgnoreCase("100")) {
                if (adCustomType.equalsIgnoreCase("custom")) {
                    scrollView = new NativeAdScrollView(context, manager,
                            new AdViewProvider() {

                                @Override
                                public void destroyView(NativeAd nativeAd,
                                                        View arg1) {
                                    nativeAd.unregisterView();
                                }

                                @Override
                                public View createView(NativeAd nativeAd,
                                                       int arg1) {
                                    View adView = inflater.inflate(
                                            R.layout.conversation_list_item, null);
                                    inflateCustomAd(nativeAd, adView, context);
                                    Utility.fireBusEvent(context, true, new AdEvent(AdEvent.AD_LOAD_SUCCESS));
                                    return adView;
                                }
                            });

                } else {
                    scrollView = new NativeAdScrollView(context, manager,
                            NativeAdView.Type.HEIGHT_100);
                }

            } else if (size.equalsIgnoreCase("120")) {
                scrollView = new NativeAdScrollView(context, manager,
                        NativeAdView.Type.HEIGHT_120);
            } else if (size.equalsIgnoreCase("300")) {
                scrollView = new NativeAdScrollView(context, manager,
                        NativeAdView.Type.HEIGHT_300);
            } else if (size.equalsIgnoreCase("400")) {
                scrollView = new NativeAdScrollView(context, manager,
                        NativeAdView.Type.HEIGHT_400);
            } else {
                scrollView = new NativeAdScrollView(context, manager,
                        NativeAdView.Type.HEIGHT_100);
            }

            (nativeAdContainer).addView(scrollView);
        } catch (Exception ignored) {
        }

    }

    protected void inflateCustomAd(NativeAd nativeAd, View adView,
                                   Context context) {
        try {

            ConversationListAdapter.ConversationMessageViewHolder cmvh = new ConversationListAdapter.ConversationMessageViewHolder(adView);

            Preconditions.checkNotNull(cmvh.getProfile_pic_spark_indicator()).setVisibility(View.GONE);
            cmvh.getProfileTitleTv().setText(nativeAd.getAdTitle());
            cmvh.getProfileTitleTv().setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
//            cmvh.getConversationSelectLabel().setVisibility(View.GONE);
            cmvh.getLastMessageTv().setText(nativeAd.getAdSubtitle());
            cmvh.getConversationDateTv().setVisibility(View.GONE);
            cmvh.getSponsoredTv().setVisibility(View.VISIBLE);
            cmvh.getConversationListActionBlockIv().setVisibility(View.GONE);
            //cmvh.profileTitleTv.setTypeface(null, Typeface.NORMAL);
            cmvh.getLastMessageTv().setTypeface(null, Typeface.NORMAL);
            cmvh.getLastMessageTv().setTextColor(context.getResources().getColor(R.color.grey_text_color));
            cmvh.getFront().setBackgroundResource(R.drawable.conversation_list_item_front);
            NativeAd.Image adIcon = nativeAd.getAdIcon();
            NativeAd.downloadAndDisplayImage(adIcon, cmvh.getProfilePicIv());
//			Picasso.with(context).load(nativeAd.getAdIcon()).transform(new CircleTransformation())
//					.into(cmvh.getProfilePicIv());


//			LinearLayout layout = (LinearLayout) adView
//					.findViewById(R.id.nativeAdIconLinearLayout);
//			ImageView nativeAdIcon = (ImageView) adView
//					.findViewById(R.id.nativeAdIconCustom);
//			TextView nativeAdTitle = (TextView) adView
//					.findViewById(R.id.nativeAdTitleCustom);
//			layout.setVisibility(View.VISIBLE);
//			Button nativeAdCallToAction = (Button) adView
//					.findViewById(R.id.nativeAdCallToActionCustom);
//			nativeAdCallToAction.setVisibility(View.VISIBLE);
//			TextView tvAds = (TextView) adView.findViewById(R.id.tvAds);
//			tvAds.setVisibility(View.VISIBLE);
//			// Setting the Text
//			nativeAdCallToAction.setText(nativeAd.getAdCallToAction());
//			nativeAdCallToAction.setVisibility(View.VISIBLE);
//			nativeAdCallToAction.setText(nativeAd.getAdCallToAction());
//			nativeAdTitle.setText(nativeAd.getAdTitle());
//			// Downloading and setting the ad icon.
//			NativeAd.Image adIcon = nativeAd.getAdIcon();
//			NativeAd.downloadAndDisplayImage(adIcon, nativeAdIcon);
//			// Downloading and setting the cover image.
//			WindowManager wm = (WindowManager) context
//					.getSystemService(Context.WINDOW_SERVICE);
//			Display display = wm.getDefaultDisplay();
//			DisplayMetrics metrics = new DisplayMetrics();
//			display.getMetrics(metrics);
            nativeAd.registerViewForInteraction(adView);
        } catch (Exception ignored) {
        }

    }

}
