package seventynine.sdk.adapter;

import android.content.Context;
import android.view.ViewGroup;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.seventynine.sdkadapter.AdapterCallback;

public class FacebookBannerAdapter {

    AdView adView;
    AdapterCallback adCallbackListener = new AdapterCallback();
    ViewGroup nativeAdContainerView;
    String strZoneId;

    public void showFbBanner(Context context, String Pid,
                             ViewGroup nativeAdContainer, String BannerHeight, String zoneId) {

        try {

            nativeAdContainerView = nativeAdContainer;
            strZoneId = zoneId;

            if (nativeAdContainer != null) {
                ViewGroup parentViewGroup = nativeAdContainer;
                if (parentViewGroup != null) {
                    parentViewGroup.removeAllViews();

                    adView = new AdView(context, Pid, AdSize.BANNER_320_50);
                    nativeAdContainer.addView(adView);
//					 AdSettings.addTestDevice("2bd0dc2bcbb8824e8cb08d305b04c63b");
                    //f45ff523b14cbe49382374d4cdf055b3
                    adView.loadAd();

                    adView.setAdListener(new AdListener() {

                        @Override
                        public void onError(Ad ad, AdError error) {

                            adCallbackListener.onAdFaild("FacebookBanner",
                                    nativeAdContainerView, strZoneId);
                        }

                        @Override
                        public void onAdLoaded(Ad ad) {
                            adCallbackListener.onAdLoad("FacebookBanner",
                                    strZoneId);
                        }

                        @Override
                        public void onAdClicked(Ad ad) {
                            adCallbackListener.onClick();
                        }

                    });

                }
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

}
