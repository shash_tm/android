package seventynine.sdk.adapter;

import android.content.Context;
import android.util.Log;
import android.view.ViewGroup;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.InterstitialAd;
import com.facebook.ads.InterstitialAdListener;
import com.seventynine.sdkadapter.AdapterCallback;
import com.seventynine.sdkadapter.CallBackAdapter.AdCallbackListener;
import com.seventynine.sdkadapter.CallBackAdapter.AdapterCallbackListener;
import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.ads.AdsHelper;
import com.trulymadly.android.app.ads.AdsType;
import com.trulymadly.android.app.ads.SeventyNineHandler;

import java.util.HashMap;

public class FaceBookInterstitialAdapter implements AdListener,
        InterstitialAdListener, AdapterCallbackListener {

    AdCallbackListener adCallbackListener;
    String zoneId = "";
    ViewGroup adViewContainer;
    private InterstitialAd interstitialAd;
    private Context mContext;

    // private void loadInterstitialAd() {
    //
    // }

    @Override
    public void onLoad(Context context, String Pid,
                       AdapterCallback adapterCallbackListener, String zoneId,
                       ViewGroup adViewContainer) {
        Log.d(SeventyNineHandler.TAG, "FaceBookInterstitialAdapter : onLoad");
        mContext = context.getApplicationContext();
        adCallbackListener = adapterCallbackListener;
        this.zoneId = zoneId;
        this.adViewContainer = adViewContainer;
        Log.e("*****", "In FaceBookAdapterInterstitial  " + Pid);
//        AdSettings.addTestDevice("c816a11f6b34f2f68183cad9aa1f2c5f");
        interstitialAd = new InterstitialAd(context, Pid);
        interstitialAd.setAdListener(this);
        // AdSettings.addTestDevice("46625b79df08cb7245c7a959bd959698");
        interstitialAd.loadAd();
    }

    @Override
    public void onAdShow() {
        Log.d(SeventyNineHandler.TAG, "FaceBookInterstitialAdapter : onAdShow");
        //Commenting out to fix the static listenr issue
//        adCallbackListener.onAdShow();
        AdsHelper.adShown(mContext, AdsType.MATCHES_END_INTERSTITIAL);
        HashMap<String, String> params = new HashMap<>();
        params.put("method", "onAdShow");
        AdsHelper.addParams(params);
        TrulyMadlyTrackEvent.trackEvent(mContext, TrulyMadlyEvent.TrulyMadlyActivities.matches,
                TrulyMadlyEvent.TrulyMadlyEventTypes.ad_interstitial, 0,
                TrulyMadlyEvent.TrulyMadlyEventStatus.ad_mediation_shown_fb, params, true);
    }

    @Override
    public void onAdClicked(Ad arg0) {
        Log.d(SeventyNineHandler.TAG, "FaceBookInterstitialAdapter : onAdClicked");
        //Commenting out to fix the static listenr issue
//        adCallbackListener.onClick();
    }

    @Override
    public void onAdLoaded(Ad arg0) {
        Log.d(SeventyNineHandler.TAG, "FaceBookInterstitialAdapter : onAdLoaded");
        if (interstitialAd != null) {
            //Commenting out to fix the static listenr issue
//            adCallbackListener.onAdLoad("FaceBookInterstitial", zoneId);
            interstitialAd.show();

            HashMap<String, String> params = new HashMap<>();
            params.put("method", "onLoad");
            AdsHelper.addParams(params);

            TrulyMadlyTrackEvent.trackEvent(mContext, TrulyMadlyEvent.TrulyMadlyActivities.matches,
                    TrulyMadlyEvent.TrulyMadlyEventTypes.ad_interstitial, 0,
                    TrulyMadlyEvent.TrulyMadlyEventStatus.ad_mediation_success_fb, params, true);
        } else {
            //Commenting out to fix the static listenr issue
//            adCallbackListener.onAdFaild("FaceBookInterstitial", adViewContainer, zoneId);
            HashMap<String, String> params = new HashMap<>();
            params.put("method", "onLoad");
            params.put("error", "interstitialAd is null");
            AdsHelper.addParams(params);
            TrulyMadlyTrackEvent.trackEvent(mContext, TrulyMadlyEvent.TrulyMadlyActivities.matches,
                    TrulyMadlyEvent.TrulyMadlyEventTypes.ad_interstitial, 0,
                    TrulyMadlyEvent.TrulyMadlyEventStatus.ad_mediation_failed_fb, params, true);
        }
    }

    @Override
    public void onError(Ad arg0, AdError arg1) {
        Log.d(SeventyNineHandler.TAG, "FaceBookInterstitialAdapter : onError");
        Log.e("*** FaceBook Error" + arg1.getErrorMessage(),
                "  FaceBook Error Code  " + arg1.getErrorCode());

        //Commenting out to fix the static listenr issue
//        adCallbackListener.onAdFaild("FaceBookInterstitial", adViewContainer,
//                zoneId);

        HashMap<String, String> params = new HashMap<>();
        params.put("method", "onError");
        params.put("error", arg1.getErrorMessage());
        AdsHelper.addParams(params);
        TrulyMadlyTrackEvent.trackEvent(mContext, TrulyMadlyEvent.TrulyMadlyActivities.matches,
                TrulyMadlyEvent.TrulyMadlyEventTypes.ad_interstitial, 0,
                TrulyMadlyEvent.TrulyMadlyEventStatus.ad_mediation_failed_fb, params, true);

    }

    @Override
    public void onInterstitialDismissed(Ad arg0) {
        Log.d(SeventyNineHandler.TAG, "FaceBookInterstitialAdapter : onInterstitialDismissed");
        //Commenting out to fix the static listenr issue
//        adCallbackListener.onAdClose();
    }

    @Override
    public void onInterstitialDisplayed(Ad arg0) {
        AdsHelper.adShown(mContext, AdsType.MATCHES_END_INTERSTITIAL);
        Log.d(SeventyNineHandler.TAG, "FaceBookInterstitialAdapter : onInterstitialDisplayed");
        HashMap<String, String> params = new HashMap<>();
        params.put("method", "onInterstitialDisplayed");
        AdsHelper.addParams(params);
        TrulyMadlyTrackEvent.trackEvent(mContext, TrulyMadlyEvent.TrulyMadlyActivities.matches,
                TrulyMadlyEvent.TrulyMadlyEventTypes.ad_interstitial, 0,
                TrulyMadlyEvent.TrulyMadlyEventStatus.ad_mediation_shown_fb, params, true);
    }

}
