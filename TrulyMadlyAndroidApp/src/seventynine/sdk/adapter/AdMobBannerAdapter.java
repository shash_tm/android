package seventynine.sdk.adapter;

import android.content.Context;
import android.view.ViewGroup;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdRequest.Builder;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.seventynine.sdkadapter.AdapterCallback;

public class AdMobBannerAdapter {

    ViewGroup adViewContainer;
    AdapterCallback adapterCallbackListener = new AdapterCallback();
    String zoneId = "";
    private AdView mGoogleAdView;

    public void showAdmobBanner(Context context, String Pid,
                                ViewGroup nativeAdContainer, String BannerHeight, String zoneId) {

        this.adViewContainer = nativeAdContainer;
        this.zoneId = zoneId;

        try {
            if (mGoogleAdView == null) {
                mGoogleAdView = new AdView(context);
                mGoogleAdView.setAdListener(new AdViewListener());

                mGoogleAdView.setAdUnitId(Pid);

                if (BannerHeight.equalsIgnoreCase("banner")) {
                    mGoogleAdView.setAdSize(AdSize.BANNER);
                } else if (BannerHeight.equalsIgnoreCase("LARGE_BANNER")) {
                    mGoogleAdView.setAdSize(AdSize.LARGE_BANNER);
                } else if (BannerHeight.equalsIgnoreCase("MEDIUM_RECTANGLE")) {
                    mGoogleAdView.setAdSize(AdSize.MEDIUM_RECTANGLE);
                } else if (BannerHeight.equalsIgnoreCase("SMART_BANNER")) {
                    mGoogleAdView.setAdSize(AdSize.SMART_BANNER);
                }

            }
            final Builder adbuilder = new AdRequest.Builder();
            final AdRequest adRequest = adbuilder.build();
            // final AdRequest adRequest = adbuilder.addTestDevice(
            // "C30159FD049D4F51DA446496F948D073").build();
            mGoogleAdView.loadAd(adRequest);
        } catch (Exception ignored) {
        }
    }

    private class AdViewListener extends AdListener {
        /*
         * Google Play Services AdListener implementation
         */
        @Override
        public void onAdClosed() {

        }

        @Override
        public void onAdFailedToLoad(int errorCode) {

            adapterCallbackListener.onAdFaild("AdMobBanner", adViewContainer,
                    zoneId);
            mGoogleAdView = null;
//            Utility.fireBusEvent(mGoogleAdView.getContext(), true, new AdEvent(AdEvent.AD_LOAD_FAILURE));
        }

        @Override
        public void onAdLeftApplication() {
        }

        @Override
        public void onAdLoaded() {

            if (adapterCallbackListener != null) {

                adapterCallbackListener.onAdLoad(mGoogleAdView, "AdMobBanner",
                        zoneId);

                if (adViewContainer != null) {
                    ViewGroup parentViewGroup = adViewContainer;
                    if (parentViewGroup != null) {
                        parentViewGroup.removeAllViews();
                        adViewContainer.addView(mGoogleAdView);
//                        Utility.fireBusEvent(mGoogleAdView.getContext(), true, new AdEvent(AdEvent.AD_LOAD_SUCCESS));
                    }
                }

            }
        }

        @Override
        public void onAdOpened() {

            adapterCallbackListener.onAdShow();
        }
    }

}
