package io.socket.client;

/**
 * Acknowledgement.
 */
@SuppressWarnings("ALL")
public interface Ack {

    public void call(Object... args);

}

