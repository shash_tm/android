package test.java.com.trulymadly.android.app;

import com.trulymadly.android.app.utility.Utility;

import org.junit.Test;

import java.util.ArrayList;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

/**
 * Created by avin on 30/11/15.
 */

public class UtilityTest {

    /*
    * This is to test Utility#checkEmail
    * */
    @Test
    public void test_checkEmail(){
        assertFalse(Utility.checkEmail("ajhbwde"));
        assertFalse(Utility.checkEmail(null));
        assertFalse(Utility.checkEmail(""));
        assertFalse(Utility.checkEmail("test@test"));
        assertFalse(Utility.checkEmail("te@st@test.com"));
        assertFalse(Utility.checkEmail("te$st@test.com"));
        assertTrue(Utility.checkEmail("test@test.com"));
    }

    /*
    * This is to test Utility#isSet
    * */
    @Test
    public void test_isSet(){
        assertFalse(Utility.isSet(""));
        assertFalse(Utility.isSet("null"));
        assertFalse(Utility.isSet(null));

        assertTrue(Utility.isSet("test"));
    }

    /*
    * This is to test Utility#encodeURI
    * Basic rules are:
    * 1. The alphanumeric characters "a" through "z", "A" through "Z" and "0" through "9" remain the same.
    * 2. The special characters ".", "-", "*", and "_" remain the same.
    * 3. The space character " " is converted into a plus sign "+".
    * 4. All other characters are unsafe and are first converted into one or more bytes using some
    * encoding scheme. Then each byte is represented by the 3-character string "%xy",
    * where xy is the two-digit hexadecimal representation of the byte. The recommended encoding
    * scheme to use is UTF-8. However, for compatibility reasons, if an encoding is not specified,
    * then the default encoding of the platform is used.
    * */
    @Test
    public void test_encodeURI(){
        assertEquals("", Utility.encodeURI(""));
        assertEquals("", Utility.encodeURI(null));

        //Not sure if we need to handle this case
//        assertEquals("null", Utility.encodeURI("null"));

        assertEquals("test", Utility.encodeURI("test"));
        assertEquals("Tes1T0", Utility.encodeURI("Tes1T0"));
        assertEquals("_T.e*s-1T0", Utility.encodeURI("_T.e*s-1T0"));
        assertEquals("_T+.e*s-1T+0", Utility.encodeURI("_T .e*s-1T 0"));

        assertFalse("_T .e*s-1T 0".equals(Utility.encodeURI("_T .e*s-1T 0")));
        assertFalse("test#$%".equals(Utility.encodeURI("test#$%")));
        assertEquals("test%25", Utility.encodeURI("test%"));
        assertEquals("test%25%22", Utility.encodeURI("test%\""));
    }

    /*
    * This is to test Utility#encodeURI
    * Basic rules are:
    * 1. The alphanumeric characters "a" through "z", "A" through "Z" and "0" through "9" remain the same.
    * 2. The special characters ".", "-", "*", and "_" remain the same.
    * 3. The space character " " is converted into a plus sign "+".
    * 4. All other characters are unsafe and are first converted into one or more bytes using some
    * encoding scheme. Then each byte is represented by the 3-character string "%xy",
    * where xy is the two-digit hexadecimal representation of the byte. The recommended encoding
    * scheme to use is UTF-8. However, for compatibility reasons, if an encoding is not specified,
    * then the default encoding of the platform is used.
    * */
    @Test
    public void test_decodeURI(){
        assertEquals(null, Utility.decodeURI(""));
        assertEquals(null, Utility.decodeURI(null));

        //Not sure if we need to handle this case
//        assertEquals("null", Utility.encodeURI("null"));

        assertEquals("test", Utility.decodeURI("test"));
        assertEquals("Tes1T0", Utility.decodeURI("Tes1T0"));
        assertEquals("_T.e*s-1T0", Utility.decodeURI("_T.e*s-1T0"));
        assertEquals("_T+.e*s-1T+0", Utility.decodeURI("_T+.e*s-1T+0"));

        assertEquals("test#$%", Utility.decodeURI("test#$%25"));
        assertEquals("test%", Utility.decodeURI("test%25"));
        assertEquals("test%\"", Utility.decodeURI("test%25%22"));
    }

    /*
    * This is to test Utility#isValidJsonObject
    * */
    @Test
    public void test_isValidJsonObject(){
        assertTrue(Utility.isValidJsonObject(""));
        assertTrue(Utility.isValidJsonObject(null));
        assertTrue(Utility.isValidJsonObject("kjncdew"));
        assertTrue(Utility.isValidJsonObject("{}"));
        assertTrue(Utility.isValidJsonObject("{'hello':hfv}"));
        assertTrue(Utility.isValidJsonObject("{\"hello\":hfv}"));
    }

    /*
    * This is to test Utility#stringCompare
    * */
    @Test
    public void test_stringCompare(){
//        assertTrue(Utility.stringCompare("", ""));
//        assertTrue(Utility.stringCompare(null, null));
        assertTrue(Utility.stringCompare("hello", "HELLO"));
        assertTrue(Utility.stringCompare("hello", "hello"));
    }

    /*
    * This is to test Utility#generateUniqueRandomId
    * */
    @Test
    public void test_generateUniqueRandomId(){
        ArrayList<String> ids = new ArrayList<>();
        String id = null;
        for(int i = 0; i < 25; i++){
            id = Utility.generateUniqueRandomId();
            assertFalse(ids.contains(id));
            assertTrue(Utility.isSet(id));
            ids.add(id);
        }
    }
}
