package com.cesards.cropimageview;

import android.support.annotation.NonNull;

abstract class CropImage {

    final
    @NonNull
    CropImageView imageView;

    CropImage(@NonNull CropImageView imageView) {
        this.imageView = imageView;
    }
}
