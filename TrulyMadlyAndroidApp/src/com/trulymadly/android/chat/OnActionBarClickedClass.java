package com.trulymadly.android.chat;

import android.support.v7.app.AppCompatActivity;

import com.trulymadly.android.app.listener.OnActionBarClickedInterface;
import com.trulymadly.android.app.modal.MatchMessageMetaData;
import com.trulymadly.android.app.utility.ActivityHandler;
import com.trulymadly.android.app.utility.Utility;

public class OnActionBarClickedClass implements OnActionBarClickedInterface {

	private final AppCompatActivity activity;
	private MatchMessageMetaData matchMessageMetaData;
	
	public OnActionBarClickedClass(AppCompatActivity activity,MatchMessageMetaData matchMessageMetaData) {
		this.activity=activity;
		this.matchMessageMetaData= matchMessageMetaData ;
		
	}
	
	@Override
	public void onUserProfileClicked() {
		if (Utility.isSet(matchMessageMetaData.getProfileUrl())) {
			ActivityHandler.startProfileActivity(activity,
                    matchMessageMetaData.getProfileUrl(), false, false, false);
		}
	}

	@Override
	public void onConversationsClicked() {
	}

    @Override
    public void onLocationClicked() {
    }

	@Override
	public void onBackClicked() {
		this.activity.onBackPressed();
	}

	@Override
	public void onTitleClicked() {	
	}
	
	@Override
	public void onTitleLongClicked() {	
	}

	@Override
	public void onSearchViewOpened() {

	}

	@Override
	public void onSearchViewClosed() {

	}

	@Override
	public void onCategoriesClicked() {

	}

	@Override
	public void onCuratedDealsClicked() {

	}

	public void setMetaData(MatchMessageMetaData metaData) {
		matchMessageMetaData = metaData;
		
	}
}
