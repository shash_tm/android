package com.trulymadly.android.chat;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

import com.trulymadly.android.app.R;
import com.trulymadly.android.app.adapter.GalleryListAdapter;
import com.trulymadly.android.app.adapter.GalleryListAdapter.OnClickInterface;
import com.trulymadly.android.app.adapter.StickerPagerAdapter;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.modal.StickerData;
import com.trulymadly.android.app.sqlite.StickerDBHandler;
import com.trulymadly.android.app.utility.UiUtils;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public class StickerFragment extends Fragment implements OnPageChangeListener {
	private GalleryListAdapter mGalleryListAdapter;
	private StickerPagerAdapter mStickerPagerAdapter;
	private ViewPager mViewPager;
	private RecyclerView mGalleryListView2;
	private Activity aActivity;
	private LinkedHashMap<Integer, StickerData> hashMapGalleries;
	private ArrayList<StickerData> recentStickers;
	private int previousPageSelected = 0;
	private boolean islaunched;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle paramBundle) {
		this.islaunched = false;
		aActivity = getActivity();
		View stickerFragmentView = inflater.inflate(
				R.layout.sticker_fragment_layout, container, false);
		LinkedHashMap<StickerData, ArrayList<StickerData>> hashMapStickers = StickerDBHandler.createHashMap(aActivity);
		hashMapGalleries = StickerDBHandler.getGalleries(hashMapStickers);
		recentStickers = StickerDBHandler.createRecentUsedStickers(aActivity);

		mGalleryListView2 = (RecyclerView) stickerFragmentView
				.findViewById(R.id.gallery_list_view_2);
		mGalleryListView2.setHasFixedSize(true);
		LinearLayoutManager mListLayoutManager = new LinearLayoutManager(aActivity,
				LinearLayoutManager.HORIZONTAL, false);

		mGalleryListAdapter = new GalleryListAdapter(aActivity,
				hashMapGalleries);
		OnClickInterface mOnClickInterface = new OnClickInterface() {

			@Override
			public void onClick(int position) {
				mViewPager.setCurrentItem(position);
			}
		};
		mGalleryListAdapter.setOnClickInterface(mOnClickInterface);
		mGalleryListView2.setLayoutManager(mListLayoutManager);
		mGalleryListView2.setAdapter(mGalleryListAdapter);

		LayoutParams params = new RelativeLayout.LayoutParams(-1,
				(int) (0.4D * UiUtils.getScreenHeight(aActivity)));
		stickerFragmentView.setLayoutParams(params);

		mStickerPagerAdapter = new StickerPagerAdapter(aActivity,
				hashMapGalleries, hashMapStickers, recentStickers);
		mViewPager = (ViewPager) stickerFragmentView.findViewById(R.id.pager);
        mViewPager.addOnPageChangeListener(this);
        mViewPager.setAdapter(mStickerPagerAdapter);

		return stickerFragmentView;
	}

	@Override
	public void onViewCreated(android.view.View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		initializeUI();
	}

	private void initializeUI() {
		if (recentStickers.size() == 0) {
			mViewPager.setCurrentItem(1);
		} else {
			mViewPager.setCurrentItem(0);
		}
	}

	@Override
	public void onPageScrollStateChanged(int position) {
	}

	@Override
	public void onPageScrolled(int position, float arg1, int position2) {
	}

	@Override
	public void onPageSelected(int position) {
		if (position == 0) {
			recentStickers = StickerDBHandler
					.createRecentUsedStickers(aActivity);

			mStickerPagerAdapter
					.updateRecentStickerGridView(
							recentStickers,
							(GridView) mViewPager
									.findViewWithTag(Constants.STICKER_RECENT_GALLERY_ID));

		}
		setColor(position);
		if (this.islaunched) {
			scroll(position, position - previousPageSelected);
		}
		islaunched = true;
		previousPageSelected = position;
		mGalleryListAdapter.onItemChanged(position);
	}

	private void scroll(int position, int direction) {
		if (direction >= 0) {
			if (position + 1 <= hashMapGalleries.size())
				mGalleryListView2.scrollToPosition(position + 1);
			else
				mGalleryListView2.scrollToPosition(position);

		}

		else {
			if (position - 1 >= 0)
				mGalleryListView2.scrollToPosition(position - 1);
			else
				mGalleryListView2.scrollToPosition(position);
		}
	}

	private void setColor(int position) {
		mGalleryListAdapter.setSelectedIndex(position);
		mGalleryListAdapter.notifyDataSetChanged();

	}

}
