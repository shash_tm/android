package com.trulymadly.android.chat;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes;
import com.trulymadly.android.app.CircleTransformation;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.bus.BackPressed;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.listener.CustomOkHttpResponseHandler;
import com.trulymadly.android.app.modal.NudgeClass;
import com.trulymadly.android.app.modal.QuizImage;
import com.trulymadly.android.app.modal.TakeQuizFragmentModal;
import com.trulymadly.android.app.sqlite.QuizDBHandler;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.ImageCacheHelper;
import com.trulymadly.android.app.utility.OkHttpHandler;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

public class TakeQuizFragment extends Fragment
        implements OnClickListener, OnCheckedChangeListener, OnKeyListener {

    private final RadioButton[] radioButtonArr = new RadioButton[10];
    private int quiz_id;
    private QuizImage imagelocations;
    private Activity aActivity;
    private View oldOverlay = null;
    private ViewGroup questionContainer;
    private RelativeLayout nudgeLayout;
    private ImageView imageOption0, imageOption1, imageOption2, imageOption3;
    private int questionNo;
    private ArrayList<String> questions;
    private LinkedHashMap<Integer, Integer> positionMap;
    private LinkedHashMap<Integer, ArrayList<String>> answers;
    private LinkedHashMap<Integer, String> selectedAnswer;
    private LinkedHashMap<String, String> optionID;
    private LinkedHashMap<Integer, String> optionType;
    private Animation pushLeftOut, pushRightIn;
    private RadioGroup option_radio_group;
    private ProgressBar loader;
    private int checkedId;
    private ImageView selectedImageView, previousImageView;
    private String selectedUrl, previousUrl;
    private int selectedIndex, countSameSelected;
    private int previousIndex;
    private JSONObject response;
    private String match_id, match_name;
    private String quiz_name;
    private ProgressDialog mProgressDialog;
    private ImageView quiz_icon_sq;
    private LinearLayout start_quiz;
    private TextView quiz_name_sq, quiz_description_sq, sponsored_tv;
    private ImageView play_button_sq;
    private boolean showNudgeScreen = false;
    private BackPressed backPressed;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        this.checkedId = -1;
        aActivity = getActivity();
        TakeQuizFragmentModal modal = (TakeQuizFragmentModal) getArguments()
                .getSerializable("takeQuizFragmentModal");
        this.response = modal.getResponse();
        this.quiz_name = modal.getQuizName();
        this.quiz_id = modal.getQuizId();
        this.match_id = modal.getMatchId();
        this.match_name = modal.getMatchName();
        pushLeftOut = AnimationUtils.loadAnimation(aActivity,
                R.anim.push_left_out);
        pushRightIn = AnimationUtils.loadAnimation(aActivity,
                R.anim.push_right_in);
        pushLeftOut.setDuration(400);
        pushRightIn.setDuration(400);
        imagelocations = QuizDBHandler.getQuizImageUrl(aActivity, quiz_id);
        backPressed = new BackPressed();
        backPressed.setMatchId(match_id);
        backPressed.setQuizId(quiz_id);
        backPressed.setQuizName(quiz_name);
        backPressed.setFromTakeQuizFragement(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle paramBundle) {

        View overlayContainer = inflater.inflate(R.layout.quiz_overlay, container,
                false);

        overlayContainer.setOnClickListener(this);

        questionContainer = (ViewGroup) overlayContainer
                .findViewById(R.id.question_container);
        // nudgeLayout = (RelativeLayout) overlayContainer
        // .findViewById(R.id.nudge_screen);
        start_quiz = (LinearLayout) overlayContainer
                .findViewById(R.id.start_quiz);
        quiz_icon_sq = (ImageView) start_quiz.findViewById(R.id.quiz_icon_sq);
        quiz_name_sq = (TextView) start_quiz.findViewById(R.id.quiz_name_sq);
        quiz_description_sq = (TextView) start_quiz.findViewById(R.id.quiz_description_sq);
        sponsored_tv = (TextView) start_quiz.findViewById(R.id.sponsored_tv);
        play_button_sq = (ImageView) start_quiz
                .findViewById(R.id.play_button_sq);
        createStartQuizUI();

        positionMap = new LinkedHashMap<>();
        answers = new LinkedHashMap<>();
        questions = new ArrayList<>();
        optionID = new LinkedHashMap<>();
        optionType = new LinkedHashMap<>();
        selectedAnswer = new LinkedHashMap<>();

        // loader = (ProgressBar) newOverlay
        // .findViewById(R.id.take_quiz_fragment_loader);
        // loader.setVisibility(View.VISIBLE);
        // loader.setVisibility(View.GONE);
        ArrayList<NudgeClass> nudgeList = new ArrayList<>();
        questionNo = 1;
        createQuestionsAndAnswers(response);

        return overlayContainer;

    }

    private void createStartQuizUI() {
        quiz_name_sq.setText(quiz_name);

        if (imagelocations != null) {
            ImageCacheHelper.with(aActivity).
                    loadWithKey(imagelocations.getThumbnailServerLocation(), quiz_id + "")
                    .transform(new CircleTransformation())
                    .into(quiz_icon_sq, null);
        }

        quiz_description_sq.setText(response.optString("description"));
        String sponsoresText = response.optString("sponsored_text");
        if(Utility.isSet(sponsoresText)) {
            sponsored_tv.setText(sponsoresText);
            sponsored_tv.setVisibility(View.VISIBLE);
        }else{
            sponsored_tv.setVisibility(View.GONE);
        }
        play_button_sq.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                createQuestionUI(start_quiz);
                start_quiz.setVisibility(View.GONE);
            }
        });

    }

    private void createQuestionsAndAnswers(JSONObject responseJSON) {

        JSONArray questionsArray = responseJSON.optJSONArray("questions");

        for (int i = 0; i < questionsArray.length(); i++) {
            JSONObject question = questionsArray.optJSONObject(i);
            questions.add(question.optString("question_text"));
            int question_id = question.optInt("question_id");
            positionMap.put(i, question_id);
            optionType.put(question_id, question.optString("option_type"));

            JSONArray options = question.optJSONArray("options");

            ArrayList<String> option_text = new ArrayList<>();

            for (int j = 0; j < options.length(); j++) {
                JSONObject option = options.optJSONObject(j);
                if (optionType.get(question_id).equalsIgnoreCase("text")) {
                    option_text.add(option.optString("option_text"));
                    optionID.put(option.optString("option_text"),
                            option.optString("option_id"));
                } else {
                    option_text.add(option.optString("option_image"));
                    Picasso.with(aActivity)
                            .load(option.optString("option_image")).fetch();
                    optionID.put(option.optString("option_image"),
                            option.optString("option_id"));
                }

            }
            answers.put(question_id, option_text);

        }

    }

    private void nextQuestion() {
        if (checkedId != -1) {
            questionNo++;
            if (questionNo <= questions.size()) {
                createQuestionUI(oldOverlay);
            } else if (questionNo == questions.size() + 1) {
                sendAnswersToServer();
            }
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.quiz_next_button:
                nextQuestion();
                break;
            case R.id.radio_button_image_0:
                init(0);
                break;
            case R.id.radio_button_image_1:
                init(1);
                break;
            case R.id.radio_button_image_2:
                init(2);
                break;
            case R.id.radio_button_image_3:
                init(3);
                break;

            case R.id.cross_button:
                Utility.fireBusEvent(aActivity, true, new BackPressed());
                break;
            case R.id.nudge_tv:
                backPressed.setSendNudge(true);
                Utility.fireBusEvent(aActivity, true, backPressed);
                break;
            case R.id.skip_tv:
                backPressed.setSendNudge(false);
                Utility.fireBusEvent(aActivity, true, backPressed);
                break;

            case R.id.quiz_options_radio_group:
                int radioButtonID = option_radio_group.getCheckedRadioButtonId();
                View radioButton = option_radio_group.findViewById(radioButtonID);
                int idx = option_radio_group.indexOfChild(radioButton);
                setAnswer(idx);
                this.checkedId = idx;
                nextQuestion();
                break;
            default:
                break;

        }

    }

    private void sendAnswersToServer() {
        mProgressDialog = UiUtils.showProgressBar(aActivity, mProgressDialog,
                R.string.saving_answers_loader_message);
        CustomOkHttpResponseHandler sendAnswerHandler = new CustomOkHttpResponseHandler(
                aActivity, TrulyMadlyActivities.quiz,
                TrulyMadlyEventTypes.save_quiz_answers, quiz_id + "") {

            @Override
            public void onRequestSuccess(JSONObject responseJSON) {
                // response = responseJSON;

                // String responseType = responseJSON.optString("responseType");
                // showNudgeScreen = !(Utility.isSet(responseType) &&
                // responseType
                // .equalsIgnoreCase("common_answers"));
                createQuestionUI(oldOverlay);
                String oldStatus = QuizDBHandler.getQuizStatus(aActivity,
                        Utility.getMyId(aActivity),
                        match_id, quiz_id);

                if (oldStatus == null)
                    oldStatus = "NONE";

                String newStatus = "USER";
                showNudgeScreen = true;
                boolean fireBackPressed = false;
                if (oldStatus.equalsIgnoreCase("MATCH")
                        || oldStatus.equalsIgnoreCase("BOTH")) {
                    newStatus = "BOTH";
                    showNudgeScreen = false;
                    backPressed.setResponse(responseJSON);
                    backPressed.setShowCommonAnswers(true);
                    fireBackPressed = true;
                }

                QuizDBHandler.updateQuizStatus(aActivity,
                        Utility.getMyId(aActivity),
                        match_id, quiz_id, newStatus);

                UiUtils.hideProgressBar(mProgressDialog);

                if (fireBackPressed) {
                    Utility.fireBusEvent(aActivity, true, backPressed);
                }

            }

            @Override
            public void onRequestFailure(Exception exception) {
                UiUtils.hideProgressBar(mProgressDialog);
                AlertsHandler.showMessage(aActivity, R.string.cant_save_answers);
                questionNo = questionNo - 2;
                nextQuestion();
            }
        };

        HashMap<String, String> params = new HashMap<>();
        params.put("action", "save_answer");
        params.put("match_id", match_id);
        params.put("quiz_id", "" + quiz_id);
        JSONObject answersJsonObject = new JSONObject();
        for (Entry<Integer, String> pair : selectedAnswer.entrySet()) {
            try {
                answersJsonObject.put("" + pair.getKey(), pair.getValue());
            } catch (JSONException ignore) {
            }
        }
        params.put("answers", answersJsonObject.toString());
        OkHttpHandler.httpGet(aActivity, ConstantsUrls.get_quiz_url(), params,
                sendAnswerHandler);
    }

    private void init(int position) {
        previousUrl = selectedUrl;
        previousIndex = selectedIndex;
        previousImageView = getImageView(previousIndex);
        selectedIndex = position;
        selectedImageView = getImageView(position);
        selectedUrl = setAnswer(position);
        changeBackgroud();
        // setNextButtonEnabled(true);

        checkedId = position;
        nextQuestion();

    }

    //
    // private void setNextButtonEnabled(boolean isEnabled) {
    // nextButton.setEnabled(isEnabled);
    // nextButton
    // .setBackgroundColor(aActivity.getResources().getColor(
    // isEnabled ? R.color.material_pink_primary
    // : R.color.grey_light));
    //
    // nextButton.setTextColor(aActivity.getResources().getColor(
    // isEnabled ? R.color.black : R.color.white));
    // }

    private void changeBackgroud() {

        if (previousIndex == selectedIndex) {
            return;
        }

        UiUtils.setBackground(aActivity, selectedImageView,
                R.drawable.blue_border);
        if (previousIndex != -1) {
            UiUtils.setBackground(aActivity, previousImageView,
                    R.drawable.border_white);

        }

    }

    private void setVisibilityOff(String s) {
        if (s.equals("text")) {
            option_radio_group.setVisibility(View.GONE);
        } else {
            imageOption0.setVisibility(View.GONE);
            imageOption1.setVisibility(View.GONE);
            imageOption2.setVisibility(View.GONE);
            imageOption3.setVisibility(View.GONE);
        }

    }

    private void setVisibilityOn(String s) {
        if (s.equals("text")) {
            option_radio_group.setVisibility(View.VISIBLE);
        } else {
            imageOption0.setVisibility(View.VISIBLE);
            imageOption1.setVisibility(View.VISIBLE);
            imageOption2.setVisibility(View.VISIBLE);
            imageOption3.setVisibility(View.VISIBLE);

        }

    }

    private void createQuestionUI(View oldView) {

        View v;
        ImageView quizImageView;
        if (questionNo <= questions.size()) {
            v = LayoutInflater.from(aActivity).inflate(
                    R.layout.quiz_overlay_new, questionContainer, false);
            v.setOnClickListener(this);
            ImageView crossButton = (ImageView) v.findViewById(R.id.cross_button);
            crossButton.setOnClickListener(this);

            quizImageView = (ImageView) v
                    .findViewById(R.id.question_image_take_quiz);

            TextView questionTextView = (TextView) v.findViewById(R.id.question);
            questionTextView.setText(questions.get(questionNo - 1));

            TextView totalQuestionTv = (TextView) v
                    .findViewById(R.id.total_no_of_questions);
            totalQuestionTv.setText("" + questions.size());

            TextView questionNoTv = (TextView) v.findViewById(R.id.question_no);
            questionNoTv.setText("" + questionNo);


            if (imagelocations != null) {
                ImageCacheHelper.with(aActivity)
                        .loadWithKey(imagelocations.getThumbnailServerLocation(), quiz_id + "")
                        .transform(new CircleTransformation())
                        .into(quizImageView, null);
            }

            View images = v.findViewById(R.id.images_options);
            imageOption0 = (ImageView) images
                    .findViewById(R.id.radio_button_image_0);
            imageOption1 = (ImageView) images
                    .findViewById(R.id.radio_button_image_1);
            imageOption2 = (ImageView) images
                    .findViewById(R.id.radio_button_image_2);
            imageOption3 = (ImageView) images
                    .findViewById(R.id.radio_button_image_3);

            option_radio_group = (RadioGroup) v
                    .findViewById(R.id.quiz_options_radio_group);
            option_radio_group.setOnCheckedChangeListener(this);
            // option_radio_group.setOnClickListener(this);

            setVisibilityOff("text");
            setVisibilityOff("image");

            if (optionType.get(positionMap.get(questionNo - 1))
                    .equalsIgnoreCase("text")) {
                createUIForTextTypeOptions();
            } else {
                createUIForImageTypeOptions();
            }

        }
        // else if (showNudgeScreen) {
        else {
            v = LayoutInflater.from(aActivity).inflate(
                    R.layout.new_nudge_screen, questionContainer, false);

            v.setOnClickListener(this);

            quizImageView = (ImageView) v
                    .findViewById(R.id.question_image_nudge_screen);

            if (imagelocations != null) {
                ImageCacheHelper.with(aActivity)
                        .loadWithKey(imagelocations.getThumbnailServerLocation(), quiz_id + "")
                        .transform(new CircleTransformation())
                        .into(quizImageView, null);
            }

            TextView sendNudgeTv = (TextView) v
                    .findViewById(R.id.nudge_match_tv);

            TextView quiz_success_message_tv = (TextView) v
                    .findViewById(R.id.quiz_success_message);

            String quizSuccessMsg = response.optString("quiz_success_msg");
            if(Utility.isSet(quizSuccessMsg)){
                quiz_success_message_tv.setText(quizSuccessMsg);
            }

            String quizSuccessNudgeMsg = response.optString("quiz_success_nudge_msg");
            if(Utility.isSet(quizSuccessNudgeMsg)){
                quizSuccessNudgeMsg = quizSuccessNudgeMsg.replace("_name", match_name);
                sendNudgeTv.setText(quizSuccessNudgeMsg);
            }else {
                sendNudgeTv.setText(aActivity.getResources().getString(R.string.nudge_screen_sub_text) + " "
                        + match_name + " ?");
            }

            String quizSponsoredImage = response.optString("sponsored_image");
            ImageView sponsored_image = (ImageView) v.findViewById(R.id.sponsored_image);
            if(Utility.isSet(quizSponsoredImage)){
                sponsored_image.setVisibility(View.VISIBLE);
                Picasso.with(getContext()).load(quizSponsoredImage).into(sponsored_image);
            }else {
                sponsored_image.setVisibility(View.GONE);
            }


            TextView nudgeTv = (TextView) v.findViewById(R.id.nudge_tv);
            TextView skipTv = (TextView) v.findViewById(R.id.skip_tv);
            nudgeTv.setOnClickListener(this);
            skipTv.setOnClickListener(this);

        }
        if (v != null) {
            v.setOnClickListener(this);

            questionContainer.addView(v);

            oldView.startAnimation(pushLeftOut);
            v.startAnimation(pushRightIn);

            questionContainer.removeView(oldOverlay);

            oldOverlay = v;
        }

    }

    //
    // public void changeImageBackground() {
    // Utility.setBackground(aActivity, imageOption0,
    // R.drawable.circle_bg_transparent);
    //
    // Utility.setBackground(aActivity, imageOption1,
    // R.drawable.circle_bg_transparent);
    //
    // Utility.setBackground(aActivity, imageOption2,
    // R.drawable.circle_bg_transparent);
    //
    // Utility.setBackground(aActivity, imageOption3,
    // R.drawable.circle_bg_transparent);
    // }

    private void createUIForImageTypeOptions() {

        option_radio_group.removeAllViews();
        checkedId = -1;
        setVisibilityOff("text");
        setVisibilityOn("image");

        previousImageView = null;
        selectedImageView = null;
        previousUrl = null;
        selectedUrl = null;
        selectedIndex = -1;
        previousIndex = -1;

        final ArrayList<String> choices = answers
                .get(positionMap.get(questionNo - 1));

        Picasso.with(aActivity).load(choices.get(0)).fit()
                .placeholder(R.drawable.place_holder_banner).into(imageOption0);

        Picasso.with(aActivity).load(choices.get(1))

                .placeholder(R.drawable.place_holder_banner).into(imageOption1);

        Picasso.with(aActivity).load(choices.get(2)).fit()
                .placeholder(R.drawable.place_holder_banner).into(imageOption2);

        Picasso.with(aActivity).load(choices.get(3)).fit()
                .placeholder(R.drawable.place_holder_banner).into(imageOption3);

        setVisibilityOn("image");
        imageOption0.setOnClickListener(this);
        imageOption1.setOnClickListener(this);
        imageOption2.setOnClickListener(this);
        imageOption3.setOnClickListener(this);

    }

    private void createUIForTextTypeOptions() {
        checkedId = -1;
        setVisibilityOff("image");
        ArrayList<String> choices = answers
                .get(positionMap.get(questionNo - 1));

        option_radio_group.removeAllViews();
        for (int i = 0; i < choices.size(); i++) {
            radioButtonArr[i] = (RadioButton) LayoutInflater.from(aActivity)
                    .inflate(R.layout.radio_button_quiz_options_text,
                            option_radio_group, false);
            radioButtonArr[i].setText(choices.get(i));
            option_radio_group.addView(radioButtonArr[i]);
        }
        setVisibilityOn("text");
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        // setNextButtonEnabled(true);
        int radioButtonID = group.getCheckedRadioButtonId();
        View radioButton = group.findViewById(radioButtonID);
        int idx = group.indexOfChild(radioButton);
        setAnswer(idx);
        this.checkedId = checkedId;
        // checkedId= -1;

        nextQuestion();

    }

    private String setAnswer(int idx) {
        String url = null;
        int pos = questionNo - 1;
        if (positionMap.size() > pos && positionMap.get(pos) != null) {
            url = answers.get(positionMap.get(pos)).get(idx);
            selectedAnswer.put(positionMap.get(pos),
                    optionID.get(url));
        }
        return url;
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        return false;
    }

    private ImageView getImageView(int position) {
        if (position == 0)
            return imageOption0;
        else if (position == 1)
            return imageOption1;
        else if (position == 2)
            return imageOption2;
        else if (position == 3)
            return imageOption3;
        else
            return null;

    }
}
