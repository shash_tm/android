package com.trulymadly.android.chat;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.trulymadly.android.app.CircleTransformation;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.bus.BackPressed;
import com.trulymadly.android.app.bus.NudgeMatchesClickEvent;
import com.trulymadly.android.app.modal.NudgeClass;
import com.trulymadly.android.app.modal.NudgeListFragmentModal;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;

import java.util.ArrayList;

public class NudgeMatchesFragment extends Fragment implements
		View.OnClickListener {

	private Activity aActivity;
	private ArrayList<NudgeClass> nudgeList;
	private Button nudgeButton;
	private String quizName;
	private int quizId;
	private int nudgeCount;

	public String getQuizName() {
		return quizName;
	}

	public void setQuizName(String quizName) {
		this.quizName = quizName;
	}

	public int getQuizId() {
		return quizId;
	}

	public void setQuizId(int quizId) {
		this.quizId = quizId;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.aActivity = getActivity();
		NudgeListFragmentModal modal = (NudgeListFragmentModal) getArguments()
				.getSerializable("nudgeListFragmentModal");
		this.quizName = modal.getQuizName();
		String matchId = modal.getMatchId();
		this.quizId = modal.getQuizId();
		this.nudgeList = modal.getNudgList();
		setNudgeCount(nudgeList.size());

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle paramBundle) {

		View nudgeListFragmentView = inflater.inflate(
				R.layout.nudge_list_layout, container, false);
		ListView nudgeListView = (ListView) nudgeListFragmentView
				.findViewById(R.id.nudge_list_view);
		nudgeListView.setAdapter(new NudgeListAdapter());
		nudgeButton = (Button) nudgeListFragmentView
				.findViewById(R.id.nudge_button);
		ImageView crossButtonImage = (ImageView) nudgeListFragmentView
				.findViewById(R.id.cross_button);
		nudgeListFragmentView.setOnClickListener(null);
		nudgeButton.setOnClickListener(this);
		crossButtonImage.setOnClickListener(this);

		return nudgeListFragmentView;
	}

	public int getNudgeCount() {
		return nudgeCount;
	}

	private void setNudgeCount(int nudgeCount) {
		this.nudgeCount = nudgeCount;
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
			case R.id.nudge_button:
				if (nudgeCount > 0) {
					Utility.fireBusEvent(aActivity, true,
							new NudgeMatchesClickEvent(nudgeList, quizId, quizName,
									nudgeCount));
				}
				break;

			case R.id.cross_button:
				Utility.fireBusEvent(aActivity, true, new BackPressed());
				break;
			default:
				break;

		}

	}

	private void setNudgeButton() {
		boolean flag = false;
		for (int i = 0; i < nudgeList.size(); i++) {
			if (nudgeList.get(i).send_nudge) {
				flag = true;
				break;
			}
		}

		if (flag) {
			nudgeButton.setTextColor(aActivity.getResources().getColor(
					R.color.black));
			UiUtils.setBackground(aActivity, nudgeButton,
					R.drawable.rounded_corner_bottom_pink);
			nudgeButton.setEnabled(true);

		} else {
			nudgeButton.setBackgroundColor(aActivity.getResources().getColor(
					R.color.grey_light));
			nudgeButton.setTextColor(aActivity.getResources().getColor(
					R.color.white));

			nudgeButton.setEnabled(false);
		}
	}

	public class NudgeListAdapter extends BaseAdapter {

		public NudgeListAdapter() {
		}

		@Override
		public int getCount() {
			return nudgeList.size();
		}

		@Override
		public Object getItem(int position) {
			return nudgeList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {

			// inflate the layout
			if (convertView == null) {
				LayoutInflater inflater = aActivity
						.getLayoutInflater();
				convertView = inflater.inflate(R.layout.nudge_list_item,
						parent, false);
			}

			NudgeClass objectItem = nudgeList.get(position);

			TextView textViewItem = (TextView) convertView
					.findViewById(R.id.match_name);
			ImageView imageViewItem = (ImageView) convertView
					.findViewById(R.id.match_image);

			CheckBox checkBox = (CheckBox) convertView
					.findViewById(R.id.nudge_checkbox);

			textViewItem.setText(objectItem.match_name);
			Picasso.with(aActivity)
					.load(objectItem.match_pic_url)
					.transform(new CircleTransformation())
					.placeholder(R.drawable.place_holder_image_url).fit()
					.into(imageViewItem);

			OnCheckedChangeListener listener = new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton buttonView,
						boolean isChecked) {

					nudgeList.get(position).send_nudge = isChecked;
					setNudgeButton();

				}
			};
			checkBox.setOnCheckedChangeListener(listener);
			checkBox.setChecked(nudgeList.get(position).send_nudge);
			convertView.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					CheckBox checkBox = (CheckBox) v
							.findViewById(R.id.nudge_checkbox);
					checkBox.setChecked(!checkBox.isChecked());

					setNudgeButton();

				}
			});

			return convertView;

		}

	}

}
