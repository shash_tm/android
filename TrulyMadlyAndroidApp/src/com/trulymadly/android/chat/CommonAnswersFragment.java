package com.trulymadly.android.chat;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;
import com.trulymadly.android.app.CircleTransformation;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.adapter.CommonAnswersAdapter;
import com.trulymadly.android.app.bus.BackPressed;
import com.trulymadly.android.app.bus.SendNudgeToMatchEvent;
import com.trulymadly.android.app.bus.SendQuizMessageEvent;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.modal.CommonAnswersFragmentModal;
import com.trulymadly.android.app.modal.MatchMessageMetaData;
import com.trulymadly.android.app.modal.QuestionDetailModal;
import com.trulymadly.android.app.modal.QuizDetailModal;
import com.trulymadly.android.app.sqlite.MessageDBHandler;
import com.trulymadly.android.app.sqlite.QuizDBHandler;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class CommonAnswersFragment extends Fragment implements
		View.OnClickListener {

	private Activity aActivity;
	private JSONObject response;
	private QuizDetailModal quizModal;
	private ArrayList<QuestionDetailModal> questionList;
	private String quizUrl, quizName, matchId;
	private EditText textBox;
	private int quizId;
	private LinearLayout sendNudgeContainer;
	private LinearLayout conversationViewLayout;
	private LinearLayout sendMessageLayout;
	private LinearLayout sendNudgeLayout;
	private ImageView sendNudgeIcon;

	public CommonAnswersFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.aActivity = getActivity();
		CommonAnswersFragmentModal modal = (CommonAnswersFragmentModal) getArguments()
				.getSerializable("commonAnswersFragmentModal");
		this.response = modal.getResponse();
		this.matchId = modal.getMatchID();

	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle paramBundle) {

		View commonAnswersFragmentView = inflater.inflate(
				R.layout.common_answers_layout, container, false);
		commonAnswersFragmentView.setOnClickListener(this);

		LinearLayout startConverstionContainer = (LinearLayout) commonAnswersFragmentView
				.findViewById(R.id.start_conversation_container);
		sendNudgeContainer = (LinearLayout) commonAnswersFragmentView
				.findViewById(R.id.send_nudge_container);

		conversationViewLayout = (LinearLayout) commonAnswersFragmentView
				.findViewById(R.id.conversation_view_layout);

		sendMessageLayout = (LinearLayout) commonAnswersFragmentView
				.findViewById(R.id.send_common_answer_message_layout);

		sendNudgeLayout = (LinearLayout) commonAnswersFragmentView
				.findViewById(R.id.send_nudge_layout);

		sendNudgeIcon = (ImageView) commonAnswersFragmentView
				.findViewById(R.id.send_nudge_icon);

		startConverstionContainer.setOnClickListener(this);
		sendNudgeContainer.setOnClickListener(this);

		ImageView quizImageView = (ImageView) commonAnswersFragmentView
				.findViewById(R.id.quiz_image_view);

		ImageView flare = (ImageView) commonAnswersFragmentView.findViewById(R.id.flare);
		
		QuizDBHandler.updateFlare(aActivity,
				Utility.getMyId(aActivity), matchId,
				this.response.optInt("quiz_id"),
				this.response.optBoolean("flare"));


		if (this.response.optBoolean("flare")) {
			flare.setVisibility(View.VISIBLE);

		} else {
			flare.setVisibility(View.GONE);
		}

		ImageView crossButtonImage = (ImageView) commonAnswersFragmentView
				.findViewById(R.id.cross_button);
		quizModal = new QuizDetailModal();
		questionList = new ArrayList<>();
		View sendButton = commonAnswersFragmentView
				.findViewById(R.id.send_button_common_answers_layout);
		textBox = (EditText) commonAnswersFragmentView
				.findViewById(R.id.text_to_add_common_answers_layout);

		sendButton.setOnClickListener(this);
		crossButtonImage.setOnClickListener(this);

		parsingResponse();

		Picasso.with(aActivity).load(quizUrl)
				.transform(new CircleTransformation())
				.placeholder(R.drawable.quiz_image_option_placeholder)
				.into(quizImageView);

		LinearLayoutManager mListLayoutManager = new LinearLayoutManager(aActivity,
				LinearLayoutManager.VERTICAL, false);
		RecyclerView commonAnswers = (RecyclerView) commonAnswersFragmentView
				.findViewById(R.id.common_answers_recycler_view);

		commonAnswers.setLayoutManager(mListLayoutManager);
		commonAnswers.setHasFixedSize(true);

		CommonAnswersAdapter mCommonAnswersAdapter = new CommonAnswersAdapter(aActivity,
				questionList, quizModal);
		commonAnswers.setAdapter(mCommonAnswersAdapter);
		commonAnswersFragmentView.setOnClickListener(null);

		return commonAnswersFragmentView;
	}

	private void parsingResponse() {

		MatchMessageMetaData matchMessageMetaData = MessageDBHandler
				.getMatchMessageMetaData(
						Utility.getMyId(aActivity),
						this.matchId, aActivity);
		quizId = response.optInt("quiz_id");
		quizName = response.optString("name");
		quizUrl = response.optString("image");

		// setting user and match name and image url
		quizModal.setMatchName(matchMessageMetaData.getFName());
		quizModal.setMatchImageUrl(matchMessageMetaData.getProfilePic());
		quizModal.setUserName(SPHandler.getString(aActivity,
				ConstantsSP.SHARED_KEYS_USER_NAME));
		quizModal.setUserImageUrl(SPHandler.getString(aActivity,
				ConstantsSP.SHARED_KEYS_USER_PROFILE_THUMB_URL));

		JSONArray questions = response.optJSONArray("answers");

		if (questions != null) {
			quizModal.setNoOfQuestion(questions.length());

			for (int i = 0; i < questions.length(); i++) {
				JSONObject question = questions.optJSONObject(i);
				QuestionDetailModal questionDetailModal = new QuestionDetailModal();

				questionDetailModal.setQuestionText(question
						.optString("question_text"));
				questionDetailModal
						.setOptionType(question.optString("option_type"));

				// null checks
				JSONObject match_answer = question.optJSONObject("match_answer");
				JSONObject user_answer = question.optJSONObject("user_answer");

				if (match_answer != null && user_answer != null) {
					// image option
					if (questionDetailModal.getOptionType().equals("image")) {
						questionDetailModal.setMatchOptionImageUrl(match_answer
								.optString("image"));
						questionDetailModal.setUserOptionImageUrl(user_answer
								.optString("image"));

					}
					// text option
					else {
						questionDetailModal.setMatchOptionText(match_answer
								.optString("text"));
						questionDetailModal.setUserOptionText(user_answer
								.optString("text"));
					}
				}
				questionList.add(questionDetailModal);

			}
		}

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.send_button_common_answers_layout:
			if (Utility.isSet(textBox.getText().toString().trim())
					&& quizId > 0) {
				Utility.fireBusEvent(aActivity, true, new SendQuizMessageEvent(
						matchId, textBox.getText().toString().trim(), quizId));

			}
			break;

		case R.id.cross_button:
			Utility.fireBusEvent(aActivity, true, new BackPressed());
			break;

		case R.id.start_conversation_container:
			// if editbox is not visible show animation

			// sendMessageLayout.animate().
			if (sendMessageLayout.getVisibility() != View.VISIBLE) {
				sendNudgeLayout.setVisibility(View.GONE);
				sendNudgeIcon.setVisibility(View.VISIBLE);
				sendNudgeContainer
						.setLayoutParams(new LinearLayout.LayoutParams(UiUtils
								.dpToPx(50), UiUtils.dpToPx(50), 0f));
				conversationViewLayout.setVisibility(View.GONE);
				sendMessageLayout.setVisibility(View.VISIBLE);
			}

			break;

		case R.id.send_nudge_container:
			Utility.fireBusEvent(aActivity, true, new SendNudgeToMatchEvent(
					matchId, quizId, quizName, "BOTH", true));
			break;
		default:
			break;

		}

	}

}
