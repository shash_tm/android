package com.trulymadly.android.chat;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

import com.trulymadly.android.app.R;
import com.trulymadly.android.app.adapter.QuizGridAdapter;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.modal.QuizData;
import com.trulymadly.android.app.sqlite.QuizDBHandler;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public class QuizFragment extends Fragment {
	private Activity aActivity;
	private String match_id, user_id;

	private ArrayList<QuizData> quizList;
	private LinkedHashMap<Integer, String> statusMap;
	private LinkedHashMap<Integer, Boolean> flareMap, animationMap;

	private GridView quizGrid;
	private int maxQuizId = 0;
	private boolean onViewCreated = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.match_id = getArguments().getString("match_id");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle paramBundle) {

		aActivity = getActivity();
		quizList = QuizDBHandler.createQuizList(aActivity);

		user_id = Utility.getMyId(aActivity);
		flareMap = new LinkedHashMap<>();
		animationMap = new LinkedHashMap<>();
		statusMap = QuizDBHandler.createStatusMap(aActivity, user_id, match_id,
				flareMap, animationMap);

		View quizFragmentView = inflater.inflate(R.layout.quiz_fragment_layout,
				container, false);

		quizGrid = (GridView) quizFragmentView
				.findViewById(R.id.quiz_grid_view);
		onViewCreated = true;
		setQuizGridAdapter();
		LayoutParams params = new RelativeLayout.LayoutParams(-1,
				(int) (0.4D * UiUtils.getScreenHeight(aActivity)));
		quizFragmentView.setLayoutParams(params);

		return quizFragmentView;
	}

	@Override
	public void onDestroy() {
		onViewCreated = false;
		super.onDestroy();
	}

	public void setQuizGridAdapter() {
		if (onViewCreated) {
			quizGrid.setAdapter(new QuizGridAdapter(aActivity, quizList,
					statusMap, match_id, flareMap, animationMap,
					SPHandler.getInt(aActivity,
							ConstantsSP.SHARED_KEYS_MAX_QUIZ_ID)));
			maxQuizId = 0;
			for (QuizData quizData : quizList) {
				maxQuizId = maxQuizId > quizData.getId() ? maxQuizId
						: quizData.getId();
			}
			QuizDBHandler.updateShowAnimationFalse(user_id, match_id,
					aActivity);
		}
	}

	@Override
	public void onDestroyView() {
		SPHandler.setInt(aActivity,
				ConstantsSP.SHARED_KEYS_MAX_QUIZ_ID, maxQuizId);
		super.onDestroyView();
	}

}
