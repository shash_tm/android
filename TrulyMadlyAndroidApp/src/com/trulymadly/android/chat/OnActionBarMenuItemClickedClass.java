package com.trulymadly.android.chat;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.bus.SimpleAction;
import com.trulymadly.android.app.bus.TriggerClearChatEvent;
import com.trulymadly.android.app.listener.ConfirmDialogInterface;
import com.trulymadly.android.app.listener.CustomOkHttpResponseHandler;
import com.trulymadly.android.app.listener.OnActionBarMenuItemClickedInterface;
import com.trulymadly.android.app.modal.BlockReason;
import com.trulymadly.android.app.modal.MatchMessageMetaData;
import com.trulymadly.android.app.sqlite.MessageDBHandler;
import com.trulymadly.android.app.utility.ActivityHandler;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.FilesHandler;
import com.trulymadly.android.app.utility.OkHttpHandler;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;

public class OnActionBarMenuItemClickedClass implements
        OnActionBarMenuItemClickedInterface {

    private final Activity activity;
    private final String message_one_one_conversion_url;
    private final String mTrackingActivity;
    private BlockReason selectedReason = null;
    private MatchMessageMetaData matchMessageMetaData;
    private ProgressDialog mProgressDialog;
    private ArrayList<BlockReason> allBlockReasons;
    private InputMethodManager localInputMethodManager;

    public OnActionBarMenuItemClickedClass(Activity activity,
                                           MatchMessageMetaData matchMessageMetaData,
                                           String message_one_one_conversion_url, String trackingActivity) {
        this.activity = activity;
        this.matchMessageMetaData = matchMessageMetaData;
        this.message_one_one_conversion_url = message_one_one_conversion_url;
        mTrackingActivity = trackingActivity;
    }

    @Override
    public void onViewProfileClicked() {
        if (Utility.isSet(matchMessageMetaData.getProfileUrl())) {
            ActivityHandler.startProfileActivity(activity,
                    matchMessageMetaData.getProfileUrl(), false, false, false);
        }
    }

    @Override
    public void onDeleteSparkClicked() {
        if (matchMessageMetaData != null && matchMessageMetaData.isSparkReceived() &&
                !matchMessageMetaData.isMutualSpark()) {
            Utility.fireBusEvent(activity, true,
                    new SimpleAction(SimpleAction.ACTION_DELETE_SPARK));
        }
    }

    @Override
    public void onShareProfileClicked() {
        if (Utility.isSet(matchMessageMetaData.getProfileUrl())) {
            ActivityHandler.onShareClicked(new WeakReference<>(activity), matchMessageMetaData.getMatchId(), mTrackingActivity);
        }
    }

    @Override
    public void onBlockUserClicked() {
        if (allBlockReasons == null || allBlockReasons.size() == 0) {
            return;
        }
        LayoutInflater inflater = (LayoutInflater) this.activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.msg_report_abuse, null);

        RadioGroup radioGroup = (RadioGroup) layout
                .findViewById(R.id.radio_groups);
        final EditText other_comments = (EditText) layout
                .findViewById(R.id.other_reason);
        localInputMethodManager = (InputMethodManager) activity
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        OnFocusChangeListener focusListener = new OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    localInputMethodManager.showSoftInput(v,
                            InputMethodManager.SHOW_IMPLICIT);
                } else {
                    localInputMethodManager.hideSoftInputFromWindow(
                            v.getWindowToken(), 0);
                }
            }
        };
        other_comments.setOnFocusChangeListener(focusListener);

        if (allBlockReasons != null) {
            for (int i = 0; i < allBlockReasons.size(); i++) {
                RadioButton row = (RadioButton) LayoutInflater.from(activity)
                        .inflate(R.layout.radio_button_report_abuse, null);
                row.setText(allBlockReasons.get(i).getReason());
                row.setTag(allBlockReasons.get(i));
                radioGroup.addView(row);
            }
        }

        OnCheckedChangeListener onBlockUserChecked = new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int id) {
                RadioButton selectedButton = (RadioButton) radioGroup
                        .findViewById(id);
                if (selectedButton != null) {
                    selectedReason = (BlockReason) selectedButton.getTag();
                    other_comments.setText("");
                    if (selectedReason.isEditableNeeded()) {
                        other_comments.setVisibility(View.VISIBLE);
                        other_comments.requestFocus();
                    } else {
                        other_comments.setVisibility(View.GONE);
                    }
                }
            }
        };
        radioGroup.setOnCheckedChangeListener(onBlockUserChecked);

        AlertDialog.Builder builder = new AlertDialog.Builder(activity)
                .setView(layout);
        builder.setPositiveButton(R.string.report,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                }).setNegativeButton(R.string.cancel,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        // http://stackoverflow.com/questions/2620444/how-to-prevent-a-dialog-from-closing-when-a-button-is-clicked/15619098#15619098
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(
                new ReportAbuseButtonOnClickListner(radioGroup, alertDialog,
                        layout, this.activity,
                        this.message_one_one_conversion_url, matchMessageMetaData.getMatchId()));
    }

    public void setBlockReasons(ArrayList<BlockReason> allBlockReasons) {
        this.allBlockReasons = allBlockReasons;

    }

    public void setMetaData(MatchMessageMetaData metaData) {
        matchMessageMetaData = metaData;

    }

    @Override
    public void onUmatchUserClicked() {

        ConfirmDialogInterface confirmDialogInterface = new ConfirmDialogInterface() {

            @Override
            public void onPositiveButtonSelected() {
                CustomOkHttpResponseHandler okHttpResponseHandler = new CustomOkHttpResponseHandler(activity) {

                    @Override
                    public void onRequestSuccess(JSONObject responseJSON) {
                        UiUtils.hideProgressBar(mProgressDialog);
                        MessageDBHandler.setUserBlocked(activity, matchMessageMetaData.getMatchId());
                        Bundle mBundle = new Bundle();
                        mBundle.putString("blocked_text", activity.getResources().getString(R.string.unmatch_successfull));

                        //Clearing images of IMAGE messages
                        FilesHandler.deleteFileOrDirectoryFromImageAssets(matchMessageMetaData.getMatchId());

                        // Extract each value from the bundle for usage
                        ActivityHandler.startConversationListActivity(activity, mBundle);
                        activity.finish();
                    }

                    @Override
                    public void onRequestFailure(Exception exception) {
                        UiUtils.hideProgressBar(mProgressDialog);
                        AlertsHandler.showNetworkError(activity, exception);

                    }
                };
                mProgressDialog = UiUtils.showProgressBar(activity, mProgressDialog);
                ReportAbuseButtonOnClickListner.sendReportAbuseReasonToServer(activity, "Not interested anymore", "", message_one_one_conversion_url, okHttpResponseHandler);
            }

            @Override
            public void onNegativeButtonSelected() {

            }
        };
        String gender;
        if (Utility.isMale(activity)) {
            gender = "her";
        } else {
            gender = "him";
        }
        //i18n
        AlertsHandler.showConfirmDialog(activity, "Unmatch " + matchMessageMetaData.getFName() + "?\n\nThis will clear all chat history and remove " + gender + " permanently from your matches.", R.string.yes, R.string.nope, confirmDialogInterface, true);
    }

    @Override
    public void onClearChat() {

        ConfirmDialogInterface confirmDialogInterface = new ConfirmDialogInterface() {

            @Override
            public void onPositiveButtonSelected() {

                final HashMap<String ,String > result=MessageDBHandler.fetchLastMessageId(activity, matchMessageMetaData.getMatchId());

                CustomOkHttpResponseHandler okHttpResponseHandler = new CustomOkHttpResponseHandler(activity) {

                    @Override
                    public void onRequestSuccess(JSONObject responseJSON) {
                        TrulyMadlyTrackEvent.trackEvent(activity,
                                TrulyMadlyEvent.TrulyMadlyActivities.message_full_conversation,
                                TrulyMadlyEvent.TrulyMadlyEventTypes.clear_chat,
                                0, Utility.getMyId(activity), null,
                                true);
                        UiUtils.hideProgressBar(mProgressDialog);
                        MessageDBHandler.updateClearChatTstamp(activity, matchMessageMetaData.getMatchId(), result.get("tstamp"));
                        MessageDBHandler.deleteAllMessages(activity, matchMessageMetaData.getMatchId());

                        //Clearing images of IMAGE messages
                        FilesHandler.deleteFileOrDirectoryFromImageAssets(matchMessageMetaData.getMatchId());

                        Utility.fireBusEvent(activity, true,
                                new TriggerClearChatEvent());

                    }

                    @Override
                    public void onRequestFailure(Exception exception) {
                        UiUtils.hideProgressBar(mProgressDialog);
                        AlertsHandler.showNetworkError(activity, exception);

                    }
                };
                HashMap<String, String> params = new HashMap<>();
                String last_message_id= result.get("message_id");
                params.put("last_msg_id",last_message_id );
                params.put("type", "clear_chat");

                if(Utility.isSet(last_message_id)) {
                    mProgressDialog = UiUtils.showProgressBar(activity, mProgressDialog);
                    OkHttpHandler.httpPost(activity, message_one_one_conversion_url, params,
                            okHttpResponseHandler);
                }
            }

            @Override
            public void onNegativeButtonSelected() {

            }
        };

        AlertsHandler.showConfirmDialog(activity, R.string.clear_chat_warning, R.string.clear, R.string.cancel, confirmDialogInterface, true);

    }


}
