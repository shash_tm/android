package com.trulymadly.android.chat;

import android.os.Parcel;
import android.os.Parcelable;


public class Sticker implements Parcelable{
	
	public static final Parcelable.Creator<Sticker> CREATOR = new Parcelable.Creator<Sticker>()
	{
		public Sticker createFromParcel(Parcel paramAnonymousParcel)
		{
			return new Sticker(paramAnonymousParcel);
		}

		public Sticker[] newArray(int paramAnonymousInt)
		{
			return new Sticker[paramAnonymousInt];
		}
	};
	
	private int id;
	private String type;
	private String url;

	public Sticker()
	{
	}

	private Sticker(Parcel paramParcel)
	{
		this.id = paramParcel.readInt();
		this.url = paramParcel.readString();
		this.type = paramParcel.readString();
	}

	public int describeContents()
	{
		return 0;
	}

	public int getId()
	{
		return this.id;
	}

	public String getType()
	{
		return this.type;
	}

	public String getUrl()
	{
		return this.url;
	}

	public void setId(int paramInt)
	{
		this.id = paramInt;
	}

	public void setType(String paramString)
	{
		this.type = paramString;
	}

	public void setUrl(String paramString)
	{
		this.url = paramString;
	}

	public void writeToParcel(Parcel paramParcel, int paramInt)
	{
		paramParcel.writeInt(this.id);
		paramParcel.writeString(this.url);
		paramParcel.writeString(this.type);
	}
}
