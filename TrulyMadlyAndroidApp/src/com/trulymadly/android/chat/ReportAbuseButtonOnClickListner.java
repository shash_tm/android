package com.trulymadly.android.chat;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.trulymadly.android.app.R;
import com.trulymadly.android.app.listener.AlertDialogInterface;
import com.trulymadly.android.app.listener.CustomOkHttpResponseHandler;
import com.trulymadly.android.app.modal.BlockReason;
import com.trulymadly.android.app.modal.SparkModal;
import com.trulymadly.android.app.sqlite.MessageDBHandler;
import com.trulymadly.android.app.sqlite.SparksDbHandler;
import com.trulymadly.android.app.utility.ActivityHandler;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.OkHttpHandler;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ReportAbuseButtonOnClickListner implements View.OnClickListener {

	private final RadioGroup radioGroup;
	private final AlertDialog alertDialog;
	private final View layout;
	private final Activity activity;
	private final String message_one_one_conversion_url;
	private final String match_id;
	private ProgressDialog mProgressDialog;

	public ReportAbuseButtonOnClickListner(RadioGroup radioGroup,
			AlertDialog alertDialog, View layout, Activity activity,
			String message_one_one_conversion_url, String match_id) {
		this.radioGroup = radioGroup;
		this.alertDialog = alertDialog;
		this.layout = layout;
		this.activity = activity;
		this.message_one_one_conversion_url = message_one_one_conversion_url;
		this.match_id = match_id;
	}

	// http://stackoverflow.com/questions/14847841/checking-radio-group-either-empty-null
	// http://stackoverflow.com/questions/8323778/how-to-set-on-click-listener-on-the-radio-button-in-android
	// http://stackoverflow.com/questions/2620444/how-to-prevent-a-dialog-from-closing-when-a-button-is-clicked/15619098#15619098

	public static void sendReportAbuseReasonToServer(Context aContext, String reason, String description, String message_one_one_conversion_url, CustomOkHttpResponseHandler okHttpResponseHandler) {
		// send data to the server
		Map<String, String> params = new HashMap<>();
		params.put("type", "report_abuse");
		params.put("reason", reason);
		params.put("description", description);
		if (Utility.isSet(message_one_one_conversion_url) && Utility.isNetworkAvailable(aContext)) {
			OkHttpHandler.httpPost(aContext, message_one_one_conversion_url, params, okHttpResponseHandler);
		} else {
			okHttpResponseHandler.onRequestFailure(null);
		}

	}

	@Override
	public void onClick(View v) {
		int clickedbutton = radioGroup.getCheckedRadioButtonId();
		if (clickedbutton != -1) {
			RadioButton button = (RadioButton) layout
					.findViewById(clickedbutton);
			EditText other_reason = (EditText) layout
					.findViewById(R.id.other_reason);
			String description = other_reason.getText().toString().trim();
			BlockReason selectedReason = (BlockReason) button.getTag();
			String reason = button.getText().toString();
			if (selectedReason.isEditableNeeded()
					&& !Utility.isSet(description)) {
				//Utility.hideKeyBoard(activity);
				//Utility.showMessage(activity, R.string.enter_reason_for_reporting, false);
				AlertsHandler.showMessageInDialog(alertDialog.getWindow().getDecorView(), R.string.enter_reason_for_reporting, false);
			} else {
				CustomOkHttpResponseHandler okHttpResponseHandler = new CustomOkHttpResponseHandler(
						activity) {

					@Override
					public void onRequestSuccess(JSONObject response) {
						MessageDBHandler.setUserBlocked(activity, match_id);
						mProgressDialog = UiUtils.hideProgressBar(mProgressDialog);
						AlertsHandler.showAlertDialog(activity, R.string.thank_you_for_reporting_message, new AlertDialogInterface() {

                            @Override
                            public void onButtonSelected() {
                                Bundle mBundle = new Bundle();
                                mBundle.putString("blocked_text", activity.getResources().getString(R.string.block_successfull));

								SparkModal sparkModal = SparksDbHandler.getSpark(activity, Utility.getMyId(activity), match_id);
								if(sparkModal != null){
									mBundle.putString("remove_spark", sparkModal.getmMatchId());
									mBundle.putBoolean("call_api", false);
								}
                                // Extract each value from the bundle for usage
                                ActivityHandler.startConversationListActivity(activity, mBundle);
                                activity.finish();
                            }
                        });
					}

					@Override
					public void onRequestFailure(Exception exception) {
						mProgressDialog = UiUtils.hideProgressBar(mProgressDialog);
						AlertsHandler.showNetworkError(activity,
								exception);

					}
				};
				mProgressDialog = UiUtils.showProgressBar(activity, mProgressDialog, R.string.processing_your_request);
				sendReportAbuseReasonToServer(activity, reason, description, message_one_one_conversion_url, okHttpResponseHandler);
				alertDialog.dismiss();
			}

		} else {
			AlertsHandler.showMessage(activity, R.string.select_an_option, false);
		}
	}

}