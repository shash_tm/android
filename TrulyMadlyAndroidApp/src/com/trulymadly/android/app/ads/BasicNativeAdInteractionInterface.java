package com.trulymadly.android.app.ads;

/**
 * Created by avin on 13/07/16.
 */
public interface BasicNativeAdInteractionInterface {
    void onClicked(AdsType adsType, Object object);
    void reportImpression(AdsType adsType, Object object);

    void adLoaded(AdsType adsType, boolean success);
}
