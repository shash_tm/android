package com.trulymadly.android.app.ads;

import seventynine.sdk.SeventynineAdSDK;

/**
 * Created by avin on 28/12/16.
 */

public abstract class TMSeventyNineCallbackListener implements SeventynineAdSDK.SeventynineCallbackListener{

    private AdsType mAdsType;

    public TMSeventyNineCallbackListener(AdsType mAdsType) {
        this.mAdsType = mAdsType;
    }

    public AdsType getmAdsType() {
        return mAdsType;
    }
}
