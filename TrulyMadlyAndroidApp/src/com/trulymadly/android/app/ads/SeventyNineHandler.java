package com.trulymadly.android.app.ads;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.ViewGroup;

import com.seventynine.sdkadapter.SeventynineSdkAdapter;
import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.utility.TmLogger;

import java.util.HashMap;

import seventynine.sdk.DisplayAds;
import seventynine.sdk.SeventynineAdSDK;

/**
 * Created by avin on 11/07/16.
 */
public class SeventyNineHandler extends AdsHandlerNew{
    public static final String TAG = "SeventyNineHandler";
    private SeventynineAdSDK mSeventynineAdSDK;
    private SeventynineAdSDK.SeventynineCallbackListener mSeventynineCallbackListener;

    public SeventyNineHandler(Context mContext, AdConstraintManager mAdConstraintManager, AdsType mAdsType, String mTrackingCategory) {
        super(mContext, mAdConstraintManager, mAdsType, mTrackingCategory);
    }

    public SeventyNineHandler(Context mContext, AdConstraintManager mAdConstraintManager, AdsType mAdsType,
                              AdsListener adsListener, String mTrackingCategory) {
        super(mContext, mAdConstraintManager, mAdsType, adsListener, mTrackingCategory);
    }

    public static String getAdString(AdsType adsType) {
        switch (adsType) {
            case MATCHES_END_INTERSTITIAL:
                return "mid";

            case MATCHES_MID_PROFILE_VIDEO:
                return "nativeAd";
        }

        return null;
    }

    @Override
    public void initialize() {
        if(!AdsHelper.is79AdsEnabled(getmContext())){
            return;
        }
        if(mSeventynineAdSDK == null) {
            mSeventynineAdSDK = new SeventynineAdSDK();
            mSeventynineCallbackListener = new TMSeventyNineCallbackListener(getmAdsType()) {
                @Override
                public void onAdStarted() {
                    HashMap<String, String> eventInfo = new HashMap<>();
                    eventInfo.put("from_79", String.valueOf(isFrom79()));
                    eventInfo.put("method", "onAdStarted");
                    AdsHelper.addParams(eventInfo);
                    TmLogger.log(Log.DEBUG, TAG, "onAdStarted");

                    //Commenting out to fix the static listenr issue
//                    if (getmAdsType() == AdsType.MATCHES_END_INTERSTITIAL && !isFrom79()) {
//                        TrulyMadlyTrackEvent.trackEvent(getmContext(), getmTrackingCategory(),
//                                getmAdsType().getEventTypeForTracking(), 0,
//                                TrulyMadlyEvent.TrulyMadlyEventStatus.ad_request_success, eventInfo, true);
//                        Log.d(TAG, getmTrackingCategory() + " : " + getmAdsType().getEventTypeForTracking() + " : "
//                                + "ad_request_success :: method-onAdStarted,from79-false");
//                        if (getmAdsListener() != null) {
//                            getmAdsListener().onAdLoadSuccess(getmAdsType(), null);
//                        }
//                    }
                    TrulyMadlyTrackEvent.trackEvent(getmContext(), getmTrackingCategory(),
                            getmAdsType().getEventTypeForTracking(), 0,
                            TrulyMadlyEvent.TrulyMadlyEventStatus.ad_shown, eventInfo, true);
                    Log.d(TAG, getmTrackingCategory() + " : " + getmAdsType().getEventTypeForTracking() + " : "
                            + "ad_shown :: method-onAdStarted,from79-" + isFrom79());
                    adShown(getmAdsType());
                    if (getmAdsListener() != null) {
                        getmAdsListener().onAdStarted(getmAdsType());
                    }
                }

                @Override
                public void onVideoView25() {
                    TmLogger.log(Log.DEBUG, TAG, "onVideoView25");
                }

                @Override
                public void onVideoView50() {
                    TmLogger.log(Log.DEBUG, TAG, "onVideoView50");
                }

                @Override
                public void onVideoView75() {
                    TmLogger.log(Log.DEBUG, TAG, "onVideoView75");
                }

                @Override
                public void onAdFinished() {
                    TmLogger.log(Log.DEBUG, TAG, "onAdFinished");
                    if (getmAdsListener() != null) {
                        getmAdsListener().onAdFinished(getmAdsType());
                    }
                }

                @Override
                public void onClose() {
                    TmLogger.log(Log.DEBUG, TAG, "onClose");
                    if (getmAdsListener() != null) {
                        getmAdsListener().onAdStopped(getmAdsType());
                    }
                }

                @Override
                public void onAdClick() {
                    HashMap<String, String> eventInfo = new HashMap<>();
                    eventInfo.put("from_79", String.valueOf(isFrom79()));
                    AdsHelper.addParams(eventInfo);
                    TrulyMadlyTrackEvent.trackEvent(getmContext(), getmTrackingCategory(),
                            getmAdsType().getEventTypeForTracking(), 0,
                            TrulyMadlyEvent.TrulyMadlyEventStatus.ad_clicked, eventInfo, true);
                    TmLogger.log(Log.DEBUG, TAG, "onAdClick");
                    if (getmAdsListener() != null) {
                        getmAdsListener().onAdClicked(getmAdsType());
                    }
                }

                @Override
                public void onSkipEnable() {
                    TmLogger.log(Log.DEBUG, TAG, "onSkipEnable");

                }

                @Override
                public void onErrorReceived() {
                    HashMap<String, String> eventInfo = new HashMap<>();
                    eventInfo.put("from_79", String.valueOf(isFrom79()));
                    eventInfo.put("method", "onErrorReceived");
                    AdsHelper.addParams(eventInfo);
                    if (getmAdsType() == AdsType.MATCHES_END_INTERSTITIAL && !isFrom79()) {
                        TrulyMadlyTrackEvent.trackEvent(getmContext(), getmTrackingCategory(),
                                getmAdsType().getEventTypeForTracking(), 0,
                                TrulyMadlyEvent.TrulyMadlyEventStatus.ad_request_failure, eventInfo, true);
                        if (getmAdsListener() != null) {
                            getmAdsListener().onAdLoadfailed(getmAdsType());
                        }
                        return;
                    }
                    TrulyMadlyTrackEvent.trackEvent(getmContext(), getmTrackingCategory(),
                            getmAdsType().getEventTypeForTracking(), 0,
                            TrulyMadlyEvent.TrulyMadlyEventStatus.ad_error, eventInfo, true);
                    TmLogger.log(Log.DEBUG, TAG, "onErrorReceived");
                    if (getmAdsListener() != null) {
                        getmAdsListener().onErrorReceived(getmAdsType());
                    }
                }

                @Override
                public void onNoAdToShow() {

                }
            };
//            mSeventynineCallbackListener = new SeventynineAdSDK.SeventynineCallbackListener() {
//                @Override
//                public void onAdStarted() {
//                    HashMap<String, String> eventInfo = new HashMap<>();
//                    eventInfo.put("from_79", String.valueOf(isFrom79()));
//                    eventInfo.put("method", "onAdStarted");
//                    AdsHelper.addParams(eventInfo);
//                    TmLogger.log(Log.DEBUG, TAG, "onAdStarted");
//
//                    if (getmAdsType() == AdsType.MATCHES_END_INTERSTITIAL && !isFrom79()) {
//                        TrulyMadlyTrackEvent.trackEvent(getmContext(), getmTrackingCategory(),
//                                getmAdsType().getEventTypeForTracking(), 0,
//                                TrulyMadlyEvent.TrulyMadlyEventStatus.ad_request_success, eventInfo, true);
//                        Log.d(TAG, getmTrackingCategory() + " : " + getmAdsType().getEventTypeForTracking() + " : "
//                                + "ad_request_success :: method-onAdStarted,from79-false");
//                        if (getmAdsListener() != null) {
//                            getmAdsListener().onAdLoadSuccess(getmAdsType(), null);
//                        }
//                    }
//                    TrulyMadlyTrackEvent.trackEvent(getmContext(), getmTrackingCategory(),
//                            getmAdsType().getEventTypeForTracking(), 0,
//                            TrulyMadlyEvent.TrulyMadlyEventStatus.ad_shown, eventInfo, true);
//                    Log.d(TAG, getmTrackingCategory() + " : " + getmAdsType().getEventTypeForTracking() + " : "
//                            + "ad_shown :: method-onAdStarted,from79-" + isFrom79());
//                    adShown();
//                    if (getmAdsListener() != null) {
//                        getmAdsListener().onAdStarted(getmAdsType());
//                    }
//                }
//
//                @Override
//                public void onVideoView25() {
//                    TmLogger.log(Log.DEBUG, TAG, "onVideoView25");
//                }
//
//                @Override
//                public void onVideoView50() {
//                    TmLogger.log(Log.DEBUG, TAG, "onVideoView50");
//                }
//
//                @Override
//                public void onVideoView75() {
//                    TmLogger.log(Log.DEBUG, TAG, "onVideoView75");
//                }
//
//                @Override
//                public void onAdFinished() {
//                    TmLogger.log(Log.DEBUG, TAG, "onAdFinished");
//                    if (getmAdsListener() != null) {
//                        getmAdsListener().onAdFinished(getmAdsType());
//                    }
//                }
//
//                @Override
//                public void onClose() {
//                    TmLogger.log(Log.DEBUG, TAG, "onClose");
//                    if (getmAdsListener() != null) {
//                        getmAdsListener().onAdStopped(getmAdsType());
//                    }
//                }
//
//                @Override
//                public void onAdClick() {
//                    HashMap<String, String> eventInfo = new HashMap<>();
//                    eventInfo.put("from_79", String.valueOf(isFrom79()));
//                    AdsHelper.addParams(eventInfo);
//                    TrulyMadlyTrackEvent.trackEvent(getmContext(), getmTrackingCategory(),
//                            getmAdsType().getEventTypeForTracking(), 0,
//                            TrulyMadlyEvent.TrulyMadlyEventStatus.ad_clicked, eventInfo, true);
//                    TmLogger.log(Log.DEBUG, TAG, "onAdClick");
//                    if (getmAdsListener() != null) {
//                        getmAdsListener().onAdClicked(getmAdsType());
//                    }
//                }
//
//                @Override
//                public void onSkipEnable() {
//                    TmLogger.log(Log.DEBUG, TAG, "onSkipEnable");
//
//                }
//
//                @Override
//                public void onErrorReceived() {
//                    HashMap<String, String> eventInfo = new HashMap<>();
//                    eventInfo.put("from_79", String.valueOf(isFrom79()));
//                    eventInfo.put("method", "onErrorReceived");
//                    AdsHelper.addParams(eventInfo);
//                    if (getmAdsType() == AdsType.MATCHES_END_INTERSTITIAL && !isFrom79()) {
//                        TrulyMadlyTrackEvent.trackEvent(getmContext(), getmTrackingCategory(),
//                                getmAdsType().getEventTypeForTracking(), 0,
//                                TrulyMadlyEvent.TrulyMadlyEventStatus.ad_request_failure, eventInfo, true);
//                        if (getmAdsListener() != null) {
//                            getmAdsListener().onAdLoadfailed(getmAdsType());
//                        }
//                        return;
//                    }
//                    TrulyMadlyTrackEvent.trackEvent(getmContext(), getmTrackingCategory(),
//                            getmAdsType().getEventTypeForTracking(), 0,
//                            TrulyMadlyEvent.TrulyMadlyEventStatus.ad_error, eventInfo, true);
//                    TmLogger.log(Log.DEBUG, TAG, "onErrorReceived");
//                    if (getmAdsListener() != null) {
//                        getmAdsListener().onErrorReceived(getmAdsType());
//                    }
//                }
//
//                @Override
//                public void onNoAdToShow() {
//
//                }
//            };
            mSeventynineAdSDK.setSeventynineTrackingListener(new SeventynineAdSDK.SeventynineTrackingCallbackListener() {
                @Override
                public void onImpressionTrack() {

                }

                @Override
                public void onOpportunityTrack() {

                }

                @Override
                public void onCachingStated() {

                }

                @Override
                public void onCachingComplete() {

                }

                @Override
                public void onAlreadyCached() {

                }

                @Override
                public void onAdRequeted() {

                }

                @Override
                public void onInitialiseCalled() {

                }

                @Override
                public void onNoCampaignAvailable() {

                }
            });
        }
    }

    @Override
    public void loadAd(Object object, boolean fromMediation) {
        if(object == null || !(object instanceof SeventyNineAdUnit)){
            throw new IllegalArgumentException("SeventyNineHandler#loadAd parameter object should be an instance of SeventyNineAdUnit");
        }

        SeventyNineAdUnit seventyNineAdUnit = (SeventyNineAdUnit) object;
        String correctPlacementId = getmAdsType().getPlacementId(AdsSource.SEVENTY_NINE);

//        if (mSeventynineAdSDK.isAdReady(correctPlacementId, getmContext(), "", getAdString(getmAdsType()))) {
        if (!fromMediation) {
            setFrom79(true);
            mSeventynineAdSDK.setCallbackListener(mSeventynineCallbackListener);
            if (getmAdsListener() != null) {
                HashMap<String, String> eventInfo = new HashMap<>();
                eventInfo.put("from_79", String.valueOf(isFrom79()));
                eventInfo.put("method", "loadAd");
                AdsHelper.addParams(eventInfo);
                TrulyMadlyTrackEvent.trackEvent(getmContext(), getmTrackingCategory(),
                        getmAdsType().getEventTypeForTracking(), 0,
                        TrulyMadlyEvent.TrulyMadlyEventStatus.ad_request_success, eventInfo, true);
                Log.d(TAG, getmTrackingCategory() + " : " + getmAdsType().getEventTypeForTracking() + " : "
                        + "ad_request_success :: method-loadAd,from79-true");
                getmAdsListener().onAdLoadSuccess(getmAdsType(), null);
            }

            switch (getmAdsType()) {
                case MATCHES_MID_PROFILE_VIDEO:
                    (new DisplayAds()).customView(seventyNineAdUnit.getActivity(), seventyNineAdUnit.getIntoView(),
                            getAdString(getmAdsType()), correctPlacementId);
                    break;

                case MATCHES_END_INTERSTITIAL:
                    Intent intent = new Intent();
                    intent.setClass(seventyNineAdUnit.getActivity(), DisplayAds.class);
                    intent.putExtra("zoneId" , correctPlacementId);
                    intent.putExtra("adLocation" , getAdString(getmAdsType()));
                    seventyNineAdUnit.getActivity().startActivity(intent);
                    break;
            }
        } else {
            HashMap<String, String> eventInfo = new HashMap<>();
            eventInfo.put("from_79", String.valueOf(isFrom79()));
            eventInfo.put("method", "loadAd");
            AdsHelper.addParams(eventInfo);
            TrulyMadlyTrackEvent.trackEvent(getmContext(), getmTrackingCategory(),
                    getmAdsType().getEventTypeForTracking(), 0,
                    TrulyMadlyEvent.TrulyMadlyEventStatus.ad_request_failure, eventInfo, true);
            Log.d(TAG, getmTrackingCategory() + " : " + getmAdsType().getEventTypeForTracking() + " : "
                    + "ad_request_failure :: method-loadAd,from79-true");
            if (is79MediationEnabled() && getmAdsType() == AdsType.MATCHES_END_INTERSTITIAL) {
                setFrom79(false);
                mSeventynineAdSDK.setCallbackListener(mSeventynineCallbackListener);
                SeventynineSdkAdapter.getInstance().callAdapter(getmContext(), null,
                        correctPlacementId);
                HashMap<String, String> newEventInfo = new HashMap<>();
                newEventInfo.put("from_79", String.valueOf(isFrom79()));
                newEventInfo.put("method", "loadAd");
                AdsHelper.addParams(eventInfo);
                TrulyMadlyTrackEvent.trackEvent(getmContext(), getmTrackingCategory(),
                        getmAdsType().getEventTypeForTracking(), 0,
                        TrulyMadlyEvent.TrulyMadlyEventStatus.ad_request_made, newEventInfo, true);
                Log.d(TAG, getmTrackingCategory() + " : " + getmAdsType().getEventTypeForTracking() + " : "
                        + "ad_request_made :: method-loadAd,from79-false");
            } else if (getmAdsListener() != null) {
                getmAdsListener().onAdLoadfailed(getmAdsType());
            }
        }
    }

    @Override
    public boolean shouldShowAd(boolean callFailed) {
        boolean isAdReady = false;

        initialize();

        Log.d(TAG, getmTrackingCategory() + " : " + getmAdsType().getEventTypeForTracking() + " : "
                + "ad_request_made :: method-shouldShowAd");
        HashMap<String, String> eventInfo = new HashMap<>();
        eventInfo.put("from_79", String.valueOf(true));
        eventInfo.put("method", "shouldShowAd");
        AdsHelper.addParams(eventInfo);
        TrulyMadlyTrackEvent.trackEvent(getmContext(), getmTrackingCategory(),
                getmAdsType().getEventTypeForTracking(), 0,
                TrulyMadlyEvent.TrulyMadlyEventStatus.ad_request_made, eventInfo, true);

        String correctPlacementId = getmAdsType().getPlacementId(AdsSource.SEVENTY_NINE);
        isAdReady = mSeventynineAdSDK.isAdReady(correctPlacementId, getmContext(), "",
                getAdString(getmAdsType()), null);
        if (!isAdReady && callFailed && getmAdsListener() != null) {
            if (getmAdsType() == AdsType.MATCHES_MID_PROFILE_VIDEO) {
                Log.d(TAG, getmTrackingCategory() + " : " + getmAdsType().getEventTypeForTracking() + " : "
                        + "ad_request_failure :: method-shouldShowAd");
                TrulyMadlyTrackEvent.trackEvent(getmContext(), getmTrackingCategory(),
                        getmAdsType().getEventTypeForTracking(), 0,
                        TrulyMadlyEvent.TrulyMadlyEventStatus.ad_request_failure, eventInfo, true);
                getmAdsListener().onAdLoadfailed(getmAdsType());
            }
        }
        return isAdReady;
    }

    @Override
    public boolean areConstraintsSatisfied() {
        return super.areConstraintsSatisfied();
    }


    ///////////////////////////////////
    //Utility functions
    ///////////////////////////////////

    public static class SeventyNineAdUnit{
        private Activity activity;
        private ViewGroup intoView;

        public SeventyNineAdUnit(Activity activity, ViewGroup intoView) {
            this.activity = activity;
            this.intoView = intoView;
        }

        public Activity getActivity() {
            return activity;
        }

        public void setActivity(Activity activity) {
            this.activity = activity;
        }


        public ViewGroup getIntoView() {
            return intoView;
        }

        public void setIntoView(ViewGroup intoView) {
            this.intoView = intoView;
        }
    }
}
