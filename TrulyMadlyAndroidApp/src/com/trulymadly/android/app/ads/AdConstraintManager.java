package com.trulymadly.android.app.ads;

import android.content.Context;

/**
 * Created by avin on 11/07/16.
 */
public abstract class AdConstraintManager {
    public boolean isAdAllowed(Context context){
        return AdsHelper.is79AdsEnabled(context) && validateConstraints(context);
    }

    public abstract boolean validateConstraints(Context context);
}
