package com.trulymadly.android.app.ads;

import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.app.json.Constants;

/**
 * Created by avin on 11/07/16.
 */
public enum AdsType {
    MATCHES_END_INTERSTITIAL, CONV_LIST_NATIVE, MATCHES_MID_PROFILE_VIDEO;

    public String getPlacementId(AdsSource adsSource) {
        switch (this) {
            case MATCHES_END_INTERSTITIAL:
                switch (adsSource) {
                    case SEVENTY_NINE:
                        return Constants.ADS_79_PLACEMENT_ID_MATCHES_END_INTERSTITIAL_AD;
                }
                break;

            case MATCHES_MID_PROFILE_VIDEO:
                switch (adsSource) {
                    case SEVENTY_NINE:
                        return Constants.ADS_79_PLACEMENT_ID_MATCHES_VIDEO_AD;
                }
                break;

            case CONV_LIST_NATIVE:
                switch (adsSource) {
                    case SEVENTY_NINE:
                        return Constants.ADS_79_PLACEMENT_ID_CONV_NATIVE_AD;
                }
                break;
        }

        return null;
    }

    public AdConstraintManager getConstraintManager(){
        switch (this){
            case MATCHES_END_INTERSTITIAL:
                return new MatchesInterstitialConstraintManager();

            case MATCHES_MID_PROFILE_VIDEO:
                return new MatchesProfileVideoAdConstraintManager();

            case CONV_LIST_NATIVE:
                return new ConvNativeAdConstraintManager();
        }

        return null;
    }

    public String getEventTypeForTracking(){
        switch (this){
            case MATCHES_END_INTERSTITIAL:
                return TrulyMadlyEvent.TrulyMadlyEventTypes.ad_interstitial;

            case MATCHES_MID_PROFILE_VIDEO:
                return TrulyMadlyEvent.TrulyMadlyEventTypes.profile_ads;

            case CONV_LIST_NATIVE:
                return TrulyMadlyEvent.TrulyMadlyEventTypes.ad_native;
        }

        return null;
    }

}
