package com.trulymadly.android.app.ads;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.util.Log;
import android.view.ViewGroup;

import com.crashlytics.android.Crashlytics;
import com.google.common.base.Preconditions;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.bus.SimpleAction;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.ConstantsRF;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.sqlite.RFHandler;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.TimeUtils;
import com.trulymadly.android.app.utility.Utility;

import java.util.HashMap;

import seventynine.sdk.SeventynineAdSDK;
import seventynine.sdk.SeventynineConstants;

import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities.matches;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes.constraints_check;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes.sdk_init;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes.sdk_init_req;

/**
 * Created by avin on 26/04/16.
 */

public class AdsHelper implements BasicNativeAdInteractionInterface {

    private static boolean is79Initialized = false;
    private static boolean is79InitRequested = false;
    private final AdsListener mAdsListener;
    private final Context mContext;
    private AdsSource mAdsSource;
    private HashMap<AdsType, AdsHandlerNew> mTypeToHandlerMap;

    public AdsHelper(Context mContext, AdsListener mAdsListener, AdsSource adsSource) {
        this.mAdsListener = mAdsListener;
        this.mContext = mContext;
        this.mAdsSource = adsSource;
        mTypeToHandlerMap = new HashMap<>();
    }

    public static boolean is79AdsEnabled(Context context){
        return RFHandler.getBool(context, ConstantsRF.RIGID_FILED_IS_79_ADS_ENABLED);
    }

    public static void resetAdsData(Context context){
        String userId = Utility.getMyId(context);
        RFHandler.insert(context, userId,
                ConstantsRF.RIGID_FILED_IS_79_ADS_ENABLED, false);
        resetProfileAdsData(context);
        resetInterstitialNativeAdsData(context);
    }

    public static void resetProfileAdsData(Context context){
        String userId = Utility.getMyId(context);

        RFHandler.insert(context, userId,
                ConstantsRF.RIGID_FILED_IS_PROFILE_ADS_ENABLED, false);
        RFHandler.insert(context, userId,
                ConstantsRF.RIGID_FILED_FINAL_PROFILE_AD_POSITION, -1);
        RFHandler.insert(context, userId,
                ConstantsRF.RIGID_FILED_IS_PROFILE_ADS_REPEATABLE, false);
        RFHandler.insert(context, userId,
                ConstantsRF.RIGID_FILED_IS_PROFILE_ADS_TIME_INTERVAL, 0L);
    }

    public static void resetInterstitialNativeAdsData(Context context) {
        String userId = Utility.getMyId(context);

        //Resetting all counters
        RFHandler.insert(context, userId,
                ConstantsRF.RIGID_FIELD_INTERSTITIAL_MATCHES_COUNTER, 0);
        RFHandler.insert(context, userId,
                ConstantsRF.RIGID_FIELD_INTERSTITIAL_COUNTER, 0);

        //Resetting timestamp fields
        RFHandler.insert(context, userId,
                ConstantsRF.RIGID_FIELD_LAST_INTERSTITIAL_AD_TIMESTAMP, 0L);
        RFHandler.insert(context, userId,
                ConstantsRF.RIGID_FIELD_LAST_NATIVE_AD_TIMESTAMP, 0L);

        //Resetting timeinterval fields
        RFHandler.insert(context, userId,
                ConstantsRF.RIGID_FIELD_INTERSTITIAL_AD_TIMEINTERVAL, -1);
        RFHandler.insert(context, userId,
                ConstantsRF.RIGID_FIELD_NATIVE_AD_TIMEINTERVAL, -1);
    }

    public static void initialize79SDK(Context context){
        final Context appContext = context.getApplicationContext();
        if(!is79InitRequested) {
            is79InitRequested = true;
            SeventynineConstants.strPublisherId = Constants.ADS_79_PUBLISHER_ID;
            SeventynineConstants.strIsStartZoneActive = "0";
            SeventynineConstants.strIsEndZoneActive = "0";
            SeventynineConstants.appContext = appContext;
            SeventynineConstants.strFirstActivityPath = "";
            SeventynineConstants.strSkipButtonByDeveloper = "1000000";

            String gender = SPHandler.getString(appContext, ConstantsSP.SHARED_KEYS_USER_GENDER);
            if (Utility.isSet(gender)) {
                SeventynineAdSDK.setGender(gender);
            }

            String age = SPHandler.getString(appContext, ConstantsSP.SHARED_KEYS_USER_AGE);
            if (Utility.isSet(age)) {
                SeventynineAdSDK.setAge(age);
            }

            SeventynineAdSDK mSeventynineAdSDK = new SeventynineAdSDK();
            mSeventynineAdSDK.init(appContext);
            mSeventynineAdSDK.setInitCallbackListener(new SeventynineAdSDK.SeventynineInitCallbackListener() {
                @Override
                public void InitSuccess() {
                    is79Initialized = true;
                    Log.d(SeventyNineHandler.TAG, "initialize79SDK : Initialized");
                    HashMap<String, String> params = new HashMap<>();
                    addParams(params);
                    params.put("ts", TimeUtils.getFormattedTime());
                    TrulyMadlyTrackEvent.trackEvent(appContext, matches, sdk_init, 0, null, params, true);
                    Utility.fireBusEvent(appContext, true, new SimpleAction(SimpleAction.ACTION_79_SDK_INIT));
                }
            });
            Log.d(SeventyNineHandler.TAG, "initialize79SDK : Requested");
            HashMap<String, String> params = new HashMap<>();
            addParams(params);
            params.put("ts", TimeUtils.getFormattedTime());
            TrulyMadlyTrackEvent.trackEvent(appContext, matches, sdk_init_req, 0, null, params, true);
        }
    }

    public static boolean is79Initialized() {
        return is79Initialized;
    }

    public static void addParams(HashMap<String, String> eventInfo) {
        eventInfo.put("tm_init", String.valueOf(is79Initialized()));
        eventInfo.put("79_init", String.valueOf(SeventynineConstants.isSdkInitilize));
    }

    public String[] getHashTags() {
        String singleStringHashTags = SeventynineAdSDK.getHashTag();
        if (Utility.isSet(singleStringHashTags)) {
            return singleStringHashTags.split(";");
        }

        return null;
    }

    private void initialize(AdsType adsType, String trackingCategory) {
        if (mTypeToHandlerMap.containsKey(adsType)
                && mTypeToHandlerMap.get(adsType) != null) {
            return;
        }

        try {
            switch (mAdsSource) {
                case SEVENTY_NINE:
                    switch (adsType) {
                        case MATCHES_END_INTERSTITIAL:
                        case MATCHES_MID_PROFILE_VIDEO:
                            AdsHandlerNew matchesHandlerNew = new SeventyNineHandler(mContext, adsType.getConstraintManager(),
                                    adsType, mAdsListener, trackingCategory);
                            matchesHandlerNew.initialize();
                            mTypeToHandlerMap.put(adsType, matchesHandlerNew);
                            break;

                        case CONV_LIST_NATIVE:
                            AdsHandlerNew nativeAdsHandlerNew = new SeventyNineNativeHandler(mContext, adsType.getConstraintManager(),
                                    adsType, mAdsListener, trackingCategory);
                            nativeAdsHandlerNew.initialize();
                            mTypeToHandlerMap.put(adsType, nativeAdsHandlerNew);
                            break;
                    }
                    break;
            }
        } catch (VerifyError verifyError) {
            //Hack to fix :
            //https://fabric.io/trulymadly/android/apps/com.trulymadly.android.app.t1/issues?q=java.lang.VerifyError
            Crashlytics.logException(verifyError);
            mTypeToHandlerMap.put(adsType, null);
        }
    }

    public void loadAd(Activity activity, AdsType adsType, ViewGroup intoView, boolean fromMediation) {
        AdsHandlerNew mAdsHandlerNew = mTypeToHandlerMap.get(adsType);
        //Fixing issue: ANDROID-2503
        try {
            if (mAdsHandlerNew != null) {
                Preconditions.checkNotNull(mAdsHandlerNew).loadAd(
                        new SeventyNineHandler.SeventyNineAdUnit(activity, intoView), fromMediation);
            }
        } catch (ActivityNotFoundException ignored) {

        } catch (VerifyError verifyError) {
            //Hack to fix :
            //https://fabric.io/trulymadly/android/apps/com.trulymadly.android.app.t1/issues?q=java.lang.VerifyError
            Crashlytics.logException(verifyError);
            mAdsHandlerNew.getmAdsListener().onAdLoadfailed(mAdsHandlerNew.getmAdsType());
        }
    }

    public boolean areConstraintsSatisfied(AdsType adsType, String trackingCategory) {
        boolean areConstraintsSatisfied = false;

        if (is79Initialized()) {
            initialize(adsType, trackingCategory);
            AdsHandlerNew mAdsHandlerNew = mTypeToHandlerMap.get(adsType);
            if (mAdsHandlerNew != null) {
                areConstraintsSatisfied = mAdsHandlerNew.areConstraintsSatisfied();
            }
        }

        HashMap<String, String> params = new HashMap<>();
        addParams(params);
        TrulyMadlyTrackEvent.trackEvent(mContext, trackingCategory, constraints_check, 0,
                String.valueOf(areConstraintsSatisfied), params, true);

        return areConstraintsSatisfied;
    }

    public boolean shouldShowAd(AdsType adsType, boolean callFailed) {
        boolean shouldShowAd = false;
        AdsHandlerNew mAdsHandlerNew = mTypeToHandlerMap.get(adsType);
        //Fixing issue: ANDROID-2503
        try {
            if (mAdsHandlerNew != null) {
                shouldShowAd = Preconditions.checkNotNull(mAdsHandlerNew).shouldShowAd(callFailed);
            }
        } catch (VerifyError verifyError) {
            //Hack to fix :
            //https://fabric.io/trulymadly/android/apps/com.trulymadly.android.app.t1/issues?q=java.lang.VerifyError
            Crashlytics.logException(verifyError);
        }

        return shouldShowAd;

    }

    @Override
    public void onClicked(AdsType adsType, Object object) {
        AdsHandlerNew mAdsHandlerNew = mTypeToHandlerMap.get(adsType);
        if (mAdsHandlerNew != null && object != null && mAdsHandlerNew instanceof BasicNativeAdInteractionInterface) {
            ((BasicNativeAdInteractionInterface) mAdsHandlerNew).onClicked(adsType, object);
        }
    }

    @Override
    public void reportImpression(AdsType adsType, Object object) {
        AdsHandlerNew mAdsHandlerNew = mTypeToHandlerMap.get(adsType);
        if (mAdsHandlerNew != null && object != null && mAdsHandlerNew instanceof BasicNativeAdInteractionInterface) {
            ((BasicNativeAdInteractionInterface) mAdsHandlerNew).reportImpression(adsType, object);
        }
    }

    @Override
    public void adLoaded(AdsType adsType, boolean success) {
        AdsHandlerNew mAdsHandlerNew = mTypeToHandlerMap.get(adsType);
        if (mAdsHandlerNew != null && mAdsHandlerNew instanceof BasicNativeAdInteractionInterface) {
            ((BasicNativeAdInteractionInterface) mAdsHandlerNew).adLoaded(adsType, success);
        }
    }

    public static void adShown(Context context, AdsType adsType){
        switch (adsType){
            case MATCHES_END_INTERSTITIAL:
                String userId = Utility.getMyId(context);
                int currentCounter = RFHandler.getInt(context, userId, ConstantsRF.RIGID_FIELD_INTERSTITIAL_MATCHES_COUNTER, 0);
                int globalCounter = RFHandler.getInt(context, userId,
                        ConstantsRF.RIGID_FIELD_INTERSTITIAL_COUNTER, 0);
                currentCounter++;
                globalCounter++;
                RFHandler.insert(context, userId, ConstantsRF.RIGID_FIELD_INTERSTITIAL_MATCHES_COUNTER, currentCounter);
                RFHandler.insert(context, userId, ConstantsRF.RIGID_FIELD_INTERSTITIAL_COUNTER, globalCounter);
                RFHandler.insert(context, userId, ConstantsRF.RIGID_FIELD_LAST_INTERSTITIAL_AD_TIMESTAMP, System.currentTimeMillis());
                break;

            case CONV_LIST_NATIVE:
                RFHandler.insert(context, Utility.getMyId(context), ConstantsRF.RIGID_FIELD_LAST_NATIVE_AD_TIMESTAMP, System.currentTimeMillis());
                break;
        }
    }

}
