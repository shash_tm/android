package com.trulymadly.android.app.ads;

/**
 * Created by avin on 02/05/16.
 */
public class ProfileAdsConfig {
    private boolean isProfileAdEnabled;
    private int mFinalAdPosition = -1;
    private boolean isRepeatEnabled = false;
    private long mAdsTimeIntervalInMillis = 0;

    public boolean isProfileAdEnabled() {
        return isProfileAdEnabled;
    }

    public void setIsProfileAdEnabled(boolean isProfileAdEnabled) {
        this.isProfileAdEnabled = isProfileAdEnabled;
    }

    public int getmFinalAdPosition() {
        return mFinalAdPosition;
    }

    public void setmFinalAdPosition(int mFinalAdPosition) {
        this.mFinalAdPosition = mFinalAdPosition;
    }

    public boolean isRepeatEnabled() {
        return isRepeatEnabled;
    }

    public void setIsRepeatEnabled(boolean isRepeatEnabled) {
        this.isRepeatEnabled = isRepeatEnabled;
    }

    public long getmAdsTimeIntervalInMillis() {
        return mAdsTimeIntervalInMillis;
    }

    public void setmAdsTimeIntervalInMillis(long mAdsTimeIntervalInMillis) {
        this.mAdsTimeIntervalInMillis = mAdsTimeIntervalInMillis;
    }
}
