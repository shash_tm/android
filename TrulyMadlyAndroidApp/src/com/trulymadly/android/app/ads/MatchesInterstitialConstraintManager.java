package com.trulymadly.android.app.ads;

import android.content.Context;

import com.trulymadly.android.app.json.ConstantsRF;
import com.trulymadly.android.app.sqlite.RFHandler;
import com.trulymadly.android.app.utility.TimeUtils;
import com.trulymadly.android.app.utility.Utility;

import java.util.Calendar;

/**
 * Created by avin on 11/07/16.
 */
public class MatchesInterstitialConstraintManager extends AdConstraintManager {

    @Override
    public boolean validateConstraints(Context context) {
        return (isAdTimeStampSatisfied(context) && isAdCounterSatisfied(context));
    }

    private static boolean isAdTimeStampSatisfied(Context context) {
        //Initialization of keys and default values
        String lastTimestampKey = ConstantsRF.RIGID_FIELD_LAST_INTERSTITIAL_AD_TIMESTAMP;
        String timeoutKey = ConstantsRF.RIGID_FIELD_INTERSTITIAL_AD_TIMEINTERVAL;
        long defaultTimeout = -1;
        String userId = Utility.getMyId(context);

        long timeinterval = RFHandler.getLong(context, userId, timeoutKey, defaultTimeout);
        if (timeinterval == -1) {
            return false;
        } else {
            if (isDayChanged(context, userId, lastTimestampKey)) {
                RFHandler.insert(context, userId, ConstantsRF.RIGID_FIELD_INTERSTITIAL_COUNTER, 0);
                RFHandler.insert(context, userId, ConstantsRF.RIGID_FIELD_INTERSTITIAL_MATCHES_COUNTER, 0);
            }
        }
        return TimeUtils.isDifferenceGreater(context, lastTimestampKey, timeinterval);
    }

    private static boolean isDayChanged(Context context, String userId, String timeStampKey) {
        long lastTimeStamp = RFHandler.getLong(context, userId, timeStampKey, -1);
        if (lastTimeStamp == -1)
            return false;

        Calendar lastCalendar = Calendar.getInstance();
        lastCalendar.setTimeInMillis(lastTimeStamp);

        Calendar currentCalendar = Calendar.getInstance();
        return lastCalendar.get(Calendar.DAY_OF_MONTH) != currentCalendar.get(Calendar.DAY_OF_MONTH) ||
                lastCalendar.get(Calendar.MONTH) != currentCalendar.get(Calendar.MONTH) ||
                lastCalendar.get(Calendar.YEAR) != currentCalendar.get(Calendar.YEAR);
    }

    private static boolean isAdCounterSatisfied(Context context) {
        String globalInterstitialUpperCapKey = ConstantsRF.RIGID_FIELD_INTERSTITIAL_UPPER_CAP;
        String currentInterstitialUpperCapKey = "";
        String globalInterstitialCounterKey = ConstantsRF.RIGID_FIELD_INTERSTITIAL_COUNTER;
        String currentInterstitialCounterKey = "";
        int currentInterstitialUpperCap = -1, currentInterstitialCounter = -1,
                globalInterstitialUpperCap = -1, globalInterstitialCounter = -1;
        int defaultCurrentUpperCap = 0;
        int defaultGlobalUpperCap = 0;
        String userId = Utility.getMyId(context);
        currentInterstitialUpperCapKey = ConstantsRF.RIGID_FIELD_MATCHES_INTERSTITIAL_UPPER_CAP;
        currentInterstitialCounterKey = ConstantsRF.RIGID_FIELD_INTERSTITIAL_MATCHES_COUNTER;

        //default values
        defaultGlobalUpperCap = 0;
        defaultCurrentUpperCap = 0;

        currentInterstitialCounter = RFHandler.getInt(context,
                userId, currentInterstitialCounterKey, 0);
        globalInterstitialCounter = RFHandler.getInt(context, userId,
                globalInterstitialCounterKey, 0);

        currentInterstitialUpperCap = RFHandler.getInt(context,
                userId, currentInterstitialUpperCapKey, defaultCurrentUpperCap);
        globalInterstitialUpperCap = RFHandler.getInt(context, userId,
                globalInterstitialUpperCapKey, defaultGlobalUpperCap);

        return globalInterstitialCounter < globalInterstitialUpperCap &&
                currentInterstitialCounter < currentInterstitialUpperCap;

    }

}
