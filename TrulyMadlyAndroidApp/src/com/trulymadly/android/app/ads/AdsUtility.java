package com.trulymadly.android.app.ads;

import com.trulymadly.android.app.modal.ProfileNewModal;
import com.trulymadly.android.app.modal.UserModal;

/**
 * Created by avin on 25/04/16.
 */
public class AdsUtility {
    public static ProfileNewModal prepareProfileNewModal() {
        ProfileNewModal profileNewModal = new ProfileNewModal();
        profileNewModal.setUser(prepareUserModal());
        profileNewModal.setIsAdProfile(true);
        profileNewModal.setInterest(new String[]{"#Sponsored", "#Advertisement"});
        return profileNewModal;
    }

    private static UserModal prepareUserModal() {
        UserModal userModal = new UserModal();
        userModal.setName("Some Name");
        return userModal;
    }
}
