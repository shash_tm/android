package com.trulymadly.android.app.ads;

/**
 * Created by avin on 26/04/16.
 */
public interface AdsListener {
    void onAdLoadSuccess(AdsType adsType, Object object);

    void onAdLoadfailed(AdsType adsType);

    void onAdStarted(AdsType adsType);

    void onAdFinished(AdsType adsType);

    void onErrorReceived(AdsType adsType);

    @SuppressWarnings("EmptyMethod")
    void onAdStopped(AdsType adsType);

    void onAdClicked(AdsType adsType);
}
