package com.trulymadly.android.app.ads;

/**
 * Created by avin on 13/07/16.
 */
public interface BasicNativeAdEventsListener  {
    void reportImpression();

    void onClicked();
}
