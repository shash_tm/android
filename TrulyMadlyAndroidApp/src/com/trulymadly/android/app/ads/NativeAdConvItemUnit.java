package com.trulymadly.android.app.ads;

/**
 * Implemented by respective AdsHandlerNew (currently only SeventyNineNativeHandler)
 * and passed to conversationlistactivity
 *
 * Created by avin on 14/07/16.
 */

public class NativeAdConvItemUnit {
    private NativeAdConvItemUIUnit mNativeAdConvItemUIUnit;

    public NativeAdConvItemUnit(NativeAdConvItemUIUnit mNativeAdConvItemUIUnit) {
        this.mNativeAdConvItemUIUnit = mNativeAdConvItemUIUnit;
    }

    public NativeAdConvItemUIUnit getmNativeAdConvItemUIUnit() {
        return mNativeAdConvItemUIUnit;
    }

    public void setmNativeAdConvItemUIUnit(NativeAdConvItemUIUnit mNativeAdConvItemUIUnit) {
        this.mNativeAdConvItemUIUnit = mNativeAdConvItemUIUnit;
    }
}
