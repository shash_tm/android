package com.trulymadly.android.app.ads;

/**
 * Created by avin on 22/07/16.
 */
public class AdsConfig {
    private boolean isAdsEnabled;

    private boolean isProfileAdEnabled;
    private int mFinalAdPosition = -1;
    private boolean isRepeatEnabled = false;
    private long mProfileAdsTimeIntervalInMillis = 0;
    private boolean isMediationEnabled = false;

    private long mGlobalInterstitialCounter, mMatchesInterstitialCounter,
//            mConversationsInterstitialCounter,
            mInterstitialTimeout, mNativeAdTimeout;

    public boolean isAdsEnabled() {
        return isAdsEnabled;
    }

    public void setAdsEnabled(boolean adsEnabled) {
        isAdsEnabled = adsEnabled;
    }

    public boolean isProfileAdEnabled() {
        return isProfileAdEnabled;
    }

    public void setProfileAdEnabled(boolean profileAdEnabled) {
        isProfileAdEnabled = profileAdEnabled;
    }

    public int getmFinalAdPosition() {
        return mFinalAdPosition;
    }

    public void setmFinalAdPosition(int mFinalAdPosition) {
        this.mFinalAdPosition = mFinalAdPosition;
    }

    public boolean isRepeatEnabled() {
        return isRepeatEnabled;
    }

    public void setRepeatEnabled(boolean repeatEnabled) {
        isRepeatEnabled = repeatEnabled;
    }

    public long getmProfileAdsTimeIntervalInMillis() {
        return mProfileAdsTimeIntervalInMillis;
    }

    public void setmProfileAdsTimeIntervalInMillis(long mProfileAdsTimeIntervalInMillis) {
        this.mProfileAdsTimeIntervalInMillis = mProfileAdsTimeIntervalInMillis;
    }

    public long getmGlobalInterstitialCounter() {
        return mGlobalInterstitialCounter;
    }

    public void setmGlobalInterstitialCounter(long mGlobalInterstitialCounter) {
        this.mGlobalInterstitialCounter = mGlobalInterstitialCounter;
    }

    public long getmMatchesInterstitialCounter() {
        return mMatchesInterstitialCounter;
    }

    public void setmMatchesInterstitialCounter(long mMatchesInterstitialCounter) {
        this.mMatchesInterstitialCounter = mMatchesInterstitialCounter;
    }

//    public long getmConversationsInterstitialCounter() {
//        return mConversationsInterstitialCounter;
//    }
//
//    public void setmConversationsInterstitialCounter(long mConversationsInterstitialCounter) {
//        this.mConversationsInterstitialCounter = mConversationsInterstitialCounter;
//    }

    public long getmInterstitialTimeout() {
        return mInterstitialTimeout;
    }

    public void setmInterstitialTimeout(long mInterstitialTimeout) {
        this.mInterstitialTimeout = mInterstitialTimeout;
    }

    public long getmNativeAdTimeout() {
        return mNativeAdTimeout;
    }

    public void setmNativeAdTimeout(long mNativeAdTimeout) {
        this.mNativeAdTimeout = mNativeAdTimeout;
    }

    public boolean isMediationEnabled() {
        return isMediationEnabled;
    }

    public void setMediationEnabled(boolean mediationEnabled) {
        isMediationEnabled = mediationEnabled;
    }
}
