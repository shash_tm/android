package com.trulymadly.android.app.ads;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.seventynine.sdkadapter.SeventynineSdkAdapter;
import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.utility.TmLogger;
import com.trulymadly.android.app.utility.Utility;

import java.util.ArrayList;
import java.util.HashMap;

import seventynine.nativeads.NativeAd;
import seventynine.sdk.SeventynineAdSDK;
import seventynine.sdk.SeventynineConstants;

/**
 * Created by avin on 13/07/16.
 */
public class SeventyNineNativeHandler extends AdsHandlerNew implements BasicNativeAdInteractionInterface {
    private final String TAG = "SeventyNineNative";
    private NativeAd mNativeAds;
    private ArrayList mNativeAdsList;
    private int mImpressionCount = 0;
    private FrameLayout mAdContainer;
    private SeventynineAdSDK mSeventynineAdSDK;

    public SeventyNineNativeHandler(Context mContext, AdConstraintManager mAdConstraintManager,
                                    AdsType mAdsType, String mTrackingCategory) {
        super(mContext, mAdConstraintManager, mAdsType, mTrackingCategory);
    }

    public SeventyNineNativeHandler(Context mContext, AdConstraintManager mAdConstraintManager,
                                    AdsType mAdsType, AdsListener adsListener, String mTrackingCategory) {
        super(mContext, mAdConstraintManager, mAdsType, adsListener, mTrackingCategory);
    }

    @Override
    public void initialize() {
        if(!AdsHelper.is79AdsEnabled(getmContext())){
            return;
        }

        mSeventynineAdSDK = new SeventynineAdSDK();
        mSeventynineAdSDK.setCallbackListener(null);

        if(mNativeAds == null) {
            mNativeAds = new NativeAd();
        }
    }

    private void fetchAds(){
        mNativeAdsList = NativeAd.getNativeAds();
    }

    @Override
    public void loadAd(Object object, boolean fromMediation) {
        mImpressionCount = 0;
        HashMap<String, String> eventInfo = new HashMap<>();
        eventInfo.put("from_79", String.valueOf(isFrom79()));
        eventInfo.put("method", "loadAd");
        AdsHelper.addParams(eventInfo);
        TrulyMadlyTrackEvent.trackEvent(getmContext(), getmTrackingCategory(),
                getmAdsType().getEventTypeForTracking(), 0,
                TrulyMadlyEvent.TrulyMadlyEventStatus.ad_request_made, eventInfo, true);
        Log.d(TAG, getmTrackingCategory() + " : " + getmAdsType().getEventTypeForTracking() + " : "
                + "ad_request_made :: method-loadAd,from79-" + isFrom79());
        if(mNativeAdsList == null || mNativeAdsList.size() == 0){
            fetchAds();
        }

        if(mNativeAdsList != null && mNativeAdsList.size() > 0){
            setFrom79(true);
            HashMap hashMap = (HashMap) mNativeAdsList.get(0);
            NativeAdConvItemUIUnit nativeAdConvItemUIUnit = new NativeAdConvItemUIUnit(
                    (String) hashMap.get("icon"), (String) hashMap.get("title"),
                    (String) hashMap.get("description"));
            SeventyNinenativeAdUnit seventyNinenativeAdUnit = new SeventyNinenativeAdUnit(nativeAdConvItemUIUnit,
                    (String) hashMap.get("adMd5"));

            TrulyMadlyTrackEvent.trackEvent(getmContext(), getmTrackingCategory(),
                    getmAdsType().getEventTypeForTracking(), 0,
                    TrulyMadlyEvent.TrulyMadlyEventStatus.ad_request_success, eventInfo, true);
            Log.d(TAG, getmTrackingCategory() + " : " + getmAdsType().getEventTypeForTracking() + " : "
                    + "ad_request_success :: method-loadAd,from79-" + isFrom79());

            if (getmAdsListener() != null) {
                getmAdsListener().onAdLoadSuccess(getmAdsType(), seventyNinenativeAdUnit);
            }

            mNativeAdsList.remove(0);
        }else{
            eventInfo.put("from_79", String.valueOf(isFrom79()));
            TrulyMadlyTrackEvent.trackEvent(getmContext(), getmTrackingCategory(),
                    getmAdsType().getEventTypeForTracking(), 0,
                    TrulyMadlyEvent.TrulyMadlyEventStatus.ad_request_failure, eventInfo, true);
            Log.d(TAG, getmTrackingCategory() + " : " + getmAdsType().getEventTypeForTracking() + " : "
                    + "ad_request_failure :: method-loadAd,from79-" + isFrom79());
            if (is79MediationEnabled()) {
                mAdContainer = new FrameLayout(getmContext());
                setFrom79(false);
                SeventynineSdkAdapter.getInstance().callAdapter(getmContext(), mAdContainer,
                        getmAdsType().getPlacementId(AdsSource.SEVENTY_NINE));
                eventInfo.put("from_79", String.valueOf(isFrom79()));
                TrulyMadlyTrackEvent.trackEvent(getmContext(), getmTrackingCategory(),
                        getmAdsType().getEventTypeForTracking(), 0,
                        TrulyMadlyEvent.TrulyMadlyEventStatus.ad_request_made, eventInfo, true);
                Log.d(TAG, getmTrackingCategory() + " : " + getmAdsType().getEventTypeForTracking() + " : "
                        + "ad_request_made :: method-loadAd,from79-" + isFrom79());
            } else if (getmAdsListener() != null) {
                getmAdsListener().onAdLoadfailed(getmAdsType());
            }
        }
    }

    @Override
    public boolean shouldShowAd(boolean callFailed) {
//        if(getmAdConstraintManager().isAdAllowed(getmContext())) {
            initialize();
            return true;
//        }
//        return false;
    }

    @Override
    public void onClicked(AdsType adsType, Object object) {
        SeventyNinenativeAdUnit seventyNinenativeAdUnit = (SeventyNinenativeAdUnit) object;
        SeventynineConstants.isOpenInAppBrawser = true;
        HashMap<String, String> eventInfo = new HashMap<>();
        eventInfo.put("from_79", String.valueOf(isFrom79()));
        AdsHelper.addParams(eventInfo);
        TrulyMadlyTrackEvent.trackEvent(getmContext(), getmTrackingCategory(),
            getmAdsType().getEventTypeForTracking(), 0,
                TrulyMadlyEvent.TrulyMadlyEventStatus.ad_clicked, eventInfo, true);
    }

    @Override
    public void reportImpression(AdsType adsType, Object object) {
        SeventyNinenativeAdUnit seventyNinenativeAdUnit = (SeventyNinenativeAdUnit) object;
        if (Utility.isSet(seventyNinenativeAdUnit.getmMD5String())) {
            NativeAd.onNativeAdImpression(seventyNinenativeAdUnit.getmMD5String());
        }
        mImpressionCount++;
        if(mImpressionCount == 1){
            adShown(getmAdsType());
            HashMap<String, String> eventInfo = new HashMap<>();
            eventInfo.put("from_79", String.valueOf(isFrom79()));
            AdsHelper.addParams(eventInfo);
            TrulyMadlyTrackEvent.trackEvent(getmContext(), getmTrackingCategory(),
                getmAdsType().getEventTypeForTracking(), 0,
                    TrulyMadlyEvent.TrulyMadlyEventStatus.ad_shown, eventInfo, true);
        }
    }

    private boolean hackToFixBlankAd() {
        boolean isAdReady = false;

        if (mAdContainer != null) {
            TextView profile_title = (TextView) mAdContainer.findViewById(R.id.profile_title);
            if (profile_title != null && Utility.isSet(profile_title.getText().toString())) {
                isAdReady = true;
            }
        }

        return isAdReady;
    }

    @Override
    public void adLoaded(AdsType adsType, boolean success) {
        boolean isAdReady = hackToFixBlankAd();
        TmLogger.d(TAG, "adLoaded : isAdReady : " + isAdReady + " && success : " + success);
        success = isAdReady;
        HashMap<String, String> eventInfo = new HashMap<>();
        eventInfo.put("from_79", String.valueOf(isFrom79()));
        eventInfo.put("method", "adLoaded : success : " + success);
        AdsHelper.addParams(eventInfo);
        TrulyMadlyTrackEvent.trackEvent(getmContext(), getmTrackingCategory(),
                getmAdsType().getEventTypeForTracking(), 0,
                (success) ? TrulyMadlyEvent.TrulyMadlyEventStatus.ad_request_success :
                        TrulyMadlyEvent.TrulyMadlyEventStatus.ad_request_failure, eventInfo, true);
        if (getmAdsListener() != null) {
            if (success) {
                SeventyNinenativeAdUnit seventyNinenativeAdUnit = new SeventyNinenativeAdUnit(mAdContainer);
                getmAdsListener().onAdLoadSuccess(getmAdsType(), seventyNinenativeAdUnit);
            } else {
                getmAdsListener().onAdLoadfailed(getmAdsType());
            }
        }
    }

    public class SeventyNinenativeAdUnit extends NativeAdConvItemUnit {
        private View mAdContainer;
        private String mMD5String;

        public SeventyNinenativeAdUnit(NativeAdConvItemUIUnit mNativeAdConvItemUIUnit, String mMD5String) {
            super(mNativeAdConvItemUIUnit);
            this.mMD5String = mMD5String;
        }

        public SeventyNinenativeAdUnit(View mAdContainer) {
            super(null);
            this.mAdContainer = mAdContainer;
        }

        public String getmMD5String() {
            return mMD5String;
        }

        public void setmMD5String(String mMD5String) {
            this.mMD5String = mMD5String;
        }

        public View getmAdContainer() {
            return mAdContainer;
        }

        public void setmAdContainer(View mAdContainer) {
            this.mAdContainer = mAdContainer;
        }
    }
}
