package com.trulymadly.android.app.ads;

/**
 * Created by avin on 12/07/16.
 */
public class SimpleAdsListener implements AdsListener {

    @Override
    public void onAdLoadSuccess(AdsType adsType, Object object) {

    }

    @Override
    public void onAdLoadfailed(AdsType adsType) {

    }

    @Override
    public void onAdStarted(AdsType adsType) {

    }

    @Override
    public void onAdFinished(AdsType adsType) {

    }

    @Override
    public void onErrorReceived(AdsType adsType) {

    }

    @Override
    public void onAdStopped(AdsType adsType) {

    }

    @Override
    public void onAdClicked(AdsType adsType) {

    }
}
