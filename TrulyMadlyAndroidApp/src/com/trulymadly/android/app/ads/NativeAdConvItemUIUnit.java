package com.trulymadly.android.app.ads;

/**
 * Used by ConversationListAdapter to render the respective ad
 *
 * Created by avin on 13/07/16.
 */
public class NativeAdConvItemUIUnit {
    private String mImageUrl, mTitle, mDescription;

    public NativeAdConvItemUIUnit(String mImageUrl, String mTitle, String mDescription) {
        this.mImageUrl = mImageUrl;
        this.mTitle = mTitle;
        this.mDescription = mDescription;
    }

    public String getmImageUrl() {
        return mImageUrl;
    }

    public void setmImageUrl(String mImageUrl) {
        this.mImageUrl = mImageUrl;
    }

    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getmDescription() {
        return mDescription;
    }

    public void setmDescription(String mDescription) {
        this.mDescription = mDescription;
    }
}
