package com.trulymadly.android.app.ads;

import android.content.Context;

import com.trulymadly.android.app.json.ConstantsRF;
import com.trulymadly.android.app.sqlite.RFHandler;
import com.trulymadly.android.app.utility.TimeUtils;
import com.trulymadly.android.app.utility.Utility;

/**
 * Created by avin on 14/07/16.
 */
public class ConvNativeAdConstraintManager extends AdConstraintManager {

    private static boolean isAdTimeStampSatisfied(Context context) {
        //Initialization of keys and default values
        String lastTimestampKey = ConstantsRF.RIGID_FIELD_LAST_NATIVE_AD_TIMESTAMP;
        String timeoutKey = ConstantsRF.RIGID_FIELD_NATIVE_AD_TIMEINTERVAL;
        long defaultTimeout = -1;
        String userId = Utility.getMyId(context);
        long timeinterval = RFHandler.getLong(context, userId, timeoutKey, defaultTimeout);
        return timeinterval != -1 && TimeUtils.isDifferenceGreater(context, lastTimestampKey, timeinterval);
    }

    @Override
    public boolean validateConstraints(Context context) {
//        return true;
        return isAdTimeStampSatisfied(context);
    }

}
