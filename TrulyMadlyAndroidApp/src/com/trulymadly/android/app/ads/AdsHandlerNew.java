package com.trulymadly.android.app.ads;

import android.content.Context;

import com.trulymadly.android.app.json.ConstantsRF;
import com.trulymadly.android.app.sqlite.RFHandler;
import com.trulymadly.android.app.utility.Utility;

/**
 * Created by avin on 11/07/16.
 */
public abstract class AdsHandlerNew {
    protected final String TAG = "AdsHandlerNew";
    private Context mContext;
    private AdConstraintManager mAdConstraintManager;
    private AdsListener mAdsListener;
    private AdsType mAdsType;
    private String mTrackingCategory;
    private boolean isFrom79 = true;

    public AdsHandlerNew(Context mContext, AdConstraintManager mAdConstraintManager, AdsType mAdsType,
                         String mTrackingCategory) {
        this(mContext, mAdConstraintManager, mAdsType, null, mTrackingCategory);
    }

    public AdsHandlerNew(Context mContext, AdConstraintManager mAdConstraintManager, AdsType mAdsType,
                         AdsListener adsListener, String mTrackingCategory) {
        this.mContext = mContext;
        this.mAdConstraintManager = mAdConstraintManager;
        this.mAdsListener = adsListener;
        this.mAdsType = mAdsType;
        this.mTrackingCategory = mTrackingCategory;
        isFrom79 = true;
    }

    public abstract void initialize();

    public abstract void loadAd(Object object, boolean fromMediation);
    public abstract boolean shouldShowAd(boolean callFailed);

    public void adShown(AdsType adsType){
        AdsHelper.adShown(mContext, adsType);
//        switch (adsType){
//            case MATCHES_END_INTERSTITIAL:
//                String userId = Utility.getMyId(mContext);
//                int currentCounter = RFHandler.getInt(mContext, userId, ConstantsRF.RIGID_FIELD_INTERSTITIAL_MATCHES_COUNTER, 0);
//                int globalCounter = RFHandler.getInt(mContext, userId,
//                        ConstantsRF.RIGID_FIELD_INTERSTITIAL_COUNTER, 0);
//                currentCounter++;
//                globalCounter++;
//                RFHandler.insert(mContext, userId, ConstantsRF.RIGID_FIELD_INTERSTITIAL_MATCHES_COUNTER, currentCounter);
//                RFHandler.insert(mContext, userId, ConstantsRF.RIGID_FIELD_INTERSTITIAL_COUNTER, globalCounter);
//                RFHandler.insert(mContext, userId, ConstantsRF.RIGID_FIELD_LAST_INTERSTITIAL_AD_TIMESTAMP, System.currentTimeMillis());
//                break;
//
//            case CONV_LIST_NATIVE:
//                RFHandler.insert(mContext, Utility.getMyId(mContext), ConstantsRF.RIGID_FIELD_LAST_NATIVE_AD_TIMESTAMP, System.currentTimeMillis());
//                break;
//        }
    }

    public AdConstraintManager getmAdConstraintManager() {
        return mAdConstraintManager;
    }

    public void setmAdConstraintManager(AdConstraintManager mAdConstraintManager) {
        this.mAdConstraintManager = mAdConstraintManager;
    }

    public Context getmContext() {
        return mContext;
    }

    public void setmContext(Context mContext) {
        this.mContext = mContext;
    }

    public AdsListener getmAdsListener() {
        return mAdsListener;
    }

    public void setmAdsListener(AdsListener mAdsListener) {
        this.mAdsListener = mAdsListener;
    }

    public AdsType getmAdsType() {
        return mAdsType;
    }

    public void setmAdsType(AdsType mAdsType) {
        this.mAdsType = mAdsType;
    }

    public boolean areConstraintsSatisfied(){
        return getmAdConstraintManager().isAdAllowed(mContext);
    }

    public String getmTrackingCategory() {
        return mTrackingCategory;
    }

    public void setmTrackingCategory(String mTrackingCategory) {
        this.mTrackingCategory = mTrackingCategory;
    }

    public boolean isFrom79() {
        return isFrom79;
    }

    public void setFrom79(boolean from79) {
        isFrom79 = from79;
    }

    public boolean is79MediationEnabled() {
        return RFHandler.getBool(getmContext(), Utility.getMyId(getmContext()),
                ConstantsRF.RIGID_FIELD_79_MEDIATION_ENABLED);
    }

    protected boolean is79SDKInitialized() {
        return AdsHelper.is79Initialized();
    }
}
