package com.trulymadly.android.app.ads;

/**
 * Created by avin on 03/12/15.
 */
public class InterstitialNativeAdsConfig {
    private long mGlobalInterstitialCounter, mMatchesInterstitialCounter,
            mConversationsInterstitialCounter, mInterstitialTimeout, mNativeAdTimeout;

    public long getmGlobalInterstitialCounter() {
        return mGlobalInterstitialCounter;
    }

    public void setmGlobalInterstitialCounter(long mGlobalInterstitialCounter) {
        this.mGlobalInterstitialCounter = mGlobalInterstitialCounter;
    }

    public long getmMatchesInterstitialCounter() {
        return mMatchesInterstitialCounter;
    }

    public void setmMatchesInterstitialCounter(long mMatchesInterstitialCounter) {
        this.mMatchesInterstitialCounter = mMatchesInterstitialCounter;
    }

    public long getmConversationsInterstitialCounter() {
        return mConversationsInterstitialCounter;
    }

    public void setmConversationsInterstitialCounter(long mConversationsInterstitialCounter) {
        this.mConversationsInterstitialCounter = mConversationsInterstitialCounter;
    }

    public long getmInterstitialTimeout() {
        return mInterstitialTimeout;
    }

    public void setmInterstitialTimeout(long mInterstitialTimeout) {
        this.mInterstitialTimeout = mInterstitialTimeout;
    }

    public long getmNativeAdTimeout() {
        return mNativeAdTimeout;
    }

    public void setmNativeAdTimeout(long mNativeAdTimeout) {
        this.mNativeAdTimeout = mNativeAdTimeout;
    }
}
