/**
 * 
 */
package com.trulymadly.android.app;

import android.content.Context;

import com.koushikdutta.ion.Ion;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.utility.OkHttpHandler;
import com.trulymadly.android.app.utility.Utility;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author udbhav
 * 
 */
public class GetHttpRequestParams {

	@SuppressWarnings("ArraysAsListWithZeroOrOneArgument")
	public static Map<String, List<String>> getIonHeaders(Context aContext) {

		Ion ion = Ion.getDefault(aContext);
		ion.getCookieMiddleware().clear();
		//String PHPSESSID = Utility.getCookieByName(aContext, "PHPSESSID");
		String PHPSESSID = OkHttpHandler.getCookieByName(aContext, "PHPSESSID");
		String cookieString = "PHPSESSID" + "=" + PHPSESSID;

		Map<String, List<String>> headers = new HashMap<>();
		headers.put("login_mobile", Arrays.asList("true"));
		headers.put("Cookie", Arrays.asList(cookieString));
		headers.put("source", Arrays.asList("androidApp"));
		headers.put("store", Arrays.asList(Constants.RELEASE_STORE));
		headers.put("app_version_code", Arrays.asList(Utility.getAppVersionCode(aContext)));
		headers.put("app_version_name", Arrays.asList(Utility.getAppVersionName(aContext)));
		return headers;
	}
	
	public static Map<String, String> getHeaders(Context aContext) {

		String PHPSESSID = OkHttpHandler.getCookieByName(aContext, "PHPSESSID");
		String cookieString = "PHPSESSID" + "=" + PHPSESSID;

		Map<String, String> headers = new HashMap<>();
		headers.put("login_mobile", "true");
		headers.put("Cookie", cookieString);
		headers.put("source", "androidApp");
		headers.put("store", Constants.RELEASE_STORE);
		headers.put("app_version_code", Utility.getAppVersionCode(aContext));
		headers.put("app_version_name", Utility.getAppVersionName(aContext));
		return headers;
	}

}
