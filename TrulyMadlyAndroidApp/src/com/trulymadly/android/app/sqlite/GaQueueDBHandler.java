package com.trulymadly.android.app.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteCantOpenDatabaseException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.database.sqlite.SQLiteDiskIOException;

import com.trulymadly.android.Exception.DbIsNullException;
import com.trulymadly.android.app.modal.GaEventModal;
import com.trulymadly.android.app.utility.TimeUtils;

import java.util.ArrayList;

public class GaQueueDBHandler {

    //@Trace(category = MetricCategory.DATABASE)
    public static void insertGaItem(Context ctx, GaEventModal gaEventModal) {

        ContentValues values = new ContentValues();
        values.put("event_json", gaEventModal.getJsonStringForEvent());
        values.put("tstamp", TimeUtils.getFormattedTime());
        try {
            TrulyMadlySQLiteHandler.getDatabase(ctx).insert("ga_queue", null, values);
        } catch (DbIsNullException ignored) {
        }
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static int getCount(Context context) {
        int count = 0;
        Cursor cursor = null;
        try {
            cursor = TrulyMadlySQLiteHandler.getDatabase(context)
                    .rawQuery("select count(*) as count from ga_queue", null);
        } catch (DbIsNullException e) {
            return count;
        }
        cursor.moveToFirst();
        count = cursor.getInt(cursor.getColumnIndex("count"));
        if (cursor != null)
            cursor.close();
        return count;
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static ArrayList<GaEventModal> getItems(Context ctx, int itemLimit) {
        ArrayList<GaEventModal> gaEventModalList = new ArrayList<>();
        Cursor cursor = null;
        try {
            cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).query("ga_queue", null, null, null,
                    null, null, "rowid", itemLimit + "");
        } catch (DbIsNullException e) {
            return gaEventModalList;
        }
        try {
            while (cursor != null && cursor.moveToNext()) {
                String event_json = cursor
                        .getString(cursor.getColumnIndex("event_json"));
                gaEventModalList.add(new GaEventModal(event_json));
            }
        } catch (RuntimeException ignored) {
        }
        if (cursor != null)
            cursor.close();
        return gaEventModalList;
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static boolean deleteItems(Context ctx, int itemLimit) {
        int minRowid = 0;
        Cursor cursor = null;
        SQLiteDatabase db = null;
        try {
            db = TrulyMadlySQLiteHandler.getDatabase(ctx);
        } catch (DbIsNullException e) {
            return false;
        }
        cursor = db.query("ga_queue",
                new String[]{"rowid"}, null, null, null, null, "rowid", "1");
        boolean returnVal = true;
        try {
            if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
                minRowid = cursor.getInt(cursor.getColumnIndex("rowid"));
            }
            String whereClause = "rowid < ?";
            String[] whereArgs = new String[]{(minRowid + itemLimit) + ""};
            db.delete("ga_queue", whereClause, whereArgs);
            returnVal = true;
        } catch (IllegalStateException | SQLiteDiskIOException | SQLiteCantOpenDatabaseException | SQLiteDatabaseLockedException e) {
            returnVal = false;
        }
        if (cursor != null) {
            cursor.close();
        }
        return returnVal;
    }
}
