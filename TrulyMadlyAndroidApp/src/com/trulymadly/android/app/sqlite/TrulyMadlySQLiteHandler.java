package com.trulymadly.android.app.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.trulymadly.android.Exception.DbIsNullException;
import com.trulymadly.android.app.MessageModal;
import com.trulymadly.android.app.MessageModal.MessageType;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.Constants.FetchSource;
import com.trulymadly.android.app.json.Constants.MessageState;
import com.trulymadly.android.app.json.ConstantsDB;
import com.trulymadly.android.app.json.ConstantsRF;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.TimeUtils;
import com.trulymadly.android.app.utility.TmLogger;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

public class TrulyMadlySQLiteHandler extends SQLiteOpenHelper {
    private static final String DB_NAME = "trulymadlydb.sql";
    private static final int VERSION = 46;
    private static SQLiteDatabase singletonSQLiteWritableDatabase = null;
    private static TrulyMadlySQLiteHandler singletonTrulyMadlySQLiteHandler;
    private final Context aContext;

    private TrulyMadlySQLiteHandler(Context context) {
        super(context, DB_NAME, null, VERSION);
        this.aContext = context;
    }

    public static TrulyMadlySQLiteHandler getInstance(
            Context aContext) {
        if (singletonTrulyMadlySQLiteHandler == null)
            synchronized (TrulyMadlySQLiteHandler.class) {
                if (singletonTrulyMadlySQLiteHandler == null)
                    singletonTrulyMadlySQLiteHandler = new TrulyMadlySQLiteHandler(
                            aContext.getApplicationContext());
            }
        return singletonTrulyMadlySQLiteHandler;
    }


    public static SQLiteDatabase getDatabase(Context aContext) throws DbIsNullException {
        SQLiteDatabase db = getInstance(aContext)
                .getWritableDatabase();
        if (db != null) {
            return db;
        } else {
            throw (new DbIsNullException());
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // http://stackoverflow.com/questions/2701877/sqlite-table-constraint-unique-on-multiple-columns
        db.execSQL(
                "CREATE TABLE IF NOT EXISTS " + ConstantsDB.MESSAGES_TABLE + " (" +
                        "user_id VARCHAR(100), " +
                        "match_id VARCHAR(100), " +
                        "message_id VARCHAR(100), " +
                        "msg_json VARCHAR(5024), " +
                        "tstamp varchar(100) , " +
                        "showsending boolean, " +
                        "UNIQUE (match_id, message_id) " +
                        "ON CONFLICT REPLACE)");

        db.execSQL(
                " CREATE TABLE IF NOT EXISTS compatibility_hack (match_id VARCHAR(100), type VARCHAR(100), UNIQUE (match_id, type) ON CONFLICT IGNORE) ");

        // http://stackoverflow.com/questions/1676448/using-sqlite-how-do-i-index-columns-in-a-create-table-statement
        db.execSQL("CREATE INDEX match_id ON " + ConstantsDB.MESSAGES_TABLE + " (match_id)");

        // http://stackoverflow.com/questions/946011/sqlite-add-primary-key
        db.execSQL(
                "CREATE TABLE IF NOT EXISTS " + ConstantsDB.MESSAGE_METADATA_TABLE + " (" +
                        "user_id VARCHAR(100), " +
                        "match_id VARCHAR(100), " +
                        "last_seen_receiver_tstamp varchar(100), " +
                        "last_fetched_id VARCHAR(100), " +
                        "profile_url varchar(500), " +
                        "fname varchar(300), " +
                        "profile_pic varchar(300), " +
                        "last_fetched_tstamp varchar(100), "+
                        "PRIMARY KEY(match_id) " +
                        "on CONFLICT REPLACE)");

        upgradeTo3(db);

        // event tracking
        upgradeTo7(db);

        // vouchers
        upgradeTo8(db);

        // adding user_id in messages
        upgradeTo10(db);

        // adding indexing to the messages to user user_id as well
        upgradeTo11(db);

        // adding last_updated_timestamp to match message data
        upgradeTo12(db);

        // adding message fetch_source in messages
        upgradeTo13(db);

        // adding sent_success flag in messages
        upgradeTo14(db);

        // adding last_chat_read_sent_tstamp in match message data
        upgradeTo15(db);

        // adding upadting meta data table for conversation view
        // clean messages tables
        // adding stickers table
        upgradeTo22(db);

        // clear the stickerstable , stickers_api_version , api_call_time
        upgradeTo23(db);

        // adding quizes table
        // adding quizStatus
        // add quiz id in message table
        upgradeTo24(db);

        // adding quiz_status_index
        upgradeTo25(db);

        // removing registar static basic data
        upgradeTo26();

        // adding conversation table
        upgradeTo30(db);

        // adding flareColumn
        upgradeTo31(db);

        // adding two columns to rigid table - userid, value (to store strings)
        upgradeTo32(db);

        // adding GA TABLE
        upgradeTo33(db);

        // adding is miss TM Flag
        upgradeTo34(db);

        upgradeTo35(db);

        upgradeTo36(db);

        upgradeTo37();

        upgradeTo38(db);

        upgradeTo39(db);

        //Migrating for photo messages
        upgradeTo40(db);

        //Migrating for share event messages
        upgradeTo41(db);

        //Adding doodle in meta data
        upgradeTo42(db);

        //Making sparks related changes
        upgradeTo43(db);

        //Adding payment_mode to Purchases table
        upgradeTo44(db);

        // deleting response of profile.php for video profile
        upgradeTo45(db);

        // adding select related changes
        upgradeTo46(db);
    }


    private void upgradeTo3(SQLiteDatabase db) {
        db.execSQL(
                "CREATE TABLE IF NOT EXISTS RIGID_FIELDS (ID VARCHAR(100), IS_SET VARCHAR(1))");
    }

    private void upgradeTo7(SQLiteDatabase db) {
        db.execSQL(
                "CREATE TABLE IF NOT EXISTS event_tracking (_id INTEGER PRIMARY KEY,event_info text)");

        db.execSQL(
                "CREATE TABLE IF NOT EXISTS EDIT_PREF (ID VARCHAR(100) PRIMARY KEY, DATA TEXT)");
    }

    private void upgradeTo8(SQLiteDatabase db) {
        db.execSQL(
                "CREATE TABLE IF NOT EXISTS vouchers (voucher_code varchar(512) PRIMARY KEY , user_id varchar(100), expiry DATETIME, campaign varchar(256), friendly_text text)");
    }

    private void upgradeTo10(SQLiteDatabase db) {
        try {
            db.execSQL("SELECT user_id from " + ConstantsDB.MESSAGES_TABLE);
        } catch (Exception e) {
            db.execSQL("ALTER TABLE " + ConstantsDB.MESSAGES_TABLE + " ADD COLUMN user_id VARCHAR(100)");
            db.execSQL(
                    "ALTER TABLE " + ConstantsDB.MESSAGE_METADATA_TABLE + " ADD COLUMN user_id VARCHAR(100)");
        }
    }

    private void upgradeTo11(SQLiteDatabase db) {
        db.execSQL(
                "CREATE INDEX message_meta_data_on_user_id_match_id ON " + ConstantsDB.MESSAGE_METADATA_TABLE + "(user_id,match_id)");

        db.execSQL("DROP INDEX match_id");

        db.execSQL(
                "CREATE INDEX messages_on_user_id_match_id ON " + ConstantsDB.MESSAGES_TABLE + "(user_id,match_id)");
        db.execSQL(
                "CREATE INDEX messages_on_user_id_match_id_tstam ON " + ConstantsDB.MESSAGES_TABLE + "(user_id,match_id,tstamp)");

    }

    private void upgradeTo12(SQLiteDatabase db) {
        db.execSQL(
                "ALTER TABLE " + ConstantsDB.MESSAGE_METADATA_TABLE + " ADD COLUMN last_updated_tstamp DATETIME");
    }

    private void upgradeTo13(SQLiteDatabase db) {
        db.execSQL(
                "ALTER TABLE " + ConstantsDB.MESSAGES_TABLE + " ADD COLUMN fetch_source VARCHAR(100) default '"
                        + FetchSource.POLLING.toString() + "'");
    }

    private void upgradeTo14(SQLiteDatabase db) {
        db.execSQL(
                "ALTER TABLE " + ConstantsDB.MESSAGES_TABLE + " ADD COLUMN sent_success boolean default 1");
    }

    private void upgradeTo15(SQLiteDatabase db) {
        db.execSQL(
                "ALTER TABLE " + ConstantsDB.MESSAGE_METADATA_TABLE + " ADD COLUMN last_chat_read_sent_tstamp DATETIME");
    }

    private void upgradeTo22(SQLiteDatabase db) {
        db.execSQL("ALTER TABLE " + ConstantsDB.MESSAGE_METADATA_TABLE + " ADD COLUMN age VARCHAR(10)");
        db.execSQL(
                "ALTER TABLE " + ConstantsDB.MESSAGE_METADATA_TABLE + " ADD COLUMN message_link VARCHAR(512)");
        db.execSQL(
                "ALTER TABLE " + ConstantsDB.MESSAGE_METADATA_TABLE + " ADD COLUMN last_message_state VARCHAR(100)");
        db.execSQL(
                "ALTER TABLE " + ConstantsDB.MESSAGES_TABLE + " ADD COLUMN message_state VARCHAR(100)");
        db.execSQL("ALTER TABLE " + ConstantsDB.MESSAGES_TABLE + " ADD COLUMN message_type VARCHAR(100)");
        db.execSQL(
                "ALTER TABLE " + ConstantsDB.MESSAGES_TABLE + " ADD COLUMN message_text VARCHAR(5024)");
        db.execSQL(
                "CREATE TABLE IF NOT EXISTS stickers (id int  PRIMARY KEY , name varchar(100), default_download boolean , updated_in_version int , cached_time DATETIME,last_used DATETIME, gallery_id int, type varchar(10), active boolean);");
        migrateExistingMessagesTable(db);
    }

    private void upgradeTo23(SQLiteDatabase db) {
        db.execSQL("Delete from stickers");
        SPHandler.remove(aContext,
                ConstantsSP.SHARED_KEYS_STICKER_API_CALL_TIME);
        SPHandler.remove(aContext,
                ConstantsSP.SHARED_KEYS_STICKER_VERSION);
    }

    private void upgradeTo24(SQLiteDatabase db) {
        db.execSQL(
                "CREATE TABLE IF NOT EXISTS quizzes (id int PRIMARY KEY, name varchar(100), updated_in_version int , cached_time DATETIME,banner_cached_time DATETIME, image_url varchar(200),banner_url varchar(200));");
        db.execSQL(
                "CREATE TABLE IF NOT EXISTS quizStatus (user_id varchar(100), match_id varchar(100), quiz_id int, status varchar(200), UNIQUE (user_id, match_id, quiz_id) ON CONFLICT REPLACE);");
        db.execSQL("ALTER TABLE " + ConstantsDB.MESSAGES_TABLE + " ADD COLUMN quiz_id int;");
    }

    private void upgradeTo25(SQLiteDatabase db) {
        db.execSQL(
                "CREATE INDEX truly_madly_quiz_status_index ON quizStatus(user_id,match_id,quiz_id);");
    }

    private void upgradeTo26() {
        // removing basic data url to handle new interests
        SPHandler.remove(aContext,
                ConstantsSP.SHARED_KEYS_EDIT_BASIC_DATA_URL);
    }

    private void upgradeTo30(SQLiteDatabase db) {
        // Conversation list
        db.execSQL(
                "CREATE TABLE IF NOT EXISTS " + ConstantsDB.CONVERSATION_LIST_TABLE + " (" +
                        "user_id VARCHAR(100), " +
                        "match_id VARCHAR(100), " +
                        "tstamp varchar(100), " +
                        "sort_tstamp varchar(100), " +
                        "last_message_state VARCHAR(100), " +
                        "last_message_type VARCHAR(100), " +
                        "last_message_text VARCHAR(5024), " +
                        "UNIQUE (user_id, match_id) " +
                        "on CONFLICT REPLACE);");
        migrateToConversationListTable(db);
    }

    private void upgradeTo31(SQLiteDatabase db) {
        db.execSQL(
                "ALTER TABLE quizStatus ADD COLUMN showFlare boolean  DEFAULT 0 ");
        // db.execSQL("ALTER TABLE " + ConstantsDB.MESSAGE_METADATA + " ADD COLUMN
        // last_quiz_status_call DATETIME");
        db.execSQL(
                "ALTER TABLE " + ConstantsDB.MESSAGE_METADATA_TABLE + " ADD COLUMN user_last_quiz_action_time VARCHAR(100) ");
        db.execSQL(
                "ALTER TABLE " + ConstantsDB.MESSAGE_METADATA_TABLE + " ADD COLUMN match_last_quiz_action_time VARCHAR(100)");
        db.execSQL(
                "ALTER TABLE quizStatus ADD COLUMN showAnimation boolean  DEFAULT 0 ");
    }

    private void upgradeTo32(SQLiteDatabase db) {
        // Adding two columns - USER_ID and VALUE and a unique constraint on the
        // combination of these two columns
        db.execSQL("ALTER TABLE RIGID_FIELDS ADD COLUMN USER_ID VARCHAR(100)");
        db.execSQL("ALTER TABLE RIGID_FIELDS ADD COLUMN VALUE VARCHAR(100)");
        db.execSQL(
                "CREATE UNIQUE INDEX RIGID_FIELDS_UNIQUE ON RIGID_FIELDS(ID, USER_ID)");
    }

    private void upgradeTo33(SQLiteDatabase db) {
        // adding GA queue
        db.execSQL(
                "CREATE TABLE IF NOT EXISTS ga_queue (tstamp varchar(100), event_json text);");
    }

    private void upgradeTo34(SQLiteDatabase db) {
        // adding miss tm flag
        db.execSQL(
                "ALTER TABLE " + ConstantsDB.MESSAGE_METADATA_TABLE + " ADD COLUMN is_miss_tm boolean default 0");
    }

    private void upgradeTo35(SQLiteDatabase db) {
        db.execSQL(
                "CREATE TABLE IF NOT EXISTS cached_data (url varchar(100), response TEXT, last_updated_tstamp varchar(100), hash_value CHAR(32),user_id varchar(100),  UNIQUE(url,user_id) on CONFLICT REPLACE);");
    }

    private void upgradeTo36(SQLiteDatabase db) {
        db.execSQL("ALTER TABLE " + ConstantsDB.MESSAGES_TABLE + " ADD COLUMN " + Constants.CD_KEY_DEAL_ID + " varchar(100)");
        db.execSQL("ALTER TABLE " + ConstantsDB.MESSAGES_TABLE + " ADD COLUMN " + Constants.CD_KEY_DATESPOT_ID + " varchar(100)");
        db.execSQL("ALTER TABLE " + ConstantsDB.MESSAGE_METADATA_TABLE + " ADD COLUMN " + Constants.CD_KEY_CLICKABLE
                + " int default -1");

        db.execSQL("CREATE TABLE IF NOT EXISTS " + ConstantsDB.DATE_SPOT_TABLE + " (" +
                        "message varchar(250), " +
                        Constants.CD_KEY_IMAGE_URI + " varchar(150), " +
                        "date_spot_id varchar(100), " +
                        "user_id varchar(100),  " +
                        "address varchar(100),  " +
                        "UNIQUE(date_spot_id, user_id) on CONFLICT REPLACE);");
    }

    private void upgradeTo37() {
        SPHandler.remove(aContext,
                ConstantsSP.SHARED_KEYS_STICKER_API_CALL_TIME);
        SPHandler.remove(aContext,
                ConstantsSP.SHARED_KEYS_QUIZ_API_CALL_TIME);
    }


    private void upgradeTo38(SQLiteDatabase db) {
        db.execSQL("ALTER TABLE " + ConstantsDB.MESSAGE_METADATA_TABLE + " ADD COLUMN clear_chat_tstamp varchar(100)");
    }


    private void upgradeTo39(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + ConstantsDB.MATCHES_DATA_TABLE + "(user_id varchar(100), match_id varchar(100), details TEXT,action boolean default 0 ,UNIQUE(user_id,match_id) on CONFLICT IGNORE)");
    }

    private void upgradeTo40(SQLiteDatabase db) {
        migrateExistingMessagesTableForImages(db);
    }

    private void upgradeTo41(SQLiteDatabase db) {
        migrateExistingMessagesTableForShareEvents(db);
    }

    private void upgradeTo42(SQLiteDatabase db) {
        db.execSQL("ALTER TABLE " + ConstantsDB.MESSAGE_METADATA_TABLE + " ADD COLUMN doodle_url varchar(300)");
    }

    private void upgradeTo43(SQLiteDatabase db) {
        db.execSQL("ALTER TABLE " + ConstantsDB.MESSAGE_METADATA_TABLE + " ADD COLUMN " + ConstantsDB.MESSAGE_METADATA.IS_SPARK_RECEIVED + " boolean default 0");
        db.execSQL("ALTER TABLE " + ConstantsDB.MESSAGE_METADATA_TABLE + " ADD COLUMN " + ConstantsDB.MESSAGE_METADATA.IS_MUTUAL_SPARK + " boolean default 0");

        db.execSQL(ConstantsDB.SPARKS.getCreateQuery());
        db.execSQL(ConstantsDB.PURCHASES.getCreateQuery());
    }

    private void upgradeTo44(SQLiteDatabase db) {
        db.execSQL(ConstantsDB.PURCHASES.addPaymentModeColumn());
    }

    private void upgradeTo45(SQLiteDatabase db) {
        CachingDBHandler.deleteURL(aContext, ConstantsUrls.get_photos_url(), Utility.getMyId(aContext));
    }

    private void upgradeTo46(SQLiteDatabase db) {
        db.execSQL("ALTER TABLE " + ConstantsDB.MESSAGE_METADATA_TABLE + " ADD COLUMN " + ConstantsDB.MESSAGE_METADATA.IS_SELECT_MATCH + " boolean default 0");
        db.execSQL(ConstantsDB.PURCHASES.addPaymentForColumn());
        SPHandler.remove(aContext, ConstantsSP.SHARED_KEYS_CONVERSATION_LIST_API_CALL_TIME);
    }

    //@Trace(category = MetricCategory.DATABASE)
    public void migrateToConversationListTable(SQLiteDatabase db) {
        String user_id = Utility.getMyId(aContext);
        Cursor cursor = null;
        String rigidIdKey = ConstantsRF.RIGID_FIELD_CONVERSATION_LIST_DB_MIGRATED;
        if (Utility.isSet(user_id)) {
            rigidIdKey += "_" + user_id;
            if (RFHandler.getBool(db, null,
                    rigidIdKey)) {
                return;
            }
            String messageStatesSent = "'" + MessageState.INCOMING_DELIVERED
                    + "','" + MessageState.INCOMING_READ + "','"
                    + MessageState.OUTGOING_SENT + "','"
                    + MessageState.OUTGOING_DELIVERED + "','"
                    + MessageState.OUTGOING_READ + "','"
                    + MessageState.CONVERSATION_FETCHED + "','"
                    + MessageState.CONVERSATION_FETCHED_NEW + "','"
                    + MessageState.BLOCKED_UNSHOWN + "'";
            String rawQuery = "SELECT meta.match_id, meta.match_id as _id, meta.fname, meta.age, meta.profile_pic, meta.last_fetched_tstamp, meta.message_link, meta.last_message_state, x.tstamp, x.message_text, x.message_type, x.message_state, x.fetch_source, case when meta.last_fetched_tstamp is not null and x.tstamp is not null then max(meta.last_fetched_tstamp, x.tstamp) when meta.last_fetched_tstamp is null then x.tstamp when x.tstamp is null then meta.last_fetched_tstamp else NULL END as max_tstamp FROM " + ConstantsDB.MESSAGE_METADATA_TABLE + " meta LEFT JOIN (SELECT msg2.tstamp, msg2.match_id, msg2.user_id, msg2.message_text, msg2.message_type, msg2.message_state, msg2.fetch_source FROM (SELECT MAX(tstamp) AS tstamp, match_id, user_id FROM " + ConstantsDB.MESSAGES_TABLE + " WHERE message_state IN ("
                    + messageStatesSent
                    + ") and match_id not in ('admin') GROUP BY match_id) msg JOIN " + ConstantsDB.MESSAGES_TABLE + " msg2 ON msg2.tstamp = msg.tstamp AND msg2.match_id = msg.match_id AND msg2.user_id = msg.user_id where message_state IN ("
                    + messageStatesSent
                    + ") group by msg.match_id) x ON meta.match_id = x.match_id AND meta.user_id = x.user_id WHERE meta.user_id = ? and meta.match_id not IN ('admin')  and meta.fname is not null and meta.fname not IN ('null', 'TrulyMadly Admin')  and meta.last_message_state not IN ('BLOCKED_SHOWN') AND max_tstamp IS NOT NULL ORDER BY max_tstamp DESC;";
            cursor = db.rawQuery(rawQuery, new String[]{user_id});
        } else {
            return;
        }
        if (cursor == null) {
            return;
        }
        while (cursor.moveToNext()) {
            String match_id = cursor
                    .getString(cursor.getColumnIndex("match_id"));
            String last_message_text = cursor
                    .getString(cursor.getColumnIndex("message_text"));

            // last_message_type
            MessageType last_message_type;
            String msgType = cursor
                    .getString(cursor.getColumnIndex("message_type"));
            if (Utility.isSet(msgType)) {
                last_message_type = MessageModal
                        .getMessageTypeFromString(msgType);
            } else {
                continue;
            }

            // tstamp and sort_tstamp, same initially
            Date tStamp = null;
            String tstampMessage = cursor
                    .getString(cursor.getColumnIndex("tstamp"));
            String tStampMeta = cursor
                    .getString(cursor.getColumnIndex("last_fetched_tstamp"));
            if (Utility.isSet(tstampMessage)) {
                tStamp = TimeUtils.getParsedTime(tstampMessage);
            } else if (Utility.isSet(tStampMeta)) {
                tStamp = TimeUtils.getParsedTime(tStampMeta);
            } else {
                continue;
            }

            // last_message_state
            MessageState last_message_state;
            String lastMessageStateMeta = cursor
                    .getString(cursor.getColumnIndex("last_message_state"));
            String lastMessageStateMessage = cursor
                    .getString(cursor.getColumnIndex("message_state"));
            if (Utility.isSet(lastMessageStateMessage)) {
                last_message_state = MessageState
                        .valueOf(lastMessageStateMessage);
            } else if (Utility.isSet(lastMessageStateMeta)) {
                last_message_state = MessageState.valueOf(lastMessageStateMeta);
            } else {
                last_message_state = MessageState.MATCH_ONLY_NEW;
            }
            if (Utility.isSet(lastMessageStateMeta) && (MessageState.valueOf(
                    lastMessageStateMeta) == MessageState.BLOCKED_UNSHOWN
                    || MessageState.valueOf(
                    lastMessageStateMeta) == MessageState.BLOCKED_SHOWN)) {
                last_message_state = MessageState.valueOf(lastMessageStateMeta);
            }
            if (!Utility.isSet(last_message_text)
                    && last_message_state != MessageState.MATCH_ONLY_NEW
                    && last_message_state != MessageState.BLOCKED_UNSHOWN) {
                last_message_state = MessageState.MATCH_ONLY;
            }

            // insert into new conversation list table.
            // user_id VARCHAR(100)
            // match_id VARCHAR(100)
            // tstamp varchar(100)
            // sort_tstamp varchar(100)
            // last_message_state VARCHAR(100)
            // last_message_type VARCHAR(100)
            // last_message_text VARCHAR(5024)

            ContentValues values = new ContentValues();
            values.put("user_id", user_id);
            values.put("match_id", match_id);
            values.put("tstamp", TimeUtils.getFormattedTime(tStamp));
            values.put("sort_tstamp", TimeUtils.getFormattedTime(tStamp));
            values.put("last_message_type", last_message_type.toString());
            values.put("last_message_state", last_message_state.toString());
            values.put("last_message_text", last_message_text);
            db.insert(ConstantsDB.CONVERSATION_LIST_TABLE, null, values);
        }
        cursor.close();
        // TODO: DATABASE: On Migrate success.
        RFHandler.insert(db, null, rigidIdKey, true);
        // delete all messages from "messages" with state conversation_fetched
        // and conversation_fetched_new
        // delete all messages from "messages" with state match_only and
        // match_only_new
        // Columns to remove
        // " + ConstantsDB.MESSAGE_METADATA + " -> last_message_state
        // messages -> msg_json
        // messages -> sent_success
        // sticker -> cached_time

    }

    //@Trace(category = MetricCategory.DATABASE)
    private void migrateExistingMessagesTable(SQLiteDatabase db) {
        // http://stackoverflow.com/questions/5707348/how-to-use-rawquery-in-android
        Cursor cursor = db.query(ConstantsDB.MESSAGES_TABLE,
                new String[]{"user_id", "message_id", "msg_json",
                        "showsending", "sent_success"},
                null, null, null, null, null);
        while (cursor.moveToNext()) {
            String msg_json = cursor
                    .getString(cursor.getColumnIndex("msg_json"));
            String user_id = cursor.getString(cursor.getColumnIndex("user_id"));
            String message_id = cursor
                    .getString(cursor.getColumnIndex("message_id"));
            boolean showSending = cursor
                    .getInt(cursor.getColumnIndex("showsending")) == 1;
            boolean sent_success = cursor
                    .getInt(cursor.getColumnIndex("sent_success")) == 1;
            MessageState message_state;
            if (!Utility.isSet(msg_json)) {
                message_state = MessageState.GCM_FETCHED;
                ContentValues values = new ContentValues();
                values.put("message_state", message_state.toString());
                db.update(ConstantsDB.MESSAGES_TABLE, values, "message_id=?",
                        new String[]{message_id});
            } else {
                JSONObject jsonObj;
                try {
                    jsonObj = new JSONObject(msg_json);
                    String msg = jsonObj.optString("msg");
                    String sender_id = jsonObj.optString("sender_id");
                    Boolean is_message_sent_by_me = true;
                    if (Utility.isSet(user_id) && Utility.isSet(sender_id))
                        is_message_sent_by_me = user_id.equals(sender_id);
                    MessageType message_type = MessageModal
                            .getMessageTypeFromString(
                                    jsonObj.optString("message_type"));
                    if (is_message_sent_by_me) {
                        if (showSending) {
                            message_state = MessageState.OUTGOING_SENDING;
                        } else if (sent_success) {
                            message_state = MessageState.OUTGOING_SENT;
                        } else {
                            message_state = MessageState.OUTGOING_FAILED;
                        }
                    } else {
                        message_state = MessageState.INCOMING_READ;
                    }
                    ContentValues values = new ContentValues();
                    values.put("message_state", message_state.toString());
                    values.put("message_type", message_type.toString());
                    values.put("message_text", msg);
                    db.update(ConstantsDB.MESSAGES_TABLE, values, "message_id=?",
                            new String[]{message_id});
                } catch (JSONException ignored) {
                }
            }

        }
        cursor.close();
    }

    //@Trace(category = MetricCategory.DATABASE)
    private void migrateExistingMessagesTableForImages(SQLiteDatabase db) {
        String ignoreMessageStates = "'" + MessageState.GCM_FETCHED.toString()
                + "','" + MessageState.CONVERSATION_FETCHED.toString() + "','"
                + MessageState.CONVERSATION_FETCHED_NEW.toString() + "','"
                + MessageState.BLOCKED_SHOWN.toString() + "'";

        String user_id = Utility.getMyId(aContext);
        Cursor cursor = null;
        if (Utility.isSet(user_id)) {
            cursor = db.rawQuery(
                    "SELECT *, message_id as _id FROM " + ConstantsDB.MESSAGES_TABLE +
                            " where user_id=? and message_type = ? and message_state NOT IN ("
                            + ignoreMessageStates + ") " +
                            " and msg_json LIKE '%\"message_type\":\"IMAGE\"%' " +
                            "order by tstamp asc;",
                    new String[]{user_id, MessageType.TEXT.name()});
        }

        if (cursor != null) {
            while (cursor.moveToNext()) {
                String messageJsonMsg = cursor.getString(cursor.getColumnIndex("msg_json"));
                if (Utility.isSet(messageJsonMsg)) {
                    try {
                        MessageModal messageModal = new MessageModal(user_id, new JSONObject(messageJsonMsg));
                        if (messageModal.getMessageType() == MessageType.IMAGE) {
                            MessageDBHandler.updateAMessage(db, user_id, messageModal,
                                    messageModal.getMsg_id(),
                                    FetchSource.valueOf(cursor.getString(cursor.getColumnIndex("fetch_source"))));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            cursor.close();
        }
    }

    //@Trace(category = MetricCategory.DATABASE)
    private void migrateExistingMessagesTableForShareEvents(SQLiteDatabase db) {
        String ignoreMessageStates = "'" + MessageState.GCM_FETCHED.toString()
                + "','" + MessageState.CONVERSATION_FETCHED.toString() + "','"
                + MessageState.CONVERSATION_FETCHED_NEW.toString() + "','"
                + MessageState.BLOCKED_SHOWN.toString() + "'";

        String user_id = Utility.getMyId(aContext);
        Cursor cursor = null;
        if (Utility.isSet(user_id)) {
            cursor = db.rawQuery(
                    "SELECT *, message_id as _id FROM " + ConstantsDB.MESSAGES_TABLE +
                            " where user_id=? and message_type = ? and message_state NOT IN ("
                            + ignoreMessageStates + ") " +
                            " and msg_json LIKE '%\"message_type\":\"SHARE_EVENT\"%' " +
                            "order by tstamp asc;",
                    new String[]{user_id, MessageType.TEXT.name()});
        }

        if (cursor != null) {
            while (cursor.moveToNext()) {
                String messageJsonMsg = cursor.getString(cursor.getColumnIndex("msg_json"));
                if (Utility.isSet(messageJsonMsg)) {
                    try {
                        MessageModal messageModal = new MessageModal(user_id, new JSONObject(messageJsonMsg));
                        if (messageModal.getMessageType() == MessageType.SHARE_EVENT) {
                            MessageDBHandler.updateAMessage(db, user_id, messageModal,
                                    messageModal.getMsg_id(),
                                    FetchSource.valueOf(cursor.getString(cursor.getColumnIndex("fetch_source"))));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            cursor.close();
        }
    }

    private void updateMessage(SQLiteDatabase db, String user_id,
                               MessageModal messageObj, String random_msg_id,
                               FetchSource fetch_source) {
        ContentValues values = new ContentValues();
        values.put("user_id", user_id);
        values.put("match_id", messageObj.getMatch_id());
        values.put("message_id", messageObj.getMsg_id());
        values.put("tstamp", messageObj.getTstamp());
        values.put("fetch_source", fetch_source.toString());
        values.put("message_type", messageObj.getMessageType().toString());
        values.put("message_state", messageObj.getMessageState().toString());
        values.put("message_text", messageObj.getMessage());
        values.put(Constants.CD_KEY_DEAL_ID, messageObj.getmDealId());
        values.put(Constants.CD_KEY_DATESPOT_ID, messageObj.getmDateSpotId());
        if (Utility.isSet(messageObj.getMsgJsonString())) {
            values.put("msg_json", messageObj.getMsgJsonString());
        }
        String whereClause = "user_id=? and match_id=? and message_id=?";
        String[] whereArgs = new String[]{user_id, messageObj.getMatch_id(),
                random_msg_id};
        db.update(ConstantsDB.MESSAGES_TABLE, values, whereClause, whereArgs);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        TmLogger.w(TrulyMadlySQLiteHandler.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion);

        if (newVersion == 3 || oldVersion < 3) {
            upgradeTo3(db);
        }
        if (newVersion == 7 || oldVersion < 7) {
            upgradeTo7(db);
        }
        if (newVersion == 8 || oldVersion < 8) {
            upgradeTo8(db);
        }
        if (newVersion == 10 || oldVersion < 10) {
            upgradeTo10(db);
        }

        if (newVersion == 11 || oldVersion < 11) {
            upgradeTo11(db);
        }

        if (newVersion == 12 || oldVersion < 12) {
            upgradeTo12(db);
        }

        if (newVersion == 13 || oldVersion < 13) {
            upgradeTo13(db);
        }

        if (newVersion == 14 || oldVersion < 14) {
            upgradeTo14(db);
        }

        if (newVersion == 15 || oldVersion < 15) {
            upgradeTo15(db);
        }

        if (newVersion == 22 || oldVersion < 22) {
            upgradeTo22(db);
        }

        if (newVersion == 23 || oldVersion < 23) {
            upgradeTo23(db);
        }

        if (newVersion == 24 || oldVersion < 24) {
            upgradeTo24(db);
        }

        if (newVersion == 25 || oldVersion < 25) {
            upgradeTo25(db);
        }

        if (newVersion == 26 || oldVersion < 26) {
            upgradeTo26();
        }

        if (newVersion == 30 || oldVersion < 30) {
            upgradeTo30(db);
        }

        if (newVersion == 31 || oldVersion < 31) {
            upgradeTo31(db);
        }

        if (newVersion == 32 || oldVersion < 32) {
            upgradeTo32(db);
        }

        if (newVersion == 33 || oldVersion < 33) {
            upgradeTo33(db);
        }

        if (newVersion == 34 || oldVersion < 34) {
            upgradeTo34(db);
        }

        if (newVersion == 35 || oldVersion < 35) {
            upgradeTo35(db);
        }

        if (newVersion == 36 || oldVersion < 36) {
            upgradeTo36(db);
        }
        if (newVersion == 37 || oldVersion < 37) {
            upgradeTo37();
        }

        if (newVersion == 38 || oldVersion < 38) {
            upgradeTo38(db);
        }

        if (newVersion == 39 || oldVersion < 39) {
            upgradeTo39(db);
        }

        if (newVersion == 40 || oldVersion < 40) {
            upgradeTo40(db);
        }

        if (newVersion == 41 || oldVersion < 41) {
            upgradeTo41(db);
        }

        if (newVersion == 42 || oldVersion < 42) {
            upgradeTo42(db);
        }

        if (newVersion == 43 || oldVersion < 43) {
            upgradeTo43(db);
        }

        if (newVersion == 44 || oldVersion < 44) {
            upgradeTo44(db);
        }

        if (newVersion == 45 || oldVersion < 45) {
            upgradeTo45(db);
        }

        if (newVersion == 46 || oldVersion < 46) {
            upgradeTo46(db);
        }
    }

    @Override
    public SQLiteDatabase getWritableDatabase() {
        if (singletonSQLiteWritableDatabase == null
                || !singletonSQLiteWritableDatabase.isOpen()) {
            synchronized (TrulyMadlySQLiteHandler.class) {
                try {
                    if (singletonSQLiteWritableDatabase == null
                            || !singletonSQLiteWritableDatabase.isOpen()) {
                        singletonSQLiteWritableDatabase = super.getWritableDatabase();
                    }
                } catch (SQLException | IllegalStateException ignored) {
                }
            }

        }
        return singletonSQLiteWritableDatabase;
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }


}
