package com.trulymadly.android.app.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.trulymadly.android.Exception.DbIsNullException;
import com.trulymadly.android.app.utility.Utility;

public class RFHandler {

    // added for rigid fields

    //@Trace(category = MetricCategory.DATABASE)
    public static void insert(Context ctx, String userId, String rigidIdKey, long value) {
        ContentValues values = new ContentValues();
        values.put("ID", rigidIdKey);
        if (userId != null)
            values.put("USER_ID", userId);
        values.put("VALUE", value + "");
        try {
            TrulyMadlySQLiteHandler.getDatabase(ctx).replace("RIGID_FIELDS", null, values);
        } catch (DbIsNullException ignored) {
        }
    }


    public static void insert(Context ctx, String userId, String rigidIdKey, int value) {
        ContentValues values = new ContentValues();
        values.put("ID", rigidIdKey);
        if (userId != null)
            values.put("USER_ID", userId);
        values.put("VALUE", value + "");
        try {
            TrulyMadlySQLiteHandler.getDatabase(ctx).replace("RIGID_FIELDS", null, values);
        } catch (DbIsNullException ignored) {
        }
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static void insert(Context ctx, String userId, String rigidIdKey, String value) {
        ContentValues values = new ContentValues();
        values.put("ID", rigidIdKey);
        if (userId != null)
            values.put("USER_ID", userId);
        values.put("VALUE", value);
        try {
            TrulyMadlySQLiteHandler.getDatabase(ctx).replace("RIGID_FIELDS", null, values);
        } catch (DbIsNullException ignored) {
        }
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static void insert(Context ctx, String userId, String rigidIdKey, boolean isSet) {
        try {
            insert(TrulyMadlySQLiteHandler.getDatabase(ctx), userId, rigidIdKey, isSet);
        } catch (DbIsNullException ignored) {
        }
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static void insert(Context ctx, String rigidIdKey, boolean isSet) {
        try {
            insert(TrulyMadlySQLiteHandler.getDatabase(ctx), null, rigidIdKey, isSet);
        } catch (DbIsNullException ignored) {
        }
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static void insert(SQLiteDatabase db, String userId, String rigidIdKey, boolean isSet) {
        if (db == null) {
            return;
        }
        ContentValues values = new ContentValues();
        values.put("ID", rigidIdKey);
        if (userId != null)
            values.put("USER_ID", userId);
        values.put("IS_SET", isSet ? "1" : "0");
        db.replace("RIGID_FIELDS", null, values);
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static boolean getBool(Context ctx, String rigidIdKey) {
        return getBool(ctx, null, rigidIdKey);
    }

    public static boolean getBool(Context ctx, String userId, String rigidIdKey) {
        try {
            return getBool(TrulyMadlySQLiteHandler.getDatabase(ctx), userId, rigidIdKey);
        } catch (DbIsNullException e) {
            return false;
        }
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static boolean getBool(SQLiteDatabase db, String userId, String rigidIdKey) {
        if (db == null) {
            return false;
        }
        String query = "SELECT IS_SET FROM RIGID_FIELDS WHERE ID=?";
        String[] arguments = null;
        if (userId != null) {
            query += " AND USER_ID=?";
            arguments = new String[]{rigidIdKey, userId};
        } else {
            arguments = new String[]{rigidIdKey};
        }

        String isSetFlag = null;
        Cursor cursor = db.rawQuery(query, arguments);
        try {
            if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
                isSetFlag = cursor.getString(cursor.getColumnIndex("IS_SET"));
            }
        } catch (RuntimeException ignored) {
        }
        if (cursor != null)
            cursor.close();
        return Utility.stringCompare(isSetFlag, "1");
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static long getLong(Context ctx, String userId, String rigidIdKey, long defaultValue) {
        String value = getString(ctx, userId, rigidIdKey);
        if (!Utility.isSet(value))
            return defaultValue;
        else
            return Long.parseLong(value);
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static int getInt(Context ctx, String userId, String rigidIdKey, int defaultValue) {
        String value = getString(ctx, userId, rigidIdKey);
        if (!Utility.isSet(value))
            return defaultValue;
        else
            return Integer.parseInt(value);
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static int getInt(Context ctx, String rigidIdKey, int defaultValue) {
        String value = getString(ctx, rigidIdKey);
        if (!Utility.isSet(value))
            return defaultValue;
        else
            return Integer.parseInt(value);
    }

    //@Trace(category = MetricCategory.DATABASE)
    private static String getString(Context ctx, String rigidIdKey) {
        return getString(ctx, null, rigidIdKey);
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static String getString(Context ctx, String userId, String rigidIdKey) {

        String query = "SELECT VALUE FROM RIGID_FIELDS WHERE ID=?";
        String[] arguments = null;
        if (Utility.isSet(userId)) {
            query += " AND USER_ID=?";
            arguments = new String[]{rigidIdKey, userId};
        } else {
            arguments = new String[]{rigidIdKey};
        }

        Cursor cursor = null;
        try {
            cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).rawQuery(query, arguments);
        } catch (DbIsNullException e) {
            return null;
        }

        String value = null;

        try {
            if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
                value = cursor.getString(cursor.getColumnIndex("VALUE"));
            }
        } catch (RuntimeException ignored) {
        }

        if (cursor != null)
            cursor.close();

        if (Utility.isSet(value))
            return value;
        else
            return null;

    }

}
