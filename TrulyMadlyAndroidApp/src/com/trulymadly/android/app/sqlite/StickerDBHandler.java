package com.trulymadly.android.app.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.crashlytics.android.Crashlytics;
import com.trulymadly.android.Exception.DbIsNullException;
import com.trulymadly.android.app.modal.StickerData;
import com.trulymadly.android.app.utility.FilesHandler;
import com.trulymadly.android.app.utility.ImageCacheHelper;
import com.trulymadly.android.app.utility.Utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

public class StickerDBHandler {

//    public static void createStickerBackupTable(SQLiteDatabase db, Context context) {
//        db.execSQL(
//                "CREATE TABLE IF NOT EXISTS stickers_backup (id int  PRIMARY KEY  ,last_used DATETIME, gallery_id int, type varchar(10));");
//    }
//@Trace(category = MetricCategory.DATABASE)
public static void deleteTable(SQLiteDatabase db, String tableName) {
        db.execSQL("Delete from " + tableName);

    }

//    public static void copyDataFromBackUp(SQLiteDatabase db, Context context, String tableName) {
//        db.execSQL("INSERT INTO stickers SELECT id, last_used,gallery_id,type FROM stickers_backup");
//
//    }

    //@Trace(category = MetricCategory.DATABASE)
    public static boolean isStickerExists(String id, Context ctx) {
        boolean isStickerExists = false;
        String selection = "id=?";
        String[] selectionArgs = new String[]{id};
        Cursor cursor = null;
        try {
            cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).query("stickers", null, selection,
                    selectionArgs, null, null, null, "1");
        } catch (DbIsNullException e) {
            return false;
        }
        if (cursor != null && cursor.getCount() > 0) {
            isStickerExists = true;
        }
        if (cursor != null)
            cursor.close();
        return isStickerExists;
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static HashMap<Integer, Integer> newStickerMap(Context ctx) {

        HashMap<Integer, Integer> stickerMap = new HashMap<>();
        String[] columns = new String[]{"id"};
        Cursor cursor = null;
        try {
            cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).query("stickers", columns, null,
                    null, null, null, null, null);
        } catch (DbIsNullException e) {
            Crashlytics.logException(e);

        }
        if (cursor != null && cursor.getCount() > 0) {

            while (cursor.moveToNext()) {
                int id = cursor.getInt(cursor.getColumnIndex("id"));
                stickerMap.put(id, 1);
            }

        }
        if (cursor != null)
            cursor.close();


        return stickerMap;
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static void updateLatestTimeStamp(ArrayList<StickerData> stickers, Context aContext) {
        if (stickers == null || stickers.size() == 0) {
            return;
        }
        String sql = "UPDATE stickers SET last_used = ? where id=?;";
        SQLiteDatabase db;
        try {
            db = TrulyMadlySQLiteHandler.getDatabase(aContext);
        } catch (DbIsNullException e) {
            return;
        }
        SQLiteStatement statement = db.compileStatement(sql);
        db.beginTransaction();
        for (StickerData sticker : stickers) {
            statement.clearBindings();
            statement.bindString(1, sticker.getLastUsed());
            statement.bindString(2, "" + sticker.getId());
            statement.execute();
        }
        db.setTransactionSuccessful();
        db.endTransaction();

    }

    //@Trace(category = MetricCategory.DATABASE)
    public static boolean insertSticker(StickerData sticker, Context ctx) {
        if (sticker.getId() > 0 && Utility.isSet(sticker.getType())) {
            ContentValues values = new ContentValues();
            values.put("id", sticker.getId());
            values.put("gallery_id", sticker.getGalleryId());
            values.put("type", sticker.getType());
            values.put("last_used", sticker.getLastUsed());
            try {
                TrulyMadlySQLiteHandler.getDatabase(ctx).replace("stickers", null, values);
            } catch (DbIsNullException e) {
                return false;
            }
        }
        return true;

    }

    //@Trace(category = MetricCategory.DATABASE)
    public static void updateAttribute(StickerData s, String attributeName,
                                       String attributeValue, Context ctx) {
        ContentValues values = new ContentValues();
        values.put(attributeName, attributeValue);
        String whereClause = "id=?";
        String[] whereArgs = new String[]{"" + s.getId()};
        try {
            TrulyMadlySQLiteHandler.getDatabase(ctx).update("stickers", values, whereClause, whereArgs);
        } catch (DbIsNullException ignored) {
        }

    }

    //@Trace(category = MetricCategory.DATABASE)
    public static LinkedHashMap<StickerData, ArrayList<StickerData>> createHashMap(
            Context ctx) {

        LinkedHashMap<Integer, StickerData> hmapGalleries = new LinkedHashMap<>();
        LinkedHashMap<StickerData, ArrayList<StickerData>> hmapSticker = new LinkedHashMap<>();


        Cursor cursor = null;
        try {
            cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).query("stickers", null, null,
                    null, null, null, null);
        } catch (DbIsNullException e) {
            return hmapSticker;
        }
        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                StickerData sticker = new StickerData(cursor);
                if (sticker.getType().equals("gallery")) {
                    hmapGalleries.put(sticker.getId(), sticker);
                    hmapSticker.put(sticker, new ArrayList<StickerData>());
                } else {
                    StickerData gallery = hmapGalleries.get(sticker
                            .getGalleryId());
                    hmapSticker.get(gallery).add(sticker);
                }
            }
        }
        if (cursor != null) {
            cursor.close();
        }
        return hmapSticker;
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static LinkedHashMap<Integer, StickerData> getGalleries(
            HashMap<StickerData, ArrayList<StickerData>> hmapSticker) {
        LinkedHashMap<Integer, StickerData> hmapGalleries = new LinkedHashMap<>();
        for (Entry<StickerData, ArrayList<StickerData>> pair : hmapSticker
                .entrySet()) {
            StickerData gallery = pair.getKey();
            if (!hmapGalleries.containsKey(gallery.getId())) {
                hmapGalleries.put(gallery.getId(), gallery);
            }
        }
        return hmapGalleries;
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static ArrayList<StickerData> createRecentUsedStickers(
            Context aContext) {
        ArrayList<StickerData> arr = new ArrayList<>();
        String selection = "last_used IS NOT NULL";

        Cursor cursor = null;
        try {
            cursor = TrulyMadlySQLiteHandler.getDatabase(aContext).query("stickers", null,
                    selection, null, null, null, "last_used  DESC", "12");
        } catch (DbIsNullException e) {
            return arr;
        }
        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                StickerData sticker = new StickerData(cursor);

                arr.add(sticker);

            }
        }
        if (cursor != null) {
            cursor.close();
        }
        return arr;
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static void downloadAll(Context ctx) {
        Cursor cursor = null;
        try {
            cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).query("stickers", null, null, null,
                    null, null, null);
        } catch (DbIsNullException e) {
            return;
        }
        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                StickerData sticker = new StickerData(cursor);
                String url = FilesHandler.createStickerUrl(sticker, "thumbnail");
                String stickerKey = FilesHandler.getStickerKey(url);
                ImageCacheHelper.with(ctx).loadWithKey(url, stickerKey).fetch();
            }
        }
        if (cursor != null)
            cursor.close();
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static LinkedHashMap<Integer, Integer> returnExistingGalleries(Context ctx) {

        LinkedHashMap<Integer, Integer> hmapGalleries = new LinkedHashMap<>();
        Cursor cursor = null;

        String whereClause = "type=?";
        String[] whereArgs = new String[]{"gallery"};
        String[] columns = new String[]{"id"};
        try {
            cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).query("stickers", columns, whereClause,
                    whereArgs, null, null, null);
        } catch (DbIsNullException e) {
            return hmapGalleries;
        }
        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {

                hmapGalleries.put(cursor.getInt(cursor.getColumnIndex("id")), 1);
            }
        }

        if (cursor != null) {
            cursor.close();
        }
        return hmapGalleries;
    }


}
