package com.trulymadly.android.app.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.trulymadly.android.Exception.DbIsNullException;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;


public class CachingDBHandler {


    //@Trace(category = MetricCategory.DATABASE)
    public static JSONObject getResponse(Context ctx, String url, String user_id) {

        if (!Utility.isSet(url) || !Utility.isSet(user_id)) {
            return null;
        }
        String json = null;
        String selection = "url=? and user_id=?";
        String columns[] = new String[]{"response"};
        String[] selectionArgs = new String[]{url, user_id};
        Cursor cursor = null;

        try {
            cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).query("cached_data", columns, selection,
                    selectionArgs, null, null, null, "1");
        } catch (DbIsNullException e) {
            return null;
        }

        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                json = cursor.getString(cursor.getColumnIndex("response"));
            }

            cursor.close();
        }

        if (Utility.isSet(json)) {
            try {
                return new JSONObject(json);
            } catch (JSONException e) {
                return null;
            }
        } else
            return null;


    }

    //@Trace(category = MetricCategory.DATABASE)
    public static String getHashValue(Context ctx, String url, String user_id) {
        if (!Utility.isSet(url) || !Utility.isSet(user_id)) {
            return null;
        }

        String hashValue = null;
        String selection = "url=? and user_id=?";
        String[] selectionArgs = new String[]{url, user_id};
        String columns[] = new String[]{"hash_value"};
        Cursor cursor = null;
        try {
            cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).query("cached_data", columns, selection,
                    selectionArgs, null, null, null, "1");
        } catch (DbIsNullException e) {
            return null;
        }
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            hashValue = cursor.getString(cursor.getColumnIndex("hash_value"));
        }
        if (cursor != null)
            cursor.close();

        return hashValue;


    }

    //@Trace(category = MetricCategory.DATABASE)
    public static void insertResponse(Context ctx, String url, String hashValue, String response, String user_id, String tstamp) {

        if (Utility.isSet(url) && Utility.isSet(hashValue) && Utility.isSet(response) && Utility.isSet(user_id)
                ) {
            ContentValues values = new ContentValues();

            values.put("url", url);
            values.put("hash_value", hashValue);
            values.put("response", response);
            values.put("user_id", user_id);
            values.put("last_updated_tstamp", tstamp);

            try {
                TrulyMadlySQLiteHandler.getDatabase(ctx).replace("cached_data", null, values);
            } catch (DbIsNullException ignored) {
            }
        }

    }

    //@Trace(category = MetricCategory.DATABASE)
    public static void insertTstamp(Context ctx, String url, String user_id, String tstamp) {

        if (Utility.isSet(url) && Utility.isSet(user_id)) {
            ContentValues values = new ContentValues();

            values.put("last_updated_tstamp", tstamp);

            String whereArgs[] = new String[]{url, user_id};
            String whereClause = "url=? and user_id=?";

            try {
                TrulyMadlySQLiteHandler.getDatabase(ctx).update("cached_data", values, whereClause, whereArgs);
            } catch (DbIsNullException ignored) {
            }
        }

    }

    //@Trace(category = MetricCategory.DATABASE)
    public static void deleteURL(Context ctx, String url, String user_id) {
        if (Utility.isSet(url) && Utility.isSet(user_id)) {
            String whereClause = "url=? and user_id=?";
            String[] whereArgs = new String[]{url, user_id};
            try {
                TrulyMadlySQLiteHandler.getDatabase(ctx).delete("cached_data", whereClause, whereArgs);
            } catch (DbIsNullException ignored) {

            }

        }
    }

    public static void clearAllData(Context aContext) {
        String whereClause = "user_id=?";
        String[] whereArgs = new String[]{Utility.getMyId(aContext)};
        try {
            TrulyMadlySQLiteHandler.getDatabase(aContext).delete("cached_data",
                    whereClause, whereArgs);
        } catch (DbIsNullException e) {
        }
    }
}
