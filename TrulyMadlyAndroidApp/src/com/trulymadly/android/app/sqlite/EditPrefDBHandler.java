package com.trulymadly.android.app.sqlite;

import android.content.Context;

import com.crashlytics.android.Crashlytics;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.json.EditPartnerPrefBasicDataResponseParser;
import com.trulymadly.android.app.modal.EditPrefBasicDataModal;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by udbhav on 10/11/15.
 */
public class EditPrefDBHandler {
    private static void clearEditPrefBasicData(Context aContext) {
        DatabaseAccessNew dba = new DatabaseAccessNew(aContext);
        try {
            dba.deleteEditPref();
        } catch (Exception ignored) {
        }
    }

    // fetching basic data for Edit Pref and inserting into db
    //@Trace(category = MetricCategory.DATABASE)
    private static void insertBasicData(String id, JSONArray data, Context aContext) {
        if (data != null) {
            DatabaseAccessNew dba = new DatabaseAccessNew(aContext);
            try {
                dba.insertEditPrefBasicData(id, data.toString());
            } catch (Exception e) {
                Crashlytics.logException(e);
            }
        }
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static void insertBasicDataInDB(JSONObject response,
                                           String urlCalled, Context aContext) {

        clearEditPrefBasicData(aContext);

        insertBasicData(Constants.EDIT_PREF_INCOME,
                response.optJSONArray("income"), aContext);
        insertBasicData(Constants.EDIT_PREF_COUNTRY,
                response.optJSONArray("countries"), aContext);
        insertBasicData(Constants.EDIT_PREF_STATE,
                response.optJSONArray("states"), aContext);
        insertBasicData(Constants.EDIT_PREF_CITY,
                response.optJSONArray("cities"), aContext);
        insertBasicData(Constants.EDIT_PREF_DEGREE,
                response.optJSONArray("highest_degree"), aContext);
        insertBasicData(Constants.EDIT_PROFILE_HASTAG,
                response.optJSONArray("interest_new"), aContext);
        insertBasicData(Constants.EDIT_PROFILE_INDUSTRY,
                response.optJSONArray("industries"), aContext);
        try {
            SPHandler.setString(aContext, ConstantsSP.SHARED_KEYS_BACHELORS,
                    response.getJSONObject("bachelors").optString("key"));
            SPHandler.setString(aContext, ConstantsSP.SHARED_KEYS_MASTERS,
                    response.getJSONObject("masters").optString("key"));
        } catch (JSONException e) {
            Crashlytics.logException(e);
        }
        SPHandler.setString(aContext,
                ConstantsSP.SHARED_KEYS_EDIT_BASIC_DATA_URL, urlCalled);

    }

    //@Trace(category = MetricCategory.DATABASE)
    public static ArrayList<EditPrefBasicDataModal> getBasicDataFromDB(
            String id, Context aContext, int n) {

        ArrayList<EditPrefBasicDataModal> editPrefBasicDataList = null;

        DatabaseAccessNew dba = new DatabaseAccessNew(aContext);
        // dba.open();
        String editPrefBasicDataJsonResponse = dba.getEditPrefBasicData(id);

        editPrefBasicDataList = EditPartnerPrefBasicDataResponseParser
                .getEditPrefBasicDataList(editPrefBasicDataJsonResponse, n);
        if (editPrefBasicDataList != null && editPrefBasicDataList.size() > 0) {
            return editPrefBasicDataList;
        }
        return null;
    }

    /**
     * Returns url is data is not saved. Otherwise return basic data url.
     *
     * @param url      url for basic data
     * @param aContext
     * @return
     */
    //@Trace(category = MetricCategory.DATABASE)
    public static String checkBasicDataExists(String url, Context aContext) {
        if (Utility.isSet(url)) {
            String basicDataOld = SPHandler.getString(aContext,
                    ConstantsSP.SHARED_KEYS_EDIT_BASIC_DATA_URL);
            if (Utility.isSet(basicDataOld) && url.equals(basicDataOld)) {
                return null;
            }
        } else {
            url = ConstantsUrls.get_basic_data_url_default();
        }
        return url;
    }
}
