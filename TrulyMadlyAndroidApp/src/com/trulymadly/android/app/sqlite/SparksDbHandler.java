package com.trulymadly.android.app.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.trulymadly.android.Exception.DbIsNullException;
import com.trulymadly.android.app.MessageModal;
import com.trulymadly.android.app.billing.PaymentFor;
import com.trulymadly.android.app.billing.PaymentMode;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.ConstantsDB;
import com.trulymadly.android.app.json.ConstantsDB.PURCHASES;
import com.trulymadly.android.app.json.ConstantsDB.PURCHASES.PurchaseState;
import com.trulymadly.android.app.json.ConstantsDB.SPARKS;
import com.trulymadly.android.app.modal.MatchMessageMetaData;
import com.trulymadly.android.app.modal.PurchaseModal;
import com.trulymadly.android.app.modal.SparkModal;
import com.trulymadly.android.app.utility.TimeUtils;
import com.trulymadly.android.app.utility.Utility;

import java.util.ArrayList;
import java.util.Date;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.trulymadly.android.app.json.ConstantsDB.PURCHASES_TABLE;

/**
 * Created by avin on 10/05/16.
 */
public class SparksDbHandler {
    /**
     * Inserts a new spark into the sparks table
     *
     * @param ctx
     * @param sparkModal
     * @param matchMessageMetaData
     */
    public static void insertSpark(Context ctx, SparkModal sparkModal, MatchMessageMetaData matchMessageMetaData) {
        ContentValues values = new ContentValues();
        values.put(SPARKS.USER_ID, sparkModal.getmUserId());
        values.put(SPARKS.MATCH_ID, sparkModal.getmMatchId());
        values.put(SPARKS.DESIGNATION, sparkModal.getmDesignation());
        values.put(SPARKS.MESSAGE_ID, sparkModal.getmMessageId());
        values.put(SPARKS.MESSAGE, sparkModal.getmMessage());
        values.put(SPARKS.TIMESTAMP, sparkModal.getmTimeStamp());
        values.put(SPARKS.SORT_TIMESTAMP, sparkModal.getmSortTimeStamp());
        values.put(SPARKS.EXPIRE_TIME, sparkModal.getmExpiredTimeInSeconds());
        values.put(SPARKS.START_TIME, sparkModal.getmStartTime());
        values.put(SPARKS.CITY, sparkModal.getmCity());
        values.put(SPARKS.IMAGES, sparkModal.getmImages());
        values.put(SPARKS.HASH, sparkModal.getmHashKey());
        try {
            TrulyMadlySQLiteHandler.getDatabase(ctx).insert(ConstantsDB.SPARKS_TABLE, null, values);
            MessageDBHandler.insertMessageDetails(matchMessageMetaData, ctx);
            if (Utility.isSet(sparkModal.getmMessageId())) {
                MessageModal messageModal = new MessageModal(Utility.getMyId(ctx));
                messageModal.parseMessage(sparkModal.getmMessageId(), sparkModal.getmMessage(),
                        TimeUtils.getFormattedTime(new Date(Long.parseLong(sparkModal.getmTimeStamp()))),
                        MessageModal.MessageType.SPARK.name(), sparkModal.getmMatchId(),
                        Constants.MessageState.INCOMING_DELIVERED, 0, null, null);
                MessageDBHandler.insertAMessage(ctx, Utility.getMyId(ctx), messageModal, true, Constants.FetchSource.LOCAL, false);
            }
        } catch (DbIsNullException ignored) {
        }
    }

    public static void insertSparks(Context ctx, String userId, ArrayList<SparkModal> sparkModals) {
        for (SparkModal sparkModal : sparkModals) {
            insertSpark(ctx, checkNotNull(sparkModal), sparkModal.getmMatchMessageMetaData());
        }
    }

    public static int removePendingSparks(Context ctx, String userId) {
        String sparksWhereClause = SPARKS.USER_ID + "=? and " +
//                ConstantsDB.SPARKS.MATCH_ID + "!=? and " +
                SPARKS.START_TIME + "=? ;";
        String[] sparksWhereArgs = new String[]{userId, "0"};

        SparkModal sparkModal = getTopPendingSpark(ctx, userId);
        String metadataWhereClause = SPARKS.USER_ID + "=? and " +
                ConstantsDB.MESSAGE_METADATA.IS_SPARK_RECEIVED + "=? and " +
                ((sparkModal != null) ? (ConstantsDB.MESSAGE_METADATA.MATCH_ID + "!=? and ") : "") +
                ConstantsDB.MESSAGE_METADATA.IS_MUTUAL_SPARK + "=?;";
        String[] metadataWhereArgs;
        if (sparkModal != null) {
            metadataWhereArgs = new String[]{userId, "1", "0", sparkModal.getmMatchId()};
        } else {
            metadataWhereArgs = new String[]{userId, "1", "0"};
        }
        int rowsDeleted = 0;
        try {
            rowsDeleted = TrulyMadlySQLiteHandler.getDatabase(ctx).delete(ConstantsDB.SPARKS_TABLE, sparksWhereClause, sparksWhereArgs);
            TrulyMadlySQLiteHandler.getDatabase(ctx).delete(ConstantsDB.MESSAGE_METADATA_TABLE, metadataWhereClause, metadataWhereArgs);
        } catch (DbIsNullException ignored) {
        }

        return rowsDeleted;
    }

    public static void removeSpark(Context ctx, String userId, String matchId) {
        String whereClause = SPARKS.USER_ID + "=? and " + SPARKS.MATCH_ID + "=?";
        String[] whereArgs = new String[]{userId, matchId};
        try {
            TrulyMadlySQLiteHandler.getDatabase(ctx).delete(ConstantsDB.SPARKS_TABLE, whereClause, whereArgs);
            TrulyMadlySQLiteHandler.getDatabase(ctx).delete(ConstantsDB.MESSAGE_METADATA_TABLE, whereClause, whereArgs);
        } catch (DbIsNullException ignored) {
        }
    }

    public static SparkModal getNextSpark(Context ctx, String userId) {
        SparkModal sparkModal = getTopPendingSpark(ctx, userId);
        if (sparkModal == null) {
            sparkModal = getTopSpark(ctx, userId);
        }
        return sparkModal;
    }

    public static SparkModal getTopPendingSpark(Context ctx, String userId) {
        SparkModal sparkModal = null;
        if (Utility.isSet(userId)) {
            Cursor cursor;
            try {
                cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).rawQuery(
                        "SELECT * FROM " + ConstantsDB.SPARKS_TABLE +
                                " where " + SPARKS.USER_ID + "=? " + " " +
                                " and " + SPARKS.START_TIME + "!=?" +
                                " ORDER BY rowid " +
//                                "order by " + ConstantsDB.SPARKS.SORT_TIMESTAMP + " asc " +
                                "LIMIT 1;",
                        new String[]{userId, "0"});

            } catch (DbIsNullException e) {
                return null;
            }
            if (cursor != null && cursor.getCount() == 1) {
                cursor.moveToFirst();
                sparkModal = SparkModal.parseSpark(cursor);
            }

            if (cursor != null) {
                cursor.close();
            }
        }

        return sparkModal;
    }

    public static SparkModal getTopSpark(Context ctx, String userId) {
        SparkModal sparkModal = null;
        if (Utility.isSet(userId)) {
            Cursor cursor;
            try {
                cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).rawQuery(
                        "SELECT * FROM " + ConstantsDB.SPARKS_TABLE +
                                " where " + SPARKS.USER_ID + "=? " + " " +
                                " ORDER BY rowid " +
//                                "order by " + ConstantsDB.SPARKS.SORT_TIMESTAMP + " asc " +
                                "LIMIT 1;",
                        new String[]{userId});

            } catch (DbIsNullException e) {
                return null;
            }
            if (cursor != null && cursor.getCount() == 1) {
                cursor.moveToFirst();
                sparkModal = SparkModal.parseSpark(cursor);
            }

            if (cursor != null) {
                cursor.close();
            }
        }

        return sparkModal;
    }

    public static boolean areThereAnyPendingSparks(Context ctx, String userId) {
        boolean toReturn = false;
        if (Utility.isSet(userId)) {
            Cursor cursor;
            try {
                cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).rawQuery(
                        "SELECT * FROM " + ConstantsDB.SPARKS_TABLE +
                                " where " + SPARKS.USER_ID + "=? " + " " +
                                " and " + SPARKS.START_TIME + "=? ;",
                        new String[]{userId, "0"});

            } catch (DbIsNullException e) {
                return false;
            }
            if (cursor != null && cursor.getCount() > 0) {
                toReturn = true;
            }

            if (cursor != null) {
                cursor.close();
            }
        }

        return toReturn;
    }

    public static int getPendingSparksCount(Context ctx, String userId, String exceptId) {
        int toReturn = 0;
        if (Utility.isSet(userId)) {
            Cursor cursor;

            StringBuilder matchIdQuery = new StringBuilder("");
            ArrayList<String> parameters = new ArrayList<>();
            parameters.add(userId);
            parameters.add("0");
            if (Utility.isSet(exceptId)) {
                matchIdQuery.append(" and " + SPARKS.MATCH_ID + "!=? ");
                parameters.add(exceptId);
            }


            try {
                cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).rawQuery(
                        "SELECT * FROM " + ConstantsDB.SPARKS_TABLE +
                                " where " + SPARKS.USER_ID + "=? " + " " +
                                " and " + SPARKS.START_TIME + "=? " +
                                matchIdQuery.toString() +
                                ";",
                        parameters.toArray(new String[0]));

            } catch (DbIsNullException e) {
                return 0;
            }
            if (cursor != null && cursor.getCount() > 0) {
                toReturn = cursor.getCount();
            }

            if (cursor != null) {
                cursor.close();
            }
        }

        return toReturn;
    }

    public static int getPendingSparksCount(Context ctx, String userId) {
        return getPendingSparksCount(ctx, userId, null);
//        int toReturn = 0;
//        if (Utility.isSet(userId)) {
//            Cursor cursor;
//            try {
//                cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).rawQuery(
//                        "SELECT * FROM " + ConstantsDB.SPARKS_TABLE +
//                                " where " + SPARKS.USER_ID + "=? " + " " +
//                                " and " + SPARKS.START_TIME + "=? ;",
//                        new String[]{userId, "0"});
//
//            } catch (DbIsNullException e) {
//                return 0;
//            }
//            if (cursor != null && cursor.getCount() > 0) {
//                toReturn = cursor.getCount();
//            }
//
//            if (cursor != null) {
//                cursor.close();
//            }
//        }
//
//        return toReturn;
    }

    public static int getSparksCount(Context ctx, String userId) {
        int toReturn = 0;
        if (Utility.isSet(userId)) {
            Cursor cursor;
            try {
                cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).rawQuery(
                        "SELECT * FROM " + ConstantsDB.SPARKS_TABLE +
                                " where " + SPARKS.USER_ID + "=? " + " ;",
                        new String[]{userId});

            } catch (DbIsNullException e) {
                return 0;
            }
            if (cursor != null && cursor.getCount() > 0) {
                toReturn = cursor.getCount();
            }

            if (cursor != null) {
                cursor.close();
            }
        }

        return toReturn;
    }

    public static ArrayList<SparkModal> getTopTwoSparks(Context ctx, String userId) {
        ArrayList<SparkModal> sparkModals = null;
        if (Utility.isSet(userId)) {
            Cursor cursor;
            try {
                cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).rawQuery(
                        "SELECT * FROM " + ConstantsDB.SPARKS_TABLE +
                                " where " + SPARKS.USER_ID + "=? " +
                                " ORDER BY " + SPARKS.START_TIME + " DESC " +
                                " LIMIT 2 " +
                                " ; "
                        , new String[]{userId});

            } catch (DbIsNullException e) {
                return null;
            }
            if (cursor != null) {
                sparkModals = new ArrayList<>();
                while (cursor.moveToNext()) {
                    sparkModals.add(SparkModal.parseSpark(cursor));
                }
                cursor.close();
            }
        }

        return sparkModals;
    }

    public static ArrayList<SparkModal> getAllSparks(Context ctx, String userId) {
        ArrayList<SparkModal> sparkModals = null;
        if (Utility.isSet(userId)) {
            Cursor cursor;
            try {
                cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).rawQuery(
                        "SELECT * FROM " + ConstantsDB.SPARKS_TABLE +
                                " where " + SPARKS.USER_ID + "=?; "
                        , new String[]{userId});

            } catch (DbIsNullException e) {
                return null;
            }
            if (cursor != null) {
                sparkModals = new ArrayList<>();
                while (cursor.moveToNext()) {
                    sparkModals.add(SparkModal.parseSpark(cursor));
                }
                cursor.close();
            }
        }

        return sparkModals;
    }

    public static SparkModal getSpark(Context ctx, String userId, String matchId) {
        SparkModal sparkModal = null;
        if (Utility.isSet(userId) && Utility.isSet(matchId)) {
            Cursor cursor;
            try {
                cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).rawQuery(
                        "SELECT * FROM " + ConstantsDB.SPARKS_TABLE +
                                " where " + SPARKS.USER_ID + "=? " + " " +
                                "and " + SPARKS.MATCH_ID + "=? ;",
                        new String[]{userId, matchId});

            } catch (DbIsNullException e) {
                return null;
            }
            if (cursor != null && cursor.getCount() == 1) {
                cursor.moveToFirst();
                sparkModal = SparkModal.parseSpark(cursor);
            }

            if (cursor != null) {
                cursor.close();
            }
        }

        return sparkModal;
    }

    public static void sparkRejected(Context ctx, String userId, String matchId) {
        String whereClause = "user_id=? and match_id=?";
        String[] whereArgs = new String[]{userId, matchId};
        try {
            TrulyMadlySQLiteHandler.getDatabase(ctx).delete(ConstantsDB.SPARKS_TABLE, whereClause, whereArgs);
            TrulyMadlySQLiteHandler.getDatabase(ctx).delete(ConstantsDB.MESSAGE_METADATA_TABLE, whereClause, whereArgs);
            TrulyMadlySQLiteHandler.getDatabase(ctx).delete(ConstantsDB.MESSAGES_TABLE, whereClause, whereArgs);
            //Remove the following line - adding for testing purpose for dummy data
//            TrulyMadlySQLiteHandler.getDatabase(ctx).delete(ConstantsDB.CONVERSATION_LIST_TABLE, whereClause, whereArgs);
        } catch (DbIsNullException ignored) {
        }
    }

    public static void sparkAccepted(Context ctx, String userId, String matchId, MessageModal messageModal) {
        String whereClause = "user_id=? and match_id=?";
        String[] whereArgs = new String[]{userId, matchId};
        ContentValues contentValues = new ContentValues();
        contentValues.put(ConstantsDB.MESSAGE_METADATA.IS_MUTUAL_SPARK, 1);
        try {
            MessageDBHandler.insertConversationItem(ctx, userId, messageModal, Constants.FetchSource.LOCAL);
            TrulyMadlySQLiteHandler.getDatabase(ctx).delete(ConstantsDB.SPARKS_TABLE, whereClause, whereArgs);
            TrulyMadlySQLiteHandler.getDatabase(ctx).update(ConstantsDB.MESSAGE_METADATA_TABLE, contentValues, whereClause, whereArgs);
        } catch (DbIsNullException ignored) {
        }
    }

    public static void startSparkTimer(Context ctx, String userId, String matchId, long currentTime) {
        String whereClause = "user_id=? and match_id=?";
        String[] whereArgs = new String[]{userId, matchId};
        ContentValues contentValues = new ContentValues();
        contentValues.put(SPARKS.START_TIME, String.valueOf(currentTime));
        try {
            TrulyMadlySQLiteHandler.getDatabase(ctx).update(ConstantsDB.SPARKS_TABLE, contentValues, whereClause, whereArgs);
        } catch (DbIsNullException ignored) {
        }
    }


    /////////////////////////////////////////////
    //Purchase related queries
    /////////////////////////////////////////////
//    public static boolean createPurchase(Context ctx, String userId, String sku,
//                                              String developerPayload, PaymentMode paymentMode) {
//        return createPurchase(ctx, userId, sku, developerPayload, paymentMode, PaymentFor.SPARKS);
//    }

    public static boolean createPurchase(Context ctx, String userId, String sku,
                                         String developerPayload, PaymentMode paymentMode,
                                         PaymentFor paymentFor) {
        ContentValues values = new ContentValues();
        values.put(PURCHASES.USER_ID, userId);
        values.put(PURCHASES.SKU, sku);
        values.put(PURCHASES.DEVELOPER_PAYLOAD, developerPayload);
        values.put(PURCHASES.PAYMENT_MODE, paymentMode.getmKey());
        values.put(PURCHASES.PAYMENT_FOR, paymentFor.getmKey());
        try {
            TrulyMadlySQLiteHandler.getDatabase(ctx).insert(PURCHASES_TABLE, null, values);
        } catch (DbIsNullException e) {
            return false;
        } catch (Exception e) {
            return false;
        }
        return true;
    }

//    public static boolean setSparkPurchaseState(Context aContext, String myId, String developerPayload,
//                                                PurchaseState purchaseState, String token,
//                                                String orderId, String sku, PaymentMode paymentMode) {
//        return setSparkPurchaseState(aContext, myId, developerPayload, purchaseState,
//                token, orderId, sku, paymentMode, PaymentFor.SPARKS);
//    }

    public static boolean setSparkPurchaseState(Context aContext, String myId, String developerPayload,
                                                PurchaseState purchaseState, String token,
                                                String orderId, String sku, PaymentMode paymentMode,
                                                PaymentFor paymentFor) {
        //TODO : SPARKS: Use paymentMode
        if (!Utility.isSet(myId) || !Utility.isSet(developerPayload)) {
            return false;
        }

        String whereClause = PURCHASES.USER_ID + "=? and " + PURCHASES.DEVELOPER_PAYLOAD + "=? and " +
                PURCHASES.PAYMENT_MODE + "=? and " + PURCHASES.PAYMENT_FOR + " =? ";
        String[] whereArgs = new String[]{myId, developerPayload, String.valueOf(paymentMode.getmKey())
                , String.valueOf(paymentFor.getmKey())};
        ContentValues contentValues = new ContentValues();
        contentValues.put(PURCHASES.PURCHASE_STATE, purchaseState.toString());
        if (purchaseState == PurchaseState.PURCHASED_STORE) {
            contentValues.put(PURCHASES.PURCHASE_TIMESTAMP, TimeUtils.getFormattedTime());
        }
        if (Utility.isSet(token)) {
            contentValues.put(PURCHASES.TOKEN, token);
        }
        if (Utility.isSet(orderId)) {
            contentValues.put(PURCHASES.ORDER_ID, orderId);
        }
        try {
            int rowsAffected = TrulyMadlySQLiteHandler.getDatabase(aContext).update(PURCHASES_TABLE, contentValues, whereClause, whereArgs);
            if (rowsAffected < 1) {
                createPurchase(aContext, myId, sku, developerPayload, paymentMode, paymentFor);
                TrulyMadlySQLiteHandler.getDatabase(aContext).update(PURCHASES_TABLE, contentValues, whereClause, whereArgs);
            }
            return true;
        } catch (DbIsNullException e) {
            return false;
        } catch (Exception e) {
            return false;
        }
    }

//    public static ArrayList<String> getUniqueSkus(Context aContext) {
//        return getUniqueSkus(aContext, PaymentFor.SPARKS);
//    }

    public static ArrayList<String> getUniqueSkus(Context aContext, PaymentFor paymentFor) {
        ArrayList<String> skuList = new ArrayList<>();
        String myId = Utility.getMyId(aContext);
        if (!Utility.isSet(myId)) {
            return skuList;
        }
        String whereClause = PURCHASES.USER_ID + "=? and " + PURCHASES.PAYMENT_FOR + " =?";
        String[] whereArgs = new String[]{myId, String.valueOf(paymentFor.getmKey())};

        Cursor cursor;
        try {
            cursor = TrulyMadlySQLiteHandler.getDatabase(aContext).query(true, PURCHASES_TABLE, new String[]{PURCHASES.SKU}, whereClause, whereArgs, null, null, null, null);
        } catch (DbIsNullException e) {
            return skuList;
        }
        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                skuList.add(cursor.getString(cursor.getColumnIndex(PURCHASES.SKU)));
            }
        }

        if (cursor != null) {
            cursor.close();
        }
        return skuList;
    }

//    public static ArrayList<PurchaseModal> getPurchasesInPurchaseState(Context aContext,
//                                                                       PurchaseState purchaseState,
//                                                                       PaymentMode paymentMode) {
//        return getPurchasesInPurchaseState(aContext, purchaseState, paymentMode, PaymentFor.SPARKS);
//    }

    public static ArrayList<PurchaseModal> getPurchasesInPurchaseState(Context aContext,
                                                                       PurchaseState purchaseState,
                                                                       PaymentMode paymentMode,
                                                                       PaymentFor paymentFor) {
        ArrayList<PurchaseModal> unconsumedPurchases = new ArrayList<>();
        String purchaseStateString = "'" + purchaseState.toString() + "'";
        String myId = Utility.getMyId(aContext);
        if (!Utility.isSet(myId)) {
            return unconsumedPurchases;
        }

        Cursor cursor;
        try {
            cursor = TrulyMadlySQLiteHandler.getDatabase(aContext).rawQuery(
                    "SELECT * FROM " + PURCHASES_TABLE
                            + " where " + PURCHASES.USER_ID + "=?  and " + PURCHASES.PAYMENT_MODE
                            + " =?  and " + PURCHASES.PAYMENT_FOR
                            + " =?  and " + PURCHASES.PURCHASE_STATE + " IN ("
                            + purchaseStateString + ") "
                            + " ;",
                    new String[]{myId, String.valueOf(paymentMode.getmKey()), String.valueOf(paymentFor.getmKey())});
        } catch (DbIsNullException e) {
            return unconsumedPurchases;
        }
        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                PurchaseModal purchaseModal = new PurchaseModal(cursor);
                unconsumedPurchases.add(purchaseModal);
            }
        }
        if (cursor != null) {
            cursor.close();
        }
        return unconsumedPurchases;
    }

//    public static int getIncompletePurchasesCount(Context aContext, PaymentMode paymentMode) {
//        return getIncompletePurchasesCount(aContext, paymentMode, PaymentFor.SPARKS);
//    }

    public static int getIncompletePurchasesCount(Context aContext, PaymentMode paymentMode,
                                                  PaymentFor paymentFor) {
        String purchaseState = "'" + PurchaseState.CONSUMED_STORE.toString() + "',"
                + "'" + PurchaseState.PURCHASED_STORE.toString() + "',"
                + "'" + PurchaseState.PURCHASED_SERVER.toString() + "'";
        String myId = Utility.getMyId(aContext);
        int count = 0;
        if (!Utility.isSet(myId)) {
            return count;
        }

        Cursor cursor;
        try {
            cursor = TrulyMadlySQLiteHandler.getDatabase(aContext).rawQuery(
                    "SELECT count(*) as count from " + PURCHASES_TABLE
                            + " where " + PURCHASES.USER_ID + "=?  and " + PURCHASES.PAYMENT_MODE
                            + " =?  and " + PURCHASES.PAYMENT_FOR
                            + " =?  and " + PURCHASES.PURCHASE_STATE + " IN ("
                            + purchaseState + ") "
                            + " ;",
                    new String[]{myId, String.valueOf(paymentMode.getmKey()), String.valueOf(paymentFor.getmKey())});
        } catch (DbIsNullException e) {
            return count;
        }
        if (cursor != null && cursor.moveToFirst()) {
            count = cursor.getInt(cursor.getColumnIndex("count"));
        }
        if (cursor != null) {
            cursor.close();
        }
        return count;
    }

}
