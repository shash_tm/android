package com.trulymadly.android.app.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.trulymadly.android.Exception.DbIsNullException;
import com.trulymadly.android.app.modal.Voucher;
import com.trulymadly.android.app.utility.TimeUtils;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class VoucherDBHandler {

    // Database fields
    private final Context aContext;

    public VoucherDBHandler(Context aContext) {
        this.aContext = aContext;
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static void saveVoucherData(Context aContext, String voucher_data) {
        if (Utility.isSet(voucher_data)) {
            JSONObject voucherData;
            try {
                voucherData = new JSONObject(voucher_data);
                VoucherDBHandler x = new VoucherDBHandler(aContext);
                x.insertVoucher(voucherData.optString("voucher_code"),
                        voucherData.optString("user_id"),
                        voucherData.optString("expiry"),
                        voucherData.optString("campaign"),
                        voucherData.optString("friendly_text"));
                // x.close();
            } catch (JSONException ignored) {
            }

        }
    }

    //@Trace(category = MetricCategory.DATABASE)
    private void insertVoucher(String voucher_code, String user_id,
                               String expiry, String campaign, String friendly_text) {
        if (Utility.isSet(voucher_code) && Utility.isSet(user_id)
                && Utility.isSet(expiry) && Utility.isSet(campaign)
                && Utility.isSet(friendly_text)) {
            ContentValues values = new ContentValues();
            values.put("voucher_code", voucher_code);
            values.put("user_id", user_id);
            values.put("expiry", expiry);
            values.put("campaign", campaign);
            values.put("friendly_text", friendly_text);
            try {
                TrulyMadlySQLiteHandler.getDatabase(aContext).replace("vouchers", null, values);
            } catch (DbIsNullException ignored) {
            }
        }
    }

    //@Trace(category = MetricCategory.DATABASE)
    public ArrayList<Voucher> getVouchersForUser(String user_id) {
        ArrayList<Voucher> allVouchers = new ArrayList<>();

        String date = TimeUtils.getFormattedTime();

        Cursor cursor = null; // h. limit
        try {
            cursor = TrulyMadlySQLiteHandler.getDatabase(aContext).query("vouchers", // a. table
                    null, // b. column names
                    " user_id = ? and date(expiry) > ?", // c. selections
                    new String[]{user_id, date}, // d. selections args
                    null, // e. group by
                    null, // f. having
                    null, // g. order by
                    null);
        } catch (DbIsNullException e) {
            return allVouchers;
        }

        try {
            if (cursor.moveToFirst()) {
                do {
                    allVouchers.add(new Voucher(cursor));
                } while (cursor.moveToNext());
            }
        } catch (RuntimeException ignored) {

        }
        if (cursor != null) {
            cursor.close();
        }
        return allVouchers;
    }

}
