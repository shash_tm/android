package com.trulymadly.android.app.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.trulymadly.android.Exception.DbIsNullException;
import com.trulymadly.android.app.json.ConstantsDB;
import com.trulymadly.android.app.json.ConstantsRF;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by deveshbatra on 2/22/16.
 */
public class MatchesDbHandler {

//    public static void updateMatches(Context ctx , JSO)
    //@Trace(category = MetricCategory.DATABASE)

    public static void insertMatches(Context ctx, JSONObject response) {


        String myId = Utility.getMyId(ctx);
        if (!Utility.isSet(myId)) {
            return;
        }

        JSONArray data = response.optJSONArray("data");
        if (data == null || data.length() == 0) {
            return;
        }

        String sql = "INSERT OR IGNORE INTO matches_cached_data (user_id,match_id,details,action) VALUES (?,?,?,?);";
        SQLiteDatabase db;
        try {
            db = TrulyMadlySQLiteHandler.getDatabase(ctx);
        } catch (DbIsNullException e) {
            return;
        }

        SQLiteStatement statement = db.compileStatement(sql);
        db.beginTransaction();


        String matchId;
        for (int i = 0; i < data.length(); i++) {

            JSONObject matchObject = data.optJSONObject(i);
            matchId = null;

            if (matchObject != null)
                matchId = matchObject.optString("user_id");

            if (Utility.stringCompare(matchId, myId))
                continue;

            if (matchObject != null && Utility.isSet(matchId)) {
                statement.clearBindings();
                statement.bindString(1, myId);
                statement.bindString(2, matchId);
                statement.bindString(3, matchObject.toString());
                statement.bindString(4, "0");
                statement.execute();
            }
        }
        db.setTransactionSuccessful();
        db.endTransaction();


    }


    public static void updateActionToTrue(Context aContext, String match_id) {
        String[] whereArgs = new String[]{Utility.getMyId(aContext), match_id};
        String whereClause = "user_id=? and match_id=?";
        ContentValues values = new ContentValues();
        try {
            values.put("action", "1");
            TrulyMadlySQLiteHandler.getDatabase(aContext).update(ConstantsDB.MATCHES_DATA_TABLE, values, whereClause, whereArgs);
        } catch (DbIsNullException ignored) {
        }
    }
    //@Trace(category = MetricCategory.DATABASE)

    public static JSONArray getProfileList(Context aContext) {

        JSONArray profileList = new JSONArray();
        String myId = Utility.getMyId(aContext);
        if (!Utility.isSet(myId)) {
            return profileList;
        }
        String[] whereArgs = new String[]{myId, "0"};
        String whereClause = "user_id=? and action=?";
        String[] columns = new String[]{"details", "rowid"};

        Cursor cursor;
        try {
            cursor = TrulyMadlySQLiteHandler.getDatabase(aContext).query(ConstantsDB.MATCHES_DATA_TABLE, columns, whereClause, whereArgs, null, null, "rowid asc");
        } catch (DbIsNullException e) {
            return profileList;
        }

        String jsonString;
        if (cursor != null) {
            while (cursor.moveToNext()) {
                jsonString = cursor.getString(cursor.getColumnIndex("details"));
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(jsonString);
                    if (!Utility.stringCompare(jsonObject.optString("user_id"), Utility.getMyId(aContext))) {
                        profileList.put(jsonObject);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            cursor.close();
        }
        return profileList;
    }


    //@Trace(category = MetricCategory.DATABASE)
    public static void deleteMatchId(Context aContext, String matchId) {
        if (!Utility.isSet(matchId)) {
            return;
        }

        String whereClause = "user_id=? and match_id=?";
        String[] whereArgs = new String[]{Utility.getMyId(aContext), matchId};
        try {
            TrulyMadlySQLiteHandler.getDatabase(aContext).delete(ConstantsDB.MATCHES_DATA_TABLE, whereClause, whereArgs);
        } catch (DbIsNullException e) {
            e.printStackTrace();
        }
    }


    //@Trace(category = MetricCategory.DATABASE)
    public static int getLastRowId(Context aContext) {
        int rowId = 0;
        String[] columns = new String[]{"rowid"};
        String whereClause = "user_id=?";
        String[] whereArgs = new String[]{Utility.getMyId(aContext)};
        Cursor cursor = null;
        try {
            cursor = TrulyMadlySQLiteHandler.getDatabase(aContext).query(ConstantsDB.MATCHES_DATA_TABLE,
                    columns, whereClause, whereArgs, null, null, "rowid DESC", "1");
        } catch (DbIsNullException e) {
            return rowId;
        }
        if (cursor != null) {
            if (cursor.getCount() > 0 && cursor.moveToFirst()) {
                rowId = cursor.getInt(cursor.getColumnIndex("rowid"));
            }
            cursor.close();
        }

        return rowId;
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static void clearMatchesCache(Context aContext) {
        String whereClause = "user_id=?";
        String[] whereArgs = new String[]{Utility.getMyId(aContext)};
        try {
            TrulyMadlySQLiteHandler.getDatabase(aContext).delete(ConstantsDB.MATCHES_DATA_TABLE,
                    whereClause, whereArgs);
        } catch (DbIsNullException e) {
            return;
        }

        // clear the values of some rigid fields pending
        RFHandler.insert(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_LAST_MATCHES_CALL_TSTAMP, (long) 0);
//        RFHandler.insert(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_HAS_MORE_MATCHES, false);
        RFHandler.insert(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_MATCHES_CALL_INTERVAL, 30 * 60 * 1000);
        SPHandler.remove(aContext, ConstantsSP.SHARED_KEY_MATCHES_RESPONSE);

    }


    public static void deleteDataAfterCertainId(Context aContext, String id) {
        int rowId = 0;
        String[] columns = new String[]{"rowid"};
        String whereClause = "match_id=?";
        String[] whereArgs = new String[]{id};
        Cursor cursor = null;
        try {
            cursor = TrulyMadlySQLiteHandler.getDatabase(aContext).query(ConstantsDB.MATCHES_DATA_TABLE,
                    columns, whereClause, whereArgs, null, null, "rowid DESC", "1");
        } catch (DbIsNullException ignored) {
        }
        if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
            rowId = cursor.getInt(cursor.getColumnIndex("rowid"));

        }

        if (cursor != null)
            cursor.close();


        // after this rowId delete all the rows

        String newWhereClause = "rowid > ?";
        String[] newWhereArgs = new String[]{"" + rowId};
        try {
            TrulyMadlySQLiteHandler.getDatabase(aContext).delete(ConstantsDB.MATCHES_DATA_TABLE, newWhereClause, newWhereArgs);
        } catch (DbIsNullException ignored) {
        }


    }
}
