package com.trulymadly.android.app.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.trulymadly.android.Exception.DbIsNullException;
import com.trulymadly.android.app.MessageModal;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.ConstantsDB;
import com.trulymadly.android.app.modal.CuratedDealsChatsModal;
import com.trulymadly.android.app.utility.Utility;

/**
 * Created by avin on 29/12/15.
 */
public class DateSpotsDBHandler {

//    public static void insertDummyDateSpot(Context ctx, String user_id, String dateSpotId){
//        insertDateSpot(ctx, user_id, dateSpotId, dateSpotId + " message", "https://s3-ap-southeast-1.amazonaws.com/" +
//                "trulymadlytestbucketnew/files/images/profiles/" +
//                "1448626819031.2_503314150031656000.jpg",
//                "https://s3-ap-southeast-1.amazonaws.com/" +
//                "trulymadlytestbucketnew/files/images/profiles/" +
//                "1449834649107.4_225232259836047872.jpg", dateSpotId + "Some address", dateSpotId + "20% Off");
//    }

    //@Trace(category = MetricCategory.DATABASE)
    public static void insertDateSpot(Context ctx, String user_id, String dateSpotId, String message,
                                      String rectUri, String address
    ) {
        ContentValues values = new ContentValues();
        values.put("user_id", user_id);
        values.put("message", message);
        values.put("date_spot_id", dateSpotId);
        values.put(Constants.CD_KEY_IMAGE_URI, rectUri);
        values.put("address", address);

        try {
            TrulyMadlySQLiteHandler.getDatabase(ctx).insert(ConstantsDB.DATE_SPOT_TABLE, null, values);
        } catch (DbIsNullException ignored) {
        }
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static CuratedDealsChatsModal getDateSpot(String user_id, String datespotId,
                                                     Context ctx) {
        CuratedDealsChatsModal retVal = null;
        if (Utility.isSet(user_id) && Utility.isSet(datespotId)) {
            Cursor cursor = null;
            try {
                cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).rawQuery(
                        "SELECT * FROM " + ConstantsDB.DATE_SPOT_TABLE + " where user_id=? and date_spot_id=?;",
                        new String[]{user_id, datespotId});
                retVal = getCuratedDealFromCursor(cursor);
            } catch (Exception ignored) {

            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return retVal;
    }

    //@Trace(category = MetricCategory.DATABASE)
    private static CuratedDealsChatsModal getCuratedDealFromCursor(Cursor cursor) {
        String mDealId = null, mDateSpotId, mRectUri, mMessage, mAddress;
        MessageModal.MessageType mMessageType = null;
        CuratedDealsChatsModal retVal = null;
        if (cursor.moveToNext()) {
            mDateSpotId = cursor.getString(cursor.getColumnIndex("date_spot_id"));
            mRectUri = cursor.getString(cursor.getColumnIndex(Constants.CD_KEY_IMAGE_URI));
            mMessage = cursor.getString(cursor.getColumnIndex("message"));
            mAddress = cursor.getString(cursor.getColumnIndex("address"));

            retVal = new CuratedDealsChatsModal(mMessageType, mDealId, mDateSpotId,
                    mRectUri, mMessage, mAddress);
        }
        return retVal;
    }
}
