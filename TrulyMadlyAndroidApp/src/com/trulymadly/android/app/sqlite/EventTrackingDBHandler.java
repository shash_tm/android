package com.trulymadly.android.app.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.trulymadly.android.Exception.DbIsNullException;
import com.trulymadly.android.analytics.Event;
import com.trulymadly.android.analytics.EventToServer;

import java.util.ArrayList;
import java.util.List;

public class EventTrackingDBHandler {

	// private TrulyMadlySQLiteHandler dbHelper;
	private static final String TABLE_EVENTTRACKING = "event_tracking";
	private static final String EVENTTRACKING_KEY_DB_ID = "_id";
	private static final String EVENTTRACKING_KEY_INFO = "event_info";
	private static final int number_to_send_at_one_go = 10;
	private static EventTrackingDBHandler instance;
	// Database fields
	private SQLiteDatabase database;
	// Try not to hit the database unless we need to. This value is polled every
	// few seconds.
	private boolean payloadsDirty = true;

	private EventTrackingDBHandler(Context context) {
        try {
			database = TrulyMadlySQLiteHandler.getDatabase(context);
		} catch (DbIsNullException ignored) {
		}
	}

	public static EventTrackingDBHandler getInstance(Context context) {
		if (instance == null) {
			instance = new EventTrackingDBHandler(
					context.getApplicationContext());
		}
		return instance;
	}

	private static void ensureClosed(Cursor cursor) {
		try {
			if (cursor != null) {
				cursor.close();
			}
		} catch (Exception ignored) {
		}
	}

	/**
	 * Does it need to be synchronized? If an item with the same nonce as an
	 * item passed in already exists, it is overwritten by the item. Otherwise a
	 * new message is added.
	 */
	public synchronized void addPayLoad(Event... payloads) {
		try {
			payloadsDirty = true;
			for (Event payload : payloads) {
				if (payload.payLoad() != null) {
					ContentValues values = new ContentValues();
					if (values != null) {
						values.put(EVENTTRACKING_KEY_INFO, payload.payLoad()
								.toString());
                        if (database != null) {
                            database.insert(TABLE_EVENTTRACKING, null, values);
                        }
                    }
				}
			}
		} catch (NullPointerException | IllegalStateException ignored) {

		}
	}

	public synchronized void deletePayload(List<Long> ids_to_delete) {
		// payloadsDirty = true;
		if (ids_to_delete != null && ids_to_delete.size() > 0) {
			String args = TextUtils.join(", ", ids_to_delete);
			try {
				String sql = String.format("DELETE FROM " + TABLE_EVENTTRACKING
						+ " WHERE " + EVENTTRACKING_KEY_DB_ID + " IN (%s)",
						args);
                if (database != null) {
                    database.execSQL(sql);
                }
			} catch (SQLException ignored) {
			}
		}
	}

	/**
	 * it will fetch 10 entries and return key value pair
	 *
	 * @return
	 */
	public synchronized List<EventToServer> getPayloadToSent() {
		if (!payloadsDirty) {
			return null;
		}
        if (database == null) {
            return null;
        }
		List<EventToServer> dataToSend = new ArrayList<>();
		// JSONArray dataToSend = new JSONArray();//new
		// ArrayList<EventToServer>();

		Cursor cursor = null;
		try {
			String limit = String.valueOf(number_to_send_at_one_go);
			cursor = database.query(TABLE_EVENTTRACKING, new String[] {
					EVENTTRACKING_KEY_DB_ID, EVENTTRACKING_KEY_INFO }, null,
					null, null, null, EVENTTRACKING_KEY_DB_ID, limit);
			cursor.moveToFirst();

			while (!cursor.isAfterLast()) {
				long databaseId = cursor.getLong(cursor
						.getColumnIndex(EVENTTRACKING_KEY_DB_ID));
				String event_info = cursor.getString(cursor
						.getColumnIndex(EVENTTRACKING_KEY_INFO));

				dataToSend.add(new EventToServer(databaseId, event_info));
				cursor.moveToNext();
			}
			if (dataToSend.size() < number_to_send_at_one_go) {
				payloadsDirty = false;
			}
			return dataToSend;
		} finally {
			ensureClosed(cursor);
		}
	}

}
