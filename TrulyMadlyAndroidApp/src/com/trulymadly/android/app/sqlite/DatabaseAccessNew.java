package com.trulymadly.android.app.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.trulymadly.android.Exception.DbIsNullException;

class DatabaseAccessNew {

	// public static TrulyMadlySQLiteHandler dbHandler;

	// private TrulyMadlySQLiteHandler dbHandler;
	private SQLiteDatabase database;

	public DatabaseAccessNew(Context aContext) {
		try {
			database = TrulyMadlySQLiteHandler.getDatabase(aContext);
		} catch (DbIsNullException e) {
			database = null;
		}
	}

	/*
	 * public void open() throws SQLException { database =
	 * dbHandler.getWritableDatabase(); }
	 */

    //@Trace(category = MetricCategory.DATABASE)
	private void deleteTable(String tableName) {
		if (database != null) {
			database.delete(tableName, null, null);
		}

	}

	// inserting into edit pref
    //@Trace(category = MetricCategory.DATABASE)
    public void insertEditPrefBasicData(String id, String data) {
		ContentValues values = new ContentValues();
		values.put("ID", id);
		values.put("DATA", data);
		if (database != null) {
			database.insert("EDIT_PREF", null, values);
		}
	}

    //@Trace(category = MetricCategory.DATABASE)
	@SuppressWarnings("TryFinallyCanBeTryWithResources")
	public String getEditPrefBasicData(String id) {
		String data = null;
		if (database == null) {
			return data;
		}
		String getMsgConvSeenSql = " SELECT * FROM EDIT_PREF WHERE ID=?";
		Cursor cursor = database.rawQuery(getMsgConvSeenSql,
				new String[]{id});

		try {
			if (cursor != null && cursor.getCount() > 0 && cursor.moveToNext()) {
				try {
					data = cursor.getString(cursor.getColumnIndex("DATA"));
				} catch (OutOfMemoryError | NoSuchMethodError ignored) {
				} finally {
					cursor.close();
				}
				return data;
			}
		} finally {
			cursor.close();
		}
		return data;
	}

    //@Trace(category = MetricCategory.DATABASE)
    public void deleteEditPref() {
		deleteTable("EDIT_PREF");
	}

}
