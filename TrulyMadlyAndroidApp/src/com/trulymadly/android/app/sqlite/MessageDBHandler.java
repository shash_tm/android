package com.trulymadly.android.app.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteCantOpenDatabaseException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import android.database.sqlite.SQLiteStatement;

import com.trulymadly.android.Exception.DbIsNullException;
import com.trulymadly.android.app.MessageModal;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.bus.SimpleAction;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.Constants.FetchSource;
import com.trulymadly.android.app.json.Constants.MessageState;
import com.trulymadly.android.app.json.ConstantsDB;
import com.trulymadly.android.app.modal.ActiveConversationModal;
import com.trulymadly.android.app.modal.ConversationModal;
import com.trulymadly.android.app.modal.CuratedDealsChatsModal;
import com.trulymadly.android.app.modal.MatchMessageMetaData;
import com.trulymadly.android.app.utility.FilesHandler;
import com.trulymadly.android.app.utility.ImageCacheHelper;
import com.trulymadly.android.app.utility.MoEHandler;
import com.trulymadly.android.app.utility.TimeUtils;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;

public class MessageDBHandler {

    //@Trace(category = MetricCategory.DATABASE)
    public static void migrateToConversationListTable(Context context) {
        try {
            TrulyMadlySQLiteHandler.getInstance(context)
                    .migrateToConversationListTable(TrulyMadlySQLiteHandler.getDatabase(context));
        } catch (DbIsNullException ignored) {
        }
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static void insertConversationItem(Context ctx, String user_id,
                                              MessageModal messageObj, FetchSource fetch_source) {
        switch (fetch_source) {
            case GCM:
                return;
            default:
                break;
        }

        if (messageObj == null) {
            return;
        }

        switch (messageObj.getMessageState()) {
            case OUTGOING_SENDING:
            case OUTGOING_FAILED:
                return;
            default:
                break;
        }
        if (isConversationTstampGreaterAndNotBlocked(ctx, user_id,
                messageObj.getMatch_id(), messageObj.getTstamp())) {
            ContentValues values = new ContentValues();
            values.put("user_id", user_id);
            values.put("match_id", messageObj.getMatch_id());
            values.put("tstamp", messageObj.getTstamp());
            values.put("sort_tstamp", messageObj.getTstamp());
            values.put("last_message_type",
                    messageObj.getMessageType().toString());
            values.put("last_message_state",
                    messageObj.getMessageState().toString());
            values.put("last_message_text", messageObj.getDisplayMessage());
            try {
                TrulyMadlySQLiteHandler.getDatabase(ctx).insert(ConstantsDB.CONVERSATION_LIST_TABLE, null, values);
            } catch (DbIsNullException ignored) {
            }
        }
    }

    // public static MessageState getLastMessageStateForConversation(Context
    // ctx, String user_id, String match_id){
    //@Trace(category = MetricCategory.DATABASE)
    public static boolean isAnyUnreadMessage(Context ctx, String user_id,
                                             String match_id) {
        if (!Utility.isSet(user_id) || !Utility.isSet(match_id)) {
            return false;
        }
        Cursor cursor = null;
        String[] whereArgs = new String[]{user_id, match_id};
        String unreadMessageStates = "'"
                + MessageState.INCOMING_DELIVERED.toString() + "','"
                + MessageState.CONVERSATION_FETCHED_NEW.toString() + "','"
                + MessageState.MATCH_ONLY_NEW.toString() + "'";
        //CHECK FROM CONVERSATION_LIST_TABLE TABLE
        try {
            cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).query(ConstantsDB.CONVERSATION_LIST_TABLE,
                    null,
                    "user_id=? and match_id=? and last_message_state IN ("
                            + unreadMessageStates + "); ", whereArgs,
                    null, null, null, "1");
        } catch (DbIsNullException e) {
            return false;
        }
        if (cursor != null && cursor.getCount() > 0) {
            cursor.close();
            return true;
        }
        if (cursor != null)
            cursor.close();

        //CHECK FROM MESSAGES TABLE
        try {
            cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).query(ConstantsDB.MESSAGES_TABLE, null,
                    "user_id=? and match_id=? and message_state IN ("
                            + unreadMessageStates + "); ",
                    whereArgs, null, null, null, "1");
        } catch (DbIsNullException e) {
            return false;
        }
        if (cursor != null && cursor.getCount() > 0) {
            cursor.close();
            return true;
        }
        if (cursor != null)
            cursor.close();
        return false;
    }

    //@Trace(category = MetricCategory.DATABASE)
    private static boolean isConversationTstampGreaterAndNotBlocked(Context ctx,
                                                                    String user_id, String match_id, String tstamp) {
        if (!Utility.isSet(user_id) || !Utility.isSet(match_id)) {
            return true;
        }
        Cursor cursor = null;
        try {
            cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).query(ConstantsDB.CONVERSATION_LIST_TABLE,
                    new String[]{"tstamp", "last_message_state"},
                    "user_id=? and match_id=?", new String[]{user_id, match_id},
                    null, null, null, "1");
        } catch (DbIsNullException e) {
            return true;
        }
        boolean isConversationTstampGreater = true;
        boolean isNotBlocked = true;

        if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
            String maxTstamp = cursor
                    .getString(cursor.getColumnIndex("tstamp"));
            String lastMessageState = cursor
                    .getString(cursor.getColumnIndex("last_message_state"));
            if (Utility.isSet(maxTstamp) && Utility.isSet(tstamp)) {
                Date tstampDate = TimeUtils.getParsedTime(tstamp);
                Date maxTstampDate = TimeUtils.getParsedTime(maxTstamp);
                if (tstampDate != null && maxTstampDate != null && tstampDate.before(maxTstampDate)) {
                    isConversationTstampGreater = false;
                }
            }
            if (Utility.isSet(lastMessageState) && MessageState.BLOCKED_SHOWN
                    .toString().equalsIgnoreCase(lastMessageState)) {
                isNotBlocked = false;
            }
        }
        if (cursor != null)
            cursor.close();
        return isConversationTstampGreater && isNotBlocked;
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static boolean insertAMessage(Context ctx, String user_id,
                                         MessageModal messageObj, boolean overwriteExisting,
                                         FetchSource fetch_source, boolean updateConversations) {
        MessageState messageState = null;
        if (Utility.isSet(user_id) && Utility.isSet(messageObj.getMatch_id())
                && Utility.isSet(messageObj.getMsg_id())) {
            messageState = isMessageExists(user_id, messageObj.getMatch_id(),
                    messageObj.getMsg_id(), ctx);
            if (messageState == null || overwriteExisting) {
                ContentValues values = new ContentValues();
                values.put("user_id", user_id);
                values.put("match_id", messageObj.getMatch_id());
                values.put("message_id", messageObj.getMsg_id());
                values.put("tstamp", messageObj.getTstamp());
                values.put("fetch_source", fetch_source.toString());
                values.put("message_type",
                        messageObj.getMessageType().toString());
                if (messageState != null
                        && messageState == MessageState.INCOMING_READ
                        && messageObj
                        .getMessageState() == MessageState.INCOMING_DELIVERED) {
                    messageObj.setMessageState(messageState);
                }
                values.put("message_state",
                        messageObj.getMessageState().toString());
                values.put("message_text", messageObj.getMessage());
                if (messageObj.getQuiz_id() > 0) {
                    values.put("quiz_id", messageObj.getQuiz_id());
                }


                //Start
                if (Utility.isSet(messageObj.getmDealId())) {
                    values.put(Constants.CD_KEY_DEAL_ID, messageObj.getmDealId());
                }
                if (Utility.isSet(messageObj.getmDateSpotId())) {
                    values.put(Constants.CD_KEY_DATESPOT_ID, messageObj.getmDateSpotId());
                }
                if (Utility.isSet(messageObj.getMsgJsonString())) {
                    values.put("msg_json", messageObj.getMsgJsonString());
                }

                try {
                    TrulyMadlySQLiteHandler.getDatabase(ctx).insert(ConstantsDB.MESSAGES_TABLE, null, values);
                } catch (DbIsNullException e) {
                    return false;
                }

                //Inserting the DateSpot to the dateSpots table
                CuratedDealsChatsModal curatedDealsChatsModal = messageObj.getmCuratedDealsChatsModal();
                if (curatedDealsChatsModal != null) {
                    DateSpotsDBHandler.insertDateSpot(ctx, Utility.getMyId(ctx),
                            curatedDealsChatsModal.getmDateSpotId(),
                            curatedDealsChatsModal.getmMessage(), curatedDealsChatsModal.getmRectUri(),
                            curatedDealsChatsModal.getmAddress()
                    );
                }
                //End


                // Delete message by Unique ID if it is not same as message id.
                if (Utility.isSet(messageObj.getMsg_id())
                        && Utility.isSet(messageObj.getUniqueId())
                        && !messageObj.getUniqueId()
                        .equals(messageObj.getMsg_id())) {
                    String whereClause = "user_id=? and match_id=? and message_id=?";
                    String[] whereArgs = new String[]{user_id,
                            messageObj.getMatch_id(),
                            messageObj.getUniqueId()};
                    try {
                        TrulyMadlySQLiteHandler.getDatabase(ctx).delete(ConstantsDB.MESSAGES_TABLE, whereClause, whereArgs);
                    } catch (DbIsNullException e) {
                        return false;
                    }
                }

                if (messageObj
                        .getMessageState() == MessageState.INCOMING_DELIVERED || messageObj
                        .getMessageState() == MessageState.INCOMING_READ) {
                    JSONObject json = new JSONObject();
                    try {
                        json.put("Match Id", messageObj.getMatch_id());
                    } catch (JSONException ignored) {
                    }

                    MoEHandler.trackEvent(ctx, MoEHandler.Events.MESSAGE_RECEIVED, json, true);
                }
            }
            if (updateConversations) {
                insertConversationItem(ctx, user_id, messageObj, fetch_source);
            }
        }
        return (messageState != null);
    }

    //@Trace(category = MetricCategory.DATABASE)
    private static MessageState isMessageExists(String user_id, String match_id,
                                                String message_id, Context ctx) {
        MessageState state = null;
        Cursor cursor = null;
        try {
            cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).query(ConstantsDB.MESSAGES_TABLE, null,
                    "user_id=? and match_id=? and message_id=?",
                    new String[]{user_id, match_id, message_id}, null, null,
                    null, "1");
        } catch (DbIsNullException e) {
            return state;
        }
        if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
            String stateString = cursor
                    .getString(cursor.getColumnIndex("message_state"));
            if (Utility.isSet(stateString)) {
                state = MessageState.valueOf(stateString);
            }
        }
        if (cursor != null)
            cursor.close();
        return state;
    }

    /***
     * Will be called when we added a message and it is return from the server
     * as well
     */
    //@Trace(category = MetricCategory.DATABASE)
    public static void updateAMessage(Context ctx, String user_id,
                                      MessageModal messageObj, String random_msg_id,
                                      FetchSource fetch_source) {
        try {
            updateAMessage(TrulyMadlySQLiteHandler.getDatabase(ctx), user_id, messageObj,
                    random_msg_id, fetch_source);

            if (messageObj.isSparkAccepted()) {
                SparksDbHandler.sparkAccepted(ctx, Utility.getMyId(ctx), messageObj.getMatch_id(), messageObj);
                SimpleAction simpleAction = new SimpleAction(SimpleAction.ACTION_SPARK_ACCEPTED);
                simpleAction.setmStringValue(messageObj.getMatch_id());
                Utility.fireBusEvent(ctx, true, simpleAction);
                return;
            }
        } catch (DbIsNullException e) {
            return;
        }

        insertConversationItem(ctx, user_id, messageObj, fetch_source);
    }

    /***
     * Will be called when we added a message and it is return from the server
     * as well
     */
    //@Trace(category = MetricCategory.DATABASE)
    public static void updateAMessage(SQLiteDatabase db, String user_id,
                                      MessageModal messageObj, String random_msg_id,
                                      FetchSource fetch_source) {
        ContentValues values = new ContentValues();
        values.put("user_id", user_id);
        values.put("match_id", messageObj.getMatch_id());
        values.put("message_id", messageObj.getMsg_id());
        values.put("tstamp", messageObj.getTstamp());
        values.put("fetch_source", fetch_source.toString());
        values.put("message_type", messageObj.getMessageType().toString());
        values.put("message_state", messageObj.getMessageState().toString());
        values.put("message_text", messageObj.getMessage());
        values.put(Constants.CD_KEY_DEAL_ID, messageObj.getmDealId());
        values.put(Constants.CD_KEY_DATESPOT_ID, messageObj.getmDateSpotId());
        if (Utility.isSet(messageObj.getMsgJsonString())) {
            values.put("msg_json", messageObj.getMsgJsonString());
        }
        String whereClause = "user_id=? and match_id=? and message_id=?";
        String[] whereArgs = new String[]{user_id, messageObj.getMatch_id(),
                random_msg_id};

        db.update(ConstantsDB.MESSAGES_TABLE, values, whereClause, whereArgs);
    }


    //@Trace(category = MetricCategory.DATABASE)
    public static void deleteAMessage(String user_id, String match_id,
                                      String message_id, Context ctx) {
        String whereClause = "user_id=? and match_id=? and message_id=?";
        String[] whereArgs = new String[]{user_id, match_id, message_id};
        try {
            TrulyMadlySQLiteHandler.getDatabase(ctx).delete(ConstantsDB.MESSAGES_TABLE, whereClause, whereArgs);
        } catch (DbIsNullException ignored) {
        }
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static void markMessageAsNotSent(String user_id, String match_id,
                                            String message_id, Context ctx) {
        ContentValues values = new ContentValues();
        values.put("message_state", MessageState.OUTGOING_FAILED.toString());
        String whereClause = "user_id=? and match_id=? and message_id=?";
        String[] whereArgs = new String[]{user_id, match_id, message_id};
        try {
            TrulyMadlySQLiteHandler.getDatabase(ctx).update(ConstantsDB.MESSAGES_TABLE, values, whereClause, whereArgs);
        } catch (DbIsNullException ignored) {
        }
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static void changeMessageState(String user_id, String match_id, Context ctx, String messageId,
                                          MessageState toMessageState) {
        if (Utility.isSet(user_id) && Utility.isSet(match_id)) {
            ContentValues values = new ContentValues();
            values.put("message_state", toMessageState.name());
            String whereClause = "user_id=? and match_id=? and message_id=?";
            String[] whereArgs = new String[]{user_id, match_id, messageId};
            try {
                TrulyMadlySQLiteHandler.getDatabase(ctx).update(ConstantsDB.MESSAGES_TABLE, values, whereClause, whereArgs);
            } catch (DbIsNullException ignored) {
            }
        }
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static void markAllSendingMessagesAsNotSent(String user_id,
                                                       String match_id, Context ctx,
                                                       boolean excludeImageType) {
        if (Utility.isSet(user_id) && Utility.isSet(match_id)) {
            ContentValues values = new ContentValues();
            values.put("message_state",
                    MessageState.OUTGOING_FAILED.toString());
            String excludeCondition = (excludeImageType) ?
                    ("' and message_type != '" + MessageModal.MessageType.IMAGE.name()) : "";
            String whereClause = "user_id=? and match_id=? and message_state='"
                    + MessageState.OUTGOING_SENDING.toString()
                    + excludeCondition
                    + "' ";
            String[] whereArgs = new String[]{user_id, match_id};
            try {
                TrulyMadlySQLiteHandler.getDatabase(ctx).update(ConstantsDB.MESSAGES_TABLE, values, whereClause, whereArgs);
            } catch (DbIsNullException ignored) {
            }
        }
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static void markAllSendingMessagesAsNotSent(String user_id,
                                                       String match_id, Context ctx) {
        markAllSendingMessagesAsNotSent(user_id, match_id, ctx, true);
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static MatchMessageMetaData getMatchMessageMetaData(String user_id,
                                                               String matching_id, Context ctx) {

        MatchMessageMetaData messageDetails = new MatchMessageMetaData();

        if (Utility.isSet(user_id) && Utility.isSet(matching_id)) {
            // http://stackoverflow.com/questions/9341204/android-sqlite-rawquery-parameters
            Cursor cursor = null;
            try {
                cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).query(ConstantsDB.MESSAGE_METADATA_TABLE, null,
                        "user_id=? and match_id=?",
                        new String[]{user_id, matching_id}, null, null, null,
                        "1");


                if (cursor != null && cursor.getCount() > 0
                        && cursor.moveToFirst()) {
                    messageDetails.setUserId(
                            cursor.getString(cursor.getColumnIndex("user_id")));
                    messageDetails.setMatchId(
                            cursor.getString(cursor.getColumnIndex("match_id")));
                    messageDetails.setLastSeenReceiverTstamp(cursor.getString(
                            cursor.getColumnIndex("last_seen_receiver_tstamp")));
                    messageDetails.setLastFetchedId(cursor
                            .getString(cursor.getColumnIndex("last_fetched_id")));
                    messageDetails.setProfileUrl(
                            cursor.getString(cursor.getColumnIndex("profile_url")));
                    messageDetails.setFName(
                            cursor.getString(cursor.getColumnIndex("fname")));
                    messageDetails.setProfilePic(
                            cursor.getString(cursor.getColumnIndex("profile_pic")));
                    messageDetails.setLastFetchedTstamp(cursor.getString(
                            cursor.getColumnIndex("last_fetched_tstamp")));
                    String dateString = cursor.getString(
                            cursor.getColumnIndex("last_updated_tstamp"));
                    if (Utility.isSet(dateString)) {
                        messageDetails.setLastUpdateTstamp(
                                TimeUtils.getParsedTime(dateString));
                    }
                    dateString = cursor.getString(
                            cursor.getColumnIndex("last_chat_read_sent_tstamp"));
                    if (Utility.isSet(dateString)) {
                        messageDetails.setLast_chat_read_sent_tstamp(
                                TimeUtils.getParsedTime(dateString));
                    }

                    dateString = cursor.getString(
                            cursor.getColumnIndex("user_last_quiz_action_time"));
                    if (Utility.isSet(dateString)) {
                        messageDetails.setUserLastQuizActionTime(dateString);
                    }

                    dateString = cursor.getString(
                            cursor.getColumnIndex("match_last_quiz_action_time"));
                    if (Utility.isSet(dateString)) {
                        messageDetails.setMatchLastQuizActionTime(dateString);
                    }

                    String age = cursor.getString(cursor.getColumnIndex("age"));
                    if (Utility.isSet(age)) {
                        messageDetails.setAge(age);
                    }

                    String message_link = cursor
                            .getString(cursor.getColumnIndex("message_link"));
                    if (Utility.isSet(message_link)) {
                        messageDetails.setMessage_link(message_link);
                    }

                    String isMissTmString = cursor
                            .getString(cursor.getColumnIndex("is_miss_tm"));
                    if (Utility.stringCompare(isMissTmString, "1")) {
                        messageDetails.setIsMissTm(true);
                    }

                    int isCDClickable = cursor.getInt(cursor.getColumnIndex(Constants.CD_KEY_CLICKABLE));
                    messageDetails.setmCDClickableState(isCDClickable);

                    String doodleUrl = cursor
                            .getString(cursor.getColumnIndex("doodle_url"));
                    if (Utility.isSet(doodleUrl)) {
                        messageDetails.setDoodleUrl(doodleUrl);
                    }

                    messageDetails.setSparkReceived(cursor.getInt(
                            cursor.getColumnIndex(
                                    ConstantsDB.MESSAGE_METADATA.IS_SPARK_RECEIVED)) == 1);

                    messageDetails.setMutualSpark(cursor.getInt(
                            cursor.getColumnIndex(
                                    ConstantsDB.MESSAGE_METADATA.IS_MUTUAL_SPARK)) == 1);

                    messageDetails.setSelectMatch(cursor.getInt(
                            cursor.getColumnIndex(
                                    ConstantsDB.MESSAGE_METADATA.IS_SELECT_MATCH)) == 1);

                    cursor.close();
                    return messageDetails;
                }

            } catch (DbIsNullException | SQLiteCantOpenDatabaseException ignored) {

            }
            if (cursor != null)
                cursor.close();
        }
        if (!Utility.isSet(messageDetails.getMatchId())) {
            messageDetails.setUserId(user_id);
            messageDetails.setMatchId(matching_id);
        }
        return messageDetails;
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static void insertMessageDetails(MatchMessageMetaData match_details,
                                            Context ctx) {

        SQLiteDatabase db;
        try {
            db = TrulyMadlySQLiteHandler.getDatabase(ctx);
        } catch (DbIsNullException e) {
            return;
        }
        insertMessageDetails(db, match_details);
    }

    //@Trace(category = MetricCategory.DATABASE)
    private static void insertMessageDetails(SQLiteDatabase db, MatchMessageMetaData match_details) {

        if (!Utility.isSet(match_details.getMatchId())
                || !Utility.isSet(match_details.getFName())) {
            return;
        }

        Date last_updated_tstamp = match_details.getLastUpdateTstamp();
        Date last_chat_read_sent_tstamp = match_details
                .getLastChatReadSentTstamp();
        String age = match_details.getAge();

        String columns = "match_id,user_id,fname,profile_pic,is_miss_tm";
        String updateString = "user_id=?,fname=?,profile_pic=?,is_miss_tm=?";
        String valuesQ = "?,?,?,?,?";
        int len = 5;

        if (match_details.getLastSeenReceiverTstamp() != null) {
            columns += ",last_seen_receiver_tstamp";
            updateString += ",last_seen_receiver_tstamp=?";
            valuesQ += ",?";
            len++;
        }
        if (match_details.getLastFetchedId() != null) {
            columns += ",last_fetched_id";
            updateString += ",last_fetched_id=?";
            valuesQ += ",?";
            len++;
        }
        if (match_details.getProfileUrl() != null) {
            columns += ",profile_url";
            updateString += ",profile_url=?";
            valuesQ += ",?";
            len++;
        }
        if (match_details.getLastFetchedTstamp() != null) {
            columns += ",last_fetched_tstamp";
            updateString += ",last_fetched_tstamp=?";
            valuesQ += ",?";
            len++;
        }
        if (last_updated_tstamp != null) {
            columns += ",last_updated_tstamp";
            updateString += ",last_updated_tstamp=?";
            valuesQ += ",?";
            len++;
        }
        if (last_chat_read_sent_tstamp != null) {
            columns += ",last_chat_read_sent_tstamp";
            updateString += ",last_chat_read_sent_tstamp=?";
            valuesQ += ",?";
            len++;
        }
        if (Utility.isSet(age)) {
            columns += ",age";
            updateString += ",age=?";
            valuesQ += ",?";
            len++;
        }
        if (Utility.isSet(match_details.getMessage_link())) {
            columns += ",message_link";
            updateString += ",message_link=?";
            valuesQ += ",?";
            len++;
        }
        if (match_details.isSparkReceived()) {
            columns += "," + ConstantsDB.MESSAGE_METADATA.IS_SPARK_RECEIVED;
            updateString += "," + ConstantsDB.MESSAGE_METADATA.IS_SPARK_RECEIVED + "=?";
            valuesQ += ",?";
            len++;
        }
        if (match_details.isMutualSpark()) {
            columns += "," + ConstantsDB.MESSAGE_METADATA.IS_MUTUAL_SPARK;
            updateString += "," + ConstantsDB.MESSAGE_METADATA.IS_MUTUAL_SPARK + "=?";
            valuesQ += ",?";
            len++;
        }

        columns += "," + ConstantsDB.MESSAGE_METADATA.IS_SELECT_MATCH;
        updateString += "," + ConstantsDB.MESSAGE_METADATA.IS_SELECT_MATCH + "=?";
        valuesQ += ",?";
        len++;

        String[] args = new String[len];
        String[] argsUpdate = new String[len];
        int i = 0;
        args[i] = match_details.getMatchId();
        i++;

        argsUpdate[i - 1] = match_details.getUserId();
        args[i] = match_details.getUserId();
        i++;

        argsUpdate[i - 1] = match_details.getFName();
        args[i] = match_details.getFName();
        i++;

        argsUpdate[i - 1] = match_details.getProfilePic();
        args[i] = match_details.getProfilePic();
        i++;

        argsUpdate[i - 1] = match_details.isMissTm() ? "1" : "0";
        args[i] = match_details.isMissTm() ? "1" : "0";
        i++;

        if (match_details.getLastSeenReceiverTstamp() != null) {
            argsUpdate[i - 1] = match_details.getLastSeenReceiverTstamp();
            args[i] = match_details.getLastSeenReceiverTstamp();
            i++;
        }

        if (match_details.getLastFetchedId() != null) {
            argsUpdate[i - 1] = match_details.getLastFetchedId();
            args[i] = match_details.getLastFetchedId();
            i++;
        }

        if (match_details.getProfileUrl() != null) {
            argsUpdate[i - 1] = match_details.getProfileUrl();
            args[i] = match_details.getProfileUrl();
            i++;
        }

        if (match_details.getLastFetchedTstamp() != null) {
            argsUpdate[i - 1] = match_details.getLastFetchedTstamp();
            args[i] = match_details.getLastFetchedTstamp();
            i++;
        }

        if (last_updated_tstamp != null) {
            argsUpdate[i - 1] = TimeUtils.getFormattedTime(last_updated_tstamp);
            args[i] = TimeUtils.getFormattedTime(last_updated_tstamp);
            i++;
        }
        if (last_chat_read_sent_tstamp != null) {
            argsUpdate[i - 1] = TimeUtils
                    .getFormattedTime(last_chat_read_sent_tstamp);
            args[i] = TimeUtils.getFormattedTime(last_chat_read_sent_tstamp);
            i++;
        }
        if (Utility.isSet(age)) {
            argsUpdate[i - 1] = age;
            args[i] = age;
            i++;
        }
        if (Utility.isSet(match_details.getMessage_link())) {
            argsUpdate[i - 1] = match_details.getMessage_link();
            args[i] = match_details.getMessage_link();
            i++;
        }
        if (match_details.isSparkReceived()) {
            argsUpdate[i - 1] = match_details.isSparkReceived() ? "1" : "0";
            args[i] = match_details.isSparkReceived() ? "1" : "0";
            i++;
        }
        if (match_details.isMutualSpark()) {
            argsUpdate[i - 1] = match_details.isMutualSpark() ? "1" : "0";
            args[i] = match_details.isMutualSpark() ? "1" : "0";
            i++;
        }

        argsUpdate[i - 1] = match_details.isSelectMatch() ? "1" : "0";
        args[i] = match_details.isSelectMatch() ? "1" : "0";
        i++;

        argsUpdate[i - 1] = match_details.getMatchId();

        db.execSQL("INSERT OR IGNORE INTO " + ConstantsDB.MESSAGE_METADATA_TABLE + " ("
                + columns + ") VALUES (" + valuesQ + ");", args);
        db.execSQL("UPDATE " + ConstantsDB.MESSAGE_METADATA_TABLE + " SET " + updateString
                + " where match_id=?;", argsUpdate);

    }

    //@Trace(category = MetricCategory.DATABASE)
    public static void insertAllConversations(String myId,
                                              ArrayList<ConversationModal> conversationList, Context ctx) {
        if (!Utility.isSet(myId)) {
            return;
        }
        String sql = "INSERT OR REPLACE INTO conversation_list (user_id,match_id,tstamp,sort_tstamp,last_message_type,last_message_state,last_message_text) VALUES (?,?,?,?,?,?,?);";
        SQLiteDatabase db;
        try {
            db = TrulyMadlySQLiteHandler.getDatabase(ctx);
        } catch (DbIsNullException e) {
            return;
        }

        SQLiteStatement statement = db.compileStatement(sql);
        db.beginTransaction();
        for (ConversationModal conversation : conversationList) {

            statement.clearBindings();
            statement.bindString(1, myId);
            statement.bindString(2, conversation.getMatch_id());

            statement.bindString(3,
                    TimeUtils.getFormattedTime(conversation.getTstamp()));
            statement.bindString(4,
                    TimeUtils.getFormattedTime(conversation.getTstamp()));


            statement.bindString(5,
                    conversation.getLast_message_type().toString());
            statement.bindString(6,
                    conversation.getLastMessageState().toString());
//
//            if (conversation.getIsChatCleared()) {
//                statement.bindString(7, " ");
//            } else {
//
//                statement.bindString(7, conversation.getLast_message());
//            }

            statement.bindString(7, conversation.getLast_message());
            statement.execute();
        }
        db.setTransactionSuccessful();
        db.endTransaction();

        for (ConversationModal conversation : conversationList) {
            if (conversation
                    .getLastMessageState() == MessageState.INCOMING_READ) {
                setChatReadIncoming(ctx, myId, conversation.getMatch_id());
            }
        }

        MatchMessageMetaData match_details = null;
        for (ConversationModal conversation : conversationList) {
            match_details = new MatchMessageMetaData();
            match_details.setUserId(myId);
            match_details.setMatchId(conversation.getMatch_id());
            match_details.setFName(conversation.getFname());
            match_details.setProfilePic(conversation.getProfile_pic());
            match_details.setAge(conversation.getAge());
            match_details.setMessage_link(conversation.getMessage_link());
            if (Utility.isSet(conversation.getProfile_link())) {
                match_details.setProfileUrl(conversation.getProfile_link());
            }
            if (conversation.isMissTm()) {
                match_details.setIsMissTm(true);
            }
            match_details.setSparkReceived(conversation.isSparkReceived());
            match_details.setMutualSpark(conversation.isMutualSpark());
            match_details.setSelectMatch(conversation.isSelectMatch());
            insertMessageDetails(db, match_details);
        }
    }

    private static Cursor runCoversationQuery(Context aContext, String user_id, boolean excludeMissTM, String searchConstraint, int limit, boolean unreadOnly) {
        if (Utility.isSet(user_id)) {
            String ignoreMessageStates = "'" + MessageState.BLOCKED_SHOWN.toString()
                    + "','" + MessageState.BLOCKED_UNSHOWN.toString() + "'";
            if (unreadOnly) {
                ignoreMessageStates += ",'" + MessageState.CONVERSATION_FETCHED.toString() + "'";
                ignoreMessageStates += ",'" + MessageState.INCOMING_READ.toString() + "'";
                ignoreMessageStates += ",'" + MessageState.OUTGOING_SENT.toString() + "'";
                ignoreMessageStates += ",'" + MessageState.OUTGOING_DELIVERED.toString() + "'";
                ignoreMessageStates += ",'" + MessageState.OUTGOING_READ.toString() + "'";
                ignoreMessageStates += ",'" + MessageState.MATCH_ONLY.toString() + "'";
            }
            String rawQuery = "SELECT c.user_id,c.match_id,c.match_id as _id,c.tstamp,c.sort_tstamp," +
                    "c.last_message_state,c.last_message_type,c.last_message_text,meta.is_miss_tm," +
                    "meta.fname,meta.age,meta.message_link,meta.profile_url,meta.profile_pic," +
                    "meta.clear_chat_tstamp, " +
                    "meta." + ConstantsDB.MESSAGE_METADATA.IS_SPARK_RECEIVED + ", " +
                    "meta." + ConstantsDB.MESSAGE_METADATA.IS_MUTUAL_SPARK + ", " +
                    "meta." + ConstantsDB.MESSAGE_METADATA.IS_SELECT_MATCH + " " +
                    "from conversation_list c JOIN " +
                    ConstantsDB.MESSAGE_METADATA_TABLE + " meta " +
                    "ON c.match_id = meta.match_id AND c.user_id = meta.user_id " +
                    "where c.user_id=? and c.match_id != 'admin' " +
                    "and meta.fname NOT IN ('null' , 'TrulyMadly Admin') " +
                    (excludeMissTM ? " and meta.is_miss_tm = 0 " : "") +
                    (Utility.isSet(searchConstraint) ? " and meta.fname LIKE ? " : "") +
                    "and c.last_message_state NOT IN ("
                    + ignoreMessageStates + ") ORDER BY c.sort_tstamp desc" +
                    (limit > 0 ? " LIMIT ?" : "") +
                    ";";
            int len = 1;
            len += Utility.isSet(searchConstraint) ? 1 : 0;
            len += limit > 0 ? 1 : 0;

            String[] params = new String[len];
            int i = 0;
            params[i] = user_id;
            if (Utility.isSet(searchConstraint)) params[++i] = searchConstraint + "%";
            if (limit > 0) params[++i] = limit + "";

            try {
                return TrulyMadlySQLiteHandler.getDatabase(aContext).rawQuery(rawQuery, params
                );
            } catch (DbIsNullException e) {
                return null;
            }
        } else {
            return null;
        }
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static int getUnreadConversationCount(String myId, Context ctx) {
        int count = 0;
        if (!Utility.isSet(myId)) {
            return 0;
        }
        Cursor cursor = runCoversationQuery(ctx, myId, false, null, 0, true);
        if (cursor != null) {
            count = cursor.getCount();
        }
        if (cursor != null) {
            cursor.close();
        }
        return count;
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static int getRealMutualMatchesCount(String myId, Context ctx) {
        int count = 0;
        if (!Utility.isSet(myId)) {
            return 0;
        }
        Cursor cursor = runCoversationQuery(ctx, myId, true, null, 0, false);
        if (cursor != null) {
            count = cursor.getCount();
        }
        if (cursor != null) {
            cursor.close();
        }
        return count;
    }


    public static Cursor getConversationDetails(String user_id, Context aContext) {
        return runCoversationQuery(aContext, user_id, false, null, 0, false);
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static Cursor getConversationDetails(String user_id, Context aContext, String constraint) {
        return runCoversationQuery(aContext, user_id, false, constraint, 0, false);
    }


    //@Trace(category = MetricCategory.DATABASE)
    public static ArrayList<ActiveConversationModal> getActiveConversations(Context aContext, String user_id, int limit) {
        ArrayList<ActiveConversationModal> activeConversations = new ArrayList<>();
        if (Utility.isSet(user_id)) {
            Cursor cursor = runCoversationQuery(aContext, user_id, true, null, limit, false);
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    activeConversations.add(
                            new ActiveConversationModal(
                                    cursor.getString(cursor.getColumnIndex("match_id")),
                                    Utility.stringCompare(cursor.getString(cursor.getColumnIndex("is_miss_tm")), "1"),
                                    cursor.getString(cursor.getColumnIndex("fname")),
                                    cursor.getString(cursor.getColumnIndex("age")),
                                    cursor.getString(cursor.getColumnIndex("message_link")),
                                    cursor.getString(cursor.getColumnIndex("profile_url")),
                                    cursor.getString(cursor.getColumnIndex("profile_pic"))
                            )
                    );
                }
                cursor.close();
            }
        }
        return activeConversations;
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static String getLastMessageTstampForMatch(String user_id,
                                                      String match_id, Context ctx) {
        String tStamp = null;
        if (Utility.isSet(user_id) && Utility.isSet(match_id)) {
            Cursor cursor = null;
            try {
                cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).rawQuery(
                        "SELECT max(tstamp) as max_tstamp FROM " + ConstantsDB.MESSAGES_TABLE + " where user_id=? and match_id=?",
                        new String[]{user_id, match_id});
            } catch (DbIsNullException e) {
                return tStamp;
            }
            if (cursor != null && cursor.getCount() > 0
                    && cursor.moveToFirst()) {
                int index = cursor.getColumnIndex("max_tstamp");
                if (index >= 0) {
                    tStamp = cursor.getString(index);
                }
            }
            if (cursor != null)
                cursor.close();
        }
        return tStamp;
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static String getFirstCDASKMessageTstamp(String user_id, Context ctx) {
        String tStamp = null;

        String messageStatesReceived = "'"
                + MessageState.INCOMING_DELIVERED.toString() + "','"
                + MessageState.INCOMING_READ.toString() + "'";

        if (Utility.isSet(user_id)) {
            Cursor cursor = null;
            try {
                cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).rawQuery(
                        "select tstamp from " + ConstantsDB.MESSAGES_TABLE + " where user_id = ? and message_type = ? and tstamp > ? and message_state IN (" +
                                messageStatesReceived + ") and message_text like '" + ctx.getResources().getString(R.string.ask_out_message) + "%' order by tstamp asc limit 1;",
                        new String[]{user_id, MessageModal.MessageType.TEXT.toString(), "2016-01-01 00:00:00"});
            } catch (DbIsNullException e) {
                return tStamp;
            }
            if (cursor != null && cursor.getCount() > 0
                    && cursor.moveToFirst()) {
                int index = cursor.getColumnIndex("tstamp");
                if (index >= 0) {
                    tStamp = cursor.getString(index);
                }
            }
            if (cursor != null)
                cursor.close();
        }
        return tStamp;
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static MessageModal getAMessage(String user_id, String messageId, Context ctx) {
        MessageModal messageModal = null;
        String ignoreMessageStates = "'" + MessageState.GCM_FETCHED.toString()
                + "','" + MessageState.CONVERSATION_FETCHED.toString() + "','"
                + MessageState.CONVERSATION_FETCHED_NEW.toString() + "','"
                + MessageState.BLOCKED_SHOWN.toString() + "'";

        if (Utility.isSet(user_id) && Utility.isSet(messageId)) {
            Cursor cursor = null;
            try {
                cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).rawQuery(
                        "SELECT *, message_id as _id FROM " + ConstantsDB.MESSAGES_TABLE +
                                " where user_id=? "
                                + "and message_id=? "
                                + "and message_state NOT IN ("
                                + ignoreMessageStates + ") order by tstamp asc;",
                        new String[]{user_id, messageId});

            } catch (DbIsNullException e) {
                return null;
            }
            if (cursor != null && cursor.getCount() == 1) {
                messageModal = new MessageModal(user_id);
                cursor.moveToFirst();
                messageModal.parseMessage(cursor);
            }

            if (cursor != null) {
                cursor.close();
            }
        }

        return messageModal;
    }

    public static MessageModal getLastMessage(String user_id, String matchId, Context ctx) {
        MessageModal messageModal = null;
        String ignoreMessageStates = "'" + MessageState.GCM_FETCHED.toString()
                + "','" + MessageState.CONVERSATION_FETCHED.toString() + "','"
                + MessageState.CONVERSATION_FETCHED_NEW.toString() + "','"
                + MessageState.BLOCKED_SHOWN.toString() + "'";

        if (Utility.isSet(user_id) && Utility.isSet(matchId)) {
            Cursor cursor = null;
            try {
                cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).rawQuery(
                        "SELECT *, message_id as _id FROM " + ConstantsDB.MESSAGES_TABLE +
                                " where user_id=? "
                                + "and match_id=? "
                                + "and message_state NOT IN ("
                                + ignoreMessageStates + ") order by tstamp asc LIMIT 1;",
                        new String[]{user_id, matchId});

            } catch (DbIsNullException e) {
                return null;
            }
            if (cursor != null && cursor.getCount() == 1) {
                messageModal = new MessageModal(user_id);
                cursor.moveToFirst();
                messageModal.parseMessage(cursor);
            }

            if (cursor != null) {
                cursor.close();
            }
        }

        return messageModal;
    }


    //@Trace(category = MetricCategory.DATABASE)
    public static Cursor getAllMessages(String user_id, String match_id,
                                        Context ctx) {
        return getAllMessages(user_id, match_id, -1, ctx);
    }

    //@Trace(category = MetricCategory.DATABASE)
    private static Cursor getAllMessages(String user_id, String match_id, int slots,
                                         Context ctx) {
        String ignoreMessageStates = "'" + MessageState.GCM_FETCHED.toString()
                + "','" + MessageState.CONVERSATION_FETCHED.toString() + "','"
                + MessageState.CONVERSATION_FETCHED_NEW.toString() + "','"
                + MessageState.BLOCKED_SHOWN.toString() + "'";

        String limitQuery = "";
        if (slots != -1) {
            limitQuery = " LIMIT " + slots * Constants.DEFAULT_SLOT_SIZE;
        }

        String clear_chat_tstamp = getClearChatTstamp(ctx, match_id);
        if (Utility.isSet(user_id) && Utility.isSet(match_id)) {
            Cursor cursor = null;
            try {
                if (Utility.isSet(clear_chat_tstamp)) {
                    cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).rawQuery(
                            "SELECT * FROM ("
                                    + "SELECT *, message_id as _id FROM " + ConstantsDB.MESSAGES_TABLE
                                    + " where user_id=? and match_id=? and tstamp>? "
                                    + "and message_state NOT IN ("
                                    + ignoreMessageStates + ") order by tstamp desc " + limitQuery
                                    + " ) ORDER BY tstamp asc"
                                    + " ;",
                            new String[]{user_id, match_id, clear_chat_tstamp});
                } else {
                    cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).rawQuery(
                            "SELECT * FROM ("
                                    + "SELECT *, message_id as _id FROM " + ConstantsDB.MESSAGES_TABLE
                                    + " where user_id=? and match_id=? and message_state NOT IN ("
                                    + ignoreMessageStates + ") order by tstamp desc " + limitQuery
                                    + " ) ORDER BY tstamp asc"
                                    + " ;",
                            new String[]{user_id, match_id});
                }

            } catch (DbIsNullException e) {
                return null;
            }
            return cursor;
        } else {
            return null;
        }
    }

    public static ArrayList<MessageModal> getAllMessagesModals(String user_id, String match_id, int slots,
                                                               Context ctx) {
        Cursor cursor = getAllMessages(user_id, match_id, slots, ctx);
        ArrayList<MessageModal> messageModals = new ArrayList<>();
        if (cursor != null) {
            while (cursor.moveToNext()) {
                MessageModal messageModal = new MessageModal(user_id);
                messageModal.parseMessage(cursor);
                messageModals.add(messageModal);
            }
            cursor.close();
        }

        return messageModals;
    }

    public static int getTotalRecords(String user_id, String match_id,
                                      Context ctx) {
        int totalRecords = 0;
        String ignoreMessageStates = "'" + MessageState.GCM_FETCHED.toString()
                + "','" + MessageState.CONVERSATION_FETCHED.toString() + "','"
                + MessageState.CONVERSATION_FETCHED_NEW.toString() + "','"
                + MessageState.BLOCKED_SHOWN.toString() + "'";

        String clear_chat_tstamp = getClearChatTstamp(ctx, match_id);
        if (Utility.isSet(user_id) && Utility.isSet(match_id)) {
            Cursor cursor = null;
            try {
                if (Utility.isSet(clear_chat_tstamp)) {
                    cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).rawQuery(
                            "SELECT count(*) as num FROM " + ConstantsDB.MESSAGES_TABLE
                                    + " where user_id=? and match_id=? and tstamp>? "
                                    + "and message_state NOT IN ("
                                    + ignoreMessageStates + ") "
                                    + " ;",
                            new String[]{user_id, match_id, clear_chat_tstamp});
                } else {
                    cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).rawQuery(
                            "SELECT count(*) as num FROM " + ConstantsDB.MESSAGES_TABLE
                                    + " where user_id=? and match_id=? and message_state NOT IN ("
                                    + ignoreMessageStates + ") "
                                    + " ;",
                            new String[]{user_id, match_id});
                }

            } catch (DbIsNullException ignored) {

            }
            if (cursor != null) {
                cursor.moveToNext();
                totalRecords = cursor.getInt(cursor.getColumnIndex("num"));
                cursor.close();
            }
        }

        return totalRecords;
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static ArrayList<String> getMatchIdsWithNullValue(String user_id,
                                                             Context ctx) {
        ArrayList<String> matchesIdList = null;
        if (Utility.isSet(user_id)) {
            // http://stackoverflow.com/questions/5707348/how-to-use-rawquery-in-android
            Cursor cursor = null;
            try {
                cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).rawQuery(
                        "select match_id from " + ConstantsDB.MESSAGE_METADATA_TABLE + " where user_id=? and match_id != 'admin' and (fname is null or fname = 'null' or age is null or age = 'null');",
                        new String[]{user_id});
            } catch (DbIsNullException e) {
                return matchesIdList;
            }
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    if (matchesIdList == null) {
                        matchesIdList = new ArrayList<>();
                    }
                    matchesIdList.add(cursor
                            .getString(cursor.getColumnIndex("match_id")));
                }
                cursor.close();
            }
        }
        return matchesIdList;
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static void deleteMatchIdsWithNullValue(String user_id,
                                                   Context ctx) {
        if (Utility.isSet(user_id)) {
            try {
                TrulyMadlySQLiteHandler.getDatabase(ctx).execSQL("delete from " + ConstantsDB.MESSAGE_METADATA_TABLE
                        + " where user_id=? and match_id != 'admin' "
                        + "and (fname is null or fname = 'null' or age is null or age = 'null');", new String[]{user_id});
//                TrulyMadlySQLiteHandler.getDatabase(ctx).rawQuery(
//                        "delete from " + ConstantsDB.MESSAGE_METADATA_TABLE + " where user_id=? and match_id != 'admin' and (fname is null or fname = 'null' or age is null or age = 'null');",
//                        new String[]{user_id});
            } catch (DbIsNullException ignored) {
            }
        }
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static String getTstampForEmptyMessageRefresh(String user_id,
                                                         String matching_id, Context ctx) {
        if (Utility.isSet(user_id) && Utility.isSet(matching_id)) {
            // http://stackoverflow.com/questions/5707348/how-to-use-rawquery-in-android
            Cursor cursor = null;
            try {
                cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).rawQuery(
                        "select count(*) as count from " + ConstantsDB.MESSAGES_TABLE + " where user_id=? and match_id=? and message_state = '"
                                + MessageState.GCM_FETCHED.toString() + "'",
                        new String[]{user_id, matching_id});

                int count = 0;
                try {
                    cursor.moveToFirst();
                    count = cursor.getInt(cursor.getColumnIndex("count"));
                } catch (RuntimeException ignored) {
                } finally {
                    if (cursor != null)
                        cursor.close();
                }
                if (count > 0) {
                    cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).rawQuery(
                            "select tstamp from " + ConstantsDB.MESSAGES_TABLE + " where user_id=? and match_id=? order by tstamp asc limit 1",
                            new String[]{user_id, matching_id});
                    String tstamp = null;
                    try {
                        cursor.moveToFirst();
                        tstamp = cursor.getString(cursor.getColumnIndex("tstamp"));
                    } catch (RuntimeException ignored) {
                    } finally {
                        if (cursor != null)
                            cursor.close();
                    }
                    return tstamp;
                } else {
                    return null;
                }
            } catch (DbIsNullException e) {
                return null;
            }
        } else {
            return null;
        }

    }

    //@Trace(category = MetricCategory.DATABASE)
    public static String getLastMessageReceivedTstamp(String user_id,
                                                      Context ctx) {
        String tStamp = null;
        if (Utility.isSet(user_id)) {
            String messageStatesReceived = "'"
                    + MessageState.INCOMING_DELIVERED.toString() + "','"
                    + MessageState.INCOMING_READ.toString() + "'";
            Cursor cursor = null;
            try {
                cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).rawQuery(
                        "SELECT max(tstamp) as max_tstamp FROM " + ConstantsDB.MESSAGES_TABLE + " where user_id=? and message_state IN ("
                                + messageStatesReceived + ") and fetch_source='"
                                + FetchSource.SOCKET.toString() + "';",
                        new String[]{user_id});
            } catch (DbIsNullException e) {
                return tStamp;
            }
            try {
                if (cursor != null && cursor.getCount() > 0
                        && cursor.moveToFirst()) {
                    tStamp = cursor
                            .getString(cursor.getColumnIndex("max_tstamp"));
                }
            } catch (SQLiteDatabaseCorruptException | IllegalStateException ignored) {

            }

            if (cursor != null)
                cursor.close();
        }
        return tStamp;
    }

    //@Trace(category = MetricCategory.DATABASE)
    private static void editColumnValueInTable(Context ctx, String user_id,
                                               String match_id, String tableName, String columnName, String oldVal,
                                               String newVal) {
        ContentValues values = new ContentValues();
        values.put(columnName, newVal);
        String whereClause = "user_id=? and match_id=? and " + columnName + "='"
                + oldVal + "'";
        String[] whereArgs = new String[]{user_id, match_id};
        try {
            TrulyMadlySQLiteHandler.getDatabase(ctx).update(tableName, values, whereClause, whereArgs);
        } catch (DbIsNullException ignored) {
        }
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static void setChatReadIncoming(Context ctx, String user_id,
                                           String match_id) {
        editColumnValueInTable(ctx, user_id, match_id, ConstantsDB.MESSAGES_TABLE,
                "message_state", MessageState.INCOMING_DELIVERED.toString(),
                MessageState.INCOMING_READ.toString());
        editColumnValueInTable(ctx, user_id, match_id, ConstantsDB.MESSAGES_TABLE,
                "message_state", MessageState.MATCH_ONLY_NEW.toString(),
                MessageState.MATCH_ONLY.toString());
        editColumnValueInTable(ctx, user_id, match_id, ConstantsDB.MESSAGES_TABLE,
                "message_state",
                MessageState.CONVERSATION_FETCHED_NEW.toString(),
                MessageState.CONVERSATION_FETCHED.toString());

        // update chat read in convseration_list table
        editColumnValueInTable(ctx, user_id, match_id, ConstantsDB.CONVERSATION_LIST_TABLE,
                "last_message_state",
                MessageState.INCOMING_DELIVERED.toString(),
                MessageState.INCOMING_READ.toString());
        editColumnValueInTable(ctx, user_id, match_id, ConstantsDB.CONVERSATION_LIST_TABLE,
                "last_message_state", MessageState.MATCH_ONLY_NEW.toString(),
                MessageState.MATCH_ONLY.toString());
        editColumnValueInTable(ctx, user_id, match_id, ConstantsDB.CONVERSATION_LIST_TABLE,
                "last_message_state",
                MessageState.CONVERSATION_FETCHED_NEW.toString(),
                MessageState.CONVERSATION_FETCHED.toString());

    }

    //@Trace(category = MetricCategory.DATABASE)
    public static void setChatUnreadIncomingInConversationList(Context ctx, String user_id,
                                                               String match_id) {

        // update chat read in convseration_list table
        editColumnValueInTable(ctx, user_id, match_id, ConstantsDB.CONVERSATION_LIST_TABLE,
                "last_message_state",
                MessageState.INCOMING_READ.toString(),
                MessageState.INCOMING_DELIVERED.toString());
        editColumnValueInTable(ctx, user_id, match_id, ConstantsDB.CONVERSATION_LIST_TABLE,
                "last_message_state", MessageState.MATCH_ONLY.toString(),
                MessageState.MATCH_ONLY_NEW.toString());
        editColumnValueInTable(ctx, user_id, match_id, ConstantsDB.CONVERSATION_LIST_TABLE,
                "last_message_state",
                MessageState.CONVERSATION_FETCHED.toString(),
                MessageState.CONVERSATION_FETCHED_NEW.toString());

    }

    //@Trace(category = MetricCategory.DATABASE)
    public static void setChatReadOutgoing(Context ctx, String user_id,
                                           String match_id, String last_seen_msg_time) {
        boolean isTimeSet = Utility.isSet(last_seen_msg_time);
        String messageStatesOutgoing = "'"
                + MessageState.OUTGOING_DELIVERED.toString() + "','"
                + MessageState.OUTGOING_SENT.toString() + "'";
        try {
            ContentValues values = new ContentValues();
            values.put("message_state", MessageState.OUTGOING_READ.toString());
            String[] whereArgs = new String[]{user_id, match_id};
            String[] whereArgsWithTstamp = new String[]{user_id, match_id,
                    last_seen_msg_time};
            String whereClause = "user_id=? and match_id=? and message_state IN ("
                    + messageStatesOutgoing + ")"
                    + (isTimeSet ? " and tstamp <= ? ;" : ";");

            TrulyMadlySQLiteHandler.getDatabase(ctx).update(ConstantsDB.MESSAGES_TABLE, values, whereClause,
                    isTimeSet ? whereArgsWithTstamp : whereArgs);


            values = new ContentValues();
            values.put("last_message_state", MessageState.OUTGOING_READ.toString());
            whereClause = "user_id=? and match_id=? and last_message_state='"
                    + MessageState.OUTGOING_DELIVERED.toString() + "'"
                    + (isTimeSet ? " and tstamp <= ? ;" : ";");
            TrulyMadlySQLiteHandler.getDatabase(ctx).update(ConstantsDB.CONVERSATION_LIST_TABLE, values, whereClause,
                    isTimeSet ? whereArgsWithTstamp : whereArgs);
        } catch (DbIsNullException ignored) {
        }
    }


    //@Trace(category = MetricCategory.DATABASE)
    public static void deleteAllMessages(Context ctx, String match_id) {

        String whereClause = "match_id=? and user_id=?";
        String[] whereArgs = new String[]{match_id, Utility.getMyId(ctx)};
        try {
            TrulyMadlySQLiteHandler.getDatabase(ctx).delete(ConstantsDB.MESSAGES_TABLE, whereClause, whereArgs);
        } catch (DbIsNullException ignored) {
        }

    }

    //@Trace(category = MetricCategory.DATABASE)
    public static void updateClearChatTstampForAllMatches(Context ctx, JSONObject response) {

        JSONArray cleared_data = response.optJSONArray("cleared_data");
        if (cleared_data == null || cleared_data.length() == 0) {
            return;
        }

        String sql = "INSERT OR REPLACE INTO " + ConstantsDB.MESSAGE_METADATA_TABLE + " (user_id,match_id,clear_chat_tstamp) VALUES (?,?,?);";
        SQLiteDatabase db;
        try {
            db = TrulyMadlySQLiteHandler.getDatabase(ctx);
        } catch (DbIsNullException e) {
            return;
        }

        SQLiteStatement statement = db.compileStatement(sql);
        db.beginTransaction();


        for (int i = 0; i < cleared_data.length(); i++) {

            JSONObject clearedChatObject = cleared_data.optJSONObject(i);
            String userId1 = null, userId2 = null, timestamp = null;
            if (clearedChatObject != null) {
                userId1 = clearedChatObject.optString("user_id_1");
                userId2 = clearedChatObject.optString("user_id_2");
                timestamp = clearedChatObject.optString("time_stamp");
            }

            if (Utility.isSet(userId1) && Utility.isSet(userId2) && Utility.isSet(timestamp)) {
                statement.clearBindings();
                statement.bindString(1, userId1);
                statement.bindString(2, userId2);
                statement.bindString(3, timestamp);
                statement.execute();
            }
        }
        db.setTransactionSuccessful();
        db.endTransaction();


    }

    //@Trace(category = MetricCategory.DATABASE)
    public static void updateClearChatTstamp(Context ctx, String match_id, String time) {
        ContentValues values;
        String whereClause = "match_id=? and user_id=?";
        String[] whereArgs = new String[]{match_id, Utility.getMyId(ctx)};
        try {
            values = new ContentValues();
            values.put("clear_chat_tstamp", time);
            TrulyMadlySQLiteHandler.getDatabase(ctx).update(ConstantsDB.MESSAGE_METADATA_TABLE, values, whereClause, whereArgs);
        } catch (DbIsNullException ignored) {
        }
    }

    //@Trace(category = MetricCategory.DATABASE)
    private static String getClearChatTstamp(Context ctx, String match_id) {
        String result = "";
        String user_id = Utility.getMyId(ctx);
        if (!Utility.isSet(user_id) || !Utility.isSet(match_id)) {
            return result;
        }
        String whereClause = "match_id=? and user_id=?";
        String[] whereArgs = new String[]{match_id, user_id};
        Cursor cursor;
        String[] columns = new String[]{"clear_chat_tstamp"};
        try {
            cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).query(ConstantsDB.MESSAGE_METADATA_TABLE, columns, whereClause, whereArgs, null, null, null, null);
        } catch (DbIsNullException e) {
            return result;
        }
        if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
            result = cursor
                    .getString(cursor.getColumnIndex("clear_chat_tstamp"));
        }
        if (cursor != null)
            cursor.close();
        return result;
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static HashMap<String, String> fetchLastMessageId(Context ctx, String match_id) {
        HashMap<String, String> result = new HashMap<>();
        Cursor cursor = null;

//        String messageStates = "'"
//                + MessageState.OUTGOING_SENT.toString() + "','"
//                + MessageState.OUTGOING_READ.toString() + "','"
//                + MessageState.OUTGOING_DELIVERED.toString() + "','"
//                + MessageState.INCOMING_DELIVERED.toString() + "','"
//                + MessageState.INCOMING_READ.toString() + "'";
//
//        try {
//            cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).rawQuery(
//                    "SELECT message_id , tstamp  FROM " + ConstantsDB.MESSAGES_TABLE + " where user_id=? and match_id=? and message_state IN ("
//                            + messageStates + ") order by tstamp desc",
//                    new String[]{Utility.getMyId(ctx),match_id});
//        } catch (DbIsNullException e) {
//            return result;
//        }


        String whereClause = "match_id=? and user_id=?";
        String[] whereArgs = new String[]{match_id, Utility.getMyId(ctx)};
        String[] columns = new String[]{"message_id", "tstamp"};
        String orderBy = "tstamp";

        try {
            cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).query(ConstantsDB.MESSAGES_TABLE, columns, whereClause, whereArgs, null, null, orderBy + " DESC");
        } catch (DbIsNullException e) {
            return result;
        }


        if (cursor != null && cursor.getCount() > 0
                && cursor.moveToFirst()) {
            result.put("message_id", cursor
                    .getString(cursor.getColumnIndex("message_id")));
            result.put("tstamp", cursor
                    .getString(cursor.getColumnIndex("tstamp")));
        }

        if (cursor != null)
            cursor.close();

        return result;


    }

    //@Trace(category = MetricCategory.DATABASE)
    public static void setUserBlocked(Context ctx, String match_id) {
        ContentValues values;
        String whereClause = "match_id=?";
        String[] whereArgs = new String[]{match_id};


        try {
            values = new ContentValues();
            values.put("message_state", MessageState.BLOCKED_SHOWN.toString());
            TrulyMadlySQLiteHandler.getDatabase(ctx).update(ConstantsDB.MESSAGES_TABLE, values, whereClause, whereArgs);

            values = new ContentValues();
            values.put("last_message_state", MessageState.BLOCKED_SHOWN.toString());
            TrulyMadlySQLiteHandler.getDatabase(ctx).update(ConstantsDB.CONVERSATION_LIST_TABLE, values, whereClause,
                    whereArgs);
        } catch (DbIsNullException e) {
            return;
        }

        // Removing image from local cache after blocking
        ImageCacheHelper.removeImageWithUniqueKey(match_id);

        //Clearing images of IMAGE messages
        FilesHandler.deleteFileOrDirectoryFromImageAssets(match_id);
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static void updateMessageMetaDataValue(Context ctx, String user_id,
                                                  String match_id, String key, String value) {
        String whereClause = "user_id=? and match_id=?";
        String[] whereArgs = new String[]{user_id, match_id};
        ContentValues values = new ContentValues();
        values.put(key, value);
        try {
            TrulyMadlySQLiteHandler.getDatabase(ctx).update(ConstantsDB.MESSAGE_METADATA_TABLE, values, whereClause,
                    whereArgs);
        } catch (DbIsNullException ignored) {
        }
    }

    public static void updateMessageMetaDataValue(Context ctx, String user_id,
                                                  String match_id, String key, int value) {
        String whereClause = "user_id=? and match_id=?";
        String[] whereArgs = new String[]{user_id, match_id};
        ContentValues values = new ContentValues();
        values.put(key, value);
        try {
            TrulyMadlySQLiteHandler.getDatabase(ctx).update(ConstantsDB.MESSAGE_METADATA_TABLE, values, whereClause,
                    whereArgs);
        } catch (DbIsNullException ignored) {
        }
    }

    public static void updateMessageMetaDataValue(Context ctx, String user_id,
                                                  String match_id, HashMap<String, String> pairs) {
        String whereClause = "user_id=? and match_id=?";
        String[] whereArgs = new String[]{user_id, match_id};
        ContentValues values = new ContentValues();
        for (Map.Entry<String, String> pair : pairs
                .entrySet()) {
            values.put(checkNotNull(pair).getKey(), checkNotNull(pair).getValue());
        }
        try {
            TrulyMadlySQLiteHandler.getDatabase(ctx).update("message_meta_data", values, whereClause,
                    whereArgs);
        } catch (DbIsNullException ignored) {
        }
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static void updateLastQuizActionTime(String user_time,
                                                String match_time, String user_id, String match_id, Context ctx) {
        if (Utility.isSet(user_id) && Utility.isSet(match_id)) {
            ContentValues values = new ContentValues();
            values.put("user_last_quiz_action_time", user_time);
            values.put("match_last_quiz_action_time", match_time);
            String whereClause = "user_id=? and match_id=? ";
            String[] whereArgs = new String[]{user_id, match_id};
            try {
                TrulyMadlySQLiteHandler.getDatabase(ctx).update(ConstantsDB.MESSAGE_METADATA_TABLE, values, whereClause,
                        whereArgs);
            } catch (DbIsNullException ignored) {
            }

        }
    }

    /**
     * @param user_id
     * @param match_id
     * @param ctx
     * @param cdClickableState - DISABLED, CLICKABLE, NOT_CLICKABLE
     */
    //@Trace(category = MetricCategory.DATABASE)
    public static void updateCuratedDealsClickableFlag(String user_id, String match_id, Context ctx,
                                                       MatchMessageMetaData.CDClickableState cdClickableState) {
        if (Utility.isSet(user_id) && Utility.isSet(match_id)) {
            ContentValues values = new ContentValues();
            values.put(Constants.CD_KEY_CLICKABLE, cdClickableState.getKey());
            String whereClause = "user_id=? and match_id=? ";
            String[] whereArgs = new String[]{user_id, match_id};
            try {
                TrulyMadlySQLiteHandler.getDatabase(ctx).update(ConstantsDB.MESSAGE_METADATA_TABLE, values, whereClause,
                        whereArgs);
            } catch (DbIsNullException ignored) {
            }
        }
    }


    /**
     * @param user_id
     * @param match_id
     * @param ctx      checks and returns if the given curated deals is clickable for this user and match combo
     */
    //@Trace(category = MetricCategory.DATABASE)
    public static MatchMessageMetaData.CDClickableState isCuratedDealsClickable(String user_id, String match_id, Context ctx) {
        if (Utility.isSet(user_id) && Utility.isSet(match_id)) {
            Cursor cursor = null;
            try {
                cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).rawQuery(
                        "SELECT " + Constants.CD_KEY_CLICKABLE + " FROM " + ConstantsDB.MESSAGE_METADATA_TABLE + " where user_id=? " +
                                "and match_id=?;", new String[]{user_id, match_id});
            } catch (DbIsNullException e) {
                return MatchMessageMetaData.CDClickableState.NOT_CLICKABLE;
            }

            if (cursor != null && cursor.getCount() == 1 && cursor.moveToNext()) {
                int isClickable = cursor.getInt(cursor.getColumnIndex(Constants.CD_KEY_CLICKABLE));
                cursor.close();
                return MatchMessageMetaData.CDClickableState.createFromInt(isClickable);
            }

            if (cursor != null) {
                cursor.close();
            }
        }

        return MatchMessageMetaData.CDClickableState.NOT_CLICKABLE;
    }

    public static int getMinTwoWayConversationCount(Context ctx, String user_id, String match_id) {

        String outgoingMessageStates = "'" + MessageState.OUTGOING_SENT.toString()
                + "','" + MessageState.OUTGOING_DELIVERED.toString() + "','"
                + "','" + MessageState.OUTGOING_READ.toString() + "'";

        String incomingMessageStates = "'" + MessageState.INCOMING_DELIVERED.toString()
                + "','" + MessageState.INCOMING_READ.toString() + "'";


        int outgoingMessagesCount = 0, incomingMessagesCount = 0;
        if (Utility.isSet(user_id) && Utility.isSet(match_id)) {
            Cursor cursor = null;
            try {
                cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).rawQuery(
                        "SELECT count(*) as count FROM " + ConstantsDB.MESSAGES_TABLE
                                + " where user_id=? and match_id=? and message_state IN ("
                                + outgoingMessageStates + ") "
                                + " ;",
                        new String[]{user_id, match_id});

            } catch (DbIsNullException e) {
                return 0;
            }
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    outgoingMessagesCount = cursor.getInt(cursor.getColumnIndex("count"));
                }
                cursor.close();
            }
            cursor = null;
            try {
                cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).rawQuery(
                        "SELECT count(*) as count FROM " + ConstantsDB.MESSAGES_TABLE
                                + " where user_id=? and match_id=? and message_state IN ("
                                + incomingMessageStates + ") "
                                + " ;",
                        new String[]{user_id, match_id});

            } catch (DbIsNullException e) {
                return 0;
            }
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    incomingMessagesCount = cursor.getInt(cursor.getColumnIndex("count"));
                }
                cursor.close();
            }
        } else {
            return 0;
        }


        return outgoingMessagesCount < incomingMessagesCount ? outgoingMessagesCount : incomingMessagesCount;
    }
}
