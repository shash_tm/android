package com.trulymadly.android.app.sqlite;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.trulymadly.android.Exception.DbIsNullException;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.modal.QuizData;
import com.trulymadly.android.app.modal.QuizImage;
import com.trulymadly.android.app.modal.QuizStatusModal;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class QuizDBHandler {


    //@Trace(category = MetricCategory.DATABASE)
    public static QuizImage getQuizImageUrl(Context ctx, int id) {
        QuizImage qi = null;
        String banner = null, thumb = null;
        String selection = "id=?";
        String[] selectionArgs = new String[]{"" + id};
        String[] columns = new String[]{"banner_url", "image_url"};
        Cursor cursor = null;
        try {
            cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).query("quizzes", columns, selection,
                    selectionArgs, null, null, null, "1");
        } catch (DbIsNullException e) {
            return qi;
        }
        if (cursor != null && cursor.getCount() > 0 && cursor.moveToNext()) {
            banner = cursor.getString(cursor.getColumnIndex("banner_url"));
            thumb = cursor.getString(cursor.getColumnIndex("image_url"));
            qi = new QuizImage(banner,
                    thumb);
        }
        if (cursor != null)
            cursor.close();
        return qi;
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static String getQuizName(Context ctx, int id) {
        String name = "";
        String selection = "id=?";
        String[] selectionArgs = new String[]{"" + id};
        String[] columns = new String[]{"name"};
        Cursor cursor = null;
        try {
            cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).query("quizzes", columns, selection,
                    selectionArgs, null, null, null, "1");
        } catch (DbIsNullException e) {
            return name;
        }
        if (cursor != null && cursor.getCount() > 0 && cursor.moveToNext()) {
            name = cursor.getString(cursor.getColumnIndex("name"));
        }
        if (cursor != null)
            cursor.close();
        return name;
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static boolean isPlayedByMatch(Context ctx, String match_id,
                                          String user_id, int quiz_id) {
        String status = null;
        String selection = "quiz_id=? and user_id=? and match_id=?";
        String[] selectionArgs = new String[]{"" + quiz_id, user_id,
                match_id};
        String[] columns = new String[]{"status"};
        Cursor cursor = null;
        try {
            cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).query("quizStatus", columns, selection,
                    selectionArgs, null, null, null, "1");
        } catch (DbIsNullException e) {
            return false;
        }
        if (cursor != null && cursor.getCount() > 0 && cursor.moveToNext()) {
            status = cursor.getString(cursor.getColumnIndex("status"));
        }
        if (cursor != null)
            cursor.close();

        return status != null && (status.equalsIgnoreCase("MATCH")
                || status.equalsIgnoreCase("BOTH"));
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static boolean isQuizExists(String id, Context ctx) {
        boolean isQuizExists = false;
        String selection = "id=?";
        String[] selectionArgs = new String[]{id};
        Cursor cursor = null;
        try {
            cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).query("quizzes", null, selection,
                    selectionArgs, null, null, null, "1");
        } catch (DbIsNullException e) {
            return isQuizExists;
        }
        if (cursor != null && cursor.getCount() > 0) {
            isQuizExists = true;
        }
        if (cursor != null)
            cursor.close();
        return isQuizExists;
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static void insertQuiz(QuizData quiz, Context ctx) {
        if (Utility.isSet(quiz.getName()) && quiz.getId() > 0
                && quiz.getUpdatedInVersion() > 0) {
            ContentValues values = new ContentValues();
            values.put("id", quiz.getId());
            values.put("name", quiz.getName());
            values.put("updated_in_version", quiz.getUpdatedInVersion());
            values.put("image_url", quiz.getImageUrl());
            values.put("banner_url", quiz.getBannerUrl());
            try {
                TrulyMadlySQLiteHandler.getDatabase(ctx).replace("quizzes", null, values);
            } catch (DbIsNullException ignored) {
            }
        }

    }

    //@Trace(category = MetricCategory.DATABASE)
    public static int returnMaxId(Context ctx) {
        int maxQuizId = 0;
        Cursor cursor = null;
        try {
            cursor = TrulyMadlySQLiteHandler.getDatabase(ctx)
                    .rawQuery("SELECT  MAX(id) as maxId FROM quizzes", null);
        } catch (DbIsNullException e) {
            return maxQuizId;
        }
        if (cursor != null && cursor.getCount() > 0
                && cursor.getColumnIndex("maxId") != -1) {
            cursor.moveToFirst();
            maxQuizId = cursor.getInt(cursor.getColumnIndex("maxId"));
        }
        if (cursor != null) {
            cursor.close();
        }
        return maxQuizId;

    }

    //@Trace(category = MetricCategory.DATABASE)
    public static void updateAttribute(QuizData q, String attributeName,
                                       String attributeValue, Context ctx) {
        ContentValues values = new ContentValues();
        values.put(attributeName, attributeValue);
        String whereClause = "id=?";
        String[] whereArgs = new String[]{"" + q.getId()};
        try {
            TrulyMadlySQLiteHandler.getDatabase(ctx).update("quizzes", values, whereClause, whereArgs);
        } catch (DbIsNullException ignored) {
        }

    }

    //@Trace(category = MetricCategory.DATABASE)
    public static void updateShowAnimationFalse(String user_id, String match_id,
                                                Context ctx) {
        ContentValues values = new ContentValues();
        values.put("showAnimation", "0");
        String whereClause = "user_id=? and match_id=? ";
        String[] whereArgs = new String[]{user_id, match_id};
        try {
            TrulyMadlySQLiteHandler.getDatabase(ctx).update("quizStatus", values, whereClause, whereArgs);
        } catch (DbIsNullException ignored) {
        }

    }

    //@Trace(category = MetricCategory.DATABASE)
    public static ArrayList<QuizData> createQuizList(Activity aActivity) {
        ArrayList<QuizData> arr = new ArrayList<>();
        String selection = "updated_in_version=?";
        int version = SPHandler.getInt(aActivity,
                ConstantsSP.SHARED_KEYS_QUIZ_VERSION);
        String[] selectionArgs = new String[]{"" + version};

        Cursor cursor = null;
        try {
            cursor = TrulyMadlySQLiteHandler.getDatabase(aActivity).query("quizzes", null, selection,
                    selectionArgs, null, null, null);
        } catch (DbIsNullException e) {
            return arr;
        }
        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                QuizData quiz = new QuizData(cursor);
                arr.add(quiz);
            }
        }
        if (cursor != null) {
            cursor.close();
        }
        return arr;
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static void insertAllStatus(Context ctx, String myId,
                                       String match_id, ArrayList<QuizStatusModal> statusList) {
        if (statusList == null || statusList.size() == 0) {
            return;
        }
        SQLiteDatabase db;
        try {
            db = TrulyMadlySQLiteHandler.getDatabase(ctx);
        } catch (DbIsNullException e) {
            return;
        }
        String sql = "INSERT OR IGNORE INTO quizStatus (user_id,match_id,quiz_id,status,showFlare) VALUES (?,?,?,?,?);";
        SQLiteStatement statement = db.compileStatement(sql);
        db.beginTransaction();
        for (QuizStatusModal status : statusList) {
            statement.clearBindings();
            statement.bindString(1, myId);
            statement.bindString(2, match_id);
            statement.bindLong(3, status.getQuizId());
            statement.bindString(4, status.getStatus());
            statement.bindString(5, status.getShowFlare() ? "1" : "0");
            statement.execute();
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        updateAllStatus(ctx, myId, match_id, statusList);
    }

    //@Trace(category = MetricCategory.DATABASE)
    private static void updateAllStatus(Context ctx, String myId,
                                        String match_id, ArrayList<QuizStatusModal> statusList) {
        if (statusList == null || statusList.size() == 0) {
            return;
        }
        String sql = "UPDATE quizStatus SET showFlare = ? , status= ? where user_id=? and match_id=? and quiz_id =?;";
        SQLiteDatabase db;
        try {
            db = TrulyMadlySQLiteHandler.getDatabase(ctx);
        } catch (DbIsNullException e) {
            return;
        }
        SQLiteStatement statement = db.compileStatement(sql);
        db.beginTransaction();
        for (QuizStatusModal status : statusList) {
            statement.clearBindings();
            statement.bindString(1, status.getShowFlare() ? "1" : "0");
            statement.bindString(2, status.getStatus());
            statement.bindString(3, myId);
            statement.bindString(4, match_id);
            statement.bindLong(5, status.getQuizId());
            statement.execute();
        }
        db.setTransactionSuccessful();
        db.endTransaction();
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static void insertStatusMap(Context ctx, String myId,
                                       String match_id, LinkedHashMap<Integer, String> statusMap) {
        if (statusMap == null) {
            return;
        }
        SQLiteDatabase db;
        try {
            db = TrulyMadlySQLiteHandler.getDatabase(ctx);
        } catch (DbIsNullException e) {
            return;
        }
        String sql = "INSERT OR IGNORE INTO quizStatus (user_id,match_id,quiz_id,status) VALUES (?,?,?,?);";
        SQLiteStatement statement = db.compileStatement(sql);
        db.beginTransaction();
        for (Integer id : statusMap.keySet()) {
            statement.clearBindings();
            statement.bindString(1, myId);
            statement.bindString(2, match_id);
            statement.bindLong(3, id);
            statement.bindString(4, statusMap.get(id));
            statement.execute();
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        updateStatusMap(ctx, myId, match_id, statusMap);
    }

    //@Trace(category = MetricCategory.DATABASE)
    private static void updateStatusMap(Context ctx, String myId,
                                        String match_id, LinkedHashMap<Integer, String> statusMap) {
        if (statusMap == null) {
            return;
        }
        SQLiteDatabase db;
        try {
            db = TrulyMadlySQLiteHandler.getDatabase(ctx);
        } catch (DbIsNullException e) {
            return;
        }
        String sql = "UPDATE quizStatus SET status= ? where user_id=? and match_id=? and quiz_id =?;";
        SQLiteStatement statement = db.compileStatement(sql);
        db.beginTransaction();
        for (Integer id : statusMap.keySet()) {

            statement.clearBindings();
            statement.bindString(1, statusMap.get(id));
            statement.bindString(2, myId);
            statement.bindString(3, match_id);
            statement.bindLong(4, id);
            statement.execute();
        }
        db.setTransactionSuccessful();
        db.endTransaction();
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static String getQuizStatus(Context ctx, String myId,
                                       String match_id, int quiz_id) {
        String status = "NONE";
        String columns[] = new String[]{"status"};
        String selection = "user_id=? and match_id=? and quiz_id=?";
        String selectionArgs[] = new String[]{myId, match_id, quiz_id + ""};

        Cursor cursor = null;
        try {
            cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).query("quizStatus", columns, selection,
                    selectionArgs, null, null, null);
        } catch (DbIsNullException e) {
            return status;
        }

        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                status = cursor.getString(cursor.getColumnIndex("status"));
            }
        }
        if (cursor != null)
            cursor.close();
        return status;

    }

    //@Trace(category = MetricCategory.DATABASE)
    public static void updateQuizStatus(Context ctx, String myId,
                                        String match_id, int quiz_id, String status) {
        if (Utility.isSet(myId) && Utility.isSet(match_id)
                && Utility.isSet(status)) {
            ContentValues values = new ContentValues();
            values.put("user_id", myId);
            values.put("match_id", match_id);
            values.put("status", status);
            values.put("quiz_id", quiz_id);
            try {
                TrulyMadlySQLiteHandler.getDatabase(ctx).insert("quizStatus", null, values);
            } catch (DbIsNullException ignored) {
            }
        }
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static LinkedHashMap<Integer, String> createStatusMap(Context ctx,
                                                                 String userId, String matchId) {
        return createStatusMap(ctx, userId, matchId, null, null);
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static LinkedHashMap<Integer, String> createStatusMap(Context ctx,
                                                                 String userId, String matchId, HashMap<Integer, Boolean> flareMap,
                                                                 HashMap<Integer, Boolean> animationMap) {

        LinkedHashMap<Integer, String> hmapStatus = new LinkedHashMap<>();
        if (!Utility.isSet(userId) || !Utility.isSet(matchId)) {
            return hmapStatus;

        }
        String columns[] = new String[]{"quiz_id", "status", "showFlare",
                "showAnimation"};
        String selection = "user_id=? and match_id=?";
        String selectionArgs[] = new String[]{userId, matchId};

        Cursor cursor = null;
        try {
            cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).query("quizStatus", columns, selection,
                    selectionArgs, null, null, null);
        } catch (DbIsNullException e) {
            return hmapStatus;
        }

        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                int quiz_id = cursor.getInt(cursor.getColumnIndex("quiz_id"));
                hmapStatus.put(quiz_id,
                        cursor.getString(cursor.getColumnIndex("status")));

                if (flareMap != null) {
                    flareMap.put(
                            quiz_id,
                            cursor.getString(cursor.getColumnIndex("showFlare"))
                                    .equalsIgnoreCase("1"));
                }

                if (animationMap != null) {
                    animationMap.put(
                            quiz_id,
                            cursor.getString(
                                    cursor.getColumnIndex("showAnimation"))
                                    .equalsIgnoreCase("1"));
                }

            }
        }

        if (cursor != null) {
            cursor.close();
        }

        return hmapStatus;
    }

    //@Trace(category = MetricCategory.DATABASE)
    public static boolean returnShowFlareStatus(String match_id, String user_id,
                                                int quiz_id, Context ctx) {
        boolean flag = false;
        String columns[] = new String[]{"showFlare"};
        String selection = "user_id=? and match_id=? and quiz_id=?";
        String selectionArgs[] = new String[]{user_id, match_id,
                "" + quiz_id};

        Cursor cursor = null;
        try {
            cursor = TrulyMadlySQLiteHandler.getDatabase(ctx).query("quizStatus", columns, selection,
                    selectionArgs, null, null, null);
        } catch (DbIsNullException e) {
            return false;
        }
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            flag = (cursor.getString(cursor.getColumnIndex("showFlare"))
                    .equals("1"));

        }
        if (cursor != null)
            cursor.close();

        return flag;

    }


    //@Trace(category = MetricCategory.DATABASE)
    public static void insertAnimationStatus(Context ctx, String myId,
                                             String match_id, JSONArray ids) {
        if (ids == null || ids.length() == 0) {
            return;
        }
        SQLiteDatabase db;
        try {
            db = TrulyMadlySQLiteHandler.getDatabase(ctx);
        } catch (DbIsNullException e) {
            return;
        }
        if (Utility.isSet(myId) && Utility.isSet(match_id)) {
            String sql = "INSERT OR IGNORE INTO quizStatus (user_id,match_id,quiz_id,showAnimation) VALUES (?,?,?,?);";
            SQLiteStatement statement = db.compileStatement(sql);
            db.beginTransaction();
            for (int i = 0; i < ids.length(); i++) {
                statement.clearBindings();
                statement.bindString(1, myId);
                statement.bindString(2, match_id);
                statement.bindLong(3, ids.optInt(i));
                statement.bindString(4, "1");
                statement.execute();
            }
            db.setTransactionSuccessful();
            db.endTransaction();

            updateAnimationAttribute(myId, match_id, ids, ctx);
        }
    }

    //@Trace(category = MetricCategory.DATABASE)
    private static void updateAnimationAttribute(String user_id,
                                                 String match_id, JSONArray ids, Context ctx) {
        if (ids == null || ids.length() == 0)
            return;

        String id = "\"" + ids.optInt(0);
        for (int i = 1; i < ids.length(); i++) {
            id += "\",\"" + ids.optInt(i);
        }
        id += "\"";

        try {
            TrulyMadlySQLiteHandler.getDatabase(ctx)
                    .execSQL(
                            "UPDATE quizStatus SET showAnimation = ? where user_id=? and match_id=? and quiz_id IN ("
                                    + id + ");",
                            new String[]{"1", user_id, match_id});
        } catch (DbIsNullException ignored) {
        }

    }

    //@Trace(category = MetricCategory.DATABASE)
    public static void insertFlareStatus(Context ctx, String myId,
                                         String match_id, JSONArray ids) {
        if (ids == null || ids.length() == 0) {
            return;
        }
        SQLiteDatabase db;
        try {
            db = TrulyMadlySQLiteHandler.getDatabase(ctx);
        } catch (DbIsNullException e) {
            return;
        }
        if (Utility.isSet(myId) && Utility.isSet(match_id)) {
            String sql = "INSERT OR IGNORE INTO quizStatus (user_id,match_id,quiz_id,showFlare, status) VALUES (?,?,?,?,?);";
            SQLiteStatement statement = db.compileStatement(sql);
            db.beginTransaction();
            for (int i = 0; i < ids.length(); i++) {
                statement.clearBindings();
                statement.bindString(1, myId);
                statement.bindString(2, match_id);
                statement.bindLong(3, ids.optInt(i));
                statement.bindString(4, "1");
                statement.bindString(5, "BOTH");
                statement.execute();
            }
            db.setTransactionSuccessful();
            db.endTransaction();

            updateFlareAttribute(myId, match_id, ids, ctx);
        }
    }

    //@Trace(category = MetricCategory.DATABASE)
    private static void updateFlareAttribute(String user_id, String match_id,
                                             JSONArray ids, Context ctx) {
        if (ids == null || ids.length() == 0)
            return;

        String id = "\"" + ids.optInt(0);
        for (int i = 1; i < ids.length(); i++) {
            id += "\",\"" + ids.optInt(i);
        }
        id += "\"";
        try {
            TrulyMadlySQLiteHandler.getDatabase(ctx).execSQL(
                    "UPDATE quizStatus SET showFlare = ?,status = ? where user_id=? and match_id=? and quiz_id IN ("
                            + id + ");",
                    new String[]{"1", "BOTH", user_id, match_id});
        } catch (DbIsNullException ignored) {
        }

    }

    //@Trace(category = MetricCategory.DATABASE)
    public static void updateFlare(Context ctx, String user_id, String match_id,
                                   int quiz_id, boolean flag) {

        ContentValues values = new ContentValues();
        values.put("showFlare", flag);
        String whereClause = "user_id =? and match_id=? and quiz_id =?";
        String[] whereArgs = new String[]{user_id, match_id, "" + quiz_id};
        try {
            TrulyMadlySQLiteHandler.getDatabase(ctx).update("quizStatus", values, whereClause, whereArgs);
        } catch (DbIsNullException ignored) {
        }
    }

}
