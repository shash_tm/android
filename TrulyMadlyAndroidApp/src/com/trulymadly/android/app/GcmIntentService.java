/**
 *
 */
package com.trulymadly.android.app;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.moengage.push.PushManager;
import com.moengage.pushbase.push.MoEngageNotificationUtils;
import com.trulymadly.android.app.MessageModal.MessageType;
import com.trulymadly.android.app.bus.GCMMessageReceivedEvent;
import com.trulymadly.android.app.bus.ServiceEvent;
import com.trulymadly.android.app.bus.ServiceEvent.SERVICE_EVENT_TYPE;
import com.trulymadly.android.app.bus.TriggerConversationListUpdateEvent;
import com.trulymadly.android.app.bus.TriggerQuizStatusRefreshEvent;
import com.trulymadly.android.app.json.Constants.FetchSource;
import com.trulymadly.android.app.json.Constants.MessageState;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.modal.NotificationModal.NotificationType;
import com.trulymadly.android.app.receivers.GcmBroadcastReceiver;
import com.trulymadly.android.app.sqlite.MessageDBHandler;
import com.trulymadly.android.app.sqlite.VoucherDBHandler;
import com.trulymadly.android.app.utility.NotificationHandler;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.Utility;
import com.trulymadly.android.socket.SocketHandler;

/**
 * @author udbhav
 */
public class GcmIntentService extends IntentService {
    NotificationCompat.Builder builder;

    public GcmIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent == null) {
            return;
        }
        final Bundle extras = intent.getExtras();


        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);
        if (messageType != null && !extras.isEmpty()) { // has effect of
            /*
             * Filter messages based on message type. Since it is likely that
			 * GCM will be extended in the future with new message types, just
			 * ignore any message types you're not interested in, or that you
			 * don't recognize.
			 */
            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR
                    .equals(messageType)) {
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED
                    .equals(messageType)) {
                // If it's a regular GCM message, do some work.
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE
                    .equals(messageType)) {
                if (MoEngageNotificationUtils.isFromMoEngagePlatform(intent)) {
                    try {
                        PushManager.getInstance().getPushHandler().handlePushPayload(getApplicationContext(), intent);
                    } catch (OutOfMemoryError | NullPointerException ignored) {
                        //Mo Engage crash
                    }
                } else {
                    // Post notification of received message.
                    String TAG = "GCM_INTENT_SERVICE";
                    Log.i(TAG, "Received: " + extras.toString());
                    // if voucher then save in db


                    if (Utility.stringCompare(extras.getString("push_type"),
                            NotificationType.VOUCHER.toString())) {
                        VoucherDBHandler.saveVoucherData(getApplicationContext(),
                                extras.getString("voucher_data"));
                    }
                    if (Utility.stringCompare(extras.getString("push_type"),
                            NotificationType.QUIZ_REFRESH.toString())) {
                        String matchId = extras.getString("sender_id");
                        Utility.fireBusEvent(getApplicationContext(),
                                MessageOneonOneConversionActivity.isChatActiveForId(getApplicationContext(), matchId),
                                new TriggerQuizStatusRefreshEvent(matchId));
                    }

                    Utility.fireServiceBusEvent(getApplicationContext(),
                            new ServiceEvent(SERVICE_EVENT_TYPE.CONNECT_SOCKET));
                    Utility.fireBusEvent(getApplicationContext(), true,
                            new TriggerConversationListUpdateEvent());
                    if (Utility.stringCompare(extras.getString("push_type"),
                            NotificationType.MESSAGE.toString())) {
                        boolean toDelayNotification = SocketHandler.isSocketEnabled(getApplicationContext());
                        if (!toDelayNotification) {
                            sendDelayedNotification(extras);
                        } else {
                            new Handler().postDelayed(new Runnable() {

                                @Override
                                public void run() {
                                    sendDelayedNotification(extras);

                                }
                            }, 15000);
                        }
                    } else if (Utility.stringCompare(extras.getString("push_type"),
                            NotificationType.MATCHES_IMAGE_MULTI.toString())) {
                        NotificationHandler.startMatchesMultiLocation(this, extras);
                    } else {
                        // Generate Notification
                        NotificationHandler.sendNotification(extras, this, false, false, true);
                    }
                }
            }
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    private void sendDelayedNotification(Bundle extras) {
        String match_id = extras.getString("match_id");
        String user_id = Utility
                .getMyId(getApplicationContext());
        boolean isMessageExist = false;
        //check if used if logged in and same user is logged in
        if (Utility.isUserLoggedIn(getApplicationContext()) && Utility.stringCompare(user_id, SPHandler.getString(getApplicationContext(), ConstantsSP.SHARED_KEYS_USER_ID))) {
            match_id = extras.getString("is_admin_set").equalsIgnoreCase("1") ? "admin" : match_id;
            MessageModal msgObject = new MessageModal(user_id);
            msgObject.parseMessage(extras.getString("msg_id"), Utility.stripHtml(Utility.decodeURI(extras
                            .getString("content_text"))), extras.getString("tstamp"), MessageType.TEXT.toString(),
                    match_id, MessageState.GCM_FETCHED, 0, null, null);
            isMessageExist = MessageDBHandler.insertAMessage(getApplicationContext(), user_id, msgObject, false, FetchSource.GCM, true);
            if (MessageOneonOneConversionActivity.isChatActiveForId(
                    getApplicationContext(), match_id)) {
                // send broadcast if type = message and app running and
                // is chatting with the match.
                Utility.fireBusEvent(getApplicationContext(), true, new GCMMessageReceivedEvent());
            } else if (!isMessageExist) {
                NotificationHandler.sendNotification(extras, this, false, false, true);
            }
        }

    }

}
