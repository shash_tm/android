/**
 * The login activity which handles all the initial loading of the app
 * Default activity
 *
 * @author udbhav
 */

package com.trulymadly.android.app;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings.Secure;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.BadTokenException;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;

import com.crashlytics.android.Crashlytics;
import com.facebook.CallbackManager;
import com.facebook.appevents.AppEventsConstants;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.moe.pushlibrary.MoEHelper;
import com.moe.pushlibrary.utils.MoEHelperConstants;
import com.squareup.otto.Subscribe;
import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.LoginFragment.OnForgetPasswordViewChanged;
import com.trulymadly.android.app.LoginFragment.OnLoginFragmentClick;
import com.trulymadly.android.app.LoginSliderFragment.OnLoginSliderFragmentClick;
import com.trulymadly.android.app.RegisterHomeFragment.OnRegisterHomeFragmentClick;
import com.trulymadly.android.app.adapter.FacebookLoginCallback;
import com.trulymadly.android.app.adapter.FacebookLoginCallback.FacebookLoginCallbackInterface;
import com.trulymadly.android.app.asynctasks.TrackEventToFbAsyncTask;
import com.trulymadly.android.app.bus.BusProvider;
import com.trulymadly.android.app.bus.InstallReferrerEvent;
import com.trulymadly.android.app.bus.ToggleView;
import com.trulymadly.android.app.bus.ToggleViewOnBack;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.Constants.ActivityName;
import com.trulymadly.android.app.json.Constants.EditProfilePageType;
import com.trulymadly.android.app.json.Constants.HttpRequestType;
import com.trulymadly.android.app.json.ConstantsRF;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.json.ConstantsSocket.SOCKET_END;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.listener.AlertDialogInterface;
import com.trulymadly.android.app.listener.ConfirmDialogInterface;
import com.trulymadly.android.app.listener.CustomOkHttpResponseHandler;
import com.trulymadly.android.app.listener.OnDataFetchedInterface;
import com.trulymadly.android.app.modal.LoginDetail;
import com.trulymadly.android.app.modal.RegisterDetail;
import com.trulymadly.android.app.modal.UserData;
import com.trulymadly.android.app.sqlite.MessageDBHandler;
import com.trulymadly.android.app.sqlite.RFHandler;
import com.trulymadly.android.app.utility.ActivityHandler;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.CompetitorTrackingHandling;
import com.trulymadly.android.app.utility.MoEHandler;
import com.trulymadly.android.app.utility.NotificationHandler;
import com.trulymadly.android.app.utility.OkHttpHandler;
import com.trulymadly.android.app.utility.PermissionsHelper;
import com.trulymadly.android.app.utility.ReferralHandler;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import seventynine.sdk.SeventynineAdSDK;

import static com.trulymadly.android.app.modal.NotificationModal.getNotificationTypeFromString;

//import com.inmobi.sdk.InMobiSdk;

public class LoginActivity extends FragmentActivity
        implements OnLoginFragmentClick, OnRegisterHomeFragmentClick,
        OnLoginSliderFragmentClick, OnForgetPasswordViewChanged {
    private static final String TAG = "LoginActivity";
    private final Validator validator = new Validator();
    private LoginDetail aLoginDetail = null;
    private CustomOkHttpResponseHandler loginHandlerResponse = null;
    private Context aContext;
    private CallbackManager callbackManager;
    private FacebookLoginCallback facebookLoginCallBack;
    private String firstName, lastName, userId = null, passwd = null, gender;
    private EditText firstNameEditText, lastNameEditText, userIdEditText,
            passwdEditText;
    private GoogleCloudMessaging gcm;
    private String regid;
    private Boolean isPush = false;
    private String pushType = null;
    private String push_match_id, push_message_url, push_profile_url,
            push_notification_from_admin;
    private String pushClassName = null, pushWebUrl = null, pushEventId = null, pushCategoryId = null, pushCategoryName = null;
    private ProgressDialog mProgressDialog;
    private Activity aActivity;
    private boolean isGcmNetworkCallInProgress = false;
    private String pushEventStatus;
    // to check which view is visible in loginfragment
    private boolean isPasswordViewOpen = false;
    private LoginFragment loginFragment = null;
    private MoEHelper mhelper = null;
    private boolean isCitySet = false;
    private boolean isFbConnected = false;
    private Date dob = null;
    private boolean isInformationViewVisible;
    private FacebookLoginCallbackInterface callbackInterface;
    private boolean launchSparksOnMatches = false;
    private boolean launchSelectOnMatches = false;
//    private SeventynineAdSDK mSeventynineAdSDK;

    private Handler mHandler;
    private Runnable fetchUserStatusRunnable;

    public static void setUserId(String user_id, Context ctx) {
        if (Utility.isSet(user_id)) {
            SPHandler.setString(ctx, ConstantsSP.SHARED_KEYS_USER_ID,
                    user_id);
            Crashlytics.setUserIdentifier(user_id);
            Crashlytics.setString("USER_ID", user_id);
            MoEHelper.getInstance(ctx).setUserAttribute(
                    MoEHelperConstants.USER_ATTRIBUTE_UNIQUE_ID, user_id);

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_NO_TITLE); // change in mainifest
        super.onCreate(savedInstanceState);

//        SeventynineConstants.strPublisherId = Constants.ADS_79_PUBLISHER_ID;
//        SeventynineConstants.strIsStartZoneActive = "0";
//        SeventynineConstants.strIsEndZoneActive = "0";
//        SeventynineConstants.appContext = getApplicationContext();
//        SeventynineConstants.strFirstActivityPath = "";
//        SeventynineConstants.strSkipButtonByDeveloper = "1000000";
//        mSeventynineAdSDK = new SeventynineAdSDK();
//        mSeventynineAdSDK.init(this);

        aContext = this;
        aActivity = this;

        mHandler = new Handler();

        //REMOVING APP DYNAMICS
        //Instrumentation.start(Constants.APP_DYNAMICS_KEY, getApplicationContext());

        TrackEventToFbAsyncTask.trackEvent(aContext, AppEventsConstants.EVENT_NAME_ACTIVATED_APP);

        Intent i = getIntent();
        Bundle b = i.getExtras();

        if (savedInstanceState != null) {
            // isInformationViewVisible =
            // savedInstanceState.getBoolean("isInformationViewVisible");
        }

        if (b != null) {
            if (b.getBoolean("isPush")) {
                try {
                    isPush = true;
                    pushType = b.getString("push_type");
                    push_profile_url = b.getString("match_profile_url");
                    push_message_url = b.getString("message_url");
                    push_match_id = b.getString("match_id");
                    push_notification_from_admin = b.getString("is_admin_set");
                    pushClassName = b.getString("class_name");
                    pushWebUrl = b.getString("web_url");
                    pushEventStatus = b.getString("event_status");
                    pushEventId = b.getString("event_id");
                    pushCategoryName = b.getString("category_name");
                    pushCategoryId = b.getString("category_id");

                    if (pushEventStatus == null) {
                        pushEventStatus = pushType;
                    }
                } catch (NoSuchMethodError e) {
                    finish();
                    return;
                }
            } else {
                isPush = false;
            }
        } else {
            isPush = false;
        }


        NotificationHandler.clearAllNonMessageNotifications(aContext);

        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        mhelper = new MoEHelper(this);
        // MoEHelper.APP_DEBUG=true;

        //NewRelic. Commeting this out as trial has expired.
        // Also change in newrelic.properties
//        NewRelic.withApplicationToken("AAa6a09a0ad545221cf8951df47642464030704e6d")
//                .withApplicationBuild(BuildConfig.BASE_VERSION_CODE + "")
//                .withCrashReportingEnabled(false)
//                .withLoggingEnabled(false)
//                .withLogLevel(Log.ERROR)
//                .start(this.getApplication());

        boolean isExistingUser = Utility.isUserLoggedIn(aContext);
        if (isExistingUser) {
            setUserId(Utility.getMyId(aContext), aContext);
            MoEHelper.getInstance(aContext).setUserAttribute(
                    MoEHelperConstants.USER_ATTRIBUTE_UNIQUE_ID,
                    Utility.getMyId(aContext));
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                // See sample code at
                // http://developer.android.com/google/play-services/id.html
                try {
                    Info adInfo = AdvertisingIdClient
                            .getAdvertisingIdInfo(getApplicationContext());
                    MoEHelper.getInstance(aContext).setUserAttribute(
                            "GOOGLE_ADVERTISING_ID", adInfo.getId());

                } catch (IOException | GooglePlayServicesNotAvailableException | NullPointerException | GooglePlayServicesRepairableException e) {
                    // Unrecoverable error connecting to Google Play services
                    // (e.g.,
                    // the old version of the service doesn't support getting
                    // AdvertisingId).
                    MoEHelper.getInstance(aContext).setUserAttribute(
                            "ANDROID_ID", Secure.getString(getContentResolver(),
                                    Secure.ANDROID_ID));

                } catch (SecurityException | IllegalStateException | IllegalArgumentException ignored) {
                }
            }
        }).start();
        MoEHelper.getInstance(aContext).setUserAttribute("ANDROID_ID",
                Secure.getString(getContentResolver(), Secure.ANDROID_ID));

        if (isExistingUser && Utility.handleDeepLinks(aActivity, i.getDataString())) {
            finish();
            return;
        }


        // send to matches if registration setString
        if (!isPush && Utility.isSet(SPHandler.getString(aContext,
                ConstantsSP.SHARED_KEYS_IS_REGISTRATION_COMPLETE))) {
            getRegId(false, TrulyMadlyEventTypes.login);
            launchActivity(ActivityName.MATCHES, null);
            return;
        }

        try {
            setContentView(R.layout.login_container);
        } catch (OutOfMemoryError e) {
            System.gc();
            finish();
            return;
        }
        if (savedInstanceState != null) {
            return;
        }
        loginHandlerResponse = createLoginHandler();
        createFacebookLoginInstance();

        if (isPush) {
            launchPushActivity();
        } else {
            fetchUserStatus();
        }

    }

    private void createFacebookLoginInstance() {
        if (callbackManager == null) {
            callbackManager = CallbackManager.Factory.create();
        }
        if (callbackInterface == null) {
            callbackInterface = new FacebookLoginCallbackInterface() {
                @Override
                public void onFail() {
                    hideLoaders();
                }

                @Override
                public void onLogin(String accessToken,
                                    boolean isBirthdayDeclined, boolean isPhotoDeclined) {
                    issueFacebookRequest(accessToken);
                }

                @Override
                public void onRetry() {
                    mProgressDialog = UiUtils.showProgressBar(aContext,
                            mProgressDialog);
                }
            };
        }
        if (facebookLoginCallBack == null) {
            facebookLoginCallBack = new FacebookLoginCallback(this,
                    callbackManager, callbackInterface, true, false, TrulyMadlyActivities.login);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        ReferralHandler.checkAndLoadUrl(this, ReferralHandler.ACTION_POSITION.on_install);
    }

    @Subscribe
    public void onInstallReferrerEvent(InstallReferrerEvent installReferrerEvent) {
        if (installReferrerEvent != null && Utility.isSet((installReferrerEvent.getReferrer()))) {
            ReferralHandler.checkAndLoadUrl(this, ReferralHandler.ACTION_POSITION.on_install);
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    private CustomOkHttpResponseHandler createLoginHandler() {
        return new CustomOkHttpResponseHandler(aContext,
                TrulyMadlyEvent.TrulyMadlyActivities.login,
                TrulyMadlyEvent.TrulyMadlyEventTypes.login_via_email) {
            @Override
            public void onRequestSuccess(JSONObject response) {
                switch (response.optInt("responseCode")) {
                    case 200:
                        if (response.optJSONObject("data") != null) {
                            String gender = response.optJSONObject("data")
                                    .optString("gender");
                            setGender(gender);
                            if (Utility.stringCompare(getEventType(),
                                    TrulyMadlyEvent.TrulyMadlyEventTypes.fb_server_call)
                                    && Utility.stringCompare(getActivity(),
                                    TrulyMadlyEvent.TrulyMadlyActivities.login)) {
                                trackNewUser(response.optJSONObject("data")
                                        .optBoolean("isNewUser"), gender);
                            } else {
                                //login using email case
                                trackNewUser(false, gender);
                            }
                        }
                        onLoginSuccess(response);
                        break;
                    case 403:
                        hideLoaders();
                        AlertsHandler.showAlertDialog(aActivity,
                                response.optString("error", "An error occured."),
                                null);
                        logoutFacebook();
                        break;
                    default:
                        onRequestFailure(null);
                        break;
                }

            }

            @Override
            public void onRequestFailure(Exception exception) {
                hideLoaders();
                logoutFacebook();
                AlertsHandler.showNetworkError(aActivity, exception);
            }
        };
    }

    private void logoutFacebook() {
        if (facebookLoginCallBack != null) {
            facebookLoginCallBack.logout();
        }
    }

    private void loginFacebook() {
        createFacebookLoginInstance();
        facebookLoginCallBack.login();
    }

    private void onLoginSuccess(JSONObject userDataJson) {
        OkHttpHandler.saveCookieOnLogin(aContext);
        getRegId(true, TrulyMadlyEventTypes.login);
        TrulyMadlyApplication.restartService(aContext,
                SOCKET_END.restart_on_login);

        MoEHandler.trackEvent(aContext, MoEHandler.Events.LOGIN_SUCCESS);
        JSONObject data = userDataJson.optJSONObject("data");
        if (userDataJson != null && !Utility.stringCompare(data.optString("status"), "incomplete")) {
            updateClearChat();
        }
        continueSession(userDataJson);
    }

    private void updateClearChat() {
        // making call to update clear_chat_tstamp
        CustomOkHttpResponseHandler okHttpResponseHandler = new CustomOkHttpResponseHandler(aActivity) {

            @Override
            public void onRequestSuccess(JSONObject responseJSON) {
                if (responseJSON != null) {
                    MessageDBHandler.updateClearChatTstampForAllMatches(aContext, responseJSON);
                }
            }

            @Override
            public void onRequestFailure(Exception exception) {
            }
        };
        okHttpResponseHandler.setForceSuccess(true);
        HashMap<String, String> params = new HashMap<>();
        params.put("get_clear_chat_data", "1");
        OkHttpHandler.httpGet(aActivity, ConstantsUrls.get_message_url(), params,
                okHttpResponseHandler);
        //
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            if (this.getSupportFragmentManager().getBackStackEntryCount() > 0) {
                if (isPasswordViewOpen && loginFragment != null) {
                    loginFragment.closeForgotPassword();
                } else {
                    try {
                        this.getSupportFragmentManager().popBackStack();
                        showStatusBarColor(R.color.black);
                    } catch (IllegalStateException ignored) {

                    }
                }
            } else if (isInformationViewVisible) {
                hideInformationView(true);
            } else {
                Utility.minimizeApp(this);
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void hideInformationView(boolean toTrack) {
        if (isInformationViewVisible) {
            isInformationViewVisible = false;
            Utility.fireBusEvent(aActivity, true,
                    new ToggleViewOnBack(isInformationViewVisible, toTrack));
        }
    }

    @Subscribe
    public void onToggleInformationView(ToggleView toggleView) {
        isInformationViewVisible = toggleView.isToShow();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("isPush", false);
    }

    @Override
    protected void onDestroy() {
        if (mHandler != null) {
            mHandler.removeCallbacks(fetchUserStatusRunnable);
        }
        super.onDestroy();
    }

    @Override
    public void onStart() {
        super.onStart();
//        mSeventynineAdSDK.showAd(this);

        // Send install request to server
        if (Utility.isNetworkAvailable(aContext)) {
            if (!Utility.isSet(SPHandler.getString(aContext,
                    ConstantsSP.SHARED_KEYS_INSTALL_REQUEST_SENT))) {
                TrulyMadlyTrackEvent.trackEvent(aContext,
                        TrulyMadlyActivities.install,
                        TrulyMadlyEventTypes.install, 0, null, null, true);
                SPHandler.setString(aContext,
                        ConstantsSP.SHARED_KEYS_INSTALL_REQUEST_SENT, "yes");

                //TODO: DEBUG : FOR PRE-BURNING EMBEDDING ON DEVICE PHONES, assisted app installs
                //Utility.sendInstallReferrerRequestToServer("INTEX-EMBEDDING", aContext);
                //Utility.sendInstallReferrerRequestToServer("RELIANCE", aContext);
            }
        }
        getRegId(false, null);
        BusProvider.getInstance().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        try {
            BusProvider.getInstance().unregister(this);
        } catch (IllegalArgumentException ignored) {

        }
    }

    /**
     * Process the response of all the activities which were started for result.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        createFacebookLoginInstance();
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.OPEN_WEB_LINK) {
            insertLoginSlideFragment();
            fetchUserStatus();
        }
    }

    /**
     * Send the login request for login using email d.
     */
    private void issueLoginRequest() {
        aLoginDetail = new LoginDetail();
        aLoginDetail.setEmail(userId);
        aLoginDetail.setPassword(passwd);
        aLoginDetail.setIsFromFB(false);
        mProgressDialog = UiUtils.showProgressBar(aContext, mProgressDialog,
                R.string.login_email_loader_message);
        if (loginHandlerResponse == null) {
            loginHandlerResponse = createLoginHandler();
        }
        loginHandlerResponse
                .setActivity(TrulyMadlyEvent.TrulyMadlyActivities.login);
        HttpTasks.login(aLoginDetail, aContext, loginHandlerResponse);
    }

    /**
     * Fetch user status from server based on cookie.
     */

    private void fetchUserStatus() {
        if (Utility.isUserLoggedIn(aContext)) {
            CustomOkHttpResponseHandler okHttpResponseHandler = new CustomOkHttpResponseHandler(
                    aContext, TrulyMadlyActivities.index,
                    TrulyMadlyEventTypes.page_load) {

                @Override
                public void onRequestSuccess(JSONObject response) {
                    switch (response.optInt("responseCode")) {
                        case 200:
                            continueSession(response);
                            break;
                        case 403:
                            hideLoaders();
                            AlertsHandler.showAlertDialog(aActivity,
                                    response.optString("error", "An error occured."),
                                    new AlertDialogInterface() {
                                        @Override
                                        public void onButtonSelected() {
                                            Utility.logoutSession(aActivity);
                                        }
                                    });
                            break;
                        default:
                            onRequestFailure(null);
                            break;
                    }

                }

                @Override
                public void onRequestFailure(Exception exception) {
                    isPush = false;
                    hideLoaders();
                    if (fetchUserStatusRunnable == null) {
                        fetchUserStatusRunnable = new Runnable() {
                            @Override
                            public void run() {
                                fetchUserStatus();
                            }
                        };
                    }
                    mHandler.postDelayed(fetchUserStatusRunnable, 500);
                }
            };
            mProgressDialog = UiUtils.showProgressBar(aContext,
                    mProgressDialog);
            OkHttpHandler.httpGet(aContext, ConstantsUrls.get_login_url(),
                    GetOkHttpRequestParams.getHttpRequestParams(
                            HttpRequestType.GET_USER_DATA),
                    okHttpResponseHandler);
        } else {
            insertLoginSlideFragment();
        }
    }

    /**
     * Display the slide fragment.
     */
    private void insertLoginSlideFragment() {
        try {
            showStatusBarColor(R.color.black);
            LoginSliderFragment loginSliderFragment = new LoginSliderFragment();
            Bundle extras = getIntent().getExtras();
            loginSliderFragment.setArguments(extras);
            FragmentTransaction transaction = getSupportFragmentManager()
                    .beginTransaction();
            transaction.add(R.id.fragment_container_login, loginSliderFragment,
                    "TAG_loginFragment");
            transaction.commit();
        } catch (IllegalStateException ignored) {

        }
    }

    /**
     * Display the login fragment.
     */
    private void insertLoginFragment() {
        showStatusBarColor(R.color.black);
        try {
            loginFragment = new LoginFragment();
            loginFragment.setArguments(getIntent().getExtras());
            FragmentTransaction transaction = getSupportFragmentManager()
                    .beginTransaction();
            transaction.setCustomAnimations(R.anim.push_left_in,
                    R.anim.push_left_out, R.anim.push_in_from_left,
                    R.anim.push_right_out);
            transaction.replace(R.id.fragment_container_login, loginFragment,
                    "TAG_loginFragment");
            transaction.addToBackStack(null);
            transaction.commit();
        } catch (IllegalStateException ignored) {

        }
    }

    /**
     * Launch the registration fragment.
     */
    private void insertRegistrationHomeFragment() {
        showStatusBarColor(R.color.black);
        try {
            Bundle data = new Bundle();
            data.putString("userId", userId);
            data.putString("passwd", passwd);
            RegisterHomeFragment registerHomeFragment = new RegisterHomeFragment();
            registerHomeFragment.setArguments(data);
            FragmentTransaction transaction = getSupportFragmentManager()
                    .beginTransaction();
            transaction.setCustomAnimations(R.anim.push_left_in,
                    R.anim.push_left_out, R.anim.push_in_from_left,
                    R.anim.push_right_out);
            transaction.replace(R.id.fragment_container_login,
                    registerHomeFragment, "TAG_registerHomeFragment");
            transaction.addToBackStack(null);
            transaction.commit();
        } catch (IllegalStateException ignored) {
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void showStatusBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(color));
        }

    }

    /**
     * Click listener callback for Login fragment
     */
    @Override
    public void onLoginFragmentClick(View v, View rootView) {
        Button login = (Button) rootView.findViewById(R.id.btn_login);
        InputMethodManager imm = (InputMethodManager) getSystemService(
                Context.INPUT_METHOD_SERVICE);
        switch (v.getId()) {
            // For login Button via email id.
            case R.id.btn_login:
            case R.id.et_passwd:
                userIdEditText = (EditText) rootView
                        .findViewById(R.id.et_userid_login);
                passwdEditText = (EditText) rootView.findViewById(R.id.et_passwd);
                userId = userIdEditText.getText().toString();
                passwd = passwdEditText.getText().toString();

                imm.hideSoftInputFromWindow(login.getWindowToken(), 0);
                if (validateLoginData()) {
                    issueLoginRequest();
                }
                break;

            // For new user.
            case R.id.btn_newuser:
                imm.hideSoftInputFromWindow(login.getWindowToken(), 0);
                userIdEditText = (EditText) rootView
                        .findViewById(R.id.et_userid_login);
                passwdEditText = (EditText) rootView.findViewById(R.id.et_passwd);
                userId = userIdEditText.getText().toString();
                passwd = passwdEditText.getText().toString();
                if (userId != null && userId.trim().length() > 0) {
                    userId = userId.trim();
                } else {
                    userId = null;
                }
                if (passwd != null && passwd.trim().length() > 0) {
                    passwd = passwd.trim();
                } else {
                    passwd = null;
                }
                TrulyMadlyTrackEvent.trackEvent(aContext,
                        TrulyMadlyActivities.login,
                        TrulyMadlyEventTypes.sign_up_on_email_page_clicked, 0, null,
                        null, true);
                insertRegistrationHomeFragment();

                break;

            // for login button via fb.
            case R.id.login_fb_id:

                imm.hideSoftInputFromWindow(login.getWindowToken(), 0);
                if (!Utility.isNetworkAvailable(aContext)) {
                    AlertsHandler.showMessage(aActivity, R.string.ERROR_NETWORK_FAILURE);
                    break;
                } else {
                    mProgressDialog = UiUtils.showProgressBar(aContext,
                            mProgressDialog);
                    if (loginHandlerResponse == null) {
                        loginHandlerResponse = createLoginHandler();
                    }
                    loginHandlerResponse.setActivity(
                            TrulyMadlyEvent.TrulyMadlyActivities.login);
                    loginHandlerResponse.setStartTime();
                    TrulyMadlyTrackEvent.trackEvent(aContext,
                            TrulyMadlyActivities.login,
                            TrulyMadlyEventTypes.fb_click, 0, null, null, true);
                    loginFacebook();
                }
                break;

            default:
                break;

        }
    }

    @Override
    public void onLoginSliderFragmentClick(View v) {
        switch (v.getId()) {
            case R.id.email_icon:
            case R.id.home_login_link:
            case R.id.home_login_container:
                TrulyMadlyTrackEvent.trackEvent(aContext,
                        TrulyMadlyActivities.login,
                        TrulyMadlyEventTypes.email_instead_clicked, 0, isInformationViewVisible ? "privacy_page" : "home_page", null,
                        true);
                insertLoginFragment();
                break;
            case R.id.home_new_user_link:
                insertRegistrationHomeFragment();
                break;
            case R.id.fb_register_icon:
            case R.id.register_fb_id:
                registerFromFB();
                break;
            default:
                break;
        }
        hideInformationView(false);

    }

    /**
     * Click listener callback for Login fragment
     */
    @Override
    public void onRegisterHomeFragmentClick(View v, View rootView) {
        Button registerButton = (Button) rootView
                .findViewById(R.id.btn_register);
        InputMethodManager imm = (InputMethodManager) getSystemService(
                Context.INPUT_METHOD_SERVICE);
        int selectedGenderButtonId;
        switch (v.getId()) {
            // for register button.
            case R.id.btn_register:
            case R.id.new_passwd:
                firstNameEditText = (EditText) rootView
                        .findViewById(R.id.new_firstname);
                lastNameEditText = (EditText) rootView
                        .findViewById(R.id.new_lastname);
                userIdEditText = (EditText) rootView.findViewById(R.id.new_userid);
                passwdEditText = (EditText) rootView.findViewById(R.id.new_passwd);
                RadioGroup genderRadioGroup = (RadioGroup) rootView
                        .findViewById(R.id.radio_gender);
                EditText dob_et = (EditText) rootView
                        .findViewById(R.id.birthdate_text_register);

                firstName = firstNameEditText.getText().toString();
                lastName = lastNameEditText.getText().toString();
                userId = userIdEditText.getText().toString();
                passwd = passwdEditText.getText().toString();
                selectedGenderButtonId = genderRadioGroup.getCheckedRadioButtonId();
                if (selectedGenderButtonId != -1) {
                    gender = findViewById(selectedGenderButtonId)
                            .getTag().toString();
                }

                dob = (Date) dob_et.getTag();

                imm.hideSoftInputFromWindow(registerButton.getWindowToken(), 0);
                if (validateRegisterHomeData()) {
                    issueEmailRegisterRequest();
                }
                break;

            // for registration using fb.
            case R.id.register_fb_id:
                imm.hideSoftInputFromWindow(registerButton.getWindowToken(), 0);
                registerFromFB();
                break;
            default:
                break;

        }
    }

    private void registerFromFB() {
        if (!Utility.isNetworkAvailable(aContext)) {
            AlertsHandler.showMessage(aActivity, R.string.ERROR_NETWORK_FAILURE);
        } else {
            mProgressDialog = UiUtils.showProgressBar(aContext, mProgressDialog,
                    R.string.sign_up_fb_loader_message);
            if (loginHandlerResponse == null) {
                loginHandlerResponse = createLoginHandler();
            }
            TrulyMadlyTrackEvent.trackEvent(aContext,
                    TrulyMadlyActivities.login, TrulyMadlyEventTypes.fb_click,
                    0, isInformationViewVisible ? "privacy_page" : "home_page", null, true);
            loginHandlerResponse
                    .setActivity(TrulyMadlyEvent.TrulyMadlyActivities.login);
            loginFacebook();
        }
    }

    /**
     * Validate login fields.
     *
     * @return Boolean Whether all login fields are valid.
     */
    private boolean validateLoginData() {
        if (!validator.emailValidator(userId, userIdEditText,
                "Please enter a valid email id.")) {
            showSoftKeyboard(userIdEditText);
            return false;
        }
        userId = userId.trim();
        if (!validator.nameValidator(passwd, passwdEditText)) {
            showSoftKeyboard(passwdEditText);
            return false;
        }
        return true;
    }

    /**
     * Validate register fields.
     *
     * @return Boolean Whether all register fields are valid.
     */
    private boolean validateRegisterHomeData() {
        if (!validator.emailValidator(userId, userIdEditText,
                "Please enter a valid email id.")) {
            showSoftKeyboard(userIdEditText);
            return false;
        }
        userId = userId.trim();

        if (!validator.passwordValidator(passwd, passwdEditText)) {
            showSoftKeyboard(passwdEditText);
            return false;
        }

        if (!validator.nameValidator(firstName, firstNameEditText)) {
            showSoftKeyboard(firstNameEditText);
            return false;
        }
        firstName = firstName.trim();

        if (!validator.nameValidator(lastName, lastNameEditText)) {
            showSoftKeyboard(lastNameEditText);
            return false;
        }
        lastName = lastName.trim();

        if (!validator.genderValidator(gender)) {
            AlertsHandler.showAlertDialog(aActivity, R.string.please_select_gender);
            return false;
        }

        if (dob == null) {
            AlertsHandler.showAlertDialog(aActivity, R.string.enter_dob);
            return false;
        }

        return true;
    }

    /**
     * Show the keyboard on focus on the edit text field.
     *
     * @param editText the field to be focused on.
     */
    private void showSoftKeyboard(EditText editText) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(
                Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInputFromWindow(
                editText.getApplicationWindowToken(),
                InputMethodManager.SHOW_FORCED, 0);
    }

    /**
     * Send facebook token to server to login/register.
     *
     * @param token
     */
    private void issueFacebookRequest(String token) {
        aLoginDetail = new LoginDetail();
        aLoginDetail.setIsFromFB(true);
        aLoginDetail.setFBToken(token);
        aLoginDetail.setReferrer(SPHandler.getString(aContext,
                ConstantsSP.SHARED_KEYS_INSTALL_REFERRER));

        if (loginHandlerResponse == null) {
            loginHandlerResponse = createLoginHandler();
        } else {
            TrulyMadlyTrackEvent.trackEvent(aContext,
                    loginHandlerResponse.getActivity(),
                    TrulyMadlyEventTypes.fb_popup,
                    loginHandlerResponse.getTimeDiffNow(), null, null, true);
        }
        HttpTasks.login(aLoginDetail, aContext, loginHandlerResponse);
    }

    /**
     * Send request to server for new user registration using email id.
     */
    private void issueEmailRegisterRequest() {
        RegisterDetail aRegisterDetail = new RegisterDetail();
        aRegisterDetail.setFromFB(false);
        aRegisterDetail.setFirstName(firstName);
        aRegisterDetail.setLastName(lastName);
        aRegisterDetail.setEmail(userId);
        aRegisterDetail.setPassword(passwd);
        aRegisterDetail.setGender(gender);
        aRegisterDetail.setDob_d(dob.getDate());
        aRegisterDetail.setDob_m(dob.getMonth() + 1);
        aRegisterDetail.setDob_y(dob.getYear() + 1900);

        mProgressDialog = UiUtils.showProgressBar(aContext, mProgressDialog);

        CustomOkHttpResponseHandler okHttpResponseHandler = new CustomOkHttpResponseHandler(
                aContext, TrulyMadlyEvent.TrulyMadlyActivities.login,
                TrulyMadlyEvent.TrulyMadlyEventTypes.signup_via_email) {
            @Override
            public void onRequestSuccess(JSONObject response) {
                switch (response.optInt("responseCode")) {
                    case 200:
                        if (response.optJSONObject("data") != null) {
                            setGender(response.optJSONObject("data")
                                    .optString("gender"));
                        }
                        trackNewUser(true, gender);
                        onLoginSuccess(response);
                        break;
                    case 403:
                        hideLoaders();
                        AlertsHandler.showAlertDialog(aActivity,
                                response.optString("error", "An error occured."),
                                null);
                        logoutFacebook();
                        break;
                    default:
                        onRequestFailure(null);
                        break;
                }
            }

            @Override
            public void onRequestFailure(Exception exception) {
                hideLoaders();
                AlertsHandler.showNetworkError(aActivity, exception);
            }
        };

        Map<String, String> params = new HashMap<>();
        params = GetOkHttpRequestParams.getHttpRequestParams(
                HttpRequestType.REGISTER_EMAIL, aRegisterDetail);
        params.put("device_id", Utility.getDeviceId(aContext));

        OkHttpHandler.httpPost(aContext, ConstantsUrls.get_login_url(), params,
                okHttpResponseHandler);
    }

    private void trackNewUser(boolean isNewUser, String gender) {
        if (isNewUser) {
            MoEHandler.trackEvent(aContext, MoEHandler.Events.NEW_USER);
        }
    }

    /**
     * Continue previously logged in session
     *
     * @param userDataJson The data containing the current status of the user who is
     *                     logged in.
     */
    private void continueSession(final JSONObject userDataJson) {
        String status = "";
        JSONObject data = userDataJson.optJSONObject("data");
        UserData userData = new UserData();
        if (userDataJson.optJSONObject("basics_data") != null) {
            userData.setDataUrl(
                    userDataJson.optJSONObject("basics_data").optString("url"));
        }

        if (data == null) {
            hideLoaders();
            Utility.logoutSession(aActivity);
            return;
        } else {
            status = data.optString("status");
            // isFb = data.optString("is_fb_connected").equalsIgnoreCase("Y");
        }
        if (Utility.isSet(data.optString("name"))) {
            userData.setFname(data.optString("name"));
            SPHandler.setString(aContext,
                    ConstantsSP.SHARED_KEYS_USER_NAME, data.optString("name"));
            MoEHelper.getInstance(aContext).setUserAttribute(
                    MoEHelperConstants.USER_ATTRIBUTE_USER_NAME,
                    SPHandler.getString(aContext,
                            ConstantsSP.SHARED_KEYS_USER_NAME));

        }
        setUserId(data.optString("user_id"), aContext);
        String gender = data.optString("gender");
        setGender(gender);

//        if(Utility.isSet(gender)){
//            SeventynineAdSDK.setGender(gender);
//        }

        userData.setFbDesignation(data.optString("fb_designation"));

        if (Utility.isSet(data.optString("current_credits"))) {
            SPHandler.setString(aContext,
                    ConstantsSP.SHARED_KEYS_CREDITS_COUNT,
                    data.optString("current_credits"));
        }
        if (Utility.isSet(data.optString("is_fb_connected"))) {
            userData.setIsFb(true);
            SPHandler.setString(aContext,
                    ConstantsSP.SHARED_KEYS_FB_CONNECTED,
                    data.optString("is_fb_connected"));
        }
        if (Utility.isSet(data.optString("city"))) {
            isCitySet = true;
            SPHandler.setString(aContext, ConstantsSP.SHARED_KEYS_USER_CITY, data.optString("city"));
//			InMobiSdk.setLocationWithCityStateCountry(data.optString("city"), );

        }
        if (Utility.isSet(data.optString("is_fb_connected"))
                && data.optString("is_fb_connected").equalsIgnoreCase("Y")) {
            isFbConnected = true;
        }

        String age = data.optString("age");
        if (Utility.isSet(age)) {
            SPHandler.setString(aContext, ConstantsSP.SHARED_KEYS_USER_AGE, age);
//            InMobiSdk.setAge(data.optInt("age"));
            SeventynineAdSDK.setAge(age);
        }

        CompetitorTrackingHandling.trackCompetitors(this);

        if (isPush) {
            launchPushActivity();
        } else {

            SPHandler.setString(aContext, ConstantsSP.SHARED_KEYS_USER_STATUS, status);
            MoEHelper.getInstance(aContext).setUserAttribute("UserStatus", status);
            if (status.equalsIgnoreCase("incomplete")) {
                getRegId(false, TrulyMadlyEventTypes.login);
                HttpTasks.fetchStaticData(aContext, userData, null,
                        new OnDataFetchedInterface() {
                            @Override
                            public void onSuccess(EditProfilePageType type,
                                                  UserData userData) {
                                launchActivity(ActivityName.REGISTRATION_FLOW, userData.getFbDesignation());
                            }

                            @Override
                            public void onFailure(Exception exception) {
                                hideLoaders();
                                continueSession(userDataJson);
                            }
                        }, TrulyMadlyActivities.login);
            } else if (status.equalsIgnoreCase("authentic")) {
                getRegId(false, TrulyMadlyEventTypes.login);
                launchActivity(ActivityName.MATCHES, null);
            } else if (status.equalsIgnoreCase("non-authentic")) {
                getRegId(false, TrulyMadlyEventTypes.login);
                launchActivity(ActivityName.TRUST_BUILDER, null);
            } else if (status.equalsIgnoreCase("suspended")) {
                hideLoaders();
                hideKeyboard();
                AlertsHandler.showConfirmDialog(aContext, R.string.sure_reactivate,
                        R.string.reactivate, R.string.cancel,
                        new ConfirmDialogInterface() {
                            @Override
                            public void onPositiveButtonSelected() {
                                sendReActivationCallToServer();
                            }

                            @Override
                            public void onNegativeButtonSelected() {
                                Utility.logoutSession(aActivity);
                            }
                        });
            } else if (status.equalsIgnoreCase("blocked")) {
                hideLoaders();
                AlertsHandler.showAlertDialog(aActivity, R.string.profile_blocked,
                        new AlertDialogInterface() {

                            @Override
                            public void onButtonSelected() {
                                Utility.logoutSession(aActivity);

                            }
                        });
            } else {
                hideLoaders();
            }
        }
    }

    private void setGender(String gender) {
        if (Utility.isSet(gender)) {
            // mhelper.setUserAttribute(MoEHelperConstants.USER_ATTRIBUTE_USER_GENDER,
            // MoEHelperConstants.)
            MoEHelper.getInstance(aContext).setUserAttribute(
                    MoEHelperConstants.USER_ATTRIBUTE_USER_GENDER,
                    gender.equalsIgnoreCase("m") ? MoEHelperConstants.GENDER_MALE
                            : MoEHelperConstants.GENDER_FEMALE);
            SPHandler.setString(aContext,
                    ConstantsSP.SHARED_KEYS_USER_GENDER, gender);

//            if (gender.equalsIgnoreCase("m"))
//                InMobiSdk.setGender(InMobiSdk.Gender.MALE);
//            else
//                InMobiSdk.setGender(InMobiSdk.Gender.FEMALE);
        }
    }

    private void sendReActivationCallToServer() {
        mProgressDialog = UiUtils.showProgressBar(aContext, mProgressDialog);
        CustomOkHttpResponseHandler okHttpResponseHandler = new CustomOkHttpResponseHandler(
                aContext) {

            @Override
            public void onRequestSuccess(JSONObject response) {
                fetchUserStatus();
            }

            @Override
            public void onRequestFailure(Exception exception) {
                hideLoaders();
                Utility.logoutSession(aActivity);
            }
        };
        OkHttpHandler.httpPost(aContext,
                ConstantsUrls.get_login_url(), GetOkHttpRequestParams
                        .getHttpRequestParams(HttpRequestType.ACTIVATE),
                okHttpResponseHandler);
    }

    private void hideLoaders() {
        mProgressDialog = UiUtils.hideProgressBar(mProgressDialog);
    }

    /**
     * getBool gcm registration id and send to server.
     *
     * @param isForce    force resend gcm id to server.
     * @param event_type
     */
    private void getRegId(boolean isForce, String event_type) {
        if (checkPlayServices()) {
            gcm = GoogleCloudMessaging.getInstance(this);
            regid = getRegistrationId(isForce);

            if (!Utility.isSet(regid)) {
                // register in background
                new GetRegId(event_type).execute(null, null, null);
            }
        }
    }

    private void launchActivity(ActivityName type, String fbDesignation) {
        hideLoaders();
        if (type == null) {
            return;
        }
        switch (type) {
            case REGISTRATION_FLOW:
                ActivityHandler.startRegistrationFlowActivity(aContext, isCitySet,
                        isFbConnected, fbDesignation);
                finish();
                break;
            case MATCHES:
                ActivityHandler.startMatchesActivity(aContext, launchSparksOnMatches, launchSelectOnMatches);
                finish();
                break;
            case TRUST_BUILDER:
                ActivityHandler.startTrustBuilder(aContext, false, true);
                finish();
            default:
                break;
        }
    }

    private void launchPushActivity() {

        if (!Utility.isUserLoggedIn(aContext)) {
            Utility.logoutSession(aActivity);
        }

        if (isPush && Utility.isSet(pushType)) {
            TrulyMadlyTrackEvent.trackEvent(aContext,
                    TrulyMadlyActivities.push,
                    TrulyMadlyEventTypes.push_open, 0, pushEventStatus,
                    null, true);
            switch (getNotificationTypeFromString(pushType)) {
                case MESSAGE:
                    hideLoaders();
                    if (Utility.isSet(push_notification_from_admin)
                            && this.push_notification_from_admin.equals("1")) {
                        ActivityHandler.startMessageOneonOneActivity(aContext, null, null, true, true, true, false);
                    } else {
                        // Check if chats are in hidden mode right now
                        // If yes, simple open the conversations activity
                        // Otherwise open the one-to-one chat activity
                        if (RFHandler.getBool(aContext,
                                userId,
                                ConstantsRF.RIGID_FIELD_IS_PASSCODE_PROTECTION_ENABLED)
                                && RFHandler.getBool(
                                aContext, userId,
                                ConstantsRF.RIGID_FIELD_CONVERSATIONS_HIDDEN))
                            ActivityHandler.startConversationListActivity(aContext, null);
                        else
                            ActivityHandler.startMessageOneonOneActivity(aContext,
                                    push_match_id, push_message_url, true, false, true, false);
                    }
                    finish();
                    break;
                case MATCH:
                    hideLoaders();
                    ActivityHandler.startProfileActivity(aContext, push_profile_url, true,
                            false, false);
                    finish();
                    break;
                case TRUST:
                    hideLoaders();
                    ActivityHandler.startTrustBuilder(aContext, true, false);
                    finish();
                    break;
                case VOUCHER:
                    ActivityHandler.startVoucherActivity(aContext, true);
                    finish();
                    break;
                case ACTIVITY:
                    ActivityHandler.startActivityFromString(aContext, true, pushClassName, false);
                    finish();
                    break;
                case EVENT:
                    if (Utility.isSet(pushEventId)) {
                        ActivityHandler.startEventProfileActivity(aContext, pushEventId, "", false);
                        finish();
                    }
                    break;
                case EVENT_LIST:
                    if (Utility.isSet(pushCategoryId)) {
                        ActivityHandler.startCategoriesActivity(aContext, pushCategoryId, pushCategoryName);
                        finish();
                    }
                    break;
                case WEB:
                    isPush = false;
                    if (Utility.isSet(pushWebUrl)) {
                        Utility.openWebsite(aContext, pushWebUrl);
                    }
                    getIntent().removeExtra("isPush");
                    break;
                case BUY_SPARKS:
                    launchSparksOnMatches = true;
                    isPush = false;
                    fetchUserStatus();
                    break;
                case BUY_SELECT:
                    launchSelectOnMatches = true;
                    isPush = false;
                    fetchUserStatus();
                    break;
                case PROMOTE:
                default:
                    isPush = false;
                    fetchUserStatus();
                    break;
            }
        }
    }

    /**
     * Gets the current registration ID for application on GCM service.
     * <p/>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     * registration ID.
     */
    private String getRegistrationId(boolean isForce) {

        String registrationId = SPHandler.getString(aContext,
                ConstantsSP.SHARED_KEYS_REG_ID);
        if (!Utility.isSet(registrationId) || isForce) {
            removeRegIds();
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        String registeredVersion = SPHandler.getString(aContext,
                ConstantsSP.SHARED_KEYS_APP_VERSION);
        String currentVersion = Utility.getAppVersionCode(aContext);
        if (!Utility.isSet(registeredVersion)
                || !registeredVersion.equalsIgnoreCase(currentVersion)) {
            TrulyMadlyTrackEvent.trackEvent(aContext,
                    TrulyMadlyActivities.update, TrulyMadlyEventTypes.update, 0,
                    null, null, true);
            Utility.packageVersionName = null;
            Utility.packageVersionCode = null;
            removeRegIds();
            JSONObject newJson = new JSONObject();
            try {
                newJson.put("Gender ", SPHandler.getString(aContext, ConstantsSP.SHARED_KEYS_USER_GENDER));
                newJson.put("User ID", Utility.getMyId(aContext));
                newJson.put("User Name", SPHandler.getString(aContext, ConstantsSP.SHARED_KEYS_USER_NAME));
                newJson.put("Age", SPHandler.getString(aContext, ConstantsSP.SHARED_KEYS_USER_AGE));
            } catch (JSONException ignored) {
            }
            MoEHelper.getInstance(aContext).trackEvent("App Updated", newJson);
            return "";
        }
        String timeStampString = SPHandler.getString(aContext,
                ConstantsSP.SHARED_KEYS_REG_ID_TIMESTAMP);
        long timestamp;
        if (Utility.isSet(timeStampString)) {
            timestamp = Long.valueOf(timeStampString);
            // expire reg id every twelve hours
            if ((new Date()).getTime() - timestamp > 12 * 3600 * 1000) {
                registrationId = "";
            }
        } else {
            removeRegIds();
            registrationId = "";
        }

        return registrationId;
    }

    private void removeRegIds() {
        SPHandler.remove(aContext, ConstantsSP.SHARED_KEYS_REG_ID);
        SPHandler.remove(aContext,
                ConstantsSP.SHARED_KEYS_REG_ID_TIMESTAMP);
    }

    private void sendRegistrationIdToBackend(boolean hasRegId,
                                             String event_type) {

        if (Utility.isSet(regid) && !isGcmNetworkCallInProgress) {

            String oldRegId = SPHandler.getString(
                    getApplicationContext(), ConstantsSP.SHARED_KEYS_REG_ID);
            if (!hasRegId || !Utility.isSet(oldRegId)
                    || !oldRegId.equalsIgnoreCase(regid)) {
                final String cookie = SPHandler.getString(aContext,
                        ConstantsSP.SHARED_KEYS_PHPSESSID);
                CustomOkHttpResponseHandler okHttpResponseHandler = new CustomOkHttpResponseHandler(
                        getApplicationContext()) {

                    @Override
                    public void onRequestSuccess(JSONObject response) {
                        isGcmNetworkCallInProgress = false;
                        switch (response.optInt("responseCode")) {
                            case 200:
                                Utility.storeRegistrationId(regid,
                                        getApplicationContext());
                                break;
                            case 403:
                                IOException e = new IOException(
                                        "GCM registration failed - Cookie : "
                                                + cookie + " - Error : "
                                                + response.optString("error",
                                                "no_error_key"));
                                Crashlytics.log(Log.ERROR, TAG, "Cookie : " + SPHandler.getString(aContext,
                                        ConstantsSP.SHARED_KEYS_PHPSESSID));
                                Crashlytics.logException(e);
                                break;
                            default:
                                break;
                        }
                    }

                    @Override
                    public void onRequestFailure(Exception exception) {
                        isGcmNetworkCallInProgress = false;
                    }
                };

                if (!Utility.isSet(event_type)) {
                    if (!Utility.isSet(SPHandler.getString(aContext,
                            ConstantsSP.SHARED_KEYS_INSTALL_GCM_REQUEST_SENT))) {
                        event_type = TrulyMadlyEventTypes.install;
                        SPHandler.setString(aContext,
                                ConstantsSP.SHARED_KEYS_INSTALL_GCM_REQUEST_SENT,
                                "yes");
                    } else {
                        event_type = TrulyMadlyEventTypes.login;
                    }
                }
                Crashlytics.log(Log.ERROR, TAG, "GCM event_type : " + event_type);
                if (event_type.equals(TrulyMadlyEventTypes.login) && !Utility.isUserLoggedIn(aContext)) {
                    return;
                }

                TrulyMadlyTrackEvent.trackEvent(aContext,
                        TrulyMadlyActivities.GCM, event_type, 0, regid, null, true);
                isGcmNetworkCallInProgress = true;
                Utility.sendRegistrationIdToBackend(regid,
                        okHttpResponseHandler, aContext, event_type);
            }
        }
    }


    @Override
    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(
                Context.INPUT_METHOD_SERVICE);
        if (imm != null && getCurrentFocus() != null) {
            imm.hideSoftInputFromWindow(
                    getCurrentFocus().getRootView().getWindowToken(), 0);
        }

    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If it
     * doesn't, display a dialog that allows users to download the APK from the
     * Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                try {
                    GooglePlayServicesUtil
                            .getErrorDialog(resultCode, this,
                                    Constants.PLAY_SERVICES_RESOLUTION_REQUEST)
                            .show();
                } catch (BadTokenException | ArrayIndexOutOfBoundsException ignored) {

                }
            } else {
                //"This device is not supported."
            }
            return false;
        }
        return true;
    }

    @Override
    public void onViewChanged(boolean isViewShown) {
        isPasswordViewOpen = isViewShown;
    }

    @SuppressLint({"NewApi", "Override"})
    @TargetApi(23)
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        if (grantResults == null || grantResults.length == 0) {
            return;
        }
        switch (requestCode) {
            case PermissionsHelper.REQUEST_GET_ACCOUNTS_PERMISSIONS_CODE:
                // if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // ((OnPermissionResultListener)registerHomeFragment).onPermissionGranted(requestCode);
                // } else {
                // ((OnPermissionResultListener)registerHomeFragment).onPermissionRejected(requestCode);
                // }
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions,
                        grantResults);
                break;
        }
    }

    private class GetRegId extends AsyncTask<Void, Void, Void> {

        final String event_type;

        public GetRegId(String event_type) {
            this.event_type = event_type;
        }

        @Override
        protected Void doInBackground(Void... param) {
            try {
                if (gcm == null) {
                    gcm = GoogleCloudMessaging.getInstance(aContext);
                }
                regid = gcm.register(Constants.SENDER_ID);
            } catch (Exception ignored) {
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void param) {

            CustomOkHttpResponseHandler responseHandler = new CustomOkHttpResponseHandler(
                    aContext) {

                @Override
                public void onRequestSuccess(JSONObject responseJSON) {
                    JSONArray ids = responseJSON
                            .optJSONArray("registration_ids");
                    boolean hasRegId = false;
                    if (ids != null) {
                        for (int i = 0; i < ids.length(); i++) {
                            if (regid.equalsIgnoreCase(ids.optString(i))) {
                                hasRegId = true;
                                break;
                            }
                        }
                    }
                    sendRegistrationIdToBackend(hasRegId, event_type);
                }

                @Override
                public void onRequestFailure(Exception exception) {
                }
            };
            responseHandler.setForceSuccess(true);

            // check gcm status from server.
            // use synchronouse methods
            // OkHttpHandler.httpGet(aContext, Constants.gcm_url,
            // GetOkHttpRequestParams.getHttpRequestParams(HttpRequestType.GET_REG_IDS),
            // responseHandler);

            // ignoring hasRegIdChecking
            // assuming server has reg id.
            sendRegistrationIdToBackend(true, event_type);
        }
    }

}
