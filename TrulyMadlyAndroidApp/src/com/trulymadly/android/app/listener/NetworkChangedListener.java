package com.trulymadly.android.app.listener;

import com.trulymadly.android.app.bus.NetworkChangeEvent;

/**
 * Created by avin on 24/08/16.
 */
public interface NetworkChangedListener {
    void onNetworkChanged(NetworkChangeEvent networkChangeEvent);
}
