package com.trulymadly.android.app.listener;

import org.json.JSONObject;

/**
 * Created by deveshbatra on 12/8/15.
 */
public interface CachedDataInterface {

    void onSuccess(JSONObject response);

    void onError(Exception e);


}
