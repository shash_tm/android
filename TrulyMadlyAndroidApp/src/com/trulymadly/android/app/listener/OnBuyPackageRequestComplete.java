package com.trulymadly.android.app.listener;

import com.trulymadly.android.app.modal.MySelectData;

/**
 * Created by udbhav on 14/06/16.
 */
public interface OnBuyPackageRequestComplete {

    void onSuccess(String developer_payload, boolean isConsumedStore, int newSparksAdded, int totalSparks, boolean isRelationshipExpertAdded, MySelectData mySelectData);
}
