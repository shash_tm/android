/**
 *
 */
package com.trulymadly.android.app.listener;

/**
 * @author udbhav
 */
public interface CategoryListItemClickInterface {
    void onCategoryItemClicked(String datespotId, String name);


}
