package com.trulymadly.android.app.listener;

import android.content.Intent;

/**
 * Created by avin on 15/12/15.
 */
public abstract class ConfirmDialogImpl implements ConfirmDialogInterface {
    public abstract void onPositiveButtonSelected(Intent data);
}
