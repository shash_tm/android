package com.trulymadly.android.app.listener;

/**
 * Created by deveshbatra on 7/21/16.
 */
public interface OnProgressVideoListener {

    void updateProgress(int time, int max, float scale);
}
