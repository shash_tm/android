package com.trulymadly.android.app.listener;

import android.content.Context;

import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.billing.PaymentMode;
import com.trulymadly.android.app.fragments.BuySparkEventListener;
import com.trulymadly.android.app.modal.MySelectData;
import com.trulymadly.android.app.modal.SelectPackageModal;
import com.trulymadly.android.app.utility.Utility;

import java.util.HashMap;

import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities.select;

/**
 * Created by avin on 02/11/16.
 */

public class TrackingBuySelectEventListener implements BuySparkEventListener {

    private BuySparkEventListener mBuySparkEventListener;
    private Context mContext;
    private String mLaunchSource, mMatchId;
    private int mLikesDone = -1, mHidesDone = -1, mSparksDone = -1;
    private boolean isFavouritesViewed = false;

//    public TrackingBuySelectEventListener(Context mContext, BuySparkEventListener mBuySparkEventListener,
//                                          String launchSource) {
//        this.mContext = mContext;
//        this.mLaunchSource = launchSource;
//        this.mBuySparkEventListener = mBuySparkEventListener;
//    }

    public TrackingBuySelectEventListener(Context mContext, BuySparkEventListener mBuySparkEventListener,
                                          String mLaunchSource, int mLikesDone, int mHidesDone,
                                          int mSparksDone, boolean isFavViewed, String matchId) {
        this.mBuySparkEventListener = mBuySparkEventListener;
        this.mContext = mContext;
        this.mLaunchSource = mLaunchSource;
        this.mLikesDone = mLikesDone;
        this.mHidesDone = mHidesDone;
        this.mSparksDone = mSparksDone;
        this.isFavouritesViewed = isFavViewed;
        this.mMatchId = matchId;
    }

    public void register(BuySparkEventListener mBuySparkEventListener) {
        this.mBuySparkEventListener = mBuySparkEventListener;
    }

    @Override
    public void closeFragment() {
        if (mBuySparkEventListener != null) {
            mBuySparkEventListener.closeFragment();
        }
    }

    @Override
    public void onBuySparkClicked(Object packageModal, String matchId) {
        SelectPackageModal selectPackageModal = (SelectPackageModal) packageModal;
        HashMap<String, String> eventInfo = new HashMap<>();
        if (Utility.isSet(mMatchId)) {
            eventInfo.put("match_id", mMatchId);
        }
        eventInfo.put("source", mLaunchSource);
        eventInfo.put("pkg_id", selectPackageModal.getmSku());

        if (mLikesDone >= 0) {
            eventInfo.put("likes_done", String.valueOf(mLikesDone));
        }
        if (mHidesDone >= 0) {
            eventInfo.put("hides_done", String.valueOf(mHidesDone));
        }
        if (mSparksDone >= 0) {
            eventInfo.put("sparks_done", String.valueOf(mSparksDone));
        }
        if (mLikesDone >= 0) {
            eventInfo.put("favorites_viewed", String.valueOf(isFavouritesViewed));
        }

        TrulyMadlyTrackEvent.trackEvent(mContext, select, TrulyMadlyEvent.TrulyMadlyEventTypes.buy, 0,
                TrulyMadlyEvent.TrulyMadlyEventStatus.buy,
                eventInfo, true, true);

        if (mBuySparkEventListener != null) {
            mBuySparkEventListener.onBuySparkClicked(packageModal, matchId);
        }
    }

    @Override
    public void onBuySparkSuccess(PaymentMode mPaymentMode, String sku,
                                  int newSparksAdded, int totalSparks,
                                  boolean isRelationshipExpertAdded, MySelectData mySelectData) {
        HashMap<String, String> eventInfo = new HashMap<>();
        if (Utility.isSet(mMatchId)) {
            eventInfo.put("match_id", mMatchId);
        }
        eventInfo.put("source", mLaunchSource);
        eventInfo.put("pkg_id", sku);
        if (mPaymentMode != null) {
            eventInfo.put("pg", mPaymentMode.name());
            if (mPaymentMode.getmMode() != null) {
                eventInfo.put("paytm_mode", mPaymentMode.getmMode().name());
            }
        }

        if (mLikesDone >= 0) {
            eventInfo.put("likes_done", String.valueOf(mLikesDone));
        }
        if (mHidesDone >= 0) {
            eventInfo.put("hides_done", String.valueOf(mHidesDone));
        }
        if (mSparksDone >= 0) {
            eventInfo.put("sparks_done", String.valueOf(mSparksDone));
        }
        if (mLikesDone >= 0) {
            eventInfo.put("favorites_viewed", String.valueOf(isFavouritesViewed));
        }

        TrulyMadlyTrackEvent.trackEvent(mContext, select, TrulyMadlyEvent.TrulyMadlyEventTypes.buy, 0,
                TrulyMadlyEvent.TrulyMadlyEventStatus.success,
                eventInfo, true, true);

        if (mBuySparkEventListener != null) {
            mBuySparkEventListener.onBuySparkSuccess(mPaymentMode, sku, newSparksAdded,
                    totalSparks, isRelationshipExpertAdded, mySelectData);
        }
    }

    @Override
    public void restorePurchasesClicked() {
        if (mBuySparkEventListener != null) {
            mBuySparkEventListener.restorePurchasesClicked();
        }
    }

    @Override
    public void onRegistered() {
        if (mBuySparkEventListener != null) {
            mBuySparkEventListener.onRegistered();
        }
    }
}
