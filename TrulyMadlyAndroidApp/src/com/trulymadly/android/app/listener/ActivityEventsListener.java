package com.trulymadly.android.app.listener;

/**
 * Created by avin on 01/06/16.
 */
public interface ActivityEventsListener {
    void reInitialize(Object object);
    void onBackPressedActivity();
    void registerListener(Object listener);
}
