package com.trulymadly.android.app.listener;

import com.trulymadly.android.app.modal.LinkedInData;

public interface OnImportFromLinkedin {
	void onSuccess(String source, LinkedInData liData);

	@SuppressWarnings("UnusedParameters")
	void onSuccessfullVerification(String source, String connections, String profile_pic);

	void onFailure(String source, String reason);

	void onCancel(String source);

	@SuppressWarnings("UnusedParameters")
	void onProgress(String source);

	void onLiFailure(String source, String reason);

	void onLiNetworkError(String source, Exception exception);
}
