/**
 *
 */
package com.trulymadly.android.app.listener;

import com.trulymadly.android.app.modal.FavoriteDataModal;

import java.util.ArrayList;

/**
 * @author udbhav
 */
public interface FavoriteSuggestionFetchedInterface {
    void onSuggestionListFetched(String category, ArrayList<FavoriteDataModal> mSuggestionsList);
}
