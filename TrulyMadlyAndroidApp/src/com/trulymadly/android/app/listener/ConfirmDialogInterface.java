/**
 * 
 */
package com.trulymadly.android.app.listener;

/**
 * @author udbhav
 *
 */
public interface ConfirmDialogInterface {
	void onPositiveButtonSelected();

	void onNegativeButtonSelected();
}
