package com.trulymadly.android.app.listener;

import com.trulymadly.android.app.RangeSeekBarView;

/**
 * Created by deveshbatra on 7/21/16.
 */
public interface OnRangeSeekBarListener {
    void onCreate(RangeSeekBarView rangeSeekBarView, int index, float value);

    void onSeek(RangeSeekBarView rangeSeekBarView, int index, float value);

    void onSeekStart(RangeSeekBarView rangeSeekBarView, int index, float value);

    void onSeekStop(RangeSeekBarView rangeSeekBarView, int index, float value);
}

