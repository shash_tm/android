/**
 * 
 */
package com.trulymadly.android.app.listener;

import org.json.JSONObject;

/**
 * @author udbhav
 * 
 */
public interface ConnectFBviaTrustBuilderCallbackInterface {
	void onSuccess(String access_token, String fbConnections, boolean isFBPhotoDeclined);

	void onFail(int error_msg_res_id);

	void onFail(String error);

	void onMismatch(String mismatch, JSONObject diff, boolean isFBPhotoDeclined);
}
