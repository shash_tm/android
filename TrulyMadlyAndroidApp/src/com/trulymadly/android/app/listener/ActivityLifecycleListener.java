package com.trulymadly.android.app.listener;

/**
 * Created by avin on 17/08/16.
 */
public interface ActivityLifecycleListener {
    void onStart();
    void onStop();
    void onPause();
    void onResume();
    void onDestroy();
    void onCreate();
}
