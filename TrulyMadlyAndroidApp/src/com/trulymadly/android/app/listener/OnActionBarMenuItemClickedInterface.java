/**
 * 
 */
package com.trulymadly.android.app.listener;

/**
 * @author udbhav
 *
 */
public interface OnActionBarMenuItemClickedInterface {
	void onViewProfileClicked();
	void onDeleteSparkClicked();
	void onShareProfileClicked();
	void onBlockUserClicked();
	void onUmatchUserClicked();
	void onClearChat();
}
