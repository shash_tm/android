package com.trulymadly.android.app.listener;

/**
 * Created by avin on 11/08/16.
 */
public interface VideoControlListener {
    void prePlay(int position);
    void play(int position);
    void pause(int position);

    void resume(int position);
    void stop(int position);
    void destroy(int position);
}
