/**
 * 
 */
package com.trulymadly.android.app.listener;

/**
 * @author udbhav
 *
 */
public interface ThreeButtonDialogInterface {
	void onPositiveButtonSelected();

	void onNegativeButtonSelected();

	@SuppressWarnings("EmptyMethod")
	void onNeutralButtonSelected();

}
