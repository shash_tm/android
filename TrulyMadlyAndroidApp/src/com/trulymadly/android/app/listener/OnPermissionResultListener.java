package com.trulymadly.android.app.listener;

public interface OnPermissionResultListener {
	void onPermissionGranted(int requestCode);

	void onPermissionRejected();
}
