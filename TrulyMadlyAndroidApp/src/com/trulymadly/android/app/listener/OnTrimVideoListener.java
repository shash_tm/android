package com.trulymadly.android.app.listener;

import android.net.Uri;

/**
 * Created by deveshbatra on 7/22/16.
 */
public interface OnTrimVideoListener {

    void getResult(final Uri uri);

    void cancelAction();

    void errorOnTrim();
}

