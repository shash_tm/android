package com.trulymadly.android.app.listener;

import com.trulymadly.android.app.modal.ActiveConversationModal;

/**
 * Created by deveshbatra on 3/21/16.
 */
public interface ShareEventUserPicClickedInterface {
    void onShareEventUserPicClicked(ActiveConversationModal user);
}
