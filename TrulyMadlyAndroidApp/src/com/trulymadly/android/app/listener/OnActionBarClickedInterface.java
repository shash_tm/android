/**
 * 
 */
package com.trulymadly.android.app.listener;

/**
 * @author udbhav
 *
 */
public interface OnActionBarClickedInterface {
	void onBackClicked();

	void onLocationClicked();

	void onUserProfileClicked();

	void onConversationsClicked();

    void onTitleClicked();

	@SuppressWarnings("EmptyMethod")
	void onTitleLongClicked();

	void onSearchViewOpened();

	void onSearchViewClosed();

	void onCategoriesClicked();

	void onCuratedDealsClicked();
}
