/**
 *
 */
package com.trulymadly.android.app.listener;

/**
 * @author udbhav
 */
public interface DateSpotListItemClickInterface {
    void onDateSpotItemClicked(String dateSpotId);

}
