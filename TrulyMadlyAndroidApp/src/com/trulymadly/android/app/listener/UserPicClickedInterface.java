package com.trulymadly.android.app.listener;

import com.trulymadly.android.app.modal.UserInfoModal;

/**
 * Created by deveshbatra on 3/21/16.
 */
public interface UserPicClickedInterface {
    void onUserPicClicked(UserInfoModal user);
}
