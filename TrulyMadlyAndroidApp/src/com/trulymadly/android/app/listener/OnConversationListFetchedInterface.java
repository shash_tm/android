package com.trulymadly.android.app.listener;

public interface OnConversationListFetchedInterface {
    void onSuccess(int unreadConversationsCount);

    void onFail(Exception exception);
}
