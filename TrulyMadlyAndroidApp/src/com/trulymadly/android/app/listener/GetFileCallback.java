/**
 *
 */
package com.trulymadly.android.app.listener;

import android.net.Uri;

import com.trulymadly.android.app.json.Constants;

import java.io.File;

/**
 * @author udbhav
 */
public interface GetFileCallback {
    void onGetFileComplete(File file, Uri selectedImage, Constants.HttpRequestType httpRequestType);

}
