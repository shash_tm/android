package com.trulymadly.android.app.listener;

import com.trulymadly.android.app.json.Constants.PhotoUploadType;

import org.json.JSONArray;

public interface OnPhotoUploaded {
	void onPhotoUploded(PhotoUploadType photoUploadType, JSONArray photoUploadResponse);
}
