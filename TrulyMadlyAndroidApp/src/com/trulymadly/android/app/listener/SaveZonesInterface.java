/**
 *
 */
package com.trulymadly.android.app.listener;

import java.util.ArrayList;

/**
 * @author udbhav
 */
public interface SaveZonesInterface {
    void onSaveClicked(ArrayList<String> selectedZoneIds);
}
