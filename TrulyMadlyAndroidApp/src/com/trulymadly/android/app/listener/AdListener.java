package com.trulymadly.android.app.listener;

import com.trulymadly.android.app.modal.AdNativeModal;

/**
 * Created by avin on 29/10/15.
 */
public interface AdListener {
    void onAdLoadSucceeded(AdNativeModal modal);

    void onAdLoadFailed();

    void onAdLoadDismissed();
}
