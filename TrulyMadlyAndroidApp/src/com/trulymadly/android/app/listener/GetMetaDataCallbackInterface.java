/**
 * 
 */
package com.trulymadly.android.app.listener;


/**
 * @author udbhav
 *
 */
public interface GetMetaDataCallbackInterface {
	void onMetaDataReceived();

}
