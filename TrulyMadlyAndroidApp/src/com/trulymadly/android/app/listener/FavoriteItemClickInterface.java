/**
 *
 */
package com.trulymadly.android.app.listener;

import com.trulymadly.android.app.modal.FavoriteDataModal;

/**
 * @author udbhav
 */
public interface FavoriteItemClickInterface {
    void showMoreFavorites();

    void onFavoriteClicked(FavoriteDataModal favoriteDataModal, boolean isSelected);
}
