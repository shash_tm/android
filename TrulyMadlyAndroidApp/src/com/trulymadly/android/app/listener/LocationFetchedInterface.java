package com.trulymadly.android.app.listener;

import android.location.Location;

public interface LocationFetchedInterface {
    void onLocationRecived(Location location);
}
