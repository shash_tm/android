/**
 *
 */
package com.trulymadly.android.app.listener;

import android.content.Context;
import android.os.Handler;

import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.custom.ResponseFailureException;
import com.trulymadly.android.app.utility.OkHttpHandler;
import com.trulymadly.android.app.utility.TimeUtils;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author udbhav
 */
public abstract class CustomOkHttpResponseHandler implements Callback {

    private final static String TAG = "OK HTTP RESPONSE HANDLER";
    private final static String EVENT_STATUS_SUCCESS = "success";
    private final static String EVENT_STATUS_SUCCESS_304 = "success_304";

    private final static String EVENT_STATUS_TIMEOUT = "error_timeout";
    private final static String EVENT_STATUS_ERROR_OTHER = "error";
    private final static String EVENT_STATUS_ERROR_403 = "error403";
    private final static String EVENT_STATUS_ERROR_401 = "error401";
    private final Context aContext;
    private long trkStartTime, trkEndTime;
    private String trkActivity, trkEventType, trkEventStatus = null;
    private Map<String, String> trkEventinfo;
    private boolean forceSuccess = false;

    public CustomOkHttpResponseHandler(Context aContext) {
        this.aContext = aContext;
        //NetworkHandler.getmDeviceBandwidthSampler().startSampling();
    }

    public CustomOkHttpResponseHandler(Context context, String activity,
                                       String event_type) {
        this(context, activity, event_type, null, null);
    }

    public CustomOkHttpResponseHandler(Context context, String activity,
                                       String event_type, String event_status) {
        this(context, activity, event_type, event_status, null);
    }

    public CustomOkHttpResponseHandler(Context aContext, String activity,
                                       String event_type, Map<String, String> event_info) {
        this(aContext, activity, event_type, null, event_info);
    }

    public CustomOkHttpResponseHandler(Context aContext, String activity,
                                       String event_type, String event_status, Map<String, String> event_info) {
        this.aContext = aContext;
        this.trkActivity = activity;
        this.trkEventType = event_type;
        this.trkEventinfo = event_info;
        this.trkEventStatus = event_status;
        this.setStartTime();
        //NetworkHandler.getmDeviceBandwidthSampler().startSampling();
    }

    private Handler getMainHandler() {
        return new Handler(aContext.getMainLooper());
    }

    /**
     * Called when the request could not be executed due to cancellation, a connectivity problem or
     * timeout. Because networks can fail during an exchange, it is possible that the remote server
     * accepted the request before the failure.
     *
     * @param call
     * @param e
     */
    @Override
    public void onFailure(Call call, IOException e) {
        //NetworkHandler.getmDeviceBandwidthSampler().stopSampling();
        if (e != null && e instanceof SocketTimeoutException) {
            setEndTime(EVENT_STATUS_TIMEOUT);
        } else {
            String error_message = EVENT_STATUS_ERROR_OTHER;
            if (Utility.isSet(e.getMessage())) {
                error_message += "---" + e.getMessage();
            }
            if (e.getCause() != null && Utility.isSet(e.getCause().getMessage())) {
                error_message += "---" + e.getCause().getMessage();
            }
            setEndTime(error_message);
        }
        callFailureOnUiThread(e);
    }


    /**
     * Called when the HTTP response was successfully returned by the remote server. The callback may
     * proceed to read the response body with {@link Response#body}. The response is still live until
     * its response body is closed with {@code response.body().close()}. The recipient of the callback
     * may even consume the response body on another thread.
     * <p/>
     * <p>Note that transport-layer success (receiving a HTTP response code, headers and body) does
     * not necessarily indicate application-layer success: {@code response} may still indicate an
     * unhappy HTTP response code like 404 or 500.
     *
     * @param call
     * @param response
     */
    @Override
    public void onResponse(Call call, Response response) throws IOException {

        //NetworkHandler.getmDeviceBandwidthSampler().stopSampling();
        if (!response.isSuccessful()) {
            if (trkEventinfo == null) {
                trkEventinfo = new HashMap<>();
            }
            trkEventinfo.put("httpResponseCode", String.valueOf(response.code()));
            setEndTime(EVENT_STATUS_ERROR_OTHER);
            if (response.code() == 500) {
                callFailureOnUiThread(new ResponseFailureException("Unexpected code " + response));
            } else {
                callFailureOnUiThread(new IOException("Unexpected code " + response));
            }
            return;
        }
        String str = null;
        List<String> setCookieHeader = response.headers("Set-Cookie");
        for (String cookie : setCookieHeader) {
            if (checkNotNull(cookie).contains("=deleted")) {
                OkHttpHandler.clearCookie(aContext, cookie.split("=deleted")[0]);
            }
        }
        try {
            str = response.body().string();
            JSONObject responseJSON = new JSONObject(str);
            if (responseJSON != null
                    && !responseJSON.isNull("responseCode")) {
                switch (responseJSON.optInt("responseCode")) {
                    case 304:
                    case 200:
                        if (trkEventStatus != null) {
                            setEndTime(trkEventStatus);
                        } else {
                            setEndTime(EVENT_STATUS_SUCCESS);
                        }
                        callSuccessOnUiThread(responseJSON);
                        break;
                    case 401:
                        setEndTime(EVENT_STATUS_ERROR_401 + responseJSON.optString("error", ""));
                        callLogoutOnUiThread();
                        break;
                    case 403:
                        setEndTime(EVENT_STATUS_ERROR_403
                                + " - "
                                + responseJSON.optString("error",
                                "unknown error"));
                        callSuccessOnUiThread(responseJSON);
                        break;
                    default:
                        if (trkEventinfo == null) {
                            trkEventinfo = new HashMap<>();
                        }
                        trkEventinfo.put("httpResponseCode", String.valueOf(response.code()));
                        setEndTime(EVENT_STATUS_ERROR_OTHER);
                        callFailureOnUiThread((new IOException(
                                "Unexpected responseCode ")));
                        break;
                }
            } else if (responseJSON != null) {
                setEndTime(EVENT_STATUS_SUCCESS);
                callSuccessOnUiThread(responseJSON);
            }
        } catch (JSONException e) {
            if (str != null) {
                try {
                    JSONArray responseJSON = new JSONArray(str);
                    callSuccessOnUiThreadForArray(responseJSON);
                } catch (JSONException e1) {
                    callFailureOnUiThread(e1);
                }
            } else {
                callFailureOnUiThread(e);
            }
        } catch (IOException e) {
            callFailureOnUiThread(e);
        } catch (OutOfMemoryError e) {
            callFailureOnUiThread(new IOException("OutOfMemoryError"));
        } catch (AssertionError e) {
            callFailureOnUiThread(new IOException("AssertionError"));
        } catch (ArrayIndexOutOfBoundsException e) {
            callFailureOnUiThread(new IOException("ArrayIndexOutOfBoundsException"));
        }

    }

    private void callLogoutOnUiThread() {
        if (forceSuccess) {
            callSuccessOnUiThread(null);
            return;
        }
        getMainHandler().post(new Runnable() {
            @Override
            public void run() {
                Utility.logoutSession(aContext);
            }
        });

    }

    private void callFailureOnUiThread(final Exception exception) {
        if (forceSuccess) {
            callSuccessOnUiThread(null);
            return;
        }
        getMainHandler().post(new Runnable() {
            @Override
            public void run() {
                onRequestFailure(exception);
            }
        });
    }

    private void callSuccessOnUiThread(final JSONObject responseJSON) {
        getMainHandler().post(new Runnable() {
            @Override
            public void run() {
                if (responseJSON == null) {
                    onRequestSuccess(new JSONObject());
                } else {
                    onRequestSuccess(responseJSON);
                }
            }
        });
    }

    private void callSuccessOnUiThreadForArray(final JSONArray responseJSON) {
        getMainHandler().post(new Runnable() {
            @Override
            public void run() {
                if (responseJSON == null) {
                    onRequestSuccess(new JSONArray());
                } else {
                    onRequestSuccess(responseJSON);
                }
            }
        });
    }

    /**
     * Methods to be implemented by Class objects
     */

    public abstract void onRequestSuccess(JSONObject responseJSON);

    public abstract void onRequestFailure(Exception exception);

    public void onRequestSuccess(JSONArray responseJSON) {
    }


    /**
     * Event tracking methods
     */
    public void setStartTime() {
        trkStartTime = (new java.util.Date()).getTime();
    }

    public String getActivity() {
        return trkActivity;
    }

    public void setActivity(String activity) {
        trkActivity = activity;
    }

    private void setEndTime(String trkEventStatus) {
        trkEndTime = (new java.util.Date()).getTime();
        if (Utility.isSet(trkActivity)) {
            TrulyMadlyTrackEvent.trackEvent(aContext, trkActivity,
                    trkEventType, getTimeDiff(), trkEventStatus, trkEventinfo, true);
        }
    }

    public String getEventType() {
        return trkEventType;
    }

    public void setEventType(String event_type) {
        trkEventType = event_type;
    }

    private long getTimeDiff() {
        return trkEndTime - trkStartTime;
    }

    public long getTimeDiffNow() {
        return TimeUtils.getSystemTimeInMiliSeconds() - trkStartTime;
    }

    public boolean isForceSuccess() {
        return forceSuccess;
    }

    public void setForceSuccess(boolean forceSuccess) {
        this.forceSuccess = forceSuccess;
    }

}
