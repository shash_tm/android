package com.trulymadly.android.app.listener;

public interface OnSocketConnectedCallbackInterface {
	void connected();

	void failed();
}
