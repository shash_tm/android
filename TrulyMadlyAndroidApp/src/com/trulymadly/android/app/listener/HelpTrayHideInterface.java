package com.trulymadly.android.app.listener;

/**
 * Created by deveshbatra on 5/16/16.
 */
public interface HelpTrayHideInterface {
    void hideHelpTrayMethod(boolean showHelpIcon);
}
