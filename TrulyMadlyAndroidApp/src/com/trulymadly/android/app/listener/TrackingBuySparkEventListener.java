package com.trulymadly.android.app.listener;

import android.content.Context;

import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.billing.PaymentMode;
import com.trulymadly.android.app.fragments.BuySparkEventListener;
import com.trulymadly.android.app.modal.MySelectData;
import com.trulymadly.android.app.modal.SparkPackageModal;
import com.trulymadly.android.app.utility.MoEHandler;
import com.trulymadly.android.app.utility.SparksHandler;
import com.trulymadly.android.app.utility.Utility;

import java.util.HashMap;

import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities.sparks;

/**
 * Created by avin on 07/07/16.
 */
public class TrackingBuySparkEventListener implements BuySparkEventListener {

    private BuySparkEventListener mBuySparkEventListener;
    private boolean isFromIntro, isFromScenes;
    private String mReceiverId, mScenesId;
    private Context mContext;
    private int mLikesDone, mHidesDone, mSparksDone;
    private boolean favorites_viewed;

    public TrackingBuySparkEventListener(Context mContext, BuySparkEventListener mBuySparkEventListener) {
        this.mContext = mContext;
        this.mBuySparkEventListener = mBuySparkEventListener;
    }

    public void register(BuySparkEventListener mBuySparkEventListener) {
        this.mBuySparkEventListener = mBuySparkEventListener;
    }

    public BuySparkEventListener getmBuySparkEventListener() {
        return mBuySparkEventListener;
    }

    public void setmBuySparkEventListener(BuySparkEventListener mBuySparkEventListener) {
        this.mBuySparkEventListener = mBuySparkEventListener;
    }

    public void reInitialize(boolean isFromIntro, boolean isFromScenes, String mScenesId, String mReceiverId, int mLikesDone,
                             int mHidesDone, int mSparksDone, boolean favorites_viewed) {
        this.isFromIntro = isFromIntro;
        this.isFromScenes = isFromScenes;
        this.mScenesId = mScenesId;
        this.mReceiverId = mReceiverId;
        this.mLikesDone = mLikesDone;
        this.mHidesDone = mHidesDone;
        this.mSparksDone = mSparksDone;
        this.favorites_viewed = favorites_viewed;
    }

    @Override
    public void closeFragment() {
        if (mBuySparkEventListener != null) {
            mBuySparkEventListener.closeFragment();
        }
    }

    @Override
    public void onBuySparkClicked(Object packageModal, String matchId) {
        SparkPackageModal sparkPackageModal = (SparkPackageModal) packageModal;
        HashMap<String, String> eventInfo = new HashMap<>();
        if (Utility.isSet(mReceiverId)) {
            eventInfo.put("match_id", mReceiverId);
        } else {
            eventInfo.put("match_id", TrulyMadlyEvent.TrulyMadlyActivities.sparks);
        }
        eventInfo.put("sparks_left", String.valueOf(SparksHandler.getSparksLeft(mContext)));
        eventInfo.put("from_intro", String.valueOf(isFromIntro));
        eventInfo.put("from_scenes", String.valueOf(isFromScenes));
        if (isFromScenes) {
            eventInfo.put("event_id", mScenesId);
        }
        eventInfo.put("pkg_id", sparkPackageModal.getmPackageSku());

        eventInfo.put("likes_done", String.valueOf(mLikesDone));
        eventInfo.put("hides_done", String.valueOf(mHidesDone));
        eventInfo.put("sparks_done", String.valueOf(mSparksDone));
        eventInfo.put("favorites_viewed", String.valueOf(favorites_viewed));
        TrulyMadlyTrackEvent.trackEvent(mContext, sparks, TrulyMadlyEvent.TrulyMadlyEventTypes.buy, 0,
                TrulyMadlyEvent.TrulyMadlyEventStatus.buy,
                eventInfo, true, true);

        MoEHandler.trackEvent(mContext, MoEHandler.Events.CLICKED_ON_A_PACKAGE + " " + sparkPackageModal.getmPackageSku());

        if (mBuySparkEventListener != null) {
            mBuySparkEventListener.onBuySparkClicked(sparkPackageModal, mReceiverId);
        }
    }

    @Override
    public void onBuySparkSuccess(PaymentMode mPaymentMode, String sku, int newSparksAdded,
                                  int totalSparks, boolean isRelationshipExpertAdded, MySelectData mySelectData) {
        HashMap<String, String> eventInfo = new HashMap<>();
        if (Utility.isSet(mReceiverId)) {
            eventInfo.put("match_id", mReceiverId);
        } else {
            eventInfo.put("match_id", TrulyMadlyEvent.TrulyMadlyActivities.sparks);
        }
        eventInfo.put("from_intro", String.valueOf(isFromIntro));
        eventInfo.put("from_scenes", String.valueOf(isFromScenes));
        if (isFromScenes) {
            eventInfo.put("event_id", mScenesId);
        }
        eventInfo.put("total_sparks", String.valueOf(totalSparks));
        eventInfo.put("pkg_id", sku);
        if(mPaymentMode != null) {
            eventInfo.put("pg", mPaymentMode.name());
            if(mPaymentMode.getmMode() != null){
                eventInfo.put("paytm_mode", mPaymentMode.getmMode().name());
            }
        }
        TrulyMadlyTrackEvent.trackEvent(mContext, sparks, TrulyMadlyEvent.TrulyMadlyEventTypes.buy, 0,
                TrulyMadlyEvent.TrulyMadlyEventStatus.success,
                eventInfo, true, true);

        MoEHandler.trackEvent(mContext, MoEHandler.Events.PURCHASE_COMPLETED + " " + sku);

        if (mBuySparkEventListener != null) {
            mBuySparkEventListener.onBuySparkSuccess(null, sku, newSparksAdded, totalSparks,
                    isRelationshipExpertAdded, mySelectData);
        }
    }

    @Override
    public void restorePurchasesClicked() {
        if (mBuySparkEventListener != null) {
            mBuySparkEventListener.restorePurchasesClicked();
        }
    }

    @Override
    public void onRegistered() {
        if (mBuySparkEventListener != null) {
            mBuySparkEventListener.onRegistered();
        }
    }

    public boolean isFromIntro() {
        return isFromIntro;
    }

    public void setFromIntro(boolean fromIntro) {
        this.isFromIntro = fromIntro;
    }

    public String getReceiverId() {
        return mReceiverId;
    }

    public void setReceiverId(String receiverId) {
        this.mReceiverId = receiverId;
    }
}
