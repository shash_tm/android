package com.trulymadly.android.app.listener;

public interface OnDataLoadedInterface {
	void onLoadSuccess(long mTimeTaken);

	void onLoadFailure(Exception e, long mTimeTaken);
}
