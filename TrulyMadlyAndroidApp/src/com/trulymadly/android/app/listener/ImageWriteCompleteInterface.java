package com.trulymadly.android.app.listener;

/**
 * @author udbhav
 *
 */
public interface ImageWriteCompleteInterface {
	void onSuccess(long mTimeTaken);

	void onFail(Exception e, long mTimeTaken);
}
