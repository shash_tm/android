/**
 * 
 */
package com.trulymadly.android.app.listener;

import com.trulymadly.android.app.json.Constants.EditProfilePageType;
import com.trulymadly.android.app.modal.UserData;

/**
 * @author udbhav
 *
 */
public interface OnDataFetchedInterface {
	void onSuccess(EditProfilePageType type, UserData userData);

	void onFailure(Exception exception);
}
