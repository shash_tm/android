/**
 * 
 */
package com.trulymadly.android.app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources.NotFoundException;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.method.PasswordTransformationMethod;
import android.view.InflateException;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.crashlytics.android.Crashlytics;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.listener.AlertDialogInterface;
import com.trulymadly.android.app.listener.CustomOkHttpResponseHandler;
import com.trulymadly.android.app.utility.ABHandler;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.OkHttpHandler;
import com.trulymadly.android.app.utility.UiUtils;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * @author udbhav
 * 
 */
public class LoginFragment extends Fragment implements OnClickListener {

    private OnLoginFragmentClick mCallback;
    private View loginFragmentView;
    private View fields, forget_password, login_buttons_container, fb_container;
    private EditText emailEditText, userId, password;
	private Context aContext;
	private Activity aActivity;
    private ProgressDialog mProgressDialog;
    private OnForgetPasswordViewChanged onForgetPasswordViewChanged;
    private Button submitBtn;


	public LoginFragment() {
	}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        aContext = getActivity();
        aActivity = getActivity();
        try {
            loginFragmentView = inflater.inflate(R.layout.login_new, container,
                    false);
        } catch (InflateException | OutOfMemoryError | NotFoundException ignored) {
            aActivity.finish();
            return loginFragmentView;
        }

        Button login = (Button) loginFragmentView.findViewById(R.id.btn_login);
        login.setOnClickListener(this);
        View login_fb_id = loginFragmentView.findViewById(R.id.login_fb_id);
        login_fb_id.setOnClickListener(this);
        Button new_user = (Button) loginFragmentView.findViewById(R.id.btn_newuser);
        new_user.setOnClickListener(this);
        fields = loginFragmentView.findViewById(R.id.fields_container);
        forget_password = loginFragmentView
                .findViewById(R.id.layout_forget_password);
        login_buttons_container = loginFragmentView
                .findViewById(R.id.login_buttons_container);
        fb_container = loginFragmentView
                .findViewById(R.id.fb_container);

        OnClickListener forgotPasswordClickListener = new OnClickListener() {

            @Override
            public void onClick(View v) {
                onForgotPasswordClick(v);
            }
        };

        ABHandler.setVisibility(aContext, ABHandler.ABTYPE.AB_EMAIL_SIGNUP_DISABLED, new_user);


        // Setting up animation
        View containerLayout = loginFragmentView
                .findViewById(R.id.loginHomeContainer);
        //Animation slideUpAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.push_left_in);
        //containerLayout.startAnimation(slideUpAnimation);

        ((EditText) loginFragmentView.findViewById(R.id.et_passwd))
                .setOnEditorActionListener(new OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId,
                                                  KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_GO) {
                            mCallback
                                    .onLoginFragmentClick(v, loginFragmentView);
                        }
                        return false;
                    }
                });

        emailEditText = (EditText) loginFragmentView
                .findViewById(R.id.et_email);

        emailEditText.setOnEditorActionListener(new OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    submitForgotPassword();
                }
                return false;
            }
        });
        userId = (EditText) loginFragmentView
                .findViewById(R.id.et_userid_login);
        password = (EditText) loginFragmentView.findViewById(R.id.et_passwd);

        password.setTypeface(Typeface.DEFAULT);
        password.setTransformationMethod(new PasswordTransformationMethod());

        TextView tvForgotPassword = (TextView) loginFragmentView
                .findViewById(R.id.tv_forget_password);

        submitBtn = (Button) loginFragmentView.findViewById(R.id.btn_submit);

        tvForgotPassword.setOnClickListener(forgotPasswordClickListener);
        submitBtn.setOnClickListener(forgotPasswordClickListener);

        return loginFragmentView;
    }

    private void onForgotPasswordClick(View v) {
        switch (v.getId()) {
            case R.id.tv_forget_password:
                UiUtils.hideKeyBoard(aContext);
                password.setText("");
                emailEditText.setText(userId.getText().toString().trim());
                setVisibility(forget_password, fields);
                setVisibility(submitBtn, login_buttons_container);
                setVisibility(submitBtn, fb_container);
                passFlag(true);
                break;
            case R.id.btn_submit:
                submitForgotPassword();
                break;
            default:
                break;

        }

	}

    private void passFlag(boolean flag) {
        onForgetPasswordViewChanged.onViewChanged(flag);
    }

    public void closeForgotPassword() {
        passFlag(false);
        emailEditText.setText("");
        setVisibility(fields, forget_password);
        setVisibility(login_buttons_container, submitBtn);
        setVisibility(fb_container, submitBtn);
    }

    private void submitForgotPassword() {

        Validator validator = new Validator();

        String email = emailEditText.getText().toString().trim();

        // email is valid
        if (validator.emailValidator(email, emailEditText,
                "Please enter a valid email id.")) {
            CustomOkHttpResponseHandler okHttpResponseHandler = new CustomOkHttpResponseHandler(
                    aContext, TrulyMadlyActivities.login,
                    TrulyMadlyEventTypes.forget_password_server_call) {

                @Override
                public void onRequestSuccess(JSONObject responseJSON) {
                    // hide
                    mProgressDialog = UiUtils.hideProgressBar(mProgressDialog);

                    int responseCode = responseJSON.optInt("responseCode");
                    String message = "";
                    AlertDialogInterface onComplete = null;

                    if (responseCode == 200) {
                        onComplete = new AlertDialogInterface() {
                            @Override
                            public void onButtonSelected() {
                                closeForgotPassword();
                            }

                        };
                        message = aActivity.getResources().getString(R.string.default_forgot_password_success);
                    } else if (responseCode == 403) {
                        message = responseJSON.optString("error",
                                aActivity.getResources().getString(R.string.default_forgot_password_error));
                    } else {
                        message = aActivity.getResources().getString(R.string.default_forgot_password_error);
                    }

                    AlertsHandler.showAlertDialog(aActivity, message, onComplete);

				}

                @Override
                public void onRequestFailure(Exception exception) {
                    // hide
                    mProgressDialog = UiUtils.hideProgressBar(mProgressDialog);
                    AlertsHandler.showNetworkError(aActivity, exception);
                }

            };
            Map<String, String> params = new HashMap<>();
            params.put("email", email);

            // show
            UiUtils.hideKeyBoard(aContext);
            /*
			 * InputMethodManager imm = (InputMethodManager)
			 * getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			 * imm.hideSoftInputFromInputMethod(submitBtn.getWindowToken(), 0);
			 */

            mProgressDialog = UiUtils
                    .showProgressBar(aContext, mProgressDialog);
            OkHttpHandler.httpGet(aContext, ConstantsUrls.get_forget_password_url(),
                    params, okHttpResponseHandler);

		}

	}

    private void setVisibility(View setView, View offView) {
        setView.setVisibility(View.VISIBLE);
        offView.setVisibility(View.GONE);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception.
        try {
            onForgetPasswordViewChanged = (OnForgetPasswordViewChanged) activity;
            mCallback = (OnLoginFragmentClick) activity;
        } catch (ClassCastException e) {
            Crashlytics.logException(e);
            throw new ClassCastException(activity.toString()
                    + " must implement OnLoginFragmentClick");
        }
	}

    @Override
    public void onClick(View v) {
        mCallback.onLoginFragmentClick(v, loginFragmentView);
    }

    public interface OnLoginFragmentClick {
        void onLoginFragmentClick(View v, View loginFragmentView);

	}

    public interface OnForgetPasswordViewChanged {
        void onViewChanged(boolean isViewShown);
    }
	
	

}
