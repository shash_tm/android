package com.trulymadly.android.app.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;

import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventStatus;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.listener.CustomOkHttpResponseHandler;
import com.trulymadly.android.app.listener.GetSubReasonsInterface;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class DidntLikeItHereFragment extends Fragment
        implements GetSubReasonsInterface, OnCheckedChangeListener {

    private Context mContext;
    //	private CheckBox mReason1, mReason2, mReason3, mReason4, mReason5;
    private ArrayList<String> mSelectedReasons;
    private EditText mOthersET;
    private String mOthersReason;

    private ProgressDialog mProgressDialog;
    private Unbinder unbinder;

    public DidntLikeItHereFragment() {
        // Required empty public constructor
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mDidntLikeFragment = inflater.inflate(R.layout.del_acc_didnt_like_here_fragment_new, container, false);
        unbinder = ButterKnife.bind(this, mDidntLikeFragment);
        mContext = getActivity();
        mOthersET = (EditText) mDidntLikeFragment.findViewById(R.id.others_et);
//		mReason1 = (CheckBox) mDidntLikeFragment.findViewById(R.id.reason_cb_1);
//		mReason2 = (CheckBox) mDidntLikeFragment.findViewById(R.id.reason_cb_2);
//		mReason3 = (CheckBox) mDidntLikeFragment.findViewById(R.id.reason_cb_3);
//		mReason4 = (CheckBox) mDidntLikeFragment.findViewById(R.id.reason_cb_4);
//		mReason5 = (CheckBox) mDidntLikeFragment.findViewById(R.id.reason_cb_5);
//		mDidntLikeFragment.getRootView().
//
//		mReason1.setOnCheckedChangeListener(this);
//		mReason2.setOnCheckedChangeListener(this);
//		mReason3.setOnCheckedChangeListener(this);
//		mReason4.setOnCheckedChangeListener(this);
//		mReason5.setOnCheckedChangeListener(this);

        View mProfileVisibilityButton = mDidntLikeFragment.findViewById(R.id.profile_visibility_button);
        mProfileVisibilityButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                final boolean isChecked = false;
                CustomOkHttpResponseHandler okHttpResponseHandler = new CustomOkHttpResponseHandler(
                        mContext) {

                    @Override
                    public void onRequestSuccess(JSONObject responseJSON) {
                        SPHandler.setBool(mContext,
                                Constants.DISCOVERY_OPTION, isChecked);
                        mProgressDialog = UiUtils.hideProgressBar(mProgressDialog);
                        SPHandler.setBool(mContext,
                                Constants.SHOW_DISCOVERY_OPTION, true);
                        TrulyMadlyTrackEvent.trackEvent(mContext, TrulyMadlyActivities.settings,
                                TrulyMadlyEventTypes.discovery_option_toggled_delete, 0, TrulyMadlyEventStatus.success, null, true);

                        SPHandler.setBool(mContext, ConstantsSP.SHARED_KEYS_PROFILE_VISIBILITY_MESSAGE, true);
                        DidntLikeItHereFragment.this.getActivity().finish();
                    }

                    @Override
                    public void onRequestFailure(Exception exception) {
                        AlertsHandler.showNetworkError(DidntLikeItHereFragment.this.getActivity(), exception);
                    }
                };
                mProgressDialog = UiUtils
                        .showProgressBar(mContext, mProgressDialog);
                Utility.sendDiscoveryOptions(isChecked, mContext, okHttpResponseHandler);

            }
        });

        boolean isShowingPV = SPHandler.getBool(mContext,
                Constants.SHOW_DISCOVERY_OPTION);
        boolean isFemale = Utility.stringCompare(SPHandler.getString(mContext, ConstantsSP.SHARED_KEYS_USER_GENDER), "f");
        boolean showPV = !isShowingPV && isFemale;

        if (!showPV) {
            mDidntLikeFragment.findViewById(R.id.profile_visibility_desc).setVisibility(View.GONE);
            mDidntLikeFragment.findViewById(R.id.profile_visibility_button).setVisibility(View.GONE);
        }

        mSelectedReasons = new ArrayList<>();
        return mDidntLikeFragment;
    }

    private String generateSubReason() {
        if (mOthersReason != null) {
            mSelectedReasons.remove(mOthersReason);
        }
        mOthersReason = mOthersET.getText().toString();
        if (Utility.isSet(mOthersReason))
            mSelectedReasons.add(mOthersReason);
        String subreasons = android.text.TextUtils.join(",", mSelectedReasons);
        if (Utility.isSet(subreasons))
            return subreasons;

        return null;
    }

    @Override
    public String getSubReason() {
        return generateSubReason();
    }

    @Override
    @OnCheckedChanged({R.id.reason_cb_1, R.id.reason_cb_2, R.id.reason_cb_3, R.id.reason_cb_4,
            R.id.reason_cb_5, R.id.reason_cb_6, R.id.reason_cb_7, R.id.reason_cb_8})
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        String text = buttonView.getText().toString();
        if (isChecked)
            mSelectedReasons.add(text);
        else
            mSelectedReasons.remove(text);

        if (buttonView.getId() == R.id.reason_cb_4) {

//            boolean isSelectMember = TMSelectHandler.isSelectMember(mContext);
//            final Map<String, String> eventInfoSelect = new HashMap<>();
//            eventInfoSelect.put("is_select_member", String.valueOf(isSelectMember));
//            eventInfoSelect.put("source", "delete_profile");
//            TrulyMadlyTrackEvent.trackEvent(mContext, select, select_serious_intent_nudge, 0,
//                    TrulyMadlyEventStatus.view,
//                    eventInfoSelect, true);
//            AlertsHandler.showSimpleSelectDialog(mContext, R.string.select_nudge_text_delete_accnt,
//                    R.string.learn_more, R.string.no_thanks_caps, new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            switch (view.getId()) {
//                                case R.id.positive_button_tv:
//                                    TrulyMadlyTrackEvent.trackEvent(mContext, select, select_serious_intent_nudge, 0,
//                                            TrulyMadlyEventStatus.clicked_yes,
//                                            eventInfoSelect, true);
//                                    ActivityHandler.startTMSelectActivity(mContext, select_serious_intent_nudge + ":delete_profile", false);
//                                    DidntLikeItHereFragment.this.getActivity().finish();
//                                    break;
//
//                                case R.id.negative_button_tv:
//                                    TrulyMadlyTrackEvent.trackEvent(mContext, select, select_serious_intent_nudge, 0,
//                                            TrulyMadlyEventStatus.clicked_no,
//                                            eventInfoSelect, true);
//                                    break;
//                            }
//                        }
//                    });
        }
    }

}
