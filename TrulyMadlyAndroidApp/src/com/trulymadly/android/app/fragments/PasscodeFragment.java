package com.trulymadly.android.app.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventStatus;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.sqlite.RFHandler;
import com.trulymadly.android.app.utility.Utility;

import me.philio.pinentry.PinEntryView;

/**
 * A simple {@link Fragment} subclass. Activities that contain this fragment
 * must implement the {@link PasscodeFragment.OnFragmentInteractionListener}
 * interface to handle interaction events. Use the
 * {@link PasscodeFragment#newInstance} factory method to create an instance of
 * this fragment.
 *
 */
public class PasscodeFragment extends Fragment {

	public static final String PASSCODE_PARAM = "PASSCODE";
	private static final String MODE_PARAM = "MODE";
	private static final String HEADER_TEXT_PARAM = "HEADER_TEXT";
	private static final String PASSCODE_KEY_PARAM = "PASSCODE_KEY";
	private static final int PASSCODE_MODE_FIRST = 1;
	private static final int PASSCODE_MODE_NORMAL = 2;
	private final Handler mHandler;
	private String mMyId;
	private OnPasscodeEnteredInterface mListener;
	private TextView mHeaderTextView;
	private TextView mForgotTextView;
	private PinEntryView mPinEntryView;
	private Context mContext;
	private int mMode = -1;
	private String mPasscodeKey, mPasscode, mHeaderText, mTryAgainMessage;
	private int mPasscodeColor, mPasscodeWrongColor, mPasscodeCorrectColor;
	private String mCurrentPasscode;


	public PasscodeFragment() {
		// Required empty public constructor
		mHandler = new Handler();
	}

	public static PasscodeFragment getInstance(String passcodeKey){
		PasscodeFragment passcodeFragment = new PasscodeFragment();
		Bundle bundle = new Bundle();
		bundle.putString(PASSCODE_KEY_PARAM, passcodeKey);
		passcodeFragment.setArguments(bundle);
		return passcodeFragment;
	}
	
	public static PasscodeFragment getInstance(int mode, String passcode, String headerText){
		PasscodeFragment passcodeFragment = new PasscodeFragment();
		Bundle bundle = new Bundle();
		bundle.putString(PASSCODE_PARAM, passcode);
		if(mode == -1)
			bundle.putInt(MODE_PARAM, PASSCODE_MODE_NORMAL);
		else
			bundle.putInt(MODE_PARAM, mode);
		
		if(headerText != null)
			bundle.putString(HEADER_TEXT_PARAM, headerText);
		passcodeFragment.setArguments(bundle);
		return passcodeFragment;
	}
	
	public static PasscodeFragment getInstance(String passcode, String headerText){
		PasscodeFragment passcodeFragment = new PasscodeFragment();
		Bundle bundle = new Bundle();
		bundle.putString(PASSCODE_PARAM, passcode);
		bundle.putInt(MODE_PARAM, PASSCODE_MODE_NORMAL);
		if(headerText != null)
			bundle.putString(HEADER_TEXT_PARAM, headerText);
		passcodeFragment.setArguments(bundle);
		return passcodeFragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = getActivity();
		if (getArguments() != null) {
			if(getArguments().containsKey(PASSCODE_KEY_PARAM)){
				mPasscodeKey = getArguments().getString(PASSCODE_KEY_PARAM);
				mPasscode = RFHandler.getString(mContext,
						Utility.getMyId(mContext),
						mPasscodeKey);
			}

			if (getArguments().containsKey(MODE_PARAM)) {
				mMode = getArguments().getInt(MODE_PARAM);
				if(getArguments().containsKey(PASSCODE_PARAM))
					mPasscode = getArguments().getString(PASSCODE_PARAM);
			}else if(mPasscode == null)
				mMode = PASSCODE_MODE_FIRST;
			else
				mMode = PASSCODE_MODE_NORMAL;
			
			if(getArguments().containsKey(HEADER_TEXT_PARAM)){
				mHeaderText = getArguments().getString(HEADER_TEXT_PARAM);
			}else{
				mHeaderText = mContext.getResources().getString(R.string.enter_password);
			}
		}
		mPasscodeColor = mContext.getResources().getColor(R.color.pass_text_grey);
		mPasscodeCorrectColor = mContext.getResources().getColor(R.color.pass_star_green);
		mPasscodeWrongColor = mContext.getResources().getColor(R.color.pass_star_red);
		mTryAgainMessage = mContext.getResources().getString(R.string.try_again);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View mPasscodeFragmentView = inflater.inflate(R.layout.fragment_passcode, container, false);
		
		mHeaderTextView = (TextView) mPasscodeFragmentView.findViewById(R.id.tv_enter_password);
		mHeaderTextView.setText(mHeaderText);
		
		mForgotTextView = (TextView) mPasscodeFragmentView.findViewById(R.id.tv_forgot_password);
		mForgotTextView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mListener.onPasscodeForgot();
			}
		});
		
		mPinEntryView = (PinEntryView) mPasscodeFragmentView.findViewById(R.id.conversations_passcode);
		mPinEntryView.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				if(s == null || s.length() != 4)
					return;
				checkAndVerifyPasscode(s.toString());
			}
		});
		
		mPinEntryView.getFocus();
		
		return mPasscodeFragmentView;
	}
	
	
	private void checkAndVerifyPasscode(final String currentPasscode){
		if(currentPasscode == null || currentPasscode.length() != 4)
			return;
		int mFailureDelay = 800;
		int mSuccessDelay = 400;

		switch(mMode){
			//Case where password is entered twice
			//1. To enter the new passcode
			//2. To re-enter the passcode for confirmation
			case PASSCODE_MODE_FIRST:
				if(mCurrentPasscode == null){
					mCurrentPasscode = currentPasscode;
					
					mHandler.postDelayed(new Runnable() {
						  @Override
						  public void run() {
							  mHeaderTextView.setText(R.string.confirm_password);
							  clearPasscodeFields();
						  }
					}, mSuccessDelay + 100);

				} else if (verifyPasscode(currentPasscode, mCurrentPasscode)) {
					//Tracking - registering feature enabling
					TrulyMadlyTrackEvent.trackEvent(mContext, TrulyMadlyActivities.conversation_list,
							TrulyMadlyEventTypes.conversation_passcode, 0,
							TrulyMadlyEventStatus.setup, null, true);
					setPasscodeFieldsColor(mPasscodeCorrectColor);
					RFHandler.insert(mContext, Utility.getMyId(mContext),
							  mPasscodeKey, currentPasscode);
					mHandler.postDelayed(new Runnable() {
						  @Override
						  public void run() {
							  mListener.onPasscodeSuccess();
						  }
						}, mSuccessDelay);
				}
				else{
					mHeaderTextView.setText(mTryAgainMessage);
					mHeaderTextView.setTextColor(mPasscodeWrongColor);
					setPasscodeFieldsColor(mPasscodeWrongColor);
					mHandler.postDelayed(new Runnable() {
						  @Override
						  public void run() {
							  clearPasscodeFields();
						  }
						}, mFailureDelay);
					
				}
				break;

			//Ask the user to enter and then confirm the passcode
			case PASSCODE_MODE_NORMAL:
				if(mListener != null){
					if (verifyPasscode(currentPasscode, mPasscode)) {
						setPasscodeFieldsColor(mPasscodeCorrectColor);
						mHandler.postDelayed(new Runnable() {
							  @Override
							  public void run() {
								  if (mListener != null)
									  mListener.onPasscodeSuccess();

							  }
							}, mSuccessDelay);
						
					}
					else{
						mHeaderTextView.setText(mTryAgainMessage);
						mHeaderTextView.setTextColor(mPasscodeWrongColor);
						mForgotTextView.setVisibility(View.VISIBLE);
						setPasscodeFieldsColor(mPasscodeWrongColor);
						mHandler.postDelayed(new Runnable() {
							  @Override
							  public void run() {
								  clearPasscodeFields();
							  }
							}, mFailureDelay);
						
					}
				}
				break;
		}
	}
	
	//Verifies if the current passcode entered by the user matches the given passcode or not
	private boolean verifyPasscode(String currentPasscode, String passcode){
        return currentPasscode.equals(passcode);
    }

    private void clearPasscodeFields(){
		mPinEntryView.clearPasscode();
		setPasscodeFieldsColor(mPasscodeColor);
	}
	
	private void setPasscodeFieldsColor(int color){
		mPinEntryView.setTextColor(color);
	}
	

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mListener = (OnPasscodeEnteredInterface) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnPasscodeEnteredInterface");
		}
	}
	
	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}

	/**
	 * This interface must be implemented by activities that contain this
	 * fragment to allow an interaction in this fragment to be communicated to
	 * the activity and potentially other fragments contained in that activity.
	 */
	public interface OnPasscodeEnteredInterface {
        void onPasscodeSuccess();

		@SuppressWarnings("EmptyMethod")
		void onPasscodeFailure();

        void onPasscodeForgot();
    }

}
