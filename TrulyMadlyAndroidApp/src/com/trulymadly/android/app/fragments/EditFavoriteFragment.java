package com.trulymadly.android.app.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.adapter.FavoriteAutoCompleteAdapter;
import com.trulymadly.android.app.adapter.MyFavoritesGridViewAdapter;
import com.trulymadly.android.app.bus.AddFavoriteOnServerEvent;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.listener.CustomOkHttpResponseHandler;
import com.trulymadly.android.app.listener.FavoriteItemClickInterface;
import com.trulymadly.android.app.modal.FavoriteDataModal;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.FavoriteUtils;
import com.trulymadly.android.app.utility.OkHttpHandler;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities.favorites;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventStatus.my_favorite;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventStatus.suggestion;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes.deselect;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes.select;
import static com.trulymadly.android.app.utility.FavoriteUtils.TAB_CATEGORY_KEYS;

/**
 * Created by udbhav on 12/07/16.
 */
public class EditFavoriteFragment extends Fragment {
    public static final String ARG_POSITION = "ARG_POSITION";
    public static final String ARG_TAB_CATEGORY_KEY = "ARG_TAB_CATEGORY_KEY";
    public static final String ARG_TAB_FAVORITE_MODAL_LIST = "ARG_TAB_FAVORITE_MODAL_LIST";
    public static final String ARG_FAVORITE_ACTIVITY_SOURCE = "ARG_FAVORITE_ACTIVITY_SOURCE";
    private final int hintTextsResIds[] = new int[]{R.string.favorites_etc, R.string.music_etc, R.string.books_etc, R.string.cuisines_etc, R.string.sports_etc};
    @BindView(R.id.edit_favorites_fragment_layout_autocomplete_tv)
    AutoCompleteTextView editFavoriteAutoCompleteTextView;
    @BindView(R.id.edit_favorites_fragment_layout_my_favorites_header_tv)
    TextView myFavoritesHeader;
    @BindView(R.id.edit_favorites_fragment_layout_show_less_tv)
    TextView myFavoritesShowLess;
    @BindView(R.id.edit_favorites_fragment_layout_my_favorites_gridview)
    RecyclerView myFavoritesGridView;
    @BindView(R.id.edit_favorites_fragment_layout_my_favorites_container)
    View myFavoritesContainer;
    @BindView(R.id.favorites_suggestion_gridview)
    RecyclerView suggestionGridView;
    @BindView(R.id.favorites_suggestion_container)
    View suggestionContainer;
    @BindView(R.id.favorites_suggestion_header_tv)
    TextView suggestionHeader;
    @BindView(R.id.favorites_no_suggestion_tv)
    TextView noSuggestionTextView;
    @BindView(R.id.favorites_suggestion_progress_bar)
    View suggestionProgressBar;
    private int mPosition;
    private Unbinder unbinder;
    private String mTabCategoryKey;
    private ArrayList<FavoriteDataModal> mFavoritesList, mSuggestionsList, mFavoritesShortenedList;
    private boolean mFetchSuggestionInProgress = false;
    private FavoriteItemClickInterface favoriteItemClickInterface;
    private boolean showShortenedListIfAvailable = true;
    private FavoriteAutoCompleteAdapter autoCompleteAdapter;
    private boolean isDestroyed = false;
    private MyFavoritesGridViewAdapter myFavoritesGridViewAdapter = null;
    private boolean fragmentOnCreated, fragmentVisible;
    private String mFavoriteActivitySource;

    public static EditFavoriteFragment newInstance(int position, String tabCategoryKey, ArrayList<FavoriteDataModal> favoriteDataModals, String source) {
        Bundle args = new Bundle();
        args.putInt(ARG_POSITION, position);
        args.putString(ARG_TAB_CATEGORY_KEY, tabCategoryKey);
        args.putParcelableArrayList(ARG_TAB_FAVORITE_MODAL_LIST, favoriteDataModals);
        args.putString(ARG_FAVORITE_ACTIVITY_SOURCE, source);
        EditFavoriteFragment fragment = new EditFavoriteFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        mPosition = args.getInt(ARG_POSITION);
        mTabCategoryKey = args.getString(ARG_TAB_CATEGORY_KEY);
        mFavoriteActivitySource = args.getString(ARG_FAVORITE_ACTIVITY_SOURCE);
        mFavoritesList = args.getParcelableArrayList(ARG_TAB_FAVORITE_MODAL_LIST);
        createShortenedFavoritesList();
        favoriteItemClickInterface = new FavoriteItemClickInterface() {
            @Override
            public void showMoreFavorites() {
                showShortenedListIfAvailable = false;
                myFavoritesShowLess.setVisibility(View.VISIBLE);
                Map<String, String> map = new HashMap<>();
                map.put("fav_count", mFavoritesList.size() + "");
                map.put("tab", mTabCategoryKey);
                map.put("source", mFavoriteActivitySource);
                TrulyMadlyTrackEvent.trackEvent(getContext(), favorites, TrulyMadlyEvent.TrulyMadlyEventTypes.show_more, 0, null, map, true);
                showMyFavouriteList();
            }

            @Override
            public void onFavoriteClicked(FavoriteDataModal favoriteDataModal, boolean isSelected) {
                Map<String, String> map = new HashMap<>();
                map.put("tab", mTabCategoryKey);
                map.put("source", mFavoriteActivitySource);
                TrulyMadlyTrackEvent.trackEvent(getContext(), favorites, isSelected ? select : deselect, 0, favoriteDataModal.isSuggestion() ? suggestion : my_favorite, map, true);
                addFavoriteToSaveList(favoriteDataModal, isSelected);
            }
        };
        autoCompleteAdapter = new FavoriteAutoCompleteAdapter(getContext(), TAB_CATEGORY_KEYS[mPosition], mFavoritesList);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.edit_favorites_fragment_layout, container, false);
        unbinder = ButterKnife.bind(this, view);
        isDestroyed = false;

        //AUTOCOMPLETE RELATED
        editFavoriteAutoCompleteTextView.setHint(hintTextsResIds[mPosition]);
        editFavoriteAutoCompleteTextView.setThreshold(3);
        editFavoriteAutoCompleteTextView.setAdapter(autoCompleteAdapter);
        editFavoriteAutoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                onFavoriteSelectedFromAutoCompleter((FavoriteDataModal) parent.getAdapter().getItem(position));
            }
        });

        myFavoritesShowLess.setVisibility(View.GONE);
        showMyFavouriteList();

        noSuggestionTextView.setVisibility(View.GONE);
        suggestionGridView.setVisibility(View.GONE);
        suggestionProgressBar.setVisibility(View.GONE);
        return view;
    }

    private void loadSuggestionList() {
        if (mSuggestionsList == null) {
            fetchSuggestion();
        } else {
            showSuggestionList();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isResumed()) {
            // only at fragment screen is resumed
            // fragmentResume=true;
            fragmentVisible = false;
            fragmentOnCreated = true;
            loadSuggestionList();
        } else if (isVisibleToUser) {
            // only at fragment onCreated
            // fragmentResume=false;
            fragmentVisible = true;
            fragmentOnCreated = true;
        } else if (!isVisibleToUser && fragmentOnCreated) {
            // only when you go out of fragment screen
            fragmentVisible = false;
            // fragmentResume=false;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (fragmentVisible) {
            loadSuggestionList();
        }
    }

    private void showMyFavouriteList() {
        if (showShortenedListIfAvailable && (mFavoritesShortenedList == null || mFavoritesShortenedList.size() == 0)) {
            showShortenedListIfAvailable = false;
        }
        // MY FAVOURTIES RELATED
        int len = 0;
        if (mFavoritesList != null && mFavoritesList.size() > 0) {
            len = mFavoritesList.size();
        }
        if (len == 0) {
            myFavoritesContainer.setVisibility(View.GONE);
        } else {
            myFavoritesContainer.setVisibility(View.VISIBLE);
            myFavoritesHeader.setText(getResources().getString(R.string.your_favorites) + " (" + mFavoritesList.size() + ")");
            myFavoritesGridView.setVisibility(View.VISIBLE);
        }

        ViewGroup.LayoutParams params = myFavoritesGridView.getLayoutParams();
        int rows = showShortenedListIfAvailable ? 2 : mFavoritesList.size() / 4 + (mFavoritesList.size() % 4 == 0 ? 0 : 1);
        params.height = UiUtils.dpToPx(120 * rows);
        myFavoritesGridView.setLayoutParams(params);

        myFavoritesGridView.setLayoutManager(new GridLayoutManager(getContext(), 4));
        myFavoritesGridViewAdapter = new MyFavoritesGridViewAdapter(getContext(), showShortenedListIfAvailable ? mFavoritesShortenedList : mFavoritesList, favoriteItemClickInterface);
        myFavoritesGridView.setAdapter(myFavoritesGridViewAdapter);
        myFavoritesGridView.setNestedScrollingEnabled(false);

    }

    private void showSuggestionList() {
        mFetchSuggestionInProgress = false;
        if (isDestroyed) {
            return;
        }
        suggestionProgressBar.setVisibility(View.GONE);
        if (mSuggestionsList == null || mSuggestionsList.size() == 0) {
            noSuggestionTextView.setVisibility(View.VISIBLE);
            suggestionGridView.setVisibility(View.GONE);
            return;
        }

        noSuggestionTextView.setVisibility(View.GONE);
        suggestionGridView.setVisibility(View.VISIBLE);
        suggestionGridView.setNestedScrollingEnabled(false);


        ViewGroup.LayoutParams params = suggestionGridView.getLayoutParams();
        int rows = mSuggestionsList.size() / 4 + (mSuggestionsList.size() % 4 == 0 ? 0 : 1);
        params.height = UiUtils.dpToPx(120 * rows);
        suggestionGridView.setLayoutParams(params);

        suggestionGridView.setLayoutManager(new GridLayoutManager(getActivity(), 4));
        suggestionGridView.setAdapter(new MyFavoritesGridViewAdapter(getActivity(), mSuggestionsList, favoriteItemClickInterface));


    }

    private void fetchSuggestion() {
        if (mFetchSuggestionInProgress) {
            return;
        }
        suggestionProgressBar.setVisibility(View.VISIBLE);

        mSuggestionsList = new ArrayList<>();

        HashMap<String, String> params = new HashMap<>();
        params.put("action", "suggestions");
        params.put("categories", mTabCategoryKey);
        params.put("source", mFavoriteActivitySource);
        CustomOkHttpResponseHandler responseHandler = new CustomOkHttpResponseHandler(getContext(), TrulyMadlyEvent.TrulyMadlyActivities.favorites, TrulyMadlyEvent.TrulyMadlyEventTypes.favorites_suggestion_server_call) {
            @Override
            public void onRequestSuccess(JSONObject responseJSON) {
                mFetchSuggestionInProgress = false;
                if (responseJSON.optJSONObject("suggestions") != null && responseJSON.optJSONObject("suggestions").optJSONArray(mTabCategoryKey) != null) {
                    JSONArray suggestionsJsonArray = responseJSON.optJSONObject("suggestions").optJSONArray(mTabCategoryKey);
                    if (suggestionsJsonArray != null && suggestionsJsonArray.length() > 0) {
                        int len = suggestionsJsonArray.length();
                        for (int i = 0; i < len; i++) {
                            JSONObject singleFavJson = suggestionsJsonArray.optJSONObject(i);
                            if (singleFavJson != null) {
                                FavoriteDataModal favDataModal = new FavoriteDataModal(singleFavJson.optString("id"), singleFavJson.optString("name"), mTabCategoryKey, singleFavJson.optString("pic"), false, true, false, true, true);
                                if (!mSuggestionsList.contains(favDataModal)) {
                                    mSuggestionsList.add(favDataModal);
                                }
                            }
                        }
                    }
                }

                showSuggestionList();
            }

            @Override
            public void onRequestFailure(Exception exception) {
                mFetchSuggestionInProgress = false;
                showSuggestionList();
            }
        };

        OkHttpHandler.httpGet(getContext(), ConstantsUrls.get_favorites_url(), params, responseHandler);

        mFetchSuggestionInProgress = true;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        isDestroyed = true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        System.gc();
    }

    private void clearData() {
        if (mFavoritesList != null) {
            mFavoritesList.clear();
        }
        mFavoritesList = null;
        if (mSuggestionsList != null) {
            mSuggestionsList.clear();
        }
        mSuggestionsList = null;

    }

    public void onFavoriteSelectedFromAutoCompleter(FavoriteDataModal favoriteDataModal) {
        editFavoriteAutoCompleteTextView.dismissDropDown();
        editFavoriteAutoCompleteTextView.setText("");
        if (mFavoritesList.contains(favoriteDataModal)) {
            AlertsHandler.showMessage(getActivity(), getResources().getString(R.string.favorites_already_exists));
        } else {
            Map<String, String> map = new HashMap<>();
            map.put("tab", favoriteDataModal.getCategory());
            map.put("source", mFavoriteActivitySource);
            TrulyMadlyTrackEvent.trackEvent(getContext(), favorites, select, 0, TrulyMadlyEvent.TrulyMadlyEventStatus.autocomplete, map, true);
            favoriteDataModal.setSelected(true);
            mFavoritesList.add(0, favoriteDataModal);
            createShortenedFavoritesList();
            showMyFavouriteList();
            addFavoriteToSaveList(favoriteDataModal, true);
        }
        UiUtils.hideKeyBoard(getContext());
    }

    private void addFavoriteToSaveList(FavoriteDataModal favoriteDataModal, boolean isAdded) {
        Utility.fireBusEvent(getContext(), true, new AddFavoriteOnServerEvent(favoriteDataModal, isAdded));
    }

    private void createShortenedFavoritesList() {
        mFavoritesShortenedList = new ArrayList<>();
        if (mFavoritesList != null && mFavoritesList.size() > 8) {
            mFavoritesShortenedList = new ArrayList<>(mFavoritesList.subList(0, 7));
            mFavoritesShortenedList.add(FavoriteUtils.createSeeMoreItem());
        }
    }

    public void destroy() {
        mFavoritesList = null;
        myFavoritesGridViewAdapter = null;
    }

    @OnClick(R.id.edit_favorites_fragment_layout_show_less_tv)
    void onClickShowLess() {
        myFavoritesShowLess.setVisibility(View.GONE);
        showShortenedListIfAvailable = true;
        showMyFavouriteList();
        Map<String, String> map = new HashMap<>();
        map.put("fav_count", mFavoritesList.size() + "");
        map.put("tab", mTabCategoryKey);
        map.put("source", mFavoriteActivitySource);
        TrulyMadlyTrackEvent.trackEvent(getContext(), favorites, TrulyMadlyEvent.TrulyMadlyEventTypes.show_less, 0, null, map, true);
    }
}
