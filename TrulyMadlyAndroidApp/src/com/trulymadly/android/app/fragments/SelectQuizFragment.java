package com.trulymadly.android.app.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.adapter.SelectQuizQuesAdapter;
import com.trulymadly.android.app.custom.HorizontalPagerIndicator;
import com.trulymadly.android.app.modal.SelectQuizCompareModal;
import com.trulymadly.android.app.modal.SelectQuizModal;
import com.trulymadly.android.app.sqlite.CachingDBHandler;
import com.trulymadly.android.app.sqlite.MatchesDbHandler;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.TMSelectHandler;
import com.trulymadly.android.app.utility.Utility;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static butterknife.ButterKnife.findById;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SelectQuizFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SelectQuizFragment extends Fragment {

    public static final String PARAM_QUIZ_STARTED_DIRECTLY = "quiz_started_directly";

    @BindView(R.id.pager_indicator)
    HorizontalPagerIndicator mPagerIndicator;
    @BindView(R.id.ques_completed_tv)
    TextView mQuestionsCompletedTV;
    @BindView(R.id.action_container)
    View mActionContainer;
    @BindView(R.id.quiz_container)
    View mQuizContainer;

    @BindView(R.id.loader_container)
    View mLoaderContainer;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;
    @BindView(R.id.progress_description)
    TextView mProgressDescriptionTV;
    @BindView(R.id.retry_container)
    View mRetryContainer;
    @BindView(R.id.retry_message_tv)
    TextView mRetryMessageTV;

    @BindView(R.id.hide_fab)
    View mHideFAB;
    @BindView(R.id.like_fab)
    View mLikeFAB;
    @BindView(R.id.questions_pager)
    ViewPager mQuestionsPager;
    //Last Page
    @BindView(R.id.last_screen_stub)
    ViewStub mSelectQuizLastScreenStub;
    @BindView(R.id.first_screen_stub)
    ViewStub mSelectQuizFirstScreenStub;
    private View mSelectQuizLastScreen;
    private ProgressBar mSubmitProgressbar;
    private View mFirstScreen;


    private String mQuestionStatus;
    private Animation mScaleUpAnimation, mScaleDownAnimation;
    private Animation mScaleUpAnimationDur0, mScaleDownAnimationDur0;
    private Animation.AnimationListener mScaleUpListener;
    private SelectQuizQuesAdapter mSelectQuizQuesAdapter;
    private Unbinder mUnbinder;
    private TMSelectHandler.QuizHandler mQuizHandler;
    private SelectQuizModal mSelectQuizModal;
    private SelectQuizFragmentListener mSelectQuizFragmentListener;

    private boolean isQuizStartedDirectly;

    public SelectQuizFragment() {
        // Required empty public constructor
    }

    public static SelectQuizFragment newInstance(boolean isQuizStartedDirectly) {
        SelectQuizFragment selectQuizFragment = new SelectQuizFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(PARAM_QUIZ_STARTED_DIRECTLY, isQuizStartedDirectly);
        selectQuizFragment.setArguments(bundle);
        return selectQuizFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.isQuizStartedDirectly = getArguments().getBoolean(PARAM_QUIZ_STARTED_DIRECTLY, false);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_select_quiz, container, false);
        mUnbinder = ButterKnife.bind(this, view);

        mScaleDownAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.scale_down_from_20_pivot_bottom);
        mScaleDownAnimation.setFillAfter(true);
        mScaleDownAnimation.setDuration(300);

        mScaleUpAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.scale_up_by_50_pivot_bottom);
        mScaleUpAnimation.setFillAfter(true);
        mScaleUpAnimation.setDuration(300);
        mScaleUpListener = new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                onLikeHideClicked(mQuestionStatus);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        };
        mScaleUpAnimation.setAnimationListener(mScaleUpListener);
        mScaleUpAnimationDur0 = AnimationUtils.loadAnimation(getContext(), R.anim.scale_up_by_50_pivot_bottom);
        mScaleUpAnimationDur0.setFillAfter(true);
        mScaleDownAnimationDur0 = AnimationUtils.loadAnimation(getContext(), R.anim.scale_down_from_20_pivot_bottom);
        mScaleDownAnimationDur0.setFillAfter(true);

        mPagerIndicator.setOnSelectionChangedListener(new HorizontalPagerIndicator.OnSelectionChangedListener() {
            @Override
            public void onSelectionChanged(int selectedPosition, int completedBlocks, int totalBlocks) {
                mQuestionsCompletedTV.setText(String.format(getString(R.string.ques_completed_text),
                        String.valueOf(mPagerIndicator.getCurrentBlock() + 1),
                        String.valueOf(mPagerIndicator.getmTotalBlocks())));
                if (mSelectQuizQuesAdapter != null && completedBlocks < totalBlocks && completedBlocks == mSelectQuizQuesAdapter.getCount()) {
                    mSelectQuizQuesAdapter.incCount();
                    mQuestionsPager.setCurrentItem(selectedPosition);
                }
                //TODO : Button states
            }
        });

        mQuestionsPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                int lastSelected = mPagerIndicator.getCurrentBlock();
                if (lastSelected == position + 1) {
                    mPagerIndicator.moveBack();
                } else if (lastSelected == position - 1) {
                    mPagerIndicator.moveNext(false);
                }

                toggleActionButtons(mSelectQuizModal.getAnswerOfQuestion(position));
                prefetchNext3(position);
            }
        });

        if (getmQuizHandler().isQuizAlreadyPlayed()) {
            getmQuizHandler().fetchQuizData();
            toggleView(true, false, null);
        } else {
            toggleFirstScreen(true);
        }

        return view;
    }

    @OnClick({R.id.like_fab, R.id.hide_fab, R.id.retry_button})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.like_fab:
            case R.id.hide_fab:
                boolean animate = true;
                String newSelectedOptionId = (String) view.getTag();
                if (Utility.stringCompare(mQuestionStatus, newSelectedOptionId)) {
                    animate = false;
                } else {
                    if (Utility.isSet(mQuestionStatus)) {
                        if (mQuestionStatus.equals(mLikeFAB.getTag())) {
                            mLikeFAB.setBackgroundResource(R.drawable.circle_pink);
                            mLikeFAB.startAnimation(mScaleDownAnimationDur0);
                        } else {
                            mHideFAB.setBackgroundResource(R.drawable.circle_blue);
                            mHideFAB.startAnimation(mScaleDownAnimationDur0);
                        }
                    }
                    mQuestionStatus = newSelectedOptionId;
                }

                if (animate) {
                    if (newSelectedOptionId.equals(mLikeFAB.getTag())) {
                        mLikeFAB.setBackgroundResource(R.drawable.circle_pink_with_border);
                        mLikeFAB.startAnimation(mScaleUpAnimation);
                    } else {
                        mHideFAB.setBackgroundResource(R.drawable.circle_blue_with_border);
                        mHideFAB.startAnimation(mScaleUpAnimation);
                    }
                } else {
                    onLikeHideClicked((String) view.getTag());
                }
                break;

            case R.id.retry_button:
                getmQuizHandler().fetchQuizData();
                toggleView(true, false, null);
                break;

            case R.id.lets_go_button:
                mSubmitProgressbar.setVisibility(View.VISIBLE);
                getmQuizHandler().submitQuizData(mSelectQuizModal);
                break;

            case R.id.im_ready_button:
                mFirstScreen.setVisibility(View.GONE);
                getmQuizHandler().fetchQuizData();
                toggleView(true, false, null);
                break;
        }
    }

    private void onLikeHideClicked(String selectedOptionId) {
        checkNotNull(mSelectQuizModal.getSelectQuizQuestionModal(mPagerIndicator.getCurrentBlock())).setmSelectedOptionId(selectedOptionId);
//        if (isLiked) {
//            mSelectQuizModal.getSelectQuizQuestionModal(mPagerIndicator.getCurrentBlock()).setmResponse(1);
//        } else {
//            mSelectQuizModal.getSelectQuizQuestionModal(mPagerIndicator.getCurrentBlock()).setmResponse(0);
//        }
        mPagerIndicator.moveNext(true);
        loadQuiz(false);
    }

    private void toggleActionButtons(String newQuestionStatus) {
        toggleHideButton(Utility.stringCompare((String) mHideFAB.getTag(), mQuestionStatus),
                Utility.stringCompare((String) mHideFAB.getTag(), newQuestionStatus), false);
        toggleLikeButton(Utility.stringCompare((String) mLikeFAB.getTag(), mQuestionStatus),
                Utility.stringCompare((String) mLikeFAB.getTag(), newQuestionStatus), false);
        mQuestionStatus = newQuestionStatus;
    }

    private void toggleHideButton(boolean previouslySelected, boolean selected, boolean animate) {
        if (previouslySelected != selected) {
            mHideFAB.setBackgroundResource((selected) ? R.drawable.circle_blue_with_border : R.drawable.circle_blue);
            mHideFAB.startAnimation((selected) ?
                    ((!animate) ? mScaleUpAnimationDur0 : mScaleUpAnimation) :
                    ((!animate) ? mScaleDownAnimationDur0 : mScaleDownAnimation));
        }
    }

    private void toggleLikeButton(boolean previouslySelected, boolean selected, boolean animate) {
        if (previouslySelected != selected) {
            mLikeFAB.setBackgroundResource((selected) ? R.drawable.circle_pink_with_border : R.drawable.circle_pink);
            mLikeFAB.startAnimation((selected) ?
                    ((!animate) ? mScaleUpAnimationDur0 : mScaleUpAnimation) :
                    ((!animate) ? mScaleDownAnimationDur0 : mScaleDownAnimation));
        }
    }

    private void toggleView(boolean isInProgress, boolean isFailed, String errorMessage) {
        if (mFirstScreen != null) {
            mFirstScreen.setVisibility(View.GONE);
        }

        if (isInProgress) {
            mProgressDescriptionTV.setVisibility((isQuizStartedDirectly) ? View.VISIBLE : View.GONE);
            mProgressBar.setVisibility(View.VISIBLE);
            mRetryContainer.setVisibility(View.GONE);
            mQuizContainer.setVisibility(View.GONE);
            mLoaderContainer.setVisibility(View.VISIBLE);
        } else if (isFailed) {
            mProgressDescriptionTV.setVisibility(View.GONE);
            mProgressBar.setVisibility(View.GONE);
            mRetryContainer.setVisibility(View.VISIBLE);
            mRetryMessageTV.setText(errorMessage);
            mQuizContainer.setVisibility(View.GONE);
            mLoaderContainer.setVisibility(View.VISIBLE);
        } else {
            mProgressDescriptionTV.setVisibility(View.GONE);
            mProgressBar.setVisibility(View.VISIBLE);
            mRetryContainer.setVisibility(View.GONE);
            mQuizContainer.setVisibility(View.VISIBLE);
            mLoaderContainer.setVisibility(View.GONE);
            loadQuiz(true);
        }
    }

    private void prefetchNext3(int currentPosition) {
        if (mSelectQuizModal == null) {
            return;
        }

        ArrayList<SelectQuizModal.SelectQuizQuestionModal> selectQuizQuestionModals =
                mSelectQuizModal.getSelectQuizQuestionModals();
        if (selectQuizQuestionModals != null) {
            for (int i = currentPosition + 1; i < selectQuizQuestionModals.size() && i <= currentPosition + 3; i++) {
                String url = selectQuizQuestionModals.get(i).getmImageUrl();
                Picasso.with(getContext()).load(url).fetch();
            }
        }
    }

    private void toggleFirstScreen(boolean show) {
        if (show) {
            mLoaderContainer.setVisibility(View.GONE);
            if (mFirstScreen == null) {
                mFirstScreen = mSelectQuizFirstScreenStub.inflate();
                Button imReady = findById(mFirstScreen, R.id.im_ready_button);
                imReady.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        SelectQuizFragment.this.onClick(view);
                    }
                });
            }
            mFirstScreen.setVisibility(View.VISIBLE);
        }
    }

    private void toggleLastScreen(boolean show) {
        if (show) {
            if (mSelectQuizLastScreen == null) {
                mSelectQuizLastScreen = mSelectQuizLastScreenStub.inflate();
                mSubmitProgressbar = findById(mSelectQuizLastScreen, R.id.submit_progressbar);
                Button letsGo = findById(mSelectQuizLastScreen, R.id.lets_go_button);
                letsGo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        SelectQuizFragment.this.onClick(view);
                    }
                });
            }
            mQuestionsPager.setVisibility(View.GONE);
            mQuestionsCompletedTV.setVisibility(View.GONE);
            mActionContainer.setVisibility(View.GONE);
            mSelectQuizLastScreen.setVisibility(View.VISIBLE);
        }
    }

    private void loadQuiz(boolean reset) {
        if (reset) {
            mPagerIndicator.reset(mSelectQuizModal.getTotalQuestions(), mSelectQuizModal.getmTotalCompletedQuestions());

            if (mSelectQuizQuesAdapter == null) {
                mSelectQuizQuesAdapter = new SelectQuizQuesAdapter(getContext());
                mQuestionsPager.setAdapter(mSelectQuizQuesAdapter);
            }
            mSelectQuizQuesAdapter.changeList(mSelectQuizModal.getSelectQuizQuestionModals(), mPagerIndicator.getCurrentBlock() + 1);

            toggleActionButtons(mSelectQuizModal.getAnswerOfQuestion(0));
        }
        mLikeFAB.setTag(mSelectQuizModal.getOptionIdOfQuestion(mPagerIndicator.getCurrentBlock(), 1));
        mHideFAB.setTag(mSelectQuizModal.getOptionIdOfQuestion(mPagerIndicator.getCurrentBlock(), 0));
        mQuestionsPager.setCurrentItem(mPagerIndicator.getCurrentBlock());
        prefetchNext3(mQuestionsPager.getCurrentItem());
        if (Utility.isSet(mQuestionStatus) &&
                mPagerIndicator.getCurrentBlock() == mPagerIndicator.getmTotalBlocks() - 1) {
            toggleLastScreen(true);
        }
    }

    public boolean onBackPressed() {
        AlertsHandler.showSimpleSelectDialog(getContext(), R.string.select_nudge_text_on_quiz,
                R.string.continue_quiz, R.string.quit_anyway, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        switch (view.getId()) {
                            case R.id.positive_button_tv:
                                break;

                            case R.id.negative_button_tv:
                                mSelectQuizFragmentListener.closeFragment(true);
                                break;
                        }
                    }
                });

        return true;
    }

    private void saveData() {
        getmQuizHandler().saveQuizData(mSelectQuizModal);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        saveData();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        getmQuizHandler().unregister();
        mUnbinder.unbind();
        super.onDestroyView();
    }

    private TMSelectHandler.QuizHandler getmQuizHandler() {
        if (mQuizHandler == null) {
            mQuizHandler = new TMSelectHandler.QuizHandler(getContext(), new TMSelectHandler.QuizHandler.QuizHandlerListener() {
                @Override
                public void onQuizFetch(boolean success, SelectQuizModal selectQuizModal, String errorMessage) {
                    if (success) {
                        mSelectQuizModal = selectQuizModal;
                        toggleView(false, false, null);
                    } else {
                        toggleView(false, true, errorMessage);
                    }
                }

                @Override
                public void onQuizSubmit(boolean success, String error) {
                    mSubmitProgressbar.setVisibility(View.GONE);
                    if (success) {
                        MatchesDbHandler.clearMatchesCache(getContext());
                        CachingDBHandler.clearAllData(getContext());
                        TMSelectHandler.setSelectQuizPlayed(getContext(), true);
                        if (mSelectQuizFragmentListener != null) {
                            mSelectQuizFragmentListener.closeFragment(false);
                        }
                    } else {
                        AlertsHandler.showMessage(getActivity(), error);
                    }
                }

                @Override
                public void onQuizResult(boolean success, SelectQuizCompareModal selectQuizCompareModal, String errorMessage) {

                }
            });
        }

        return mQuizHandler;
    }

    public void registerListener(SelectQuizFragmentListener selectQuizFragmentListener) {
        this.mSelectQuizFragmentListener = selectQuizFragmentListener;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface SelectQuizFragmentListener {
        void closeFragment(boolean minimizeApp);
    }
}
