package com.trulymadly.android.app.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.otto.Subscribe;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.adapter.BuySparkDialogViewPagerAdapter;
import com.trulymadly.android.app.adapter.SparkPackagesAdapter;
import com.trulymadly.android.app.billing.GooglePlayBillingHandler;
import com.trulymadly.android.app.bus.BusProvider;
import com.trulymadly.android.app.bus.ShowRestorePurchasesEvent;
import com.trulymadly.android.app.listener.ActivityEventsListener;
import com.trulymadly.android.app.listener.TrackingBuySparkEventListener;
import com.trulymadly.android.app.modal.ImageAndText;
import com.trulymadly.android.app.modal.SparkPackageModal;
import com.trulymadly.android.app.utility.ABHandler;
import com.trulymadly.android.app.utility.CirclePageIndicator;
import com.trulymadly.android.app.utility.SparksHandler;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class BuySparkDialogFragment extends Fragment implements ActivityEventsListener {

    public static final String SPARKS_MESSAGES_LIST_KEY = "sparks_messages";
    public static final String SPARKS_RECEIVER_ID_KEY = "sparks_receiver_id";
    public static final String SPARKS_FROM_INTRO = "sparks_from_intro";
    public static final String SPARKS_FROM_SCENES = "sparks_from_scnes";
    public static final String SPARKS_SCENES_ID = "sparks_scenes_id";
    public static final String SPARKS_LIKES_KEY = "sparks_likes_id";
    public static final String SPARKS_HIDES_KEY = "sparks_hides_id";
    public static final String SPARKS_SPARKS_KEY = "sparks_sparks_id";
    public static final String SPARKS_FAVORITES_VIEWED_KEY = "sparks_favorites_viewed";
    @BindView(R.id.spark_packages_rv)
    RecyclerView mSparkPackagesRV;
    @BindView(R.id.packages_container)
    View mPackagesContainer;
    @BindView(R.id.loader_container)
    View mLoaderContainer;
    @BindView(R.id.progressbar)
    ProgressBar mProgressbar;
    @BindView(R.id.determinate_progressbar)
    ProgressBar mDeterminateProgressbar;
    @BindView(R.id.buy_spark_viewpager)
    ViewPager mViewPager;
    @BindView(R.id.retry_button)
    Button mRetryButton;
    @BindView(R.id.retry_container)
    View mRetryContainer;
    @BindView(R.id.retry_message_tv)
    TextView mRetryMessageTV;
    @BindView(R.id.page_indicator)
    CirclePageIndicator mPageIndicator;
    @BindView(R.id.buy_spark_dialog)
    View mBuySparkDialog;
    @BindView(R.id.buy_spark_popup_iv)
    ImageView mBuySparkPopupIV;
    @BindView(R.id.buy_spark_popup_iv_container)
    View mBuySparkPopupIVContainer;
    @BindView(R.id.loading_tv)
    View mLoadingTV;
    @BindView(R.id.restore_purchases_view)
    View mRestorePurchasesView;
    @BindView(R.id.match_guarantee_tnc_tv)
    TextView mMatchGuaranteeTncTv;
    @BindView(R.id.match_guarantee_tnc_container)
    View mMatchGuaranteeTncContainerView;

    private Context mContext;
    //    private BuySparkEventListener mBuySparkEventListener;
    private TrackingBuySparkEventListener mTrackingBuySparkEventListener;
    private SparksHandler.PackagesHandler mPackagesHandler;

    private ArrayList<String> mDescStrings;
    private ArrayList<Integer> mDescImages;
    private ArrayList<ImageAndText> mImageAndTexts;

    private Timer mTimer;
    private int mCurrentPage = 0;
    private WeakReference<FragmentActivity> mActivityWeakReference;

    private int mCurrentPosition = 0;
    private boolean isFromAutoScroll = true;
    private ViewPager.OnPageChangeListener mOnPageChangeListener;
    private Unbinder unbinder;
    private boolean isFromIntro, isFromScenes;
    private String mReceiverId, mScenesId;
    private int mLikesDone = 0;
    private int mHidesDone = 0;
    private int mSparksDone = 0;
    private boolean isFailed = false;
    private boolean favorites_viewed = false;

    public BuySparkDialogFragment() {
        // Required empty public constructor
//        mSimpleBuySparkEventListener = new SimpleBuySparkEventListener();
    }

    public static ArrayList<SparkPackageModal> getSamplePackages() {
        ArrayList<SparkPackageModal> sparkPackageModals = new ArrayList<>();

        for (int i = 0; i < 2; i++) {
            SparkPackageModal sparkPackageModal = new SparkPackageModal();
            sparkPackageModal.setmTitle("This is title " + i);
            sparkPackageModal.setmSubtitle("This is sub-title " + i);
            sparkPackageModal.setmPrice("$ 123" + i);
            sparkPackageModal.setmExpiryDays(10);
            sparkPackageModal.setmPackageSku(GooglePlayBillingHandler.TEST_SKU_PURCHASED);
            sparkPackageModal.setmSparkCount(5);
            sparkPackageModals.add(sparkPackageModal);
        }

        return sparkPackageModals;
    }

    public static BuySparkDialogFragment newInstance(BuySparkFragmentDataModal buySparkFragmentDataModal) {
        BuySparkDialogFragment buySparkDialogFragment = new BuySparkDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(SPARKS_RECEIVER_ID_KEY, buySparkFragmentDataModal.getmReceiverId());
        bundle.putBoolean(SPARKS_FROM_INTRO, buySparkFragmentDataModal.isFromIntro());
        bundle.putBoolean(SPARKS_FROM_SCENES, buySparkFragmentDataModal.isFromScenes());
        bundle.putString(SPARKS_SCENES_ID, buySparkFragmentDataModal.getmScenesId());
        bundle.putInt(SPARKS_LIKES_KEY, buySparkFragmentDataModal.getmLikesDone());
        bundle.putInt(SPARKS_HIDES_KEY, buySparkFragmentDataModal.getmHidesDone());
        bundle.putInt(SPARKS_SPARKS_KEY, buySparkFragmentDataModal.getmSparksDone());
        bundle.putBoolean(SPARKS_FAVORITES_VIEWED_KEY, buySparkFragmentDataModal.isFavorites_viewed());
        buySparkDialogFragment.setArguments(bundle);
        return buySparkDialogFragment;
    }

    public boolean isFromIntro() {
        return isFromIntro;
    }

    public void setFromIntro(boolean fromIntro) {
        isFromIntro = fromIntro;
//        mSimpleBuySparkEventListener.setFromIntro(fromIntro);
    }

    public void setFromScenes(boolean fromScenes) {
        isFromScenes = fromScenes;
    }

    public void setmScenesId(String mScenesId) {
        this.mScenesId = mScenesId;
    }

    public void setmReceiverId(String mReceiverId) {
        this.mReceiverId = mReceiverId;
//        mSimpleBuySparkEventListener.setReceiverId(mReceiverId);
    }

    @Override
    public void onDestroyView() {
        if (mPackagesHandler != null) {
            mPackagesHandler.register(null);
        }

        if (mTimer != null) {
            mTimer.cancel();
        }

        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_buy_spark_dialog, container, false);
        mContext = getActivity().getApplicationContext();
        unbinder = ButterKnife.bind(this, view);
        mActivityWeakReference = new WeakReference<>(getActivity());

        setFromIntro(getArguments().getBoolean(SPARKS_FROM_INTRO));
        setmReceiverId(getArguments().getString(SPARKS_RECEIVER_ID_KEY));
        mLikesDone = getArguments().getInt(SPARKS_LIKES_KEY);
        mHidesDone = getArguments().getInt(SPARKS_HIDES_KEY);
        mSparksDone = getArguments().getInt(SPARKS_SPARKS_KEY);
        favorites_viewed = getArguments().getBoolean(SPARKS_FAVORITES_VIEWED_KEY, false);
        if (mTrackingBuySparkEventListener != null) {
            mTrackingBuySparkEventListener.reInitialize(isFromIntro, isFromScenes, mScenesId, mReceiverId, mLikesDone, mHidesDone, mSparksDone, favorites_viewed);
        }

        String[] strings = getResources().getStringArray(R.array.desc_packages_new);
        mDescStrings = new ArrayList<>(Arrays.asList(strings));
        TypedArray typedArray = getResources().obtainTypedArray(R.array.desc_packages_drawables_new);
        mDescImages = new ArrayList<>();
        for (int i = 0; i < typedArray.length(); i++) {
            mDescImages.add(i, typedArray.getResourceId(i, -1));
        }
        typedArray.recycle();

        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(mContext);
        mSparkPackagesRV.setLayoutManager(mLinearLayoutManager);
        mProgressbar.getIndeterminateDrawable().setColorFilter(ActivityCompat.getColor(mContext, R.color.colorTertiary),
                android.graphics.PorterDuff.Mode.SRC_IN);
//        initializeRecyclerView(BuySparkDialogFragment.getSamplePackages());
        toggleProgressContainer(true, false, null);
        getmPackagesHandler().getPackages();

        return view;
    }

    @Subscribe
    public void showRestorePurchases(ShowRestorePurchasesEvent showRestorePurchaseEvent) {
        mRestorePurchasesView.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.restore_purchases_view)
    public void restorePurchases() {
        if (mTrackingBuySparkEventListener != null) {
            mTrackingBuySparkEventListener.restorePurchasesClicked();
        }
//        if (mSimpleBuySparkEventListener != null) {
//            mSimpleBuySparkEventListener.restorePurchasesClicked();
//        }
    }

    @OnClick(R.id.retry_button)
    public void retryGetPackages() {
        toggleProgressContainer(true, false, null);
        getmPackagesHandler().getPackages();
    }

    @OnClick(R.id.buy_spark_dialog)
    public void onBuySparkDialogClick() {
    }

//    @OnClick(R.id.close_button)
//    public void onCloseButtonClicked() {
//        getActivity().onBackPressed();
//    }

    private ArrayList<String> getDescStrings() {
        if (mDescStrings == null) {
            String[] strings = getResources().getStringArray(R.array.desc_packages_new);
            mDescStrings = new ArrayList<>(Arrays.asList(strings));
        }

        return mDescStrings;
    }

    private ArrayList<Integer> getDescImages() {
        if (mDescImages == null) {
            TypedArray typedArray = getResources().obtainTypedArray(R.array.desc_packages_drawables_new);
            mDescImages = new ArrayList<>();
            for (int i = 0; i < typedArray.length(); i++) {
                mDescImages.add(i, typedArray.getResourceId(i, -1));
            }
            typedArray.recycle();
        }

        return mDescImages;
    }

    private void initializeUI(ArrayList<SparkPackageModal> sparkPackageModals) {

        ArrayList<String> mDescStrings = getDescStrings();
        ArrayList<Integer> mDescImages = getDescImages();

        mImageAndTexts = new ArrayList<>();
        for (int i = 0; i < mDescStrings.size(); i++) {
            ImageAndText imageAndText = new ImageAndText();
            imageAndText.setmText(mDescStrings.get(i));
            imageAndText.setmDrawable(mDescImages.get(i));
            mImageAndTexts.add(imageAndText);
        }

        BuySparkDialogViewPagerAdapter mBuySparkDialogViewPagerAdapter = new BuySparkDialogViewPagerAdapter(mImageAndTexts, mContext);
        mViewPager.setAdapter(mBuySparkDialogViewPagerAdapter);
        if (mOnPageChangeListener == null) {
            mOnPageChangeListener = new ViewPager.SimpleOnPageChangeListener() {
                @Override
                public void onPageSelected(int position) {
                    super.onPageSelected(position);
                    if (!isFromAutoScroll) {
                        if (mTimer != null) {
                            mTimer.cancel();
                        }
                    }

                    isFromAutoScroll = false;
                }
            };
        }
        mViewPager.addOnPageChangeListener(mOnPageChangeListener);
        startTimer();

        mPageIndicator.setStrokeColor(
                ActivityCompat.getColor(mContext, R.color.black));
        mPageIndicator.setFillColor(
                ActivityCompat.getColor(mContext, R.color.black));
        mPageIndicator.setViewPager(mViewPager);


        SparkPackagesAdapter mSparkPackagesAdapter = new SparkPackagesAdapter(mContext, sparkPackageModals,
                mTrackingBuySparkEventListener, mReceiverId);
        mSparkPackagesRV.setAdapter(mSparkPackagesAdapter);
        if (isAnyMatchGuarantee(sparkPackageModals) && ABHandler.getABValue(mContext, ABHandler.ABTYPE.AB_SPARKS_MATCH_GUARANTEE_TAG)) {
            mMatchGuaranteeTncTv.setVisibility(View.VISIBLE);
            mMatchGuaranteeTncTv.setText(Html.fromHtml("<sup>*</sup>" + mContext.getResources().getString(R.string.terms_conditions_apply)));
        } else {
            mMatchGuaranteeTncTv.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.match_guarantee_tnc_tv)
    void onClickMatchGuaranteeTncTv() {
        //TODO: move this to view stub
        mMatchGuaranteeTncContainerView.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.match_guarantee_tnc_ok_tv)
    void onClickMatchGuaranteeTncOkTV() {
        mMatchGuaranteeTncContainerView.setVisibility(View.GONE);
    }

    @OnClick(R.id.match_guarantee_tnc_container)
    void onClickMatchGuaranteeTncContainer() {

    }

    private boolean isAnyMatchGuarantee(ArrayList<SparkPackageModal> sparkPackageModals) {
        for (SparkPackageModal spark : sparkPackageModals) {
            if (spark.isMatchGuarantee()) {
                return true;
            }
        }
        return false;
    }

    private void toggleProgressContainer(boolean showProgressContainer, boolean isFailed, String message) {
        if (showProgressContainer) {
            mBuySparkPopupIVContainer.setVisibility(View.VISIBLE);
            mPackagesContainer.setVisibility(View.GONE);
            mLoaderContainer.setVisibility(View.VISIBLE);
            if (isFailed) {
//                animateLoader(UiUtils.dpToPx(92), 200, false);
                animateLoader(-UiUtils.dpToPx(50), 200, false);
                mRetryContainer.setVisibility(View.VISIBLE);
                if (Utility.isSet(message)) {
                    mRetryMessageTV.setText(message);
                } else {
                    mRetryMessageTV.setText(R.string.whoops_no_internet);
                }
                mLoadingTV.setVisibility(View.GONE);
                this.isFailed = true;
            } else {
                this.isFailed = false;
//                animateLoader(UiUtils.dpToPx(150), 0, true);
                animateLoader(0, 0, true);
                mRetryContainer.setVisibility(View.GONE);
                mLoadingTV.setVisibility(View.VISIBLE);
            }
        } else {
            this.isFailed = false;
            mCurrentPosition = 0;
//            animateLoader(0, 200, false);
            mPackagesContainer.setVisibility(View.VISIBLE);
            mLoaderContainer.setVisibility(View.GONE);
            mLoadingTV.setVisibility(View.GONE);
            mBuySparkPopupIVContainer.setVisibility(View.GONE);
        }
    }

    private void animateLoader(int toYPosition, int duration, boolean isLoading) {
        TranslateAnimation translateAnimation = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, 0,
                Animation.RELATIVE_TO_PARENT, 0,
                Animation.ABSOLUTE, mCurrentPosition,
                Animation.ABSOLUTE, toYPosition
        );

        translateAnimation.setDuration(duration);
        translateAnimation.setFillAfter(true);
        mBuySparkPopupIVContainer.startAnimation(translateAnimation);
        mCurrentPosition = toYPosition;
        mDeterminateProgressbar.setVisibility((!isLoading) ? View.VISIBLE : View.GONE);
        mProgressbar.setVisibility((!isLoading) ? View.GONE : View.VISIBLE);
    }

    public SparksHandler.PackagesHandler getmPackagesHandler() {
        if (mPackagesHandler == null) {
            mPackagesHandler = new SparksHandler.PackagesHandler(mContext, new SparksHandler.PackagesHandler.PackageshandlerListener() {
                @Override
                public void onPackagesReceived(boolean success,
                                               ArrayList<SparkPackageModal> sparkPackageModals,
                                               ArrayList<String> mDescStrings,
                                               String message) {
                    if (success && sparkPackageModals != null && sparkPackageModals.size() > 0) {
                        toggleProgressContainer(false, false, message);
                        initializeUI(sparkPackageModals);
                    } else {
                        toggleProgressContainer(true, true, message);
//                        AlertsHandler.showMessage(getActivity(), message);
                    }

                }
            });
            mPackagesHandler.setmMatchId(mReceiverId);
        }

        return mPackagesHandler;
    }

    @Override
    public void reInitialize(Object object) {
        if (!(object instanceof BuySparkFragmentDataModal)) {
            throw new IllegalArgumentException("Parameter object should be an instance of " + BuySparkFragmentDataModal.class.getSimpleName());
        }
        BuySparkFragmentDataModal buySparkFragmentDataModal = (BuySparkFragmentDataModal) object;
        setmReceiverId(buySparkFragmentDataModal.getmReceiverId());
        getmPackagesHandler().setmMatchId(buySparkFragmentDataModal.getmReceiverId());
        setFromIntro(buySparkFragmentDataModal.isFromIntro);
        setFromScenes(buySparkFragmentDataModal.isFromScenes());
        setmScenesId(buySparkFragmentDataModal.getmScenesId());
        mLikesDone = buySparkFragmentDataModal.getmLikesDone();
        mHidesDone = buySparkFragmentDataModal.getmHidesDone();
        mSparksDone = buySparkFragmentDataModal.getmSparksDone();
        favorites_viewed = buySparkFragmentDataModal.isFavorites_viewed();
        if (mTrackingBuySparkEventListener != null) {
            mTrackingBuySparkEventListener.reInitialize(isFromIntro, isFromScenes, mScenesId, mReceiverId, mLikesDone, mHidesDone, mSparksDone, favorites_viewed);
        }
        if (isFailed) {
            toggleProgressContainer(true, false, null);
            getmPackagesHandler().getPackages();
        }
        startTimer();
    }

    @OnClick(R.id.buy_spark_dialog_container)
    public void closeFragment() {
        if (mTrackingBuySparkEventListener != null) {
            mTrackingBuySparkEventListener.closeFragment();
        }
//        if (mSimpleBuySparkEventListener != null) {
//            mSimpleBuySparkEventListener.closeFragment();
//        }

        if (mTimer != null) {
            mTimer.cancel();
        }
    }

    @Override
    public void onBackPressedActivity() {
        closeFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
        BusProvider.getInstance().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        BusProvider.getInstance().unregister(this);
    }

    @Override
    public void registerListener(Object listener) {
        if (listener != null && !(listener instanceof BuySparkEventListener)) {
            throw new IllegalArgumentException("listener should be an instance of " + BuySparkEventListener.class.getName());
        }

        mTrackingBuySparkEventListener = (TrackingBuySparkEventListener) listener;
        if (mTrackingBuySparkEventListener != null) {
            mTrackingBuySparkEventListener.onRegistered();
            mTrackingBuySparkEventListener.reInitialize(isFromIntro, isFromScenes, mScenesId, mReceiverId, mLikesDone, mHidesDone, mSparksDone, favorites_viewed);
        }
//        mBuySparkEventListener = (BuySparkEventListener) listener;
//        mBuySparkEventListener.onRegistered();
//        mSimpleBuySparkEventListener.register(mBuySparkEventListener);
    }

    private void startTimer() {
        mTimer = new Timer();
        TimerTask mTimerTask = new TimerTask() {
            @Override
            public void run() {
                Activity activity = mActivityWeakReference.get();
                if (mViewPager != null && mImageAndTexts != null && mImageAndTexts.size() > 1
                        && activity != null) {
                    mCurrentPage = (mCurrentPage + 1) % mImageAndTexts.size();
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (mViewPager != null) {
                                isFromAutoScroll = true;
                                mViewPager.setCurrentItem(mCurrentPage, true);
                            }
                        }
                    });
                } else {
                    mTimer.cancel();
                }
            }
        };

        mTimer.schedule(mTimerTask, 5000, 5000);
    }

    public static class BuySparkFragmentDataModal {
        private String mReceiverId, mScenesId;
        private boolean isFromIntro = false, favorites_viewed = false, isFromScenes;
        private int mLikesDone = 0, mHidesDone = 0, mSparksDone;

        public BuySparkFragmentDataModal(String mReceiverId, boolean isFromIntro, int mLikesDone,
                                         int mHidesDone, int mSparksDone, boolean favorites_viewed,
                                         boolean isFromScenes, String mScenesId) {
            this.mReceiverId = mReceiverId;
            this.isFromIntro = isFromIntro;
            this.mLikesDone = mLikesDone;
            this.mHidesDone = mHidesDone;
            this.mSparksDone = mSparksDone;
            this.favorites_viewed = favorites_viewed;
            this.isFromScenes = isFromScenes;
            this.mScenesId = mScenesId;
        }

        public boolean isFromIntro() {
            return isFromIntro;
        }

        public void setFromIntro(boolean fromIntro) {
            isFromIntro = fromIntro;
        }

        public String getmReceiverId() {
            return mReceiverId;
        }

        public void setmReceiverId(String mReceiverId) {
            this.mReceiverId = mReceiverId;
        }

        public int getmLikesDone() {
            return mLikesDone;
        }

        public void setmLikesDone(int mLikesDone) {
            this.mLikesDone = mLikesDone;
        }

        public int getmHidesDone() {
            return mHidesDone;
        }

        public void setmHidesDone(int mHidesDone) {
            this.mHidesDone = mHidesDone;
        }

        public int getmSparksDone() {
            return mSparksDone;
        }

        public void setmSparksDone(int mSparksDone) {
            this.mSparksDone = mSparksDone;
        }

        public boolean isFavorites_viewed() {
            return favorites_viewed;
        }

        public boolean isFromScenes() {
            return isFromScenes;
        }

        public void setFromScenes(boolean fromScenes) {
            isFromScenes = fromScenes;
        }

        public String getmScenesId() {
            return mScenesId;
        }

        public void setmScenesId(String mScenesId) {
            this.mScenesId = mScenesId;
        }
    }

}
