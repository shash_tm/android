package com.trulymadly.android.app.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.trulymadly.android.app.R;
import com.trulymadly.android.app.listener.GetSubReasonsInterface;
import com.trulymadly.android.app.utility.Utility;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class JustTryingItOutFragment extends Fragment implements GetSubReasonsInterface{

	private EditText mExperienceET;

	public JustTryingItOutFragment() {
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View mJustTryingItOutFragment = inflater.inflate(R.layout.del_acc_just_trying_it_out_fragment, container, false);
		
		mExperienceET = (EditText) mJustTryingItOutFragment.findViewById(R.id.experience_et);
		
		return mJustTryingItOutFragment;
	}

	@Override
	public String getSubReason() {
		String text = mExperienceET.getText().toString();
		if(Utility.isSet(text))
			return text;
		else
			return null;
	}

}
