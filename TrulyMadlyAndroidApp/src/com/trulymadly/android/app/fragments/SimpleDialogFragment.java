package com.trulymadly.android.app.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.listener.ActivityEventsListener;
import com.trulymadly.android.app.modal.SimpleDialogModal;
import com.trulymadly.android.app.utility.Utility;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class SimpleDialogFragment extends Fragment implements ActivityEventsListener {

    public static final int SIMPLE_DIALOG_INTRODUCING_SPARK = 1;
    public static final int SIMPLE_DIALOG_ONE_LEFT = 2;
    public static final int SIMPLE_DIALOG_PURCHASE_COMPLETE = 3;
    private static final String SIMPLE_DIALOG_MODAL_KEY = "simple_dialog_modal";
    @BindView(R.id.simple_dialog_icon)
    ImageView mIconIV;
    @BindView(R.id.simple_dialog_icon_2)
    ImageView mIconIV2;
    @BindView(R.id.header)
    TextView mHeaderTV;
    @BindView(R.id.description_tv)
    TextView mDescTV;
    @BindView(R.id.action_button)
    Button mActionButton;

    private SimpleDialogActionListener mSimpleDialogActionListener;
    private SimpleDialogModal mSimpleDialogModal;
    private Unbinder unbinder;

    public SimpleDialogFragment() {
        // Required empty public constructor
    }

    public static SimpleDialogFragment newInstance(SimpleDialogModal simpleDialogModal) {
        SimpleDialogFragment simpleDialogFragment = new SimpleDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(SIMPLE_DIALOG_MODAL_KEY, simpleDialogModal);
        simpleDialogFragment.setArguments(bundle);
        return simpleDialogFragment;
    }

    public static SimpleDialogModal getSimpleDialogModal(Context context, int mode) {
        return getSimpleDialogModal(context, mode, 0, 0, false);

    }

    public static SimpleDialogModal getSimpleDialogModal(Context context, int mode, int newSparksAdded, int totalSparks, boolean isRelationshipExpertAdded) {
        SimpleDialogModal simpleDialogModal = null;
        switch (mode) {
            case SIMPLE_DIALOG_INTRODUCING_SPARK:
                simpleDialogModal = new SimpleDialogModal(context.getString(R.string.introducing_spark),
                        context.getString(R.string.introducing_spark_desc), context.getString(R.string.try_it),
                        SIMPLE_DIALOG_INTRODUCING_SPARK, R.drawable.ic_spark_intro, 0);
                break;
            case SIMPLE_DIALOG_ONE_LEFT:
                simpleDialogModal = new SimpleDialogModal(context.getString(R.string.one_spark_left),
                        context.getString(R.string.buy_more_to_spark), context.getString(R.string.buy_spark), SIMPLE_DIALOG_ONE_LEFT,
                        R.drawable.ic_spark_intro, 0);
                break;
            case SIMPLE_DIALOG_PURCHASE_COMPLETE:
                String completeString = newSparksAdded + " " + context.getString(isRelationshipExpertAdded ? R.string.new_sparks : R.string.new_sparks_added);
                if (newSparksAdded == 0) {
                    completeString = context.getString(isRelationshipExpertAdded ? R.string.new_sparks_caps : R.string.new_sparks_have_been_added);
                }
                if (isRelationshipExpertAdded) {
                    completeString += "\n&\n" + context.getString(R.string.tm_relationship_expert_added);
                }
                simpleDialogModal = new SimpleDialogModal(completeString,
                        null, context.getString(R.string.get_sparking), SIMPLE_DIALOG_PURCHASE_COMPLETE,
                        R.drawable.ic_spark_intro, isRelationshipExpertAdded ? R.drawable.ic_spark_relationship_expert : 0);
                simpleDialogModal.setNewSparksAdded(newSparksAdded);
                simpleDialogModal.setTotalSparks(totalSparks);
                break;

        }

        return simpleDialogModal;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_simple_dialog, container, false);
        Context mContext = getActivity().getApplicationContext();
        unbinder = ButterKnife.bind(this, view);

        mSimpleDialogModal = getArguments().getParcelable(SIMPLE_DIALOG_MODAL_KEY);
        mHeaderTV.setText(mSimpleDialogModal.getmHeader());
        if (Utility.isSet(mSimpleDialogModal.getmDesc())) {
            mDescTV.setVisibility(View.VISIBLE);
            mDescTV.setText(mSimpleDialogModal.getmDesc());
        } else {
            mDescTV.setVisibility(View.GONE);
        }
        mActionButton.setText(mSimpleDialogModal.getmActionText());
        Picasso.with(mContext).load(mSimpleDialogModal.getmDrawableId()).noFade().into(mIconIV);
        if (mSimpleDialogModal.getmDrawableId2() != 0) {
            mIconIV2.setVisibility(View.VISIBLE);
            Picasso.with(mContext).load(mSimpleDialogModal.getmDrawableId2()).noFade().into(mIconIV2);
        } else {
            mIconIV2.setVisibility(View.GONE);
        }

        return view;
    }

    @OnClick({R.id.simple_dialog_container, R.id.simple_dialog, R.id.action_button})
    public void onClicked(View view) {
        switch (view.getId()) {
            case R.id.simple_dialog_container:
                if (mSimpleDialogActionListener != null) {
                    mSimpleDialogActionListener.closeFragment();
                }
                break;
            case R.id.simple_dialog:
                break;
            case R.id.action_button:
                if (mSimpleDialogActionListener != null) {
                    mSimpleDialogActionListener.onActionButtonClicked(mSimpleDialogModal.getmAction());
                }
                break;
        }
    }

    @Override
    public void reInitialize(Object object) {

    }

    @Override
    public void onBackPressedActivity() {
        if (mSimpleDialogActionListener != null) {
            mSimpleDialogActionListener.closeFragment();
        }
    }

    @Override
    public void registerListener(Object listener) {
        if (listener != null && !(listener instanceof SimpleDialogActionListener)) {
            throw new IllegalArgumentException("listener should be an instance of " +
                    SimpleDialogActionListener.class.getSimpleName());
        }

        mSimpleDialogActionListener = (SimpleDialogActionListener) listener;
    }

    public interface SimpleDialogActionListener {
        void onActionButtonClicked(int action);

        void closeFragment();
    }
}
