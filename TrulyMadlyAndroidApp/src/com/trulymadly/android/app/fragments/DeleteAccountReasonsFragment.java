package com.trulymadly.android.app.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.trulymadly.android.app.R;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class DeleteAccountReasonsFragment extends Fragment {

	private OnDeleteAccountReasonsClickedListener mListener;

	public DeleteAccountReasonsFragment() {
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View mDeleteAccountReasonsView = inflater.inflate(R.layout.delete_account_reasons_fragment, container, false);

		Context mContext = getActivity();

//		boolean isShowingPV = Utility.getBool(mContext,
//				Constants.SHOW_DISCOVERY_OPTION);
//		boolean isFemale = Utility.stringCompare(Utility.getBool(mContext, Constants.SHARED_KEYS_USER_GENDER), "f");
//		boolean showPV = !isShowingPV && isFemale; 

		View mFoundSomeoneSpecialView = mDeleteAccountReasonsView.findViewById(R.id.reason_found_someone);
		View mDidntLikeHereView = mDeleteAccountReasonsView.findViewById(R.id.reason_didnt_like);
		View mTooPublicView = mDeleteAccountReasonsView.findViewById(R.id.reason_too_public);
//		mOthersView = mDeleteAccountReasonsView.findViewById(R.id.reason_others);
		
		mFoundSomeoneSpecialView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(mListener != null)
					mListener.onReasonClicked(v.getId());
			}
		});
		
		mDidntLikeHereView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(mListener != null)
					mListener.onReasonClicked(v.getId());
			}
		});

//		mOthersView.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				if(mListener != null)
//					mListener.onReasonClicked(v.getId());
//			}
//		});
		
		mTooPublicView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(mListener != null)
					mListener.onReasonClicked(v.getId());
		}
			});

		
		return mDeleteAccountReasonsView;
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		mListener = (OnDeleteAccountReasonsClickedListener) activity;
	}
	
	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}
	
	public interface OnDeleteAccountReasonsClickedListener{
		void onReasonClicked(int id);
	}

}
