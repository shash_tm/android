package com.trulymadly.android.app.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.adapter.SparkPackagesAdapter;
import com.trulymadly.android.app.modal.SparkPackageModal;
import com.trulymadly.android.app.modal.TMExtrasViewPagerItemModal;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.SparksHandler;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class TMExtrasFragment extends Fragment {

    public static final String SPARKS_PACKAGES_LIST_KEY = "sparks_packages";
    public static final String SPARKS_TM_EXTRAS_PAGER_LIST_KEY = "sparks_images_pager";

    @BindView(R.id.tm_extras_individual_pager)
    ViewPager mViewPager;
    @BindView(R.id.tm_extras_individual_rv)
    RecyclerView mRecyclerView;

    private Context mContext;
    private LinearLayoutManager mLinearLayoutManager;
    private SparkPackagesAdapter mSparkPackagesAdapter;
    private TMExtrasViewPagerAdapter mTmExtrasViewPagerAdapter;
    private SparksHandler.PackagesHandler mPackagesHandler;
    private BuySparkEventListener mBuySparkEventListener;
    private Unbinder unbinder;

    public TMExtrasFragment() {
        // Required empty public constructor
    }

    public static TMExtrasFragment getInstance(ArrayList<SparkPackageModal> sparkPackageModals,
                                               ArrayList<TMExtrasViewPagerItemModal> tmExtrasViewPagerItemModals) {
        TMExtrasFragment tmExtrasFragment = new TMExtrasFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(SPARKS_PACKAGES_LIST_KEY, sparkPackageModals);
        bundle.putParcelableArrayList(SPARKS_TM_EXTRAS_PAGER_LIST_KEY, tmExtrasViewPagerItemModals);
        tmExtrasFragment.setArguments(bundle);
        return tmExtrasFragment;
    }

    public static ArrayList<TMExtrasViewPagerItemModal> getDummyData() {
        ArrayList<TMExtrasViewPagerItemModal> list = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            TMExtrasViewPagerItemModal item = new TMExtrasViewPagerItemModal(
                    "http://www.menucool.com/slider/jsImgSlider/images/image-slider-2.jpg", "", "Text " + i);
            list.add(item);
        }
        return list;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tmextras, container, false);
        unbinder = ButterKnife.bind(this, view);
        mContext = getContext();

        mLinearLayoutManager = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);

        //Required to enable fling inside NestedScrollView
        mRecyclerView.setNestedScrollingEnabled(false);

        ArrayList<TMExtrasViewPagerItemModal> tmExtrasViewPagerItemModals =
                getArguments().getParcelableArrayList(SPARKS_TM_EXTRAS_PAGER_LIST_KEY);
        mTmExtrasViewPagerAdapter = new TMExtrasViewPagerAdapter(mContext, tmExtrasViewPagerItemModals);
        mViewPager.setAdapter(mTmExtrasViewPagerAdapter);

        getmPackagesHandler().getPackages();

        return view;
    }

    private void initializeRecyclerView(ArrayList<SparkPackageModal> sparkPackageModals) {

//        sparkPackageModals = getArguments().getParcelableArrayList(SPARKS_PACKAGES_LIST_KEY);
        mSparkPackagesAdapter = new SparkPackagesAdapter(mContext, sparkPackageModals, mBuySparkEventListener, "EXTRAS");
        mRecyclerView.setAdapter(mSparkPackagesAdapter);
    }

    public SparksHandler.PackagesHandler getmPackagesHandler() {
        if (mPackagesHandler == null) {
            mPackagesHandler = new SparksHandler.PackagesHandler(mContext, new SparksHandler.PackagesHandler.PackageshandlerListener() {
                @Override
                public void onPackagesReceived(boolean success,
                                               ArrayList<SparkPackageModal> sparkPackageModals,
                                               ArrayList<String> descStrings,
                                               String message) {
                    if (success) {
                        initializeRecyclerView(sparkPackageModals);
                    } else {
                        AlertsHandler.showMessage(getActivity(), message);
                    }

                }
            });
        }

        return mPackagesHandler;
    }

    public void setBuySparkEventListener(BuySparkEventListener mBuySparkEventListener) {
        this.mBuySparkEventListener = mBuySparkEventListener;
    }

    public class TMExtrasViewPagerAdapter extends PagerAdapter {

        private Context mContext;
        private ArrayList<TMExtrasViewPagerItemModal> mTmExtrasViewPagerItemModals;
        private LayoutInflater mInflater;

        public TMExtrasViewPagerAdapter(Context mContext, ArrayList<TMExtrasViewPagerItemModal>
                mTmExtrasViewPagerItemModals) {
            this.mContext = mContext;
            this.mTmExtrasViewPagerItemModals = mTmExtrasViewPagerItemModals;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            if (mInflater == null) {
                mInflater = (LayoutInflater) mContext
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            }

            View itemView = mInflater.inflate(R.layout.tmextra_viewpager_item,
                    container, false);

            ((TextView) itemView.findViewById(R.id.text)).setText(
                    mTmExtrasViewPagerItemModals.get(position).getmText());
            ((TextView) itemView.findViewById(R.id.title)).setText(
                    mTmExtrasViewPagerItemModals.get(position).getmTitle());
            Picasso.with(mContext).load(mTmExtrasViewPagerItemModals.get(position).getmImageUrl()).into(
                    ((ImageView) itemView.findViewById(R.id.image))
            );

            container.addView(itemView);
            return itemView;
        }

        @Override
        public int getCount() {
            return (mTmExtrasViewPagerItemModals == null) ? 0 : mTmExtrasViewPagerItemModals.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((RelativeLayout) object);
        }

    }

}
