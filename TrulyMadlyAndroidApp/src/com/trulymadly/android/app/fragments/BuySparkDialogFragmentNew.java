package com.trulymadly.android.app.fragments;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.trulymadly.android.app.R;
import com.trulymadly.android.app.adapter.BuySparkDialogViewPagerAdapterNew;
import com.trulymadly.android.app.listener.ActivityEventsListener;
import com.trulymadly.android.app.listener.TrackingBuySparkEventListener;
import com.trulymadly.android.app.modal.ImageAndText;
import com.trulymadly.android.app.modal.SparkPackageModal;
import com.trulymadly.android.app.utility.CirclePageIndicator;
import com.trulymadly.android.app.utility.SparksHandler;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 */
public class BuySparkDialogFragmentNew extends BottomSheetDialogFragment implements ActivityEventsListener {

    public static final String SPARKS_RECEIVER_ID_KEY = "sparks_receiver_id";
    public static final String SPARKS_FROM_INTRO = "sparks_from_intro";
    public static final String SPARKS_FROM_SCENES = "sparks_from_scenes";
    public static final String SPARKS_SCENES_ID = "sparks_scenes_id";
    public static final String SPARKS_LIKES_KEY = "sparks_likes_id";
    public static final String SPARKS_HIDES_KEY = "sparks_hides_id";
    public static final String SPARKS_SPARKS_KEY = "sparks_sparks_id";
    public static final String CLOSE_ACTIVITY_ON_DISMISSED = "close_activity_on_dismissed";
    public static final String SPARKS_FAVORITES_VIEWED_KEY = "sparks_favorites_viewed";

    @BindView(R.id.buy_spark_viewpager)
    ViewPager mViewPager;
    @BindView(R.id.packages_container)
    View mPackagesContainer;
    @BindView(R.id.page_indicator)
    CirclePageIndicator mPageIndicator;
    @BindView(R.id.loading_tv)
    View mLoadingTV;
    @BindView(R.id.retry_button)
    Button mRetryButton;
    @BindView(R.id.retry_container)
    View mRetryContainer;
    @BindView(R.id.retry_message_tv)
    TextView mRetryMessageTV;
    @BindView(R.id.loader_container)
    View mLoaderContainer;

    @BindView(R.id.buy_spark_popup_iv_container)
    View mBuySparkPopupIVContainer;
    @BindView(R.id.progressbar)
    ProgressBar mProgressbar;
    @BindView(R.id.determinate_progressbar)
    ProgressBar mDeterminateProgressbar;

    //Tabs
    @BindViews({R.id.first_tab_container, R.id.sec_tab_container, R.id.third_tab_container})
    List<View> mTabContainers;
    @BindViews({R.id.first_tab, R.id.sec_tab, R.id.third_tab})
    List<View> mTabs;
    @BindViews({R.id.first_tab_most_popular_tag, R.id.sec_tab_most_popular_tag, R.id.third_tab_most_popular_tag})
    List<View> mTabsMostPopularTags;
    @BindViews({R.id.first_tab_selected_most_popular_tag, R.id.sec_tab_selected_most_popular_tag, R.id.third_tab_selected_most_popular_tag})
    List<View> mTabsSelectedMostPopularTags;
    @BindViews({R.id.first_tab_selected, R.id.sec_tab_selected, R.id.third_tab_selected})
    List<View> mTabsSelected;
    @BindViews({R.id.first_tab_title, R.id.sec_tab_title, R.id.third_tab_title})
    List<TextView> mTabsTitles;
    @BindViews({R.id.first_tab_selected_title, R.id.sec_tab_selected_title, R.id.third_tab_selected_title})
    List<TextView> mTabsSelectedTitles;

    @BindView(R.id.sparks_validity_string)
    TextView mSparksValidityString;
    @BindView(R.id.spark_icon_container)
    View mSparkIconContainer;
    @BindView(R.id.spark_icon_iv)
    ImageView mSparkIconIV;
    @BindView(R.id.sparks_count_tv)
    TextView mSparkCountTV;
    @BindView(R.id.chat_session_container)
    View mChatSessionContainer;
    @BindView(R.id.chat_icon_iv)
    ImageView mChatIconIV;
    @BindView(R.id.chat_session_tv)
    TextView mChatSessionTV;
    @BindView(R.id.discount_container)
    View mDiscountContainer;
    @BindView(R.id.discount_tv)
    TextView mDiscountTV;
    @BindView(R.id.buy_button)
    Button mBuyButton;

    @BindView(R.id.pulldown_arrow)
    ImageView mPulldownIV;

    private RootViewSetListener mRootViewSetListener;

    private boolean isInitialized = false;

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback;
    private CoordinatorLayout.Behavior behavior;
    private View mContentView;

    private Context mContext;
    //    private BuySparkEventListener mBuySparkEventListener;
    private TrackingBuySparkEventListener mTrackingBuySparkEventListener;
    private SparksHandler.PackagesHandler mPackagesHandler;

    private ArrayList<String> mDescStrings;
    private ArrayList<Integer> mDescImages;
    private ArrayList<ImageAndText> mImageAndTexts;

    private Timer mTimer;
    private int mCurrentPage = 0;
    private WeakReference<FragmentActivity> mActivityWeakReference;

    private int mCurrentPosition = 0;
    private boolean isFromAutoScroll = true;
    private ViewPager.OnPageChangeListener mOnPageChangeListener;
    private Unbinder unbinder;
    private boolean isFromIntro, isFromScenes;
    private String mReceiverId, mScenesId;
    private int mLikesDone = 0;
    private int mHidesDone = 0;
    private int mSparksDone = 0;
    private boolean isFailed = false, closeActivityOnDismiss = false, favorites_viewed = false;
    private ArrayList<SparkPackageModal> mSparkPackageModals;
    private int mSelectedSparkPackageModalIndex;

    public BuySparkDialogFragmentNew() {
        // Required empty public constructor
    }

    public static BuySparkDialogFragmentNew newInstance(BuySparkFragmentDataModal buySparkFragmentDataModal){
        BuySparkDialogFragmentNew buySparkDialogFragmentNew = new BuySparkDialogFragmentNew();
        Bundle bundle = new Bundle();
        bundle.putString(SPARKS_RECEIVER_ID_KEY, buySparkFragmentDataModal.getmReceiverId());
        bundle.putBoolean(SPARKS_FROM_INTRO, buySparkFragmentDataModal.isFromIntro());
        bundle.putBoolean(SPARKS_FROM_SCENES, buySparkFragmentDataModal.isFromScenes());
        bundle.putString(SPARKS_SCENES_ID, buySparkFragmentDataModal.getmScenesId());
        bundle.putInt(SPARKS_LIKES_KEY, buySparkFragmentDataModal.getmLikesDone());
        bundle.putInt(SPARKS_HIDES_KEY, buySparkFragmentDataModal.getmHidesDone());
        bundle.putInt(SPARKS_SPARKS_KEY, buySparkFragmentDataModal.getmSparksDone());
        bundle.putBoolean(SPARKS_FAVORITES_VIEWED_KEY, buySparkFragmentDataModal.isFavorites_viewed());
        bundle.putBoolean(CLOSE_ACTIVITY_ON_DISMISSED, buySparkFragmentDataModal.isCloseActivityOnDismiss());
        buySparkDialogFragmentNew.setArguments(bundle);
        return buySparkDialogFragmentNew;
//        return new BuySparkDialogFragmentNew();
    }

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        mContentView = View.inflate(getContext(), R.layout.fragment_buy_spark_dialog_new, null);
        if(mRootViewSetListener != null){
            mRootViewSetListener.onRootViewSet(mContentView);
        }
        dialog.setContentView(mContentView);
        unbinder = ButterKnife.bind(this, mContentView);
        mPulldownIV.setImageResource(R.drawable.pull_down_arrow);

//        if(!isInitialized){
            isInitialized = true;
            mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {
                    if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                        dismiss();
                    }

                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                }
            };
            mContext = getActivity().getApplicationContext();
            mActivityWeakReference = new WeakReference<>(getActivity());
            setFromIntro(getArguments().getBoolean(SPARKS_FROM_INTRO));
            setmReceiverId(getArguments().getString(SPARKS_RECEIVER_ID_KEY));
            mLikesDone = getArguments().getInt(SPARKS_LIKES_KEY);
            mHidesDone = getArguments().getInt(SPARKS_HIDES_KEY);
            mSparksDone = getArguments().getInt(SPARKS_SPARKS_KEY);
        favorites_viewed = getArguments().getBoolean(SPARKS_FAVORITES_VIEWED_KEY, false);
            closeActivityOnDismiss = getArguments().getBoolean(CLOSE_ACTIVITY_ON_DISMISSED);

            if (mTrackingBuySparkEventListener != null) {
                mTrackingBuySparkEventListener.reInitialize(isFromIntro, isFromScenes, mScenesId, mReceiverId, mLikesDone, mHidesDone, mSparksDone, favorites_viewed);
            }

            String[] strings = getResources().getStringArray(R.array.desc_packages_new);
            mDescStrings = new ArrayList<>(Arrays.asList(strings));
            TypedArray typedArray = getResources().obtainTypedArray(R.array.desc_packages_drawables_new);
            mDescImages = new ArrayList<>();
            for (int i = 0; i < typedArray.length(); i++) {
                mDescImages.add(i, typedArray.getResourceId(i, -1));
            }
            typedArray.recycle();

            mProgressbar.getIndeterminateDrawable().setColorFilter(ActivityCompat.getColor(mContext, R.color.colorTertiary),
                    android.graphics.PorterDuff.Mode.SRC_IN);
            toggleProgressContainer(true, false, null);
            getmPackagesHandler().getPackages();
//        }

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) mContentView.getParent()).getLayoutParams();
//        params.setMargins(0, UiUtils.dpToPx(30), 0, 0);
        behavior = params.getBehavior();

        if( behavior != null && behavior instanceof BottomSheetBehavior ) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
            dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog) {
                    ((BottomSheetBehavior) behavior).setPeekHeight(mContentView.getHeight());
                }
            });
        }

    }

    public void setCloseActivityOnDismissed(boolean closeActivityOnDismiss){
        this.closeActivityOnDismiss = closeActivityOnDismiss;
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        //TODO : SPARKS: Is this the right place to do this?
        if(closeActivityOnDismiss && getActivity() != null) {
            getActivity().onBackPressed();
        }
    }

    @Override
    public void onDestroyView() {
        if (mPackagesHandler != null) {
            mPackagesHandler.register(null);
        }

        if (mTimer != null) {
            mTimer.cancel();
        }

        super.onDestroyView();
        unbinder.unbind();
    }

    public void setFromIntro(boolean fromIntro) {
        isFromIntro = fromIntro;
    }

    public void setFromScenes(boolean fromScenes) {
        isFromScenes = fromScenes;
    }

    public void setmScenesId(String mScenesId) {
        this.mScenesId = mScenesId;
    }

    public void setmReceiverId(String mReceiverId) {
        this.mReceiverId = mReceiverId;
    }

    @Override
    public void reInitialize(Object object) {
        if (!(object instanceof BuySparkFragmentDataModal)) {
            throw new IllegalArgumentException("Parameter object should be an instance of " + BuySparkFragmentDataModal.class.getSimpleName());
        }
        BuySparkFragmentDataModal buySparkFragmentDataModal = (BuySparkFragmentDataModal) object;
        setmReceiverId(buySparkFragmentDataModal.getmReceiverId());
        getmPackagesHandler().setmMatchId(buySparkFragmentDataModal.getmReceiverId());
        setFromIntro(buySparkFragmentDataModal.isFromIntro);
        setFromScenes(buySparkFragmentDataModal.isFromScenes());
        setmScenesId(buySparkFragmentDataModal.getmScenesId());
        mLikesDone = buySparkFragmentDataModal.getmLikesDone();
        mHidesDone = buySparkFragmentDataModal.getmHidesDone();
        mSparksDone = buySparkFragmentDataModal.getmSparksDone();
        mSparksDone = buySparkFragmentDataModal.getmSparksDone();
        favorites_viewed = buySparkFragmentDataModal.isFavorites_viewed();
        if (mTrackingBuySparkEventListener != null) {
            mTrackingBuySparkEventListener.reInitialize(isFromIntro, isFromScenes, mScenesId, mReceiverId,
                    mLikesDone, mHidesDone, mSparksDone, favorites_viewed);
        }
        if (isFailed) {
            toggleProgressContainer(true, false, null);
            getmPackagesHandler().getPackages();
        }
        startTimer();
    }

    @Override
    public void onBackPressedActivity() {
        closeFragment();
    }

    @OnClick(R.id.buy_spark_dialog_container)
    public void closeFragment() {
        if (mTrackingBuySparkEventListener != null) {
            mTrackingBuySparkEventListener.closeFragment();
        }
//        if (mSimpleBuySparkEventListener != null) {
//            mSimpleBuySparkEventListener.closeFragment();
//        }

        if (mTimer != null) {
            mTimer.cancel();
        }
    }

    @OnClick({R.id.restore_purchases_view, R.id.buy_spark_dialog,
        R.id.first_tab, R.id.sec_tab, R.id.third_tab, R.id.buy_button,
            R.id.retry_button})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.buy_spark_dialog:
                break;

            case R.id.buy_button:
                if (mTrackingBuySparkEventListener != null) {
                    mTrackingBuySparkEventListener.onBuySparkClicked(
                            mSparkPackageModals.get(mSelectedSparkPackageModalIndex), mReceiverId);
                }
                break;

            case R.id.first_tab:
                populatePackageSegment(0, true, mSparkPackageModals.get(0));
                break;
            case R.id.sec_tab:
                populatePackageSegment(1, true, mSparkPackageModals.get(1));
                break;
            case R.id.third_tab:
                populatePackageSegment(2, true, mSparkPackageModals.get(2));
                break;
            case R.id.restore_purchases_view:
                if (mTrackingBuySparkEventListener != null) {
                    mTrackingBuySparkEventListener.restorePurchasesClicked();
                }
                break;
            case R.id.retry_button:
                toggleProgressContainer(true, false, null);
                getmPackagesHandler().getPackages();
                break;
        }
    }

    private void selectTab(int position){
        //Unselect previously selected tab
        mTabs.get(mSelectedSparkPackageModalIndex).setVisibility(View.VISIBLE);
        mTabsSelected.get(mSelectedSparkPackageModalIndex).setVisibility(View.GONE);
        if(mSparkPackageModals.get(mSelectedSparkPackageModalIndex).isMostPopular()) {
            mTabsMostPopularTags.get(mSelectedSparkPackageModalIndex).setVisibility(View.VISIBLE);
        }else{
            mTabsMostPopularTags.get(mSelectedSparkPackageModalIndex).setVisibility(View.GONE);
        }

        RelativeLayout.LayoutParams firstLayoutParams = (RelativeLayout.LayoutParams) mTabs.get(0).getLayoutParams();
        RelativeLayout.LayoutParams secLayoutParams = (RelativeLayout.LayoutParams) mTabs.get(1).getLayoutParams();
        RelativeLayout.LayoutParams thirdLayoutParams = (RelativeLayout.LayoutParams) mTabs.get(2).getLayoutParams();
        int px = UiUtils.dpToPx(2);
        //Ad appropriate margins
        switch (position){
            case 0:
                firstLayoutParams.setMargins(0, 0, 0, 0);
                secLayoutParams.setMargins(0, 0, px, 0);
                thirdLayoutParams.setMargins(px, 0, 0, 0);
                break;
            case 1:
                firstLayoutParams.setMargins(0, 0, 0, 0);
                secLayoutParams.setMargins(0, 0, 0, 0);
                thirdLayoutParams.setMargins(0, 0, 0, 0);
                break;
            case 2:
                firstLayoutParams.setMargins(0, 0, px, 0);
                secLayoutParams.setMargins(px, 0, 0, 0);
                thirdLayoutParams.setMargins(0, 0, 0, 0);
                break;
        }

        mSelectedSparkPackageModalIndex = position;
    }

    private void populatePackageSegment(int position, boolean select, SparkPackageModal sparkPackageModal){
        if(select){
            selectTab(position);
        }
        if(mSelectedSparkPackageModalIndex == position) {
            mTabs.get(position).setVisibility(View.GONE);
            mTabsSelected.get(position).setVisibility(View.VISIBLE);
            mTabsMostPopularTags.get(position).setVisibility(View.GONE);
            if(sparkPackageModal.isMostPopular()) {
                mTabsSelectedMostPopularTags.get(position).setVisibility(View.VISIBLE);
            }else{
                mTabsSelectedMostPopularTags.get(position).setVisibility(View.GONE);
            }
        }else{
            mTabs.get(position).setVisibility(View.VISIBLE);
            mTabsSelected.get(position).setVisibility(View.GONE);
            mTabsSelectedMostPopularTags.get(position).setVisibility(View.GONE);
            if(sparkPackageModal.isMostPopular()) {
                mTabsMostPopularTags.get(position).setVisibility(View.VISIBLE);
            }else{
                mTabsMostPopularTags.get(position).setVisibility(View.GONE);
            }
        }
        mTabsTitles.get(position).setText(sparkPackageModal.getmTitle());
        mTabsSelectedTitles.get(position).setText(sparkPackageModal.getmTitle());

        if(select) {
            mSparksValidityString.setText(String.format(mContext.getString(R.string.sparks_validity), String.valueOf(sparkPackageModal.getmExpiryDays())));
            mSparkCountTV.setText(Html.fromHtml(String.format(mContext.getString(R.string.count_sparks), String.valueOf(sparkPackageModal.getmSparkCount()))));
            mBuyButton.setText(String.format(mContext.getString(R.string.buy_for_action_string), String.valueOf(sparkPackageModal.getmPrice())));

            mSparkIconIV.setImageResource(R.drawable.spark_icon);
            mChatIconIV.setImageResource(R.drawable.chat_icon);

            mChatSessionContainer.setVisibility(View.VISIBLE);
            mChatSessionTV.setText(sparkPackageModal.getmSubtitle());

            if (sparkPackageModal.getmDiscount() > 0) {
                mDiscountContainer.setVisibility(View.VISIBLE);
                mDiscountTV.setText(String.valueOf(sparkPackageModal.getmDiscount()) + "%");
            } else {
                mDiscountContainer.setVisibility(View.GONE);
            }
        }

//        if(sparkPackageModal.isChatSessionsAvailable()){
//            mChatSessionContainer.setVisibility(View.VISIBLE);
//            mChatSessionTV.setText(sparkPackageModal.getmSubtitle());
//        }else{
//            mChatSessionContainer.setVisibility(View.GONE);
//        }
    }

    @Override
    public void registerListener(Object listener) {
        if (listener != null && !(listener instanceof BuySparkEventListener)) {
            throw new IllegalArgumentException("listener should be an instance of " + BuySparkEventListener.class.getName());
        }

        mTrackingBuySparkEventListener = (TrackingBuySparkEventListener) listener;
        if(mTrackingBuySparkEventListener != null) {
            mTrackingBuySparkEventListener.onRegistered();
            mTrackingBuySparkEventListener.reInitialize(isFromIntro, isFromScenes, mScenesId, mReceiverId,
                    mLikesDone, mHidesDone, mSparksDone, favorites_viewed);
        }
    }

    public View getRootView(){
        return mContentView;
    }

    private void startTimer() {
        mTimer = new Timer();
        TimerTask mTimerTask = new TimerTask() {
            @Override
            public void run() {
                Activity activity = mActivityWeakReference.get();
                if (mViewPager != null && mImageAndTexts != null && mImageAndTexts.size() > 1
                        && activity != null) {
                    mCurrentPage = (mCurrentPage + 1) % mImageAndTexts.size();
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (mViewPager != null) {
                                isFromAutoScroll = true;
                                mViewPager.setCurrentItem(mCurrentPage, true);
                            }
                        }
                    });
                } else {
                    mTimer.cancel();
                }
            }
        };

        mTimer.schedule(mTimerTask, 5000, 5000);
    }

    private ArrayList<String> getDescStrings(){
        if(mDescStrings == null){
            String[] strings = getResources().getStringArray(R.array.desc_packages_new);
            mDescStrings = new ArrayList<>(Arrays.asList(strings));
        }

        return mDescStrings;
    }

    private ArrayList<Integer> getDescImages(){
        if(mDescImages == null){
            TypedArray typedArray = getResources().obtainTypedArray(R.array.desc_packages_drawables_new);
            mDescImages = new ArrayList<>();
            for (int i = 0; i < typedArray.length(); i++) {
                mDescImages.add(i, typedArray.getResourceId(i, -1));
            }
            typedArray.recycle();
        }

        return mDescImages;
    }

    private void initializeUI(ArrayList<SparkPackageModal> sparkPackageModals) {

        ArrayList<String> mDescStrings = getDescStrings();
        ArrayList<Integer> mDescImages = getDescImages();

        mImageAndTexts = new ArrayList<>();
        for (int i = 0; i < mDescStrings.size(); i++) {
            ImageAndText imageAndText = new ImageAndText();
            imageAndText.setmText(mDescStrings.get(i));
            imageAndText.setmDrawable(mDescImages.get(i));
            mImageAndTexts.add(imageAndText);
        }

        BuySparkDialogViewPagerAdapterNew mBuySparkDialogViewPagerAdapterNew = new BuySparkDialogViewPagerAdapterNew(mImageAndTexts, mContext);
        mViewPager.setAdapter(mBuySparkDialogViewPagerAdapterNew);
        if (mOnPageChangeListener == null) {
            mOnPageChangeListener = new ViewPager.SimpleOnPageChangeListener() {
                @Override
                public void onPageSelected(int position) {
                    super.onPageSelected(position);
                    if (!isFromAutoScroll) {
                        if (mTimer != null) {
                            mTimer.cancel();
                        }
                    }

                    isFromAutoScroll = false;
                }
            };
        }
        mViewPager.addOnPageChangeListener(mOnPageChangeListener);
        startTimer();

        mPageIndicator.setStrokeColor(
                ActivityCompat.getColor(mContext, R.color.black));
        mPageIndicator.setFillColor(
                ActivityCompat.getColor(mContext, R.color.black));
        mPageIndicator.setViewPager(mViewPager);


//        sparkPackageModals.get(1).setMostPopular(true);
//        sparkPackageModals.get(1).setChatSessionsAvailable(true);
//        sparkPackageModals.get(2).setChatSessionsAvailable(true);
        mSelectedSparkPackageModalIndex = 0;
        mSparkPackageModals = sparkPackageModals;
        if(sparkPackageModals.get(1).isMostPopular()){
            mSelectedSparkPackageModalIndex = 1;
        }else if(sparkPackageModals.get(2).isMostPopular()){
            mSelectedSparkPackageModalIndex = 2;
        }

        for (int i = 0 ; i < sparkPackageModals.size(); i++) {
            populatePackageSegment(i, i == mSelectedSparkPackageModalIndex, sparkPackageModals.get(i));
        }

    }

    private void toggleProgressContainer(boolean showProgressContainer, boolean isFailed, String message) {
        if (showProgressContainer) {
            mBuySparkPopupIVContainer.setVisibility(View.VISIBLE);
            mPackagesContainer.setVisibility(View.GONE);
            mLoaderContainer.setVisibility(View.VISIBLE);
            if (isFailed) {
//                animateLoader(UiUtils.dpToPx(92), 200, false);
                animateLoader(-UiUtils.dpToPx(50), 200, false);
                mRetryContainer.setVisibility(View.VISIBLE);
                if (Utility.isSet(message)) {
                    mRetryMessageTV.setText(message);
                } else {
                    mRetryMessageTV.setText(R.string.whoops_no_internet);
                }
                mLoadingTV.setVisibility(View.GONE);
                this.isFailed = true;
            } else {
                this.isFailed = false;
                animateLoader(0, 200, true);
                mRetryContainer.setVisibility(View.GONE);
                mLoadingTV.setVisibility(View.VISIBLE);
            }
        } else {
            mBuySparkPopupIVContainer.setVisibility(View.GONE);
            this.isFailed = false;
            mCurrentPosition = 0;
//            animateLoader(0, 200, false);
            mPackagesContainer.setVisibility(View.VISIBLE);
            mLoaderContainer.setVisibility(View.GONE);
            mLoadingTV.setVisibility(View.GONE);
        }
    }

    private void animateLoader(int toYPosition, int duration, boolean isLoading) {
//        mDeterminateProgressbar.setVisibility((!isLoading) ? View.VISIBLE : View.GONE);
//        mProgressbar.setVisibility((!isLoading) ? View.GONE : View.VISIBLE);

        TranslateAnimation translateAnimation = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, 0,
                Animation.RELATIVE_TO_PARENT, 0,
                Animation.ABSOLUTE, mCurrentPosition,
                Animation.ABSOLUTE, toYPosition
        );

        translateAnimation.setDuration(duration);
        translateAnimation.setFillAfter(true);
        mBuySparkPopupIVContainer.startAnimation(translateAnimation);
        mCurrentPosition = toYPosition;
        mDeterminateProgressbar.setVisibility((!isLoading) ? View.VISIBLE : View.GONE);
        mProgressbar.setVisibility((!isLoading) ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(!(activity instanceof RootViewSetListener)){
            throw new IllegalArgumentException("Activity should be anstance of RootViewSetListener");
        }

        mRootViewSetListener = (RootViewSetListener) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mRootViewSetListener = null;
    }

    public SparksHandler.PackagesHandler getmPackagesHandler() {
        if (mPackagesHandler == null) {
            mPackagesHandler = new SparksHandler.PackagesHandler(mContext, new SparksHandler.PackagesHandler.PackageshandlerListener() {
                @Override
                public void onPackagesReceived(boolean success,
                                               ArrayList<SparkPackageModal> sparkPackageModals,
                                               ArrayList<String> mDescStrings,
                                               String message) {
                    if (success && sparkPackageModals != null && sparkPackageModals.size() > 0) {
                        toggleProgressContainer(false, false, message);
                        initializeUI(sparkPackageModals);
                    } else {
                        toggleProgressContainer(true, true, message);
//                        AlertsHandler.showMessage(getActivity(), message);
                    }

                }
            });
            mPackagesHandler.setmMatchId(mReceiverId);
        }

        return mPackagesHandler;
    }

    public interface RootViewSetListener{
        void onRootViewSet(View view);
    }

    public static class BuySparkFragmentDataModal {
        private String mReceiverId, mScenesId;
        private boolean isFromIntro = false, isFromScenes = false;
        private int mLikesDone = 0, mHidesDone = 0, mSparksDone;
        private boolean closeActivityOnDismiss, favorites_viewed;

        public BuySparkFragmentDataModal(String mReceiverId, boolean isFromIntro, int mLikesDone,
                                         int mHidesDone, int mSparksDone, boolean closeActivityOnDismiss,
                                         boolean favorites_viewed, boolean isFromScenes, String mScenesId) {
            this.mReceiverId = mReceiverId;
            this.isFromIntro = isFromIntro;
            this.mLikesDone = mLikesDone;
            this.mHidesDone = mHidesDone;
            this.mSparksDone = mSparksDone;
            this.closeActivityOnDismiss = closeActivityOnDismiss;
            this.favorites_viewed = favorites_viewed;
            this.isFromScenes = isFromScenes;
            this.mScenesId = mScenesId;
        }

        public boolean isFromIntro() {
            return isFromIntro;
        }

        public void setFromIntro(boolean fromIntro) {
            isFromIntro = fromIntro;
        }

        public boolean isFromScenes() {
            return isFromScenes;
        }

        public void setFromScenes(boolean fromScenes) {
            isFromScenes = fromScenes;
        }

        public String getmReceiverId() {
            return mReceiverId;
        }

        public void setmReceiverId(String mReceiverId) {
            this.mReceiverId = mReceiverId;
        }

        public int getmLikesDone() {
            return mLikesDone;
        }

        public void setmLikesDone(int mLikesDone) {
            this.mLikesDone = mLikesDone;
        }

        public int getmHidesDone() {
            return mHidesDone;
        }

        public void setmHidesDone(int mHidesDone) {
            this.mHidesDone = mHidesDone;
        }

        public int getmSparksDone() {
            return mSparksDone;
        }

        public void setmSparksDone(int mSparksDone) {
            this.mSparksDone = mSparksDone;
        }

        public boolean isCloseActivityOnDismiss() {
            return closeActivityOnDismiss;
        }

        public void setCloseActivityOnDismiss(boolean closeActivityOnDismiss) {
            this.closeActivityOnDismiss = closeActivityOnDismiss;
        }

        public boolean isFavorites_viewed() {
            return favorites_viewed;
        }

        public String getmScenesId() {
            return mScenesId;
        }

        public void setmScenesId(String mScenesId) {
            this.mScenesId = mScenesId;
        }
    }
}
