package com.trulymadly.android.app.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;

import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventStatus;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.listener.CustomOkHttpResponseHandler;
import com.trulymadly.android.app.listener.GetSubReasonsInterface;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class OthersFragment extends Fragment
implements GetSubReasonsInterface{

	private EditText mOthersET;
	private Context mContext;
	private ProgressDialog mProgressDialog;
	public OthersFragment() {
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View mOthersView = inflater.inflate(R.layout.del_acc_others_fragment, container, false);
		
		mContext = getActivity();
		
		mOthersET = (EditText) mOthersView.findViewById(R.id.other_reason_et);
		View mProfileVisibilityButton = mOthersView.findViewById(R.id.profile_visibility_button);
		mProfileVisibilityButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				final boolean isChecked = false;
				CustomOkHttpResponseHandler okHttpResponseHandler = new CustomOkHttpResponseHandler(
						mContext) {

					@Override
					public void onRequestSuccess(JSONObject responseJSON) {
						SPHandler.setBool(mContext,
								Constants.DISCOVERY_OPTION, isChecked);
						mProgressDialog = UiUtils.hideProgressBar(mProgressDialog);
						SPHandler.setBool(mContext,
								Constants.SHOW_DISCOVERY_OPTION, true);
						TrulyMadlyTrackEvent.trackEvent(mContext, TrulyMadlyActivities.settings,
								TrulyMadlyEventTypes.discovery_option_toggled_delete, 0, TrulyMadlyEventStatus.success, null, true);

						SPHandler.setBool(mContext, ConstantsSP.SHARED_KEYS_PROFILE_VISIBILITY_MESSAGE, true);
						OthersFragment.this.getActivity().finish();
					}

					@Override
					public void onRequestFailure(Exception exception) {
						AlertsHandler.showNetworkError(OthersFragment.this.getActivity(), exception);
					}
				};
				mProgressDialog = UiUtils
						.showProgressBar(mContext, mProgressDialog);
				Utility.sendDiscoveryOptions(isChecked, mContext, okHttpResponseHandler);
				
			}
		});

		boolean isShowingPV = SPHandler.getBool(mContext,
				Constants.SHOW_DISCOVERY_OPTION);
		boolean isFemale = !Utility.isMale(mContext);
		boolean showPV = !isShowingPV && isFemale; 
		
		if(!showPV){
			mOthersView.findViewById(R.id.profile_visibility_desc).setVisibility(View.GONE);
			mOthersView.findViewById(R.id.profile_visibility_button).setVisibility(View.GONE);
		}
		
		return mOthersView;
	}

	@Override
	public String getSubReason() {
		String otherReason = mOthersET.getText().toString();
		if(Utility.isSet(otherReason))
			return otherReason;
		else
			return null;
	}

}
