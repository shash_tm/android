package com.trulymadly.android.app.fragments;

import com.trulymadly.android.app.billing.PaymentMode;
import com.trulymadly.android.app.modal.MySelectData;

/**
 * Created by avin on 08/08/16.
 */
public interface BuySparkEventListener {
    void closeFragment();

    void onBuySparkClicked(Object packageModal, String matchId);

    void onBuySparkSuccess(PaymentMode mPaymentMode, String sku,
                           int newSparksAdded, int totalSparks, boolean isRelationshipExpertAdded, MySelectData mySelectData);

    void restorePurchasesClicked();

    void onRegistered();
}
