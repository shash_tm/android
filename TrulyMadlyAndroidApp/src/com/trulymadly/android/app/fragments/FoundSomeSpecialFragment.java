package com.trulymadly.android.app.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.listener.GetSubReasonsInterface;
import com.trulymadly.android.app.utility.ActivityHandler;
import com.trulymadly.android.app.utility.Utility;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class FoundSomeSpecialFragment extends Fragment
        implements GetSubReasonsInterface {

    @BindView(R.id.story_et)
    EditText mStoryET;
    @BindView(R.id.share_on_instagram)
    View mShareInstagram;
    private Unbinder unbinder;

    public FoundSomeSpecialFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mFoundSomeSpecialFragment = inflater.inflate(R.layout.del_acc_found_someone_fragment, container, false);
        unbinder = ButterKnife.bind(this, mFoundSomeSpecialFragment);
        boolean showInstagram = ActivityHandler.appInstalledOrNot(getContext(), Constants.packageInstagram);
        mShareInstagram.setVisibility(showInstagram ? View.VISIBLE : View.GONE);
        TrulyMadlyTrackEvent.trackEvent(getContext(), TrulyMadlyEvent.TrulyMadlyActivities.delete,
                TrulyMadlyEvent.TrulyMadlyEventTypes.share_instagram_shown, 0,
                showInstagram ? TrulyMadlyEvent.TrulyMadlyEventStatus.success : TrulyMadlyEvent.TrulyMadlyEventStatus.fail, null, true);
        return mFoundSomeSpecialFragment;
    }

    @Override
    public String getSubReason() {
        String story = mStoryET.getText().toString();
        if (Utility.isSet(story))
            return story;
        else
            return null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.share_on_instagram)
    void onClickShareInstagram() {
        TrulyMadlyTrackEvent.trackEvent(getContext(), TrulyMadlyEvent.TrulyMadlyActivities.delete,
                TrulyMadlyEvent.TrulyMadlyEventTypes.share_instagram_clicked, 0,
                TrulyMadlyEvent.TrulyMadlyEventStatus.success, null, true);
        ActivityHandler.openInstagramProfile(getContext(), Constants.instagramTmProfile);
    }

    @OnClick(R.id.like_on_facebook)
    void onClickLikeFacebook() {
        TrulyMadlyTrackEvent.trackEvent(getContext(), TrulyMadlyEvent.TrulyMadlyActivities.delete,
                TrulyMadlyEvent.TrulyMadlyEventTypes.like_facebook_clicked, 0,
                TrulyMadlyEvent.TrulyMadlyEventStatus.success, null, true);
        ActivityHandler.openFacebookProfileTrulymadly(getContext());
    }

    @OnClick(R.id.refer_a_friend)
    void onClickReferAFriend() {
        TrulyMadlyTrackEvent.trackEvent(getContext(), TrulyMadlyEvent.TrulyMadlyActivities.delete,
                TrulyMadlyEvent.TrulyMadlyEventTypes.refer_a_friend, 0,
                TrulyMadlyEvent.TrulyMadlyEventStatus.success, null, true);
        ActivityHandler.shareViaTextIntent(getActivity());
    }
}
