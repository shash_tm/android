package com.trulymadly.android.app.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.adapter.SparkMsgSuggestionsAdapter;
import com.trulymadly.android.app.json.ConstantsRF;
import com.trulymadly.android.app.listener.ActivityEventsListener;
import com.trulymadly.android.app.modal.SparkSendModal;
import com.trulymadly.android.app.sqlite.RFHandler;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.MoEHandler;
import com.trulymadly.android.app.utility.ServerCodes;
import com.trulymadly.android.app.utility.SparksHandler;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;

import net.frakbot.jumpingbeans.JumpingBeans;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.Unbinder;

import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities.sparks;

/**
 * A simple {@link Fragment} subclass.
 */
public class SendSparkFragment extends Fragment implements ActivityEventsListener {

    public static final String SPARKS_MESSAGES_LIST_KEY = "sparks_messages";
    public static final String SPARKS_RECEIVER_ID_KEY = "sparks_receiver_id";
    public static final String SPARKS_FROM_INTRO = "sparks_from_intro";
    public static final String SPARKS_FROM_SCENES = "sparks_from_scenes";
    public static final String SPARKS_SCENES_ID = "sparks_scenes_id";
    public static final String SPARKS_HASH_KEY = "sparks_hash_id";
    public static final String SPARKS_HAS_LIKED_BEFORE = "sparks_has_liked_before";
    public static final String SPARKS_LIKES_KEY = "sparks_likes_id";
    public static final String SPARKS_HIDES_KEY = "sparks_hides_id";
    public static final String SPARKS_SPARKS_KEY = "sparks_sparks_id";
    private final int SEND_SPARK_DIALOG = 1;
    private final int SEND_SPARK_CONFIRMATION_DIALOG = 2;
    private final int SPARK_SENT_DIALOG = 3;

    @BindView(R.id.send_spark_dialog)
    View mSendSparkDialog;
    @BindView(R.id.spark_left)
    ImageView mSparkLeftButton;
    @BindView(R.id.spark_right)
    ImageView mSparkRightButton;
    @BindView(R.id.spark_messages_container)
    View mSparkMessagesViewPagerContainer;
    @BindView(R.id.message_suggestions_viewpager)
    ViewPager mSparkMessagesViewPager;
    @BindView(R.id.send_spark_button)
    Button mSendSparkButton;
    @BindView(R.id.spark_message_et)
    EditText mSparkMessageET;
    @BindView(R.id.spark_message_counter)
    TextView mSparkMessageCounterTV;
    @BindView(R.id.send_spark_confirmation_dialog_container)
    View mSendConfirmationContainer;
    @BindView(R.id.send_spark_confirmation_dialog)
    View mSendConfirmationDialog;
    @BindView(R.id.commonalities_loader)
    View mCommonalitiesLoader;
    @BindView(R.id.jumping_dots)
    TextView mJumpingDotsTV;
    JumpingBeans mJumpingBeans;

    //Spark sent indicator
    @BindView(R.id.spark_sent_indicator_container)
    View mSparkSentIndicator;
    @BindView(R.id.spark_sent_indicator_iv)
    ImageView mSparkSentIndicatorIV;
    @BindView(R.id.spark_remaining_tv)
    TextView mSparkSentIndicatorTV;
    private Animation mShowSentIndicatorAnimation, mHideSentIndicatorAnimation;

    private Context mContext;
    private String mReceiverId;
    private SparkMsgSuggestionsAdapter mSparkMsgSuggestionsAdapter;
    private ArrayList<String> mMessages, mDefaultMessages;
    private int mTotalLength;
    private SendSparkEventListener mSendSparkEventListener;
    //    private boolean isConfirmationDialogVisible = false;
    private int mCurrentDialog = SEND_SPARK_DIALOG;
    private SparksHandler.Sender mSparkSender;
    private String mSparkHash;
    private boolean isCommonalitiesCallFailed = false, mHasLikedBefore = false;
    private boolean isFromScenes;
    private String mScenesId;

    private ScrollView mScrollView;
    private Unbinder unbinder;
    private boolean isSwiped = false;
    private boolean isDefaultMsgShown = false;
    private boolean isAlertShownOnce = false;
    private boolean isFromIntro = false;
    private int mLikesDone = 0;
    private int mHidesDone = 0;
    private int mSparksDone = 0;

    public SendSparkFragment() {
        // Required empty public constructor
    }

    public static ArrayList<String> getDummyMessages() {
        ArrayList<String> messages = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            messages.add("Message " + i);
        }
        return messages;
    }

    public static Fragment newInstance(SendSparkFragmentModal sendSparkFragmentModal) {
        SendSparkFragment sendSparkFragment = new SendSparkFragment();
        Bundle bundle = new Bundle();
        bundle.putString(SPARKS_RECEIVER_ID_KEY, sendSparkFragmentModal.getmReceiverId());
        bundle.putString(SPARKS_HASH_KEY, sendSparkFragmentModal.getmSparkHash());
        bundle.putBoolean(SPARKS_HAS_LIKED_BEFORE, sendSparkFragmentModal.ismHasLikedBefore());
        bundle.putBoolean(SPARKS_FROM_INTRO, sendSparkFragmentModal.isFromIntro());
        bundle.putBoolean(SPARKS_FROM_SCENES, sendSparkFragmentModal.isFromScenes());
        bundle.putString(SPARKS_SCENES_ID, sendSparkFragmentModal.getmScenesId());
        bundle.putInt(SPARKS_LIKES_KEY, sendSparkFragmentModal.getmLikesDone());
        bundle.putInt(SPARKS_HIDES_KEY, sendSparkFragmentModal.getmHidesDone());
        bundle.putInt(SPARKS_SPARKS_KEY, sendSparkFragmentModal.getmSparksDone());
        sendSparkFragment.setArguments(bundle);
        return sendSparkFragment;
    }

    @Override
    public void onDestroyView() {
        if (mSparkSender != null) {
            mSparkSender.registerListener(null);
        }
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_send_spark, container, false);
        mContext = getActivity().getApplicationContext();
        unbinder = ButterKnife.bind(this, view);
        mTotalLength = getResources().getInteger(R.integer.spark_message_char_limit);

        mScrollView = (ScrollView) view;
        mScrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                mScrollView.fullScroll(View.FOCUS_DOWN);
            }
        });

        Picasso.with(mContext).load(R.drawable.left_arrow_orange).noFade().into(mSparkLeftButton);
        Picasso.with(mContext).load(R.drawable.right_arrow_orange).noFade().into(mSparkRightButton);

        mDefaultMessages = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.default_commonalities)));
        isFromIntro = getArguments().getBoolean(SPARKS_FROM_INTRO);
        isFromScenes = getArguments().getBoolean(SPARKS_FROM_SCENES);
        mScenesId = getArguments().getString(SPARKS_SCENES_ID);
        mReceiverId = getArguments().getString(SPARKS_RECEIVER_ID_KEY);
        mSparkHash = getArguments().getString(SPARKS_HASH_KEY);
        mHasLikedBefore = getArguments().getBoolean(SPARKS_HAS_LIKED_BEFORE);
        mLikesDone = getArguments().getInt(SPARKS_LIKES_KEY);
        mHidesDone = getArguments().getInt(SPARKS_HIDES_KEY);
        mSparksDone = getArguments().getInt(SPARKS_SPARKS_KEY);
        mSparkMsgSuggestionsAdapter = new SparkMsgSuggestionsAdapter(mMessages, mContext);
        mSparkMessagesViewPager.setAdapter(mSparkMsgSuggestionsAdapter);
        mSparkMessagesViewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                isSwiped = true;
                if (mMessages != null) {
                    if (position == 0) {
                        mSparkLeftButton.setVisibility(View.INVISIBLE);
                        if (mMessages.size() > 1) {
                            mSparkRightButton.setVisibility(View.VISIBLE);
                        } else {
                            mSparkRightButton.setVisibility(View.INVISIBLE);
                        }
                    } else if (position == mMessages.size() - 1) {
                        mSparkRightButton.setVisibility(View.INVISIBLE);
                        if (mMessages.size() > 1) {
                            mSparkLeftButton.setVisibility(View.VISIBLE);
                        } else {
                            mSparkLeftButton.setVisibility(View.INVISIBLE);
                        }
                    } else {
                        mSparkLeftButton.setVisibility(View.VISIBLE);
                        mSparkRightButton.setVisibility(View.VISIBLE);
                    }
                } else {
                    mSparkLeftButton.setVisibility(View.INVISIBLE);
                    mSparkRightButton.setVisibility(View.INVISIBLE);
                }
            }
        });
        mSparkMessageET.setFilters(new InputFilter[]{new InputFilter.LengthFilter(mTotalLength)});
        onMessageTextChanged();

        mSendConfirmationDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        mShowSentIndicatorAnimation = new AlphaAnimation(0, 1);
        mShowSentIndicatorAnimation.setDuration(418);

        mHideSentIndicatorAnimation = new AlphaAnimation(1, 0);
        mHideSentIndicatorAnimation.setDuration(218);
        mHideSentIndicatorAnimation.setStartOffset(500);

        mHideSentIndicatorAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (mSendSparkEventListener != null) {
                    callCloseFragment();
//                    mSendSparkEventListener.closeFragment();
                    mSendSparkEventListener.sparkSentSuccess();
                    toggleDialogs(SEND_SPARK_DIALOG);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        mShowSentIndicatorAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mSparkSentIndicator.startAnimation(mHideSentIndicatorAnimation);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        requestCommonalities();

        mSendSparkDialog.setVisibility(View.INVISIBLE);
        mSendSparkDialog.post(new Runnable() {
            @Override
            public void run() {
                makeVisible();
            }
        });

        return view;
    }

    private void requestCommonalities() {
        mCommonalitiesLoader.setVisibility(View.VISIBLE);
        mSparkRightButton.setVisibility(View.INVISIBLE);
        mSparkLeftButton.setVisibility(View.INVISIBLE);
        mJumpingBeans = JumpingBeans.with(mJumpingDotsTV).appendJumpingDots().build();
        getSparkSender().getCommonalities(mReceiverId, mScenesId);
    }

    private void loadCommonalities(ArrayList<String> messages) {
        if (messages == null || messages.size() == 0) {
            isDefaultMsgShown = true;
            isSwiped = false;
            mMessages = mDefaultMessages;
        } else {
            isDefaultMsgShown = false;
            isSwiped = false;
            mMessages = messages;
        }
        mJumpingBeans.stopJumping();
        mCommonalitiesLoader.setVisibility(View.GONE);
        if (mMessages != null) {
            if (mMessages.size() <= 1) {
                mSparkLeftButton.setVisibility(View.INVISIBLE);
                mSparkRightButton.setVisibility(View.INVISIBLE);
            } else {
                mSparkLeftButton.setVisibility(View.INVISIBLE);
                mSparkRightButton.setVisibility(View.VISIBLE);
            }
        }

        mSparkMsgSuggestionsAdapter = new SparkMsgSuggestionsAdapter(mMessages, mContext);
        mSparkMessagesViewPager.setAdapter(mSparkMsgSuggestionsAdapter);
    }

    private SparksHandler.Sender getSparkSender() {
        if (mSparkSender == null) {
            mSparkSender = new SparksHandler.Sender(mContext, new SparksHandler.Sender.SparksSenderListener() {

                @Override
                public void onSparkSent(boolean success, ServerCodes serverCodes, long timeTaken) {
                    mSendSparkButton.setText(mContext.getString(R.string.send_spark));
                    mSparkMessageET.setEnabled(true);
                    if (success) {
                        HashMap<String, String> eventInfo = new HashMap<>();
                        eventInfo.put("match_id", mReceiverId);
                        eventInfo.put("sparks_left", String.valueOf(SparksHandler.getSparksLeft(mContext)));
                        eventInfo.put("from_intro", String.valueOf(isFromIntro));
                        eventInfo.put("from_scenes", String.valueOf(isFromScenes));
                        if (isFromScenes) {
                            eventInfo.put("event_id", mScenesId);
                        }
                        eventInfo.put("def_likes", String.valueOf(isDefaultMsgShown));
                        eventInfo.put("swiped", String.valueOf(isSwiped));
                        eventInfo.put("alert", String.valueOf(isAlertShownOnce));
                        eventInfo.put("msg_len", String.valueOf(mSparkMessageET.getText().length()));
                        eventInfo.put("time_taken", String.valueOf(timeTaken));

                        eventInfo.put("likes_done", String.valueOf(mLikesDone));
                        eventInfo.put("hides_done", String.valueOf(mHidesDone));
                        eventInfo.put("sparks_done", String.valueOf(mSparksDone));
                        TrulyMadlyTrackEvent.trackEvent(mContext, sparks, TrulyMadlyEvent.TrulyMadlyEventTypes.send, 0,
                                TrulyMadlyEvent.TrulyMadlyEventStatus.send,
                                eventInfo, true, true);

                        MoEHandler.trackEvent(mContext, (mSparkMessageET.getText().length() > 0)?
                                MoEHandler.Events.SPARK_SENT_WITH_MSG_USER:MoEHandler.Events.SPARK_SENT_WITH_MSG_DEFAULT);

                        toggleDialogs(SPARK_SENT_DIALOG);
                    } else {
                        AlertsHandler.showMessage(getActivity(), serverCodes.getDesc(getActivity()), false);
                        if(serverCodes == ServerCodes.no_sparks) {
                            callCloseFragment();
//                            mSendSparkEventListener.closeFragment();
//                            AlertsHandler.showMessageWithAction(getActivity(), serverCodes.getDesc(),
//                                    R.string.buy_spark, new View.OnClickListener() {
//                                        @Override
//                                        public void onClick(View v) {
//                                            Utility.fireBusEvent(mContext, true, new SimpleAction(SimpleAction.ACTION_OPEN_BUY_SPARKS));
//                                        }
//                                    }, false, 3);
                        }

                    }
                }

                @Override
                public void onCommonalitiesReceived(boolean success, ArrayList<String> messages, String message) {
                    loadCommonalities(messages);
                    isCommonalitiesCallFailed = !success;
                }
            });
        }
        return mSparkSender;
    }

    private void toggleDialogs(int dialog) {
        if (getSparkSender().isSending()) {
            return;
        }
        switch (dialog) {
            case SEND_SPARK_DIALOG:
                mSparkSentIndicator.setVisibility(View.GONE);
                mSendSparkDialog.setVisibility(View.VISIBLE);
                mSendConfirmationContainer.setVisibility(View.GONE);
                break;
            case SEND_SPARK_CONFIRMATION_DIALOG:
                isAlertShownOnce = true;
                mSparkSentIndicator.setVisibility(View.GONE);
                mSendSparkDialog.setVisibility(View.VISIBLE);
                mSendConfirmationContainer.setVisibility(View.VISIBLE);
                break;
            case SPARK_SENT_DIALOG:
                mSparkSentIndicatorTV.setText(String.valueOf(SparksHandler.getSparksLeft(mContext)));
                mSparkSentIndicator.setVisibility(View.VISIBLE);
                mSparkSentIndicator.startAnimation(mShowSentIndicatorAnimation);
                mSendSparkDialog.setVisibility(View.GONE);
                mSendConfirmationContainer.setVisibility(View.GONE);
                break;
            default:
                dialog = SEND_SPARK_DIALOG;
                break;
        }

        mCurrentDialog = dialog;
    }

    @OnClick({R.id.spark_left, R.id.spark_right, R.id.send_spark_button,
            R.id.confirm_send_spark_tv, R.id.confirm_write_message_tv,
            R.id.send_spark_confirmation_dialog_container,
            R.id.send_spark_dialog_container, R.id.send_spark_dialog,
            R.id.send_spark_dialog_view, R.id.spark_lay_fab,
            R.id.spark_lay_fab_container})
    public void onClick(View view) {
        if (getSparkSender().isSending()) {
            return;
        }
        switch (view.getId()) {
            case R.id.spark_left:
                int currentItemLeft = mSparkMessagesViewPager.getCurrentItem();
                if (currentItemLeft > 0) {
                    mSparkMessagesViewPager.setCurrentItem(currentItemLeft - 1, true);
                }
                break;
            case R.id.spark_right:
                int currentItemRight = mSparkMessagesViewPager.getCurrentItem();
                if (currentItemRight < mMessages.size() - 1) {
                    mSparkMessagesViewPager.setCurrentItem(currentItemRight + 1, true);
                }
                break;
            case R.id.send_spark_button:
                UiUtils.hideKeyBoard(getActivity());
                if (mSparkMessageET.getText().length() == 0) {
                    toggleDialogs(SEND_SPARK_CONFIRMATION_DIALOG);
                } else {
                    sendSpark();
                }
                break;
            case R.id.confirm_send_spark_tv:
                UiUtils.hideKeyBoard(getActivity());
                toggleDialogs(SEND_SPARK_DIALOG);
                sendSpark();
                break;
            case R.id.confirm_write_message_tv:
                toggleDialogs(SEND_SPARK_DIALOG);
                break;
            case R.id.send_spark_confirmation_dialog_container:
//                toggleDialogs(SEND_SPARK_DIALOG);
                break;
            case R.id.send_spark_dialog:
                break;
            case R.id.send_spark_dialog_container:
            case R.id.send_spark_dialog_view:
            case R.id.spark_lay_fab:
            case R.id.spark_lay_fab_container:
                UiUtils.hideKeyBoard(getActivity());
                if (mSendSparkEventListener != null) {
                    callCloseFragment();
//                    mSendSparkEventListener.closeFragment();
                }
                break;
        }
    }

    private void sendSpark() {
        mSendSparkButton.setText(mContext.getString(R.string.sending_spark));
        mSparkMessageET.setEnabled(false);
        SparkSendModal sparkSendModal = new SparkSendModal();
        sparkSendModal.setmMessage(mSparkMessageET.getText().toString());
        sparkSendModal.setmReceiverId(mReceiverId);
        sparkSendModal.setmUserId(Utility.getMyId(mContext));
        sparkSendModal.setmUniqueId(Utility.generateUniqueRandomId(mReceiverId));
        sparkSendModal.setmSparkHash(mSparkHash);
        sparkSendModal.setmHasLikedBefore(mHasLikedBefore);
        getSparkSender().sendSpark(sparkSendModal);
    }

    @OnTextChanged(R.id.spark_message_et)
    public void onMessageTextChanged() {
        int length = mSparkMessageET.getText().length();
        mSparkMessageCounterTV.setText(length + "/" + mTotalLength);
    }

    public void makeVisible() {
        //Fixing crash :
        //https://fabric.io/trulymadly/android/apps/com.trulymadly.android.app/issues/578f597fffcdc042500d4175/sessions/578f594600b300016647e37a8f6479fe
        if(mSendSparkDialog != null) {
            mSparkMessageET.setFocusable(true);
            mSparkMessageET.setFocusableInTouchMode(true);
            int radius = Math.max(mSendSparkDialog.getHeight(), mSendSparkDialog.getWidth()) + UiUtils.dpToPx(40);
            UiUtils.doRevealAnimationOnView(mSendSparkDialog, true, mSendSparkDialog.getWidth() / 2,
                    mSendSparkDialog.getHeight(), radius);

            if (RFHandler.getBool(mContext, Utility.getMyId(mContext), ConstantsRF.RIGID_FIELD_SENT_SPARKS_SHOWN_ONCE)) {
                mSparkMessageET.requestFocus();
                UiUtils.showKeyBoard(getActivity());
            } else {
                RFHandler.insert(mContext, Utility.getMyId(mContext), ConstantsRF.RIGID_FIELD_SENT_SPARKS_SHOWN_ONCE, true);
            }
        }
    }

    private void callCloseFragment(){
        mSendSparkEventListener.closeFragment();
        mSparkMessageET.setFocusable(false);
        mSparkMessageET.setFocusableInTouchMode(false);
    }

    @Override
    public void reInitialize(Object object) {
        isAlertShownOnce = false;
        isSwiped = false;
        makeVisible();

        mSparkMessageET.setEnabled(true);
        mSendSparkButton.setText(mContext.getString(R.string.send_spark));
        if (!(object instanceof SendSparkFragmentModal)) {
            throw new IllegalArgumentException("Parameter object should be an instance of " + SendSparkFragmentModal.class.getSimpleName());
        }
        SendSparkFragmentModal sendSparkFragmentModal = (SendSparkFragmentModal) object;
        isFromIntro = sendSparkFragmentModal.isFromIntro();
        isFromScenes = sendSparkFragmentModal.isFromScenes();
        mScenesId = sendSparkFragmentModal.getmScenesId();
        mLikesDone = sendSparkFragmentModal.getmLikesDone();
        mHidesDone = sendSparkFragmentModal.getmHidesDone();
        mSparksDone = sendSparkFragmentModal.getmSparksDone();
        if (!Utility.stringCompare(mReceiverId, sendSparkFragmentModal.getmReceiverId())) {
            mReceiverId = sendSparkFragmentModal.getmReceiverId();
            mSparkHash = sendSparkFragmentModal.getmSparkHash();
            mHasLikedBefore = sendSparkFragmentModal.ismHasLikedBefore();
            mSparkMsgSuggestionsAdapter.changeList(mMessages);
            mSparkMessageET.setText("");
            requestCommonalities();
        } else if (isCommonalitiesCallFailed) {
            requestCommonalities();
        }
    }

    @Override
    public void onBackPressedActivity() {
        if (getSparkSender().isSending()) {
            return;
        }
        switch (mCurrentDialog) {
            case SEND_SPARK_DIALOG:
                if (mSendSparkEventListener != null) {
                    callCloseFragment();
//                    mSendSparkEventListener.closeFragment();
                }
                break;
            case SEND_SPARK_CONFIRMATION_DIALOG:
                toggleDialogs(SEND_SPARK_DIALOG);
                break;
            case SPARK_SENT_DIALOG:
                break;
        }
    }

    @Override
    public void registerListener(Object listener) {
        if (listener != null && !(listener instanceof SendSparkEventListener)) {
            throw new IllegalArgumentException("listener should be an instance of " + SendSparkEventListener.class.getName());
        }

        mSendSparkEventListener = (SendSparkEventListener) listener;
    }

    public interface SendSparkEventListener {
        void closeFragment();

        void sparkSentSuccess();

        void sparkSentFailed();
    }

    public static class SendSparkFragmentModal {
        private String mReceiverId;
        private String mSparkHash, mScenesId;
        private boolean isFromIntro, isFromScenes, mHasLikedBefore;
        private int mSparksDone, mLikesDone, mHidesDone;

        public SendSparkFragmentModal(String mReceiverId, String mSparkHash, boolean isFromIntro,
                                      int mSparksDone, int mLikesDone, int mHidesDone,
                                      boolean isFromScenes, String mScenesId, boolean mHasLikedBefore) {
            this.mReceiverId = mReceiverId;
            this.mSparkHash = mSparkHash;
            this.isFromIntro = isFromIntro;
            this.mSparksDone = mSparksDone;
            this.mLikesDone = mLikesDone;
            this.mHidesDone = mHidesDone;
            this.isFromScenes = isFromScenes;
            this.mScenesId = mScenesId;
            this.mHasLikedBefore = mHasLikedBefore;
        }

        public int getmSparksDone() {
            return mSparksDone;
        }

        public void setmSparksDone(int mSparksDone) {
            this.mSparksDone = mSparksDone;
        }

        public int getmLikesDone() {
            return mLikesDone;
        }

        public void setmLikesDone(int mLikesDone) {
            this.mLikesDone = mLikesDone;
        }

        public int getmHidesDone() {
            return mHidesDone;
        }

        public void setmHidesDone(int mHidesDone) {
            this.mHidesDone = mHidesDone;
        }

        public String getmReceiverId() {
            return mReceiverId;
        }

        public void setmReceiverId(String mReceiverId) {
            this.mReceiverId = mReceiverId;
        }

        public String getmSparkHash() {
            return mSparkHash;
        }

        public void setmSparkHash(String mSparkHash) {
            this.mSparkHash = mSparkHash;
        }

        public boolean isFromIntro() {
            return isFromIntro;
        }

        public void setFromIntro(boolean fromIntro) {
            isFromIntro = fromIntro;
        }

        public boolean isFromScenes() {
            return isFromScenes;
        }

        public void setFromScenes(boolean fromScenes) {
            isFromScenes = fromScenes;
        }

        public String getmScenesId() {
            return mScenesId;
        }

        public void setmScenesId(String mScenesId) {
            this.mScenesId = mScenesId;
        }

        public boolean ismHasLikedBefore() {
            return mHasLikedBefore;
        }

        public void setmHasLikedBefore(boolean mHasLikedBefore) {
            this.mHasLikedBefore = mHasLikedBefore;
        }
    }
}
