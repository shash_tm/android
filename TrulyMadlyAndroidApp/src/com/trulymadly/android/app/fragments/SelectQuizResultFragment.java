package com.trulymadly.android.app.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.trulymadly.android.app.CircleTransformation;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.adapter.SelectQuizCompareAdapter;
import com.trulymadly.android.app.modal.SelectQuizCompareModal;
import com.trulymadly.android.app.modal.SelectQuizModal;
import com.trulymadly.android.app.utility.TMSelectHandler.QuizHandler;
import com.trulymadly.android.app.utility.Utility;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SelectQuizResultFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SelectQuizResultFragment extends Fragment {
    private static final String SELECT_QUIZ_COMPARE_MODAL = "select_quiz_compare";
    private static final String MATCH_ID = "match_id";
    private static final String COMPATIBILITY_TEXT = "compatibility_text";
    private static final String SELECT_QUOTE = "select_quote";

    @BindView(R.id.scrollview)
    NestedScrollView mNestedScrollView;
    @BindView(R.id.match_image_iv)
    ImageView mMatchIV;
    @BindView(R.id.user_image_iv)
    ImageView mUserIV;
    @BindView(R.id.you_and_match_tv)
    TextView mYouAndMatchTV;
    @BindView(R.id.percent_match_tv)
    TextView mPercentMatchTV;
    @BindView(R.id.compatibility_string_tv)
    TextView mCompatibilityStringTV;
    @BindView(R.id.compatibility_quote_tv)
    TextView mCompatibilityQuoteTV;
    @BindView(R.id.dots)
    TextView mDotsTV;
    @BindView(R.id.responses_rv)
    RecyclerView mResponsesRV;
    @BindView(R.id.result_container)
    View mResultContainer;

    @BindView(R.id.loader_container)
    View mLoaderContainer;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;
    @BindView(R.id.retry_container)
    View mRetryContainer;
    @BindView(R.id.retry_message_tv)
    TextView mRetryMessageTV;

    private String mMatchId;
    private SelectQuizCompareModal mSelectQuizCompareModal;
    private Unbinder mUnbinder;
    private QuizHandler mQuizHandler;

    private SelectQuizCompareAdapter mSelectQuizCompareAdapter;
    private LinearLayoutManager mLinearLayoutManager;
    private SelectQuizCompareListener mSelectQuizCompareListener;
    private String mCompatibilityText, mSelectQuote;


    public SelectQuizResultFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param selectQuizCompareModal
     * @return A new instance of fragment SelectQuizResultFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SelectQuizResultFragment newInstance(SelectQuizCompareModal selectQuizCompareModal,
                                                       String compatibilityText, String quote) {
        SelectQuizResultFragment fragment = new SelectQuizResultFragment();
        Bundle args = new Bundle();
        args.putParcelable(SELECT_QUIZ_COMPARE_MODAL, selectQuizCompareModal);
        args.putString(COMPATIBILITY_TEXT, compatibilityText);
        args.putString(SELECT_QUOTE, quote);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mSelectQuizCompareModal = getArguments().getParcelable(SELECT_QUIZ_COMPARE_MODAL);
            mCompatibilityText = getArguments().getString(COMPATIBILITY_TEXT);
            mSelectQuote = getArguments().getString(SELECT_QUOTE);
            mMatchId = mSelectQuizCompareModal.getmMatchId();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_select_quiz_result, container, false);
        mUnbinder = ButterKnife.bind(this, view);

        fetchQuizCompareData();
        return view;
    }

    public String getMatchId() {
        return mMatchId;
    }

    private void toggleView(boolean isInProgress, boolean isFailed, String errorMessage) {
        if (isInProgress) {
            mProgressBar.setVisibility(View.VISIBLE);
            mRetryContainer.setVisibility(View.GONE);
            mResultContainer.setVisibility(View.GONE);
            mLoaderContainer.setVisibility(View.VISIBLE);
        } else if (isFailed) {
            mProgressBar.setVisibility(View.GONE);
            mRetryContainer.setVisibility(View.VISIBLE);
            mRetryMessageTV.setText(errorMessage);
            mResultContainer.setVisibility(View.GONE);
            mLoaderContainer.setVisibility(View.VISIBLE);
        } else {
            mProgressBar.setVisibility(View.VISIBLE);
            mRetryContainer.setVisibility(View.GONE);
            mLoaderContainer.setVisibility(View.GONE);
            mResultContainer.setVisibility(View.VISIBLE);
            loadResults();
        }
    }

    public void reInitialize(SelectQuizCompareModal selectQuizCompareModal,
                             String compatibilityString, String quote) {
        if (selectQuizCompareModal != null) {
            boolean fetchResultdata = false;
            if (!Utility.stringCompare(mMatchId, selectQuizCompareModal.getmMatchId())) {
                mMatchId = selectQuizCompareModal.getmMatchId();
                mSelectQuizCompareModal = selectQuizCompareModal;
                if (mSelectQuizCompareAdapter != null) {
                    mSelectQuizCompareAdapter.changeList(null);
                }
                mCompatibilityText = compatibilityString;
                mSelectQuote = quote;
                fetchResultdata = true;
            } else {
                mNestedScrollView.fullScroll(View.FOCUS_UP);
            }

            if (mSelectQuizCompareAdapter == null || fetchResultdata) {
                fetchQuizCompareData();
            }
        }
    }

    private void fetchQuizCompareData() {
        getmQuizHandler().fetchQuizResultData(mMatchId);
        toggleView(true, false, null);
    }

    private void loadResults() {
        Picasso.with(getContext()).load(mSelectQuizCompareModal.getmMatchUrl()).transform(new CircleTransformation()).into(mMatchIV);
        if (Utility.isSet(mSelectQuizCompareModal.getmUserUrl())) {
            Picasso.with(getContext()).load(mSelectQuizCompareModal.getmUserUrl()).transform(new CircleTransformation()).into(mUserIV);
        } else {
            int dummyImage = (Utility.isMale(getContext())) ? R.drawable.dummy_male : R.drawable.dummy_female;
            Picasso.with(getContext()).load(dummyImage).transform(new CircleTransformation()).into(mUserIV);
        }
//        mYouAndMatchTV.setText(String.format(getString(R.string.you_and_match), mSelectQuizCompareModal.getmMatchName()));
//        if (mSelectQuizCompareModal.getmPercent() >= 60) {
//            mPercentMatchTV.setText(String.format(getString(R.string.percent_match),
//                    String.valueOf(mSelectQuizCompareModal.getmPercent())));
//            mPercentMatchTV.setVisibility(View.VISIBLE);
//        } else {
//            mPercentMatchTV.setVisibility(View.GONE);
//        }

        if (Utility.isSet(mSelectQuote)) {
            mCompatibilityQuoteTV.setText(Html.fromHtml(String.format(
                    getContext().getString(R.string.compatibility_quote), mSelectQuote)));
//            mCompatibilityQuoteTV.setText(Html.fromHtml(String.format(getContext().getString(R.string.compatibility_quote),
//                    (Utility.isSet(mSelectQuote)) ? mSelectQuote
//                            : getContext().getString(R.string.compatibility_quote_default))));
            mCompatibilityQuoteTV.setVisibility(View.VISIBLE);
        } else {
            mCompatibilityQuoteTV.setVisibility(View.GONE);
        }
        if (Utility.isSet(mCompatibilityText)) {
            mCompatibilityStringTV.setText(Html.fromHtml(mCompatibilityText));
        } else {
            mCompatibilityStringTV.setText(R.string.compatibility_text_fallback);
        }
        mCompatibilityStringTV.setVisibility(View.VISIBLE);
        mDotsTV.setVisibility(View.VISIBLE);

        if (mLinearLayoutManager == null) {
            mLinearLayoutManager = new LinearLayoutManager(getContext());
            mResponsesRV.setLayoutManager(mLinearLayoutManager);
            mSelectQuizCompareAdapter = new SelectQuizCompareAdapter(getContext(), mSelectQuizCompareModal.getmMatchUrl(),
                    mSelectQuizCompareModal.getmUserUrl(), mSelectQuizCompareModal.getmSelectQuizQuesCompareResps());
            mResponsesRV.setAdapter(mSelectQuizCompareAdapter);
            mResponsesRV.setNestedScrollingEnabled(false);
        } else {
            mSelectQuizCompareAdapter.changeList(mSelectQuizCompareModal.getmSelectQuizQuesCompareResps(),
                    mSelectQuizCompareModal.getmMatchUrl(), mSelectQuizCompareModal.getmUserUrl());
        }
    }

    @OnClick({R.id.retry_button, R.id.close_iv, R.id.scrollview})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.retry_button:
                fetchQuizCompareData();
                break;
            case R.id.close_iv:
                if (mSelectQuizCompareListener != null) {
                    mSelectQuizCompareListener.closeFragment();
                }
                break;
        }
    }

    private QuizHandler getmQuizHandler() {
        if (mQuizHandler == null) {
            mQuizHandler = new QuizHandler(getContext(), new QuizHandler.QuizHandlerListener() {
                @Override
                public void onQuizFetch(boolean success, SelectQuizModal selectQuizModal, String errorMessage) {

                }

                @Override
                public void onQuizSubmit(boolean success, String error) {

                }

                @Override
                public void onQuizResult(boolean success, SelectQuizCompareModal selectQuizCompareModal, String errorMessage) {
                    if (success) {
                        mSelectQuizCompareModal.setmSelectQuizQuesCompareResps(selectQuizCompareModal.getmSelectQuizQuesCompareResps());
                        mSelectQuizCompareModal.setmPercent(selectQuizCompareModal.getmPercent());
                        toggleView(false, false, null);
                    } else {
                        toggleView(false, true, errorMessage);
                    }
                }

            });
        }

        return mQuizHandler;
    }

    public void registerListener(Object listener) {
        if (listener != null && !(listener instanceof SelectQuizCompareListener)) {
            throw new IllegalArgumentException("listener should be an instance of " +
                    SelectQuizCompareListener.class.getSimpleName());
        }

        mSelectQuizCompareListener = (SelectQuizCompareListener) listener;
    }

    @Override
    public void onDestroyView() {
        getmQuizHandler().unregister();
        mUnbinder.unbind();
        super.onDestroyView();
    }

    public interface SelectQuizCompareListener {
        void closeFragment();
    }
}
