package com.trulymadly.android.app.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.adapter.VenueListAdapter;
import com.trulymadly.android.app.modal.VenueModal;

import java.util.ArrayList;

/**
 * Created by deveshbatra on 3/8/16.
 */

public class VenuesFragment extends Fragment {

    private Activity aActivity;

    private ArrayList<VenueModal> venueList;
    //    public static boolean isCreated = false;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        aActivity = getActivity();
        venueList = new ArrayList<>();
        String categoryId = getArguments().getString("categoryId");
        //noinspection unchecked
        this.venueList = (ArrayList<VenueModal>) getArguments().getSerializable("venueList");

//        isCreated  = true;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle paramBundle) {

        View venueFragmentView = inflater.inflate(
                R.layout.venue_list_layout, container, false);

//        createDummyData();
        View no_events_container = venueFragmentView.findViewById(R.id.no_events_container);
        TextView no_events_tv = (TextView) venueFragmentView.findViewById(R.id.no_events_tv);
        ImageView no_events_icon = (ImageView) venueFragmentView.findViewById(R.id.no_events_icon);
        RecyclerView venue_list_view = (RecyclerView) venueFragmentView.findViewById(R.id.venue_list_view);
        venue_list_view.setLayoutManager(new LinearLayoutManager(aActivity, LinearLayout.VERTICAL, false));
        VenueListAdapter venueListAdapter = new VenueListAdapter(aActivity, venueList);
        if (venueList == null || venueList.size() == 0) {

            Picasso.with(aActivity).load(R.drawable.no_events).into(no_events_icon);
            no_events_tv.setText(aActivity.getResources().getString(R.string.no_events_recommended));
            no_events_container.setVisibility(View.VISIBLE);
        } else {
            venue_list_view.setVisibility(View.VISIBLE);
            venue_list_view.setAdapter(venueListAdapter);
        }


        return venueFragmentView;
    }


    //
//    public void setVenueList(ArrayList<VenueModal> venueList) {
//        this.venueList = venueList;
//        if (venueListAdapter == null) {
//            venueListAdapter = new VenueListAdapter(aActivity, venueList);
//        } else {
//            venueListAdapter.setVenueList(this.venueList);
//            venueListAdapter.notifyDataSetChanged();
//        }
//
//    }
}


