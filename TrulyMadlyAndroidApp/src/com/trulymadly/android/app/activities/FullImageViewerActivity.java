package com.trulymadly.android.app.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.trulymadly.android.app.R;
import com.trulymadly.android.app.adapter.FullImageViewerPagerAdapter;
import com.trulymadly.android.app.listener.OnActionBarClickedInterface;
import com.trulymadly.android.app.utility.ActionBarHandler;
import com.trulymadly.android.app.utility.CirclePageIndicator;
import com.trulymadly.android.app.utility.Utility;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FullImageViewerActivity extends AppCompatActivity {
    public static final String IMAGES = "images";
    public static final String FOLDER_NAME = "folder_name";
    public static final String IS_ZOOMABLE = "is_zoomable";
    public static final String IS_CACHING_ENABLED = "is_caching_enabled";
    private static final String TITLE = "title";
    @BindView(R.id.fullview_pager)
    ViewPager mFullViewPager;
    @BindView(R.id.indicator)
    CirclePageIndicator mPagerIndicator;
    private boolean isZoomable = false;
    private boolean isCachingEnabled = false;
    private String[] mImages;
    private String mFolderName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_full_image_viewer);
        } catch (OutOfMemoryError | IllegalStateException ignored) {

        }
        ButterKnife.bind(this);
        processIntent(getIntent());
        Utility.disableScreenShot(this);

        OnActionBarClickedInterface onActionBarClickedInterface = new OnActionBarClickedInterface() {

            @Override
            public void onConversationsClicked() {
            }

            @Override
            public void onLocationClicked() {

            }

            @Override
            public void onCuratedDealsClicked() {

            }

            @Override
            public void onBackClicked() {
                try {
                    onBackPressed();
                } catch (IllegalStateException | NullPointerException ignored) {

                }
            }

            @Override
            public void onUserProfileClicked() {
            }

            @Override
            public void onTitleClicked() {
            }

            @Override
            public void onTitleLongClicked() {
            }

            @Override
            public void onSearchViewOpened() {

            }

            @Override
            public void onSearchViewClosed() {

            }

            @Override
            public void onCategoriesClicked() {

            }
        };

        ActionBarHandler mActionBarHandler = new ActionBarHandler(this, "", null, onActionBarClickedInterface, false, false, false);
        mActionBarHandler.setToolbarColor(Color.BLACK, "");

        FullImageViewerPagerAdapter mAdapter = new FullImageViewerPagerAdapter(this, mImages, isZoomable, isCachingEnabled, mFolderName);
        mFullViewPager.setAdapter(mAdapter);
        if (mImages == null || mImages.length <= 1) {
            mPagerIndicator.setVisibility(View.GONE);
        } else {
            mPagerIndicator.setViewPager(mFullViewPager);
        }
    }

    private void processIntent(Intent intent) {
        if (intent == null) {
            return;
        }

        String mTitle = intent.getStringExtra(TITLE);
        mImages = intent.getStringArrayExtra(IMAGES);
        mFolderName = intent.getStringExtra(FOLDER_NAME);
        isZoomable = intent.getBooleanExtra(IS_ZOOMABLE, false);
        isCachingEnabled = intent.getBooleanExtra(IS_CACHING_ENABLED, false);
    }

}
