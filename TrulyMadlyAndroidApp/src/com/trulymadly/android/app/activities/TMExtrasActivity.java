package com.trulymadly.android.app.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.billing.PaymentMode;
import com.trulymadly.android.app.billing.SparksBillingController;
import com.trulymadly.android.app.bus.BusProvider;
import com.trulymadly.android.app.fragments.BuySparkDialogFragment;
import com.trulymadly.android.app.fragments.BuySparkEventListener;
import com.trulymadly.android.app.fragments.TMExtrasFragment;
import com.trulymadly.android.app.modal.MySelectData;
import com.trulymadly.android.app.modal.SparkPackageModal;
import com.trulymadly.android.app.utility.ActionBarHandler;
import com.trulymadly.android.app.utility.SlidingMenuHandler;
import com.trulymadly.android.app.utility.TmLogger;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TMExtrasActivity extends AppCompatActivity {

    private final int TAG_PACKS = 0;
    private final int TAG_VOUCHERS = 1;

    @BindView(R.id.tm_extras_pager)
    ViewPager mViewPager;
    @BindView(R.id.tab_layout)
    TabLayout mTabLayout;
    private TMExtrasPagerAdapter mTMExtrasPagerAdapter;
    private SparksBillingController mSparksBillingController;
    private BuySparkEventListener mBuySparkEventListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tmextras);
        ButterKnife.bind(this);

        new ActionBarHandler(this, getResources().getString(R.string.tm_frills), new SlidingMenuHandler(this, SlidingMenu.TOUCHMODE_MARGIN), null,
                false, false, false);

        mBuySparkEventListener = new BuySparkEventListener() {
            @Override
            public void closeFragment() {

            }

            @Override
            public void onBuySparkClicked(Object packageModal, String matchId) {
                SparkPackageModal sparkPackageModal = (SparkPackageModal) packageModal;
                TmLogger.d("SparksBillingHandler", "Spark buy clicked in Adapter : " + sparkPackageModal.getmPackageSku());
//                mSparksBillingController.launchPurchaseFlow(PaymentMode.google, sparkPackageModal.getmPackageSku(), matchId);
//                mSparksBillingController.launchPurchaseFlow(PaymentMode.paytm, sparkPackageModal.getmPackageSku(), matchId,
//                        sparkPackageModal.getmPrice());
                mSparksBillingController.askForPaymentOption(sparkPackageModal.getmPackageSku(), matchId,
                        sparkPackageModal.getmSparkCount(), sparkPackageModal.getmPrice());
            }

            @Override
            public void onBuySparkSuccess(PaymentMode mPaymentMode, String sku, int newSparksAdded, int totalSparks, boolean isRelationshipExpertAdded, MySelectData mySelectData) {
                //TODO: SPARKS: show buy complete simple dialog fragment
            }

            @Override
            public void restorePurchasesClicked() {
                mSparksBillingController.restorePurchases(false);
            }

            @Override
            public void onRegistered() {
                mSparksBillingController.restorePurchases(true);
            }
        };

        mTMExtrasPagerAdapter = new TMExtrasPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mTMExtrasPagerAdapter);

        mViewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                mTabLayout.getTabAt(position).select();
            }
        });

        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.packs).setTag(TAG_PACKS));
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.vouchers).setTag(TAG_VOUCHERS));
        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem((Integer) tab.getTag(), true);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        mSparksBillingController = new SparksBillingController(this, null, mBuySparkEventListener);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mSparksBillingController != null && mSparksBillingController.handleActivityResult(requestCode, resultCode, data)) {
            TmLogger.d("SparksBillingHandler", "onActivityResult handled by SparkBillingHandler.");
            return;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mSparksBillingController != null) {
            mSparksBillingController.onDestroy();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        BusProvider.getInstance().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        BusProvider.getInstance().unregister(this);
    }

    public class TMExtrasPagerAdapter extends FragmentPagerAdapter {

        public TMExtrasPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            TMExtrasFragment fragment = TMExtrasFragment.getInstance(BuySparkDialogFragment.getSamplePackages(),
                    TMExtrasFragment.getDummyData());
            fragment.setBuySparkEventListener(mBuySparkEventListener);
            return fragment;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "OBJECT " + (position + 1);
        }
    }
}
