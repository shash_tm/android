package com.trulymadly.android.app.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.moe.pushlibrary.MoEHelper;
import com.squareup.picasso.Picasso;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventStatus;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.listener.ConfirmDialogInterface;
import com.trulymadly.android.app.listener.CustomOkHttpResponseHandler;
import com.trulymadly.android.app.listener.OnActionBarClickedInterface;
import com.trulymadly.android.app.utility.ActionBarHandler;
import com.trulymadly.android.app.utility.ActivityHandler;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.TMSelectHandler;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;
import com.trulymadly.android.socket.SocketHandler;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities.select;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes.select_serious_intent_nudge;

public class SettingsActivity extends AppCompatActivity implements
        OnCheckedChangeListener {

    @BindView(R.id.sound)
    SwitchCompat sound;
    @BindView(R.id.vibration)
    SwitchCompat vibration;
    @BindView(R.id.discovery)
    SwitchCompat discovery;
    @BindView(R.id.discovery_message_tv)
    View discovery_tv;
    @BindView(R.id.discovery_message_tv_on)
    View discovery_tv_on_state;
    @BindView(R.id.settings_sv)
    ScrollView mScrollView;
    @BindView(R.id.settings_footer_version)
    TextView version_tv;
    @BindView(R.id.profile_discovery_layout)
    View profile_discovery_layout;
    private Context aContext;
    private Activity aActivity;
    private MoEHelper mHelper = null;
    private ProgressDialog mProgressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            setContentView(R.layout.settings);
        } catch (OutOfMemoryError e) {
            System.gc();
            finish();
            return;
        }

        aContext = this;
        aActivity = this;
        mHelper = new MoEHelper(this);
        ButterKnife.bind(this);

        version_tv.setText(Utility.getAppVersionName(aContext) + " (" + Utility.getAppVersionCode(aContext) + ")"
                + (Constants.isLive ? "" : (Constants.isT1 ? " (T1) " : " (DEV) ")));
        version_tv.setTextColor(aContext.getResources().getColor(SocketHandler.isSocketEnabled(aContext) ? R.color.grey_text_color : R.color.black));

        Picasso.with(this)
                .load(R.drawable.right_arrow).into((ImageView) findViewById(R.id.help_terms_arrow));
        Picasso.with(this)
                .load(R.drawable.right_arrow).into((ImageView) findViewById(R.id.help_safety_arrow));
        Picasso.with(this)
                .load(R.drawable.right_arrow).into((ImageView) findViewById(R.id.help_trust_arrow));
        Picasso.with(this)
                .load(R.drawable.right_arrow).into((ImageView) findViewById(R.id.help_true_arrow));

        new ActionBarHandler(this, getResources().getString(R.string.settings), null, new OnActionBarClickedInterface() {
            @Override
            public void onBackClicked() {
                onBackPressed();
            }

            @Override
            public void onLocationClicked() {

            }

            @Override
            public void onUserProfileClicked() {

            }

            @Override
            public void onConversationsClicked() {

            }

            @Override
            public void onTitleClicked() {

            }

            @Override
            public void onTitleLongClicked() {

            }

            @Override
            public void onSearchViewOpened() {

            }

            @Override
            public void onSearchViewClosed() {

            }

            @Override
            public void onCategoriesClicked() {

            }

            @Override
            public void onCuratedDealsClicked() {

            }
        },
                false, false, false);
    }

    @OnClick(R.id.logout_button)
    void onLogoutClick() {
        Utility.logoutSession(aActivity);
    }

    @OnClick(R.id.delete_account)
    void onDeleteAccountClick() {
        if(TMSelectHandler.isSelectEnabled(this)) {
            TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyActivities.settings,
                    TrulyMadlyEventTypes.delete_click, 0, TrulyMadlyEventStatus.success, null, true);

            boolean isSelectMember = TMSelectHandler.isSelectMember(aContext);
            final Map<String, String> eventInfoSelect = new HashMap<>();
            eventInfoSelect.put("is_select_member", String.valueOf(isSelectMember));
            eventInfoSelect.put("source", "delete_profile");
            TrulyMadlyTrackEvent.trackEvent(aContext, select, select_serious_intent_nudge, 0,
                    TrulyMadlyEventStatus.view,
                    eventInfoSelect, true);
            AlertsHandler.showSimpleSelectDialog(aContext, R.string.select_nudge_text_delete_accnt,
                    R.string.learn_more, R.string.delete_caps, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            switch (view.getId()) {
                                case R.id.positive_button_tv:
                                    TrulyMadlyTrackEvent.trackEvent(aContext, select, select_serious_intent_nudge, 0,
                                            TrulyMadlyEventStatus.clicked_yes,
                                            eventInfoSelect, true);
                                    ActivityHandler.startTMSelectActivity(aContext, select_serious_intent_nudge + ":delete_profile", false);
                                    break;

                                case R.id.negative_button_tv:
                                    TrulyMadlyTrackEvent.trackEvent(aContext, select, select_serious_intent_nudge, 0,
                                            TrulyMadlyEventStatus.clicked_no,
                                            eventInfoSelect, true);

                                    TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyActivities.settings,
                                            TrulyMadlyEventTypes.delete_continue_click, 0,
                                            TrulyMadlyEventStatus.success, null, true);
                                    ActivityHandler.startDeleteAccountActivity(aContext);
                                    break;
                            }
                        }
                    });
        }else {
            ConfirmDialogInterface confirmDialogInterface = new ConfirmDialogInterface() {

                @Override
                public void onPositiveButtonSelected() {
                    TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyActivities.settings,
                            TrulyMadlyEventTypes.delete_continue_click, 0, TrulyMadlyEventStatus.success, null, true);

                    ActivityHandler.startDeleteAccountActivity(aContext);
                }

                @Override
                public void onNegativeButtonSelected() {
                }
            };

            AlertsHandler.showConfirmDialog(aContext, R.string.delete_account_warning_message,
                    R.string.continue_string,
                    R.string.cancel, confirmDialogInterface);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        sound.setChecked(SPHandler.getBool(aContext,
                Constants.SOUND_OPTION, true));

        vibration.setChecked(SPHandler.getBool(aContext,
                Constants.VIBRATION_OPTION, true));

        // user is female and shared perference is on
        if (SPHandler.getBool(aContext,
                Constants.SHOW_DISCOVERY_OPTION)
                && !Utility.isMale(aContext)) {
            profile_discovery_layout.setVisibility(
                    View.VISIBLE);
            discovery.setChecked(SPHandler.getBool(aContext,
                    Constants.DISCOVERY_OPTION, true));
            setlayout(discovery.isChecked());
        }

        if (SPHandler.getBool(aContext, ConstantsSP.SHARED_KEYS_PROFILE_VISIBILITY_MESSAGE)) {
            //Scrolling to the top before showing the message to show the PV option to the user.
            mScrollView.scrollTo(0, 0);
            AlertsHandler.showMessage(aActivity, R.string.da_profile_visibility_off_message, false);
            SPHandler.setBool(aContext, ConstantsSP.SHARED_KEYS_PROFILE_VISIBILITY_MESSAGE, false);
        }

        discovery.setOnCheckedChangeListener(this);
        sound.setOnCheckedChangeListener(this);
        vibration.setOnCheckedChangeListener(this);

    }

    @Override
    public void onPause() {
        discovery.setOnCheckedChangeListener(null);
        sound.setOnCheckedChangeListener(null);
        vibration.setOnCheckedChangeListener(null);
        super.onPause();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView,
                                 final boolean isChecked) {
        String event_type = null;
        switch (buttonView.getId()) {
            case R.id.sound:
                event_type = TrulyMadlyEventTypes.sound_option_toggled;
                SPHandler.setBool(aContext,
                        Constants.SOUND_OPTION, isChecked);
                break;
            case R.id.vibration:
                event_type = TrulyMadlyEventTypes.vibration_option_toggled;
                SPHandler.setBool(aContext,
                        Constants.VIBRATION_OPTION, isChecked);
                break;
            case R.id.discovery:
                event_type = TrulyMadlyEventTypes.discovery_option_toggled;
                CustomOkHttpResponseHandler okHttpResponseHandler = new CustomOkHttpResponseHandler(
                        aContext) {

                    @Override
                    public void onRequestSuccess(JSONObject responseJSON) {
                        SPHandler.setBool(aContext,
                                Constants.DISCOVERY_OPTION, isChecked);
                        mProgressDialog = UiUtils.hideProgressBar(mProgressDialog);
                        setlayout(isChecked);
                    }

                    @Override
                    public void onRequestFailure(Exception exception) {
                        mProgressDialog = UiUtils.hideProgressBar(mProgressDialog);
                        onSendFailed(isChecked, exception);
                    }
                };
                mProgressDialog = UiUtils
                        .showProgressBar(aContext, mProgressDialog);
                Utility.sendDiscoveryOptions(isChecked, aContext, okHttpResponseHandler);
                break;
            default:
                break;
        }

        TrulyMadlyTrackEvent.trackEvent(aContext,
                TrulyMadlyActivities.settings, event_type, 0,
                isChecked ? " yes" : "no", null, true);
    }

    private void onSendFailed(boolean isChecked, Exception exception) {
        discovery.setOnCheckedChangeListener(null);
        discovery.setChecked(!isChecked);
        discovery.setOnCheckedChangeListener(this);
        AlertsHandler.showNetworkError(aActivity, exception);
    }

    private void setlayout(Boolean isChecked) {
        discovery_tv.setVisibility(isChecked ? View.GONE : View.VISIBLE);
        discovery_tv_on_state.setVisibility(isChecked ? View.VISIBLE
                : View.GONE);
    }

    @OnClick(R.id.help_terms_layout)
    public void onClickTerms() {
        ActivityHandler.startTermsConditionsActivity(aContext);
    }


    @OnClick(R.id.help_safety_layout)
    public void onClickSafety() {
        ActivityHandler.startSafetyActivity(aContext);
    }

    @OnClick(R.id.help_trust_layout)
    public void onClickTrust() {
        ActivityHandler.startTrustSecurityActivity(aContext);
    }

    @OnClick(R.id.help_true_layout)
    public void onClickTrue() {
        ActivityHandler.startTrueCompatibilityActivity(aContext);
    }

    @OnClick(R.id.settings_help_desk)
    public void onClickHelpDesk() {
        ActivityHandler.startFaqsActivity(aContext);
    }

}
