package com.trulymadly.android.app.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.trulymadly.android.app.R;
import com.trulymadly.android.app.billing.PaymentMode;
import com.trulymadly.android.app.billing.SparksBillingController;
import com.trulymadly.android.app.fragments.BuySparkDialogFragmentNew;
import com.trulymadly.android.app.fragments.BuySparkEventListener;
import com.trulymadly.android.app.fragments.SimpleDialogFragment;
import com.trulymadly.android.app.listener.TrackingBuySparkEventListener;
import com.trulymadly.android.app.modal.MySelectData;
import com.trulymadly.android.app.modal.SparkPackageModal;
import com.trulymadly.android.app.utility.TmLogger;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BuyPackagesActivityNew extends AppCompatActivity implements BuySparkDialogFragmentNew.RootViewSetListener{

    @BindView(R.id.spark_dialog_container)
    View mSparkDialogContainer;

    private FragmentManager mFragmentManager;


    //    private View buy_sparks_fragment;
//    private GooglePlayBillingHandler googlePlayBillingHandler;
    private SparksBillingController mSparksBillingController;
    private Context aContext;
    private SimpleDialogFragment.SimpleDialogActionListener mSimpleDialogActionListener;
    private BuySparkDialogFragmentNew mBuySparkDialogFragment;
    private boolean isResumed = false;
    private TrackingBuySparkEventListener trackingBuySparkEventListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_buy_packages);

        ButterKnife.bind(this);
        this.aContext = this;

        mSimpleDialogActionListener = new SimpleDialogFragment.SimpleDialogActionListener() {
            @Override
            public void onActionButtonClicked(int action) {
                switch (action) {
                    case SimpleDialogFragment.SIMPLE_DIALOG_PURCHASE_COMPLETE:
                        finish();
                        break;

                }
            }

            @Override
            public void closeFragment() {
                finish();
            }
        };


        mFragmentManager = getSupportFragmentManager();

        BuySparkEventListener mBuySparkEventListener = new BuySparkEventListener() {
            @Override
            public void closeFragment() {
                finish();
            }

            @Override
            public void onBuySparkClicked(Object packageModal, String matchId) {
                SparkPackageModal sparkPackageModal = (SparkPackageModal) packageModal;
                TmLogger.d("SparksBillingHandler", "Spark buy clicked in Adapter : " + sparkPackageModal.getmPackageSku());
//                mSparksBillingController.launchPurchaseFlow(PaymentMode.google, sparkPackageModal.getmPackageSku(), matchId);
//                mSparksBillingController.launchPurchaseFlow(PaymentMode.paytm, sparkPackageModal.getmPackageSku(), matchId,
//                        sparkPackageModal.getmPrice());
                mSparksBillingController.askForPaymentOption(sparkPackageModal.getmPackageSku(), matchId,
                        sparkPackageModal.getmSparkCount(), sparkPackageModal.getmPrice());
            }

            @Override
            public void onBuySparkSuccess(PaymentMode mPaymentMode, String sku, int newSparksAdded, int totalSparks, boolean isRelationshipExpertAdded, MySelectData mySelectData) {
                if(isResumed) {
                    mBuySparkDialogFragment.setCloseActivityOnDismissed(false);
                    mBuySparkDialogFragment.dismiss();
                    SimpleDialogFragment mCurrentFragment = SimpleDialogFragment.newInstance(
                            SimpleDialogFragment.getSimpleDialogModal(aContext,
                                    SimpleDialogFragment.SIMPLE_DIALOG_PURCHASE_COMPLETE, newSparksAdded, totalSparks, isRelationshipExpertAdded));
                    mFragmentManager.beginTransaction().replace(R.id.spark_dialog_container, mCurrentFragment).commit();

                    mCurrentFragment.registerListener(mSimpleDialogActionListener);
                }
            }

            @Override
            public void restorePurchasesClicked() {
                mSparksBillingController.restorePurchases(false);
            }

            @Override
            public void onRegistered() {
                mSparksBillingController.restorePurchases(true);
            }
        };

        trackingBuySparkEventListener = new TrackingBuySparkEventListener(aContext, mBuySparkEventListener);

        mSparksBillingController = new SparksBillingController(this, null, trackingBuySparkEventListener);

        BuySparkDialogFragmentNew.BuySparkFragmentDataModal buySparkFragmentDataModal =
                new BuySparkDialogFragmentNew.BuySparkFragmentDataModal(null, false, 0, 0, 0, true, false, false, null);
        mBuySparkDialogFragment = BuySparkDialogFragmentNew.newInstance(buySparkFragmentDataModal);
        mBuySparkDialogFragment.show(getSupportFragmentManager(), mBuySparkDialogFragment.getTag());
        mBuySparkDialogFragment.registerListener(trackingBuySparkEventListener);

    }

    @Override
    protected void onResume() {
        isResumed = true;
        super.onResume();
    }

    @Override
    protected void onPause() {
        isResumed = false;
        super.onPause();
    }

    @Override
    protected void onStop() {
        overridePendingTransition(0, 0);
        super.onStop();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mSparksBillingController != null && mSparksBillingController.handleActivityResult(requestCode, resultCode, data)) {
            TmLogger.d("SparksBillingHandler", "onActivityResult handled by SparkBillingHandler.");
            return;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mBuySparkDialogFragment != null){
            mBuySparkDialogFragment.registerListener(null);
        }
        if (mSparksBillingController != null) {
            mSparksBillingController.onDestroy();
        }
    }

    @Override
    public void onRootViewSet(View view) {
        mSparksBillingController.setRootView(view);
    }
}
