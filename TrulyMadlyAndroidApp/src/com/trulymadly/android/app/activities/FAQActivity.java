package com.trulymadly.android.app.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.widget.ProgressBar;

import com.moe.pushlibrary.MoEHelper;
import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.GetHttpRequestParams;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.listener.OnActionBarClickedInterface;
import com.trulymadly.android.app.utility.ActionBarHandler;
import com.trulymadly.android.app.utility.ActivityHandler;
import com.trulymadly.android.app.utility.WebViewClientWithProgressBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FAQActivity extends AppCompatActivity {

    @BindView(R.id.faqWebView)
    WebView webView;
    @BindView(R.id.faqProgressBar)
    ProgressBar progress;
    @BindView(R.id.feedback_btn_container)
    View feedback_btn_container;
    private MoEHelper mhelper;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Activity aActivity = this;
        mhelper = new MoEHelper(this);
        if (savedInstanceState != null) {
            return;
        }

        setContentView(R.layout.faq_layout);
        ButterKnife.bind(this);

        new ActionBarHandler(this, getResources().getString(R.string.faqs),
                null, new OnActionBarClickedInterface() {
            @Override
            public void onBackClicked() {
                onBackPressed();
            }

            @Override
            public void onLocationClicked() {

            }

            @Override
            public void onUserProfileClicked() {

            }

            @Override
            public void onConversationsClicked() {

            }

            @Override
            public void onTitleClicked() {

            }

            @Override
            public void onTitleLongClicked() {

            }

            @Override
            public void onSearchViewOpened() {

            }

            @Override
            public void onSearchViewClosed() {

            }

            @Override
            public void onCategoriesClicked() {

            }

            @Override
            public void onCuratedDealsClicked() {

            }
        }, false, false, false);


        webView.setWebViewClient(new WebViewClientWithProgressBar(progress) {
            @Override
            public void onPageFinished(WebView view, String url) {
                feedback_btn_container.setVisibility(View.VISIBLE);
                super.onPageFinished(view, url);
            }
        });
        webView.getSettings().setJavaScriptEnabled(true);
        TrulyMadlyTrackEvent.trackEvent(aActivity,
                TrulyMadlyEvent.TrulyMadlyActivities.faq, TrulyMadlyEvent.TrulyMadlyEventTypes.page_load, 0,
                null, null, true);
        webView.loadUrl(ConstantsUrls.get_faq_url(), GetHttpRequestParams.getHeaders(this));
    }

    @OnClick(R.id.feedback_btn)
    public void onClickFeedback() {
        ActivityHandler.startMessageOneonOneActivity(this, null, null, false, true,
                false, false);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}
