package com.trulymadly.android.app.activities;

/**
 * Created by trulymadly on 07/08/16.
 */

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.VideoTrimmer;
import com.trulymadly.android.app.listener.OnTrimVideoListener;
import com.trulymadly.android.app.utility.FilesHandler;
import com.trulymadly.android.app.utility.TmLogger;
import com.trulymadly.android.app.utility.Utility;
import com.trulymadly.android.app.utility.VideoUtils;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;


public class TrimmerActivity extends AppCompatActivity implements OnTrimVideoListener {

    private static final String TAG = TrimmerActivity.class.getName();
    @BindView(R.id.timeLine)
    VideoTrimmer mVideoTrimmer;
    String videoSource = null, camera = null;
    private int orientationDegree = VideoUtils.VIDEO_ROTATION_DEFAULT;
    private String mOldPath, mNewPath;
    private boolean isVideoSaved = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trimmer);
        ButterKnife.bind(this);
        Intent extraIntent = getIntent();
        String path = "";

        if (extraIntent != null) {
            path = extraIntent.getStringExtra("path");
            orientationDegree = extraIntent.getIntExtra("rotate", VideoUtils.VIDEO_ROTATION_DEFAULT);
            videoSource = extraIntent.getStringExtra("source");
            camera = extraIntent.getStringExtra("camera");
            mOldPath = path;
        }

        if (!Utility.isSet(videoSource)) {
            videoSource = VideoUtils.VIDEO_SOURCE_DEFAULT;
        }
        if (!Utility.isSet(camera)) {
            camera = VideoUtils.VIDEO_CAMERA_DEFAULT;
        }


        if (mVideoTrimmer != null) {
            mVideoTrimmer.setActivity(this);
            mVideoTrimmer.setOnTrimVideoListener(this);
            mVideoTrimmer.setVideoURI(Uri.parse(path));
            mVideoTrimmer.setVideoInformationVisibility(true);
            mVideoTrimmer.setVideoSource(videoSource);
            mVideoTrimmer.setCamera(camera);
        }
    }

    @Override
    public void getResult(final Uri uri) {
        Intent intent = new Intent();
        mNewPath = uri.getPath();
        boolean isDeleted = false;
        if (Utility.isSet(mOldPath) && !mOldPath.equalsIgnoreCase(mNewPath)) {
            isDeleted = FilesHandler.deleteFileOrDirectory(mOldPath);
        }
        intent.putExtra("path", uri.getPath());
        intent.putExtra("isMuted", mVideoTrimmer.isMuted());
        intent.putExtra("rotate", orientationDegree);
        intent.putExtra("trim_status", "success");
        setResult(RESULT_OK, intent);
        isVideoSaved = true;
        mVideoTrimmer.destroy();
        finish();
    }

    @Override
    public void cancelAction() {
        closeTrimmerActivity(false);
    }

    @Override
    public void onBackPressed() {
        closeTrimmerActivity(true);
    }

    private void closeTrimmerActivity(boolean isFromBackPressed) {
        mVideoTrimmer.destroy();
        Map<String, String> eventInfo = new HashMap<>();
        eventInfo.put("source", videoSource);
        if (Utility.isSet(camera) && Utility.stringCompare(videoSource, VideoUtils.VIDEO_SOURCE_CAMERA)) {
            eventInfo.put("camera", camera);
        }
        eventInfo.put("page", "trim");
        eventInfo.put("back_pressed", String.valueOf(isFromBackPressed));
        eventInfo.put("recording_status", "after_recording");
        eventInfo.put("video_length_ms", String.valueOf(VideoUtils.getVideoDurationMs(mOldPath)));
        TrulyMadlyTrackEvent.trackEvent(this, TrulyMadlyEvent.TrulyMadlyActivities.photo, TrulyMadlyEvent.TrulyMadlyEventTypes.video_cancel, 0,
                null, eventInfo, true);
        Intent intent = new Intent();
        intent.putExtra("trim_status", "cancel");
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void errorOnTrim() {
        TmLogger.d(TAG, "error on trim");
        mVideoTrimmer.destroy();
        Intent intent = new Intent();
        intent.putExtra("trim_status", "error");
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    protected void onDestroy() {
        if (!isVideoSaved && Utility.isSet(mOldPath)) {
            FilesHandler.deleteFileOrDirectory(mOldPath);
        }
        super.onDestroy();
    }
}

