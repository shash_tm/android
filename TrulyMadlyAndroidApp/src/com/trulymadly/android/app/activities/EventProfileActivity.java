package com.trulymadly.android.app.activities;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.moe.pushlibrary.MoEHelper;
import com.squareup.picasso.Picasso;
import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.MessageOneonOneConversionActivity;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.adapter.MatchesPagesViewPagerAdapter2;
import com.trulymadly.android.app.adapter.ShareEventListAdapter;
import com.trulymadly.android.app.adapter.UserListAdapter;
import com.trulymadly.android.app.asynctasks.AreAppsInstalledAsyncTask;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.ConstantsRF;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.json.EventDetailsParser;
import com.trulymadly.android.app.listener.ConfirmDialogInterface;
import com.trulymadly.android.app.listener.CustomOkHttpResponseHandler;
import com.trulymadly.android.app.listener.OnActionBarClickedInterface;
import com.trulymadly.android.app.listener.OnActionBarMenuItemClickedInterface;
import com.trulymadly.android.app.listener.ShareEventUserPicClickedInterface;
import com.trulymadly.android.app.listener.TapToRetryListener;
import com.trulymadly.android.app.listener.UserPicClickedInterface;
import com.trulymadly.android.app.modal.ActiveConversationModal;
import com.trulymadly.android.app.modal.EventChatModal;
import com.trulymadly.android.app.modal.EventDetailModal;
import com.trulymadly.android.app.modal.ProfileViewPagerModal;
import com.trulymadly.android.app.modal.UserInfoModal;
import com.trulymadly.android.app.sqlite.MessageDBHandler;
import com.trulymadly.android.app.sqlite.RFHandler;
import com.trulymadly.android.app.utility.ActionBarHandler;
import com.trulymadly.android.app.utility.ActivityHandler;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.CirclePageIndicator;
import com.trulymadly.android.app.utility.OkHttpHandler;
import com.trulymadly.android.app.utility.TMSelectHandler;
import com.trulymadly.android.app.utility.TapToRetryHandler;
import com.trulymadly.android.app.utility.TmLogger;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes.chat_match;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes.chat_match_followed;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes.details_event;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes.details_event_followed;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes.follow_event;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes.phone_called;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes.phone_called_followed;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes.share_event;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes.share_event_followed;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes.share_match;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes.share_match_followed;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes.share_outside;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes.share_outside_followed;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes.unfollow_event;
import static com.trulymadly.android.app.activities.CategoriesActivity.SHOW_FOLLOWING_TAB;

/**
 * Created by deveshbatra on 3/17/16.
 */

public class EventProfileActivity extends AppCompatActivity implements View.OnClickListener, UserPicClickedInterface, ShareEventUserPicClickedInterface {

    private static final int LIMIT_OF_MUTUAL_MATCHES_TO_SHARE_EVENT = 1000;
    private static final String TAG = "EventProfileActivity";
    private static final int MAX_FOLLOW_TUTORIAL_SHOW_LIMIT = 2;
    private final int mRotationDuration = 500;
    @BindView(R.id.event_view_pager)
    ViewPager event_view_pager;
    @BindView(R.id.event_terms_layout)
    RelativeLayout event_terms_layout;
    @BindView(R.id.event_terms_container)
    LinearLayout event_terms_container;
    @BindView(R.id.event_terms_icon)
    ImageView event_terms_icon;
    //    @BindView(R.id.event_multiple_locations)
//    TextView event_multiple_location_tv;
    @BindView(R.id.event_address_icon)
    ImageView event_address_icon;
    //    @BindView(R.id.share_white_iv)
//    ImageView share_white_iv;
    @BindView(R.id.no_events_icon)
    ImageView no_events_icon;
    @BindView(R.id.event_expired_container)
    View event_expired_container;
    @BindView(R.id.no_events_tv)
    TextView no_events_tv;
    //    @BindView(R.id.custom_prog_bar_id)
//    View custom_prog_bar_id;
    @BindView(R.id.event_profile_main_view)
    View event_profile_main_view;
    @BindView(R.id.event_contact_no_container)
    View event_contact_no_container;
    @BindView(R.id.event_contact_no_icon)
    ImageView event_contact_no_icon;
    @BindView(R.id.action_button_tv)
    TextView action_button_tv;
    @BindView(R.id.event_details_icon)
    ImageView event_details_icon;
    @BindView(R.id.event_location_tv)
    TextView event_location_tv;
    @BindView(R.id.event_friendly_name_tv)
    TextView event_friendly_name_tv;
    @BindView(R.id.following_user_icon)
    ImageView following_user_icon;
    @BindView(R.id.following_user_tv)
    TextView following_user_tv;
    @BindView(R.id.event_address_tv)
    TextView event_address_tv;
    @BindView(R.id.event_contact_no_tv)
    TextView event_contact_no_tv;
    @BindView(R.id.event_details_tv)
    TextView event_details_tv;
    @BindView(R.id.event_details_description_tv)
    TextView event_details_description_tv;
    @BindView(R.id.fb_container)
    LinearLayout fb_container;
    @BindView(R.id.fb_tv)
    TextView fb_tv;
    @BindView(R.id.fb_iv)
    ImageView fb_iv;
    @BindView(R.id.follow_unfollow_iv)
    ImageView follow_unfollow_iv;
    @BindView(R.id.action_button_container)
    View action_button_container;
    @BindView(R.id.following_user_container)
    View following_user_container;
    @BindView(R.id.users_list_view)
    RecyclerView users_list_view;
    @BindView(R.id.follow_coach_mark)
    ImageView follow_coach_mark;
    @BindView(R.id.after_follow_coach_mark)
    View after_follow_coach_mark;
    @BindView(R.id.event_scrollview)
    View event_scrollview;
    @BindView(R.id.white_scrim_container)
    View white_scrim_container;
    @BindView(R.id.coach_mark_after_follow_iv)
    ImageView coach_mark_after_follow_iv;
    @BindView(R.id.price_buy_container)
    View price_buy_container;
    @BindView(R.id.share_event_list_view)

    RecyclerView share_event_list_view;
    @BindView(R.id.share_event_with_mutual_match_container)
    View share_event_with_mutual_match_container;
    @BindView(R.id.share_event_chats_hidden_container)
    View share_event_chats_hidden_container;
    @BindView(R.id.open_conv_list_iv)
    ImageView open_conv_list_iv;
    @BindView(R.id.share_screen_container)
    View share_screen_container;
    @BindView(R.id.share_event_via_whatsapp)
    View share_event_via_whatsapp;
    @BindView(R.id.share_event_via_messenger)
    View share_event_via_messenger;
    @BindView(R.id.share_event_via_hangout)
    View share_event_via_hangout;
    //    @BindView(R.id.uncheck_iv)
//    ImageView uncheck_iv;
//    @BindView(R.id.check_iv)
//    ImageView check_iv;
    @BindView(R.id.app_icon_whatsapp_iv)
    ImageView app_icon_whatsapp_iv;
    @BindView(R.id.app_icon_messenger_iv)
    ImageView app_icon_messenger_iv;
    @BindView(R.id.app_icon_hangouts_iv)
    ImageView app_icon_hangouts_iv;
    @BindView(R.id.share_others_iv)
    ImageView share_others_iv;
    //    @BindView(R.id.ticket_iv)
//    ImageView ticket_iv;
    @BindView(R.id.price_desc_tv)
    TextView price_desc_tv;
    @BindView(R.id.price_tv)
    TextView price_tv;
    //    @BindView(R.id.music_icon)
//    ImageView music_icon;
//    @BindView(R.id.face_icon)
//    ImageView face_icon;
    @BindView(R.id.event_multiple_locations)
    TextView event_multiple_locations;
    @BindView(R.id.event_location_list_framelayout)
    View event_location_list_framelayout;
    private Animation mShakeLeftToRightAnimation, mShakeRightToLeftAnimation;
    private MoEHelper mhelper = null;
    private Context aContext;
    private Activity aActivity;
    private LayoutInflater inflater;
    private String eventId, mStartedFrom, newEventId;
    private EventDetailModal eventDetailModal;
    private MatchesPagesViewPagerAdapter2 mAdapter;
    private boolean isFetched;
    private TapToRetryHandler tapToRetryHandler;
    private UserListAdapter mUserListAdapter;
    private ShareEventListAdapter mShareEventListAdapter;
    private ActionBarHandler actionBarHandler;
    private boolean isFollowed;
    private boolean initialStatus;
    private boolean isShareUiVisible = false;
    private boolean isWhatsappInstalled = false, isHangoutsInstalled = false, isFbMessangerInstalled = false, isChecked = false;
    private float mRotateFromValue = 0.0f, mRotateToValue = 180f, mRotateMidValue = 90f;
    private MultipleLocationsFragment localMultipleLocationListFragment;
    private android.support.v4.app.FragmentManager fragmentManager;
    private boolean isMultipleLocationsFragmentVisible;

    private Handler mHandler;
    private Runnable follow_coach_mark_hide_runnable, after_follow_coach_mark_hide_runnable;
    private AreAppsInstalledAsyncTask mAreAppsInstalledAsyncTask;
    private HashMap<String, String> event_info;
    private boolean ab_enabled;

    private String mYouAnd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_layout);

        //http://www.curious-creature.com/2012/12/01/android-performance-case-study/
        //For reducing overdraws : Suggested by Romain guy
        getWindow().setBackgroundDrawable(null);

        aContext = this;
        aActivity = this;
        ButterKnife.bind(this);
        processIntent(getIntent());

        mHandler = new Handler();
        mYouAnd = getString(R.string.you_and) + " ";

//        mFollowedDrawable = ActivityCompat.getDrawable(this, R.drawable.new_label);
//        mUnfollowedDrawable = ActivityCompat.getDrawable(this, R.drawable.note);

        inflater = (LayoutInflater) aActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mhelper = new MoEHelper(this);
        mAdapter = new MatchesPagesViewPagerAdapter2(aActivity, false, false, true, null);
//        Picasso.with(aContext).load(R.drawable.info).into(event_terms_icon);
//        Picasso.with(aContext).load(R.drawable.location_green).into(event_address_icon);
//        Picasso.with(aContext).load(R.drawable.details).into(event_details_icon);
        event_terms_icon.setImageResource(R.drawable.info_icon);
        event_address_icon.setImageResource(R.drawable.location);
        event_details_icon.setImageResource(R.drawable.note);

//        Picasso.with(aContext).load(R.drawable.call_icon).into(event_contact_no_icon);
//        Picasso.with(aContext).load(R.drawable.follow).into(check_iv);
//        Picasso.with(aContext).load(R.drawable.following).into(uncheck_iv);
//        Picasso.with(aContext).load(R.drawable.share_white).into(share_white_iv);
        Picasso.with(aContext).load(R.drawable.coach_mark_follow).into(follow_coach_mark);
        Picasso.with(aContext).load(R.drawable.app_icon_whatsapp).into(app_icon_whatsapp_iv);
        Picasso.with(aContext).load(R.drawable.app_icon_messenger).into(app_icon_messenger_iv);
        Picasso.with(aContext).load(R.drawable.app_icon_hangouts).into(app_icon_hangouts_iv);
        Picasso.with(aContext).load(R.drawable.share_others).into(share_others_iv);
        Picasso.with(aContext).load(R.drawable.coach_mark_after_follow).into(coach_mark_after_follow_iv);
        Picasso.with(aContext).load(R.drawable.expired).into(no_events_icon);
        Picasso.with(aContext).load(R.drawable.menu_conversations_material).into(open_conv_list_iv);
        Picasso.with(aContext).load(R.drawable.followed).fetch();
        Picasso.with(aContext).load(R.drawable.unfollowed).fetch();
        following_user_icon.setImageResource(R.drawable.followers);
        event_contact_no_icon.setImageResource(R.drawable.ic_phone);

        mShakeLeftToRightAnimation = AnimationUtils.loadAnimation(aContext, R.anim.shake_from_left_right);
        mShakeRightToLeftAnimation = AnimationUtils.loadAnimation(aContext, R.anim.shake_from_right_left);
        event_info = new HashMap<>();


        event_multiple_locations.setOnClickListener(this);
        action_button_container.setOnClickListener(this);
        follow_unfollow_iv.setOnClickListener(this);
        share_screen_container.setOnClickListener(this);
        price_buy_container.setOnClickListener(this);

        OnActionBarClickedInterface actionBarClickedInterface = new OnActionBarClickedInterface() {
            @Override
            public void onBackClicked() {
                onBackPressed();
            }

            @Override
            public void onLocationClicked() {

            }

            @Override
            public void onUserProfileClicked() {

            }

            @Override
            public void onConversationsClicked() {

            }

            @Override
            public void onTitleClicked() {

            }

            @Override
            public void onTitleLongClicked() {

            }

            @Override
            public void onSearchViewOpened() {

            }

            @Override
            public void onSearchViewClosed() {

            }

            @Override
            public void onCategoriesClicked() {

            }

            @Override
            public void onCuratedDealsClicked() {

            }
        };

        OnActionBarMenuItemClickedInterface onActionBarMenuItemClickedInterface = new OnActionBarMenuItemClickedInterface() {

            @Override
            public void onViewProfileClicked() {

            }

            @Override
            public void onDeleteSparkClicked() {

            }

            @Override
            public void onShareProfileClicked() {
                onShareClicked();
            }

            @Override
            public void onBlockUserClicked() {

            }

            @Override
            public void onUmatchUserClicked() {

            }

            @Override
            public void onClearChat() {

            }
        };

        ab_enabled = RFHandler.getBool(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_TM_SCENES_AB_ENABLED);
        if (ab_enabled) {
            event_info.put("tracking_key", RFHandler.getString(aContext, Utility.getMyId(aContext), ConstantsRF.AB_TRACKING_KEY));
        }
        event_info.put("is_select_member", String.valueOf(TMSelectHandler.isSelectMember(aContext)));
        actionBarHandler = new ActionBarHandler(this, aActivity.getResources().getString(R.string.event_details),
                null, actionBarClickedInterface, false, false, false, false, false);
        actionBarHandler.setOptionsMenuHandler(onActionBarMenuItemClickedInterface);
        actionBarHandler.toggleNotificationCenter(false);
        actionBarHandler.toggleShareView(true);
        actionBarHandler.toggleBackButton(true);
        actionBarHandler.setToolbarTransparent(false, aActivity.getResources().getString(R.string.event_details));
        event_contact_no_tv.setOnClickListener(this);
        event_contact_no_icon.setOnClickListener(this);
        tapToRetryHandler = new TapToRetryHandler(this, findViewById(android.R.id.content), new TapToRetryListener() {
            @Override
            public void reInitialize() {
                issueRequest();
            }
        }, "Loading...", false);

        String[] mAppsPackages = new String[]{Constants.packageWhatsapp,
                Constants.packageHangouts,
                Constants.packageFacebookmessenger};
        AreAppsInstalledAsyncTask.AreAppsInstalledInterface mAreAppsInstalledInterface = new AreAppsInstalledAsyncTask.AreAppsInstalledInterface() {
            @Override
            public void onAppsChecked(boolean[] installed) {
                isWhatsappInstalled = installed[0];
                isHangoutsInstalled = installed[1];
                isFbMessangerInstalled = installed[2];
                isChecked = true;
            }
        };
        fragmentManager = getSupportFragmentManager();
        mAreAppsInstalledAsyncTask = new AreAppsInstalledAsyncTask(aContext, mAreAppsInstalledInterface);
        mAreAppsInstalledAsyncTask.execute(mAppsPackages);
        issueRequest();
    }

    private boolean isChatsHidden() {
        return RFHandler.getBool(aContext, Utility.getMyId(aContext),
                ConstantsRF.RIGID_FIELD_CONVERSATIONS_HIDDEN);
    }

    private void processIntent(Intent intent) {
        if (intent == null) {
            return;
        }

        if (intent.hasExtra("eventId")) {
            eventId = intent.getStringExtra("eventId");
        }
        if (intent.hasExtra("from")) {
            mStartedFrom = intent.getStringExtra("from");
        }
    }

    private void showMainLayout() {
        event_scrollview.setVisibility(View.VISIBLE);
        white_scrim_container.setVisibility(View.VISIBLE);
        setCardView1();
        setCardView2();
        setCardView3();
        setCardView4();
        setFooter(eventDetailModal.getmPrice(), eventDetailModal.getmPriceDesc(), eventDetailModal.getEventTicketText());
        showFollowTutorial();
    }

    private void showFollowTutorial() {
        if (!SHOW_FOLLOWING_TAB) {
            return;
        }
        if (isFollowed) {
            return;
        }
        int shownCount = RFHandler.getInt(aContext, Utility.getMyId(aActivity), ConstantsRF.RIGID_FIELD_FOLLOW_TUTORIAL_SHOWN_COUNT, 0);
        if (shownCount < MAX_FOLLOW_TUTORIAL_SHOW_LIMIT) {
            RFHandler.insert(aContext, Utility.getMyId(aActivity), ConstantsRF.RIGID_FIELD_FOLLOW_TUTORIAL_SHOWN_COUNT, shownCount + 1);
            follow_coach_mark.setVisibility(View.VISIBLE);
            follow_coach_mark
                    .startAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_down_tutorial_first_like_hide));

            if (follow_coach_mark_hide_runnable == null) {
                follow_coach_mark_hide_runnable = new Runnable() {
                    @Override
                    public void run() {
                        follow_coach_mark.setVisibility(View.VISIBLE);
                        follow_coach_mark.startAnimation(
                                AnimationUtils.loadAnimation(aActivity, R.anim.slide_up_tutorial_first_like_hide));
                        follow_coach_mark.setVisibility(View.GONE);
                    }
                };
            }

            mHandler.postDelayed(follow_coach_mark_hide_runnable, 4000);
        }
    }

    private void showAfterFollowTutorial() {
        if (!RFHandler.getBool(aContext, Utility.getMyId(aActivity), ConstantsRF.RIGID_FIELD_AFTER_FOLLOW_TUTORIAL_SHOWN) && eventDetailModal.getUsers() != null && eventDetailModal.getUsers().size() > 0) {
            RFHandler.insert(aContext, Utility.getMyId(aActivity), ConstantsRF.RIGID_FIELD_AFTER_FOLLOW_TUTORIAL_SHOWN, true);

            after_follow_coach_mark.setVisibility(View.VISIBLE);
            after_follow_coach_mark
                    .startAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_down_tutorial_first_like_hide));

            if (after_follow_coach_mark_hide_runnable == null) {
                after_follow_coach_mark_hide_runnable = new Runnable() {
                    @Override
                    public void run() {
                        after_follow_coach_mark.setVisibility(View.VISIBLE);
                        after_follow_coach_mark.startAnimation(
                                AnimationUtils.loadAnimation(aActivity, R.anim.slide_up_tutorial_first_like_hide));
                        after_follow_coach_mark.setVisibility(View.GONE);
                    }
                };
            }

            mHandler.postDelayed(after_follow_coach_mark_hide_runnable, 4000);
        }

    }


    private void setCardView1() {

        //texviews
        if (Utility.isSet(eventDetailModal.getName())) {
            event_friendly_name_tv.setVisibility(View.VISIBLE);
            event_friendly_name_tv.setText(eventDetailModal.getName());
        }

        int locationCount = eventDetailModal.getLocations() == null ? 0 : eventDetailModal.getLocations().size();


        if (locationCount > 1 && Utility.isSet(eventDetailModal.getMultiLocationText())) {
            event_location_tv.setVisibility(View.VISIBLE);
            event_location_tv.setText(eventDetailModal.getMultiLocationText());
        } else if (Utility.isSet(eventDetailModal.getLocation()) && Utility.isSet(eventDetailModal.getDate())) {
            event_location_tv.setVisibility(View.VISIBLE);
            event_location_tv.setText(eventDetailModal.getDate() + ", " + eventDetailModal.getLocation());
        }
        //pager
        View.OnClickListener firstPicListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityHandler.startAlbumFullViewPagerForResult(aActivity, Integer.parseInt(view.getTag().toString()),
                        eventDetailModal.getImages(), null,
                        getResources().getString(R.string.photo_gallery), TrulyMadlyEvent.TrulyMadlyActivities.scenes, null);
            }
        };
        // setting height
        int widthPx = (int) UiUtils.getScreenWidth(aActivity);

        ViewGroup.LayoutParams params = event_view_pager.getLayoutParams();
        params.height = widthPx;
        event_view_pager.setLayoutParams(params);

        String urls[] = eventDetailModal.getImages();


        if (urls.length >= 2) {
            Utility.prefetchPhoto(aContext, urls[1], TrulyMadlyEvent.TrulyMadlyActivities.matches);
        }
        mAdapter.setListener(firstPicListener, firstPicListener);
        mAdapter.changeList(ProfileViewPagerModal.createImagesArrayFromImages(urls), null);
        event_view_pager.setAdapter(mAdapter);

        event_view_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                if (isFetched)
                    return;

                String urls[] = eventDetailModal.getImages();
                if (urls != null && urls.length <= 2)
                    return;

                Utility.prefetchPhotos(aActivity, urls, 2, TrulyMadlyEvent.TrulyMadlyActivities.scenes);
                isFetched = true;

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        CirclePageIndicator indicator = (CirclePageIndicator) findViewById(R.id.image_indicator);
        indicator.setStrokeColor(
                aActivity.getResources().getColor(R.color.default_circle_indicator_stroke_color_alternate));
        indicator
                .setFillColor(aActivity.getResources().getColor(R.color.default_circle_indicator_fill_color_alternate));
        indicator.setViewPager(event_view_pager);

        if (mAdapter.getCount() > 1) {
            indicator.setVisibility(View.VISIBLE);
        } else {
            indicator.setVisibility(View.GONE);
        }


        // users_list_view


        ArrayList<UserInfoModal> users = eventDetailModal.getUsers();

        if (SHOW_FOLLOWING_TAB && users != null && users.size() > 0) {
            if (isFollowed) {
                following_user_tv.setText(mYouAnd + eventDetailModal.getLikeMindedText().replaceAll("LIKE_COUNT", "" + users.size()));
            } else {
                following_user_tv.setText(eventDetailModal.getLikeMindedText().replaceAll("LIKE_COUNT", "" + users.size()));
            }
            following_user_container.setVisibility(View.VISIBLE);
            users_list_view.setHasFixedSize(true);
            users_list_view.setLayoutManager(new LinearLayoutManager(aActivity,
                    LinearLayoutManager.HORIZONTAL, false));

            mUserListAdapter = new UserListAdapter(aActivity, eventDetailModal.getUsers(), this, isFollowed);
            users_list_view.setAdapter(mUserListAdapter);
        } else {
            following_user_container.setVisibility(View.GONE);
        }
    }

    private void setCardView2() {
        if (Utility.isSet(eventDetailModal.getmFBUrl())) {
            fb_container.setVisibility(View.VISIBLE);
            fb_tv.setText(R.string.follow_event);
            fb_container.setOnClickListener(this);
            Picasso.with(aContext).load(R.drawable.facebookwhite).into(fb_iv);
        }
        event_details_description_tv.setText(eventDetailModal.getDescription());


    }


    private void setCardView3() {
        int locationCount = eventDetailModal.getLocations() == null ? 0 : eventDetailModal.getLocations().size();
        if (locationCount > 1) {
            event_multiple_locations.setVisibility(View.VISIBLE);
            event_address_icon.setVisibility(View.VISIBLE);
            event_address_tv.setVisibility(View.VISIBLE);
            event_address_tv.setText(locationCount + " " + getResources().getString(R.string.locations));

        } else {

            if (Utility.isSet(eventDetailModal.getPhoneNumber())) {
                event_contact_no_container.setVisibility(View.VISIBLE);
                event_contact_no_icon.setVisibility(View.VISIBLE);
                event_contact_no_tv.setVisibility(View.VISIBLE);
                event_contact_no_tv.setText(eventDetailModal.getPhoneNumber());
            }
            if (Utility.isSet(eventDetailModal.getAddress())) {
                event_address_icon.setVisibility(View.VISIBLE);
                event_address_tv.setVisibility(View.VISIBLE);
                event_address_tv.setText(eventDetailModal.getAddress());
            }
        }
    }

    private void setCardView4() {
        String[] terms = eventDetailModal.getTerms();

        if (terms != null && terms.length > 0) {
            event_terms_layout.setVisibility(View.VISIBLE);
            View term;
            TextView termText;
            event_terms_container.removeAllViews();
            for (String term1 : terms) {

                term = inflater.inflate(R.layout.terms_item_layout, null);
                termText = (TextView) term.findViewById(R.id.term_text_view);
                termText.setText(term1);
                event_terms_container.addView(term);
            }
        }
    }

    private void setFollowing(boolean animateFollow, boolean updateList) {
        if (SHOW_FOLLOWING_TAB) {
            follow_unfollow_iv.setVisibility(View.VISIBLE);
            Picasso.with(aContext).load((isFollowed) ? R.drawable.followed : R.drawable.unfollowed).into(follow_unfollow_iv);
            if (animateFollow) {
                float targetScale = 1f;
                if (isFollowed) {
                    targetScale = 0.8f;
                }
                follow_unfollow_iv.animate().scaleX(targetScale).scaleY(targetScale).setDuration(218).start();
            }

            price_buy_container.setVisibility(View.VISIBLE);

            if (updateList) {
                if (isFollowed) {
                    showAfterFollowTutorial();
                }
            }
            ArrayList<UserInfoModal> users = eventDetailModal.getUsers();
            if (users != null && users.size() > 0) {
                if (isFollowed) {
                    following_user_tv.setText(mYouAnd + eventDetailModal.getLikeMindedText().replaceAll("LIKE_COUNT", "" + users.size()));
                } else {
                    following_user_tv.setText(eventDetailModal.getLikeMindedText().replaceAll("LIKE_COUNT", "" + users.size()));
                }
            }
        } else {
            follow_unfollow_iv.setVisibility(View.GONE);
        }
    }

    private void setFooter(int price, String priceDesc, String actionString) {
        String priceString = null;
        if (price > 0) {
            priceString = String.format(getString(R.string.ruppee_value), String.valueOf(price));
        } else {
            priceString = getString(R.string.free);
        }

        price_tv.setText(priceString);
        if (eventDetailModal.isSelectOnly()) {
            price_desc_tv.setVisibility(View.VISIBLE);
            price_desc_tv.setText(Html.fromHtml(String.valueOf(this.getText(R.string.exclusively_for_select))));
            price_desc_tv.setTextColor(getResources().getColor(R.color.black));
        } else if (Utility.isSet(priceDesc)) {
            price_desc_tv.setVisibility(View.VISIBLE);
            price_desc_tv.setText(priceDesc);
            price_desc_tv.setTextColor(getResources().getColor(R.color.red));
        } else {
            price_desc_tv.setVisibility(View.GONE);
        }
        action_button_tv.setText(actionString);
    }

    @Override
    public void onShareEventUserPicClicked(
            final ActiveConversationModal activeConversationItem) {
        if (activeConversationItem != null) {
            hideShareUi();

            AlertsHandler.showConfirmDialog(this, getResources().getString(R.string.event_share_confirmation_msg) + " " + activeConversationItem.getFname() + "?",
                    R.string.event_share_confirmation_yes, R.string.event_share_confirmation_no,
                    new ConfirmDialogInterface() {
                        @Override
                        public void onPositiveButtonSelected() {
                            shareEventWithMatch(activeConversationItem);
                        }

                        @Override
                        public void onNegativeButtonSelected() {

                        }
                    }, true);
        }
    }

    private void shareEventWithMatch(ActiveConversationModal activeConversationItem) {
        Map<String, String> event_info = new HashMap<>();
        event_info.put("match_id", activeConversationItem.getMatch_id());
        if (ab_enabled)
            event_info.put("tracking_key", RFHandler.getString(aContext, Utility.getMyId(aContext), ConstantsRF.AB_TRACKING_KEY));

        TrulyMadlyTrackEvent.trackEvent(aActivity,
                TrulyMadlyEvent.TrulyMadlyActivities.scenes, isFollowed ? share_match_followed : share_match, 0,
                eventId, event_info, true);
        ActivityHandler.startMessageOneOnOneActivity(aContext, activeConversationItem.getMatch_id(),
                activeConversationItem.getMessage_link(), new EventChatModal(
                        eventId, eventDetailModal.getName(), eventDetailModal.getImages()[0]));
    }

    public void onShareClicked() {
        if (Utility.isSet(eventId)) {
            TrulyMadlyTrackEvent.trackEvent(aActivity,
                    TrulyMadlyEvent.TrulyMadlyActivities.scenes, isFollowed ? share_event_followed : share_event, 0,
                    eventId, event_info, true);
            showShareUi(MessageDBHandler.getActiveConversations(aContext, Utility.getMyId(aContext), LIMIT_OF_MUTUAL_MATCHES_TO_SHARE_EVENT));
        } else {
            TmLogger.e(TAG, "No eventId to Share");
        }
    }

    @OnClick(R.id.share_event_chats_hidden_container)
    public void onConvListIconClicked() {
        hideShareUi();
        ActivityHandler.startConversationListActivity(aContext);
    }

    private void showShareUi(ArrayList<ActiveConversationModal> activeConversations) {
        share_screen_container.setVisibility(View.VISIBLE);
        isShareUiVisible = true;
        if (activeConversations.size() > 0) {
            if (!isChatsHidden()) {
                share_event_chats_hidden_container.setVisibility(View.GONE);
                share_event_with_mutual_match_container.setVisibility(View.VISIBLE);
                share_event_list_view.setHasFixedSize(true);
                share_event_list_view.setLayoutManager(new LinearLayoutManager(aActivity,
                        LinearLayoutManager.HORIZONTAL, false));

                if (mShareEventListAdapter == null) {
                    mShareEventListAdapter = new ShareEventListAdapter(aActivity, activeConversations, this);
                    share_event_list_view.setAdapter(mShareEventListAdapter);
                } else {
                    mShareEventListAdapter.updateActiveConversations(activeConversations);
                }
            } else {
                share_event_with_mutual_match_container.setVisibility(View.GONE);
                share_event_chats_hidden_container.setVisibility(View.VISIBLE);
            }
        } else {
            share_event_with_mutual_match_container.setVisibility(View.GONE);
            share_event_chats_hidden_container.setVisibility(View.GONE);
        }

        if (!isChecked) {
            isWhatsappInstalled = ActivityHandler.appInstalledOrNot(aContext, Constants.packageWhatsapp);
            isHangoutsInstalled = ActivityHandler.appInstalledOrNot(aContext, Constants.packageHangouts);
            isFbMessangerInstalled = ActivityHandler.appInstalledOrNot(aContext, Constants.packageFacebookmessenger);
            mAreAppsInstalledAsyncTask.cancel(true);
            isChecked = true;
        }

        share_event_via_whatsapp.setVisibility(isWhatsappInstalled ? View.VISIBLE : View.GONE);
        share_event_via_hangout.setVisibility(isHangoutsInstalled ? View.VISIBLE : View.GONE);
        share_event_via_messenger.setVisibility(isFbMessangerInstalled ? View.VISIBLE : View.GONE);
    }

    @OnClick(R.id.share_event_via_whatsapp)
    public void shareEventViaWhatsapp() {
        shareEventViaApp(Constants.packageWhatsapp);
    }

    @OnClick(R.id.share_event_via_hangout)
    public void shareEventViaHangout() {
        shareEventViaApp(Constants.packageHangouts);
    }

    @OnClick(R.id.share_event_via_messenger)
    public void shareEventViaMessenger() {
        shareEventViaApp(Constants.packageFacebookmessenger);
    }

    @OnClick(R.id.share_event_via_other)
    public void shareEventViaOther() {
        shareEventViaApp(null);
    }

    private void shareEventViaApp(String packageName) {
        hideShareUi();
        Map<String, String> params = new HashMap<>();

        if (ab_enabled)
            params.put("tracking_key", RFHandler.getString(aContext, Utility.getMyId(aContext), ConstantsRF.AB_TRACKING_KEY));

        params.put("app name", Utility.isSet(packageName) ? packageName : "other");
        TrulyMadlyTrackEvent.trackEvent(aActivity,
                TrulyMadlyEvent.TrulyMadlyActivities.scenes, isFollowed ? share_outside_followed : share_outside, 0,
                eventId, params, true);

        if (Utility.isSet(packageName)) {
            ActivityHandler.createTextIntentForApp(aActivity, getShareText(), packageName, null, false);
        } else {
            ActivityHandler.createTextIntent(aActivity, getShareText(), false);
        }
    }

    private String getShareText() {
        return eventDetailModal.getShareText() + " " + eventDetailModal.getShareUrl();
    }

    private void hideShareUi() {
        isShareUiVisible = false;
        share_screen_container.setVisibility(View.GONE);
    }

    private void issueRequest() {


        final long startTime = System.currentTimeMillis();
        CustomOkHttpResponseHandler okHttpResponseHandler = new CustomOkHttpResponseHandler(aActivity) {


            @Override
            public void onRequestSuccess(JSONObject responseJSON) {
                long endTime = System.currentTimeMillis();
                if (tapToRetryHandler != null) {
                    tapToRetryHandler.onSuccessFull();
                    mShakeLeftToRightAnimation.cancel();
                    mShakeRightToLeftAnimation.cancel();
                }

                eventDetailModal = EventDetailsParser.parseEventDetailsResponse(responseJSON);
                isFollowed = eventDetailModal.isFollowStatus();
                initialStatus = isFollowed;
                if (eventDetailModal.isEventExpired()) {
                    actionBarHandler.setToolbarTransparent(false, eventDetailModal.getName());
                    no_events_tv.setText(aActivity.getResources().getString(R.string.event_expired_message));
                    event_expired_container.setVisibility(View.VISIBLE);
                } else {
                    showMainLayout();
                    setFollowing(false, true);
                    if (eventDetailModal.isFollowStatus()) {
                        float targetScale = 0.8f;
                        follow_unfollow_iv.animate().scaleX(targetScale).scaleY(targetScale).setDuration(0).start();
                    }
                    actionBarHandler.setToolbarTransparent(false, aActivity.getResources().getString(R.string.event_details));
                }

                HashMap<String, String> eventInfo = new HashMap<>();

                int noOfMutualMatches = 0, noOfUsersLikedByMe = 0, noOfUsersSparkedByMe = 0;
                if (eventDetailModal.getUsers() != null) {
                    ArrayList<UserInfoModal> users = eventDetailModal.getUsers();
                    for (int i = 0; i < users.size(); i++) {
                        if (users.get(i).isMutualMatch()) {
                            noOfMutualMatches++;
                        } else if (users.get(i).isLikedByMe()) {
                            noOfUsersLikedByMe++;
                        } else if (users.get(i).isSparkedByMe()) {
                            noOfUsersSparkedByMe++;
                        }

                    }
                    eventInfo.put("mutual_matches", "" + noOfMutualMatches);
                    eventInfo.put("profiles_liked", "" + noOfUsersLikedByMe);
                    eventInfo.put("profiles_sparked", "" + noOfUsersSparkedByMe);
                    eventInfo.put("attendee_list", "" + (users.size() - noOfMutualMatches - noOfUsersLikedByMe));
                }

                if (ab_enabled) {
                    eventInfo.put("tracking_key", RFHandler.getString(aContext, Utility.getMyId(aContext), ConstantsRF.AB_TRACKING_KEY));
                }

                TrulyMadlyTrackEvent.trackEvent(aActivity,
                        TrulyMadlyEvent.TrulyMadlyActivities.scenes, isFollowed ? details_event_followed : details_event, endTime - startTime,
                        eventId, eventInfo, true);


            }

            @Override
            public void onRequestFailure(Exception exception) {
                long endTime = System.currentTimeMillis();
                HashMap<String, String> eventInfo = new HashMap<>();
                eventInfo.put("event_id", eventId);
                if (ab_enabled)
                    eventInfo.put("tracking_key", RFHandler.getString(aContext, Utility.getMyId(aContext), ConstantsRF.AB_TRACKING_KEY));

                TrulyMadlyTrackEvent.trackEvent(aActivity,
                        TrulyMadlyEvent.TrulyMadlyActivities.scenes, isFollowed ? details_event_followed : details_event, endTime - startTime,
                        TrulyMadlyEvent.TrulyMadlyEventStatus.error, eventInfo, true);
                if (tapToRetryHandler != null) {
                    tapToRetryHandler.onNetWorkFailed(exception);
                    mShakeLeftToRightAnimation.cancel();
                    mShakeRightToLeftAnimation.cancel();
                }
            }
        };
        HashMap<String, String> params = new HashMap<>();
        params.put("datespot_id", eventId);
        params.put("action", "event_details");
        params.put("user_id", Utility.getMyId(aContext));

        if (tapToRetryHandler != null) {
//            music_icon.startAnimation(mShakeLeftToRightAnimation);
//            face_icon.startAnimation(mShakeRightToLeftAnimation);
            tapToRetryHandler.showLoader();
        }

        OkHttpHandler.httpPost(aContext, ConstantsUrls.get_category_url(), params,
                okHttpResponseHandler);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            if (requestCode == Constants.LIKE_OR_HIDE_OF_SINGLE_MATCH) {

                Bundle b = data.getExtras();
                if (b != null) {
                    UserInfoModal user = (UserInfoModal) b.getSerializable("userInfoModal");
                    switch (resultCode) {
                        case Constants.LIKE_SINGLE_MATCH:
                            AlertsHandler.showMessage(aActivity, R.string.like_back_message, false);
                            putCheckMark(user);
                            break;
                        case Constants.HIDE_SINGLE_MATCH:
                            AlertsHandler.showMessage(aActivity, R.string.hide_single_match_happening, false);
                            deleteFromList(user);
                            break;
                        case Constants.MUTUAL_MATCH:
                            AlertsHandler.showMessage(aActivity, R.string.mutual_match_happening, false);
                            setItAsMutualMatch(user);
                            break;
                        case Constants.SPARK_SINGLE_MATCH:
                            AlertsHandler.showMessage(aActivity, R.string.spark_like_back_message, false);
                            putSparkMark(user);
                            break;
                    }
                }
            }
        }
    }

    private void putCheckMark(UserInfoModal user) {
        ArrayList<UserInfoModal> users = eventDetailModal.getUsers();
        int i;
        for (i = 0; i < users.size(); i++) {
            if (user.getId().equalsIgnoreCase(users.get(i).getId()))
                break;
        }
        if (i != users.size()) {
            users.get(i).setLikedByMe(true);
            eventDetailModal.setUsers(users);
            mUserListAdapter.setUserList(users);
            mUserListAdapter.notifyDataSetChanged();

        }
    }

    private void putSparkMark(UserInfoModal user) {
        ArrayList<UserInfoModal> users = eventDetailModal.getUsers();
        int i;
        for (i = 0; i < users.size(); i++) {
            if (user.getId().equalsIgnoreCase(users.get(i).getId()))
                break;
        }
        if (i != users.size()) {
            users.get(i).setSparkedByMe(true);
            eventDetailModal.setUsers(users);
            mUserListAdapter.setUserList(users);
            mUserListAdapter.notifyDataSetChanged();
        }
    }

    private void setItAsMutualMatch(UserInfoModal user) {

        ArrayList<UserInfoModal> users = eventDetailModal.getUsers();
        int i;
        for (i = 0; i < users.size(); i++) {
            if (user.getId().equalsIgnoreCase(users.get(i).getId()))
                break;
        }
        if (i != users.size()) {
            UserInfoModal u = users.get(i);
            u.setIsMutualMatch(true);
            u.setMessageUrl(user.getMessageUrl());
            u.setName(user.getName());
            eventDetailModal.setUsers(users);
            mUserListAdapter.setUserList(users);
            mUserListAdapter.notifyDataSetChanged();

        }

    }

    private void deleteFromList(UserInfoModal user) {
        ArrayList<UserInfoModal> users = eventDetailModal.getUsers();
        int i;
        for (i = 0; i < users.size(); i++) {
            if (user.getId().equalsIgnoreCase(users.get(i).getId()))
                break;
        }
        if (i != users.size()) {
            users.remove(i);
            if (users != null && users.size() > 0) {
                eventDetailModal.setUsers(users);
                mUserListAdapter.setUserList(users);
                mUserListAdapter.notifyDataSetChanged();
                following_user_container.setVisibility(View.VISIBLE);

                if (isFollowed) {
                    following_user_tv.setText(mYouAnd + eventDetailModal.getLikeMindedText().replaceAll("LIKE_COUNT", "" + users.size()));
                } else {
                    following_user_tv.setText(eventDetailModal.getLikeMindedText().replaceAll("LIKE_COUNT", "" + users.size()));
                }

            } else {
                following_user_container.setVisibility(View.GONE);
            }


        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent != null && intent.hasExtra("eventId"))
            newEventId = intent.getStringExtra("eventId");

        if (!Utility.stringCompare(newEventId, eventId)) {
            processIntent(intent);
            event_scrollview.setVisibility(View.GONE);
            white_scrim_container.setVisibility(View.GONE);
            issueRequest();
        }
        newEventId = null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.price_buy_container:
                break;
            case R.id.share_screen_container:
                hideShareUi();
                break;
            case R.id.fb_container:
                TrulyMadlyTrackEvent.trackEvent(aActivity,
                        TrulyMadlyEvent.TrulyMadlyActivities.scenes,
                        !isFollowed ? TrulyMadlyEvent.TrulyMadlyEventTypes.follow_fb :
                                TrulyMadlyEvent.TrulyMadlyEventTypes.follow_fb_followed, 0,
                        eventId, event_info, true);
                Intent intentFBChrome = new Intent(Intent.ACTION_VIEW, Uri.parse(eventDetailModal.getmFBUrl()));
                intentFBChrome.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intentFBChrome.setPackage("com.android.chrome");
                try {
                    aActivity.startActivity(intentFBChrome);
                } catch (ActivityNotFoundException ex) {
                    // Chrome browser presumably not installed so allow user to choose instead
                    intentFBChrome.setPackage(null);
                    aActivity.startActivity(intentFBChrome);
                }
                break;
            case R.id.action_button_container:
                TrulyMadlyTrackEvent.trackEvent(aActivity,
                        TrulyMadlyEvent.TrulyMadlyActivities.scenes,
                        !isFollowed ? TrulyMadlyEvent.TrulyMadlyEventTypes.get_tickets :
                                TrulyMadlyEvent.TrulyMadlyEventTypes.get_tickets_followed, 0,
                        eventId, event_info, true);
                if (eventDetailModal.isSelectOnly() && !TMSelectHandler.isSelectMember(aContext)) {
                    ActivityHandler.startTMSelectActivity(aContext, "scenes_detail_cta", false);
                } else {
                    Intent intentTicketChrome = new Intent(Intent.ACTION_VIEW, Uri.parse(eventDetailModal.getTicketUrl()));
                    intentTicketChrome.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intentTicketChrome.setPackage("com.android.chrome");
                    try {
                        aActivity.startActivity(intentTicketChrome);
                    } catch (ActivityNotFoundException ex) {
                        // Chrome browser presumably not installed so allow user to choose instead
                        intentTicketChrome.setPackage(null);
                        aActivity.startActivity(intentTicketChrome);
                    }
                }
                break;
            case R.id.follow_unfollow_iv:
                if (Utility.isNetworkAvailable(this)) {
                    if (isFollowed) {
                        AlertsHandler.showConfirmDialog(aContext, R.string.unfollow_alert_text, R.string.unfollow, R.string.cancel, new ConfirmDialogInterface() {
                            @Override
                            public void onPositiveButtonSelected() {
                                makeCallToFollow();
                                isFollowed = !isFollowed;
                                setFollowing(true, false);
                            }

                            @Override
                            public void onNegativeButtonSelected() {

                            }
                        });

                    } else {
                        makeCallToFollow();
                        isFollowed = !isFollowed;
                        setFollowing(true, false);
                    }
                } else {
                    AlertsHandler.showMessage(aActivity, R.string.ERROR_NETWORK_FAILURE);
                }
                break;
            case R.id.event_contact_no_tv:
            case R.id.event_contact_no_icon:
                TrulyMadlyTrackEvent.trackEvent(aActivity,
                        TrulyMadlyEvent.TrulyMadlyActivities.scenes, !isFollowed ? phone_called : phone_called_followed, 0,
                        eventId, event_info, true);
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + eventDetailModal.getPhoneNumber()));
                startActivity(intent);
                break;
            case R.id.event_multiple_locations:

                launchLocationListFragment();
                break;


        }

    }


    private void launchLocationListFragment() {
        try {

            actionBarHandler.toggleNotificationCenter(false);
            event_profile_main_view.setVisibility(View.GONE);
            event_location_list_framelayout.setVisibility(View.VISIBLE);
            localMultipleLocationListFragment = new MultipleLocationsFragment();
            Bundle data = new Bundle();
            data.putParcelableArrayList("locations",
                    eventDetailModal.getLocations());
            data.putString("dealId", Utility.isSet(eventId) ? eventId : eventDetailModal.getId());
            localMultipleLocationListFragment.setArguments(data);
            localMultipleLocationListFragment.setTrkActivity(TrulyMadlyEvent.TrulyMadlyActivities.scenes);
            actionBarHandler.setToolbarTransparent(false, eventDetailModal.getName());


            fragmentManager.beginTransaction()
                    .replace(R.id.event_location_list_framelayout,
                            localMultipleLocationListFragment,
                            MultipleLocationsFragment.class.getSimpleName())
                    .commitAllowingStateLoss();

            isMultipleLocationsFragmentVisible = true;

        } catch (IllegalStateException ignored) {
        }
    }

    private void makeCallToFollow() {
        CustomOkHttpResponseHandler okHttpResponseHandler = new CustomOkHttpResponseHandler(aContext, TrulyMadlyEvent.TrulyMadlyActivities.scenes, !isFollowed ? follow_event : unfollow_event, eventId) {
            @Override
            public void onRequestSuccess(JSONObject responseJSON) {
                setFollowing(false, true);
            }

            @Override
            public void onRequestFailure(Exception exception) {
                isFollowed = !isFollowed;
                setFollowing(true, false);
                if (!isFollowed) {
                    AlertsHandler.showMessage(aActivity, R.string.follow_req_failed, false);
                }
            }
        };
        HashMap<String, String> params = new HashMap<>();
        params.put("datespot_id", eventId);
        params.put("action", isFollowed ? "unfollow_event" : "follow_event");
        OkHttpHandler.httpPost(aContext, ConstantsUrls.get_category_url(), params,
                okHttpResponseHandler);


    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onUserPicClicked(UserInfoModal user) {
        if (user.isMutualMatch()) {
            HashMap<String, String> eventInfo = new HashMap<>();
            eventInfo.put("match_id", user.getId());
            TrulyMadlyTrackEvent.trackEvent(aActivity,
                    TrulyMadlyEvent.TrulyMadlyActivities.scenes, !isFollowed ? chat_match : chat_match_followed, 0,
                    eventId, eventInfo, true);

            ActivityHandler.startMessageOneonOneActivity(aContext, user.getId(), user.getMessageUrl(), false, false, false, false);
        } else {
            int size;
            try {
                size = checkNotNull(eventDetailModal.getUsers()).size();
            } catch (NullPointerException e) {
                size = 0;
            }
            ActivityHandler.startSingleMatchActivityForResult(aActivity, user, eventId, size);
        }
//        else if (isFollowed) {
//            int size;
//            try {
//                size = checkNotNull(eventDetailModal.getUsers()).size();
//            } catch (NullPointerException e) {
//                size = 0;
//            }
//            ActivityHandler.startSingleMatchActivityForResult(aActivity, user, eventId, size);
//        } else {
//            HashMap<String, String> eventInfo = new HashMap<>();
//            eventInfo.put("matchId", "" + user.getId());
//            TrulyMadlyTrackEvent.trackEvent(aActivity,
//                    TrulyMadlyEvent.TrulyMadlyActivities.scenes, TrulyMadlyEvent.TrulyMadlyEventTypes.disabled_match, 0, eventId
//                    , eventInfo, true);
//
//            AlertsHandler.showAlertDialog(aActivity, R.string.follow_alert);
//        }
    }

    @Override
    protected void onDestroy() {
        if (mHandler != null) {
            mHandler.removeCallbacks(follow_coach_mark_hide_runnable);
            mHandler.removeCallbacks(after_follow_coach_mark_hide_runnable);
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (isShareUiVisible) {
            hideShareUi();
        } else if (isMultipleLocationsFragmentVisible) {
            actionBarHandler.setToolbarTransparent(false, getString(R.string.datespots));
            actionBarHandler.toggleNotificationCenter(false);
            event_profile_main_view.setVisibility(View.VISIBLE);
            event_location_list_framelayout.setVisibility(View.GONE);
            isMultipleLocationsFragmentVisible = false;
            fragmentManager.beginTransaction().remove(localMultipleLocationListFragment)
                    .commitAllowingStateLoss();
        } else if (MessageOneonOneConversionActivity.ACTIVITY_NAME.equals(mStartedFrom)) {
            super.onBackPressed();
        } else if (!CategoriesActivity.isCreated) {
            ActivityHandler.startMatchesActivity(aContext);
            finish();
        } else if (initialStatus != isFollowed) {
            if (isFollowed)
                setResult(Constants.EVENT_FOLLOWED);
            else
                setResult(Constants.EVENT_UNFOLLOWED);
            finish();
        } else {
            super.onBackPressed();
        }
    }

    private class GetActiveConversationsAsyncTask extends AsyncTask<Void, Void, ArrayList<ActiveConversationModal>> {

        private final Context ctx;

        public GetActiveConversationsAsyncTask(Context aContext) {
            TmLogger.e(TAG, "Get Conversation init");
            this.ctx = aContext;
        }

        @Override
        protected ArrayList<ActiveConversationModal> doInBackground(Void... params) {
            TmLogger.e(TAG, "Get Conversation background task");
            return MessageDBHandler.getActiveConversations(ctx, Utility.getMyId(ctx), LIMIT_OF_MUTUAL_MATCHES_TO_SHARE_EVENT);
        }

        @Override
        protected void onPostExecute(ArrayList<ActiveConversationModal> activeConversationModals) {
            TmLogger.e(TAG, "Get Conversation complete : " + activeConversationModals.toString());
            showShareUi(activeConversationModals);
        }
    }


}
