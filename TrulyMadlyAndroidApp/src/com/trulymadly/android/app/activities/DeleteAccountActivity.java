package com.trulymadly.android.app.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.moe.pushlibrary.MoEHelper;
import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.HttpTasks;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.fragments.DeleteAccountReasonsFragment;
import com.trulymadly.android.app.fragments.DeleteAccountReasonsFragment.OnDeleteAccountReasonsClickedListener;
import com.trulymadly.android.app.fragments.DidntLikeItHereFragment;
import com.trulymadly.android.app.fragments.FoundSomeSpecialFragment;
import com.trulymadly.android.app.fragments.JustTryingItOutFragment;
import com.trulymadly.android.app.fragments.TooPublicFragment;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.listener.ConfirmDialogImpl;
import com.trulymadly.android.app.listener.CustomOkHttpResponseHandler;
import com.trulymadly.android.app.listener.OnActionBarClickedInterface;
import com.trulymadly.android.app.utility.ActionBarHandler;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.ImageCacheHelper;
import com.trulymadly.android.app.utility.OkHttpHandler;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class DeleteAccountActivity extends AppCompatActivity
		implements OnDeleteAccountReasonsClickedListener {

	private final int FOUND_SOMEONE = 1, DIDNT_LIKE_IT = 2, TOO_PUBLIC = 3,
			DEL_ACC_REASONS = 5;
	private TextView mDeleteAccountView;
	private FoundSomeSpecialFragment mFoundSomeoneFragment;
	private DidntLikeItHereFragment mDidntLikeHereFragment;
	private TooPublicFragment mTooPublicFragment;
	private JustTryingItOutFragment mJustTryingItOutFragment;
	private Fragment mFragment;
	private FragmentManager mFragmentManager;
	private Context mContext;
	private HashMap<Integer, String> mAllReasons, mTitles;
	private String mDeleteAccountDefaultString, mDeleteAanywayString;
	private MoEHelper mhelper;

	private String mReason, mSubReason;
	private ConfirmDialogImpl mConfirmDialogImpl;
	private int mRate = -1;
	private CURRENT_FRAGMENT mCurrentFrgament;
	private ProgressDialog mProgressDialog;
	private boolean mShowPV = false;
	private ActionBarHandler mActionBarHandler;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = this;
		boolean isShowingPV = SPHandler.getBool(mContext,
				Constants.SHOW_DISCOVERY_OPTION);
		boolean isFemale = !Utility.isMale(mContext);
		mShowPV = !isShowingPV && isFemale;

		mhelper = new MoEHelper(this);
		setContentView(R.layout.delete_account_activity);
		OnActionBarClickedInterface onActionBarClickedInterface = new OnActionBarClickedInterface() {

			@Override
			public void onUserProfileClicked() {
			}


			@Override
			public void onCuratedDealsClicked() {

			}

			@Override
			public void onConversationsClicked() {
			}

            @Override
            public void onLocationClicked() {
            }

			@Override
			public void onBackClicked() {
				DeleteAccountActivity.this.onBackPressed();
			}

            @Override
            public void onTitleClicked() {
			}

			@Override
			public void onTitleLongClicked() {
			}

			@Override
			public void onSearchViewOpened() {

			}

			@Override
			public void onSearchViewClosed() {

			}

			@Override
			public void onCategoriesClicked() {

			}
		};
		mActionBarHandler = new ActionBarHandler(this, getResources().getString(R.string.delete_my_account), null, onActionBarClickedInterface,
				false, false, false);

		mConfirmDialogImpl = new ConfirmDialogImpl() {

			@Override
			public void onPositiveButtonSelected(Intent data) {
				mSubReason = getSubReason();
				if(data != null) {
					mRate = data.getIntExtra("app_rating", 0);
				}else
					mRate = -1;

				sendDeleteRequestToServer();
			}

			@Override
			public void onPositiveButtonSelected() {
				mSubReason = getSubReason();
				sendDeleteRequestToServer();
			}

			@Override
			public void onNegativeButtonSelected() {
				TrulyMadlyTrackEvent.trackEvent(mContext, TrulyMadlyActivities.settings,
						TrulyMadlyEventTypes.delete_cancel_click, 0, TrulyMadlyEvent.TrulyMadlyEventStatus.success, null, true);
			}
		};

		mDeleteAccountView = (TextView) findViewById(R.id.delete_account);
		mDeleteAccountView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				UiUtils.hideKeyBoard(mContext);
				if(mCurrentFrgament == CURRENT_FRAGMENT.foundSomeoneFragment ||
						mCurrentFrgament == CURRENT_FRAGMENT.tooPublicFragment){
					showAppRateDialog();
				}else {
					AlertsHandler.showConfirmDialog(mContext, R.string.delete_account_confirmation, R.string.delete,
							R.string.cancel, mConfirmDialogImpl);
				}
			}
		});

		mFragmentManager = getSupportFragmentManager();

		initialize();
	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	private void initialize() {
		initializeStringConstants();
		showDefaultFragment();
	}

	private void showDefaultFragment() {
		mActionBarHandler.setTitle(mTitles.get(DEL_ACC_REASONS));
		DeleteAccountReasonsFragment mDeleteAccountReasonsFragment = new DeleteAccountReasonsFragment();
		changeFragment(CURRENT_FRAGMENT.deleteAccountReasons, mDeleteAccountReasonsFragment,
				DeleteAccountReasonsFragment.class.getSimpleName());
		mDeleteAccountView.setText(mDeleteAccountDefaultString);
		mDeleteAccountView.setVisibility(View.GONE);
	}

	private void initializeStringConstants() {
		if (mAllReasons == null) {
			mAllReasons = new HashMap<>();
			mTitles = new HashMap<>();

			mAllReasons.put(FOUND_SOMEONE, getResources().getString(R.string.da_reason_found_someone_special));
			mAllReasons.put(DIDNT_LIKE_IT, getResources().getString(R.string.da_reason_didnt_like_here_header));
			mAllReasons.put(TOO_PUBLIC, getResources().getString(R.string.da_reason_too_public_header));

			mTitles.put(FOUND_SOMEONE, getResources().getString(R.string.da_reason_found_someone_special_header));
			mTitles.put(DIDNT_LIKE_IT, getResources().getString(R.string.da_reason_didnt_like_here_header));
			mTitles.put(TOO_PUBLIC, getResources().getString(R.string.da_reason_too_public_header));
			mTitles.put(DEL_ACC_REASONS, getResources().getString(R.string.delete_my_account));
		}

		mDeleteAccountDefaultString = getResources().getString(R.string.delete_my_account);
		mDeleteAanywayString = getResources().getString(R.string.delete_anyway);
	}

	private void changeFragment(CURRENT_FRAGMENT currentFragment, Fragment fragment, String className) {
		try {
			mFragmentManager.beginTransaction().replace(R.id.da_fragment_container, fragment, className)
					.commitAllowingStateLoss();

			mCurrentFrgament = currentFragment;
			mFragment = fragment;

		} catch (IllegalStateException ignored) {
		}
	}

	private void removePreviousFragment() {
		if (mCurrentFrgament != CURRENT_FRAGMENT.deleteAccountReasons) {
			mFragmentManager.beginTransaction().remove(mFragment).commitAllowingStateLoss();
		}
	}

	@Override
	public void onBackPressed() {
		if (mCurrentFrgament != CURRENT_FRAGMENT.deleteAccountReasons &&
				mCurrentFrgament != CURRENT_FRAGMENT.justTryingItOutFragment) {
			UiUtils.hideKeyBoard(mContext);
			removePreviousFragment();
			showDefaultFragment();
			mDeleteAccountView.setVisibility(View.GONE);
			mCurrentFrgament = CURRENT_FRAGMENT.deleteAccountReasons;
		} else
			super.onBackPressed();
	}

	@Override
	public void onReasonClicked(int id) {
		if (mCurrentFrgament != CURRENT_FRAGMENT.justTryingItOutFragment)
			removePreviousFragment();
		int title = -1;
		switch (id) {
			case R.id.reason_found_someone:
				mReason = mAllReasons.get(FOUND_SOMEONE);
				title = FOUND_SOMEONE;
				mFoundSomeoneFragment = new FoundSomeSpecialFragment();
				changeFragment(CURRENT_FRAGMENT.foundSomeoneFragment, mFoundSomeoneFragment,
						FoundSomeSpecialFragment.class.getSimpleName());
				break;
			case R.id.reason_didnt_like:
				mReason = mAllReasons.get(DIDNT_LIKE_IT);
				title = DIDNT_LIKE_IT;
				mDidntLikeHereFragment = new DidntLikeItHereFragment();
				changeFragment(CURRENT_FRAGMENT.didntLikeHereFragment, mDidntLikeHereFragment,
						DidntLikeItHereFragment.class.getSimpleName());
				break;
			case R.id.reason_too_public:
				mReason = mAllReasons.get(TOO_PUBLIC);
				if (mShowPV) {
					title = TOO_PUBLIC;
					mTooPublicFragment = new TooPublicFragment();
					changeFragment(CURRENT_FRAGMENT.tooPublicFragment, mTooPublicFragment,
							TooPublicFragment.class.getSimpleName());
				} else {
					mCurrentFrgament = CURRENT_FRAGMENT.justTryingItOutFragment;
					showAppRateDialog();
					//				mDeleteAccountView.setVisibility(View.GONE);
					//				mReason = mAllReasons.getBool(2);
					//				mJustTryingItOutFragment = new JustTryingItOutFragment();
					//				changeFragment(CURRENT_FRAGMENT.justTryingItOutFragment, mJustTryingItOutFragment,
					//						JustTryingItOutFragment.class.getSimpleName());
				}
				break;
			//		case R.id.reason_others:
			//			mReason = mAllReasons.getBool(3);
			//			mOthersFragment = new OthersFragment();
			//			changeFragment(CURRENT_FRAGMENT.othersFragment, mOthersFragment, OthersFragment.class.getSimpleName());
			//			break;
		}
		if(title != -1){
			mActionBarHandler.setTitle(mTitles.get(title));
			mDeleteAccountView.setText(mDeleteAanywayString);
			mDeleteAccountView.setVisibility(View.VISIBLE);
		}else{
			mDeleteAccountView.setVisibility(View.GONE);
		}
	}

	private void showAppRateDialog(){
		AlertsHandler.showAppRateDialog(DeleteAccountActivity.this, getResources().getString(R.string.delete_account_confirmation_with_rating),
				R.string.delete, R.string.cancel, mConfirmDialogImpl, false);
	}

	private void sendDeleteRequestToServer() {
		Map<String, String> params = new HashMap<>();
		params.put("action", "delete_account");
		params.put("reason", mReason);

		if (mSubReason != null)
			params.put("sub_reason", mSubReason);

		if(mRate != -1){
			params.put("app_rate", String.valueOf(mRate));
		}

		CustomOkHttpResponseHandler okHttpResponseHandler = new CustomOkHttpResponseHandler(mContext,
				TrulyMadlyActivities.settings, TrulyMadlyEventTypes.delete_click) {

			@Override
			public void onRequestSuccess(JSONObject responseJSON) {
				mProgressDialog = UiUtils.hideProgressBar(mProgressDialog);
				SPHandler.setBool(mContext, ConstantsSP.SHARED_KEYS_ACCOUNT_DELETED, true);
				HttpTasks.logout(mContext, mProgressDialog);

				//Clear the image cache folder
				ImageCacheHelper.clearCache();
			}

			@Override
			public void onRequestFailure(Exception exception) {
				AlertsHandler.showNetworkError(DeleteAccountActivity.this, exception);
				mProgressDialog = UiUtils.hideProgressBar(mProgressDialog);
			}
		};
		mProgressDialog = UiUtils.showProgressBar(this, mProgressDialog, R.string.processing_your_request);

		OkHttpHandler.httpPost(mContext, ConstantsUrls.get_delete_api_url(), params, okHttpResponseHandler);
	}

	private String getSubReason() {
		switch (mCurrentFrgament) {
			case foundSomeoneFragment:
				return mFoundSomeoneFragment.getSubReason();
			case didntLikeHereFragment:
				return mDidntLikeHereFragment.getSubReason();
			case tooPublicFragment:
				return mTooPublicFragment.getSubReason();
			case justTryingItOutFragment:
				return null;//((GetSubReasonsInterface) mJustTryingItOutFragment).getSubReason();
//			case othersFragment:
//				return ((GetSubReasonsInterface) mOthersFragment).getSubReason();
		}

		return null;
	}

	private enum CURRENT_FRAGMENT {
		deleteAccountReasons, foundSomeoneFragment, didntLikeHereFragment, tooPublicFragment, justTryingItOutFragment, othersFragment
	}

}
