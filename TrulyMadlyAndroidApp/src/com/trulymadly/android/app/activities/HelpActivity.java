package com.trulymadly.android.app.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.moe.pushlibrary.MoEHelper;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.utility.ActionBarHandler;
import com.trulymadly.android.app.utility.ActivityHandler;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class HelpActivity extends AppCompatActivity {

	private Context aContext;
	private MoEHelper mHelper = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		aContext = this;
		mHelper = new MoEHelper(this);
		setContentView(R.layout.help);
		String title = "Help";
		new ActionBarHandler(this, title, null, null,
				false, false, false);
		ButterKnife.bind(this);
	}
	
	@OnClick(R.id.help_feedback)
	public void onClickFeedback() {
        ActivityHandler.startMessageOneonOneActivity(aContext, null, null, false, true,
                false, false);
	}
	

	@Override
	public void onStart() {
        super.onStart();
	}

	@Override
	public void onStop() {
        super.onStop();
	}

	@Override
	public void onResume() {
        super.onResume();
	}

	@Override
	public void onPause() {
        super.onPause();
	}

}
