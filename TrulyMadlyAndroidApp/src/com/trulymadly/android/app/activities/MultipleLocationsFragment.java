package com.trulymadly.android.app.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.trulymadly.android.app.R;
import com.trulymadly.android.app.adapter.LocationsAdapter;
import com.trulymadly.android.app.modal.LocationModal;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.ButterKnife;


public class MultipleLocationsFragment extends Fragment {

    private RecyclerView locations_list_view;
    private Activity activity;
    private ArrayList<LocationModal> locations;
    private String dealId;
    private String trkActivity = null;
    private HashMap<String, String> trkEventInfo = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle data = getArguments();
        if (data != null){
            locations = data.getParcelableArrayList("locations");
            dealId = data.getString("dealId");

        }

        Context aContext = getActivity();
        activity = getActivity();

        if (savedInstanceState != null) {
            return;
        }
        ButterKnife.bind(activity);

    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle paramBundle) {

        View locationsView = inflater.inflate(
                R.layout.locations_list_layout, container, false);
        locations_list_view = (RecyclerView) locationsView.findViewById(R.id.locations_list_view);
        showMultipleLocationList(locations);
        return locationsView;
    }


    private void showMultipleLocationList(ArrayList<LocationModal> locations) {
        locations_list_view.setLayoutManager(new LinearLayoutManager(activity, LinearLayout.VERTICAL, false));
        locations_list_view.setAdapter(new LocationsAdapter(activity, locations, dealId, trkActivity, trkEventInfo));

    }

    public void setTrkActivity(String trkActivity) {
        this.trkActivity = trkActivity;
    }

    public void setTrkEventInfo(HashMap<String, String> trkEventInfo) {
        this.trkEventInfo = trkEventInfo;
    }
}
