package com.trulymadly.android.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.crashlytics.android.Crashlytics;
import com.moe.pushlibrary.MoEHelper;
import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.FilesHandler;
import com.trulymadly.android.app.utility.TmLogger;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;
import com.trulymadly.android.app.utility.VideoUtils;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by deveshbatra on 7/18/16.
 */
public class VideoCaptureActivity extends AppCompatActivity implements SurfaceHolder.Callback, View.OnClickListener {


    private static final String TAG = "VideoCaptureActivity";
    //    private static boolean isTimerRunning;
    SurfaceHolder holder;
    MyTimerTask myTask;
    @BindView(R.id.video_sub_container)
    View sub_container;
    int noOfCameras;
    boolean isFrontCamera = true, isRecording, isPreparing;
    @BindView(R.id.custom_progress_bar_2)
    ProgressBar progressBar;
    @BindView(R.id.record_icon)
    ImageView record_icon;
    @BindView(R.id.min_duration_view)
    View min_duration_view;
    @BindView(R.id.horizontal_margin)
    View horizontal_margin;
    @BindView(R.id.min_duration_pointer)
    View min_duration_pointer;
    @BindView(R.id.horizontal_margin_pointer)
    View horizontal_margin_pointer;
    @BindView(R.id.camera_toggle_icon)
    View camera_toggle_icon;
    @BindView(R.id.CameraView)
    SurfaceView cameraView;
    @BindView(R.id.CameraViewContainer)
    View cameraViewContainer;
    private MoEHelper mhelper = null;
    private Activity aActivity;
    private int progressCount = 0;
    private android.os.Handler handler;
    private MediaRecorder recorder;
    private long i;
    private Camera mCamera;
    private String filePath;
    private int orientationDegree = VideoUtils.VIDEO_ROTATION_DEFAULT;
    private int widthPx, heightPx;
    private boolean isVideoSaved = false;


    private int getAngleForCorrectingRotatedCameraView(CameraInfo info) {
        /* Taken From here
        https://plus.google.com/+AndroidDevelopers/posts/jXNFNKWxsc3
        http://stackoverflow.com/questions/35448222/camera-view-is-upside-down-in-my-android-application-for-nexus-5x
         */
        int rotation = aActivity.getWindowManager().getDefaultDisplay()
                .getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        int result;
        if (info.facing == CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        TmLogger.d(TAG, "result: " + result);
        return result;
    }

    private void setAttributesRearCam(int cameraId) {
        recorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
        recorder.setVideoSource(MediaRecorder.VideoSource.DEFAULT);

//        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
//        recorder.setVideoEncodingBitRate(3000000);
//        recorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
//        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
//        recorder.setVideoFrameRate(30);
//        recorder.setMaxFileSize(100000000);

        CamcorderProfile profile = getMaxCameraProfile(cameraId);
        recorder.setProfile(profile);
    }


    private void setAttributesFrontCam(int cameraId) {
        recorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        recorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);

        CamcorderProfile profile = getMaxCameraProfile(cameraId);
        recorder.setProfile(profile);

    }

    private CamcorderProfile getMaxCameraProfile(int cameraId) {
        CamcorderProfile profile;
//        if (CamcorderProfile.hasProfile(cameraId, CamcorderProfile.QUALITY_2160P)) {
//            profile = CamcorderProfile.get(cameraId, CamcorderProfile.QUALITY_2160P);
//        } else if (CamcorderProfile.hasProfile(cameraId, CamcorderProfile.QUALITY_1080P)) {
//            profile = CamcorderProfile.get(cameraId, CamcorderProfile.QUALITY_1080P);
//        } else
        if (CamcorderProfile.hasProfile(cameraId, CamcorderProfile.QUALITY_720P))
            profile = CamcorderProfile.get(cameraId, CamcorderProfile.QUALITY_720P);
        else if (CamcorderProfile.hasProfile(cameraId, CamcorderProfile.QUALITY_480P))
            profile = CamcorderProfile.get(cameraId, CamcorderProfile.QUALITY_480P);
        else
            profile = CamcorderProfile.get(cameraId, CamcorderProfile.QUALITY_LOW);

        TmLogger.d(TAG, "final video size: " + profile.videoFrameWidth + " " + profile.videoFrameHeight);
        return profile;
    }


    private void startPreview() {
        int cameraId = 1;
        if (isFrontCamera) {
            cameraId = 1;
        } else {
            cameraId = 0;
        }
        releaseCamera();
        try {
            mCamera = Camera.open(cameraId);
        } catch (Exception e) {
            Crashlytics.logException(e);
            Intent intent = this.getIntent();
            intent.putExtra("error", getString(R.string.camera_stopped_working));
            this.setResult(RESULT_OK, intent);
            stopActivity();
        }
        Camera.Parameters parameters = mCamera.getParameters();
        CamcorderProfile profile = getMaxCameraProfile(cameraId);

        List<Camera.Size> supportedPreviewSizes = mCamera.getParameters().getSupportedPreviewSizes();
        Camera.Size previewSize = supportedPreviewSizes.get(0);
        //Checking is camera profile size is in supported preview size or not.
        Camera.Size pSize = mCamera.new Size(profile.videoFrameWidth, profile.videoFrameHeight);
        if (supportedPreviewSizes.contains(pSize)) {
            previewSize = pSize;
        }

        TmLogger.d(TAG, "cameraId: " + cameraId);

        ViewGroup.LayoutParams params = null;
        float ratio = (float) previewSize.width / (float) previewSize.height;
        params = cameraView.getLayoutParams();
        params.height = (int) (widthPx * ratio);
        cameraView.setLayoutParams(params);

        params = cameraViewContainer.getLayoutParams();
        params.height = widthPx;
        cameraViewContainer.setLayoutParams(params);
        sub_container.requestLayout();

        Rect rectangle = new Rect();
        getWindow().getDecorView().getWindowVisibleDisplayFrame(rectangle);

        int translateY = ((heightPx - widthPx) / 2) + rectangle.top;//530;//(previewSize.width - previewSize.height) / 2;
        TmLogger.d(TAG, "size.height:" + previewSize.height + ",size.width:" + previewSize.width);
        TmLogger.d(TAG, "heightPx:" + heightPx + ",widthPx:" + widthPx + ",translateY:" + translateY);
        //cameraView.animate().setDuration(100).translationY(-1 * translateY);
        cameraView.setTranslationY(-1 * translateY);


        parameters.setPreviewSize(previewSize.width, previewSize.height);

        mCamera.setParameters(parameters);

//        params = sub_container.getLayoutParams();
//        params.height = heightPx - (widthPx + UiUtils.dpToPx(64));
//        sub_container.setLayoutParams(params);

        try {
            mCamera.setPreviewDisplay(holder);
            mCamera.startPreview();
        } catch (IOException e) {
            Crashlytics.logException(e);
        }
        CameraInfo info = new CameraInfo();
        Camera.getCameraInfo(cameraId, info);
        orientationDegree = getAngleForCorrectingRotatedCameraView(info);
        TmLogger.d(TAG, "setDisplayOrientation: " + orientationDegree);
        mCamera.setDisplayOrientation(orientationDegree);
        mCamera.lock();
    }

    private boolean prepareRecorder2() {
        if (mCamera == null) {
            return false;
        }

        // Step 1: Unlock and set camera to MediaRecorder
        mCamera.unlock();
        recorder.setCamera(mCamera);

        if (isFrontCamera) {
            setAttributesFrontCam(1);
        } else {
            setAttributesRearCam(0);
        }

        // delete the junk
        if (Utility.isSet(filePath)) {
            FilesHandler.deleteFileOrDirectory(filePath);
        }

        filePath = FilesHandler.getFilePath(FilesHandler.getVideoDirectoryPath(true), "tm_video_upload_" + Calendar.getInstance().getTimeInMillis() + ".mp4");
        recorder.setOutputFile(filePath);

        TmLogger.d(TAG, "isFrontCamera: " + isFrontCamera);
        // undo compensate the mirror
        orientationDegree = isFrontCamera ? (360 - orientationDegree) % 360 : orientationDegree;
        recorder.setPreviewDisplay(holder.getSurface());
        TmLogger.d(TAG, "setOrientationHint: " + orientationDegree);
        recorder.setOrientationHint(orientationDegree);
        try {
            recorder.prepare();
        } catch (IllegalStateException | IOException e) {
            Crashlytics.logException(e);
            releaseRecorder();
            return false;
        }

        return true;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        aActivity = this;
        mhelper = new MoEHelper(this);

        handler = new android.os.Handler();


        noOfCameras = Camera.getNumberOfCameras();

        recorder = new MediaRecorder();

        setContentView(R.layout.video_capture_layout);
        ButterKnife.bind(this);

        // setting height = width
        DisplayMetrics displayMetrixLocal = aActivity.getResources().getDisplayMetrics();
        widthPx = displayMetrixLocal.widthPixels;
        heightPx = displayMetrixLocal.heightPixels;

        ViewGroup.LayoutParams params = cameraView.getLayoutParams();
        params.height = widthPx;
        cameraView.setLayoutParams(params);

        holder = cameraView.getHolder();
        holder.addCallback(this);
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        Drawable draw = getResources().getDrawable(R.drawable.video_capture_progressbar);
        progressBar.setProgressDrawable(draw);
        cameraView.setClickable(false);
        params = horizontal_margin.getLayoutParams();
        params.width = widthPx / 3;
        horizontal_margin.setLayoutParams(params);
        params = horizontal_margin_pointer.getLayoutParams();
        params.width = widthPx / 3 - UiUtils.dpToPx(40);
        horizontal_margin_pointer.setLayoutParams(params);
        if (noOfCameras > 1) {
            camera_toggle_icon.setVisibility(View.VISIBLE);
        } else {
            isFrontCamera = false;
            camera_toggle_icon.setVisibility(View.GONE);
        }
    }

    private void startRecording() {
        if (!isPreparing) {
            isPreparing = true;
            min_duration_pointer.setVisibility(View.GONE);
//            new MediaPrepareTask().execute(null, null, null);
            new MediaPrepareTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null, null, null);
        }
    }

    private void stopRecording() {
        isRecording = false;
//        isTimerRunning = false;

        if (myTask != null) {
            myTask.cancel();
        }

//        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
//        //use one of overloaded setDataSource() functions to set your data source
//        File file = new File(filePath);
//        boolean isShort = false;
//        if(file.exists()) {
//            //retriever.setDataSource(aActivity, Uri.fromFile(file));
//            retriever.setDataSource(aActivity, FileProvider.getUriForFile(aContext, BuildConfig.APPLICATION_ID + ".provider", file));
//            String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
//            long timeInMillisec = Long.parseLong(time);
//            isShort = timeInMillisec < 5000;
//        }

        if (progressCount < VideoUtils.VIDEO_DURATION_MIN_SECS_DEFAULT) {
            min_duration_pointer.setVisibility(View.VISIBLE);
            AlertsHandler.showMessage(aActivity, "Minimum duration is 5 sec");
            record_icon.setImageDrawable(getResources().getDrawable(R.drawable.record_button_inactive));
            if (Utility.isSet(filePath)) {
                FilesHandler.deleteFileOrDirectory(filePath);
            }
            try {
                recorder.stop();
            } catch (RuntimeException stopException) {
                // delete the junk
                if (Utility.isSet(filePath)) {
                    FilesHandler.deleteFileOrDirectory(filePath);
                }

                if (myTask != null) {
                    myTask.cancel();
                }
            }
            recorder.reset();
            releaseRecorder();
        } else {
            if (progressCount == VideoUtils.VIDEO_DURATION_MAX_SECS_DEFAULT) {
                progressBar.setProgress(progressCount);
            }

            Intent intent = this.getIntent();
            intent.putExtra("filepath", filePath);
            intent.putExtra("video_time_ms", progressCount * 1000);
            intent.putExtra("camera", isFrontCamera ? VideoUtils.VIDEO_CAMERA_FRONT : VideoUtils.VIDEO_CAMERA_BACK);
            intent.putExtra("rotate", orientationDegree);
            this.setResult(RESULT_OK, intent);
            isVideoSaved = true;
            stopActivity();
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        startPreview();
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        stopActivity();
    }

    private void stopActivity() {
        if (isRecording) {
            try {
                recorder.stop();
            } catch (IllegalStateException ignored) {

            }
            recorder.reset();
            isRecording = false;
        }
        releaseRecorder();
        releaseCamera();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        if (!isVideoSaved && Utility.isSet(filePath)) {
            FilesHandler.deleteFileOrDirectory(filePath);
        }
        releaseRecorder();
        releaseCamera();
        super.onDestroy();
    }

    @Override
    @OnClick({R.id.cancel_tv, R.id.record_icon, R.id.camera_toggle_icon})
    public void onClick(View view) {
        Map<String, String> eventInfo;
        switch (view.getId()) {

            case R.id.cancel_tv:
                closeVideoCapture(false);
                break;
            case R.id.record_icon:
//                recordingStarted = true;
//                if (!isTimerRunning) {
                if (isPreparing) {
                    return;
                }

                eventInfo = new HashMap<>();
                eventInfo.put("camera", isFrontCamera ? VideoUtils.VIDEO_CAMERA_FRONT : VideoUtils.VIDEO_CAMERA_BACK);
                if (!isRecording) {
                    eventInfo.put("video_length_ms", "0");
                    eventInfo.put("recording_status", "start");
                    TrulyMadlyTrackEvent.trackEvent(aActivity, TrulyMadlyEvent.TrulyMadlyActivities.photo, TrulyMadlyEvent.TrulyMadlyEventTypes.video_record, 0,
                            null, eventInfo, true);
                    startRecording();
                } else {
                    eventInfo.put("video_length_ms", String.valueOf(progressCount * 1000));
                    eventInfo.put("recording_status", "stop");
                    TrulyMadlyTrackEvent.trackEvent(aActivity, TrulyMadlyEvent.TrulyMadlyActivities.photo, TrulyMadlyEvent.TrulyMadlyEventTypes.video_record, 0,
                            null, eventInfo, true);
                    stopRecording();
                }


                break;
            case R.id.camera_toggle_icon:
                if (isRecording) {
                    return;
                }
                isFrontCamera = !isFrontCamera;
                if (myTask != null) {
                    myTask.cancel();
                }
                startPreview();
                break;

        }
    }

    private void releaseRecorder() {
        if (recorder != null) {
            // clear recorder configuration
            recorder.reset();
            // release the recorder object
            recorder.release();
            recorder = null;
            // Lock camera for later use i.e taking it back from MediaRecorder.
            // MediaRecorder doesn't need it anymore and we will release it if the activity pauses.
            if (mCamera != null) {
                mCamera.lock();
            }
        }
    }

    private void releaseCamera() {
        if (mCamera != null) {
            // release the camera for other applications
            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    public void onBackPressed() {
        closeVideoCapture(true);
    }

    private void closeVideoCapture(boolean isFromBackPressed) {
        Map<String, String> eventInfo = new HashMap<>();
        eventInfo.put("camera", isFrontCamera ? VideoUtils.VIDEO_CAMERA_FRONT : VideoUtils.VIDEO_CAMERA_BACK);
        eventInfo.put("source", VideoUtils.VIDEO_SOURCE_CAMERA);
        eventInfo.put("page", "capture");
        eventInfo.put("back_pressed", String.valueOf(isFromBackPressed));
        eventInfo.put("recording_status", isRecording ? "during_recording" : "before_recording");
        eventInfo.put("video_length_ms", String.valueOf(progressCount * 1000));
        TrulyMadlyTrackEvent.trackEvent(aActivity, TrulyMadlyEvent.TrulyMadlyActivities.photo, TrulyMadlyEvent.TrulyMadlyEventTypes.video_cancel, 0,
                null, eventInfo, true);
        stopActivity();

    }

    class MyTimerTask extends TimerTask {

        public void run() {

            if (progressCount == VideoUtils.VIDEO_DURATION_MAX_SECS_DEFAULT) {
                handler.post(new Runnable() {
                    public void run() {
                        stopRecording();

                    }
                });

            } else {
                handler.post(new Runnable() {
                    public void run() {
                        progressCount++;
                        progressBar.setProgress(progressCount);
                    }
                });
            }
        }
    }

    /**
     * Asynchronous task for preparing the {@link android.media.MediaRecorder} since it's a long blocking
     * operation.
     */
    class MediaPrepareTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... voids) {
            Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
            // initialize video camera
            releaseRecorder();
            if (recorder == null) {
                recorder = new MediaRecorder();
            } else {
                try {
                    recorder.stop();
                } catch (RuntimeException stopException) {
                    // delete the junk
                    if (Utility.isSet(filePath)) {
                        FilesHandler.deleteFileOrDirectory(filePath);
                    }

                    if (myTask != null) {
                        myTask.cancel();
                    }
                }
                recorder.reset();
            }
            if (prepareRecorder2()) {
                // Camera is available and unlocked, MediaRecorder is prepared,
                // now you can start recording
                try {
                    recorder.start();
                } catch (IllegalStateException ise) {
                    return false;
                }
                isRecording = true;
            } else {
                // prepare didn't work, release the camera
                releaseRecorder();
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            isPreparing = false;
            if (!result) {
                releaseRecorder();
                releaseCamera();

                if (myTask != null) {
                    myTask.cancel();
                }

                Intent intent = VideoCaptureActivity.this.getIntent();
                intent.putExtra("error", getString(R.string.recording_failed));
                VideoCaptureActivity.this.setResult(RESULT_OK, intent);
                VideoCaptureActivity.this.finish();
            }
            progressCount = 0;
            progressBar.setProgress(0);
            progressBar.setMax(VideoUtils.VIDEO_DURATION_MAX_SECS_DEFAULT);
            // inform the user that recording has started
            record_icon.setImageDrawable(getResources().getDrawable(R.drawable.record_button_active));
            final Timer myTimer = new Timer();
            myTask = new MyTimerTask();
            myTimer.schedule(myTask, 1000, 1000);
        }
    }


}
