package com.trulymadly.android.app.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewStub;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.squareup.otto.Subscribe;
import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.billing.PaymentFor;
import com.trulymadly.android.app.billing.PaymentMode;
import com.trulymadly.android.app.billing.SparksBillingController;
import com.trulymadly.android.app.bus.BusProvider;
import com.trulymadly.android.app.bus.ShowRestorePurchasesEvent;
import com.trulymadly.android.app.fragments.BuySparkEventListener;
import com.trulymadly.android.app.fragments.SelectQuizFragment;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.listener.TrackingBuySelectEventListener;
import com.trulymadly.android.app.modal.MySelectData;
import com.trulymadly.android.app.modal.SelectModePackageModal;
import com.trulymadly.android.app.modal.SelectPackageModal;
import com.trulymadly.android.app.sqlite.CachingDBHandler;
import com.trulymadly.android.app.sqlite.MatchesDbHandler;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.TMSelectHandler;
import com.trulymadly.android.app.utility.TMSelectPackagesUIHandler2;
import com.trulymadly.android.app.utility.TMSelectSingleActionUIHandler;
import com.trulymadly.android.app.utility.TmLogger;
import com.trulymadly.android.app.utility.Utility;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities.select;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventStatus.clicked;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventStatus.quiz_ended;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventStatus.quiz_started;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventStatus.view;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes.buy;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes.buy_trial;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes.quiz;

public class TMSelectActivity extends AppCompatActivity implements View.OnClickListener {

    public static boolean isKilled = false;
    @BindView(R.id.select_intro_action_screen_stub)
    ViewStub mSelectActionScreenStub;
    @BindView(R.id.select_packages_screen_stub)
    ViewStub mSelectPackagesScreenStub;
    @BindView(R.id.quiz_container)
    View mQuizContainer;
    @BindView(R.id.loader_container)
    View mLoaderContainer;
    @BindView(R.id.retry_message_tv)
    TextView mRetryMsgTV;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;
    @BindView(R.id.retry_container)
    View mRetryContainer;
    private View mSelectActionScreen;
    private View mSelectPackagesScreen;
//    private TMSelectPackagesUIHandler mTMSelectPackagesUIHandler;
    private TMSelectPackagesUIHandler2 mTMSelectPackagesUIHandler2;
    private TMSelectSingleActionUIHandler mTMSelectSingleActionUIHandler;
    private SelectQuizFragment mQuizFragment;
    private boolean isQuizStarted = false;
    private TMSelectHandler.PackagesHandler mPackagesHandler;
    private TMSelectHandler.SubscriptionHandler mSubscriptionHandler;
    private SelectModePackageModal mSelectModePackageModal;
    private String launchSource;
    private boolean isQuizStartedDirectly;
    private int mLikesDone, mHidesDone, mSparksDone;
    private boolean isFavViewed;
    private String mMatchId;
    private SparksBillingController mSparksBillingController;
    private boolean isResumed;
    private Context mContext;
    private TrackingBuySelectEventListener mTrackingBuySelectEventListener;
    private SELECT_VIEW mSelectView;
    private boolean isSaved = false;
//    private Handler mHandler;
//    private Runnable mTakeForwardRunnable;
//    private boolean isAlreadyTakenForward = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tmselect);
        ButterKnife.bind(this);
        mContext = this;
        callingIntent(getIntent());
        if (savedInstanceState == null) {
            if (!isQuizStartedDirectly) {
                getPackages();
            } else {
                toggleView(SELECT_VIEW.quiz, null);
            }
        } else {
            restoreData(savedInstanceState);
        }
//        mHandler = new Handler();
    }

    private void restoreData(Bundle savedData) {
        if (savedData != null) {
            //Save : mSelectModePackageModal
            mSelectModePackageModal = savedData.getParcelable("mSelectModePackageModal");
            //Save : SELECT_VIEW
            mSelectView = SELECT_VIEW.getSelectViewFromInt(savedData.getInt("mSelectView", -1));
            //Save intent data (callingIntent())
            launchSource = savedData.getString("launchSource");
            isQuizStartedDirectly = savedData.getBoolean("isQuizStartedDirectly");
            isFavViewed = savedData.getBoolean("isFavViewed");
            mLikesDone = savedData.getInt("mLikesDone", -1);
            mSparksDone = savedData.getInt("mSparksDone", -1);
            mHidesDone = savedData.getInt("mHidesDone", -1);
            mMatchId = savedData.getString("mMatchId");
            isQuizStarted = savedData.getBoolean("isQuizStarted");

            if (mSelectView != null) {
                if (mSelectView == SELECT_VIEW.loader) {
                    getPackages();
                } else {
                    mQuizFragment = (SelectQuizFragment) getSupportFragmentManager().findFragmentById(R.id.quiz_container);
                    if (mQuizFragment != null) {
                        mQuizFragment.registerListener(new SelectQuizFragment.SelectQuizFragmentListener() {
                            @Override
                            public void closeFragment(boolean minimizeApp) {
                                if (minimizeApp) {
                                    Utility.minimizeApp(TMSelectActivity.this);
                                } else {
                                    HashMap<String, String> eventInfo = new HashMap<>();
                                    if (Utility.isSet(mMatchId)) {
                                        eventInfo.put("match_id", mMatchId);
                                    }
                                    eventInfo.put("source", launchSource);

                                    if (mLikesDone >= 0) {
                                        eventInfo.put("likes_done", String.valueOf(mLikesDone));
                                    }
                                    if (mHidesDone >= 0) {
                                        eventInfo.put("hides_done", String.valueOf(mHidesDone));
                                    }
                                    if (mSparksDone >= 0) {
                                        eventInfo.put("sparks_done", String.valueOf(mSparksDone));
                                    }
                                    if (mLikesDone >= 0) {
                                        eventInfo.put("favorites_viewed", String.valueOf(isFavViewed));
                                    }
                                    TrulyMadlyTrackEvent.trackEvent(mContext, select, quiz, 0, quiz_ended,
                                            eventInfo, true, true);
                                    try {
                                        finish();
                                    } catch (IllegalArgumentException iea) {
                                        Crashlytics.logException(iea);
                                    }
                                }
                            }
                        });
                    }
                    toggleView(mSelectView, null);
                }
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        callingIntent(intent);
        super.onNewIntent(intent);
    }

    private void callingIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        if (extras != null) {
            launchSource = extras.getString("source", "default");
            isQuizStartedDirectly = extras.getBoolean("start_quiz", false);
            mLikesDone = extras.getInt("likes_done", -1);
            mHidesDone = extras.getInt("hides_done", -1);
            mSparksDone = extras.getInt("sparks_done", -1);
            isFavViewed = extras.getBoolean("fav_viewed", false);
            mMatchId = extras.getString("match_id");
            if (!isQuizStartedDirectly && TMSelectHandler.isSelectMember(mContext) &&
                    !TMSelectHandler.isSelectQuizPlayed(mContext)) {
                isQuizStartedDirectly = true;
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        isSaved = true;
        //Save : mSelectModePackageModal
        outState.putParcelable("mSelectModePackageModal", mSelectModePackageModal);
        //Save : SELECT_VIEW
        if (mSelectView != null) {
            outState.putInt("mSelectView", mSelectView.getmKey());
        }
        //Save intent data (callingIntent())
        outState.putString("launchSource", launchSource);
        if (Utility.isSet(mMatchId)) {
            outState.putString("mMatchId", mMatchId);
        }
        outState.putBoolean("isQuizStartedDirectly", isQuizStartedDirectly);
        outState.putBoolean("isFavViewed", isFavViewed);
        outState.putInt("mLikesDone", mLikesDone);
        outState.putInt("mSparksDone", mSparksDone);
        outState.putInt("mHidesDone", mHidesDone);
        //Save isQuizStarted
        outState.putBoolean("isQuizStarted", isQuizStarted);


        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        if (isQuizStarted && mQuizFragment.onBackPressed()) {
            return;
        }

        if (TMSelectHandler.isSelectMember(mContext) && !TMSelectHandler.isSelectQuizPlayed(mContext)) {
            AlertsHandler.showSimpleSelectDialog(mContext, R.string.select_nudge_text_on_success_back,
                    R.string.continue_caps, R.string.quit_anyway, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            switch (view.getId()) {
                                case R.id.positive_button_tv:
                                    break;

                                case R.id.negative_button_tv:
                                    Utility.minimizeApp(TMSelectActivity.this);
                                    break;
                            }
                        }
                    });
            return;
        }

        super.onBackPressed();
    }

    private void getPackages() {
        getPackagesHandler().getPackages(mMatchId);
        toggleView(SELECT_VIEW.loader, null);
    }

    private void toggleView(SELECT_VIEW select_view, Object data) {
        mSelectView = select_view;
        switch (select_view) {
            case error:
                toggleLoader(true, (String) data);
                break;
            case loader:
                toggleLoader(false, null);
                break;

            case free_trial:
            case get_started:
            case packages:
            case packages_success:
            case free_trial_success:
                toggleMainScreen(select_view);
                break;

            case quiz:
                toggleQuiz(true);
                break;
        }
    }

    private TMSelectSingleActionUIHandler getmTMSelectSingleActionUIHandler() {
        if (mSelectActionScreen == null) {
            mSelectActionScreen = mSelectActionScreenStub.inflate();
            mTMSelectSingleActionUIHandler = new TMSelectSingleActionUIHandler(this, mSelectActionScreen,
                    (mSelectModePackageModal.isFreeTrial() ? mSelectModePackageModal.getmFreeTrialCtaText()
                            : getResources().getString(R.string.get_started)), this, mSelectModePackageModal.getmFreeTrialCTASubText());
        }
        return mTMSelectSingleActionUIHandler;
    }

//    private TMSelectPackagesUIHandler getmTMSelectPackagesUIHandler() {
    private TMSelectPackagesUIHandler2 getmTMSelectPackagesUIHandler() {
        if (mSelectPackagesScreen == null) {
            mSelectPackagesScreen = mSelectPackagesScreenStub.inflate();
//            mTMSelectPackagesUIHandler = new TMSelectPackagesUIHandler(this, mSelectPackagesScreen,
//                    mSelectModePackageModal.getmSelectPackageModals(), this);
            mTMSelectPackagesUIHandler2 = new TMSelectPackagesUIHandler2(this, mSelectPackagesScreen,
                    mSelectModePackageModal.getmSelectPackageModals(), this, mMatchId, mTrackingBuySelectEventListener);
        }
//        return mTMSelectPackagesUIHandler;
        return mTMSelectPackagesUIHandler2;
    }

    private void toggleLoader(boolean error, String message) {
        if (mSelectActionScreen != null) {
            mSelectActionScreen.setVisibility(View.GONE);
        }

        if (mSelectPackagesScreen != null) {
            mSelectPackagesScreen.setVisibility(View.GONE);
        }

        mLoaderContainer.setVisibility(View.VISIBLE);
        if (error) {
            mProgressBar.setVisibility(View.GONE);
            mRetryContainer.setVisibility(View.VISIBLE);
            if (Utility.isSet(message)) {
                mRetryMsgTV.setText(message);
            } else {
                mRetryMsgTV.setText(R.string.SERVER_ERROR_SMALL);
            }
        } else {
            mRetryContainer.setVisibility(View.GONE);
            mProgressBar.setVisibility(View.VISIBLE);
        }
    }

    private void toggleMainScreen(SELECT_VIEW select_view) {
        mLoaderContainer.setVisibility(View.GONE);
        HashMap<String, String> eventInfo = new HashMap<>();
        if (Utility.isSet(mMatchId)) {
            eventInfo.put("match_id", mMatchId);
        }
        eventInfo.put("source", launchSource);

        if (mLikesDone >= 0) {
            eventInfo.put("likes_done", String.valueOf(mLikesDone));
        }
        if (mHidesDone >= 0) {
            eventInfo.put("hides_done", String.valueOf(mHidesDone));
        }
        if (mSparksDone >= 0) {
            eventInfo.put("sparks_done", String.valueOf(mSparksDone));
        }
        if (mLikesDone >= 0) {
            eventInfo.put("favorites_viewed", String.valueOf(isFavViewed));
        }
        switch (select_view) {
            case free_trial:
                TrulyMadlyTrackEvent.trackEvent(mContext, select, buy_trial, 0, view,
                        eventInfo, true, true);
                getmTMSelectSingleActionUIHandler().toggleMainScreen(true);
                mSelectActionScreen.setVisibility(View.VISIBLE);
                break;
            case free_trial_success:
                getmTMSelectSingleActionUIHandler().toggleSuccessScreen(true, !TMSelectHandler.isSelectQuizPlayed(mContext));
                mSelectActionScreen.setVisibility(View.VISIBLE);
//                startTimer();
                break;

            case packages:
                TrulyMadlyTrackEvent.trackEvent(mContext, select, buy, 0, view,
                        eventInfo, true, true);
                initPaymentModule();
                getmTMSelectPackagesUIHandler().toggleMainScreen(true);
                mSelectPackagesScreen.setVisibility(View.VISIBLE);
                break;
            case packages_success:
                initPaymentModule();
                getmTMSelectPackagesUIHandler().toggleSuccessScreen(true, !TMSelectHandler.isSelectQuizPlayed(mContext));
                mSelectPackagesScreen.setVisibility(View.VISIBLE);
//                startTimer();
                break;

            case get_started:
                break;
        }
    }

//    private void startTimer() {
//        if (mTakeForwardRunnable == null) {
//            mTakeForwardRunnable = new Runnable() {
//                @Override
//                public void run() {
//                    if (!isAlreadyTakenForward) {
//                        isAlreadyTakenForward = true;
//                        if (!TMSelectHandler.isSelectQuizPlayed(mContext)) {
//                            toggleView(SELECT_VIEW.quiz, null);
//                        } else {
//                            finish();
//                        }
//                    }
//                }
//            };
//        } else {
//            mHandler.removeCallbacks(mTakeForwardRunnable);
//        }
//        mHandler.postDelayed(mTakeForwardRunnable, 5000);
//    }

    private void initPaymentModule() {
        if (mTrackingBuySelectEventListener == null) {
            BuySparkEventListener buySparkEventListener = new BuySparkEventListener() {
                @Override
                public void closeFragment() {
                    finish();
                }

                @Override
                public void onBuySparkClicked(Object packageModal, String matchId) {
                    SelectPackageModal selectPackageModal = (SelectPackageModal) packageModal;
                    TmLogger.d("SparksBillingHandler", "Spark buy clicked in Adapter : " + selectPackageModal.getmSku());
                    mSparksBillingController.askForPaymentOption(selectPackageModal.getmSku(), matchId,
                            0, String.valueOf(selectPackageModal.getmPrice()));
                }

                @Override
                public void onBuySparkSuccess(PaymentMode mPaymentMode, String sku, int newSparksAdded,
                                              int totalSparks, boolean isRelationshipExpertAdded,
                                              MySelectData mySelectData) {
                    MatchesDbHandler.clearMatchesCache(mContext);
                    CachingDBHandler.clearAllData(mContext);
                    if (isResumed) {
                        mySelectData.saveMySelectData(mContext);
                        TMSelectHandler.setPaymentDone(mContext, true);
                        toggleView(SELECT_VIEW.packages_success, null);
                    }

                    if (Utility.isSet(mMatchId)) {
                        SPHandler.setString(mContext, ConstantsSP.SHARED_KEY_SELECT_SAVED_MATCH_ID, mMatchId);
                    } else {
                        SPHandler.remove(mContext, ConstantsSP.SHARED_KEY_SELECT_SAVED_MATCH_ID);
                    }
                }

                @Override
                public void restorePurchasesClicked() {
                    mSparksBillingController.restorePurchases(false);
                }

                @Override
                public void onRegistered() {
                    mSparksBillingController.restorePurchases(true);
                }
            };
            mTrackingBuySelectEventListener = new TrackingBuySelectEventListener(mContext,
                    buySparkEventListener, launchSource, mLikesDone, mHidesDone, mSparksDone, isFavViewed, mMatchId);
            mSparksBillingController = new SparksBillingController(this, null, PaymentFor.select, mTrackingBuySelectEventListener);
            mTrackingBuySelectEventListener.onRegistered();
        }
    }

    private void toggleQuiz(boolean show) {
        HashMap<String, String> eventInfo = new HashMap<>();
        if (Utility.isSet(mMatchId)) {
            eventInfo.put("match_id", mMatchId);
        }
        eventInfo.put("source", launchSource);

        if (mLikesDone >= 0) {
            eventInfo.put("likes_done", String.valueOf(mLikesDone));
        }
        if (mHidesDone >= 0) {
            eventInfo.put("hides_done", String.valueOf(mHidesDone));
        }
        if (mSparksDone >= 0) {
            eventInfo.put("sparks_done", String.valueOf(mSparksDone));
        }
        if (mLikesDone >= 0) {
            eventInfo.put("favorites_viewed", String.valueOf(isFavViewed));
        }
        TrulyMadlyTrackEvent.trackEvent(mContext, select, quiz, 0, quiz_started,
                eventInfo, true, true);
        mLoaderContainer.setVisibility(View.GONE);
        if (mSelectActionScreen != null) {
            mSelectActionScreen.setVisibility(View.GONE);
        }
        if (mSelectPackagesScreen != null) {
            mSelectPackagesScreen.setVisibility(View.GONE);
        }

        if (show) {
            if (mQuizFragment == null) {
                mQuizFragment = SelectQuizFragment.newInstance(isQuizStartedDirectly);
                mQuizFragment.registerListener(new SelectQuizFragment.SelectQuizFragmentListener() {
                    @Override
                    public void closeFragment(boolean minimizeApp) {
                        if (minimizeApp) {
                            Utility.minimizeApp(TMSelectActivity.this);
                        } else {
                            HashMap<String, String> eventInfo = new HashMap<>();
                            if (Utility.isSet(mMatchId)) {
                                eventInfo.put("match_id", mMatchId);
                            }
                            eventInfo.put("source", launchSource);

                            if (mLikesDone >= 0) {
                                eventInfo.put("likes_done", String.valueOf(mLikesDone));
                            }
                            if (mHidesDone >= 0) {
                                eventInfo.put("hides_done", String.valueOf(mHidesDone));
                            }
                            if (mSparksDone >= 0) {
                                eventInfo.put("sparks_done", String.valueOf(mSparksDone));
                            }
                            if (mLikesDone >= 0) {
                                eventInfo.put("favorites_viewed", String.valueOf(isFavViewed));
                            }
                            TrulyMadlyTrackEvent.trackEvent(mContext, select, quiz, 0, quiz_ended,
                                    eventInfo, true, true);
                            try {
                                finish();
                            } catch (IllegalArgumentException iea) {
                                Crashlytics.logException(iea);
                            }
                        }
                    }
                });
            }
            isQuizStarted = true;
            mQuizContainer.setVisibility(View.VISIBLE);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.quiz_container, mQuizFragment).commit();
        }
    }

    private TMSelectHandler.PackagesHandler getPackagesHandler() {
        if (mPackagesHandler == null) {
            mPackagesHandler = new TMSelectHandler.PackagesHandler(this, new TMSelectHandler.PackagesHandler.PackagesHandlerListener() {
                @Override
                public void onGetPackages(boolean success, SelectModePackageModal selectModePackageModal,
                                          String errorMessage) {
                    if (success) {
                        mSelectModePackageModal = selectModePackageModal;
                        SELECT_VIEW select_view = SELECT_VIEW.packages;
                        if (selectModePackageModal.isFreeTrial()) {
                            select_view = SELECT_VIEW.free_trial;
                        }
                        toggleView(select_view, selectModePackageModal);
                    } else {
                        toggleView(SELECT_VIEW.error, errorMessage);
                    }
                }
            });
        }

        return mPackagesHandler;
    }

    private TMSelectHandler.SubscriptionHandler getSubscriptionHandler() {
        if (mSubscriptionHandler == null) {
            mSubscriptionHandler = new TMSelectHandler.SubscriptionHandler(mContext, new TMSelectHandler.SubscriptionHandler.SelectSubscriptionListener() {
                @Override
                public void onSubscription(boolean success, String errorMessage) {
                    mTMSelectSingleActionUIHandler.toggleProgressbar(false);
                    if (success) {
                        HashMap<String, String> eventInfo = new HashMap<>();
                        if (Utility.isSet(mMatchId)) {
                            eventInfo.put("match_id", mMatchId);
                        }
                        eventInfo.put("source", launchSource);

                        if (mLikesDone >= 0) {
                            eventInfo.put("likes_done", String.valueOf(mLikesDone));
                        }
                        if (mHidesDone >= 0) {
                            eventInfo.put("hides_done", String.valueOf(mHidesDone));
                        }
                        if (mSparksDone >= 0) {
                            eventInfo.put("sparks_done", String.valueOf(mSparksDone));
                        }
                        if (mLikesDone >= 0) {
                            eventInfo.put("favorites_viewed", String.valueOf(isFavViewed));
                        }
                        TrulyMadlyTrackEvent.trackEvent(mContext, select, buy_trial, 0,
                                TrulyMadlyEvent.TrulyMadlyEventStatus.success,
                                eventInfo, true, true);
                        MatchesDbHandler.clearMatchesCache(mContext);
                        CachingDBHandler.clearAllData(mContext);
                        TMSelectHandler.setPaymentDone(mContext, true);
                        toggleView(SELECT_VIEW.free_trial_success, null);

                        if (Utility.isSet(mMatchId)) {
                            SPHandler.setString(mContext, ConstantsSP.SHARED_KEY_SELECT_SAVED_MATCH_ID, mMatchId);
                        } else {
                            SPHandler.remove(mContext, ConstantsSP.SHARED_KEY_SELECT_SAVED_MATCH_ID);
                        }
                    } else {
                        AlertsHandler.showMessage(TMSelectActivity.this, errorMessage, false);
                    }
                }
            });
        }

        return mSubscriptionHandler;
    }

    @OnClick(R.id.retry_button)
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.select_action_button:
                HashMap<String, String> eventInfo = new HashMap<>();
                if (Utility.isSet(mMatchId)) {
                    eventInfo.put("match_id", mMatchId);
                }
                eventInfo.put("source", launchSource);

                if (mLikesDone >= 0) {
                    eventInfo.put("likes_done", String.valueOf(mLikesDone));
                }
                if (mHidesDone >= 0) {
                    eventInfo.put("hides_done", String.valueOf(mHidesDone));
                }
                if (mSparksDone >= 0) {
                    eventInfo.put("sparks_done", String.valueOf(mSparksDone));
                }
                if (mLikesDone >= 0) {
                    eventInfo.put("favorites_viewed", String.valueOf(isFavViewed));
                }
                TrulyMadlyTrackEvent.trackEvent(mContext, select, buy_trial, 0, clicked,
                        eventInfo, true, true);
                if (mSelectModePackageModal.isFreeTrial()) {
                    mTMSelectSingleActionUIHandler.toggleProgressbar(true);
                    getSubscriptionHandler().startFreeTrial(mMatchId);
                    //Call API
                } else if (mSelectModePackageModal.isQuizAbOn()) {
                    toggleView(SELECT_VIEW.quiz, null);
                }
                break;

            case R.id.retry_button:
                getPackages();
                break;

            case R.id.first_package_container:
                mTrackingBuySelectEventListener.onBuySparkClicked(mSelectModePackageModal.getmSelectPackageModals().get(0), mMatchId);
                break;
            case R.id.second_package_container:
                mTrackingBuySelectEventListener.onBuySparkClicked(mSelectModePackageModal.getmSelectPackageModals().get(1), mMatchId);
                break;
            case R.id.third_package_container:
                mTrackingBuySelectEventListener.onBuySparkClicked(mSelectModePackageModal.getmSelectPackageModals().get(2), mMatchId);
                break;

            case R.id.ok_fab:
//                if (!isAlreadyTakenForward) {
//                    isAlreadyTakenForward = true;
                    if (!TMSelectHandler.isSelectQuizPlayed(mContext)) {
                        toggleView(SELECT_VIEW.quiz, null);
                    } else {
                        finish();
                    }
//                }
                break;

            case R.id.restore_purchases_tv:
                if (mTrackingBuySelectEventListener != null) {
                    mTrackingBuySelectEventListener.restorePurchasesClicked();
                }
                break;
        }
    }

    @Subscribe
    public void showRestorePurchases(ShowRestorePurchasesEvent showRestorePurchaseEvent) {
//        if (mTMSelectPackagesUIHandler != null) {
//            mTMSelectPackagesUIHandler.toggleRestorePurchases(true);
//        }
        if (mTMSelectPackagesUIHandler2 != null) {
            mTMSelectPackagesUIHandler2.toggleRestorePurchases(true);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mSparksBillingController != null && mSparksBillingController.handleActivityResult(requestCode, resultCode, data)) {
            TmLogger.d("SparksBillingHandler", "onActivityResult handled by SparkBillingHandler.");
            return;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onStart() {
        super.onStart();

        BusProvider.getInstance().register(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        isResumed = true;
    }

    @Override
    protected void onStop() {
//        if (mTakeForwardRunnable != null) {
//            mHandler.removeCallbacks(mTakeForwardRunnable);
//        }
        BusProvider.getInstance().unregister(this);
        super.onStop();
    }

    @Override
    protected void onDestroy() {

//        if (mTMSelectPackagesUIHandler != null) {
//            mTMSelectPackagesUIHandler.onDestroy();
//        }
        if (mTMSelectPackagesUIHandler2 != null) {
            mTMSelectPackagesUIHandler2.onDestroy();
        }

        if (mTMSelectSingleActionUIHandler != null) {
            mTMSelectSingleActionUIHandler.onDestroy();
        }

        isKilled = isSaved;

        if (mPackagesHandler != null) {
            mPackagesHandler.unregister();
        }

        if (mSubscriptionHandler != null) {
            mSubscriptionHandler.unregister();
        }

        if (mSparksBillingController != null) {
            mSparksBillingController.onDestroy();
        }
        super.onDestroy();
    }

    private enum SELECT_VIEW {
        error(0), loader(1), packages(2), get_started(3), free_trial(4),
        free_trial_success(5), packages_success(6), quiz(7);

        private int mKey;

        SELECT_VIEW(int mKey) {
            this.mKey = mKey;
        }

        public static SELECT_VIEW getSelectViewFromInt(int key) {
            switch (key) {
                case 0:
                    return error;

                case 1:
                    return loader;

                case 2:
                    return packages;

                case 3:
                    return get_started;

                case 4:
                    return free_trial;

                case 5:
                    return free_trial_success;

                case 6:
                    return packages_success;

                case 7:
                    return quiz;
            }

            return null;
        }

        public int getmKey() {
            return mKey;
        }
    }
}
