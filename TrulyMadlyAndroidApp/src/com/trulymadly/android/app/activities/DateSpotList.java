package com.trulymadly.android.app.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.koushikdutta.ion.Ion;
import com.moe.pushlibrary.MoEHelper;
import com.squareup.picasso.Picasso;
import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.TrulyMadlyApplication;
import com.trulymadly.android.app.adapter.DateSpotListAdapter;
import com.trulymadly.android.app.json.ConstantsRF;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.json.DateSpotParser;
import com.trulymadly.android.app.listener.CachedDataInterface;
import com.trulymadly.android.app.listener.CustomOkHttpResponseHandler;
import com.trulymadly.android.app.listener.DateSpotListItemClickInterface;
import com.trulymadly.android.app.listener.LocationFetchedInterface;
import com.trulymadly.android.app.listener.OnActionBarClickedInterface;
import com.trulymadly.android.app.listener.SaveZonesInterface;
import com.trulymadly.android.app.listener.TapToRetryListener;
import com.trulymadly.android.app.modal.DateSpotModal;
import com.trulymadly.android.app.modal.DateSpotZone;
import com.trulymadly.android.app.sqlite.RFHandler;
import com.trulymadly.android.app.utility.ActionBarHandler;
import com.trulymadly.android.app.utility.ActivityHandler;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.LocationHelper;
import com.trulymadly.android.app.utility.OkHttpHandler;
import com.trulymadly.android.app.utility.PermissionsHelper;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.TapToRetryHandler;
import com.trulymadly.android.app.utility.TimeUtils;
import com.trulymadly.android.app.utility.TmLogger;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;
import com.trulymadly.android.app.utility.VenueSpotPreferenceSlidingMenuHandler;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.google.common.base.Preconditions.checkNotNull;


public class DateSpotList extends AppCompatActivity implements DateSpotListItemClickInterface,
        SlidingMenu.OnCloseListener,
        SlidingMenu.OnClosedListener,
        SlidingMenu.OnOpenedListener,
        SaveZonesInterface, LocationFetchedInterface {

    private static final String TAG = "DATE_SPOT_LIST";
    @BindView(R.id.date_spot_list_view)
    RecyclerView date_spot_list_view;
    @BindView(R.id.cup_animation)
    ImageView cup_animation;
    @BindView(R.id.cup_base)
    ImageView cup_base;
    @BindView(R.id.no_dates)
    View no_dates;
    @BindView(R.id.custom_prog_bar_id)
    View custom_prog_bar_id;
    @BindView(R.id.datelicious_icon_no_dates)
    ImageView datelicious_icon_no_dates;
    @BindView(R.id.date_spot_preference_tutorial)
    View date_spot_preference_tutorial;
    @BindView(R.id.date_spot_preference_tutorial_content)
    View date_spot_preference_tutorial_content;
    private MoEHelper mhelper;
    private Context aContext;
    private Activity aActivity;
    private TapToRetryHandler tapToRetryHandler;
    private String match_id, message_one_one_conversion_url;
    private boolean isLocationPreferencesVisible = false, isRequestIsProgress = false, isLocationPreferenceTutorialVisible = false;
    private ActionBarHandler actionBarHandler;
    private VenueSpotPreferenceSlidingMenuHandler dateSpotListPreferenceSlidingMenu = null;
    private ArrayList<DateSpotZone> zones;
    private String zonesCity;
    private DATE_SPOT_LIST_REQUEST_TYPE requestType;
    private ArrayList<String> selectedZoneIds;
    private LocationHelper mLocationHelper;
    private ArrayList<DateSpotModal> dateSpotList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        aContext = this;
        aActivity = this;
        mhelper = new MoEHelper(this);
        if (savedInstanceState != null) {
            return;
        }

        setContentView(R.layout.date_spot_list_layout);
        ButterKnife.bind(this);

        OnActionBarClickedInterface onActionBarClicked = new OnActionBarClickedInterface() {
            @Override
            public void onBackClicked() {
                onBackPressed();
            }

            @Override
            public void onUserProfileClicked() {

            }


            @Override
            public void onCuratedDealsClicked() {

            }

            @Override
            public void onConversationsClicked() {
            }

            @Override
            public void onLocationClicked() {
                toggleLocationPreferences();
            }

            @Override
            public void onTitleClicked() {

            }

            @Override
            public void onTitleLongClicked() {

            }

            @Override
            public void onSearchViewOpened() {

            }

            @Override
            public void onSearchViewClosed() {

            }

            @Override
            public void onCategoriesClicked() {

            }
        };

        actionBarHandler = new ActionBarHandler(this, getResources().getString(R.string.header_datepost_list),
                null, onActionBarClicked, false, false, false, false, false);

        Picasso.with(aContext).load(R.drawable.loading_datelicious_icon_base).noFade().config(Bitmap.Config.RGB_565).into(cup_base);
        Picasso.with(aContext).load(R.drawable.loading_datelicious_icon_white).noFade().into(datelicious_icon_no_dates);
        UiUtils.setBackground(aContext, no_dates, R.drawable.blur_background);
        UiUtils.setBackground(aContext, custom_prog_bar_id, R.drawable.blur_background);
        //Glide.with(aContext).load(R.drawable.loading_datelicious_icon_top_fumes).dontAnimate().into(cup_animation);
        Ion.with(cup_animation).load(UiUtils.resIdToUri(aContext, R.drawable.loading_datelicious_icon_top_fumes).toString());
        tapToRetryHandler = new TapToRetryHandler(this, findViewById(android.R.id.content), new TapToRetryListener() {
            @Override
            public void reInitialize() {
                issueRequest(requestType, selectedZoneIds, null, null);
            }
        }, getResources().getString(R.string.curated_deal_loader), false);

        CachedDataInterface callback = new CachedDataInterface() {
            @Override
            public void onSuccess(JSONObject response) {
                onIssueRequestSuccess(response);
            }

            @Override
            public void onError(Exception e) {
                onIssueRequestFailure(e);
            }
        };

        getDataFromIntentAndMakeRequest(getIntent());

        mLocationHelper = new LocationHelper(aActivity, this);
        //dateSpotListPreferenceSlidingMenu = new DateSpotPreferenceSlidingMenuHandler(this, match_id, mLocationHelper);
        dateSpotListPreferenceSlidingMenu = new VenueSpotPreferenceSlidingMenuHandler(this, mLocationHelper, TrulyMadlyEvent.TrulyMadlyActivities.datespot_list, match_id);

        SPHandler.setString(aContext, ConstantsSP.SHARED_KEY_LAST_DEAL_VIEWED_TIMESTAMP, TimeUtils.getFormattedTimeGMT());
    }

    private void toggleLocationPreferences() {
        isLocationPreferencesVisible = !isLocationPreferencesVisible;
        dateSpotListPreferenceSlidingMenu.getMenu().toggle(true);
        TmLogger.d(TAG, "isLocationPreferencesVisible : " + isLocationPreferencesVisible);
    }

    @Override
    public void onBackPressed() {
        if (isLocationPreferenceTutorialVisible) {
            hideDateSpotLocationPreferenceTutorial();
        } else if (isLocationPreferencesVisible) {
            toggleLocationPreferences();
        } else {
            super.onBackPressed();
        }
    }

    private void getDataFromIntentAndMakeRequest(Intent intent) {
        Bundle b = intent.getExtras();
        if (b != null) {
            match_id = b.getString("match_id");
            message_one_one_conversion_url = b
                    .getString("message_one_one_conversion_url");
            boolean cameFromPushNotification = b.getBoolean("isPush", false);
        }
        issueRequest(DATE_SPOT_LIST_REQUEST_TYPE.GET_DEALS, null, null, null);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        getDataFromIntentAndMakeRequest(intent);
    }


    private void issueRequest(DATE_SPOT_LIST_REQUEST_TYPE type, ArrayList<String> selectedZoneIds, String eventLabel, Location location) {

        this.requestType = type;
        this.selectedZoneIds = selectedZoneIds != null ? selectedZoneIds : new ArrayList<String>();

        if (tapToRetryHandler != null) {
            tapToRetryHandler.showLoader();
        }

        String url = ConstantsUrls.get_datespotListUrl();
        HashMap<String, String> params = new HashMap<>();
        params.put("match_id", match_id);
        if (type == DATE_SPOT_LIST_REQUEST_TYPE.GET_DEALS) {
            if (actionBarHandler != null) {
                actionBarHandler.toggleLocationIcon(false);
            }
            params.put("action", "get_deals");
        } else if (type == DATE_SPOT_LIST_REQUEST_TYPE.SAVE_PREFERENCE) {
            params.put("action", "save_preference");
            if (location != null) {
                Log.d(TAG, "mlocation");
                params.put("lat", "" + location.getLatitude());
                params.put("lon", "" + location.getLongitude());
            } else if (selectedZoneIds != null) {
                for (int i = 0; i < selectedZoneIds.size(); i++) {
                    if (!selectedZoneIds.get(i).equals("-1")) {
                        params.put("zones[" + i + "]", selectedZoneIds.get(i));
                    }
                }
            }
        }

        //Uncomment this when adding caching
        /*
        boolean showFailure = CachedDataHandler.showDataFromDb(aContext, url, callback);
        CachedDataHandler cachedDataHandler = new CachedDataHandler(url, callback, this, TrulyMadlyEvent.TrulyMadlyActivities.datespot_list, TrulyMadlyEvent.TrulyMadlyEventTypes.page_load, null, showFailure);
        params.put("hash", CachingDBHandler.getHashValue(aContext, url, Utility.getMyId(aContext)));
        cachedDataHandler.httpGet(this, url, params);
        */

        //Comment this when add caching
        String event_type = TrulyMadlyEvent.TrulyMadlyEventTypes.page_load;
        if (type == DATE_SPOT_LIST_REQUEST_TYPE.SAVE_PREFERENCE) {
            event_type = TrulyMadlyEvent.TrulyMadlyEventTypes.save_datespot_preferences;
        }

        HashMap<String, String> eventInfo = new HashMap<>();
        eventInfo.put("nudge_shown", TrulyMadlyApplication.cdChatTutorialShownInThisSession ? "true" : "false");
        CustomOkHttpResponseHandler responseHandler = new CustomOkHttpResponseHandler(aContext,
                TrulyMadlyEvent.TrulyMadlyActivities.datespot_list, event_type, eventLabel, eventInfo) {

            @Override
            public void onRequestSuccess(JSONObject response) {
                isRequestIsProgress = false;
                onIssueRequestSuccess(response);
            }

            @Override
            public void onRequestFailure(Exception exception) {
                isRequestIsProgress = false;
                onIssueRequestFailure(exception);
            }

        };
        isRequestIsProgress = true;
        if (type == DATE_SPOT_LIST_REQUEST_TYPE.GET_DEALS) {
            OkHttpHandler.httpGet(aContext, url, params, responseHandler);
        } else if (type == DATE_SPOT_LIST_REQUEST_TYPE.SAVE_PREFERENCE) {
            OkHttpHandler.httpPost(aContext, url, params, responseHandler);
        }
    }

    private void onIssueRequestFailure(Exception exception) {
        if (tapToRetryHandler != null) {
            tapToRetryHandler.onNetWorkFailed(exception);
        }
    }

    private void onIssueRequestSuccess(JSONObject response) {
        if (tapToRetryHandler != null) {
            tapToRetryHandler.onSuccessFull();
        }
        zones = DateSpotParser.parseDateSpotListForZonesResponse(response);

        for (DateSpotZone zone :
                zones) {
            if (selectedZoneIds.contains(checkNotNull(zone).getZoneId())) {
                checkNotNull(zone).setIsSelected(true);
            } else {
                checkNotNull(zone).setIsSelected(false);
            }
        }

        zonesCity = response.optString("city_name");
        int selectedZoneCount = dateSpotListPreferenceSlidingMenu.setZoneData(zonesCity, zones);
        //actionBarHandler.showDateSpotLocationPreferenceBadge(selectedZoneCount);


        showDateSpotList(DateSpotParser.parseDateSpotListResponse(response));
    }

    private void showDateSpotList(ArrayList<DateSpotModal> dateSpotList) {
        this.dateSpotList = dateSpotList;
        actionBarHandler.toggleLocationIcon(true);
        showDateSpotLocationPreferenceTutorial();
        if (dateSpotList.size() > 0) {
            date_spot_list_view.setVisibility(View.VISIBLE);
            no_dates.setVisibility(View.GONE);
            date_spot_list_view.setLayoutManager(new LinearLayoutManager(this, LinearLayout.VERTICAL, false));
            date_spot_list_view.setAdapter(new DateSpotListAdapter(aActivity, dateSpotList, this));
        } else {
            no_dates.setVisibility(View.VISIBLE);
            date_spot_list_view.setVisibility(View.GONE);
        }
    }

    private void showDateSpotLocationPreferenceTutorial() {
        if (!RFHandler.getBool(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_DATESPOT_PREFERENCE_TUTORIAL_SHOWN)) {
            isLocationPreferenceTutorialVisible = true;
            date_spot_preference_tutorial.setVisibility(View.VISIBLE);
            Animation anim = AnimationUtils.loadAnimation(aContext, R.anim.slide_down_tutorial_date_spot_location_preferences);
            anim.setFillAfter(true);
            date_spot_preference_tutorial_content.setVisibility(View.VISIBLE);
            date_spot_preference_tutorial_content
                    .startAnimation(anim);
            RFHandler.insert(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_DATESPOT_PREFERENCE_TUTORIAL_SHOWN, true);
        }

    }

    @OnClick(R.id.date_spot_preference_tutorial)
    public void hideDateSpotLocationPreferenceTutorial() {
        isLocationPreferenceTutorialVisible = false;
        date_spot_preference_tutorial_content.setVisibility(View.GONE);
        date_spot_preference_tutorial.setVisibility(View.GONE);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    @Override
    public void onDateSpotItemClicked(String dateSpotId) {
        for (DateSpotModal dateSpot :
                dateSpotList) {
            if (Utility.stringCompare(dateSpotId, dateSpot.getId())) {
                ActivityHandler.startDateSpotActivity(aContext, dateSpotId, null, match_id, message_one_one_conversion_url, dateSpot.isMissTmRecommends(), dateSpot.isNew(),
                        dateSpot.getLocationScore(), dateSpot.getPopularityScore(), dateSpot.getTotalScore(), dateSpot.getPricingValue() + "", dateSpot.getZone_id(),
                        dateSpot.getGeo_location(), dateSpot.getRank());
                return;
            }
        }
        ActivityHandler.startDateSpotActivity(aContext, dateSpotId, null, match_id, message_one_one_conversion_url);
    }

    @Override
    public void onClose() {
        isLocationPreferencesVisible = false;

    }

    @Override
    public void onClosed() {
        isLocationPreferencesVisible = false;
        if (!isRequestIsProgress) {
            //Reset to whatever zone data was initially setString.
            dateSpotListPreferenceSlidingMenu.setZoneData(zonesCity, zones);
        }

    }

    @Override
    public void onOpened() {
        isLocationPreferencesVisible = true;
    }

    @Override
    public void onSaveClicked(ArrayList<String> selectedZoneIds) {
        toggleLocationPreferences();
        String eventLabel = null;
        for (DateSpotZone zone :
                zones) {
            if (selectedZoneIds.contains(zone.getZoneId())) {
                if (eventLabel == null) {
                    eventLabel = zone.getName();
                } else {
                    eventLabel += ", " + zone.getName();
                }
            }
        }

        // tracking
        String selectedZones = "";
        int len = selectedZoneIds.size();
        if (len > 0) {
            for (int i = 0; i < len - 1; i++)
                selectedZones += selectedZoneIds.get(i) + ",";
            selectedZones += selectedZoneIds.get(len - 1);
        }

        HashMap<String, String> eventInfo = new HashMap<>();
        eventInfo.put("Location", selectedZoneIds.contains("-2") ? "nearby" : selectedZones);
        eventInfo.put("nudge_shown", TrulyMadlyApplication.cdChatTutorialShownInThisSession ? "true" : "false");

        TrulyMadlyTrackEvent.trackEvent(aContext,
                TrulyMadlyEvent.TrulyMadlyActivities.datespot_list, TrulyMadlyEvent.TrulyMadlyEventTypes.filter_applied, 0, match_id
                , eventInfo, true);


        //Not calling api in case of location
        if (!selectedZoneIds.get(0).equals("-2")) {
            issueRequest(DATE_SPOT_LIST_REQUEST_TYPE.SAVE_PREFERENCE, selectedZoneIds, eventLabel, null);
        }

        actionBarHandler.setLocationBlooperVisibility(!selectedZoneIds.get(0).equals("-1"));

        if (!selectedZoneIds.contains("-1") && !selectedZoneIds.contains("-2")) {
            for (DateSpotZone zone : zones) {
                zone.setIsSelected(selectedZoneIds.contains(zone.getZoneId()));
            }
        } else {
            for (DateSpotZone zone : zones) {
                zone.setIsSelected(false);
            }
        }
    }

    @Override
    public void onLocationRecived(Location location) {
        Log.d(TAG, "onLocationRecieved");
        issueRequest(DATE_SPOT_LIST_REQUEST_TYPE.SAVE_PREFERENCE, null, null, location);
    }

    @SuppressLint({"Override", "NewApi"})
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (grantResults == null || grantResults.length == 0) {
            return;
        }
        switch (requestCode) {
            case PermissionsHelper.REQUEST_FINE_LOCATION_ACCESS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //TODO: Permission: Modify this line to have a better implementation
                    Log.d("permisssion", "granted ");
                    mLocationHelper.onPermissionGranted();
                } else {
                    boolean neverAskAgain = !ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[0]);
                    if (neverAskAgain) {
                        PermissionsHelper.handlePermissionDenied(this, R.string.permission_denied_location);
                    } else {
                        AlertsHandler.showMessageWithAction(this, R.string.permission_denied_location,
                                R.string.permission_rejection_action, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        PermissionsHelper.checkAndAskForPermission(aActivity,
                                                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                                                        Manifest.permission.ACCESS_FINE_LOCATION},
                                                PermissionsHelper.REQUEST_FINE_LOCATION_ACCESS);
                                    }
                                }, false, 3);
                    }
                }
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    private enum DATE_SPOT_LIST_REQUEST_TYPE {
        GET_DEALS, SAVE_PREFERENCE
    }

}
