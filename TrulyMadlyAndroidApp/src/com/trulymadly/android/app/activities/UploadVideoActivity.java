package com.trulymadly.android.app.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.moe.pushlibrary.MoEHelper;

/**
 * Created by deveshbatra on 7/20/16.
 */
public class UploadVideoActivity extends AppCompatActivity {

    private MoEHelper mhelper = null;
    private Activity aActivity;
    private Context aContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        aActivity = this;
        aContext = this;
        mhelper = new MoEHelper(this);

    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}
