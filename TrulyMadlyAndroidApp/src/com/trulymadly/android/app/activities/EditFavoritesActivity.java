package com.trulymadly.android.app.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.moe.pushlibrary.MoEHelper;
import com.squareup.otto.Subscribe;
import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.adapter.EditFavoritesFragmentPagerAdapter;
import com.trulymadly.android.app.bus.AddFavoriteOnServerEvent;
import com.trulymadly.android.app.bus.BusProvider;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.listener.CachedDataInterface;
import com.trulymadly.android.app.listener.ConfirmDialogInterface;
import com.trulymadly.android.app.listener.CustomOkHttpResponseHandler;
import com.trulymadly.android.app.listener.OnActionBarClickedInterface;
import com.trulymadly.android.app.listener.TapToRetryListener;
import com.trulymadly.android.app.modal.FavoriteDataModal;
import com.trulymadly.android.app.sqlite.CachingDBHandler;
import com.trulymadly.android.app.sqlite.MatchesDbHandler;
import com.trulymadly.android.app.utility.ActionBarHandler;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.CachedDataHandler;
import com.trulymadly.android.app.utility.FavoriteUtils;
import com.trulymadly.android.app.utility.OkHttpHandler;
import com.trulymadly.android.app.utility.TapToRetryHandler;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities.favorites;

public class EditFavoritesActivity extends AppCompatActivity {

    public static final String POSITION = "POSITION";
    public static final String IS_TAB_VISIBLE = "IS_TAB_VISIBLE";
    private static final String TAG = "EditFavoritesActivity";
    private static boolean isCreated = false;
    @BindView(R.id.edit_favorites_tab_layout_container)
    View edit_favorites_tab_layout_container;
    @BindView(R.id.edit_favorites_tab_layout)
    TabLayout edit_favorites_tab_layout;
    @BindView(R.id.edit_favorites_view_pager)
    ViewPager edit_favorites_view_pager;
    @BindView(R.id.save_favorites_button)
    Button saveFavorites;
    HashMap<String, ArrayList<FavoriteDataModal>> favoritesDataMap;
    private MoEHelper mhelper;
    private Context aContext;
    private TapToRetryHandler tapToRetryHandler;
    private boolean isTabVisible = false;
    private EditFavoritesFragmentPagerAdapter editFavoritesPagerAdapter;
    private ArrayList<FavoriteDataModal> addQueue, removeQueue;
    private ProgressDialog mProgressDialog;
    private WeakReference<Activity> activityWeakReference;
    private int moveTabToItemAfterLoading = 0;
    private boolean isActivityForResult, fromMyProfile, fromMatches;
    private int launchPosition;
    private Map<String, String> eventInfo;
    private String mFavoriteActivitySource;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        mhelper = new MoEHelper(this);
        try {
            setContentView(R.layout.edit_favorites_layout);
            ButterKnife.bind(this);
        } catch (OutOfMemoryError e) {
            System.gc();
            finish();
            return;
        }
        if (savedInstanceState != null) {
            return;
        }

        Intent i = getIntent();
        Bundle b = i.getExtras();
        if (b != null) {
            isActivityForResult = b.getBoolean("isActivityForResult", false);
            launchPosition = b.getInt("launchPosition", 0);
            fromMyProfile = b.getBoolean("fromMyProfile", false);
            fromMatches = b.getBoolean("fromMatches", false);
        }
        mFavoriteActivitySource = fromMyProfile ? "my_profile" : fromMatches ? "matches" : "other";

        eventInfo = new HashMap<>();
        eventInfo.put("source", mFavoriteActivitySource);


        aContext = this;
        activityWeakReference = new WeakReference<>((Activity) this);
        isCreated = true;

        tapToRetryHandler = new TapToRetryHandler(this,
                findViewById(android.R.id.content), new TapToRetryListener() {
            @Override
            public void reInitialize() {
                fetchFavoritesData();
            }
        }, null);
        int titleResId = isActivityForResult ? R.string.add_more_favorites : R.string.edit_favorites;
        ActionBarHandler actionBarHandler = new ActionBarHandler(this, getResources().getString(titleResId), null, new OnActionBarClickedInterface() {

            @Override
            public void onUserProfileClicked() {
            }


            @Override
            public void onCuratedDealsClicked() {

            }

            @Override
            public void onConversationsClicked() {
            }

            @Override
            public void onLocationClicked() {
            }

            @Override
            public void onBackClicked() {
                onBackPressed();

            }

            @Override
            public void onTitleClicked() {
            }

            @Override
            public void onTitleLongClicked() {
            }

            @Override
            public void onSearchViewOpened() {

            }

            @Override
            public void onSearchViewClosed() {

            }

            @Override
            public void onCategoriesClicked() {

            }

        }, false, false, false);

        actionBarHandler.toggleBackButton(!isActivityForResult);


        fetchFavoritesData();
    }

    @Override
    protected void onDestroy() {
        isCreated = false;
        super.onDestroy();
    }

    private void fetchFavoritesData() {
        tapToRetryHandler.showLoader();
        String requestUrl = ConstantsUrls.get_favorites_url();

        CachedDataInterface cachedDataInterface = new CachedDataInterface() {
            @Override
            public void onSuccess(JSONObject response) {
                if (isCreated) {
                    tapToRetryHandler.onSuccessFull();
                    if (!showFavoritesPage(response)) {
                        onError(new IOException());
                    }
                }
            }

            @Override
            public void onError(Exception e) {
                if (isCreated) {
                    tapToRetryHandler.onNetWorkFailed(e);
                }

            }
        };


        boolean showFailure = CachedDataHandler.showDataFromDb(aContext, requestUrl, cachedDataInterface);
        CachedDataHandler cachedDataHandler = new CachedDataHandler(requestUrl, cachedDataInterface, this, favorites, TrulyMadlyEvent.TrulyMadlyEventTypes.page_load, eventInfo, showFailure);
        Map<String, String> map = new HashMap<>();
        map.put("hash", CachingDBHandler.getHashValue(aContext, requestUrl, Utility.getMyId(aContext)));
        map.put("action", "get_likes");
        cachedDataHandler.httpGet(this, requestUrl, map);

    }

    private boolean showFavoritesPage(JSONObject response) {

        try {
            JSONObject currentLikes = response.getJSONObject("likes");
            launchFavoritesTabs(currentLikes);
        } catch (JSONException e) {
            return false;
        }
        return true;
    }

    private void launchFavoritesTabs(JSONObject likesData) {

        favoritesDataMap = FavoriteUtils.parseFavorites(likesData, true, true, true);

        edit_favorites_tab_layout_container.setVisibility(View.VISIBLE);
        // Get the ViewPager and set it's PagerAdapter so that it can display items
        editFavoritesPagerAdapter = new EditFavoritesFragmentPagerAdapter(getSupportFragmentManager(),
                aContext, favoritesDataMap, mFavoriteActivitySource);
        edit_favorites_view_pager.setAdapter(editFavoritesPagerAdapter);
        //Doing this as there is some leak when fragments are re-created, This will ensure all 5 fragments are in memory and stay in memory only once.
        edit_favorites_view_pager.setOffscreenPageLimit(4);

        edit_favorites_tab_layout.setupWithViewPager(edit_favorites_view_pager);

        // Iterate over all tabs and set the custom view
        for (int i = 0; i < edit_favorites_tab_layout.getTabCount(); i++) {
            edit_favorites_tab_layout.getTabAt(i).setCustomView(editFavoritesPagerAdapter.createTabView(i, i == 0));
        }

        edit_favorites_view_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    TrulyMadlyTrackEvent.trackEvent(aContext, favorites, TrulyMadlyEvent.TrulyMadlyEventTypes.movies_click, 0, null, eventInfo, true);
                } else if (position == 1) {
                    TrulyMadlyTrackEvent.trackEvent(aContext, favorites, TrulyMadlyEvent.TrulyMadlyEventTypes.music_click, 0, null, eventInfo, true);
                } else if (position == 2) {
                    TrulyMadlyTrackEvent.trackEvent(aContext, favorites, TrulyMadlyEvent.TrulyMadlyEventTypes.books_click, 0, null, eventInfo, true);
                } else if (position == 3) {
                    TrulyMadlyTrackEvent.trackEvent(aContext, favorites, TrulyMadlyEvent.TrulyMadlyEventTypes.food_click, 0, null, eventInfo, true);
                } else if (position == 4) {
                    TrulyMadlyTrackEvent.trackEvent(aContext, favorites, TrulyMadlyEvent.TrulyMadlyEventTypes.others_click, 0, null, eventInfo, true);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        edit_favorites_tab_layout.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(edit_favorites_view_pager) {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                super.onTabSelected(tab);
                UiUtils.hideKeyBoard(aContext);
                editFavoritesPagerAdapter.updateTabView(tab.getCustomView(), tab.getPosition(), true);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                super.onTabUnselected(tab);
                editFavoritesPagerAdapter.updateTabView(tab.getCustomView(), tab.getPosition(), false);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                super.onTabReselected(tab);
            }
        });


        isTabVisible = true;

        if (moveTabToItemAfterLoading != 0 && moveTabToItemAfterLoading <= 4) {
            edit_favorites_view_pager.setCurrentItem(moveTabToItemAfterLoading);
            moveTabToItemAfterLoading = 0;
        } else if (launchPosition != 0 && launchPosition <= 4) {
            edit_favorites_view_pager.setCurrentItem(launchPosition);
            launchPosition = 0;
        }

    }

    @Override
    public void onBackPressed() {
        if (saveFavorites.isEnabled()) {
            AlertsHandler.showConfirmDialog(aContext, R.string.favorites_not_saved, R.string.save_now, R.string.dont_save, new ConfirmDialogInterface() {
                @Override
                public void onPositiveButtonSelected() {
                    onSaveClicked();
                }

                @Override
                public void onNegativeButtonSelected() {
                    saveFavorites.setEnabled(false);
                    onBackPressed();

                }
            }, true);

        } else {
            if (isActivityForResult) {
                finishWithResult();
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        BusProvider.getInstance().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        BusProvider.getInstance().unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(POSITION, edit_favorites_tab_layout.getSelectedTabPosition());
        outState.putBoolean(IS_TAB_VISIBLE, isTabVisible);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        isTabVisible = savedInstanceState.getBoolean(IS_TAB_VISIBLE);
        if (isTabVisible) {
            edit_favorites_view_pager.setCurrentItem(savedInstanceState.getInt(POSITION));
        }
    }

    @Subscribe
    public void updateFavoriteOnServer(AddFavoriteOnServerEvent addFavoriteOnServerEvent) {
        if (addQueue == null) {
            addQueue = new ArrayList<>();
        }
        if (removeQueue == null) {
            removeQueue = new ArrayList<>();
        }
        FavoriteDataModal favDataModal = addFavoriteOnServerEvent.getFavoriteDataModal();
        if (addFavoriteOnServerEvent.isAdded()) {
            if (!addQueue.contains(favDataModal) && !removeQueue.contains(favDataModal)) {
                addQueue.add(favDataModal);
            }
            removeQueue.remove(favDataModal);
        } else {
            if (!addQueue.contains(favDataModal) && !removeQueue.contains(favDataModal)) {
                removeQueue.add(favDataModal);
            }
            addQueue.remove(favDataModal);
        }
        toggleSaveButton(addQueue.size() > 0 || removeQueue.size() > 0);

    }

    private void toggleSaveButton(boolean toEnable) {
        saveFavorites.setEnabled(toEnable);
    }

    @OnClick(R.id.save_favorites_button)
    void onSaveClicked() {

        boolean hasAdd = addQueue != null && addQueue.size() > 0;
        boolean hasRemove = removeQueue != null && removeQueue.size() > 0;
        int items_added = 0, items_removed = 0;

        String addList = "", removeList = "";
        if (hasAdd) {
            items_added = addQueue.size();
            for (FavoriteDataModal f : addQueue) {
                if (Utility.isSet(addList)) {
                    addList += ",";
                }
                addList += f.getId();
            }
        }
        if (hasRemove) {
            items_removed = removeQueue.size();
            for (FavoriteDataModal f : removeQueue) {
                if (Utility.isSet(removeList)) {
                    removeList += ",";
                }
                removeList += f.getId();
            }
        }


        if (!hasAdd && !hasRemove) {
            toggleSaveButton(false);
            return;
        }
        eventInfo.put("items_added", String.valueOf(items_added));
        eventInfo.put("items_removed", String.valueOf(items_removed));

        mProgressDialog = UiUtils.showProgressBar(aContext, mProgressDialog, R.string.saving_favorites);
        CustomOkHttpResponseHandler customOkHttpResponseHandler = new CustomOkHttpResponseHandler(aContext, favorites, TrulyMadlyEvent.TrulyMadlyEventTypes.favorites_save_server_call, eventInfo) {
            @Override
            public void onRequestSuccess(JSONObject responseJSON) {
                mProgressDialog = UiUtils.hideProgressBar(mProgressDialog);
                addQueue.clear();
                removeQueue.clear();
                toggleSaveButton(false);
                MatchesDbHandler.clearMatchesCache(aContext);
                if (isActivityForResult) {
                    finishWithResult();
                } else {
                    editFavoritesPagerAdapter.removeAllFragments();
                    edit_favorites_view_pager.removeAllViews();
                    moveTabToItemAfterLoading = edit_favorites_view_pager.getCurrentItem();
                    fetchFavoritesData();
                }
            }

            @Override
            public void onRequestFailure(Exception exception) {
                mProgressDialog = UiUtils.hideProgressBar(mProgressDialog);
                if (activityWeakReference != null && activityWeakReference.get() != null) {
                    AlertsHandler.showNetworkError(activityWeakReference.get(), exception);
                }
            }
        };


        Map<String, String> postParams = new HashMap<>();
        postParams.put("action", "save_likes");

        try {
            JSONObject dataParams = new JSONObject();
            if (hasAdd) {
                JSONObject addParams = new JSONObject();

                for (FavoriteDataModal addQueueItem : addQueue) {
                    JSONObject addItemData = new JSONObject();
                    if (addQueueItem.getId().startsWith("ugc")) {
                        addItemData.put("name", addQueueItem.getName());
                        addItemData.put("cat", addQueueItem.getCategory());
                    }
                    addParams.put(addQueueItem.getId(), addItemData);
                }
                dataParams.put("add", addParams);
                //urlParams.put("add", addList);
            }
            if (hasRemove) {
                JSONObject removeParams = new JSONObject();
                for (FavoriteDataModal removeQueueItem : removeQueue) {
                    JSONObject removeItemData = new JSONObject();
                    if (removeQueueItem.getId().startsWith("ugc")) {
                        removeItemData.put("name", removeQueueItem.getName());
                        removeItemData.put("cat", removeQueueItem.getCategory());
                    }
                    removeParams.put(removeQueueItem.getId(), removeItemData);
                }
                dataParams.put("remove", removeParams);
                //urlParams.put("remove", removeList);
            }
            postParams.put("data", dataParams.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        OkHttpHandler.httpPost(aContext, ConstantsUrls.get_favorites_url(), postParams, customOkHttpResponseHandler);

    }

    private void finishWithResult() {
        if (isActivityForResult) {
            Intent intent = new Intent();
            //intent.putExtra("isVerified", tbData != null && tbData.isVerified());
            setResult(RESULT_OK, intent);
            finish();
        }
    }
}
