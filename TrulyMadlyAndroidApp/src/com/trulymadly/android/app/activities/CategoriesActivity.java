package com.trulymadly.android.app.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.moe.pushlibrary.MoEHelper;
import com.squareup.picasso.Picasso;
import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.adapter.CategoryListAdapter;
import com.trulymadly.android.app.adapter.VenueListPagerAdapter;
import com.trulymadly.android.app.json.CategoryListResponseParser;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.ConstantsRF;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.json.VenueListResponseParser;
import com.trulymadly.android.app.listener.CategoryListItemClickInterface;
import com.trulymadly.android.app.listener.CustomOkHttpResponseHandler;
import com.trulymadly.android.app.listener.LocationFetchedInterface;
import com.trulymadly.android.app.listener.OnActionBarClickedInterface;
import com.trulymadly.android.app.listener.SaveZonesInterface;
import com.trulymadly.android.app.listener.TapToRetryListener;
import com.trulymadly.android.app.modal.CategoryModal;
import com.trulymadly.android.app.modal.DateSpotZone;
import com.trulymadly.android.app.modal.VenueModal;
import com.trulymadly.android.app.sqlite.RFHandler;
import com.trulymadly.android.app.utility.ActionBarHandler;
import com.trulymadly.android.app.utility.ActivityHandler;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.LocationHelper;
import com.trulymadly.android.app.utility.OkHttpHandler;
import com.trulymadly.android.app.utility.PermissionsHelper;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.TapToRetryHandler;
import com.trulymadly.android.app.utility.TimeUtils;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;
import com.trulymadly.android.app.utility.VenueSpotPreferenceSlidingMenuHandler;

import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by deveshbatra on 3/8/16.
 */
public class CategoriesActivity extends AppCompatActivity implements SlidingMenu.OnCloseListener,
        SlidingMenu.OnClosedListener,
        SlidingMenu.OnOpenedListener,
        SaveZonesInterface, LocationFetchedInterface {
    public static final boolean SHOW_FOLLOWING_TAB = false;
    private static final int CATEGORY_LIST = 1;
    private static final int VENUE_LIST = 2;
    private static final int FOLLOWING_VENUE_LIST = 3;
    public static boolean isCreated = false;
    private static boolean isResumed = false;
    @BindView(R.id.category_list_view)
    RecyclerView category_list_view;
    @BindView(R.id.main_category_layout)
    View main_category_layout;
    @BindView(R.id.fragment_pager)
    ViewPager fragment_pager;
    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.custom_prog_bar_id)
    View custom_prog_bar_id;
    @BindView(R.id.no_categories_tv)
    TextView no_categories_tv;
    @BindView(R.id.main_no_events_icon)
    ImageView main_no_events_icon;
    @BindView(R.id.music_icon)
    ImageView music_icon;
    @BindView(R.id.face_icon)
    ImageView face_icon;
    private int fragment_selected;
    private CategoryListItemClickInterface categoryListItemClickInterface;
    private Animation mShakeLeftToRightAnimation, mShakeRightToLeftAnimation;
    private Context aContext;
    //private Activity aActivity;
    private MoEHelper mHelper = null;
    private ArrayList<CategoryModal> eventList;
    private ActionBarHandler actionBarHandler;
    private boolean isVenueFragmentVisible;
    private ArrayList<DateSpotZone> zones;
    private boolean isLocationPreferencesVisible;
    private ArrayList<VenueModal> venueList;
    private ArrayList<VenueModal> followingVenueList;
    private TapToRetryHandler tapToRetryHandler;
    private VenueSpotPreferenceSlidingMenuHandler venueSpotPreferenceSlidingMenuHandler = null;
    private String category_id, city_name, category_name;
    private String selectedZones;
    private Location mLocation;
    private VenueListPagerAdapter adapter;
    private boolean isRecommendedListInProgress = false;
    private String pushCategoryId;
    private boolean isFollowingVenueListInProgress = false;
    private LocationHelper mLocationHelper;
    private boolean ab_enabled;
    private HashMap<String, String> event_info;

    private android.app.FragmentManager mFragmentManager;
    private WeakReference<Activity> mWeakActivity;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isCreated = true;
        aContext = this;
        //aActivity = this;
        mWeakActivity = new WeakReference<>((Activity) this);
        mHelper = new MoEHelper(this);
        setContentView(R.layout.categories_list_layout);
        ButterKnife.bind(this);
        Intent i = getIntent();
        Bundle b = i.getExtras();
        if (b != null) {
            pushCategoryId = b.getString("push_category_id");
            category_id = pushCategoryId;
            category_name = b.getString("push_category_name");
        }

        OnActionBarClickedInterface showFilterClickedInterface = new OnActionBarClickedInterface() {
            @Override
            public void onBackClicked() {
                onBackPressed();
            }

            @Override
            public void onUserProfileClicked() {

            }

            @Override
            public void onConversationsClicked() {
            }

            @Override
            public void onLocationClicked() {
                toggleLocationPreferences();
            }

            @Override
            public void onTitleClicked() {
            }

            @Override
            public void onTitleLongClicked() {
            }

            @Override
            public void onSearchViewOpened() {
            }

            @Override
            public void onSearchViewClosed() {

            }

            @Override
            public void onCategoriesClicked() {

            }

            @Override
            public void onCuratedDealsClicked() {

            }
        };

        categoryListItemClickInterface = new CategoryListItemClickInterface() {


            @Override
            public void onCategoryItemClicked(String cat_id, String name) {
                if (!isCreated) {
                    return;
                }

                category_id = cat_id;
                category_name = name;
                adapter.setCategoryId(category_id);
                adapter.notifyDataSetChanged();
                fragment_selected = 2;

            }
        };
        actionBarHandler = new ActionBarHandler(this, getResources().getString(R.string.category_list),
                null, showFilterClickedInterface, false, false, false, false, false);
        actionBarHandler.toggleLocationIcon(false);
        mLocationHelper = new LocationHelper(this, this);
        venueSpotPreferenceSlidingMenuHandler = new VenueSpotPreferenceSlidingMenuHandler(this, mLocationHelper, TrulyMadlyEvent.TrulyMadlyActivities.scenes);

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        adapter = new VenueListPagerAdapter
                (getSupportFragmentManager(), SHOW_FOLLOWING_TAB ? 2 : 1, venueList, followingVenueList);
        fragment_pager.setAdapter(adapter);
        TabLayout.Tab tab1 = tabLayout.newTab();
        TabLayout.Tab tab2 = tabLayout.newTab();
        tabLayout.addTab(tab1);
        if (SHOW_FOLLOWING_TAB) {
            tabLayout.addTab(tab2);
        }
        setTabHeaders();
        tabLayout.setupWithViewPager(fragment_pager);
        tabLayout.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(fragment_pager) {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                super.onTabSelected(tab);
                if (tab.getPosition() == 0) {
                    setRecommendedList();

                } else if (tab.getPosition() == 1) {
                    fragment_pager.setCurrentItem(1);
                    if (followingVenueList == null) {
                        fragment_selected = 3;
                        issueRequest(FOLLOWING_VENUE_LIST);
                    } else {
                        setTabHeaders();
                        showFragment();
                        showFollowingVenueFragment();
                    }


                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {


            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {


            }
        });
        tabLayout.setVisibility(SHOW_FOLLOWING_TAB ? View.VISIBLE : View.GONE);
        UiUtils.setBackground(aContext, custom_prog_bar_id, R.drawable.blur_background);
        Picasso.with(aContext).load(R.drawable.loader_music).noFade().into(music_icon);
        Picasso.with(aContext).load(R.drawable.loader_face).noFade().into(face_icon);
        Picasso.with(aContext).load(R.drawable.expired_event).noFade().into(main_no_events_icon);

        mShakeLeftToRightAnimation = AnimationUtils.loadAnimation(aContext, R.anim.shake_from_left_right);
        mShakeRightToLeftAnimation = AnimationUtils.loadAnimation(aContext, R.anim.shake_from_right_left);

        tapToRetryHandler = new TapToRetryHandler(this, findViewById(android.R.id.content), new TapToRetryListener() {
            @Override
            public void reInitialize() {

                if (fragment_selected == 2)
                    issueRequest(VENUE_LIST);
                else if (fragment_selected == 3)
                    issueRequest(FOLLOWING_VENUE_LIST);
                else
                    issueRequest(CATEGORY_LIST);
            }
        }, getResources().getString(R.string.meetups_loader_message), false);

        ab_enabled = RFHandler.getBool(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_TM_SCENES_AB_ENABLED);
        if (Utility.isSet(pushCategoryId)) {
            fragment_selected = 3;
            issueRequest(FOLLOWING_VENUE_LIST);
        } else if (ab_enabled) {
            // hard coded , need to integrate it with backend
            event_info = new HashMap<>();
            event_info.put("tracking_key", RFHandler.getString(aContext, Utility.getMyId(aContext), ConstantsRF.AB_TRACKING_KEY));
            category_id = RFHandler.getString(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_TM_SCENES_CATEGORY_ID);
            category_name = getResources().getString(R.string.category_list);
            fragment_selected = 2;
            issueRequest(VENUE_LIST);
        } else {
            fragment_selected = 1;
            issueRequest(CATEGORY_LIST);
        }

    }

    @Override
    protected void onDestroy() {
        Log.d("Latitude", "categories + ondestroy");
        isCreated = false;
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == Constants.REQUEST_CHECK_SETTINGS) {
            Log.d("location", "onActivityResult");
            if (resultCode == RESULT_OK) {
                mLocationHelper = new LocationHelper(this, this);
            } else {
                if (!venueSpotPreferenceSlidingMenuHandler.getPreviousSelectedZoneIds().contains("-1") && !venueSpotPreferenceSlidingMenuHandler.getPreviousSelectedZoneIds().contains("-2"))
                    for (String id : venueSpotPreferenceSlidingMenuHandler.getPreviousSelectedZoneIds()) {
                        DateSpotZone zone = new DateSpotZone();
                        zone.setZoneId(id);
                        zones.get(zones.indexOf(zone)).setIsSelected(true);
                    }
                venueSpotPreferenceSlidingMenuHandler.checkNearBy(false);
                venueSpotPreferenceSlidingMenuHandler.setZoneData(city_name, zones);
                venueSpotPreferenceSlidingMenuHandler.checkNearBy(false);
            }


        }
        if (resultCode == Constants.EVENT_UNFOLLOWED || resultCode == Constants.EVENT_FOLLOWED) {
            venueList = null;
            followingVenueList = null;
            setRecommendedList();
        }

    }

    private void setRecommendedList() {
        fragment_pager.setCurrentItem(0);
        if (venueList == null) {
            fragment_selected = 2;
            issueRequest(VENUE_LIST);
            if (followingVenueList == null || followingVenueList.size() == 0) {
                issueRequest(FOLLOWING_VENUE_LIST, false);
            }
        } else {
            setTabHeaders();
            showFragment();
            showVenueFragment();
        }
    }


    @SuppressLint({"Override", "NewApi"})
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (grantResults == null || grantResults.length == 0) {
            return;
        }
        switch (requestCode) {
            case PermissionsHelper.REQUEST_FINE_LOCATION_ACCESS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //TODO: Permission : Modify this line to have a better implementation
                    Log.d("permisssion", "granted");
                    mLocationHelper.onPermissionGranted();
                } else {
                    if (!venueSpotPreferenceSlidingMenuHandler.getPreviousSelectedZoneIds().contains("-1") && !venueSpotPreferenceSlidingMenuHandler.getPreviousSelectedZoneIds().contains("-2"))
                        for (String id : venueSpotPreferenceSlidingMenuHandler.getPreviousSelectedZoneIds()) {
                            DateSpotZone zone = new DateSpotZone();
                            zone.setZoneId(id);
                            zones.get(zones.indexOf(zone)).setIsSelected(true);
                        }
                    venueSpotPreferenceSlidingMenuHandler.checkNearBy(false);
                    venueSpotPreferenceSlidingMenuHandler.setZoneData(city_name, zones);
                    boolean neverAskAgain = !ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[0]);
                    if (neverAskAgain) {
                        PermissionsHelper.handlePermissionDenied(this, R.string.permission_denied_location);
                    } else {
                        AlertsHandler.showMessageWithAction(this, R.string.permission_denied_location,
                                R.string.permission_rejection_action, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Activity activity = mWeakActivity.get();
                                        if (activity != null) {
                                            PermissionsHelper.checkAndAskForPermission(activity,
                                                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                                                            Manifest.permission.ACCESS_FINE_LOCATION},
                                                    PermissionsHelper.REQUEST_FINE_LOCATION_ACCESS);
                                        }
                                    }
                                }, false, 3);
                    }
                }
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    private void issueRequest(final int n) {
        issueRequest(n, true);
    }

    private void issueRequest(final int n, final boolean updateFragment) {

        final long startTime = System.currentTimeMillis();
        CustomOkHttpResponseHandler okHttpResponseHandler = new CustomOkHttpResponseHandler(aContext) {

            @Override
            public void onRequestSuccess(JSONObject response) {
                if (!isCreated) {
                    return;
                }

                if (tapToRetryHandler != null) {
                    tapToRetryHandler.onSuccessFull();
                    mShakeLeftToRightAnimation.cancel();
                    mShakeRightToLeftAnimation.cancel();
                }
                Activity activity = mWeakActivity.get();
                if (activity == null) {
                    return;
                }
                Context context = activity.getApplicationContext();

                long endTime = System.currentTimeMillis();
                RFHandler.insert(context, Utility.getMyId(context), ConstantsRF.RIGID_FIELD_TM_SCENES_NEXT_TSTAMP, TimeUtils.getFormattedTimeGMT());
                switch (n) {

                    case CATEGORY_LIST:
                        CategoryListResponseParser categoryListResponseParser = new CategoryListResponseParser();
                        eventList = categoryListResponseParser.parseResponse(response);
//                        eventList=null;
                        if (eventList != null && eventList.size() > 0) {
                            category_list_view.setVisibility(View.VISIBLE);
                            category_list_view.setLayoutManager(new LinearLayoutManager(activity, LinearLayout.VERTICAL, false));
                            category_list_view.setAdapter(new CategoryListAdapter(activity, eventList, categoryListItemClickInterface));
                            main_no_events_icon.setVisibility(View.GONE);
                            no_categories_tv.setVisibility(View.GONE);
                        } else {
                            main_no_events_icon.setVisibility(View.VISIBLE);
                            no_categories_tv.setVisibility(View.VISIBLE);
                        }


                        TrulyMadlyTrackEvent.trackEvent(context,
                                TrulyMadlyEvent.TrulyMadlyActivities.scenes, TrulyMadlyEvent.TrulyMadlyEventTypes.list_category, endTime - startTime,
                                SPHandler.getString(activity, ConstantsSP.SHARED_KEYS_USER_CITY), event_info, true);
                        break;

                    case VENUE_LIST:
                        isRecommendedListInProgress = false;
                        VenueListResponseParser venueListResponseParser = new VenueListResponseParser();
                        venueListResponseParser.parseResponse(response);
                        if (zones == null) {
                            zones = venueListResponseParser.getZoneList();
                        }
                        venueList = venueListResponseParser.getVenueList();
                        city_name = venueListResponseParser.getCity_name();
                        venueList = venueListResponseParser.getVenueList();
                        int selectedZoneCount = venueSpotPreferenceSlidingMenuHandler.setZoneData(city_name, zones);

                        setTabHeaders();
                        adapter.setVenueList(venueList);
                        adapter.notifyDataSetChanged();
                        showFragment();
                        showVenueFragment();

                        if (followingVenueList == null || followingVenueList.size() == 0) {
                            issueRequest(FOLLOWING_VENUE_LIST, false);
                        }

                        TrulyMadlyTrackEvent.trackEvent(context,
                                TrulyMadlyEvent.TrulyMadlyActivities.scenes, TrulyMadlyEvent.TrulyMadlyEventTypes.list_event, endTime - startTime,
                                category_id, event_info, true);
                        break;
                    case FOLLOWING_VENUE_LIST:
                        isFollowingVenueListInProgress = false;
                        venueListResponseParser = new VenueListResponseParser();
                        venueListResponseParser.parseResponse(response);
                        if (zones == null) {
                            zones = venueListResponseParser.getZoneList();
                        }
                        followingVenueList = venueListResponseParser.getVenueList();

                        setTabHeaders();

                        if (updateFragment) {
                            showFragment();
                            showFollowingVenueFragment();
                        }
                        TrulyMadlyTrackEvent.trackEvent(context,
                                TrulyMadlyEvent.TrulyMadlyActivities.scenes, TrulyMadlyEvent.TrulyMadlyEventTypes.list_following, endTime - startTime,
                                category_id, event_info, true);

                        adapter.setFollwingVenueList(followingVenueList);
                        adapter.notifyDataSetChanged();

                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onRequestFailure(Exception exception) {
                if (tapToRetryHandler != null) {
                    mShakeLeftToRightAnimation.cancel();
                    mShakeRightToLeftAnimation.cancel();
                    tapToRetryHandler.onNetWorkFailed(exception);
                }
                switch (n) {
                    case CATEGORY_LIST:
                        break;
                    case VENUE_LIST:
                        isRecommendedListInProgress = false;
                        break;
                    case FOLLOWING_VENUE_LIST:
                        isFollowingVenueListInProgress = false;
                        break;
                    default:
                        break;
                }

            }


        };
        isVenueFragmentVisible = false;
        tabLayout.setVisibility(View.GONE);
        fragment_pager.setVisibility(View.GONE);
        if (tapToRetryHandler != null) {
            music_icon.startAnimation(mShakeLeftToRightAnimation);
            face_icon.startAnimation(mShakeRightToLeftAnimation);
            tapToRetryHandler.showLoader();
        }

        Map<String, String> params = new HashMap<>();
        params.put("last_seen_scene", RFHandler.getString(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_TM_SCENES_CURRENT_TSTAMP));
        params.put("scenes_ab", "" + ab_enabled);
        switch (n) {
            case CATEGORY_LIST:
                params.put("action", "show_categories");


                break;
            case VENUE_LIST:
                if (isRecommendedListInProgress) {
                    return;
                }
                isRecommendedListInProgress = true;
                params.put("action", "show_events");
                params.put("category_id", category_id);
                params.put("follow_status", "recommended");


                if (mLocation != null) {
                    Log.d("location", "mlocation");
                    params.put("lat", "" + mLocation.getLatitude());
                    params.put("lon", "" + mLocation.getLongitude());
                } else if (Utility.isSet(selectedZones) && !selectedZones.equalsIgnoreCase("-2")) {
                    Log.d("location", "-2");
                    params.put("zone_ids", selectedZones);
                }
                selectedZones = "";
                break;

            case FOLLOWING_VENUE_LIST:
                if (isFollowingVenueListInProgress) {
                    return;
                }
                isFollowingVenueListInProgress = true;
                params.put("action", "show_events");
                params.put("category_id", category_id);
                params.put("follow_status", "following");
                if (Utility.isSet(selectedZones) && !selectedZones.equalsIgnoreCase("-2")) {
                    params.put("zone_ids", selectedZones);
                }
                selectedZones = "";
                break;


            default:
                break;
        }
        OkHttpHandler.httpPost(aContext, ConstantsUrls.get_category_url(), params, okHttpResponseHandler);
    }

    private void setTabHeaders() {
        String tab1Text = "recommended", tab2Text = "interested";
        if (venueList != null && venueList.size() > 0) {
            tab1Text += " (" + venueList.size() + ")";
        }
        tabLayout.getTabAt(0).setText(tab1Text);

        if (SHOW_FOLLOWING_TAB) {
            if (followingVenueList != null && followingVenueList.size() > 0) {
                tab2Text += " (" + followingVenueList.size() + ")";
            }
            tabLayout.getTabAt(1).setText(tab2Text);
        }
    }

    private void showFragment() {
        if (!isVenueFragmentVisible) {
            isVenueFragmentVisible = true;
            tabLayout.setVisibility(SHOW_FOLLOWING_TAB ? View.VISIBLE : View.GONE);
            fragment_pager.setVisibility(View.VISIBLE);
            main_category_layout.setVisibility(View.GONE);
            actionBarHandler.toggleToolbarShadow(false);
        }
    }

    private void toggleLocationPreferences() {
        isLocationPreferencesVisible = !isLocationPreferencesVisible;
        venueSpotPreferenceSlidingMenuHandler.getMenu().toggle(true);
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        isResumed = true;
    }

    @Override
    public void onPause() {
        isResumed = false;
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        if (isLocationPreferencesVisible) {
            toggleLocationPreferences();
        } else if (ab_enabled) {
            ActivityHandler.startMatchesActivity(this);
            finish();
        } else if (Utility.stringCompare(pushCategoryId, "-1")) {
            pushCategoryId = "";
            ActivityHandler.startMatchesActivity(this);
            finish();
        } else if (Utility.isSet(pushCategoryId)) {
            pushCategoryId = "-1";
            showMainLayout();
            fragment_selected = 1;
            issueRequest(CATEGORY_LIST);
        } else if (isVenueFragmentVisible) {
            showMainLayout();
        } else {
            super.onBackPressed();
        }
    }

    private void showMainLayout() {
        fragment_pager.setCurrentItem(0);
        isVenueFragmentVisible = false;
        tabLayout.setVisibility(View.GONE);
        fragment_pager.setVisibility(View.GONE);
        main_category_layout.setVisibility(View.VISIBLE);
        actionBarHandler.toggleToolbarShadow(true);
        venueList = null;
        followingVenueList = null;
        selectedZones = null;
        clearLocationFilters();
        actionBarHandler.toggleLocationIcon(false);
        actionBarHandler.setTitle(getResources().getString(R.string.category_list));
    }

    private void clearLocationFilters() {
        if (zones != null) {
            for (DateSpotZone zone : zones) {
                checkNotNull(zone).setIsSelected(false);
            }
        }
        venueSpotPreferenceSlidingMenuHandler.setZoneData(city_name, zones);
        actionBarHandler.setLocationBlooperVisibility(false);
    }

    private void showVenueFragment() {
        try {
            actionBarHandler.setTitle(category_name);
            actionBarHandler.toggleLocationIcon(true);
            int selectedZoneCount = venueSpotPreferenceSlidingMenuHandler.setZoneData(city_name, zones);

        } catch (IllegalStateException ignored) {
        }
    }

    private void showFollowingVenueFragment() {
        actionBarHandler.setTitle(category_name);
        actionBarHandler.toggleLocationIcon(false);
    }

    @Override
    public void onClose() {
        isLocationPreferencesVisible = false;


    }

    @Override
    public void onClosed() {
        isLocationPreferencesVisible = false;
        if (!isRecommendedListInProgress) {
            //Reset to whatever zone data was initially setString.
            venueSpotPreferenceSlidingMenuHandler.setZoneData(city_name, zones);
            if (mLocation != null) {
                venueSpotPreferenceSlidingMenuHandler.checkNearBy(true);
                venueSpotPreferenceSlidingMenuHandler.uncheckAllNonDefault(false);
                venueSpotPreferenceSlidingMenuHandler.checkDefault(false, false);
            }
        }

    }

    @Override
    public void onOpened() {
        TrulyMadlyTrackEvent.trackEvent(aContext,
                TrulyMadlyEvent.TrulyMadlyActivities.scenes, TrulyMadlyEvent.TrulyMadlyEventTypes.filter_clicked, 0, category_id
                , event_info, true);
        isLocationPreferencesVisible = true;
    }


    @Override
    public void onSaveClicked(ArrayList<String> selectedZoneIds) {

        if (selectedZoneIds != null && selectedZoneIds.size() > 0) {

            selectedZones = "";
            int len = selectedZoneIds.size();
            for (int i = 0; i < len - 1; i++)
                selectedZones += selectedZoneIds.get(i) + ",";
            selectedZones += selectedZoneIds.get(len - 1);
            // tracking
            HashMap<String, String> eventInfo = new HashMap<>();

            eventInfo.put("Location", selectedZoneIds.contains("-2") ? "nearby" : selectedZones);
            if (ab_enabled)
                eventInfo.put("tracking_key", RFHandler.getString(aContext, Utility.getMyId(aContext), ConstantsRF.AB_TRACKING_KEY));

            TrulyMadlyTrackEvent.trackEvent(aContext,
                    TrulyMadlyEvent.TrulyMadlyActivities.scenes, TrulyMadlyEvent.TrulyMadlyEventTypes.filter_applied, 0, category_id
                    , eventInfo, true);


            if (!selectedZoneIds.get(0).equals("-2")) {
                mLocation = null;
                fragment_selected = 2;
                issueRequest(VENUE_LIST);
            }
            toggleLocationPreferences();
            actionBarHandler.setLocationBlooperVisibility(!selectedZoneIds.get(0).equals("-1"));

            if (!selectedZoneIds.contains("-1") && !selectedZoneIds.contains("-2")) {
                for (DateSpotZone zone : zones) {
                    zone.setIsSelected(selectedZoneIds.contains(zone.getZoneId()));
                }
            } else {
                for (DateSpotZone zone : zones) {
                    zone.setIsSelected(false);
                }
            }


        }
    }

    @Override
    public void onLocationRecived(Location location) {
        Log.d("location", "onLocationRecieved");
        mLocation = location;
        fragment_selected = 2;
        issueRequest(VENUE_LIST);
    }


}
