package com.trulymadly.android.app.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.InflateException;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.moe.pushlibrary.MoEHelper;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.modal.Voucher;
import com.trulymadly.android.app.sqlite.VoucherDBHandler;
import com.trulymadly.android.app.utility.ActionBarHandler;
import com.trulymadly.android.app.utility.ActivityHandler;
import com.trulymadly.android.app.utility.Utility;
import com.trulymadly.android.app.views.ExpandableHeightListView;

import java.util.ArrayList;

public class VoucherActivity extends AppCompatActivity {

	private Context aContext;
	private ArrayList<Voucher> allVouchers;
	private ExpandableHeightListView vouchersContainer;
	private boolean isPush;
	private MoEHelper mhelper = null;

    public static ArrayList<Voucher> getVouchersFromDatabase(Context ctx) {

		ArrayList<Voucher> allVouchs = new ArrayList<>();
		if (Utility.isSet(Utility.getMyId(ctx))) {
			VoucherDBHandler x = new VoucherDBHandler(ctx);
			allVouchs = x.getVouchersForUser(Utility.getMyId(ctx));
			// x.close();
		}
		return allVouchs;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		if (savedInstanceState != null) {
			return;
		}

		mhelper = new MoEHelper(this);
		Intent i = getIntent();
		Bundle b = i.getExtras();
		if (b != null) {
			isPush = b.getBoolean("isPush", false);
		}

		setContentView(R.layout.vouchers_layout);

		new ActionBarHandler(this, "My Vouchers", null, null, false, false, false);

		aContext = this;

		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		vouchersContainer = (ExpandableHeightListView) findViewById(R.id.voucher_list);
		vouchersContainer.setExpanded(true);

		allVouchers = getVouchersFromDatabase(aContext);

		displayAllVouchers();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
        super.onPause();
	}

	private void displayAllVouchers() {
		ArrayAdapter<Voucher> adapter = new ArrayAdapter<Voucher>(aContext,
				R.layout.custom_voucher_list_item, android.R.id.text1,
				allVouchers) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				View view = null;
				try {
					view = super.getView(position, convertView, parent);
					TextView text1 = (TextView) view
							.findViewById(android.R.id.text1);
					TextView text2 = (TextView) view
							.findViewById(android.R.id.text2);

					text1.setText(allVouchers.get(position).getFriendly_text());
					text2.setText(allVouchers.get(position).getVoucher_code());
				} catch (InflateException e) {
					finish();
				}
				return view;
			}

		};

		// Assign adapter to ListView
		vouchersContainer.setAdapter(adapter);

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			if (isPush) {
				ActivityHandler.startMatchesActivity(aContext);
			} else {
				finish();
			}
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public void onStop() {
		super.onStop();
	}
}
