package com.trulymadly.android.app.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.moe.pushlibrary.MoEHelper;
import com.squareup.otto.Subscribe;
import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.MatchesPageSetter;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.billing.PaymentMode;
import com.trulymadly.android.app.billing.SparksBillingController;
import com.trulymadly.android.app.bus.BusProvider;
import com.trulymadly.android.app.bus.EventInfoUpdateEvent;
import com.trulymadly.android.app.bus.NetworkChangeEvent;
import com.trulymadly.android.app.fragments.BuySparkDialogFragment;
import com.trulymadly.android.app.fragments.BuySparkDialogFragmentNew;
import com.trulymadly.android.app.fragments.BuySparkEventListener;
import com.trulymadly.android.app.fragments.SelectQuizResultFragment;
import com.trulymadly.android.app.fragments.SendSparkFragment;
import com.trulymadly.android.app.fragments.SimpleDialogFragment;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.ConstantsRF;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.json.ProfileNewResponseParser;
import com.trulymadly.android.app.listener.ActivityEventsListener;
import com.trulymadly.android.app.listener.ConfirmDialogInterface;
import com.trulymadly.android.app.listener.CustomOkHttpResponseHandler;
import com.trulymadly.android.app.listener.OnActionBarClickedInterface;
import com.trulymadly.android.app.listener.TapToRetryListener;
import com.trulymadly.android.app.listener.TrackingBuySparkEventListener;
import com.trulymadly.android.app.modal.FbFriendModal;
import com.trulymadly.android.app.modal.MatchesLatestModal2;
import com.trulymadly.android.app.modal.MySelectData;
import com.trulymadly.android.app.modal.ProfileNewModal;
import com.trulymadly.android.app.modal.SelectQuizCompareModal;
import com.trulymadly.android.app.modal.SparkPackageModal;
import com.trulymadly.android.app.modal.UserInfoModal;
import com.trulymadly.android.app.modal.UserModal;
import com.trulymadly.android.app.modal.VideoModal;
import com.trulymadly.android.app.sqlite.MatchesDbHandler;
import com.trulymadly.android.app.sqlite.RFHandler;
import com.trulymadly.android.app.utility.ABHandler;
import com.trulymadly.android.app.utility.ActionBarHandler;
import com.trulymadly.android.app.utility.ActivityHandler;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.MoEHandler;
import com.trulymadly.android.app.utility.OkHttpHandler;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.SparksHandler;
import com.trulymadly.android.app.utility.TMSelectHandler;
import com.trulymadly.android.app.utility.TapToRetryHandler;
import com.trulymadly.android.app.utility.TmLogger;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities.select;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities.sparks;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes.select_detail_card_profile;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes.select_on_action_nudge;

/**
 * Created by deveshbatra on 3/21/16.
 */
public class SingleMatchActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.spark_lay)
    FloatingActionButton mSparkFAB;
    @BindView(R.id.like_lay)
    FloatingActionButton mLikeFAB;
    @BindView(R.id.hide_lay)
    FloatingActionButton mHideFAB;
    @BindView(R.id.spark_dialog_container)
    View mSparksDialogContainer;
    @BindView(R.id.spark_lay_icon_large_ab)
    ImageView spark_lay_icon_large_ab;
    @BindView(R.id.waiting_response_tv)
    TextView waiting_response_tv;

    @BindView(R.id.spark_coachmark_tv)
    View spark_coachmark_tv;
    //Select
    @BindView(R.id.select_quiz_result_container)
    View mSelectQuizResultContainer;
    private MoEHelper mhelper = null;
    private Context aContext;
    private Activity aActivity;
    private UserInfoModal userInfoModal;
    private TapToRetryHandler tapToRetryHandler;
    private ActionBarHandler abHandler;
    private MatchesPageSetter aMatchesPageSetter;
    private View matches_page_setter, custom_prog_bar_id;
    private ProgressDialog mProgressDialog;
    private ProfileNewModal aProfileNewModal = null;
    private Vector<ProfileNewModal> profilesList;
    private LinearLayout profile_like_hide_lay;
    private LinearLayout waiting_response_container;
    private String eventId;
    private int noOfUsers;
    private android.app.FragmentManager mFragmentManager;
    private boolean isSparkDialogVisible = false;
    private SparksHandler.Sender mSparkSender;
    private SparksBillingController mSparksBillingController;
    private Fragment mCurrentFragment;
    private SendSparkFragment.SendSparkEventListener mSendSparkEventListener;
    private BuySparkEventListener mBuySparkEventListener;
    private SimpleDialogFragment.SimpleDialogActionListener mSimpleDialogActionListener;
    private HashMap<String, String> mTrackingMap;
    private int mSparkEnabledColor, mSparkDisabledColor;
    private TrackingBuySparkEventListener mTrackingBuySparkEventListener;
    private View.OnClickListener mFBOnClickListener;
    private SelectQuizResultFragment mSelectQuizresultFragment;
    private boolean isSelectQuizCompareVisible = false;
    private SelectQuizResultFragment.SelectQuizCompareListener mSelectQuizCompareListener;

    private boolean fb_mutual_friend_visible;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTrackingMap = new HashMap<>();
        try {
            setContentView(R.layout.single_match_layout);
        } catch (OutOfMemoryError e) {
            System.gc();
            finish();
            return;
        }

        //http://www.curious-creature.com/2012/12/01/android-performance-case-study/
        //For reducing overdraws : Suggested by Romain guy
        getWindow().setBackgroundDrawable(null);

        Utility.disableScreenShot(this);
        aContext = this;
        aActivity = this;
        ButterKnife.bind(this);
        mFBOnClickListener = this;

        Intent i = getIntent();
        Bundle b = i.getExtras();
        if (b != null) {
            userInfoModal = (UserInfoModal) b.getSerializable("userInfoModal");
            eventId = b.getString("eventId");
            noOfUsers = b.getInt("noOfUsers");
        }
        mhelper = new MoEHelper(this);
        OnActionBarClickedInterface actionBarClickedInterface = new OnActionBarClickedInterface() {
            @Override
            public void onBackClicked() {
                try {
                    onBackPressed();
                } catch (NullPointerException e) {
                    // gionee
                }
            }

            @Override
            public void onLocationClicked() {

            }

            @Override
            public void onUserProfileClicked() {

            }

            @Override
            public void onConversationsClicked() {

            }

            @Override
            public void onTitleClicked() {

            }

            @Override
            public void onTitleLongClicked() {

            }

            @Override
            public void onSearchViewOpened() {

            }

            @Override
            public void onSearchViewClosed() {

            }

            @Override
            public void onCategoriesClicked() {

            }

            @Override
            public void onCuratedDealsClicked() {

            }
        };
        abHandler = new ActionBarHandler(this, getResources().getString(R.string.profile),
                null, actionBarClickedInterface, false, false, false, false, false);
        abHandler.toggleNotificationCenter(false);
        abHandler.toggleBackButton(true);
        tapToRetryHandler = new TapToRetryHandler(this,
                findViewById(android.R.id.content), new TapToRetryListener() {
            @Override
            public void reInitialize() {
                issueRequest();
            }
        }, "Loading ...", false);
        matches_page_setter = findViewById(R.id.matches_page_setter);
        custom_prog_bar_id = findViewById(R.id.custom_prog_bar_id);
        profile_like_hide_lay = (LinearLayout) findViewById(R.id.profile_like_hide_lay);

        mHideFAB.setOnClickListener(this);
        mLikeFAB.setOnClickListener(this);

        waiting_response_container = (LinearLayout) findViewById(R.id.waiting_response_container);

        initializeSparkFields();
        ABHandler.setImageIconForSparkFABIcon(aContext, mSparkFAB, spark_lay_icon_large_ab);
        toggleSparksFAB(SparksHandler.isSparksEnabled(aContext));

        View category_nudge_layout = findViewById(R.id.category_nudge_layout);
        aMatchesPageSetter = new MatchesPageSetter(this, matches_page_setter,
                false, false, true, 50, mFBOnClickListener);
//        UiUtils.setBackground(aContext, custom_prog_bar_id, R.drawable.blur_background);

        issueRequest();

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            RelativeLayout.LayoutParams sparksParams = (RelativeLayout.LayoutParams) mSparkFAB.getLayoutParams();
            sparksParams.setMargins(0, 0, 0, 0); // get rid of margins since shadow area is now the margin
            mSparkFAB.setLayoutParams(sparksParams);

            LinearLayout.LayoutParams likeParams = (LinearLayout.LayoutParams) mLikeFAB.getLayoutParams();
            likeParams.setMargins(0, 0, 0, 0); // get rid of margins since shadow area is now the margin
            mLikeFAB.setLayoutParams(likeParams);

            LinearLayout.LayoutParams hideParams = (LinearLayout.LayoutParams) mHideFAB.getLayoutParams();
            hideParams.setMargins(0, 0, 0, 0); // get rid of margins since shadow area is now the margin
            mHideFAB.setLayoutParams(hideParams);
        }

        if (aMatchesPageSetter != null) {
            aMatchesPageSetter.onCreate();
        }
//        mLikeFAB.setVisibility(View.GONE);
//        mHideFAB.setVisibility(View.GONE);
    }

    private void responseParser(JSONObject response) {

        if (response != null) {
            ProfileNewResponseParser aProfileNewResponseParser = null;
            aProfileNewResponseParser = new ProfileNewResponseParser();
            aProfileNewResponseParser.setDbInsertionDisabled(true);
            MatchesLatestModal2 aMatchesLatestModal2 = null;
            aMatchesLatestModal2 = aProfileNewResponseParser
                    .parseProfileNewResponse(response, getApplicationContext(),
                            false, true, true);

            profilesList = null;
            if (aMatchesLatestModal2 != null)
                profilesList = aMatchesLatestModal2.getProfileNewModalList();
            if (profilesList != null && profilesList.size() > 0) {
                aProfileNewModal = profilesList.get(0);
            }

            ArrayList<FbFriendModal> firends = ProfileNewResponseParser.parseFBList(response, true);
            aProfileNewModal.setFbFriendList(firends);
            aMatchesPageSetter.getmFBMutualFriendsHandler().setmFbFriendsList(aProfileNewModal.getFbFriendList());

            View.OnClickListener lis = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (profilesList == null || profilesList.size() == 0 || profilesList.get(0).getUser() == null)
                        return;

                    ProfileNewModal profileNewModal = profilesList.elementAt(0);
//                    ActivityHandler.startAlbumFullViewPagerForResult(aActivity,
//                            Integer.parseInt(v.getTag().toString()),
//                            profileNewModal.getUser().getOtherPics(),
//                            profileNewModal.getUser().getVideoUrls(),
//                            profileNewModal.getUser().getThumbnailUrls(),
//                            getResources().getString(R.string.photo_gallery));

                    VideoModal[] videoModals = null;
                    ArrayList<VideoModal> videoModalsList = profileNewModal.getUser().getVideoArray();
                    if (videoModalsList != null && videoModalsList.size() > 0) {
                        videoModals = videoModalsList.toArray(new VideoModal[videoModalsList.size()]);
                    }

                    ActivityHandler.startAlbumFullViewPagerForResult(aActivity,
                            Integer.parseInt(v.getTag().toString()),
                            profileNewModal.getUser().getOtherPics(), videoModals,
                            getResources().getString(R.string.photo_gallery), TrulyMadlyEvent.TrulyMadlyActivities.scenes,
                            profileNewModal.getUser().getUserId());


                }

            };
            if (aProfileNewModal != null && aMatchesLatestModal2 != null) {
                custom_prog_bar_id.setVisibility(View.GONE);
                matches_page_setter.setVisibility(View.VISIBLE);
                mTrackingMap.clear();
                aMatchesPageSetter.instantiateItem(aProfileNewModal, lis,
                        aMatchesLatestModal2.getMatchesSysMesgModal(),
                        aMatchesLatestModal2.getMyDetailModal(), null);
                // Hiding ask friends forever and ever and ever
                // abHandler.toggleAskFriends(!isMyProfile &&
                // Utility.getBool(aContext,
                // Constants.SHARED_KEYS_USER_GENDER) .equalsIgnoreCase("f"));
            }

            boolean isFemale = !Utility.isMale(aContext);

            if (userInfoModal.isSparkedByMe()) {
                waiting_response_tv.setText(String.format(getString(R.string.you_have_sparked),
                        (Utility.isMale(aContext) ? getString(R.string.her) : getString(R.string.him))));
                waiting_response_container.setVisibility(View.VISIBLE);
                profile_like_hide_lay.setVisibility(View.GONE);
            } else if (userInfoModal.isLikedByMe()) {
                mLikeFAB.setVisibility(View.GONE);
                mSparkFAB.setVisibility(View.VISIBLE);
                mHideFAB.setVisibility(View.VISIBLE);
//                waiting_response_tv.setText(R.string.invite_send);
                waiting_response_container.setVisibility(View.GONE);
                profile_like_hide_lay.setVisibility(View.VISIBLE);

                startSparkCoachmarkAnimation();
            } else {
                mLikeFAB.setVisibility((isFemale) ? View.VISIBLE : View.GONE);
                mSparkFAB.setVisibility(View.VISIBLE);
                mHideFAB.setVisibility(View.VISIBLE);
                profile_like_hide_lay.setVisibility(View.VISIBLE);
                waiting_response_container.setVisibility(View.GONE);
            }
        }
    }

    private void startSparkCoachmarkAnimation() {
        final AnimationSet translateDownAnimation =
                (AnimationSet) AnimationUtils.loadAnimation(aContext, R.anim.slide_down_spark_tutorial);
        final AnimationSet translateUpAnimation = (AnimationSet)
                AnimationUtils.loadAnimation(aContext, R.anim.slide_up_spark_tutorial);
        translateUpAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                spark_coachmark_tv.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        translateDownAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                spark_coachmark_tv.startAnimation(translateUpAnimation);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        translateDownAnimation.setStartOffset(0);
//        ViewGroup.LayoutParams params = spark_coachmark_tv.getLayoutParams();
//        params.width = UiUtils.dpToPx(310);
//        spark_coachmark_tv.setLayoutParams(params);
        spark_coachmark_tv.setVisibility(View.VISIBLE);
        spark_coachmark_tv.startAnimation(translateDownAnimation);
    }

    private void issueRequest() {
        CustomOkHttpResponseHandler okHttpResponseHandler = new CustomOkHttpResponseHandler(aContext,
                TrulyMadlyEvent.TrulyMadlyActivities.scenes, userInfoModal.isLikedByMe() ?
                TrulyMadlyEvent.TrulyMadlyEventTypes.await_response :
                (userInfoModal.isSparkedByMe() ? TrulyMadlyEvent.TrulyMadlyEventTypes.already_sparked : TrulyMadlyEvent.TrulyMadlyEventTypes.view_match), eventId) {
            @Override
            public void onRequestSuccess(JSONObject responseJSON) {
                if (tapToRetryHandler != null)
                    tapToRetryHandler.onSuccessFull();
                responseParser(responseJSON);

                abHandler.setToolbarTransparent(true, getResources().getString(R.string.profile));
            }

            @Override
            public void onRequestFailure(Exception exception) {

                if (tapToRetryHandler != null)
                    tapToRetryHandler.onNetWorkFailed(exception);
            }
        };
        // getBool profile_url from userinfomodal.getprofileurl
//        profileUrl = "http://dev.trulymadly.com/trulymadly/profile.php?pid=eaa28b660ff8865ad2f3b9d0feab9d37_90399";
        if (tapToRetryHandler != null)
            tapToRetryHandler.showLoader();

        String profileUrl = userInfoModal.getProfile_url();
        Map<String, String> params = new HashMap<>();
        params.put("fav_v2", "true");
        params.put("from_scenes", "true");
        OkHttpHandler.httpGet(aContext, profileUrl, params,
                okHttpResponseHandler);
    }

    @Subscribe
    public void onNetworkChanged(NetworkChangeEvent networkChangeEvent) {
        if (aMatchesPageSetter != null) {
            aMatchesPageSetter.onNetworkChanged(networkChangeEvent);
        }
    }

    @Subscribe
    public void onEventInfoUpdateEventFired(EventInfoUpdateEvent eventInfoUpdateEvent) {
        if (eventInfoUpdateEvent != null) {
            String key = EventInfoUpdateEvent.getKeyString(eventInfoUpdateEvent.getmKey());
            if (key != null) {
                String value = eventInfoUpdateEvent.getmValue();
                if (Utility.isSet(value)) {
                    mTrackingMap.put(key, eventInfoUpdateEvent.getmValue());
                }
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        BusProvider.getInstance().register(this);

        if (aMatchesPageSetter != null) {
            aMatchesPageSetter.onStart();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (aMatchesPageSetter != null) {
            aMatchesPageSetter.onResume();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (aMatchesPageSetter != null) {
            aMatchesPageSetter.onPause();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        BusProvider.getInstance().unregister(this);

        if (aMatchesPageSetter != null) {
            aMatchesPageSetter.onStop();
        }
    }

    @Override
    public void onBackPressed() {
        if (isSelectQuizCompareVisible) {
            isSelectQuizCompareVisible = false;
            mSelectQuizresultFragment.registerListener(null);
            mSelectQuizResultContainer.setVisibility(View.GONE);
        } else if (isSparkDialogVisible && mCurrentFragment != null) {
            ((ActivityEventsListener) mCurrentFragment).onBackPressedActivity();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mSparksBillingController != null) {
            mSparksBillingController.onDestroy();
        }

        if (aMatchesPageSetter != null) {
            aMatchesPageSetter.onDestroy();
        }
    }

    private boolean toggleSelectQuizResult(String matchId, String matchName, String matchUrl,
                                           String userUrl, String compatibilityString, String quote) {
        if (mSelectQuizresultFragment == null) {
            mSelectQuizCompareListener = new SelectQuizResultFragment.SelectQuizCompareListener() {
                @Override
                public void closeFragment() {
                    onBackPressed();
                }
            };
            mSelectQuizresultFragment = SelectQuizResultFragment.newInstance(new
                            SelectQuizCompareModal(matchId, matchName, matchUrl, userUrl),
                    compatibilityString, quote);
            getSupportFragmentManager().beginTransaction().add(R.id.select_quiz_result_container,
                    mSelectQuizresultFragment).commit();
        } else {
            mSelectQuizresultFragment.reInitialize(new SelectQuizCompareModal(matchId,
                    matchName, matchUrl, userUrl), compatibilityString, quote);
        }
        mSelectQuizresultFragment.registerListener(mSelectQuizCompareListener);
        mSelectQuizResultContainer.setVisibility(View.VISIBLE);
        isSelectQuizCompareVisible = true;
        return true;
    }

    private void issueLikeHideRequest(String url, final boolean isLiked) {
        CustomOkHttpResponseHandler okHttpResponseHandler = new CustomOkHttpResponseHandler(aContext) {

            @Override
            public void onRequestSuccess(JSONObject responseJSON) {

                // deleteUserFromCaching
                MatchesDbHandler.deleteMatchId(aContext, userInfoModal.getId());


                HashMap<String, String> eventInfo = new HashMap<>();
                eventInfo.put("attendee_list", "" + noOfUsers);
                eventInfo.put("match_id", userInfoModal.getId());
                TrulyMadlyTrackEvent.trackEvent(aActivity,
                        TrulyMadlyEvent.TrulyMadlyActivities.scenes, isLiked ? TrulyMadlyEvent.TrulyMadlyEventTypes.like_match : TrulyMadlyEvent.TrulyMadlyEventTypes.hide_match, 0, eventId
                        , eventInfo, true);

                Intent i = new Intent();
                i.putExtra("userInfoModal", userInfoModal);

                if (Utility.isSet(responseJSON.optString("msg_url"))) {
                    userInfoModal.setName(responseJSON.optString("name"));
                    userInfoModal.setMessageUrl(responseJSON.optString("msg_url"));
                    eventInfo.remove("attendee_list");
                    TrulyMadlyTrackEvent.trackEvent(aActivity,
                            TrulyMadlyEvent.TrulyMadlyActivities.scenes, TrulyMadlyEvent.TrulyMadlyEventTypes.likeback_event, 0, eventId
                            , eventInfo, true);

                    setResult(Constants.MUTUAL_MATCH, i);
                    finish();
//                    ActivityHandler.startMessageOneonOneActivity(aActivity, userInfoModal.getId(), responseJSON.optString("message_url"), false, false, false);
                } else {
                    setResult(isLiked ? Constants.LIKE_SINGLE_MATCH : Constants.HIDE_SINGLE_MATCH, i);
                    finish();
                }
            }

            @Override
            public void onRequestFailure(Exception exception) {

            }
        };

        OkHttpHandler.httpGet(aContext, url,
                okHttpResponseHandler);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mSparksBillingController != null && mSparksBillingController.handleActivityResult(requestCode, resultCode, data)) {
            TmLogger.d("SparksBillingHandler", "onActivityResult handled by SparkBillingHandler.");
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @OnClick(R.id.fb_friend_container)
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fb_friend_container:
                if (RFHandler.getBool(aContext, Utility.getMyId(aContext), ConstantsRF.ALL_MUTUAL_FRIENDS)) {
                    fb_mutual_friend_visible = !fb_mutual_friend_visible;
                    ProfileNewModal profileModal = profilesList.get(0);
                    if (fb_mutual_friend_visible) {
                        aMatchesPageSetter.toggleMutualFriendsLayout(true, false, profileModal.getFbFriendList());
                    } else {
                        aMatchesPageSetter.toggleMutualFriendsLayout(false, true, profileModal.getFbFriendList());
                    }
                }
                break;

            case R.id.mutual_friend_layout:
                fb_mutual_friend_visible = false;
                aMatchesPageSetter.toggleMutualFriendsLayout(false, false, null);
                break;

            case R.id.sub_mutual_friend_layout:
                if (fb_mutual_friend_visible) {
                    fb_mutual_friend_visible = false;
                    aMatchesPageSetter.toggleMutualFriendsLayout(false, false, null);
                }
                break;

            case R.id.spark_lay:
                if (TMSelectHandler.isSelectEnabled(aContext)
                        && profilesList.get(0).isTMSelectMember()
                        && !TMSelectHandler.isSelectMember(aContext)
                        && !TMSelectHandler.isSparkAllowedOnSelect(aContext)) {
                    showOnActionSelectNudge(
                            SPHandler.getString(aContext,
                                    ConstantsSP.SHARED_KEYS_USER_PROFILE_FULL_URL),
                            profilesList.get(0).getUser().getProfilePic(), "spark");
                    return;
                }
                MoEHandler.trackEvent(aContext, MoEHandler.Events.SPARK_ICON_CLICKED);
                if (!SPHandler.getBool(aContext, ConstantsSP.SHARED_PREF_INTRODUCING_SPARK_TUTORIAL_SHOWN)) {
                    SPHandler.setBool(aContext, ConstantsSP.SHARED_PREF_INTRODUCING_SPARK_TUTORIAL_SHOWN, true);
                    toggleFragment(true, SparksHandler.SparksDialogType.introducing_sparks, 0, 0, false, false, userInfoModal.isLikedByMe());
                    return;
                }
                if (!SparksHandler.isSparksPresent(aContext)) {
                    MoEHandler.trackEvent(aContext, MoEHandler.Events.BUY_PAGE_OPENED);
                    toggleFragment(true, SparksHandler.SparksDialogType.buy_spark, 0, 0, false, false, userInfoModal.isLikedByMe());
                } else {
                    MoEHandler.trackEvent(aContext, MoEHandler.Events.SPARK_PAGE_OPENED);
                    toggleFragment(true, SparksHandler.SparksDialogType.send_spark, 0, 0, false, false, userInfoModal.isLikedByMe());
                }
                break;

            case R.id.like_lay:
                if (TMSelectHandler.isSelectEnabled(aContext) && profilesList.get(0).isTMSelectMember() && !TMSelectHandler.isSelectMember(aContext)
                        && !TMSelectHandler.isLikeAllowedOnSelect(aContext)) {
                    showOnActionSelectNudge(
                            SPHandler.getString(aContext,
                                    ConstantsSP.SHARED_KEYS_USER_PROFILE_FULL_URL),
                            profilesList.get(0).getUser().getProfilePic(), "like");
                    return;
                }
                if (Utility.isNetworkAvailable(this)) {
                    issueLikeHideRequest(userInfoModal.getLikeLink(), true);
                } else {
                    AlertsHandler.showMessage(aActivity, R.string.ERROR_NETWORK_FAILURE);
                }

                break;

            case R.id.hide_lay:
                if (Utility.isNetworkAvailable(this)) {
                    if (!RFHandler.getBool(aContext, Utility.getMyId(aActivity), ConstantsRF.RIGID_FIELD_FIRST_HIDE_SINGLE_MATCH)) {
                        RFHandler.insert(aContext, Utility.getMyId(aActivity), ConstantsRF.RIGID_FIELD_FIRST_HIDE_SINGLE_MATCH, true);
                        AlertsHandler.showConfirmDialog(aActivity, R.string.hide_single_match_confirmation, R.string.yes, R.string.cancel, new ConfirmDialogInterface() {
                            @Override
                            public void onPositiveButtonSelected() {
                                issueLikeHideRequest(userInfoModal.getHideLink(), false);
                            }

                            @Override
                            public void onNegativeButtonSelected() {

                            }
                        });
                    } else {
                        issueLikeHideRequest(userInfoModal.getHideLink(), false);
                    }

                } else {
                    AlertsHandler.showMessage(aActivity, R.string.ERROR_NETWORK_FAILURE);
                }
                break;

            case R.id.select_details_card:
                boolean isSelectMember = TMSelectHandler.isSelectMember(aContext);
                Map<String, String> eventInfoSelect = new HashMap<>();
                eventInfoSelect.put("is_select_member", String.valueOf(isSelectMember));
                eventInfoSelect.put("source", "event_user_profile");
                TrulyMadlyTrackEvent.trackEvent(aContext,
                        select, select_detail_card_profile, 0,
                        TrulyMadlyEvent.TrulyMadlyEventStatus.clicked, eventInfoSelect, true);

                if (TMSelectHandler.isSelectMember(aContext) && TMSelectHandler.isSelectQuizPlayed(aContext)) {
                    UserModal user = profilesList.get(0).getUser();
                    toggleSelectQuizResult(profilesList.get(0).getUserId(),
                            user.getName(),
                            user.getProfilePic(),
                            Utility.getMyProfilePic(aContext),
                            profilesList.get(0).getmSelectCommonString(),
                            profilesList.get(0).getmSelectQuote());
                } else {
                    ActivityHandler.startTMSelectActivity(aContext, select_detail_card_profile + ":event_user_profile", false);
                }
                break;
        }
    }

    private void showOnActionSelectNudge(String myImage, String matchImage, String onAction) {
        boolean isSelectMember = TMSelectHandler.isSelectMember(aContext);
        final Map<String, String> eventInfoSelect = new HashMap<>();
        eventInfoSelect.put("is_select_member", String.valueOf(TMSelectHandler.isSelectMember(aContext)));
        eventInfoSelect.put("is_profile_select", String.valueOf(aProfileNewModal.isTMSelectMember()));
        eventInfoSelect.put("on_action", onAction);
        eventInfoSelect.put("source", "event_user_profile");
        TrulyMadlyTrackEvent.trackEvent(aContext,
                select, select_on_action_nudge, 0,
                TrulyMadlyEvent.TrulyMadlyEventStatus.view, eventInfoSelect, true);

        AlertsHandler.showOnActionSelectDialog(aContext, myImage,
                matchImage, R.string.please_join_tm_select_toview, TMSelectHandler.getSelectProfileCta(aContext),
                aContext.getString(R.string.may_be_later),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        switch (view.getId()) {
                            case R.id.join_now_button:
                                TrulyMadlyTrackEvent.trackEvent(aContext,
                                        select, select_on_action_nudge, 0,
                                        TrulyMadlyEvent.TrulyMadlyEventStatus.clicked_yes, eventInfoSelect, true);
                                ActivityHandler.startTMSelectActivity(aContext, select_on_action_nudge + ":event_user_profile", false);
                                break;

                            case R.id.maybe_later_button:
                                TrulyMadlyTrackEvent.trackEvent(aContext,
                                        select, select_on_action_nudge, 0,
                                        TrulyMadlyEvent.TrulyMadlyEventStatus.clicked_no, eventInfoSelect, true);
                                break;
                        }
                    }
                });
    }

    private void initializeSparkFields() {
        mSparkEnabledColor = ActivityCompat.getColor(aContext, R.color.colorTertiary);
        mSparkDisabledColor = ActivityCompat.getColor(aContext, R.color.gray_all_d);
        mSendSparkEventListener = new SendSparkFragment.SendSparkEventListener() {
            @Override
            public void closeFragment() {
                toggleFragment(false, null, 0, 0, false, false, userInfoModal.isLikedByMe());
            }

            @Override
            public void sparkSentSuccess() {
                // deleteUserFromCaching
                MatchesDbHandler.deleteMatchId(aContext, userInfoModal.getId());

                Intent i = new Intent();
                i.putExtra("userInfoModal", userInfoModal);
                setResult(Constants.SPARK_SINGLE_MATCH, i);
                finish();
            }

            @Override
            public void sparkSentFailed() {

            }
        };

        mBuySparkEventListener = new BuySparkEventListener() {
            @Override
            public void closeFragment() {
                toggleFragment(false, null, 0, 0, false, false, userInfoModal.isLikedByMe());
            }

            @Override
            public void onBuySparkClicked(Object packageModal, String matchId) {
                SparkPackageModal sparkPackageModal = (SparkPackageModal) packageModal;
                TmLogger.d("SparksBillingHandler", "Spark buy clicked in Adapter : " + sparkPackageModal.getmPackageSku());
//                mSparksBillingController.launchPurchaseFlow(PaymentMode.google, sparkPackageModal.getmPackageSku(), matchId);
//                mSparksBillingController.launchPurchaseFlow(PaymentMode.paytm, sparkPackageModal.getmPackageSku(), matchId,
//                        sparkPackageModal.getmPrice());
                mSparksBillingController.askForPaymentOption(sparkPackageModal.getmPackageSku(), matchId,
                        sparkPackageModal.getmSparkCount(), sparkPackageModal.getmPrice());
            }

            @Override
            public void onBuySparkSuccess(PaymentMode mPaymentMode, String sku, int newSparksAdded, int totalSparks, boolean isRelationshipExpertAdded, MySelectData mySelectData) {
                toggleFragment(true, SparksHandler.SparksDialogType.spark_purchased, newSparksAdded, totalSparks, false, isRelationshipExpertAdded, userInfoModal.isLikedByMe());
            }

            @Override
            public void restorePurchasesClicked() {
                mSparksBillingController.restorePurchases(false);
            }

            @Override
            public void onRegistered() {
                mSparksBillingController.restorePurchases(true);
            }
        };

        mTrackingBuySparkEventListener = new TrackingBuySparkEventListener(aContext, mBuySparkEventListener);

        mSimpleDialogActionListener = new SimpleDialogFragment.SimpleDialogActionListener() {
            @Override
            public void onActionButtonClicked(int action) {
                switch (action) {
                    case SimpleDialogFragment.SIMPLE_DIALOG_INTRODUCING_SPARK:
                        //Tracking : Intro sparks - clicked
                        HashMap<String, String> eventInfo = new HashMap<>();
                        if (profilesList != null && profilesList.size() > 0) {
                            eventInfo.put("match_id", profilesList.get(0).getUserId());
                        }
                        eventInfo.put("sparks_left", String.valueOf(SparksHandler.getSparksLeft(aContext)));
                        eventInfo.put("from_scenes", String.valueOf(true));
                        eventInfo.put("event_id", eventId);
                        eventInfo.put("likes_done", String.valueOf(0));
                        eventInfo.put("hides_done", String.valueOf(0));
                        eventInfo.put("sparks_done", String.valueOf(0));
                        TrulyMadlyTrackEvent.trackEvent(aContext, sparks, TrulyMadlyEvent.TrulyMadlyEventTypes.intro, 0,
                                TrulyMadlyEvent.TrulyMadlyEventStatus.tryit_clicked,
                                eventInfo, true, true);

                        if (!SparksHandler.isSparksPresent(aContext)) {
                            toggleFragment(true, SparksHandler.SparksDialogType.buy_spark, 0, 0, false, false, userInfoModal.isLikedByMe());
                        } else {
                            toggleFragment(true, SparksHandler.SparksDialogType.send_spark, 0, 0, false, false, userInfoModal.isLikedByMe());
                        }
                        break;
                    case SimpleDialogFragment.SIMPLE_DIALOG_ONE_LEFT:
                        toggleFragment(true, SparksHandler.SparksDialogType.buy_spark, 0, 0, false, false, userInfoModal.isLikedByMe());
                        break;
                    case SimpleDialogFragment.SIMPLE_DIALOG_PURCHASE_COMPLETE:
                        toggleFragment(false, SparksHandler.SparksDialogType.spark_purchased, 0, 0, false, false, userInfoModal.isLikedByMe());
                        break;

                }
            }

            @Override
            public void closeFragment() {
                toggleFragment(false, null, 0, 0, false, false, userInfoModal.isLikedByMe());
            }
        };

        mSparksBillingController = new SparksBillingController(this, null, mTrackingBuySparkEventListener);

        mCurrentFragment = getSupportFragmentManager().findFragmentById(R.id.spark_dialog_container);
        if (mCurrentFragment != null) {
            SparksHandler.SparksDialogType dialogType = SparksHandler.SparksDialogType.introducing_sparks;
            if (mCurrentFragment instanceof BuySparkDialogFragment || mCurrentFragment instanceof BuySparkDialogFragmentNew) {
//                ((ActivityEventsListener) mCurrentFragment).registerListener(mBuySparkEventListener);
                ((ActivityEventsListener) mCurrentFragment).registerListener(mTrackingBuySparkEventListener);
            } else if (mCurrentFragment instanceof SimpleDialogFragment) {
                ((ActivityEventsListener) mCurrentFragment).registerListener(mSimpleDialogActionListener);
            } else if (mCurrentFragment instanceof SendSparkFragment) {
                ((ActivityEventsListener) mCurrentFragment).registerListener(mSendSparkEventListener);
            }
        }
    }

    private Object getListener(SparksHandler.SparksDialogType dialogType) {
        Object object = null;
        switch (dialogType) {
            case introducing_sparks:
            case one_spark_left:
            case spark_purchased:
                object = mSimpleDialogActionListener;
                break;
            case buy_spark:
                object = mTrackingBuySparkEventListener;
                break;
            case send_spark:
                object = mSendSparkEventListener;
                break;
        }

        return object;
    }

    private void toggleSparksFAB(boolean isSparksEnabled) {
        //Hiding spark button when
        //1. the given profile is a sponsored profile
        //2. the given profile is a video ad (SeventyNine)
        //3. sparks are not enabled yet

        mSparkFAB.setVisibility((!isSparksEnabled) ? View.GONE : View.VISIBLE);

        if (isSparksEnabled) {
            mSparkFAB.setBackgroundTintList(ColorStateList.valueOf(mSparkEnabledColor));
            mSparkFAB.setOnClickListener(this);
        } else {
            mSparkFAB.setOnClickListener(null);
        }
    }

    private void toggleFragment(boolean showFragment, SparksHandler.SparksDialogType dialogType,
                                int newSparksAdded, int totalSparks, boolean fromIntro, boolean isRelationshipExpertAdded, boolean mHasLikedBefore) {
        if (showFragment) {
            mCurrentFragment = SparksHandler.toggleFragment(this, mCurrentFragment, getListener(dialogType),
                    profilesList.get(0).getUserId(), profilesList.get(0).getUser().getmSparkHash(),
                    showFragment, dialogType, newSparksAdded, totalSparks, fromIntro, true, eventId, 0, 0, 0,
                    Utility.stringCompare(mTrackingMap.get(EventInfoUpdateEvent.getKeyString(EventInfoUpdateEvent.KEY_FAVORITES_VIEWED)), "true"),
                    isRelationshipExpertAdded, mHasLikedBefore, aProfileNewModal.isTMSelectMember());

            mSparksDialogContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            mSparksDialogContainer.setVisibility(View.VISIBLE);
            isSparkDialogVisible = true;
        } else {
            isSparkDialogVisible = false;
            mSparksDialogContainer.setVisibility(View.GONE);
        }
    }
}
