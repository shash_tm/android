package com.trulymadly.android.app.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.longtailvideo.jwplayer.JWPlayerView;
import com.longtailvideo.jwplayer.configuration.PlayerConfig;
import com.longtailvideo.jwplayer.core.PlayerState;
import com.longtailvideo.jwplayer.events.listeners.VideoPlayerEvents;
import com.moe.pushlibrary.MoEHelper;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.modal.Photos;
import com.trulymadly.android.app.utility.Utility;

import java.util.HashMap;


/**
 * Created by trulymadly on 29/07/16.
 */
public class PlayVideoActivity extends AppCompatActivity implements View.OnClickListener{

    private final String TAG = PlayVideoActivity.class.getName();

    private Activity aActivity;
    private Context aContext;
    private MoEHelper mhelper;
    private View retry_iv;
    private ViewGroup playerview_container;
    private JWPlayerView playerView;
    private View loaderView;
    private ImageView circleView;
    private ImageView loader_iv, video_thumbnail_iv;
    private Animation mScaleRepeatAnimation;
    private boolean isComplete = false;
    private String mThumbnailUrl;
    private String mVideoLink, mVideoId;
    private int mDuration = 0;
    private Photos mPhoto;
    private boolean isPlaySuccessTrackingSent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        aActivity = this;
        aContext = this;
        mhelper = new MoEHelper(this);
        setContentView(R.layout.play_video_layout);

        mScaleRepeatAnimation = AnimationUtils.loadAnimation(aContext, R.anim.scale_animation_repeat_80);
        mScaleRepeatAnimation.setRepeatMode(Animation.REVERSE);
        mScaleRepeatAnimation.setRepeatCount(Animation.INFINITE);

        mPhoto = getIntent().getExtras().getParcelable("photo");
//        if(mPhoto == null) {
//            mVideoLink = getIntent().getExtras().getString("url");
//            mThumbnailUrl = getIntent().getExtras().getString("thumbnailUrl");
//            mDuration = 0;
//        }else{
        mVideoId = mPhoto.getVideo_id();
        mVideoLink = mPhoto.getEncodedUrl();
        mThumbnailUrl = mPhoto.getThumbnail();
        mDuration = mPhoto.getDuration();
//        }
        video_thumbnail_iv = (ImageView) findViewById(R.id.video_thumbnail_iv);
        if(Utility.isSet(mThumbnailUrl)){
            video_thumbnail_iv.setVisibility(View.VISIBLE);
            Picasso.with(aContext).load(mThumbnailUrl).into(video_thumbnail_iv, new Callback() {
                @Override
                public void onSuccess() {
                    HashMap<String, String> eventInfo = new HashMap<>();
                    eventInfo.put("video_length_ms", String.valueOf(mDuration * 1000));
                    if (Utility.isSet(mVideoId)) {
                        eventInfo.put("video_id", mVideoId);
                    }

                    TrulyMadlyTrackEvent.trackEvent(aContext,
                            TrulyMadlyEvent.TrulyMadlyActivities.preview,
                            TrulyMadlyEvent.TrulyMadlyEventTypes.download_video_thumbnail, 0,
                            TrulyMadlyEvent.TrulyMadlyEventStatus.success, eventInfo, true);
                }

                @Override
                public void onError() {
                    HashMap<String, String> eventInfo = new HashMap<>();
                    eventInfo.put("video_length_ms", String.valueOf(mDuration * 1000));
                    if (Utility.isSet(mVideoId)) {
                        eventInfo.put("video_id", mVideoId);
                    }

                    TrulyMadlyTrackEvent.trackEvent(aContext,
                            TrulyMadlyEvent.TrulyMadlyActivities.preview,
                            TrulyMadlyEvent.TrulyMadlyEventTypes.download_video_thumbnail, 0,
                            TrulyMadlyEvent.TrulyMadlyEventStatus.fail, eventInfo, true);
                }
            });
        }else{
            video_thumbnail_iv.setVisibility(View.GONE);
        }
        loaderView = findViewById(R.id.loader_view);
        circleView = (ImageView) findViewById(R.id.circle_iv);
        circleView.setImageResource(R.drawable.white_circle_loader);
        loader_iv = (ImageView) findViewById(R.id.loader_iv);
        Picasso.with(aContext).load(R.drawable.video).into(loader_iv);
        retry_iv = findViewById(R.id.retry_iv);
        retry_iv.setOnClickListener(this);
        playerview_container = (ViewGroup) findViewById(R.id.playerview_container);

        createAndInitializeJWPlayer(true);

    }

    private void createAndInitializeJWPlayer(boolean playVideo) {
        if (playerView != null) {
            playerView.pause();
            playerView.onPause();
            playerView.onDestroy();
            playerView = null;
        }

        PlayerConfig playerConfig = new PlayerConfig.Builder()
                .file(mVideoLink)
                .build();
        Log.d(TAG, "Initialized : Video link : " + mVideoLink);
        playerView = new JWPlayerView(aContext, playerConfig);
        playerView.setControls(false);
        playerView.setFullscreenHandler(null);
        playerview_container.addView(playerView);
        playerView.setVisibility(View.INVISIBLE);
        if (playVideo) {
            loaderView.setVisibility(View.VISIBLE);
            circleView.startAnimation(mScaleRepeatAnimation);
            playerView.play();
        }

        playerView.addOnErrorListener(new VideoPlayerEvents.OnErrorListener() {
            @Override
            public void onError(String s) {
                Log.d(TAG, "onError : " + s);
                playerView.pause();
                playerView.onPause();
                loaderView.setVisibility(View.GONE);
                circleView.clearAnimation();
                playerView.setVisibility(View.INVISIBLE);
                retry_iv.setVisibility(View.VISIBLE);

                HashMap<String, String> eventInfo = new HashMap<>();
                eventInfo.put("video_length_ms", String.valueOf(mDuration * 1000));
                eventInfo.put("muted", String.valueOf(mPhoto.isMuted()));
                eventInfo.put("error", s);
                if (Utility.isSet(mVideoId)) {
                    eventInfo.put("video_id", mVideoId);
                }

                TrulyMadlyTrackEvent.trackEvent(aContext,
                        TrulyMadlyEvent.TrulyMadlyActivities.preview,
                        TrulyMadlyEvent.TrulyMadlyEventTypes.video_previewed, 0,
                        TrulyMadlyEvent.TrulyMadlyEventStatus.fail, eventInfo, true);
            }
        });

        playerView.addOnSetupErrorListener(new VideoPlayerEvents.OnSetupErrorListener() {

            @Override
            public void onSetupError(String s) {
                Log.d(TAG, "onSetupError : " + s);
                playerView.pause();
                playerView.onPause();
                loaderView.setVisibility(View.GONE);
                circleView.clearAnimation();
                playerView.setVisibility(View.INVISIBLE);
                retry_iv.setVisibility(View.VISIBLE);

                HashMap<String, String> eventInfo = new HashMap<>();
                eventInfo.put("video_length_ms", String.valueOf(mDuration * 1000));
                eventInfo.put("muted", String.valueOf(mPhoto.isMuted()));
                eventInfo.put("error", s);
                if (Utility.isSet(mVideoId)) {
                    eventInfo.put("video_id", mVideoId);
                }

                TrulyMadlyTrackEvent.trackEvent(aContext,
                        TrulyMadlyEvent.TrulyMadlyActivities.preview,
                        TrulyMadlyEvent.TrulyMadlyEventTypes.video_previewed, 0,
                        TrulyMadlyEvent.TrulyMadlyEventStatus.fail, eventInfo, true);
            }

        });

        playerView.addOnCompleteListener(new VideoPlayerEvents.OnCompleteListener() {
            @Override
            public void onComplete() {
                Log.d(TAG, "onComplete");
                isComplete = true;
                playerView.pause();
                playerView.stop();
                playerView.onPause();
                playerView.setVisibility(View.INVISIBLE);
                retry_iv.setVisibility(View.VISIBLE);
                createAndInitializeJWPlayer(false);

                HashMap<String, String> eventInfo = new HashMap<>();
                eventInfo.put("video_length_ms", String.valueOf(mDuration * 1000));
                eventInfo.put("muted", String.valueOf(mPhoto.isMuted()));
                if (Utility.isSet(mVideoId)) {
                    eventInfo.put("video_id", mVideoId);
                }

                TrulyMadlyTrackEvent.trackEvent(aContext,
                        TrulyMadlyEvent.TrulyMadlyActivities.preview,
                        TrulyMadlyEvent.TrulyMadlyEventTypes.video_completed, 0,
                        TrulyMadlyEvent.TrulyMadlyEventStatus.success, eventInfo, true);
            }
        });

        playerView.addOnPauseListener(new VideoPlayerEvents.OnPauseListener() {
            @Override
            public void onPause(PlayerState playerState) {
                Log.d(TAG, "onPause : PlayerState " + playerState);
            }
        });

        playerView.addOnPlayListener(new VideoPlayerEvents.OnPlayListener() {
            @Override
            public void onPlay(PlayerState playerState) {
                Log.d(TAG, "onPlay : PlayerSate : " + playerState);
                loaderView.setVisibility(View.GONE);
                circleView.clearAnimation();
                retry_iv.setVisibility(View.GONE);
                playerView.setVisibility(View.VISIBLE);

                if (!isPlaySuccessTrackingSent) {
                    isPlaySuccessTrackingSent = true;
                    HashMap<String, String> eventInfo = new HashMap<>();
                    eventInfo.put("video_length_ms", String.valueOf(mDuration * 1000));
                    eventInfo.put("muted", String.valueOf(mPhoto.isMuted()));
                    if (Utility.isSet(mVideoId)) {
                        eventInfo.put("video_id", mVideoId);
                    }

                    TrulyMadlyTrackEvent.trackEvent(aContext,
                            TrulyMadlyEvent.TrulyMadlyActivities.preview,
                            TrulyMadlyEvent.TrulyMadlyEventTypes.video_previewed, 0,
                            TrulyMadlyEvent.TrulyMadlyEventStatus.success, eventInfo, true);
                }
            }
        });

        playerView.addOnBufferListener(new VideoPlayerEvents.OnBufferListener() {
            @Override
            public void onBuffer(PlayerState playerState) {
                Log.d(TAG, "onBuffer : PlayerSate : " + playerState);
                if (!isComplete) {
                    retry_iv.setVisibility(View.GONE);
                    loaderView.setVisibility(View.VISIBLE);
                    circleView.startAnimation(mScaleRepeatAnimation);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(playerView != null){
            playerView.onResume();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(playerView != null){
            playerView.pause();
            playerView.onPause();
            loaderView.setVisibility(View.GONE);
            circleView.clearAnimation();
            playerView.setVisibility(View.INVISIBLE);
            retry_iv.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(playerView != null){
            playerView.stop();
        }

    }

    protected void onDestroy() {
        // Let JW Player know that the app is being destroyed
        if(playerView!= null){
            Log.d(TAG, "Player Destroyed");
            playerView.onDestroy();
        }

        super.onDestroy();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.retry_iv:
                isComplete = false;
                retry_iv.setVisibility(View.GONE);
                playerView.stop();
                playerView.onResume();
                playerView.play();

                HashMap<String, String> eventInfo = new HashMap<>();
                eventInfo.put("video_length_ms", String.valueOf(mDuration * 1000));
                eventInfo.put("muted", String.valueOf(mPhoto.isMuted()));
                if (Utility.isSet(mVideoId)) {
                    eventInfo.put("video_id", mVideoId);
                }

                TrulyMadlyTrackEvent.trackEvent(aContext,
                        TrulyMadlyEvent.TrulyMadlyActivities.preview,
                        TrulyMadlyEvent.TrulyMadlyEventTypes.play_clicked, 0,
                        TrulyMadlyEvent.TrulyMadlyEventStatus.success, eventInfo, true);

                break;
        }
    }
}
