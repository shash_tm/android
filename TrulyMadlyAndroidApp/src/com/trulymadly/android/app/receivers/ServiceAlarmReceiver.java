package com.trulymadly.android.app.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.trulymadly.android.app.TrulyMadlyApplication;
import com.trulymadly.android.app.TrulyMadlyService;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.json.ConstantsSocket;
import com.trulymadly.android.app.utility.TimeUtils;

/**
 * Created by avin on 01/02/16.
 */
public class ServiceAlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (TimeUtils.isTimeoutExpired(context, ConstantsSP.SHARED_KEYS_LAST_SOCKET_UPDATE,
                TrulyMadlyService.SOCKET_IDLE_TIMEOUT)) {
            TrulyMadlyApplication.stopService(context,
                    ConstantsSocket.SOCKET_END.service_idle_timeout);
        }
    }
}
