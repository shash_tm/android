/**
 * 
 */
package com.trulymadly.android.app.receivers;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.moe.pushlibrary.utils.MoEHelperConstants;
import com.moengage.pushbase.push.MoEPushWorker;
import com.trulymadly.android.app.GcmIntentService;

import static com.moengage.push.PushManager.REQ_REGISTRATION;

/**
 * @author udbhav
 * 
 */
public class GcmBroadcastReceiver extends WakefulBroadcastReceiver {
	private static final String ACTION_REGISTER = "com.google.android.c2dm.intent.REGISTRATION";

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.content.BroadcastReceiver#onReceive(android.content.Context,
	 * android.content.Intent)
	 */
	@Override
	public void onReceive(Context context, Intent intent) {
		final String registrationId = intent.getStringExtra(MoEHelperConstants.EXTRA_REGISTRATION_ID);
		String action = intent.getAction();
		if (registrationId != null && !registrationId.equals("") && ACTION_REGISTER.equals(action)) {
			// Utility.sendRegistrationIdToBackend(regId, null, context);
			Intent registrationIntent = new Intent(context, MoEPushWorker.class);
			registrationIntent.putExtra(MoEHelperConstants.EXTRA_REGISTRATION_ID, registrationId);
			registrationIntent.putExtra(REQ_REGISTRATION, true);
			context.startService(registrationIntent);
		}
		// Explicitly specify that GcmIntentService will handle the intent.
		ComponentName comp = new ComponentName(context.getPackageName(),
				GcmIntentService.class.getName());
		// Start the service, keeping the device awake while it is launching.
		startWakefulService(context, (intent.setComponent(comp)));
		setResultCode(Activity.RESULT_OK);
	}
	
	
}
