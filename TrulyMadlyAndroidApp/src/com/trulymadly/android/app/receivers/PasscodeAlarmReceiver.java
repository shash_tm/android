package com.trulymadly.android.app.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.trulymadly.android.app.R;
import com.trulymadly.android.app.fragments.PasscodeFragment;
import com.trulymadly.android.app.json.ConstantsRF;
import com.trulymadly.android.app.modal.NotificationModal;
import com.trulymadly.android.app.modal.NotificationModal.NotificationType;
import com.trulymadly.android.app.sqlite.RFHandler;
import com.trulymadly.android.app.utility.NotificationHandler;
import com.trulymadly.android.app.utility.Utility;

public class PasscodeAlarmReceiver extends BroadcastReceiver {
	public PasscodeAlarmReceiver() {
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		if(!Utility.isUserLoggedIn(context)){
			RFHandler.insert(context, Utility.getMyId(context),
					ConstantsRF.RIGID_FIELD_CONVERSATIONS_PASSCODE_REQUESTED_TIMESTAMP, "");
			return;
		}

		String collapse_key = NotificationHandler.COLLAPSE_KEY_PREFIX + 123;
		NotificationModal notificationModal = new NotificationModal(NotificationType.ACTIVITY,
				context.getResources().getString(R.string.forgot_password),
				context.getResources().getString(R.string.passcode_notification_content) + " " + intent.getStringExtra(PasscodeFragment.PASSCODE_PARAM),
				context.getResources().getString(R.string.forgot_password),
				collapse_key,
				"com.trulymadly.android.app.ConversationListActivity");
		NotificationHandler.createLocalNotification(context, notificationModal);

		RFHandler.insert(context, Utility.getMyId(context),
				ConstantsRF.RIGID_FIELD_CONVERSATIONS_PASSCODE_REQUESTED_TIMESTAMP, "");
	}
}
