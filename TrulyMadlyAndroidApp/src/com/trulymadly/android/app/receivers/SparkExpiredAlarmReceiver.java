package com.trulymadly.android.app.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.trulymadly.android.app.sqlite.SparksDbHandler;
import com.trulymadly.android.app.utility.Utility;

public class SparkExpiredAlarmReceiver extends BroadcastReceiver {

    public static final String PARAM_USER_ID = "user_id";
    public static final String PARAM_MATCH_ID = "match_id";

    public SparkExpiredAlarmReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent != null){
            String userId = intent.getStringExtra(PARAM_USER_ID);
            String matchId = intent.getStringExtra(PARAM_MATCH_ID);

            if(Utility.isSet(userId) && Utility.isSet(matchId)){
                SparksDbHandler.sparkRejected(context, userId, matchId);
            }
        }
    }
}
