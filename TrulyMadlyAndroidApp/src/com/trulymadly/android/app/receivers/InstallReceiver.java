package com.trulymadly.android.app.receivers;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;

import com.trulymadly.android.app.bus.InstallReferrerEvent;
import com.trulymadly.android.app.utility.Utility;

public class InstallReceiver extends BroadcastReceiver {

    public InstallReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // Pass recived intent to all sdk receivers. As below
        String referrer = intent.getStringExtra("referrer");
        if (Utility.isSet(referrer)) {
            Utility.sendInstallReferrerRequestToServer(referrer, context);

            //INIT MOENGAGE TRACKER
            com.moe.pushlibrary.InstallReceiver.registerInstallation(context, intent);

            Utility.fireBusEvent(context, true, new InstallReferrerEvent(referrer));

        }
    }

    protected void onRequestSuccessNew(Context context) {

        ComponentName component = new ComponentName(context,
                InstallReceiver.class);
        context.getPackageManager().setComponentEnabledSetting(component,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);

    }

}
