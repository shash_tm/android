package com.trulymadly.android.app.receivers;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.trulymadly.android.app.TrulyMadlyService;
import com.trulymadly.android.app.bus.NetworkChangeEvent;
import com.trulymadly.android.app.bus.ServiceEvent;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.Utility;
import com.trulymadly.android.socket.SocketHandler;

public class ConnectivityChangeReceiver extends BroadcastReceiver {

    @SuppressLint("UnsafeProtectedBroadcastReceiver")
    @Override
    public void onReceive(Context aContext, Intent intent) {
        Log.d("app", "Network connectivity change");
        if (intent.getExtras() != null) {
			NetworkInfo ni = (NetworkInfo) intent.getExtras().get(
					ConnectivityManager.EXTRA_NETWORK_INFO);
            if (ni != null && ni.getState() == NetworkInfo.State.CONNECTED) {
                SPHandler.remove(aContext,
                        ConstantsSP.SHARED_KEYS_LAST_TSTAMP_SOCKET_SWITCHED_TO_PHP_POLLING);

                Utility.fireServiceBusEvent(aContext,
                        new NetworkChangeEvent(NetworkChangeEvent.CONNECTED), 1000);

                //Just reconnecting the socket if the service is running
                if(TrulyMadlyService.isInstanceCreated()) {
                    SocketHandler.destroy();
                    Utility.fireServiceBusEvent(aContext,
                            new ServiceEvent(ServiceEvent.SERVICE_EVENT_TYPE.CONNECT_SOCKET), 1000);
                }
//                if (!TimeUtils.isTimeoutExpired(aContext, Constants.SHARED_KEYS_LAST_SOCKET_UPDATE, TrulyMadlyService.SOCKET_IDLE_TIMEOUT)) {
//                    TrulyMadlyApplication.forceRestartService(aContext, SOCKET_END.force_restart_on_connectivity_change);
//                } else {
//                    TrulyMadlyApplication.createService(aContext);
//                }
            }else{
                Utility.fireServiceBusEvent(aContext,
                        new NetworkChangeEvent(NetworkChangeEvent.DISCONNECTED));
            }
//            else if (intent.getBooleanExtra(
//                    ConnectivityManager.EXTRA_NO_CONNECTIVITY, Boolean.FALSE)) {
//                Log.d("app", "There's no network connectivity");
//                Utility.fireBusEvent(aContext, true, new TriggerConnectingEvent(ConstantsSocket.SOCKET_STATE.FAILED));
//            }
        }
    }
}
