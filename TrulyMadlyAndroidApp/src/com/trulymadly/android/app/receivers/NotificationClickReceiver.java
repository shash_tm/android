package com.trulymadly.android.app.receivers;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Process;

import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.modal.NotificationModal;
import com.trulymadly.android.app.modal.NotificationModal.NotificationClickType;
import com.trulymadly.android.app.utility.ActivityHandler;
import com.trulymadly.android.app.utility.NotificationHandler;
import com.trulymadly.android.app.utility.TmLogger;

import java.util.ArrayList;

public class NotificationClickReceiver extends BroadcastReceiver {

    private static final String TAG = "NOTIFICATION_CLICK_RECEIVER";

    public static PendingIntent getNotificationClickReceiverIntent(Context ctx, NotificationClickType notificationClickType, Bundle extras) {
        Intent i = new Intent(ctx, NotificationClickReceiver.class);
        i.setAction(notificationClickType.toString());
        i.putExtras(extras);
        return PendingIntent.getBroadcast(ctx,
                NotificationHandler.getRequestCode(extras), i, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    @Override
    public void onReceive(final Context aContext, Intent intent) {
        if (intent.getExtras() != null) {
            NotificationClickType notificationClickType = NotificationClickType.valueOf(intent.getAction());
            int matchImageMultiCurrentPosition = intent.getIntExtra("matchImageMultiCurrentPosition", 0);
            int pos = intent.getIntExtra("NOTIFICATION_LIST_POSITION", 0);
            final ArrayList<NotificationModal> notificationListArray = intent.getParcelableArrayListExtra("NOTIFICATION_LIST_ARRAY");
            final int nextPos = (pos + 1) < notificationListArray.size() ? pos + 1 : 0;
            final int prevPos = (pos - 1) >= 0 ? pos - 1 : notificationListArray.size() - 1;
            switch (notificationClickType) {
                case MATCHES_IMAGE_MULTI_LAUNCH_MATCH:
                    TmLogger.d(TAG, "LAUNCH CLICK");
                    TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyEvent.TrulyMadlyActivities.push,
                            "Click", 0, NotificationModal.NotificationType.MATCHES_IMAGE_MULTI.toString(), null, true);
                    ActivityHandler.startMatchesActivity(aContext);
                    break;
                case MATCHES_IMAGE_MULTI_ACTION_NEXT:
                    TmLogger.d(TAG, "NEXT CLICK");
                    TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyEvent.TrulyMadlyActivities.push,
                            "Next", 0, NotificationModal.NotificationType.MATCHES_IMAGE_MULTI.toString(), null, true);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
                            NotificationHandler.createMultiNotification(aContext, nextPos, notificationListArray, true, false);
                        }
                    }).start();
                    break;
                case MATCHES_IMAGE_MULTI_ACTION_PREV:
                    TmLogger.d(TAG, "PREV CLICK");
                    TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyEvent.TrulyMadlyActivities.push,
                            "Back", 0, NotificationModal.NotificationType.MATCHES_IMAGE_MULTI.toString(), null, true);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
                            NotificationHandler.createMultiNotification(aContext, prevPos, notificationListArray, true, false);
                        }
                    }).start();
                    break;
                default:
                    break;
            }
        }
    }


}
