package com.trulymadly.android.app.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.trulymadly.android.analytics.GAEventTrackerV4;
import com.trulymadly.android.app.modal.GaEventModal;
import com.trulymadly.android.app.sqlite.GaQueueDBHandler;

import java.util.ArrayList;

public class GaDeQueueAlarmReceiver extends BroadcastReceiver {

	public GaDeQueueAlarmReceiver() {
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		ArrayList<GaEventModal> gaEventsToSend = GaQueueDBHandler.getItems(
				context, GAEventTrackerV4.GA_QUEUE_MAX_ITEMS_TO_SENT + 1);
		int count = gaEventsToSend.size();
		if (count > 0) {
            boolean deleteSuccess = GaQueueDBHandler.deleteItems(context,
                    GAEventTrackerV4.GA_QUEUE_MAX_ITEMS_TO_SENT);
            if (deleteSuccess) {
                for (int i = 0; i < count
                        && i < GAEventTrackerV4.GA_QUEUE_MAX_ITEMS_TO_SENT; i++) {
                    GaEventModal gaEvent = gaEventsToSend.get(i);
                    GAEventTrackerV4.sendEventToGA(context, gaEvent);
                }
                if (count > GAEventTrackerV4.GA_QUEUE_MAX_ITEMS_TO_SENT) {
                    GAEventTrackerV4.checkGAQueueAlarmAndSet(context);
                }
            }
        }

	}

}
