package com.trulymadly.android.app;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.moe.pushlibrary.MoEHelper;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.bus.BusProvider;
import com.trulymadly.android.app.json.Constants.EditProfilePageType;
import com.trulymadly.android.app.json.Constants.HttpRequestType;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.json.ParseUserData;
import com.trulymadly.android.app.listener.CachedDataInterface;
import com.trulymadly.android.app.listener.CustomOkHttpResponseHandler;
import com.trulymadly.android.app.listener.GetStartTimeInterface;
import com.trulymadly.android.app.listener.OnActionBarClickedInterface;
import com.trulymadly.android.app.listener.OnDataFetchedInterface;
import com.trulymadly.android.app.listener.TapToRetryListener;
import com.trulymadly.android.app.modal.UserData;
import com.trulymadly.android.app.sqlite.CachingDBHandler;
import com.trulymadly.android.app.utility.ActionBarHandler;
import com.trulymadly.android.app.utility.ActivityHandler;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.CachedDataHandler;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.TapToRetryHandler;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONObject;

import java.util.Map;

public class EditProfileActivity extends AppCompatActivity implements
        EditProfileActionInterface, GetStartTimeInterface {

    private static final String TAG = "Edit Profile Activity";
    private AppCompatActivity aActivity;
    private FragmentManager fragmentManager;
    private Fragment currentFragment;
    private EditProfilePageType currentFragmentLaunched = null;
    private UserData userData = null;
    private EditProfilePageType launchType = null;
    private ActionBarHandler actionBarHandler;
    private TapToRetryHandler tapToRetryHandler;
    private Resources aResources;
    private boolean isSaveCall = false;
    private boolean isSavingInProgress = false;
    private boolean isInstanceActive;
    private MoEHelper mhelper;
    private long startTime;
    private boolean helpTrayVisible;
    private CachedDataInterface cachedDataInterface;
    private boolean keyboardVisible;

    public boolean isHelpTrayVisible() {
        return helpTrayVisible;
    }

    public void setHelpTrayVisible(boolean helpTrayVisible) {
        this.helpTrayVisible = helpTrayVisible;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_profile_container);
        mhelper = new MoEHelper(this);

        OnActionBarClickedInterface actionBarClickedInterface = new OnActionBarClickedInterface() {
            @Override
            public void onConversationsClicked() {
            }


            @Override
            public void onCuratedDealsClicked() {

            }

            @Override
            public void onLocationClicked() {
            }

            @Override
            public void onBackClicked() {
                onBackPressed();
            }

            @Override
            public void onUserProfileClicked() {
            }

            @Override
            public void onTitleClicked() {
            }

            @Override
            public void onTitleLongClicked() {
            }

            @Override
            public void onSearchViewOpened() {

            }

            @Override
            public void onSearchViewClosed() {

            }

            @Override
            public void onCategoriesClicked() {

            }

        };

        actionBarHandler = new ActionBarHandler(this, "", null,
                actionBarClickedInterface, false, false, false);

        fragmentManager = getSupportFragmentManager();
        aActivity = this;
        aResources = getResources();
        Intent intent = getIntent();
        Bundle b = intent.getExtras();
        if (b != null) {
            if (b.getString("launchType") != null) {
                launchType = EditProfilePageType.valueOf(b
                        .getString("launchType"));
            }
        }

//        getWindow().setSoftInputMode(
//                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        tapToRetryHandler = new TapToRetryHandler(this,
                findViewById(android.R.id.content), new TapToRetryListener() {
            @Override
            public void reInitialize() {
                if (!isSaveCall) {
                    init();
                } else {
                    saveBasicDataOnServer(currentFragmentLaunched);
                }
            }
        }, null);

        cachedDataInterface = new CachedDataInterface() {
            @Override
            public void onSuccess(JSONObject response) {

                userData = (new ParseUserData()).parseEditData(response);
                HttpTasks.fetchStaticData(aActivity, userData, launchType,
                        new OnDataFetchedInterface() {

                            @Override
                            public void onSuccess(EditProfilePageType type,
                                                  UserData userData) {
                                tapToRetryHandler.onSuccessFull();
                                launchFragment(type);

                            }

                            @Override
                            public void onFailure(Exception exception) {
                                tapToRetryHandler.onNetWorkFailed(exception);

                            }
                        }, TrulyMadlyActivities.edit_profile);
            }

            @Override
            public void onError(Exception e) {
                tapToRetryHandler.onNetWorkFailed(e);

            }
        };
        isInstanceActive = true;
        init();


    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        isInstanceActive = true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        isInstanceActive = false;
        super.onSaveInstanceState(outState);
    }

    private void init() {

        if (launchType == null) {
            launchType = EditProfilePageType.AGE;
        }

        // initing header
        initHeader();
        fetchUserDataForEdit();

    }

    private void initHeader() {
        actionBarHandler.toggleActionBar(true);
        actionBarHandler.toggleBackButton(true);
        switch (launchType) {
            case AGE:
                setHeaderText(aResources.getString(R.string.header_age));
                break;
            case INTERESTS:
                setHeaderText(aResources.getString(R.string.header_interests_edit));
                break;
            case PROFESSION:
                setHeaderText(aResources.getString(R.string.header_profession));
                break;
            case EDUCATION:
                setHeaderText(aResources.getString(R.string.header_education));
                break;
            default:
                break;
        }
    }

    private void launchFragment(EditProfilePageType type) {
        launchFragment(type, false);
    }

    private void launchFragment(EditProfilePageType editProfilePageType,
                                Boolean isNetworkFailed) {
        if (!isInstanceActive) {
            return;
        }
        if (editProfilePageType == null) {
            ActivityHandler.startMatchesActivity(aActivity);
            return;
        }

        FragmentTransaction ft;
        Bundle data = new Bundle();
        data.putSerializable("userData", userData);
        data.putBoolean("isEdit", true);

        currentFragmentLaunched = editProfilePageType;

        clearFragmentBackStack();

        switch (editProfilePageType) {
            case AGE:
//                getWindow().setSoftInputMode(
//                        WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
                ft = fragmentManager.beginTransaction();
                data.putString("fname", userData.getFname());
                data.putString("lname", userData.getLname());
                data.putString("status", userData.getStatus());
                if (userData.getDob_d() != 0) {
                    data.putInt("dob_d", userData.getDob_d());
                    data.putInt("dob_m", userData.getDob_m());
                    data.putInt("dob_y", userData.getDob_y());
                }

                if (userData.getCountryId() != null) {
                    data.putString("location_country_id", userData.getCountryId());
                    data.putString("location_state_id", userData.getStateId());
                    data.putString("location_city_id", userData.getCityId());
                }

                data.putString("height", userData.getHeight());

                currentFragment = new EditProfileFragmentAge();
                currentFragment.setArguments(data);
                ft.replace(R.id.fragment_container_edit_profile, currentFragment,
                        "FRAGMENT_TAG_EDIT_AGE");
                ft.addToBackStack(null);
                try {
                    ft.commit();
                } catch (IllegalStateException ignored) {
                }
                break;
            case INTERESTS:
//                getWindow().setSoftInputMode(
//                        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                ft = fragmentManager.beginTransaction();
                if (userData != null) {
                    data.putStringArrayList("interests", userData.getInterests());
                }
                currentFragment = new EditProfileFragmentInterests();
                currentFragment.setArguments(data);
                ft.replace(R.id.fragment_container_edit_profile, currentFragment,
                        "FRAGMENT_TAG_EDIT_INTERESTS");
                try {
                    ft.commit();
                } catch (IllegalStateException ignored) {
                }
                break;
            case PROFESSION:
//                getWindow().setSoftInputMode(
//                        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                ft = fragmentManager.beginTransaction();
                data.putString("current_title", userData.getCurrentTitle());
                data.putStringArrayList("companies", userData.getCompanies());
                data.putBoolean("isNotWorking", userData.getIsNotWorking());
                data.putString("income", userData.getIncome());

                currentFragment = new EditProfileFragmentProfession();
                currentFragment.setArguments(data);
                ft.replace(R.id.fragment_container_edit_profile, currentFragment,
                        "FRAGMENT_TAG_EDIT_PROFESSION");
                ft.addToBackStack(null);
                try {
                    ft.commit();
                } catch (IllegalStateException ignored) {
                }
                break;
            case EDUCATION:
//                getWindow().setSoftInputMode(
//                        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                ft = fragmentManager.beginTransaction();
                data.putString("highest_degree", userData.getHighestDegree());
                data.putStringArrayList("colleges", userData.getColleges());
                currentFragment = new EditProfileFragmentEducation();
                currentFragment.setArguments(data);
                ft.replace(R.id.fragment_container_edit_profile, currentFragment,
                        "FRAGMENT_TAG_EDIT_PROFESSION");
                ft.addToBackStack(null);
                try {
                    ft.commit();
                } catch (IllegalStateException ignored) {
                }
                break;
            default:
                break;
        }

    }


    private void clearFragmentBackStack() {
        if (isInstanceActive) {
            try {
                fragmentManager.popBackStack(null,
                        FragmentManager.POP_BACK_STACK_INCLUSIVE);
            } catch (IllegalStateException ignored) {
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (currentFragmentLaunched == EditProfilePageType.INTERESTS) {
            try {
                boolean flag = ((EditProfileFragmentInterests) currentFragment).isHelpTrayVisible();
                if (flag) {
                    ((EditProfileFragmentInterests) currentFragment).hideHelpTray();
                } else {
                    closeEditProfileActivity(false);
                }
            } catch (ClassCastException e) {
                e.printStackTrace();
            }
        } else if (currentFragmentLaunched == EditProfilePageType.AGE) {
            if (((EditProfileFragmentAge) currentFragment).isCityViewExpanded()) {
                ((EditProfileFragmentAge) currentFragment).onBackPressed();
            } else {
                closeEditProfileActivity(false);
            }
        } else {
            closeEditProfileActivity(false);
        }

    }

    @Override
    public void processFragmentSaved(EditProfilePageType currentPageType,
                                     Bundle responseData) {
        if (userData == null) {
            userData = new UserData();
        }
        switch (currentPageType) {

            case AGE:
                userData.setFname(responseData.getString("fname"));
                userData.setLname(responseData.getString("lname"));
                userData.setDob_d(responseData.getInt("dob_d"));
                userData.setDob_m(responseData.getInt("dob_m"));
                userData.setDob_y(responseData.getInt("dob_y"));
                userData.setCountryId(responseData.getString("location_country_id"));
                userData.setStateId(responseData.getString("location_state_id"));
                userData.setCityId(responseData.getString("location_city_id"));
                userData.setHeight(responseData.getString("height"));
                String height_inches, height_feet;
                height_feet = "" + ((Integer.parseInt(userData.getHeight()) + 48) / 12);
                height_inches = "" + ((Integer.parseInt(userData.getHeight()) + 48) % 12);
                userData.setHeight_feet(height_feet);
                userData.setHeight_inches(height_inches);
                saveBasicDataOnServer(currentPageType);
                break;
            case INTERESTS:
                userData.setInterests(responseData.getStringArrayList("interests"));
                saveBasicDataOnServer(currentPageType);
                break;
            case PROFESSION:
                userData.setCurrentTitle(responseData.getString("current_title"));
                userData.setCompanies(responseData.getStringArrayList("companies"));
                userData.setIsNotWorking(responseData.getBoolean("isNotWorking"));
                userData.addLinkedInColleges(responseData
                        .getStringArrayList("linkedinColleges"));
                userData.setIncome(responseData.getString("income"));
                saveBasicDataOnServer(currentPageType);
                break;
            case EDUCATION:
                userData.setHighestDegree(responseData.getString("highest_degree"));
                userData.setColleges(responseData.getStringArrayList("colleges"));
                saveBasicDataOnServer(currentPageType);
                break;
            default:
                break;
        }
    }

    private void saveBasicDataOnServer(EditProfilePageType currentPageType) {
        if (isSavingInProgress) {
            return;
        }
        isSavingInProgress = true;
        isSaveCall = true;
        tapToRetryHandler.showLoaderWithCustomText("Saving");

        CustomOkHttpResponseHandler responseHandler = new CustomOkHttpResponseHandler(
                aActivity, TrulyMadlyActivities.edit_profile,
                TrulyMadlyEventTypes.register_server_save_call) {

            @Override
            public void onRequestSuccess(JSONObject response) {
                isSavingInProgress = false;
                if (tapToRetryHandler != null)
                    tapToRetryHandler.onSuccessFull();
                closeEditProfileActivity(true);
            }

            @Override
            public void onRequestFailure(Exception exception) {
                isSavingInProgress = false;
                tapToRetryHandler.onNetWorkFailed(exception);
                AlertsHandler.showNetworkError(aActivity, exception);

            }
        };

        (new HttpTasks()).updateBasicDataOnServer(userData, currentPageType,
                aActivity, responseHandler);
        if (Utility.isSet(userData.getFname())) {
            SPHandler.setString(aActivity,
                    ConstantsSP.SHARED_KEYS_USER_NAME, userData.getFname());
        }

    }

    private void hideLoaders() {
        tapToRetryHandler.onSuccessFull();
    }

    private void fetchUserDataForEdit() {
        isSaveCall = false;
        tapToRetryHandler.showLoader();
        String url = ConstantsUrls.get_user_edit_url();
        boolean showFailure = CachedDataHandler.showDataFromDb(aActivity, url, cachedDataInterface);
        CachedDataHandler cachedDataHandler = new CachedDataHandler(url, cachedDataInterface, this, TrulyMadlyActivities.edit_profile, TrulyMadlyEventTypes.page_load, null, showFailure);
        Map<String, String> map = GetOkHttpRequestParams.getHttpRequestParams(HttpRequestType.FETCH_USER_DATA_EDIT);
        map.put("hash", CachingDBHandler.getHashValue(aActivity, url, Utility.getMyId(aActivity)));
        cachedDataHandler.httpPost(aActivity, url, map);

    }


    private void closeEditProfileActivity(boolean isSave) {
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        intent.putExtra("isSave", isSave);
        finish();
    }

    @Override
    public void onResume() {
        super.onResume();
        isInstanceActive = true;
        fragmentManager = getSupportFragmentManager();
        aActivity = this;
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onStart() {
        super.onStart();
        BusProvider.getInstance().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        BusProvider.getInstance().unregister(this);

    }

    private void trackfunction(String Activity, String EventType) {
        long endTime = (new java.util.Date()).getTime();
        int time_diff = (int) ((endTime - startTime));
        TrulyMadlyTrackEvent.trackEvent(aActivity, Activity, EventType,
                time_diff, null, null, true);

    }

    @Override
    public void setHeaderText(String headerText) {
        actionBarHandler.setTitle(headerText);
    }

    @Override
    public void onViewCreated() {
        hideLoaders();
    }

    @Override
    public void setStartTime(long time) {
        startTime = time;
    }

}
