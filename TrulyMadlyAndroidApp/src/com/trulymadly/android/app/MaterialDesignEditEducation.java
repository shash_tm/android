package com.trulymadly.android.app;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Spinner;

import com.trulymadly.android.app.adapter.CustomSpinnerArrayAdapter;
import com.trulymadly.android.app.bus.BackPressedRegistrationFlow;
import com.trulymadly.android.app.bus.NextRegistrationFragment;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.listener.GetStartTimeInterface;
import com.trulymadly.android.app.modal.Degree;
import com.trulymadly.android.app.modal.EditPrefBasicDataModal;
import com.trulymadly.android.app.modal.UserData;
import com.trulymadly.android.app.sqlite.EditPrefDBHandler;
import com.trulymadly.android.app.utility.Utility;

import java.util.ArrayList;

public class MaterialDesignEditEducation extends Fragment implements
		OnItemSelectedListener {

	private NextRegistrationFragment nextFragment;
	private GetStartTimeInterface returnStartTimeOfEditEducationFragment;
	private Activity aActivity;
	private Spinner highestDegreeSpinner;
	private String highestDegree = null;
	private ArrayList<EditPrefBasicDataModal> highestDegreesList;
	private UserData userData;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			returnStartTimeOfEditEducationFragment = (GetStartTimeInterface) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement ReturnStartTimeOfEditEducationFragment ");
		}
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		aActivity = getActivity();
		Bundle data = getArguments();
		userData = new UserData();
		if (data.getSerializable("userData") != null) {
			userData = (UserData) data.getSerializable("userData");
		}
		if (userData != null) {
			highestDegree = userData.getHighestDegree();
		}
		highestDegreesList = EditPrefDBHandler.getBasicDataFromDB(
                Constants.EDIT_PREF_DEGREE, aActivity, 3);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle paramBundle) {

		nextFragment = new NextRegistrationFragment();
		View editEducationView = inflater.inflate(
				R.layout.material_design_edit_education, container, false);

		highestDegreeSpinner = (Spinner) editEducationView
				.findViewById(R.id.education_spinner);
		createDegreeSpinner(Degree.getDegrees(highestDegreesList, getResources()));
		highestDegreeSpinner.setOnItemSelectedListener(this);

		editEducationView.setOnKeyListener(new OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_BACK) {
					BackPressedRegistrationFlow backPressed = new BackPressedRegistrationFlow();
					userData.setHighestDegree(highestDegree);
					backPressed.setUserData(userData);
					Utility.fireBusEvent(aActivity, true, backPressed);
					return true;
				} else {
					return false;
				}
			}
		});
		returnStartTimeOfEditEducationFragment.setStartTime((new java.util.Date()).getTime());
		
		return editEducationView;
	}

	private void createDegreeSpinner(Degree[] degrees) {
		CustomSpinnerArrayAdapter spinnerDegreeAdapter = new CustomSpinnerArrayAdapter(
				getActivity(), R.layout.custom_spinner_2, degrees);
		spinnerDegreeAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		highestDegreeSpinner.setAdapter(spinnerDegreeAdapter);

		if (highestDegree != null) {
			for (int i = 0; i < degrees.length; i++) {
				if (degrees[i].getId().equalsIgnoreCase(highestDegree)) {
					if (spinnerDegreeAdapter.getCount() > i)
						highestDegreeSpinner.setSelection(i);
					break;
				}
			}
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View v, int pos, long id) {
		switch (parent.getId()) {
		case R.id.education_spinner:
			highestDegree = ((Degree) highestDegreeSpinner.getSelectedItem())
					.getId();
			break;
		default:
			break;
		}
		validateEducationData();
	}

	private void validateEducationData() {
		Boolean isValid = true;
		if (highestDegree == null || highestDegree.equalsIgnoreCase("0")) {
			isValid = false;
		}
		userData.setHighestDegree(highestDegree);
		nextFragment.setUserData(userData);
		nextFragment.setIsValid(isValid);
		Utility.fireBusEvent(aActivity, true, nextFragment);
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {

	}

}
