package com.trulymadly.android.app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap.Config;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.moe.pushlibrary.MoEHelper;
import com.squareup.picasso.Picasso;
import com.trulymadly.android.app.json.Constants.HttpRequestType;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.listener.ConfirmDialogInterface;
import com.trulymadly.android.app.listener.CustomOkHttpResponseHandler;
import com.trulymadly.android.app.modal.Photos;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.OkHttpHandler;
import com.trulymadly.android.app.utility.UiUtils;

import org.json.JSONObject;

import java.util.ArrayList;

public class MyPhotosFull extends Activity implements OnClickListener {

    private int pos;
    private LinearLayout delete_photo_id;
    private LinearLayout switch_profile_photo_id;
    private ProgressDialog mProgressDialog = null;
    private int photo_index = 0;
    private Context aContext;
    private Activity aActivity;
    private ArrayList<Photos> completePhotosList;
    private MoEHelper mhelper = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.myphotos_fullview_pager);
        aContext = this;
        aActivity = this;
        mhelper = new MoEHelper(this);
        ViewPager viewPager = (ViewPager) findViewById(R.id.myphotos_full_viewpager);
        delete_photo_id = (LinearLayout) findViewById(R.id.delete_photo_id);
        switch_profile_photo_id = (LinearLayout) findViewById(R.id.switch_profile_photo_id);

        delete_photo_id.setOnClickListener(this);
        switch_profile_photo_id.setOnClickListener(this);

        Intent i = getIntent();
        Bundle b = i.getExtras();
        if (b != null) {
            pos = b.getInt("POS");
            completePhotosList = new ArrayList<>();
            for (Object entry : (ArrayList<?>) b
                    .getSerializable("completePhotosList")) {
                completePhotosList.add((Photos) entry);
            }
            //completePhotosList = (ArrayList<Photos>) b.getSerializable("completePhotosList");
        }

        setHeaderIconVisibility(pos);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                setHeaderIconVisibility(position);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });
        if (completePhotosList != null && completePhotosList.size() > 0) {
            ImagePagerAdapter adapter = new ImagePagerAdapter(
                    getApplicationContext(), completePhotosList);
            viewPager.setAdapter(adapter);
            viewPager.setCurrentItem(pos);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void setHeaderIconVisibility(int p) {
        if (completePhotosList != null && completePhotosList.size() > p) {
            Photos aPhoto = completePhotosList.get(p);
            if (aPhoto.getIsProfile()) {
                delete_photo_id.setVisibility(View.VISIBLE);
                switch_profile_photo_id.setVisibility(View.INVISIBLE);
            } else {
                if (!aPhoto.getStatus().equalsIgnoreCase("Rejected")) {
                    switch_profile_photo_id.setVisibility(View.VISIBLE);
                } else {
                    switch_profile_photo_id.setVisibility(View.INVISIBLE);
                }
                delete_photo_id.setVisibility(View.VISIBLE);
            }
            photo_index = p;
        }
    }

    @Override
    public void onClick(View arg0) {

        int id = arg0.getId();

        switch (id) {
            case R.id.delete_photo_id:
                if (completePhotosList != null && completePhotosList.size() > 0) {
                    if (completePhotosList.get(photo_index).getIsProfile()) {
                        AlertsHandler.showAlertDialog(aActivity,
                                R.string.cannot_delete_photo);
                    } else {
                        String photo_id = completePhotosList.get(photo_index)
                                .getPhotoID();
                        showPopUp(photo_id, HttpRequestType.PHOTO_DELETE);
                    }
                }
                break;

            case R.id.switch_profile_photo_id:
                if (completePhotosList.get(photo_index).getCan_profile()) {
                    String photo_id = completePhotosList.get(photo_index)
                            .getPhotoID();
                    showPopUp(photo_id, HttpRequestType.PHOTO_PROFILE_SET);
                } else {
                    AlertsHandler.showAlertDialog(aActivity,
                            R.string.cannot_set_photo);
                }
                break;

            default:
                break;
        }
    }

    private void onPagerItemClick() {
        finish();
    }

    private void issueRequest(String id, HttpRequestType type) {

        mProgressDialog = UiUtils.showProgressBar(aContext, mProgressDialog);
        CustomOkHttpResponseHandler responseHandler = new CustomOkHttpResponseHandler(
                aContext) {

            @Override
            public void onRequestSuccess(JSONObject responseJSON) {
                mProgressDialog = UiUtils.hideProgressBar(mProgressDialog);
                finishWithResult(responseJSON.optJSONArray("data").toString());

            }

            @Override
            public void onRequestFailure(Exception exception) {
                mProgressDialog = UiUtils.hideProgressBar(mProgressDialog);
                AlertsHandler.showNetworkError(aActivity, exception);
            }

        };

        OkHttpHandler.httpPost(aContext, ConstantsUrls.get_photos_url(),
                GetOkHttpRequestParams.getHttpRequestParams(type, id),
                responseHandler);

    }

    private void finishWithResult(String response) {
        Bundle conData = new Bundle();
        conData.putString("response", response);
        Intent intent = new Intent();
        intent.putExtras(conData);
        setResult(RESULT_OK, intent);
        finish();
    }

    private void showPopUp(final String photoID, final HttpRequestType type) {
        int msgID;
        if (type == HttpRequestType.PHOTO_DELETE)
            msgID = R.string.delete_pic;
        else
            msgID = R.string.profile_pic_set;

        AlertsHandler.showConfirmDialog(this, msgID, R.string.yes, R.string.no, new ConfirmDialogInterface() {

            @Override
            public void onPositiveButtonSelected() {
                issueRequest(photoID, type);
            }

            @Override
            public void onNegativeButtonSelected() {

            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private class ImagePagerAdapter extends PagerAdapter implements
            OnClickListener {

        final ArrayList<Photos> completePhotosList;
        final Context context;
        LayoutInflater inflater;

        public ImagePagerAdapter(Context context,
                                 ArrayList<Photos> completePhotosList) {
            this.context = context;
            this.completePhotosList = completePhotosList;
        }

        @Override
        public int getCount() {
            return completePhotosList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            ImageView pic_id;

            inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View itemView = inflater.inflate(R.layout.myphotos_viewpager_item,
                    container, false);

            Photos aPhotos = completePhotosList.get(position);
            if (aPhotos != null) {
                pic_id = (ImageView) itemView.findViewById(R.id.pic_id);
                Picasso.with(aContext).load(aPhotos.getName()).config(Config.RGB_565).into(pic_id);
                itemView.setOnClickListener(this);
                container.addView(itemView);
            }
            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }

        @Override
        public void onClick(View v) {
            onPagerItemClick();
        }
    }

}
