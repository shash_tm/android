package com.trulymadly.android.app;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.crashlytics.android.Crashlytics;
import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.moe.pushlibrary.MoEHelper;
import com.squareup.picasso.Picasso;
import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.app.listener.OnActionBarClickedInterface;
import com.trulymadly.android.app.utility.ActionBarHandler;
import com.trulymadly.android.app.utility.CirclePageIndicator;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;

import java.io.File;
import java.io.IOException;


public class MenuFullViewPager extends AppCompatActivity {

    public static String[] pics;
    private int pos = 0;
    private MoEHelper mhelper = null;

    private Activity activity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_fullview_pager);
        Utility.disableScreenShot(this);
        mhelper = new MoEHelper(this);
        String title = getIntent().getStringExtra("title");
        activity = this;


        Utility.prefetchPhotos(this, pics, 0, TrulyMadlyEvent.TrulyMadlyActivities.matches);

        OnActionBarClickedInterface onActionBarClickedInterface = new OnActionBarClickedInterface() {

            @Override
            public void onConversationsClicked() {
            }

            @Override
            public void onLocationClicked() {
            }


            @Override
            public void onCuratedDealsClicked() {

            }

            @Override
            public void onBackClicked() {
                try {
                    onBackPressed();
                } catch (IllegalStateException | NullPointerException ignored) {

                }
            }

            @Override
            public void onUserProfileClicked() {
            }

            @Override
            public void onTitleClicked() {
            }

            @Override
            public void onTitleLongClicked() {
            }

            @Override
            public void onSearchViewOpened() {

            }

            @Override
            public void onSearchViewClosed() {

            }

            @Override
            public void onCategoriesClicked() {

            }
        };

        if (Utility.isSet(title))
            new ActionBarHandler(this, title, null, onActionBarClickedInterface, false, false, false);

        else
            new ActionBarHandler(this, getResources().getString(R.string.photo_gallery), null, onActionBarClickedInterface, false, false, false);


        ViewPager viewPager = (ViewPager) findViewById(R.id.album_fullview_pager);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            pos = b.getInt("POS", 0);
        }
        MenuPagerAdapter adapter = new MenuPagerAdapter(
                getApplicationContext(), pics, activity);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(pos);
        CirclePageIndicator indicator = (CirclePageIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(viewPager);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }


    private class MenuPagerAdapter extends PagerAdapter {

        final Context context;
        final String[] thumbnails;
        final Activity activity;
        LayoutInflater inflater;


        public MenuPagerAdapter(Context context, String[] thumbnails, Activity activity) {
            this.context = context;
            this.thumbnails = thumbnails;
            this.activity = activity;
        }

        @Override
        public int getCount() {
            return thumbnails == null ? 0 : thumbnails.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {

            final SubsamplingScaleImageView album_pic_id;

            inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View itemView = inflater.inflate(R.layout.menu_viewpager_item,
                    container, false);
            ImageView backup_image_view = (ImageView) itemView.findViewById(R.id.backup_image_view);
            album_pic_id = (SubsamplingScaleImageView) itemView.findViewById(R.id.album_pic_id);
            if (UiUtils.isXxhdpi(activity)) {
                album_pic_id.setMaxScale(5F);
            } else
                album_pic_id.setMaxScale(3F);
            final ProgressBar custom_prog_bar_photo_id = (ProgressBar) itemView.findViewById(R.id.custom_prog_bar_photo_id);

            (new CreateBitmapAsyncTask(custom_prog_bar_photo_id, album_pic_id, backup_image_view)).execute(thumbnails[position]);


            container.addView(itemView);
            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((RelativeLayout) object);

        }
    }

    private class CreateBitmapAsyncTask extends AsyncTask<String, Void, File> {
        final ProgressBar custom_prog_bar_photo_id;
        final SubsamplingScaleImageView album_pic_id;
        final ImageView backup_image_view;
        Bitmap bmap = null;
        String url;

        CreateBitmapAsyncTask(ProgressBar custom_prog_bar_photo_id, SubsamplingScaleImageView album_pic_id, ImageView backup_image_view) {
            this.custom_prog_bar_photo_id = custom_prog_bar_photo_id;
            this.album_pic_id = album_pic_id;
            this.backup_image_view = backup_image_view;
        }

        @Override
        protected File doInBackground(String... urls) {
            if (urls != null && urls[0] != null) {

                this.url = urls[0];
                try {
                    bmap = Picasso.with(activity).load(url).get();

                } catch (IOException | OutOfMemoryError e) {
                    Crashlytics.logException(e);
                }
            }
            return null;


        }

        @Override
        protected void onPostExecute(File file) {


            if (bmap != null) {
                backup_image_view.setVisibility(View.GONE);
                album_pic_id.setVisibility(View.VISIBLE);
                album_pic_id.setImage(ImageSource.bitmap(bmap));
                custom_prog_bar_photo_id.setVisibility(View.GONE);

            } else {
                backup_image_view.setVisibility(View.VISIBLE);
                album_pic_id.setVisibility(View.GONE);
                Picasso.with(activity).load(url).into(backup_image_view);
                custom_prog_bar_photo_id.setVisibility(View.GONE);

            }


        }
    }
}



