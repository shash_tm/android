package com.trulymadly.android.app;

import android.content.Context;
import android.database.Cursor;

import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.Constants.MessageState;
import com.trulymadly.android.app.modal.CuratedDealsChatsModal;
import com.trulymadly.android.app.modal.EventChatModal;
import com.trulymadly.android.app.modal.QuizImage;
import com.trulymadly.android.app.sqlite.QuizDBHandler;
import com.trulymadly.android.app.utility.FilesHandler;
import com.trulymadly.android.app.utility.MaskTransformation;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import static com.google.common.base.Preconditions.checkNotNull;

public class MessageModal {

    public static final String JPEG_URL_KEY = "jpeg";
    public static final String WEBP_URL_KEY = "webp";
    public static final String JPEG_SIZE_KEY = "jpeg_size";
    public static final String WEBP_SIZE_KEY = "webp_size";
    public static final String THUMBNAIL_JPEG_KEY = "thumbnail_jpeg";
    public static final String THUMBNAIL_WEBP_KEY = "thumnnail_webp";
    private static final String METADATA_KEY = "metadata";
    private static final String URLS_KEY = "urls";
    private static final String LOCAL_URL_KEY = "local_url";

    private static final String EVENT_ID = "event_id";
    private static final String EVENT_NAME = "event_name";
    private static final String EVENT_IMAGE_URL = "event_image_url";

    private String msg_id, match_id, message, tstamp, uniqueId = "";
    private int quiz_id;
    private boolean is_message_sent_by_me = true;
    private String my_id;
    private String msgJsonString;
    private MessageType messageType;
    private MessageState message_state;

    private String mDealId;
    private String mDateSpotId;
    private CuratedDealsChatsModal mCuratedDealsChatsModal;
    private String aLinkLandingUrl, aLinkImageUrl;

    private String mJpegUrl, mWebPUrl, mThumbnailJpegUrl, mThumbnailWebpUrl, mLocalUrl;
    private String mJpegSize, mWebPSize;
    private String mMetadataString;

    private EventChatModal mEventChatModal;
    //IMAGE related optional variables
    private MaskTransformation optImageMaskTransformation;
    private int optImagePlaceholderDrawable = -1;
    private Boolean optIsWebPUsed = null;
    private String optImageUri, optFallbackUri, optImageSize;
    //EVENT related optional variables
    private MaskTransformation optEventMaskTransformation;
    private int optEventPlaceholderDrawable = -1;
    //CD_ASK related optional variables
    private MaskTransformation optCDAskMaskTransformation;
    private int optCDAskPlaceholderDrawable = -1;
    //Sticker related optional variables
    private String optStickerImageUrl, optStickerKey;
    //Quiz Message related Optional Variables
    private Boolean optToShowFlare = null;
    private QuizImage optQuizMsgImage = null;
    //Quiz related Optional Variables
    private String optQuizName, optNewMessageText;
    private QuizImage optQuizImage;
    private boolean isSparkAccepted;

    public MessageModal(String my_id) {
        this.my_id = my_id;
        // setting default message type to text
        this.messageType = MessageType.TEXT;
    }

    public MessageModal(String my_id, JSONObject data) {
        this.my_id = my_id;
        // setting default message type to text
        this.messageType = MessageType.TEXT;
        parseMessage(data);
    }

    public static MessageType getMessageTypeFromString(String type) {
        if (!Utility.isSet(type)) {
            return MessageType.TEXT;
        }
        type = type.toUpperCase(Locale.ENGLISH);
        try {
            return MessageType.valueOf(type);
        } catch (IllegalArgumentException e) {
            return MessageType.TEXT;
        }
    }

    public static String prepareImageMetadataJsonString(String jpegUrl, String jpegSize,
                                                        String webpUrl, String webpSize,
                                                        String localUrl, String jpegThumbnailUrl,
                                                        String webpThumbnailUrl) {
        JSONObject dummyObject = new JSONObject();
        JSONObject urls = new JSONObject();
        try {
            if (Utility.isSet(jpegUrl)) {
                urls.put(MessageModal.JPEG_URL_KEY, jpegUrl);
            }
            if (Utility.isSet(jpegSize)) {
                urls.put(MessageModal.JPEG_SIZE_KEY, jpegSize);
            }
            if (Utility.isSet(webpUrl)) {
                urls.put(MessageModal.WEBP_URL_KEY, webpUrl);
            }
            if (Utility.isSet(webpSize)) {
                urls.put(MessageModal.WEBP_SIZE_KEY, webpSize);
            }
            if (Utility.isSet(localUrl)) {
                urls.put(MessageModal.LOCAL_URL_KEY, localUrl);
            }
            if (Utility.isSet(jpegThumbnailUrl)) {
                urls.put(MessageModal.THUMBNAIL_JPEG_KEY, jpegThumbnailUrl);
            }
            if (Utility.isSet(webpThumbnailUrl)) {
                urls.put(MessageModal.THUMBNAIL_WEBP_KEY, webpThumbnailUrl);
            }
            dummyObject.put(MessageModal.URLS_KEY, urls);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return dummyObject.toString();
    }

	/*
     * public String getDisplayTimeStamp(){ String tstamp=this.tstamp;
	 * if(Utility.isSet(tstamp)){ return Utility.GMTToMyTime(tstamp); }
	 *
	 * return tstamp; }
	 */

    public static String prepareEventMetadataJsonString(EventChatModal eventChatModal) {
        JSONObject dummyObject = new JSONObject();
        try {
            if (Utility.isSet(eventChatModal.getmEventId())) {
                dummyObject.put(MessageModal.EVENT_ID, eventChatModal.getmEventId());
            }
            if (Utility.isSet(eventChatModal.getmName())) {
                dummyObject.put(MessageModal.EVENT_NAME, eventChatModal.getmName());
            }
            if (Utility.isSet(eventChatModal.getmImageUrl())) {
                dummyObject.put(MessageModal.EVENT_IMAGE_URL, eventChatModal.getmImageUrl());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return dummyObject.toString();
    }

    public static JSONObject prepareMetadataJsonObject(MessageType messageType,
                                                       CuratedDealsChatsModal curatedDealsChatsModal,
                                                       String messageJson, boolean isAReplyToSpark) {
        JSONObject metadata = null;
        if (curatedDealsChatsModal != null &&
                (messageType == MessageType.CD_VOUCHER || messageType == MessageType.CD_ASK)) {
            metadata = new JSONObject();
            try {
                metadata.put("date_spot_id", curatedDealsChatsModal.getmDateSpotId());
                metadata.put(Constants.CD_KEY_IMAGE_URI, curatedDealsChatsModal.getmRectUri());
                //TODO: DATABASE: remove this field , no longer need, also remove from db
                metadata.put("message", curatedDealsChatsModal.getmMessage());
                metadata.put("message_type", messageType.name());
                metadata.put("address", curatedDealsChatsModal.getmAddress());
                if (messageType == MessageType.CD_VOUCHER) {
                    metadata.put("deal_status", "accepted");
                }
                if (Utility.isSet(curatedDealsChatsModal.getmDealId()))
                    metadata.put("deal_id", curatedDealsChatsModal.getmDealId());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (messageType == MessageType.IMAGE) {
            try {
                metadata = new JSONObject(messageJson);
                metadata.put("message_type", messageType.name());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if(messageType == MessageType.SHARE_EVENT){
            try {
                metadata = new JSONObject(messageJson);
                metadata.put("message_type", messageType.name());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if(isAReplyToSpark){
            try {
                metadata = new JSONObject();
                metadata.put("message_type", messageType.name());
                metadata.put("spark_accepted", true);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return metadata;
    }

    public MessageState getMessageState() {
        return message_state;
    }

    public void setMessageState(MessageState messageState) {
        this.message_state = messageState;
    }

    public String getMy_id() {
        return my_id;
    }

    public void setMy_id(String my_id) {
        this.my_id = my_id;
    }

    public String getMsg_id() {
        return msg_id;
    }

    public void setMsg_id(String msg_id) {
        this.msg_id = msg_id;
    }

    public void setMyID(String id) {
        this.my_id = id;
    }

    /*
     * getBool raw data. Used mainly for stickers and other data types
     */
    public String getMessage() {
        return message;
    }

    private void setMessage(String message) {
        this.message = message;
    }

    /*
     * stripping the message.
     */
    public String getDisplayMessage() {
        String msg_to_return = Utility.stripHtml(this.message);
        if (Utility.isSet(msg_to_return)) {
            // http://stackoverflow.com/questions/388461/how-can-i-pad-a-string-in-java
            // msg_to_return = String.format("%1$-30s", msg_to_return);
        }
        return msg_to_return;
    }

    public String getTstamp() {
        return tstamp;
    }

    public void setTstamp(String tstamp) {
        this.tstamp = tstamp;
    }

    private Date gmtDate() {
        Date date = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
                    Locale.ENGLISH);
            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
            date = sdf.parse(this.tstamp);
            return date;
        } catch (Exception ignored) {
        }
        return date;
    }

    /**
     * //http://stackoverflow.com/questions/14853389/how-to-convert-utc-
     * timestamp-to-device-local-time-in-android
     * //http://stackoverflow.com/questions/2818086/android-get-current-utc-time
     * //http://stackoverflow.com/questions/10725246/java-android-convert-a-gmt-
     * time-string-to-local-time?rq=1
     *
     * @return
     */
    public String getMessageDisplayTime() {

        String message_time = "";
        Date gmtDate = gmtDate();
        if (gmtDate != null) {
            // http://javatechniques.com/blog/dateformat-and-simpledateformat-examples/
            SimpleDateFormat sdf_mytime = new SimpleDateFormat("h:mm a",
                    Locale.ENGLISH);
            Calendar cal = Calendar.getInstance();
            TimeZone tz = cal.getTimeZone();
            sdf_mytime.setTimeZone(tz);
            message_time = sdf_mytime.format(gmtDate);
            message_time = message_time.replace("am", "AM").replace("pm", "PM");

        }
        return message_time;
    }

    public String getMessageDisplayDate() {

        String message_time = "";
        Date gmtDate = gmtDate();
        if (gmtDate != null) {
            SimpleDateFormat sdf_mytime = new SimpleDateFormat(
                    "EEE MMM dd, yyyy", Locale.ENGLISH);
            Calendar cal = Calendar.getInstance();
            TimeZone tz = cal.getTimeZone();
            sdf_mytime.setTimeZone(tz);
            message_time = sdf_mytime.format(gmtDate);

            // getBool current time
            Date now = new Date();
            String today_time = sdf_mytime.format(now);
            if (today_time.equals(message_time)) {
                return "Today";
            }
        }
        return message_time;
    }

    public boolean isMessageSentByMe() {
        return this.is_message_sent_by_me;
    }

    public MessageType getMessageType() {
        return this.messageType;
    }

    public void setMessageType(String type) {
        this.messageType = getMessageTypeFromString(type);
    }

    /**
     * Parse Message from server JSON Object
     *
     * @param object Server JSON Objet
     */
    public void parseMessage(JSONObject object) {
        setMsgJsonString(object.toString());

        setMsg_id(object.optString("msg_id"));
        setMessage(object.optString("msg"));
        setMessageType(object.optString("message_type"));
        setTstamp(object.optString("time"));
        setQuiz_id(object.optInt("quiz_id"));
        setUniqueId(object.optString("unique_id", ""));

        String senderId = object.optString("sender_id");
        String receiverId = object.optString("receiver_id");
        setSparkAccepted(object.optBoolean("update_spark"));

        setmDealId(object.optString(Constants.CD_KEY_DEAL_ID));
        setmDateSpotId(object.optString(Constants.CD_KEY_DATESPOT_ID));

        JSONObject metadata = null;
        if (object.has(METADATA_KEY)) {
            metadata = object.optJSONObject(METADATA_KEY);
            if (metadata == null) {
                try {
                    metadata = new JSONObject(object.optString(METADATA_KEY));
                } catch (JSONException e) {
                    metadata = null;
                }
            }
        }
        if (metadata != null) {
            switch (getMessageType()) {
                case IMAGE:
                case IMAGE_LINK:
                case SHARE_EVENT:
                    setmMetadataString(metadata.toString());
                    break;

                default:
                    String dealId = null, dateSpotId = null, rectUri = null,
                            message = null, address = null;
                    MessageModal.MessageType messageType = null;
                    try {
                        if (metadata.has("deal_id"))
                            dealId = metadata.getString("deal_id");
                        dateSpotId = metadata.getString("date_spot_id");
                        rectUri = metadata.getString(Constants.CD_KEY_IMAGE_URI);
                        address = metadata.getString("address");
                        messageType = getMessageType();
                        message = getMessage();//metadata.getString("message");
                    } catch (JSONException ignored) {
                    }

                    setmCuratedDealsChatsModal(new CuratedDealsChatsModal(messageType, dealId, dateSpotId,
                            rectUri, message, address
                    ));
                    setmDateSpotId(dateSpotId);
                    setmDealId(dealId);
                    break;
            }
            //TODO: DATABASE: read link from server params from DB in MessageModal
        }


        is_message_sent_by_me = !(Utility.isSet(my_id) && Utility.isSet(senderId)) || my_id.equals(senderId);

        // for content type => images
        if (is_message_sent_by_me) {
            match_id = receiverId;
            message_state = MessageState.OUTGOING_SENT;
        } else {
            match_id = senderId;
            message_state = MessageState.INCOMING_DELIVERED;
        }
    }

    /**
     * Parse Message from parameters
     *
     * @param msg_id
     * @param msg
     * @param time
     * @param messageType
     * @param matchId
     * @param message_state
     * @param quiz_id
     * @param curatedDealsChatsModal
     * @param metadataJson
     */
    public void parseMessage(String msg_id, String msg, String time, String messageType,
                             String matchId, MessageState message_state, int quiz_id,
                             CuratedDealsChatsModal curatedDealsChatsModal, String metadataJson) {
        setMsg_id(msg_id);
        setMessage(msg);
        setMessageType(messageType);
        setTstamp(time);
        setMatch_id(matchId);
        setMessageState(message_state);
        setQuiz_id(quiz_id);
        setmCuratedDealsChatsModal(curatedDealsChatsModal);
        if (curatedDealsChatsModal != null) {
            setmDateSpotId(curatedDealsChatsModal.getmDateSpotId());
            if (Utility.isSet(curatedDealsChatsModal.getmDealId())) {
                setmDealId(curatedDealsChatsModal.getmDealId());
            }
        }

//		setmMetadataString(metadataJson);
        if (Utility.isSet(metadataJson)) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put(METADATA_KEY, metadataJson);
                setMsgJsonString(jsonObject.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        setmCuratedDealsChatsModal(curatedDealsChatsModal);
        if (curatedDealsChatsModal != null) {
            setmDateSpotId(curatedDealsChatsModal.getmDateSpotId());
            if (Utility.isSet(curatedDealsChatsModal.getmDealId())) {
                setmDealId(curatedDealsChatsModal.getmDealId());
            }
        }

        switch (message_state) {
            case INCOMING_DELIVERED:
            case INCOMING_READ:
                is_message_sent_by_me = false;
                break;
            case OUTGOING_DELIVERED:
            case OUTGOING_FAILED:
            case OUTGOING_READ:
            case OUTGOING_SENDING:
            case OUTGOING_SENT:
                is_message_sent_by_me = true;
                break;
            default:
                is_message_sent_by_me = true;
                break;
        }

    }

    /**
     * Parse message from DB data cursor.
     *
     * @param cursor
     */
    public void parseMessage(Cursor cursor) {
        setMsg_id(cursor.getString(cursor.getColumnIndex("message_id")));
        setMessage(cursor.getString(cursor.getColumnIndex("message_text")));
        this.messageType = getMessageTypeFromString(cursor.getString(cursor.getColumnIndex("message_type")));
        setTstamp(cursor.getString(cursor.getColumnIndex("tstamp")));
        setMatch_id(cursor.getString(cursor.getColumnIndex("match_id")));
        setMessageState(MessageState.valueOf(cursor.getString(cursor.getColumnIndex("message_state"))));
        setQuiz_id(cursor.getInt(cursor.getColumnIndex("quiz_id")));
        setMsgJsonString(cursor.getString(cursor.getColumnIndex("msg_json")));
        setmDealId(cursor.getString(cursor.getColumnIndex(Constants.CD_KEY_DEAL_ID)));
        setmDateSpotId(cursor.getString(cursor.getColumnIndex(Constants.CD_KEY_DATESPOT_ID)));
        switch (message_state) {
            case INCOMING_DELIVERED:
            case INCOMING_READ:
                is_message_sent_by_me = false;
                break;
            case OUTGOING_DELIVERED:
            case OUTGOING_FAILED:
            case OUTGOING_READ:
            case OUTGOING_SENDING:
            case OUTGOING_SENT:
                is_message_sent_by_me = true;
                break;
            default:
                is_message_sent_by_me = true;
                break;
        }
    }

    public String getMatch_id() {
        return match_id;
    }

    public void setMatch_id(String match_id) {
        this.match_id = match_id;
    }

    public int getQuiz_id() {
        return quiz_id;
    }

    private void setQuiz_id(int quiz_id) {
        this.quiz_id = quiz_id;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    private void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getmDealId() {
        return mDealId;
    }

    private void setmDealId(String mDealId) {
        this.mDealId = mDealId;
    }

    public CuratedDealsChatsModal getmCuratedDealsChatsModal() {
        return mCuratedDealsChatsModal;
    }

    public void setmCuratedDealsChatsModal(CuratedDealsChatsModal mCuratedDealsChatsModal) {
        this.mCuratedDealsChatsModal = mCuratedDealsChatsModal;
    }

    public String getmDateSpotId() {
        return mDateSpotId;
    }

    private void setmDateSpotId(String mDateSpotId) {
        this.mDateSpotId = mDateSpotId;
    }

    public String getMsgJsonString() {
        return msgJsonString;
    }

    public void setMsgJsonString(String msgJsonString) {
        this.msgJsonString = msgJsonString;
        if (Utility.isSet(msgJsonString)) {
            try {
                JSONObject jsonObject = new JSONObject(msgJsonString);
                if (jsonObject.has(METADATA_KEY)) {
                    JSONObject metadata = jsonObject.optJSONObject(METADATA_KEY);
                    if (metadata == null) {
                        metadata = new JSONObject(jsonObject.optString(METADATA_KEY));
                    }
                    setmMetadataString(metadata.toString());
                } else if (msgJsonString.contains(URLS_KEY)) {
                    setmMetadataString(msgJsonString);
                }
            } catch (JSONException ignored) {
            }
        }
    }

    public String getmMetadataString() {
        return mMetadataString;
    }

    private void setmMetadataString(String mMetadataString) {
        this.mMetadataString = mMetadataString;
        parseMetadataJsonString();
    }

    public String getaLinkImageUrl() {
        return aLinkImageUrl;
    }

    private void setaLinkImageUrl(String aLinkImageUrl) {
        this.aLinkImageUrl = aLinkImageUrl;
    }

    public String getaLinkLandingUrl() {
        return aLinkLandingUrl;
    }

    private void setaLinkLandingUrl(String aLinkLandingUrl) {
        this.aLinkLandingUrl = aLinkLandingUrl;
    }

    private void parseMetadataJsonString() {
        if (mMetadataString == null) {
            return;
        }

        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(mMetadataString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        switch (messageType) {
            case IMAGE:
                /*
                * {
                * 	"urls": {
                * 		"jpeg": "some_url",
                * 		"jpeg_size": "250KB",
                * 		"webp": "some url",
                * 		"webp_size": "250KB",
                * 		"thumbnail": "some url"
                * 	},
                * 	"message_type": "IMAGE"
                * }
                * */
                if (jsonObject != null) {
                    JSONObject urls = jsonObject.optJSONObject(URLS_KEY);
                    if (urls != null) {
                        setmJpegUrl(urls.optString(JPEG_URL_KEY));
                        setmJpegSize(urls.optString(JPEG_SIZE_KEY));

                        setmLocalUrl(urls.optString(LOCAL_URL_KEY));

                        setmWebPUrl(urls.optString(WEBP_URL_KEY));
                        setmWebPSize(urls.optString(WEBP_SIZE_KEY));

                        setmThumbnailJpegUrl(urls.optString(THUMBNAIL_JPEG_KEY));
                        setmThumbnailWebpUrl(urls.optString(THUMBNAIL_WEBP_KEY));
                    }
                }
                break;
            case IMAGE_LINK:
                /*
                * {
                * 	"link_landing_url": "some_url",
                *   "link_image_url": "some_url",
                * 	"message_type": "IMAGE_LINK"
                * }
                * */
                if (jsonObject != null) {
                    setaLinkLandingUrl(jsonObject.optString("link_landing_url"));
                    setaLinkImageUrl(jsonObject.optString("link_image_url"));
                }
                break;
            case SHARE_EVENT:
                if (jsonObject != null) {
                    setmEventChatModal(new EventChatModal(jsonObject.optString(EVENT_ID),
                            jsonObject.optString(EVENT_NAME),
                            jsonObject.optString(EVENT_IMAGE_URL)));
                }
                break;
            default:
                break;
        }
    }

    public boolean isSparkAccepted() {
        return isSparkAccepted;
    }

    public void setSparkAccepted(boolean sparkAccepted) {
        isSparkAccepted = sparkAccepted;
    }

    public String getmThumbnailJpegUrl() {
        return mThumbnailJpegUrl;
    }

    private void setmThumbnailJpegUrl(String mThumbnailJpegUrl) {
        this.mThumbnailJpegUrl = mThumbnailJpegUrl;
    }

    public String getmThumbnailWebpUrl() {
        return mThumbnailWebpUrl;
    }

    private void setmThumbnailWebpUrl(String mThumbnailWebpUrl) {
        this.mThumbnailWebpUrl = mThumbnailWebpUrl;
    }

    public String getmWebPUrl() {
        return mWebPUrl;
    }

    private void setmWebPUrl(String mWebPUrl) {
        this.mWebPUrl = mWebPUrl;
    }

    public String getmJpegUrl() {
        return mJpegUrl;
    }

    private void setmJpegUrl(String mJpegUrl) {
        this.mJpegUrl = mJpegUrl;
    }

    public String getmWebPSize() {
        return mWebPSize;
    }

    private void setmWebPSize(String mWebPSize) {
        this.mWebPSize = mWebPSize;
    }

    public String getmJpegSize() {
        return mJpegSize;
    }

    private void setmJpegSize(String mJpegSize) {
        this.mJpegSize = mJpegSize;
    }

    public String getmLocalUrl() {
        return mLocalUrl;
    }

    private void setmLocalUrl(String mLocalUrl) {
        this.mLocalUrl = mLocalUrl;
    }

    public EventChatModal getmEventChatModal() {
        return mEventChatModal;
    }

    private void setmEventChatModal(EventChatModal mEventChatModal) {
        this.mEventChatModal = mEventChatModal;
    }

    public boolean getAndSetOptIsWebPUsed(){
        if(optIsWebPUsed == null){
            optIsWebPUsed = (Utility.isWebPSupported()
                    && Utility.isSet(getmWebPUrl()));
        }

        return optIsWebPUsed;
    }

    public String getAndSetOptImageSize(){
        if(!Utility.isSet(optImageSize)){
            optImageSize = (getAndSetOptIsWebPUsed()) ? getmWebPSize() : getmJpegSize();
        }

        return optImageSize;
    }

    public String getAndSetOptFallbackUri(){
        if(!Utility.isSet(optFallbackUri)){
            if (!isMessageSentByMe()) {
                if(Utility.isSet(getmThumbnailWebpUrl())){
                    optFallbackUri = getmThumbnailWebpUrl();
                }else if(Utility.isSet(getmThumbnailJpegUrl())) {
                    optFallbackUri = getmThumbnailJpegUrl();
                }
            }
        }

        return optFallbackUri;
    }

    public String getAndSetOptImageUri(){
        if(!Utility.isSet(optImageUri)){
            optImageUri = (getAndSetOptIsWebPUsed())?
                    getmWebPUrl()
                    :((Utility.isSet(getmJpegUrl()))?
                    getmJpegUrl()
                    :getmLocalUrl());
        }

        return optImageUri;
    }

    public MaskTransformation getAndSetOptImageMaskTransformation(
            MaskTransformation sentTransformation,
            MaskTransformation receivedTransformation){
        if(optImageMaskTransformation == null) {
            if (getMessageState() == Constants.MessageState.INCOMING_DELIVERED ||
                    getMessageState() == Constants.MessageState.INCOMING_READ) {
                optImageMaskTransformation = receivedTransformation;
            } else {
                optImageMaskTransformation = sentTransformation;
            }
        }

        return optImageMaskTransformation;
    }

    public int getAndSetOptImagePlaceholderDrawable(int sentDrawable, int receivedDrawable){
        if(optImagePlaceholderDrawable == -1) {
            if (getMessageState() == Constants.MessageState.INCOMING_DELIVERED ||
                    getMessageState() == Constants.MessageState.INCOMING_READ) {
                optImagePlaceholderDrawable = receivedDrawable;
            } else {
                optImagePlaceholderDrawable = sentDrawable;
            }
        }

        return optImagePlaceholderDrawable;
    }

    public MaskTransformation getAndSetOptEventMaskTransformation(
            MaskTransformation sentTransformation,
            MaskTransformation receivedTransformation){
        if(optEventMaskTransformation == null) {
            if (getMessageState() == Constants.MessageState.INCOMING_DELIVERED ||
                    getMessageState() == Constants.MessageState.INCOMING_READ) {
                optEventMaskTransformation = receivedTransformation;
            } else {
                optEventMaskTransformation = sentTransformation;
            }
        }

        return optEventMaskTransformation;
    }

    public int getAndSetOptEventPlaceholderDrawable(int sentDrawable, int receivedDrawable){
        if(optEventPlaceholderDrawable == -1) {
            if (getMessageState() == Constants.MessageState.INCOMING_DELIVERED ||
                    getMessageState() == Constants.MessageState.INCOMING_READ) {
                optEventPlaceholderDrawable = receivedDrawable;
            } else {
                optEventPlaceholderDrawable = sentDrawable;
            }
        }

        return optEventPlaceholderDrawable;
    }

    public MaskTransformation getAndSetOptCDAskMaskTransformation(
            MaskTransformation sentTransformation,
            MaskTransformation receivedTransformation){
        if(optCDAskMaskTransformation == null) {
            if (getMessageState() == Constants.MessageState.INCOMING_DELIVERED ||
                    getMessageState() == Constants.MessageState.INCOMING_READ) {
                optCDAskMaskTransformation = receivedTransformation;
            } else {
                optCDAskMaskTransformation = sentTransformation;
            }
        }

        return optCDAskMaskTransformation;
    }

    public int getAndSetOptCDAskPlaceholderDrawable(int sentDrawable, int receivedDrawable){
        if(optCDAskPlaceholderDrawable == -1) {
            if (getMessageState() == Constants.MessageState.INCOMING_DELIVERED ||
                    getMessageState() == Constants.MessageState.INCOMING_READ) {
                optCDAskPlaceholderDrawable = receivedDrawable;
            } else {
                optCDAskPlaceholderDrawable = sentDrawable;
            }
        }

        return optCDAskPlaceholderDrawable;
    }

    public String getAndSetOptStickerImageUrl(Context context){
        if(!Utility.isSet(optStickerImageUrl)){
            optStickerImageUrl = FilesHandler.getStickerUrlBasedOnDensity(context, getMessage());
        }

        return optStickerImageUrl;
    }

    public String getAndSetOptStickerKey(Context context){
        if(!Utility.isSet(optStickerKey)){
            optStickerKey = FilesHandler.getStickerKey(getAndSetOptStickerImageUrl(context));
        }

        return optStickerKey;
    }

    public boolean getAndSetOptToShowVariable(Context context){
        if(optToShowFlare == null){
            optToShowFlare = QuizDBHandler.returnShowFlareStatus(getMatch_id(),
                    Utility.getMyId(context), getQuiz_id(), context);
        }

        return optToShowFlare;
    }

    public QuizImage getAndSetOptQuizMsgImage(Context context){
        if(optQuizMsgImage == null){
            optQuizMsgImage = QuizDBHandler.getQuizImageUrl(context, getQuiz_id());
        }

        return optQuizMsgImage;
    }

    public String getAndSetOptQuizName(Context context){
        if(!Utility.isSet(optQuizName)){
            optQuizName = QuizDBHandler.getQuizName(context, getQuiz_id());
        }

        return optQuizName;
    }

    public String getAndSetOptNewMessageText(Context context){
        if(!Utility.isSet(optNewMessageText)){
            //HACK TO CONVERT NUDGE SINGLE AND NUDGE DOUBLE MESSAGES TO NEW FORMAT
            optNewMessageText = checkNotNull(getDisplayMessage());
            if (Utility.isSet(optNewMessageText)) {
                if (optNewMessageText.contains("Let's check our")) {
                    optNewMessageText = context.getResources().getString(R.string.check_answers);
                } else if (optNewMessageText.contains("Tap to play") || optNewMessageText.contains("Ready to play")) {
                    optNewMessageText = context.getResources().getString(R.string.lets_play_the_quiz_together);
                }
            }
        }

        return optNewMessageText;
    }

    public QuizImage getAndSetOptQuizImage(Context context){
        if(optQuizImage == null){
            optQuizImage = QuizDBHandler.getQuizImageUrl(context, getQuiz_id());
        }

        return optQuizImage;
    }

    @SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
    @Override
    public boolean equals(Object o) {
        try {
            return Utility.stringCompare(getMsg_id(), ((MessageModal) o).getMsg_id());
        } catch (NullPointerException ignore) {
            return false;
        }
    }

    /**
     * text -> default sticker -> our defined stickers image: allow user to send
     * the images -> should be like stickers
     */
    public enum MessageType {
        LOAD_MORE(0),
        TEXT(1), STICKER(2), MUTUAL_LIKE(3), NUDGE_SINGLE(4), NUDGE_DOUBLE(5), QUIZ_MESSAGE(6),
        CD_ASK(7), CD_VOUCHER(8), IMAGE(9), IMAGE_LINK(10), SHARE_EVENT(11), SPARK(12),
        SPARK_HEADER(13);

        private final int mValue;

        MessageType(int mValue) {
            this.mValue = mValue;
        }

        public static MessageType getMessageTypeFromValue(int value) {
            switch (value) {
                case 0:
                    return LOAD_MORE;
                case 1:
                    return TEXT;
                case 2:
                    return STICKER;
                case 3:
                    return MUTUAL_LIKE;
                case 4:
                    return NUDGE_SINGLE;
                case 5:
                    return NUDGE_DOUBLE;
                case 6:
                    return QUIZ_MESSAGE;
                case 7:
                    return CD_ASK;
                case 8:
                    return CD_VOUCHER;
                case 9:
                    return IMAGE;
                case 10:
                    return IMAGE_LINK;
                case 11:
                    return SHARE_EVENT;
                case 12:
                    return SPARK;
                case 13:
                    return SPARK_HEADER;
            }

            return TEXT;
        }

        public int getValue() {
            return mValue;
        }
    }
}
