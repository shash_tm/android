package com.trulymadly.android.app;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.squareup.otto.Subscribe;
import com.trulymadly.android.app.bus.BusProvider;
import com.trulymadly.android.app.bus.ServiceEvent;
import com.trulymadly.android.app.bus.TriggerConnectingEvent;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.json.ConstantsSocket;
import com.trulymadly.android.app.json.ConstantsSocket.SOCKET_START_TYPE;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.TimeUtils;
import com.trulymadly.android.app.utility.TmLogger;
import com.trulymadly.android.app.utility.Utility;
import com.trulymadly.android.socket.SocketHandler;

public class TrulyMadlyService extends IntentService {

    public static final long SOCKET_IDLE_TIMEOUT = 10 * 60 * 1000;
//    public static final long SOCKET_IDLE_TIMEOUT = 10 * 60 * 1000;
private static final String tag = "SocketService";
    //    private static TrulyMadlyService trulyMadlyServiceinstance = null;
    private static boolean isServiceRunning = false;
//    private Handler handler = null;
//    private Runnable runnable = null;

    public TrulyMadlyService() {
        super(tag);
    }

    public static boolean isInstanceCreated() {
        return isServiceRunning;
    }

    @Override
    public void onCreate() {
        try {
            super.onCreate();
            isServiceRunning = true;
            BusProvider.getInstance().register(this);
        } catch (NullPointerException ignored) {

        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startSocket();
        return START_STICKY;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
    }

    @Override
    public void onDestroy() {
//        if (handler != null && runnable != null) {
//            handler.removeCallbacks(runnable);
//            runnable = null;
//        }
        BusProvider.getInstance().unregister(this);
        SocketHandler.destroy();
        isServiceRunning = false;
        super.onDestroy();
    }

    synchronized private void startSocket() {
        startSocket(SOCKET_START_TYPE.NORMAL);
    }

    synchronized private void startSocket(SOCKET_START_TYPE startType) {
        TmLogger.log(Log.DEBUG, tag, "starting socket - ip fallback is setString to : " + (startType == SOCKET_START_TYPE.IP_FALLBACK_ON_ERROR));
        SocketHandler socketHandler = SocketHandler.get(getApplicationContext());
        socketHandler.setSocketStartType(startType);
        socketHandler.connect();
    }

    synchronized private void updateLastEmittedOrReceivedTimestamp() {
        SPHandler.setString(getApplicationContext(), ConstantsSP.SHARED_KEYS_LAST_SOCKET_UPDATE, TimeUtils.getFormattedTime());
        Utility.generateServiceAlarm(getApplicationContext(), SOCKET_IDLE_TIMEOUT);
//        if (handler == null) {
//            handler = new Handler(Looper.getMainLooper());
//        }
//        if (runnable != null) {
//            handler.removeCallbacks(runnable);
//            runnable = null;
//        }
//        runnable = new Runnable() {
//            @Override
//            public void run() {
//                if (TimeUtils.isTimeoutExpired(getApplicationContext(), Constants.SHARED_KEYS_LAST_SOCKET_UPDATE, SOCKET_IDLE_TIMEOUT)) {
//                    TrulyMadlyApplication.stopService(getApplicationContext(),
//                            SOCKET_END.service_idle_timeout);
//                }
//            }
//        };
//        handler.postDelayed(runnable, SOCKET_IDLE_TIMEOUT);
    }

    @Subscribe
    public void onServiceMessageReceived(
            ServiceEvent serviceEvent) {
        TmLogger.log(Log.DEBUG, tag, "Service Event : " + serviceEvent.getEventType().toString());
        switch (serviceEvent.getEventType()) {
            case CHECK_CONNECTING:
                ConstantsSocket.SOCKET_STATE socketState = ConstantsSocket.SOCKET_STATE.CONNECTING;
                if (!SocketHandler.isSocketEnabled(getApplicationContext())) {
                    socketState = ConstantsSocket.SOCKET_STATE.POLLING;
                } else if (SocketHandler.get(getApplicationContext()).isConnected()) {
                    socketState = ConstantsSocket.SOCKET_STATE.CONNECTED;
                }
                if (!Utility.isNetworkAvailable(getApplicationContext())) {
                    socketState = ConstantsSocket.SOCKET_STATE.FAILED;
                }
                Utility.fireBusEvent(getApplicationContext(), true, new TriggerConnectingEvent(socketState));
                break;
            case STOP_SERVICE:
                if (Utility.isSet(serviceEvent.getStopServiceMessage())) {
                    SocketHandler.get(getApplicationContext()).connectionEnd(serviceEvent.getStopServiceMessage(), ConstantsSocket.SOCKET_STATE.POLLING, false);
                }
                stopSelf();
                break;
            case UPDATE_LAST_EMIT_TSTAMP:
                updateLastEmittedOrReceivedTimestamp();
                break;
            case CONNECT_SOCKET:
                startSocket();
                break;
            case RESTART_SOCKET_ON_ERROR:
                startSocket(SOCKET_START_TYPE.IP_FALLBACK_ON_ERROR);
                break;
            case RESTART_ON_ACK_TIMED_OUT:
                startSocket(SOCKET_START_TYPE.ACK_TIMED_OUT);
                break;
            case ACTIVITY_RESUMED:
                SocketHandler.get(getApplicationContext()).onActivityResumed();
                break;
            case EMIT_DIRECT:
                SocketHandler.get(getApplicationContext()).emitNow(serviceEvent.getEvent_name(), serviceEvent.getEvent_data(), serviceEvent.getEvent_ack(), serviceEvent.isEvent_isFirstTime(), serviceEvent.getEmitAttemptCounter());
                break;
            case EMIT:
                SocketHandler.get(getApplicationContext()).emitSocketMessage(serviceEvent.getEvent_name(), serviceEvent.getEvent_data(), serviceEvent.getEvent_ack(), serviceEvent.isEvent_isFirstTime(), serviceEvent.getEmitAttemptCounter());
                break;
            case EMIT_ARRAY:
                SocketHandler.get(getApplicationContext()).emitSocketMessage(serviceEvent.getEvent_name(), serviceEvent.getEvent_data_array(), serviceEvent.getEvent_ack(), serviceEvent.isEvent_isFirstTime(), serviceEvent.getEmitAttemptCounter());
                break;
            case GET_META_DATA:
                SocketHandler.get(getApplicationContext()).getMetaData(serviceEvent.getMatch_ids(), serviceEvent.getOnMetaDataReceived());
                break;
            case GET_MISSED_MESSAGES:
                SocketHandler.get(getApplicationContext()).getMissedMessages(serviceEvent.getGetMissedMessagesTstamp(), serviceEvent.isGetMissedMessagesShowNotification(), false);
                break;
            default:
                break;
        }
    }
}
