/**
 * 
 */
package com.trulymadly.android.app.bus;

import org.json.JSONObject;

/**
 * @author udbhav
 *
 */
public class ChatMessageReceivedEvent {

	private JSONObject data;
	private String matchId;

	public ChatMessageReceivedEvent(JSONObject data, String matchId) {
		setData(data);
		setMatchId(matchId);
	}

	public JSONObject getData() {
		return data;
	}

	private void setData(JSONObject data) {
		this.data = data;
	}

	public String getMatchId() {
		return matchId;
	}

	private void setMatchId(String matchId) {
		this.matchId = matchId;
	}

}
