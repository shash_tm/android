package com.trulymadly.android.app.bus;

import org.json.JSONObject;

public class QuizLaunchNudgeListEvent {

	private JSONObject nudgeResponseData;
	private int quiz_id;
	private String quiz_name;

	public QuizLaunchNudgeListEvent(int quiz_id, String quiz_name, JSONObject responseJSON) {
		setNudgeResponseData(responseJSON);
		setQuiz_id(quiz_id);
		setQuiz_name(quiz_name);
	}
	
	public JSONObject getNudgeResponseData() {
		return nudgeResponseData;
	}

	private void setNudgeResponseData(JSONObject nudgeResponseData) {
		this.nudgeResponseData = nudgeResponseData;
	}

	public int getQuiz_id() {
		return quiz_id;
	}

	private void setQuiz_id(int quiz_id) {
		this.quiz_id = quiz_id;
	}

	public String getQuiz_name() {
		return quiz_name;
	}

	private void setQuiz_name(String quiz_name) {
		this.quiz_name = quiz_name;
	}

}
