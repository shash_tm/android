package com.trulymadly.android.app.bus;

import com.trulymadly.android.app.modal.NudgeClass;

import java.util.ArrayList;

public class NudgeMatchesClickEvent {

	private ArrayList<NudgeClass> nudgeList;
	private int quizId, nudgeCount;
	private String quizName;

	public NudgeMatchesClickEvent(ArrayList<NudgeClass> nudgeList, int quizId, String quizName, int nudgeCount) {
		setNudgeList(nudgeList);
		setQuizId(quizId);
		setQuizName(quizName);
		setNudgeCount(nudgeCount);
	}

	public ArrayList<NudgeClass> getNudgeList() {
		return nudgeList;
	}

	private void setNudgeList(ArrayList<NudgeClass> nudgeList) {
		this.nudgeList = nudgeList;
	}

	public String getQuizName() {
		return quizName;
	}

	private void setQuizName(String quizName) {
		this.quizName = quizName;
	}

	public int getQuizId() {
		return quizId;
	}

	private void setQuizId(int quizId) {
		this.quizId = quizId;
	}

	public int getNudgeCount() {
		return nudgeCount;
	}

	private void setNudgeCount(int nudgeCount) {
		this.nudgeCount = nudgeCount;
	}

}
