package com.trulymadly.android.app.bus;

import com.trulymadly.android.app.utility.Utility;

import org.json.JSONObject;

public class SendNudgeToMatchEvent {

	private String matchId, quizName, playedStatus = "NONE";
	private int quizId;
	private boolean showCommonAnswers, fromCommonAnswers;
	private JSONObject response;

	public SendNudgeToMatchEvent(String matchId, int quizId, String quizName,
			String playedStatus) {
		setMatchId(matchId);
		setQuizId(quizId);
		setQuizName(quizName);
		setPlayedStatus(playedStatus);
		setShowCommonAnswers(false);
	}

	public SendNudgeToMatchEvent(String matchId, int quizId, String quizName,
			String playedStatus, Boolean isFromCommonAnswer) {
		setMatchId(matchId);
		setQuizId(quizId);
		setQuizName(quizName);
		setPlayedStatus(playedStatus);
		setFromCommonAnswers(isFromCommonAnswer);

	}

	public String getMatchId() {
		return matchId;
	}

	private void setMatchId(String matchId) {
		this.matchId = matchId;
	}

	public String getQuizName() {
		return quizName;
	}

	private void setQuizName(String quizName) {
		this.quizName = quizName;
	}

	public int getQuizId() {
		return quizId;
	}

	private void setQuizId(int quizId) {
		this.quizId = quizId;
	}

	public boolean getShowCommonAnswers() {
		return showCommonAnswers;
	}

	private void setShowCommonAnswers(Boolean showCommonAnswers) {
		this.showCommonAnswers = showCommonAnswers;
	}

	public JSONObject getResponse() {
		return response;
	}

	public void setResponse(JSONObject response) {
		this.response = response;
	}

	public boolean getFromCommonAnswers() {
		return fromCommonAnswers;
	}

	private void setFromCommonAnswers(boolean fromCommonAnswers) {
		this.fromCommonAnswers = fromCommonAnswers;
	}

	public String getPlayedStatus() {
		if (!Utility.isSet(playedStatus)) {
			return "NONE";
		} else {
			return playedStatus;
		}
	}

	private void setPlayedStatus(String playedStatus) {
		this.playedStatus = playedStatus;
	}

}
