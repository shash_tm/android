package com.trulymadly.android.app.bus;

/**
 * Created by avin on 22/03/16.
 */
public class CancelEvent {
    private final String mIdentifier;
    private final String mMatchId;

    public CancelEvent(String mIdentifier, String mMatchId) {
        this.mIdentifier = mIdentifier;
        this.mMatchId = mMatchId;
    }

    public String getmIdentifier() {
        return mIdentifier;
    }

    public String getmMatchId() {
        return mMatchId;
    }
}
