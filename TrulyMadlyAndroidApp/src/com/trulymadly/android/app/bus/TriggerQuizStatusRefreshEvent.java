package com.trulymadly.android.app.bus;

public class TriggerQuizStatusRefreshEvent {

	private String matchId;

	public TriggerQuizStatusRefreshEvent(String matchId) {
		setMatchId(matchId);

	}

	public String getMatchId() {
		return matchId;
	}

	private void setMatchId(String matchId) {
		this.matchId = matchId;
	}

}
