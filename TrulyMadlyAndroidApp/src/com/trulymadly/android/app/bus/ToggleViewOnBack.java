package com.trulymadly.android.app.bus;

public class ToggleViewOnBack {
    private boolean toShow, toTrack;

    public ToggleViewOnBack(boolean toShow, boolean toTrack) {
        this.setToShow(toShow);
        this.setToTrack(toTrack);
    }

    public boolean isToShow() {
        return toShow;
    }

    private void setToShow(boolean toShow) {
        this.toShow = toShow;
    }

    public boolean isToTrack() {
        return toTrack;
    }

    private void setToTrack(boolean toTrack) {
        this.toTrack = toTrack;
    }
}
