package com.trulymadly.android.app.bus;

import com.trulymadly.android.app.modal.FavoriteDataModal;

/**
 * Created by udbhav on 27/07/16.
 */
public class AddFavoriteOnServerEvent {
    private final FavoriteDataModal favoriteDataModal;
    private final boolean isAdded;

    public AddFavoriteOnServerEvent(FavoriteDataModal favoriteDataModal, boolean isAdded) {
        this.favoriteDataModal = favoriteDataModal;
        this.isAdded = isAdded;
    }

    public FavoriteDataModal getFavoriteDataModal() {
        return favoriteDataModal;
    }

    public boolean isAdded() {
        return isAdded;
    }
}
