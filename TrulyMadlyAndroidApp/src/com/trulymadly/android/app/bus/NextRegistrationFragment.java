package com.trulymadly.android.app.bus;

import com.trulymadly.android.app.modal.UserData;

public class NextRegistrationFragment {
	
	private Boolean isValid;
	private UserData userData;
    private String nextButtonText = null;

	public Boolean getIsValid() {
		return isValid;
	}

	public void setIsValid(Boolean isValid) {
		this.isValid = isValid;
	}

	public UserData getUserData() {
		return userData;
	}

	public void setUserData(UserData userData) {
		this.userData = userData;
	}


    public String getNextButtonText() {
        return nextButtonText;
    }

    public void setNextButtonText(String nextButtonText) {
        this.nextButtonText = nextButtonText;
    }
}
