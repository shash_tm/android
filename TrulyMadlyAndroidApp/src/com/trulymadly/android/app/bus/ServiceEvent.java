/**
 * 
 */
package com.trulymadly.android.app.bus;

import com.trulymadly.android.app.listener.GetMetaDataCallbackInterface;

import org.json.JSONArray;
import org.json.JSONObject;

import io.socket.client.Ack;

/**
 * @author udbhav
 *
 */
public class ServiceEvent {

	private int emitAttemptCounter;
	private SERVICE_EVENT_TYPE eventType;
	private String stopServiceMessage = null;
	private String event_name;
	private JSONObject event_data;
	private Ack event_ack;
	private boolean event_isFirstTime;
	private JSONArray match_ids;
	private GetMetaDataCallbackInterface onMetaDataReceived;
	private String getMissedMessagesTstamp;
	private boolean getMissedMessagesShowNotification;
	private JSONArray event_data_array;
	/* for connect, activity_resumed, updated_last_emit_tstamp, & stop */
	public ServiceEvent(SERVICE_EVENT_TYPE eventType) {
		setEventType(eventType);
	}

	/* for stop service */
	public ServiceEvent(SERVICE_EVENT_TYPE eventType, String stopServiceMessage) {
		setEventType(eventType);
		setStopServiceMessage(stopServiceMessage);
	}
	
	/* For chat sent and chat read and chat typing*/
	public ServiceEvent(SERVICE_EVENT_TYPE eventType, String event_name,
			JSONObject data, Ack ack, boolean isFirstTime, int emitAttemptCounter) {
		setEventType(eventType);
		setEvent_name(event_name);
		setEvent_data(data);
		setEvent_ack(ack);
		setEvent_isFirstTime(isFirstTime);
		setEmitAttemptCounter(emitAttemptCounter);
	}

	/* for sending quiz nudges in batch */
	public ServiceEvent(SERVICE_EVENT_TYPE eventType, String event_name,
			JSONArray dataArray, Ack ack, boolean isFirstTime) {
		setEventType(eventType);
		setEvent_name(event_name);
		setEvent_data_array(dataArray);
		setEvent_ack(ack);
		setEvent_isFirstTime(isFirstTime);
	}

	/* for getBool meta data call */
	public ServiceEvent(SERVICE_EVENT_TYPE eventType, JSONArray match_ids,
			GetMetaDataCallbackInterface onMetaDataReceived) {
		setEventType(eventType);
		setMatch_ids(match_ids);
		setOnMetaDataReceived(onMetaDataReceived);
	}

	/* for getBool missed messages fallback */
	public ServiceEvent(SERVICE_EVENT_TYPE eventType, String tstamp,
			boolean showNotification) {
		setEventType(eventType);
		setGetMissedMessagesTstamp(tstamp);
		setGetMissedMessagesShowNotification(showNotification);
	}

	public SERVICE_EVENT_TYPE getEventType() {
		return eventType;
	}

	private void setEventType(SERVICE_EVENT_TYPE eventType) {
		this.eventType = eventType;
	}

	public String getEvent_name() {
		return event_name;
	}

	private void setEvent_name(String event_name) {
		this.event_name = event_name;
	}

	public JSONObject getEvent_data() {
		return event_data;
	}

	private void setEvent_data(JSONObject event_data) {
		this.event_data = event_data;
	}

	public Ack getEvent_ack() {
		return event_ack;
	}

	private void setEvent_ack(Ack event_ack) {
		this.event_ack = event_ack;
	}

	public boolean isEvent_isFirstTime() {
		return event_isFirstTime;
	}

	private void setEvent_isFirstTime(boolean event_isFirstTime) {
		this.event_isFirstTime = event_isFirstTime;
	}

	public String getStopServiceMessage() {
		return stopServiceMessage;
	}

	private void setStopServiceMessage(String stopServiceMessage) {
		this.stopServiceMessage = stopServiceMessage;
	}

	public JSONArray getMatch_ids() {
		return match_ids;
	}

	private void setMatch_ids(JSONArray match_ids) {
		this.match_ids = match_ids;
	}

	public GetMetaDataCallbackInterface getOnMetaDataReceived() {
		return onMetaDataReceived;
	}

	private void setOnMetaDataReceived(
			GetMetaDataCallbackInterface onMetaDataReceived) {
		this.onMetaDataReceived = onMetaDataReceived;
	}

	public String getGetMissedMessagesTstamp() {
		return getMissedMessagesTstamp;
	}

	private void setGetMissedMessagesTstamp(String getMissedMessagesTstamp) {
		this.getMissedMessagesTstamp = getMissedMessagesTstamp;
	}

	public Boolean isGetMissedMessagesShowNotification() {
		return getMissedMessagesShowNotification;
	}

	private void setGetMissedMessagesShowNotification(
			Boolean getMissedMessagesShowNotification) {
		this.getMissedMessagesShowNotification = getMissedMessagesShowNotification;
	}

	public JSONArray getEvent_data_array() {
		return event_data_array;
	}

	private void setEvent_data_array(JSONArray event_data_array) {
		this.event_data_array = event_data_array;
	}

	public int getEmitAttemptCounter() {
		return emitAttemptCounter;
	}

	private void setEmitAttemptCounter(int emitAttemptCounter) {
		this.emitAttemptCounter = emitAttemptCounter;
	}

	public enum SERVICE_EVENT_TYPE {
		STOP_SERVICE, UPDATE_LAST_EMIT_TSTAMP, CONNECT_SOCKET, ACTIVITY_RESUMED, EMIT, EMIT_ARRAY, GET_META_DATA, GET_MISSED_MESSAGES, EMIT_DIRECT, RESTART_SOCKET_ON_ERROR, RESTART_ON_ACK_TIMED_OUT, CHECK_CONNECTING
	}

}
