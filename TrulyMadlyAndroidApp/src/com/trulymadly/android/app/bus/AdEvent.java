package com.trulymadly.android.app.bus;

/**
 * Created by avin on 19/10/16.
 */

public class AdEvent {
    public static final int AD_LOAD_SUCCESS = 1;
    public static final int AD_LOAD_FAILURE = 2;
    private int mAction;

    public AdEvent(int mAction) {
        this.mAction = mAction;
    }

    public int getmAction() {
        return mAction;
    }

    public void setmAction(int mAction) {
        this.mAction = mAction;
    }
}
