package com.trulymadly.android.app.bus;

/**
 * Created by avin on 24/08/16.
 */
public class NetworkChangeEvent {

    public static int CONNECTED = 1;
    public static int DISCONNECTED = 2;

    private final int mState;

    public NetworkChangeEvent(int mState) {
        this.mState = mState;
    }

    public int getmState() {
        return mState;
    }

    public boolean isConnected(){
        return mState == CONNECTED;
    }
}
