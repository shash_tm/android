package com.trulymadly.android.app.bus;

/**
 * Created by avin on 15/03/16.
 */
public class ImageUploadedProgress {
    private String mMesageId;
    private int mProgress;

    public ImageUploadedProgress(String mMesageId, int mProgress) {
        this.mMesageId = mMesageId;
        this.mProgress = mProgress;
    }

    public String getmMesageId() {
        return mMesageId;
    }

    public void setmMesageId(String mMesageId) {
        this.mMesageId = mMesageId;
    }

    public int getmProgress() {
        return mProgress;
    }

    public void setmProgress(int mProgress) {
        this.mProgress = mProgress;
    }
}
