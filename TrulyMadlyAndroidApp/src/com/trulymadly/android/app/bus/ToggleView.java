package com.trulymadly.android.app.bus;

public class ToggleView {
	private boolean toShow;

	public ToggleView(boolean toShow) {
		this.setToShow(toShow);
	}

	public boolean isToShow() {
		return toShow;
	}

	private void setToShow(boolean toShow) {
		this.toShow = toShow;
	}

}
