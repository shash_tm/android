/**
 * 
 */
package com.trulymadly.android.app.bus;

import com.trulymadly.android.app.modal.StickerData;

/**
 * @author udbhav
 *
 */
public class StickerClickEvent {

	private StickerData sticker;

	public StickerClickEvent(StickerData s) {
		setSticker(s);
	}

	public StickerData getSticker() {
		return sticker;
	}

	private void setSticker(StickerData sticker) {
		this.sticker = sticker;
	}

}
