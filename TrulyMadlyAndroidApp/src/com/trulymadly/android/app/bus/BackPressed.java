package com.trulymadly.android.app.bus;

import org.json.JSONObject;

public class BackPressed {

	private boolean isFromTakeQuizFragement;
	private String  quizName, matchId;
	private int quizId;
	private JSONObject response;
	private boolean showCommonAnswers, sendNudge;
	
	
	
	 
	
 
	public BackPressed() {
		setFromTakeQuizFragement(false);

	}

	public BackPressed(Boolean flag, int quizId, String quizName ) {
		setQuizName(quizName);
		setFromTakeQuizFragement(flag);
		setQuizId(quizId);

	}
	
	

	public boolean getFromTakeQuizFragement() {
		return isFromTakeQuizFragement;
	}

	public void setFromTakeQuizFragement(boolean isFromTakeQuizFragement) {
		this.isFromTakeQuizFragement = isFromTakeQuizFragement;
	}

	public String getQuizName() {
		return quizName;
	}

	public void setQuizName(String quizName) {
		this.quizName = quizName;
	}

	public int getQuizId() {
		return quizId;
	}

	public void setQuizId(int quizId) {
		this.quizId = quizId;
	}

	public String getMatchId() {
		return matchId;
	}

	public void setMatchId(String matchId) {
		this.matchId = matchId;
	}

	public JSONObject getResponse() {
		return response;
	}

	public void setResponse(JSONObject response) {
		this.response = response;
	}

	public boolean getShowCommonAnswers() {
		return showCommonAnswers;
	}

	public void setShowCommonAnswers(Boolean showCommonAnswers) {
		this.showCommonAnswers = showCommonAnswers;
	}

	public Boolean getSendNudge() {
		return sendNudge;
	}

	public void setSendNudge(Boolean sendNudge) {
		this.sendNudge = sendNudge;
	}

	



}
