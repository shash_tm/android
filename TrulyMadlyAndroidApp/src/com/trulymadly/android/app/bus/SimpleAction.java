package com.trulymadly.android.app.bus;

/**
 * Created by avin on 03/06/16.
 *
 * Use this event class to fire a blank event -> i.e. when you dont need anything to be passed
 * to the subscriber
 */
public class SimpleAction {

    public static final int ACTION_DELETE_SPARK = 1;
    public static final int ACTION_SPARK_ACCEPTED = 2;
    public static final int ACTION_OPEN_BUY_SPARKS = 3;
    public static final int ACTION_79_SDK_INIT = 4;
    private int mAction;
    private String mStringValue;
    private int mIntValue;

    public SimpleAction(int mAction) {
        this.mAction = mAction;
    }

    public int getmAction() {
        return mAction;
    }

    public void setmAction(int mAction) {
        this.mAction = mAction;
    }

    public String getmStringValue() {
        return mStringValue;
    }

    public void setmStringValue(String mStringValue) {
        this.mStringValue = mStringValue;
    }

    public int getmIntValue() {
        return mIntValue;
    }

    public void setmIntValue(int mIntValue) {
        this.mIntValue = mIntValue;
    }
}
