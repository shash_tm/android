package com.trulymadly.android.app.bus;

public class InstallReferrerEvent {

    private final String referrer;

    public InstallReferrerEvent(String referrer) {
        this.referrer = referrer;
    }

    public String getReferrer() {
        return referrer;
    }
}
