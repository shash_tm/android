/**
 * 
 */
package com.trulymadly.android.app.bus;

/**
 * @author udbhav
 *
 */
public class ChatMessageReadEvent {

	private String matchId, last_seen_msg_id, last_seen_msg_tstamp;

	public ChatMessageReadEvent(String matchId, String last_seen_msg_id,
			String last_seen_msg_tstamp) {
		setMatchId(matchId);
		setLast_seen_msg_tstamp(last_seen_msg_id);
		setLast_seen_msg_id(last_seen_msg_tstamp);
	}

	public String getMatchId() {
		return matchId;
	}

	private void setMatchId(String matchId) {
		this.matchId = matchId;
	}

	public String getLast_seen_msg_id() {
		return last_seen_msg_id;
	}

	private void setLast_seen_msg_id(String last_seen_msg_id) {
		this.last_seen_msg_id = last_seen_msg_id;
	}

	public String getLast_seen_msg_tstamp() {
		return last_seen_msg_tstamp;
	}

	private void setLast_seen_msg_tstamp(String last_seen_msg_tstamp) {
		this.last_seen_msg_tstamp = last_seen_msg_tstamp;
	}

}
