package com.trulymadly.android.app.bus;

import com.trulymadly.android.app.modal.UserData;

public class BackPressedRegistrationFlow {
	
	private UserData userData;
	private String hashtag;

	public UserData getUserData() {
		return userData;
	}

	public void setUserData(UserData userData) {
		this.userData = userData;
	}

	public String getHashtag() {
		return hashtag;
	}

	public void setHashtag(String hashtag) {
		this.hashtag = hashtag;
	}
	
	

}
