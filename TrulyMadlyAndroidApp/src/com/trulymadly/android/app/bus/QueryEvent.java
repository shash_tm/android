package com.trulymadly.android.app.bus;

/**
 * Created by avin on 17/03/16.
 */
public class QueryEvent {
    private String mIdentifier;
    private int mValue;

    public QueryEvent(String mIdentifier, int mValue) {
        this.mIdentifier = mIdentifier;
        this.mValue = mValue;
    }

    public String getmIdentifier() {
        return mIdentifier;
    }

    public void setmIdentifier(String mIdentifier) {
        this.mIdentifier = mIdentifier;
    }

    public int getmValue() {
        return mValue;
    }

    public void setmValue(int mValue) {
        this.mValue = mValue;
    }
}
