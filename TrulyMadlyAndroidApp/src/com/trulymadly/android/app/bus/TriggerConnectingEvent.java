/**
 *
 */
package com.trulymadly.android.app.bus;

import com.trulymadly.android.app.json.ConstantsSocket;

/**
 * @author udbhav
 */
public class TriggerConnectingEvent {
    private ConstantsSocket.SOCKET_STATE socketState;

    public TriggerConnectingEvent(ConstantsSocket.SOCKET_STATE socketState) {
        setSocketState(socketState);
    }

    public ConstantsSocket.SOCKET_STATE getSocketState() {
        return socketState;
    }

    private void setSocketState(ConstantsSocket.SOCKET_STATE socketState) {
        this.socketState = socketState;
    }

}
