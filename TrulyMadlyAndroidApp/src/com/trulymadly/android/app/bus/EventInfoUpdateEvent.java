package com.trulymadly.android.app.bus;

/**
 * Created by avin on 26/08/16.
 */
public class EventInfoUpdateEvent {

    public static final int KEY_VIDEOS_VIEWED = 1;
    public static final int KEY_VIDEOS_COUNT = 2;
    public static final int KEY_PHOTOS_COUNT = 3;
    public static final int KEY_FAVORITES_VIEWED = 4;
    public static final int KEY_TRUST_SCORE_VIEWED = 5;
    public static final int KEY_TRUST_SCORE = 6;

    private int mKey;
    private String mValue;

    public EventInfoUpdateEvent(int mKey, String mValue) {
        this.mKey = mKey;
        this.mValue = mValue;
    }

    public String getmValue() {
        return mValue;
    }

    public void setmValue(String mValue) {
        this.mValue = mValue;
    }

    public int getmKey() {
        return mKey;
    }

    public void setmKey(int mKey) {
        this.mKey = mKey;
    }

    public static String getKeyString(int key){
        switch (key){
            case KEY_VIDEOS_VIEWED:
                return "video_viewed";
            case KEY_VIDEOS_COUNT:
                return "video_count";
            case KEY_PHOTOS_COUNT:
                return "photo_count";
            case KEY_FAVORITES_VIEWED:
                return "favorites_viewed";
            case KEY_TRUST_SCORE_VIEWED:
                return "trust_score_viewed";
            case KEY_TRUST_SCORE:
                return "trust_score";

        }

        return null;
    }
}
