package com.trulymadly.android.app.bus;

/**
 * Created by udbhav on 24/06/16.
 */
public class ExpandFragmentEvent {

    private final boolean doExpand;

    public ExpandFragmentEvent(boolean doExpand) {
        this.doExpand = doExpand;
    }

    public boolean isDoExpand() {
        return doExpand;
    }
}
