/**
 * 
 */
package com.trulymadly.android.app.bus;


/**
 * @author udbhav
 *
 */
public class SendQuizMessageEvent {

	private String match_id, message;
	private int quiz_id;

	public SendQuizMessageEvent(String match_id, String message, int quiz_id) {
		setMatch_id(match_id);
		setMessage(message);
		setQuiz_id(quiz_id);
	}

	public String getMatch_id() {
		return match_id;
	}

	private void setMatch_id(String match_id) {
		this.match_id = match_id;
	}

	public String getMessage() {
		return message;
	}

	private void setMessage(String message) {
		this.message = message;
	}

	public int getQuiz_id() {
		return quiz_id;
	}

	private void setQuiz_id(int quiz_id) {
		this.quiz_id = quiz_id;
	}
	
	


}
