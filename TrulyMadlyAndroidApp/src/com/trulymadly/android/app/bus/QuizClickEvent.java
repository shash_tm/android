package com.trulymadly.android.app.bus;

import com.trulymadly.android.app.utility.Utility;

public class QuizClickEvent {

	private int quiz_id;
	private String quiz_name;
	private boolean fromChat = false;
	private String playedStatus = "NONE";
	private String trkEventType = "";

	public QuizClickEvent(int quiz_id, String quiz_name, boolean fromChat,
			String playedStatus, String eventType) {
		setQuiz_id(quiz_id);
		setQuiz_name(quiz_name);
		setFromChat(fromChat);
		setPlayedStatus(playedStatus);
		setTrkEventType(eventType);
	}

	public int getQuiz_id() {
		return quiz_id;
	}

	private void setQuiz_id(int quiz_id) {
		this.quiz_id = quiz_id;
	}

	public String getQuiz_name() {
		return quiz_name;
	}

	private void setQuiz_name(String quiz_name) {
		this.quiz_name = quiz_name;
	}

	public boolean isFromChat() {
		return fromChat;
	}

	private void setFromChat(boolean fromChat) {
		this.fromChat = fromChat;
	}

	public String getPlayedStatus() {
		if (!Utility.isSet(playedStatus)) {
			return "NONE";
		} else {
			return playedStatus;
		}
	}

	private void setPlayedStatus(String playedStatus) {
		this.playedStatus = playedStatus;
	}

	public String getTrkEventType() {
		return trkEventType;
	}

	private void setTrkEventType(String trkEventType) {
		this.trkEventType = trkEventType;
	}

}
