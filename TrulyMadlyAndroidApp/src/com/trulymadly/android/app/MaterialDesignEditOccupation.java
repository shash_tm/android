package com.trulymadly.android.app;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.trulymadly.android.app.bus.BackPressedRegistrationFlow;
import com.trulymadly.android.app.bus.NextRegistrationFragment;
import com.trulymadly.android.app.listener.GetStartTimeInterface;
import com.trulymadly.android.app.modal.UserData;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.BadWordsHandler;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;

import java.util.ArrayList;

public class MaterialDesignEditOccupation extends Fragment {

	private NextRegistrationFragment nextFragment;
	private GetStartTimeInterface returnStartTimeOfEditOccupationFragment;
	private Activity aActivity;
	private EditText occupationEditText;
	private String occupation = null;
	private UserData userData;
	private ArrayList<String> badWordList;
	private String fbDesignation;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			returnStartTimeOfEditOccupationFragment = (GetStartTimeInterface) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement ReturnStartTimeOfEditHeightFragment ");
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		aActivity = getActivity();
		Bundle data = getArguments();
		userData = new UserData();
		if (data.getSerializable("userData") != null) {
			userData = (UserData) data.getSerializable("userData");
			occupation = userData.getCurrentTitle();
			fbDesignation = userData.getFbDesignation();
		}
		if (!Utility.isSet(occupation) && Utility.isSet(fbDesignation)) {
			occupation = fbDesignation;
		}
//		OnBadWordListRefreshedInterface onBadWordListUpdate = new OnBadWordListRefreshedInterface() {
//			@Override
//			public void onRefresh(ArrayList<String> badWordArrayList) {
//				badWordList = badWordArrayList;
//			}
//		};
		badWordList = BadWordsHandler.getBadWordList(aActivity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle paramBundle) {
		View editOccupationView = inflater.inflate(
				R.layout.material_design_edit_occupation, container, false);

		nextFragment = new NextRegistrationFragment();

		occupationEditText = (EditText) editOccupationView
				.findViewById(R.id.material_design_occupation_edit_box);
		setOccupation();
		occupationEditText.addTextChangedListener(new TextWatcher() {
			private String lastString = "";
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if(s.toString().trim().equals(lastString)) {
					return;
				}
				lastString = s.toString().trim();
				if(BadWordsHandler.hasBadWord(aActivity, s.toString(), badWordList, R.string.bad_word_text_occupation, occupationEditText)) {
					
				}
				else if (s.toString().trim().length() > 30) {
					UiUtils.hideKeyBoard(aActivity);
					AlertsHandler.showMessage(aActivity,
                            R.string.max_char_exceeded, true);
					occupationEditText.setText(s.subSequence(0, 30));
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				validateOccupationData();
			}
		});

		occupationEditText
				.setOnEditorActionListener(new OnEditorActionListener() {

					@Override
					public boolean onEditorAction(TextView v, int actionId,
							KeyEvent event) {
						if (actionId == EditorInfo.IME_ACTION_GO) {
							UiUtils.hideKeyBoard(aActivity);
							validateOccupationData();
						}
						return false;
					}
				});

		editOccupationView.setOnKeyListener(new OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_BACK) {
					BackPressedRegistrationFlow backPressed = new BackPressedRegistrationFlow();
					userData.setCurrentTitle(occupationEditText.getText()
							.toString().trim());
					backPressed.setUserData(userData);
					Utility.fireBusEvent(aActivity, true, backPressed);
					return true;
				} else {
					return false;
				}
			}
		});

		editOccupationView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				UiUtils.hideKeyBoard(aActivity);
				validateOccupationData();

			}
		});
		returnStartTimeOfEditOccupationFragment
				.setStartTime((new java.util.Date()).getTime());

		return editOccupationView;
	}

	private void setOccupation() {
		if (Utility.isSet(occupation)) {
			occupationEditText.setText(occupation);
			validateOccupationData();
		}
	}

	private void validateOccupationData() {

		occupation = occupationEditText.getText().toString().trim();
		nextFragment.setIsValid(Utility.isSet(occupation));
		userData.setCurrentTitle(occupation);
		nextFragment.setUserData(userData);
		Utility.fireBusEvent(aActivity, true, nextFragment);

	}
}
