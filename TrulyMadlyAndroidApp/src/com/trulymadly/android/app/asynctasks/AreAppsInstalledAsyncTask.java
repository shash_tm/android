package com.trulymadly.android.app.asynctasks;

import android.content.Context;
import android.os.AsyncTask;

import com.trulymadly.android.app.utility.ActivityHandler;

/**
 * Created by avin on 21/04/16.
 */
public class AreAppsInstalledAsyncTask extends AsyncTask<String, Void, boolean[]> {

    private final Context mContext;
    private final AreAppsInstalledInterface mAreAppsInstalledInterface;

    public AreAppsInstalledAsyncTask(Context mContext, AreAppsInstalledInterface mAreAppsInstalledInterface) {
        this.mContext = mContext;
        this.mAreAppsInstalledInterface = mAreAppsInstalledInterface;
    }

    @Override
    protected boolean[] doInBackground(String... params) {
        if (params == null || params.length == 0) {
            return null;
        }

        boolean[] installed = new boolean[params.length];
        if (!isCancelled()) {
            for (int i = 0; i < installed.length; i++) {
                String packageName = params[i];
                installed[i] = ActivityHandler.appInstalledOrNot(mContext, packageName);
            }
        }
        return installed;
    }

    @Override
    protected void onPostExecute(boolean[] booleans) {
        if (booleans == null) {
            return;
        }

        if (mAreAppsInstalledInterface != null) {
            mAreAppsInstalledInterface.onAppsChecked(booleans);
        }
    }

    public interface AreAppsInstalledInterface {
        void onAppsChecked(boolean[] installed);
    }

}
