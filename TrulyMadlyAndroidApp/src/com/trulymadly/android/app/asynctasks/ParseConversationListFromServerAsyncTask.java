package com.trulymadly.android.app.asynctasks;

import android.content.Context;
import android.os.AsyncTask;

import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.ConstantsRF;
import com.trulymadly.android.app.listener.OnConversationListFetchedInterface;
import com.trulymadly.android.app.modal.ConversationModal;
import com.trulymadly.android.app.sqlite.MessageDBHandler;
import com.trulymadly.android.app.sqlite.RFHandler;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class ParseConversationListFromServerAsyncTask extends AsyncTask<JSONObject, Void, Integer> {

    private final OnConversationListFetchedInterface onConversationListFetchedInterface;
    private final Context aContext;

    public ParseConversationListFromServerAsyncTask(Context mContext, OnConversationListFetchedInterface onConversationListFetchedInterface) {
        this.aContext = mContext;
        this.onConversationListFetchedInterface = onConversationListFetchedInterface;
    }


    @Override
    protected Integer doInBackground(JSONObject... responseParams) {
        int unreadConversationCount = 0;
        JSONObject messageListData = responseParams[0];
        ArrayList<ConversationModal> conversationList = createConversationListsFromJSONArray(aContext,
                messageListData.optJSONArray("data"));
        if (conversationList != null && conversationList.size() > 0) {
            if (!RFHandler.getBool(aContext,
                    ConstantsRF.RIGID_FIELD_CONVERSATION_VIEW_FIRST_TIME)) {
                RFHandler.insert(aContext, ConstantsRF.RIGID_FIELD_CONVERSATION_VIEW_FIRST_TIME,
                        true);
            }
            MessageDBHandler.insertAllConversations(Utility.getMyId(aContext),
                    conversationList, aContext);

            int lastConvCount= RFHandler.getInt(aContext,Utility.getMyId(aContext),ConstantsRF.RIGID_FIELD_LAST_CONV_COUNT,0);
            if(lastConvCount==0)
                RFHandler.insert(aContext,Utility.getMyId(aContext),ConstantsRF.RIGID_FIELD_LAST_CONV_COUNT,MessageDBHandler.getRealMutualMatchesCount(Utility.getMyId(aContext),aContext));

        }
        unreadConversationCount = MessageDBHandler.getUnreadConversationCount(Utility.getMyId(aContext),
                aContext);
        return unreadConversationCount;
    }

    @Override
    protected void onPostExecute(Integer unreadConversationsCount) {
        onConversationListFetchedInterface.onSuccess(unreadConversationsCount);
    }

    private ArrayList<ConversationModal> createConversationListsFromJSONArray(Context aContext, JSONArray dataArray) {
        ArrayList<ConversationModal> conversationList = new ArrayList<>();

        if (dataArray == null || dataArray.length() == 0) {
            return conversationList;
        }

        boolean isExist = false;
        ConversationModal conversation;

        for (int index = 0; index < dataArray.length(); index++) {
            isExist = false;
            conversation = new ConversationModal(dataArray.optJSONObject(index), Utility.getMyId(aContext), aContext);
            for (ConversationModal existingConversation : conversationList) {
                if (Utility.stringCompare(existingConversation.getMatch_id(), conversation.getMatch_id())) {
                    isExist = true;
                    existingConversation.setTstamp(conversation.getTstamp());
                    if (existingConversation.getLastMessageState() != Constants.MessageState.BLOCKED_SHOWN
                            && existingConversation.getLastMessageState() != Constants.MessageState.BLOCKED_UNSHOWN) {
                        existingConversation.setLastMessageState(conversation.getLastMessageState());
                    }
                    if (Utility.isSet(conversation.getMessage_link()))
                        existingConversation.setMessage_link(conversation.getMessage_link());
                    if (Utility.isSet(conversation.getLast_message()))
                        existingConversation.setLast_message(conversation.getLast_message());

                    existingConversation.setSparkReceived(conversation.isSparkReceived());
                    existingConversation.setMutualSpark(conversation.isMutualSpark());

                    break;
                }
            }
            if (!isExist) {
//                String clear_chat_tsatmp = MessageDBHandler.getClearChatTstamp(aContext, conversation.getMatch_id());
//                if (Utility.isSet(clear_chat_tsatmp) && Utility.isSet(conversation.getTstamp().toString())) {
//                    if (TimeUtils.getParsedTime(clear_chat_tsatmp).compareTo(conversation.getTstamp()) > 0) {
//                        conversation.setIsChatCleared(true);
//                    } else {
//                        conversation.setIsChatCleared(false);
//                    }
//                }
                conversationList.add(conversation);
                if (conversation.isMissTm()) {
                    RFHandler.insert(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_IS_MISS_TM_CREATED, true);
                }
            }
        }
        return conversationList;
    }

}
