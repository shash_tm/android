package com.trulymadly.android.app.asynctasks;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.facebook.appevents.AppEventsLogger;
import com.trulymadly.android.app.utility.Utility;

import java.util.concurrent.RejectedExecutionException;

public class TrackEventToFbAsyncTask extends AsyncTask<Void, Void, Void> {

    private final Context mContext;
    private final Bundle params;
    private final String event_type;
    private final float time_taken_sec;


    private TrackEventToFbAsyncTask(@NonNull Context context, @NonNull String event_type, String category, String label, float time_taken_sec) {
        this.mContext = context;
        this.event_type = event_type;
        if (Utility.isSet(category)) {
            this.params = new Bundle();
            params.putString("CATEGORY", category);
            params.putString("ACTION", event_type);
            if (Utility.isSet(label)) {
                params.putString("LABEL", label);
            }
        } else {
            params = null;
        }
        this.time_taken_sec = time_taken_sec;
    }

    private TrackEventToFbAsyncTask(@NonNull Context context, @NonNull String event_type, Bundle params) {
        this.mContext = context;
        this.event_type = event_type;
        this.params = params;
        this.time_taken_sec = 0;
    }

    /* Static initializers */

    public static void trackEvent(Context aContext, String event_type) {
        trackEvent(aContext, event_type, null, null, 0);
    }

    public static void trackEvent(Context context, String event_type, String category, String label, float time_taken_sec) {
        new TrackEventToFbAsyncTask(context, event_type, category, label, time_taken_sec).execute();
    }

    public static void trackEvent(Context aContext, String event_type, Bundle params) {
        new TrackEventToFbAsyncTask(aContext, event_type, params).execute();
    }

    @Override
    protected Void doInBackground(Void... aVoids) {
        try {
            AppEventsLogger logger = AppEventsLogger
                    .newLogger(mContext);
            if (time_taken_sec > 0) {
                logger.logEvent(event_type, time_taken_sec,
                        params);
            } else {
                logger.logEvent(event_type, params);
            }
        } catch (RejectedExecutionException ignored) {
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {

    }
}
