/**
 *
 */
package com.trulymadly.android.app.asynctasks;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.crashlytics.android.Crashlytics;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.future.ResponseFuture;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.BuildConfig;
import com.trulymadly.android.app.GetHttpRequestParams;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.Constants.HttpRequestType;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.listener.CustomOkHttpResponseHandler;
import com.trulymadly.android.app.listener.GetFileCallback;
import com.trulymadly.android.app.listener.OnPermissionResultListener;
import com.trulymadly.android.app.sqlite.MatchesDbHandler;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.Compress;
import com.trulymadly.android.app.utility.CountingFileRequestBody;
import com.trulymadly.android.app.utility.FilesHandler;
import com.trulymadly.android.app.utility.OkHttpHandler;
import com.trulymadly.android.app.utility.PermissionsHelper;
import com.trulymadly.android.app.utility.TimeUtils;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import okhttp3.Call;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

import static android.graphics.Bitmap.Config;

/**
 * @author udbhav
 */
public class PhotoUploader implements View.OnClickListener, OnPermissionResultListener {

    private final PhotoUploaderRequestInterface callback;
    private final Activity aActivity;
    private final Button saveCrop;
    private final View main_layout;
    private final View crop_layout;
    private final CropImageView cropImageView;
    private final View image_loading_progressbar;
    private final ImageView cropperImageView;
    public boolean isUserPhotos;
    private int loadCount = 0;
    private ResponseFuture<String> photoUploadRequest = null;
    private String mCurrentPhotoPath = null;
    private HttpRequestType httpRequestType;
    private String photoId;
    private Uri imageUri;
    private UPLOAD_TYPE uploadType = UPLOAD_TYPE.PICTURE;
    private Call mUploadCall;
    private boolean isInProgress = false;

    public PhotoUploader(Activity act, PhotoUploaderRequestInterface cb, View main_layout, View crop_layout) {
        this.aActivity = act;
        this.callback = cb;
        this.main_layout = main_layout;
        this.crop_layout = crop_layout;
        this.saveCrop = (Button) crop_layout.findViewById(R.id.saveCrop);
        this.cropImageView = (CropImageView) crop_layout.findViewById(R.id.cropImageView);
        this.image_loading_progressbar = crop_layout.findViewById(R.id.image_loading_progressbar);
        this.cropperImageView = (ImageView) crop_layout.findViewById(R.id.imageViewCropper);
        this.saveCrop.setOnClickListener(this);

    }

    private static int calculateInSampleSize(
            BitmapFactory.Options options) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        int requiredWidth = 640;
        int requiredHeight = 640;
        if (height > requiredHeight || width > requiredWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > requiredHeight
                    && (halfWidth / inSampleSize) > requiredWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public boolean isUserPhotos() {
        return isUserPhotos;
    }

    public void setUserPhotos(boolean userPhotos) {
        isUserPhotos = userPhotos;
    }

    public void photoRequestUploadResponse(Activity aActivity, boolean isSuccess, String response,
                                           PhotoUploaderRequestInterface callback) {
        callback.onComplete();
        if (isSuccess && Utility.isSet(response) && Utility.isValidJsonObject(response)) {
            try {
                JSONObject responseJsonObject = new JSONObject(response);


                switch (responseJsonObject.getInt("responseCode")) {
                    case 200:
                        if (isUserPhotos)
                            callback.onSuccess(responseJsonObject.toString());
                        else if (responseJsonObject.optJSONArray("data") != null) {
                            callback.onSuccess(responseJsonObject.optJSONArray("data").toString());
                        } else {
                            callback.onSuccess(null);
                        }

                        break;
                    case 403:
                        callback.onFail(responseJsonObject.optString("error",
                                aActivity.getResources().getString(R.string.ERROR_NETWORK_FAILURE)));
                        break;
                    default:
                        callback.onFail(aActivity.getResources().getString(R.string.ERROR_NETWORK_FAILURE));
                        break;
                }
            } catch (JSONException e) {
                callback.onFail(aActivity.getResources().getString(R.string.ERROR_NETWORK_FAILURE));
            }
        } else {
            callback.onFail(aActivity.getResources().getString(R.string.ERROR_NETWORK_FAILURE));
        }
    }

    public boolean isInProgress() {
//        return (photoUploadRequest != null && !photoUploadRequest.isCancelled() && !photoUploadRequest.isDone());
        return isInProgress;
    }

    public void takeFromCamera(UPLOAD_TYPE upload_type) {
        this.uploadType = upload_type;
        this.photoId = null;

        if (PermissionsHelper.checkAndAskForPermission(aActivity, new String[]{Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE},
                PermissionsHelper.REQUEST_CAMERA_STORAGE_PERMISSIONS_CODE)) {
            launchCamera();
        }

    }

    private void launchCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        File photoFile = null;
        ComponentName cameraActivity = takePictureIntent.resolveActivity(aActivity.getPackageManager());
        if (cameraActivity != null) {
            photoFile = createImageFile();
            // Continue only if the File was successfully created
            if (photoFile != null) {
                mCurrentPhotoPath = photoFile.getAbsolutePath();
//                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                Uri fileUri = FileProvider.getUriForFile(aActivity, BuildConfig.APPLICATION_ID + ".provider", photoFile);
                aActivity.grantUriPermission(cameraActivity.getPackageName(), fileUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                aActivity.startActivityForResult(takePictureIntent, Constants.PICK_FROM_CAMERA_OWN);
            }
        }
    }

    public void fetchFromGallery(UPLOAD_TYPE uploadType) {
        fetchFromGallery(uploadType, false);
    }

    public void fetchFromGallery(UPLOAD_TYPE uploadType, boolean onlyPhotos) {
        this.uploadType = uploadType;
        this.photoId = null;
        if (PermissionsHelper.checkAndAskForPermission(aActivity, Manifest.permission.READ_EXTERNAL_STORAGE,
                PermissionsHelper.REQUEST_STORAGE_PERMISSIONS_CODE)) {
            launchGallery(onlyPhotos);
        }
    }


    private void launchGallery(boolean onlyPhotos) {
        Intent photoPickerIntent = new Intent();
        photoPickerIntent.setAction(Intent.ACTION_GET_CONTENT);
        photoPickerIntent.setType("*/*");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            String[] mimetypes = null;
            if (!onlyPhotos) {
                mimetypes = new String[]{"image/*", "video/*"};
            } else {
                mimetypes = new String[]{"image/*"};
            }
            photoPickerIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
        }
        try {
            aActivity.startActivityForResult(photoPickerIntent, Constants.PICK_FROM_GALLERY);
        } catch (ActivityNotFoundException e) {
            AlertsHandler.showMessage(aActivity, R.string.unable_to_load_gallery);
        }

//        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
//        photoPickerIntent.setType("video/*, images/*");
//
//        String[] mimetypes = {"image/*", "video/*"};
//        photoPickerIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
//        try {
//            aActivity.startActivityForResult(photoPickerIntent, Constants.PICK_FROM_GALLERY);
//        } catch (ActivityNotFoundException e) {
//            fetchImageFromSystem();
//        }
    }

    public void startPhotoUpload(String filePath,
                                 HttpRequestType httpRequestType, View cur_pic_upload,
                                 String trkActivity, UPLOAD_TYPE type, String photoIdTypeKey) {
        startPhotoUpload(filePath, httpRequestType, cur_pic_upload, trkActivity, type, photoIdTypeKey, null);
    }

    public void startPhotoUpload(String filePath,
                                 HttpRequestType httpRequestType, View cur_pic_upload,
                                 String trkActivity, UPLOAD_TYPE type, String photoIdTypeKey,
                                 String identifier) {

        if (filePath == null) {
            photoRequestUploadResponse(aActivity, false, null, callback);
            return;
        }

        File myFile = new File(filePath);
        startPhotoUploadFile(myFile, httpRequestType, cur_pic_upload, trkActivity, type,
                photoIdTypeKey, identifier);

    }

    public void startPhotoUploadFile(File myFile, final HttpRequestType httpRequestType, final View uploadProgressbar,
                                     final String trkActivity, UPLOAD_TYPE type, String photoIdTypeKey,
                                     String identifier) {

        this.uploadType = type;
        if (uploadProgressbar != null) {
            uploadProgressbar.setVisibility(View.VISIBLE);
        }
        final long trkStartTime = (new java.util.Date()).getTime();
        isInProgress = true;

        final String TAG = "OKHTTP_MULTIPART_UPLOAD";
        try {
            Log.d(TAG, "File::" + myFile + " : " + myFile.exists());

            MultipartBody.Builder multipartBuilder = new MultipartBody.Builder().setType(MultipartBody.FORM);

            String url;
            switch (uploadType) {
                case PICTURE:
                    url = ConstantsUrls.get_photos_url();
                    if (Utility.isSet(photoId)) {
                        multipartBuilder.addFormDataPart("action", "edit_photo");
                        multipartBuilder.addFormDataPart("photo_id", photoId);
                    } else {
                        multipartBuilder.addFormDataPart("action", "add_from_computer_without_file_reader_api");
                    }
                    break;
                case PHOTO_ID:
                    url = ConstantsUrls.get_trust_builder_api_url();
                    if (photoIdTypeKey != null) {
                        multipartBuilder.addFormDataPart("proofType", photoIdTypeKey);
                    }
                    multipartBuilder.addFormDataPart("type", "1");
                    multipartBuilder.addFormDataPart("fileSubmit", "submit");
                    break;
                default:
                    return;
            }

            if (Utility.isSet(identifier)) {
                multipartBuilder.addFormDataPart("file", myFile.getName(),
                        new CountingFileRequestBody(aActivity, myFile, "image/jpg", identifier));
            } else {
                multipartBuilder.addFormDataPart("file", myFile.getName(),
                        RequestBody.create(MediaType.parse("image/jpg"), myFile));
            }


            RequestBody requestBody = multipartBuilder.build();

            Headers headers = Headers.of(GetHttpRequestParams.getHeaders(aActivity));
            Request request = new Request.Builder()
                    .url(url)
                    .headers(headers)
                    .post(requestBody)
                    .build();

            OkHttpHandler.NET_TIMEOUT timeoutType = OkHttpHandler.NET_TIMEOUT.NORMAL;

            OkHttpClient.Builder builder = OkHttpHandler.getOkHttpBuilder(timeoutType.value(), timeoutType.value(), timeoutType.value());
            OkHttpClient client = builder.build();

            mUploadCall = client.newCall(request);
            CustomOkHttpResponseHandler mUploadCustomOkHttpResponseHandler = new CustomOkHttpResponseHandler(aActivity) {
                @Override
                public void onRequestSuccess(JSONObject responseJSON) {
                    String responseString = null;
                    if (responseJSON != null) {
                        responseString = responseJSON.toString();
                    }
                    onUploadComplete(null, responseString, httpRequestType, uploadProgressbar,
                            trkStartTime, trkActivity);
                }

                @Override
                public void onRequestFailure(Exception exception) {
                    Log.e(TAG, "Request Failure Exception: " + exception.getLocalizedMessage());
                    onUploadComplete(exception, null, httpRequestType, uploadProgressbar, trkStartTime, trkActivity);
                }
            };
            mUploadCall.enqueue(mUploadCustomOkHttpResponseHandler);

        } catch (Exception e) {
            Log.e(TAG, "Other Exception: " + e.getLocalizedMessage());
            onUploadComplete(e, null, httpRequestType, uploadProgressbar, trkStartTime, trkActivity);
        }

    }

    private void onUploadComplete(Exception e, String response, HttpRequestType httpRequestType,
                                  View uploadProgressbar, long trkStartTime, String trkActivity) {
        String event = null;
        if (uploadProgressbar != null) {
            uploadProgressbar.setVisibility(View.GONE);
        }

        if (mUploadCall != null && !mUploadCall.isCanceled()) {
            MatchesDbHandler.clearMatchesCache(aActivity);
            if (e != null) {
                photoRequestUploadResponse(aActivity, false, null, callback);
            } else {
                long timeDiff = TimeUtils.getSystemTimeInMiliSeconds() - trkStartTime;
                if (httpRequestType == HttpRequestType.UPLOAD_PIC_GALLERY) {
                    event = TrulyMadlyEventTypes.add_from_computer_upload;
                } else if (httpRequestType == HttpRequestType.UPLOAD_PIC_CAMERA) {
                    event = TrulyMadlyEventTypes.take_from_camera_upload;
                }
                TrulyMadlyTrackEvent.trackEvent(aActivity, trkActivity, event, timeDiff, null, null, true);
                photoRequestUploadResponse(aActivity, true, response, callback);
            }
        } else {
            if (httpRequestType == HttpRequestType.UPLOAD_PIC_GALLERY) {
                event = TrulyMadlyEventTypes.add_from_computer_cancel;
            } else if (httpRequestType == HttpRequestType.UPLOAD_PIC_CAMERA) {
                event = TrulyMadlyEventTypes.take_from_camera_cancel;
            }
            TrulyMadlyTrackEvent.trackEvent(aActivity, trkActivity, event, 0, null, null, true);
            callback.onComplete();
        }

        isInProgress = false;
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public void fetchFileFromSystem(UPLOAD_TYPE uploadType) {
        this.uploadType = uploadType;
        Intent photoPickerIntent = new Intent();
        photoPickerIntent.setAction(Intent.ACTION_GET_CONTENT);
        photoPickerIntent.setType("image/*");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            String[] mimetypes = {"image/*", "application/pdf", "application/msword",
                    "application/vnd.openxmlformats-officedocument.wordprocessingml.document"};
            photoPickerIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
        }
        try {
            aActivity.startActivityForResult(photoPickerIntent, Constants.PICK_FILE_FROM_SYSTEM);
        } catch (ActivityNotFoundException e) {
            AlertsHandler.showMessage(aActivity, R.string.unable_to_load_gallery);
        }
    }

    private File createImageFile() {
        File storageDir = Utility.getPictureDirectory();
        if (storageDir.exists()) {
            File image = null;
            try {
                image = File.createTempFile(Utility.getTempFileName(), /* prefix */
                        ".jpg", /* suffix */
                        storageDir /* directory */
                );
            } catch (IOException e) {
                return null;
            }
            return image;
        } else {
            return null;
        }
    }

    public void fetchImageFromUrl(final String url, UPLOAD_TYPE uploadType, String photoId) {


        this.photoId = photoId;
        this.uploadType = uploadType;

        httpRequestType = HttpRequestType.UPLOAD_PIC_CAMERA;

        main_layout.setVisibility(View.GONE);
        crop_layout.setVisibility(View.VISIBLE);
        showImageLoader(true);

        (new CreateBitmapAsyncTask(false)).execute(url);
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private void fetchImageFromSystem() {
        this.photoId = null;
        Intent photoPickerIntent = new Intent();
        photoPickerIntent.setAction(Intent.ACTION_GET_CONTENT);
        photoPickerIntent.setType("*/*");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            String[] mimetypes = {"image/*", "video/*"};
            photoPickerIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
        }
        try {
            aActivity.startActivityForResult(photoPickerIntent, Constants.PICK_IMAGE_FROM_SYSTEM);
        } catch (ActivityNotFoundException e) {
            AlertsHandler.showMessage(aActivity, R.string.unable_to_load_gallery);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.saveCrop:
                Bitmap croppedImage = null;
                try {
                    croppedImage = cropImageView.getCroppedImage();
                } catch (RuntimeException e) {
                    Crashlytics.logException(e);
                }

                if (croppedImage == null) {
                    main_layout.setVisibility(View.VISIBLE);
                    crop_layout.setVisibility(View.GONE);
                    callback.onActivityResultSuccess(httpRequestType, mCurrentPhotoPath);
                    return;
                }
                int w = croppedImage.getWidth();
                int h = croppedImage.getHeight();
                if (w < 200 || h < 200) {
                    AlertsHandler.showMessage(aActivity, aActivity.getResources().getString(R.string.min_photos_size));
                    return;
                } else {
                    try {
                        File file = null;
                        file = Compress.createFile(croppedImage);
                        String tempPath = file.getAbsolutePath();
//                        imageUri = Uri.fromFile(file);
                        imageUri = FileProvider.getUriForFile(aActivity, BuildConfig.APPLICATION_ID + ".provider", file);
                        croppedImage.recycle();
                        main_layout.setVisibility(View.VISIBLE);
                        crop_layout.setVisibility(View.GONE);
                        callback.onActivityResultSuccess(httpRequestType, tempPath);
                    } catch (OutOfMemoryError e) {
                        Crashlytics.logException(e);
                        //Uploading full image as it is.
                        callback.onActivityResultSuccess(httpRequestType, mCurrentPhotoPath);
                    } catch (Throwable throwable) {
                        Crashlytics.logException(throwable);
                        callback.onFail(aActivity.getResources().getString(R.string.please_try_again));
                    }
                }
                break;
        }
    }

    private void makeBitmap() {
        if (Utility.isSet(mCurrentPhotoPath)) {
            main_layout.setVisibility(View.GONE);
            crop_layout.setVisibility(View.VISIBLE);
            showImageLoader(true);
            cropImageView.setVisibility(View.INVISIBLE);
            cropperImageView.setVisibility(View.INVISIBLE);
            (new CreateBitmapAsyncTask(true)).execute(mCurrentPhotoPath);
        } else {
            cropImage(null, null);
        }
    }

    private void showImageLoader(boolean toShow) {
        if (toShow) {
            image_loading_progressbar.setVisibility(View.VISIBLE);
            saveCrop.setEnabled(false);
            ((ProgressBar) image_loading_progressbar)
                    .getIndeterminateDrawable()
                    .setColorFilter(aActivity.getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
        } else {
            image_loading_progressbar.setVisibility(View.GONE);
            saveCrop.setEnabled(true);
        }

    }


    private void cropImage(Bitmap bmap, File file) {
        loadCount = 0;
        FutureCallback<ImageView> loadComplete = new FutureCallback<ImageView>() {
            @Override
            public void onCompleted(Exception e, ImageView result) {
                //Loadcount not used but can be used to check whether image has been loaded into both views.
                loadCount++;
                showImageLoader(false);
            }
        };

        if (file != null && bmap != null) {
            main_layout.setVisibility(View.GONE);
            crop_layout.setVisibility(View.VISIBLE);
            showImageLoader(true);
            try {
                cropImageView.setImageBitmap(bmap, file, loadComplete);
                Ion.with(aActivity).load(file).intoImageView(cropperImageView).setCallback(loadComplete);
                cropImageView.setVisibility(View.VISIBLE);
                cropImageView.setGuidelines(2);
                cropImageView.setAspectRatio(20, 20);
                cropperImageView.setVisibility(View.VISIBLE);
            } catch (Throwable throwable) {
                Crashlytics.logException(throwable);
                main_layout.setVisibility(View.VISIBLE);
                crop_layout.setVisibility(View.GONE);
                callback.onFail(aActivity.getResources().getString(R.string.please_try_again));
            }
        } else if (imageUri != null) {
            main_layout.setVisibility(View.GONE);
            crop_layout.setVisibility(View.VISIBLE);
            showImageLoader(true);
            try {
                cropImageView.setImageUri(imageUri, loadComplete);
                Picasso.with(aActivity).load(imageUri).into(cropperImageView);
                cropImageView.setVisibility(View.VISIBLE);
                cropImageView.setGuidelines(2);
                cropImageView.setAspectRatio(20, 20);
                cropperImageView.setVisibility(View.VISIBLE);
            } catch (Throwable throwable) {
                Crashlytics.logException(throwable);
                main_layout.setVisibility(View.VISIBLE);
                crop_layout.setVisibility(View.GONE);
                callback.onFail(aActivity.getResources().getString(R.string.please_try_again));
            }
        }
        //devices are not able to make bitmap
        else if (mCurrentPhotoPath != null) {
            main_layout.setVisibility(View.VISIBLE);
            crop_layout.setVisibility(View.GONE);
            callback.onActivityResultSuccess(httpRequestType, mCurrentPhotoPath);
        } else {
            main_layout.setVisibility(View.VISIBLE);
            crop_layout.setVisibility(View.GONE);
            callback.onFail(aActivity.getResources().getString(R.string.please_try_again));
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        GetFileCallback onFileComplete = new GetFileCallback() {
            @Override
            public void onGetFileComplete(File file, Uri selectedImage, HttpRequestType requestType) {
                if (file == null) {
                    callback.onFail(aActivity.getResources().getString(R.string.please_try_again));
                } else {
                    httpRequestType = requestType;
                    mCurrentPhotoPath = file.getPath();
                    imageUri = selectedImage;
                    makeBitmap();

                }
            }
        };

        switch (requestCode) {
            case Constants.PICK_FROM_CAMERA_OWN:
                if (resultCode == Activity.RESULT_OK) {

                    if (mCurrentPhotoPath == null) {
                        callback.onFail(aActivity.getResources().getString(R.string.error_capturing_images));
                    } else {

                        httpRequestType = HttpRequestType.UPLOAD_PIC_CAMERA;
                        imageUri = null;

                        if (uploadType == UPLOAD_TYPE.PICTURE) {
                            makeBitmap();

                        } else {
                            callback.onActivityResultSuccess(httpRequestType, mCurrentPhotoPath);
                        }

                    }

                } else if (resultCode == Activity.RESULT_CANCELED) {
                    // User cancelled the image capture
                } else {
                    // Image capture failed, advise user
                }
                break;


            case Constants.PICK_FILE_FROM_SYSTEM:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    Uri selectedImage = data.getData();
                    if (data.getScheme() == null || selectedImage == null || selectedImage.getLastPathSegment() == null) {
                        AlertsHandler.showMessage(aActivity, R.string.please_try_again);
                    } else if (data.getScheme().equalsIgnoreCase("file")
                            && !isValidFileUploadExtenion(selectedImage.getLastPathSegment())) {
                        AlertsHandler.showMessage(aActivity, R.string.file_not_supported);
                    } else if (data.getScheme().equalsIgnoreCase("content")
                            && !isValidFileUploadExtenion(aActivity.getContentResolver().getType(selectedImage))) {
                        AlertsHandler.showMessage(aActivity, R.string.file_not_supported);
                    } else {
                        callback.onActivityResultSuccessFile(httpRequestType, selectedImage, data.getScheme());
                    }
                }
                break;


            case Constants.PICK_IMAGE_FROM_SYSTEM:
                if (resultCode == Activity.RESULT_OK) {
                    Uri selectedImage = data.getData();

                    if (data.getScheme() == null || selectedImage == null || selectedImage.getLastPathSegment() == null) {
                        callback.onFail(aActivity.getResources().getString(R.string.please_try_again));
                    } else if (data.getScheme().equalsIgnoreCase("file")
                            && !selectedImage.getLastPathSegment().endsWith("jpg")
                            && !selectedImage.getLastPathSegment().endsWith("jpeg")
                            && !selectedImage.getLastPathSegment().endsWith("gif")
                            && !selectedImage.getLastPathSegment().endsWith("png")) {
                        AlertsHandler.showMessage(aActivity, R.string.file_not_supported);
                    } else {

                        FilesHandler.getFile(aActivity, selectedImage, data.getScheme(), onFileComplete, HttpRequestType.UPLOAD_PIC_GALLERY);
                    }
                }
                break;


            case Constants.PICK_FROM_GALLERY:
                if (resultCode == Activity.RESULT_OK) {
                    if (data != null && data.getScheme() != null && (data.getScheme().equalsIgnoreCase("content")
                            || data.getScheme().equalsIgnoreCase("file"))) {

                        Uri selectedImage = data.getData();
                        String filePath = FilesHandler.getPath(aActivity, selectedImage, data.getScheme());
                        if (filePath != null && filePath.startsWith("http")) {
                            FilesHandler.getFile(aActivity, selectedImage, data.getScheme(), onFileComplete, httpRequestType);
                        } else {
                            httpRequestType = HttpRequestType.UPLOAD_PIC_GALLERY;
                            mCurrentPhotoPath = filePath;
                            imageUri = selectedImage;
                            makeBitmap();
                        }

                    } else {
                        callback.onFail(aActivity.getResources().getString(R.string.please_select_photo));
                    }
                }
                break;
        }


    }

    private boolean isValidFileUploadExtenion(String fileSegment) {
        return fileSegment != null && (fileSegment.endsWith("pdf") || fileSegment.endsWith("doc")
                || fileSegment.endsWith("docx") || fileSegment.endsWith("jpg") || fileSegment.endsWith("jpeg")
                || fileSegment.endsWith("png") || fileSegment.endsWith("gif"));
    }

    @Override
    public void onPermissionGranted(int requestCode) {
        switch (requestCode) {
            case PermissionsHelper.REQUEST_STORAGE_PERMISSIONS_CODE:
                launchGallery(false);
                break;
            case PermissionsHelper.REQUEST_CAMERA_STORAGE_PERMISSIONS_CODE:
                launchCamera();
                break;
            default:
                break;
        }
    }

    @Override
    public void onPermissionRejected() {
        PermissionsHelper.handlePermissionDenied(aActivity, R.string.default_permission_message);
    }


    public enum UPLOAD_TYPE {
        PICTURE, PHOTO_ID
    }

    public interface PhotoUploaderRequestInterface {
        void onComplete();

        void onSuccess(String jsonResponseString);


        void onFail(String failMessage);

        void onActivityResultSuccess(HttpRequestType type, String filePath);

        void onActivityResultSuccessFile(HttpRequestType httpRequestType, Uri selectedImage, String scheme);
    }

    private class CreateBitmapAsyncTask extends AsyncTask<String, Void, File> {
        Bitmap bmap = null;
        private boolean isLocalFile = false;

        public CreateBitmapAsyncTask(boolean isLocalFile) {
            this.isLocalFile = isLocalFile;
        }

        @Override
        protected File doInBackground(String... urls) {
            if (urls != null && urls[0] != null) {
                try {
                    if (isLocalFile) {
                        bmap = Picasso.with(aActivity).load(new File(urls[0])).config(Config.RGB_565).get();
                    } else {
                        bmap = Picasso.with(aActivity).load(urls[0]).config(Config.RGB_565).get();
                    }
                    return Compress.createFile(bmap);
                } catch (IOException e) {
                    Crashlytics.logException(e);
                    return null;
                } catch (OutOfMemoryError e) {
                    Crashlytics.logException(e);
                    return null;
                } catch (Throwable t) {
                    Crashlytics.logException(t);
                    return null;
                }
            } else {
                return null;
            }
        }

        @Override
        protected void onPostExecute(File file) {
            cropImage(bmap, file);

        }
    }

}
