package com.trulymadly.android.app.asynctasks;

import android.content.Context;
import android.os.AsyncTask;

import com.trulymadly.android.app.json.ConstantsRF;
import com.trulymadly.android.app.sqlite.MessageDBHandler;
import com.trulymadly.android.app.sqlite.RFHandler;
import com.trulymadly.android.app.utility.TimeUtils;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONArray;

/**
 * Created by udbhav on 21/03/16.
 */
public class UnreadCounterHackAsyncTask extends AsyncTask<Void, Void, Void> {
    private static final long UNREAD_COUNTER_HACK_TIMEOUT = TimeUtils.HOURS_24_IN_MILLIS;
    //private static final long UNREAD_COUNTER_HACK_TIMEOUT = 1000;
    private final JSONArray unreadIds;
    private final Context aContext;

    public UnreadCounterHackAsyncTask(Context ctx, JSONArray unreadIds) {
        this.aContext = ctx;
        this.unreadIds = unreadIds;
    }

    @Override
    protected Void doInBackground(Void... params) {
        if (!TimeUtils.isDifferenceGreater(aContext, ConstantsRF.RIGID_FIELD_UNREAD_COUNTER_HACK,
                UNREAD_COUNTER_HACK_TIMEOUT)) {
            return null;
        }
        String myId = Utility.getMyId(aContext);
        RFHandler.insert(aContext, myId,
                ConstantsRF.RIGID_FIELD_UNREAD_COUNTER_HACK, TimeUtils.getSystemTimeInMiliSeconds());

        if (unreadIds != null && unreadIds.length() > 0) {
            for (int i = 0; i < unreadIds.length(); i++) {
                //check unread ids in db and if read, mark them as unread
                if (!MessageDBHandler.isAnyUnreadMessage(aContext, myId, unreadIds.optString(i))) {
                    MessageDBHandler.setChatUnreadIncomingInConversationList(aContext, myId, unreadIds.optString(i));
                }
            }
        }

        //NOT DOING THIS FOR NOW
        //check other ids in db and if unread, mark them as read

        return null;
    }
}
