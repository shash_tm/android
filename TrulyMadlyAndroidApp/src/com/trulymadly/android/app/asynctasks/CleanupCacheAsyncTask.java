package com.trulymadly.android.app.asynctasks;

import android.os.AsyncTask;

import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.utility.FilesHandler;

import java.io.File;
import java.util.HashMap;

public class CleanupCacheAsyncTask extends AsyncTask<String, Void, Void> {

    @Override
    protected Void doInBackground(String... params) {
        String mFolderName = params[0];
        HashMap<String, Long> mKeyToTimestampMap = new HashMap<>();
        HashMap<String, File> mKeyToFileMap = new HashMap<>();

        File directory = new File(
                FilesHandler.getFilePath(Constants.disk_path, mFolderName));
        if (!directory.isDirectory()) {
            return null;
        }

        File[] files = directory.listFiles();
        if(files == null) {
            return null;
        }

        String key = "";
        String name = null;
        Long timestamp = null;
        for (File file : files) {
            name = file.getName();
            if (name.equalsIgnoreCase(".nomedia"))
                continue;
            try {
                key = name.substring(0, name.indexOf("_"));
                timestamp = file.lastModified();
                if (mKeyToTimestampMap.containsKey(key)) {
                    File existingFile = mKeyToFileMap.get(key);
                    long existingFileTimeStamp = existingFile.lastModified();
                    if (timestamp <= existingFileTimeStamp)
                        file.delete();
                    else if (timestamp > existingFileTimeStamp) {
                        existingFile.delete();
                        mKeyToTimestampMap.put(key, timestamp);
                        mKeyToFileMap.put(key, file);
                    }
                } else {
                    mKeyToTimestampMap.put(key, timestamp);
                    mKeyToFileMap.put(key, file);
                }
            } catch (StringIndexOutOfBoundsException ignored) {

            }
        }

        return null;
    }

}
