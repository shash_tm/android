package com.trulymadly.android.app.asynctasks;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.adapter.MyFavoriteGridItem;
import com.trulymadly.android.app.utility.UiUtils;

/**
 * Created by udbhav on 12/09/16.
 */
public class FavoriteImageLoadAsyncTask {
    private final MyFavoriteGridItem holder;
    private final boolean showFavoriteYellowRoundedRectFavorite;
    private final boolean showFavoriteBlackRoundedRectFavorite;
    private final int placeHolder;
    private String imageWithoutFbToken;
    private Context aContext;
//    private final WeakReference<MyFavoriteGridItem> weakReferenceHolder;
//    private final WeakReference<Context> weakReferenceContext;


    public FavoriteImageLoadAsyncTask(Context aContext, MyFavoriteGridItem holder, String imageWithoutFbToken, boolean showFavoriteYellowRoundedRectFavorite, boolean showFavoriteBlackRoundedRectFavorite, int placeHolder) {
        this.aContext = aContext;
        this.holder = holder;
        this.imageWithoutFbToken = imageWithoutFbToken;
        this.showFavoriteYellowRoundedRectFavorite = showFavoriteYellowRoundedRectFavorite;
        this.showFavoriteBlackRoundedRectFavorite = showFavoriteBlackRoundedRectFavorite;
        this.placeHolder = placeHolder;
//        this.weakReferenceHolder = new WeakReference<>(holder);
//        this.weakReferenceContext = new WeakReference<>(aContext);

    }

    public void execute() {
        holder.myFavoriteImage.setImageResource(0);
        holder.myFavoriteProgressBar.setVisibility(View.VISIBLE);
        Picasso.with(aContext).load(imageWithoutFbToken).noPlaceholder().config(Bitmap.Config.RGB_565).into(holder.myFavoriteImage, new Callback() {
            @Override
            public void onSuccess() {
                holder.myFavoriteProgressBar.setVisibility(View.GONE);
                if (showFavoriteYellowRoundedRectFavorite) {
                    UiUtils.setBackground(holder.favoriteBorder.getContext(), holder.favoriteBorder, R.drawable.yellow_rounded_rect_favorite);
                } else if (showFavoriteBlackRoundedRectFavorite) {
                    UiUtils.setBackground(holder.favoriteBorder.getContext(), holder.favoriteBorder, R.drawable.black_rounded_rect_favorite);

                }
            }

            @Override
            public void onError() {
                holder.myFavoriteProgressBar.setVisibility(View.GONE);
                holder.myFavoriteImage.setImageResource(placeHolder);
                if (showFavoriteYellowRoundedRectFavorite) {
                    UiUtils.setBackground(holder.favoriteBorder.getContext(), holder.favoriteBorder, R.drawable.yellow_rounded_rect_favorite);
                } else if (showFavoriteBlackRoundedRectFavorite) {
                    UiUtils.setBackground(holder.favoriteBorder.getContext(), holder.favoriteBorder, R.drawable.black_rounded_rect_favorite);

                }
            }
        });

    }

}
