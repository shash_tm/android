/**
 * 
 */
package com.trulymadly.android.app;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.crashlytics.android.Crashlytics;
import com.trulymadly.android.app.listener.OnPermissionResultListener;
import com.trulymadly.android.app.utility.ActivityHandler;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.DatePickerFragment;
import com.trulymadly.android.app.utility.DatePickerFragment.OnDateSetInterface;
import com.trulymadly.android.app.utility.TimeUtils;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;

import java.util.Date;

/**
 * @author udbhav
 * 
 */
public class RegisterHomeFragment extends android.support.v4.app.Fragment
		implements OnClickListener, OnPermissionResultListener {

	private OnRegisterHomeFragmentClick mCallback;
	private View registerHomeFragmentView;
	private DialogFragment datePickerFragment = null;
	private EditText dob_et;
	private int dob_d = 0, dob_m, dob_y;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		registerHomeFragmentView = inflater.inflate(R.layout.register_home,
				container, false);

		Button registerButton = (Button) registerHomeFragmentView
				.findViewById(R.id.btn_register);
		RelativeLayout registerFacebookButton = (RelativeLayout) registerHomeFragmentView
				.findViewById(R.id.register_fb_id);
		dob_et = (EditText) registerHomeFragmentView
				.findViewById(R.id.birthdate_text_register);
		View dob_et_ll = registerHomeFragmentView
				.findViewById(R.id.birthdate_text_register_ll);

		registerButton.setOnClickListener(this);
		registerFacebookButton.setOnClickListener(this);

		dob_et.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				showDatePickerDialog();
				UiUtils.hideKeyBoard(getActivity());
			}
		});
		dob_et_ll.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				showDatePickerDialog();
				UiUtils.hideKeyBoard(getActivity());
			}
		});
		dob_et.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View paramView, boolean hasFocus) {
				if (hasFocus) {
					showDatePickerDialog();
					UiUtils.hideKeyBoard(getActivity());
				}

			}
		});

		// Setting up animation
		LinearLayout registerHomeContainerLayout = (LinearLayout) registerHomeFragmentView
				.findViewById(R.id.registerHomeContainer);
		//Animation slideUpAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.push_left_in);
		//registerHomeContainerLayout.startAnimation(slideUpAnimation);

		EditText password = ((EditText) registerHomeFragmentView
				.findViewById(R.id.new_passwd));
		password.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_GO) {
					mCallback.onRegisterHomeFragmentClick(v,
							registerHomeFragmentView);
				}
				return false;
			}
		});

		password.setTypeface(Typeface.DEFAULT);
		password.setTransformationMethod(new PasswordTransformationMethod());

		((RadioGroup) registerHomeFragmentView.findViewById(R.id.radio_gender))
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(RadioGroup group, int checkedId) {
						mCallback.hideKeyboard();
					}
				});

		String possibleEmail = null;
		possibleEmail = Utility.getUserEmail(getActivity(), false);

		Bundle data = getArguments();
		String userId = data.getString("userId");
		String passwd = data.getString("passwd");

		if (Utility.isSet(passwd)) {
			password.setText(passwd);
		}

		if (Utility.isSet(userId)) {
			((EditText) registerHomeFragmentView.findViewById(R.id.new_userid))
					.setText(userId);
		} else if (Utility.isSet(possibleEmail)) {
			((EditText) registerHomeFragmentView.findViewById(R.id.new_userid))
					.setText(possibleEmail);
		}

		return registerHomeFragmentView;
	}

	private void showDatePickerDialog() {
		if (datePickerFragment == null) {
			OnDateSetInterface onDateSetInterface = new OnDateSetInterface() {

				@Override
				public void onDateSet(int day, int month, int year) {
					setDate(day, month, year);
				}
			};
			datePickerFragment = new DatePickerFragment(getActivity(),
					onDateSetInterface);
		}
		if (!datePickerFragment.isAdded() && !datePickerFragment.isVisible()) {
			Bundle data = new Bundle();
			data.putInt("dob_d", dob_d);
			data.putInt("dob_m", dob_m);
			data.putInt("dob_y", dob_y);
			datePickerFragment.setArguments(data);
			try {
				datePickerFragment.show(getActivity()
						.getSupportFragmentManager(), "ageDatePicker");
			} catch (IllegalStateException ignored) {

			}
		}
	}

	private void setDate(int day, int month, int year) {
		dob_d = day;
		dob_m = month;
		dob_y = year;
		Date date = new Date(year - 1900, month - 1, day);
		dob_et.setTag(date);
		dob_et.setText(TimeUtils.getFormattedDate(date));
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		try {
			mCallback = (OnRegisterHomeFragmentClick) activity;
		} catch (ClassCastException e) {
			Crashlytics.logException(e);
			throw new ClassCastException(activity.toString()
					+ " must implement OnRegisterHomeFragmentClick");
		}
	}

	@Override
	public void onClick(View v) {
		mCallback.onRegisterHomeFragmentClick(v, registerHomeFragmentView);
	}

	@Override
	public void onPermissionGranted(int requestCode) {
		AlertsHandler.showMessage(getActivity(), "GET_ACCOUNTS_GRANTED");
		((EditText) registerHomeFragmentView.findViewById(R.id.new_userid))
		.setText(Utility.getUserEmail(getActivity(), false));
	}

	@Override
	public void onPermissionRejected() {
		AlertsHandler.showMessageWithAction(getActivity(), "GET_ACCOUNTS_NOT_GRANTED", "Settings", new OnClickListener() {

            @Override
            public void onClick(View v) {
                ActivityHandler.openAppSettings(getActivity());
            }
        });
	}

	public interface OnRegisterHomeFragmentClick {
		void onRegisterHomeFragmentClick(View v,
										 View registerHomeFragmentView);

		void hideKeyboard();
	}
	
}
