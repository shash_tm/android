package com.trulymadly.android.app.modal;

class ProfileWork {

	private String workHeading;
	private String workDetail;

	public ProfileWork(String workHeading,String workDetail){
		this.workHeading=workHeading;
		this.workDetail=workDetail;
	}
	public ProfileWork(){}

	public String getWorkHeading() {
		return this.workHeading;
	}

	public void setWorkHeading(String workHeading){
		this.workHeading=workHeading;
	}

	public String getWorkDetail(){
		return this.workDetail;
	}

	public void setWorkDetail(String workDetail) {
		this.workDetail = workDetail;
	}
}