package com.trulymadly.android.app.modal;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by avin on 25/10/16.
 */

public class SelectQuizCompareModal implements Parcelable {
    public static final Creator<SelectQuizCompareModal> CREATOR = new Creator<SelectQuizCompareModal>() {
        @Override
        public SelectQuizCompareModal createFromParcel(Parcel in) {
            return new SelectQuizCompareModal(in);
        }

        @Override
        public SelectQuizCompareModal[] newArray(int size) {
            return new SelectQuizCompareModal[size];
        }
    };
    private String mMatchId, mMatchName, mMatchUrl, mUserUrl;
    private int mPercent;
    private ArrayList<SelectQuizQuesCompareResp> mSelectQuizQuesCompareResps;

    public SelectQuizCompareModal(int percent) {
        this.mPercent = percent;
    }

    public SelectQuizCompareModal(String mMatchId, String mMatchName, String mMatchUrl, String mUserUrl) {
        this.mMatchId = mMatchId;
        this.mMatchName = mMatchName;
        this.mMatchUrl = mMatchUrl;
        this.mUserUrl = mUserUrl;
    }

    protected SelectQuizCompareModal(Parcel in) {
        mMatchId = in.readString();
        mMatchName = in.readString();
        mMatchUrl = in.readString();
        mUserUrl = in.readString();
        mPercent = in.readInt();
    }

    public String getmMatchId() {
        return mMatchId;
    }

    public void setmMatchId(String mMatchId) {
        this.mMatchId = mMatchId;
    }

    public String getmMatchName() {
        return mMatchName;
    }

    public String getmMatchUrl() {
        return mMatchUrl;
    }

    public String getmUserUrl() {
        return mUserUrl;
    }

    public int getmPercent() {
        return mPercent;
    }

    public void setmPercent(int mPercent) {
        this.mPercent = mPercent;
    }

    public ArrayList<SelectQuizQuesCompareResp> getmSelectQuizQuesCompareResps() {
        return mSelectQuizQuesCompareResps;
    }

    public void setmSelectQuizQuesCompareResps(ArrayList<SelectQuizQuesCompareResp> mSelectQuizQuesCompareResps) {
        this.mSelectQuizQuesCompareResps = mSelectQuizQuesCompareResps;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mMatchId);
        parcel.writeString(mMatchName);
        parcel.writeString(mMatchUrl);
        parcel.writeString(mUserUrl);
        parcel.writeInt(mPercent);
    }
}
