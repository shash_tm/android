package com.trulymadly.android.app.modal;

import com.trulymadly.android.app.json.Constants;

import java.util.ArrayList;

public class UserModal {
    private String profilePic;
    private String status;
    private String userId;
    private String name;
    private String age;
    private String gender;
    private String city;
    private String lastActive;
    private String height;
    private String maritalStatus;
    private String haveChildren;
    private String incomeStart;
    private boolean celebStatus = false;
    private boolean isDummySet = false;
    private String badgeUrl = "";
    private String mSparkHash;
    private boolean hasLikedBefore = false;

    private String[] otherPics;
    private String[] videoUrls;
    private String[] thumbnailUrls;
    private ArrayList<VideoModal> videoArray;

    private int mCountryId = Constants.COUNTRY_ID_INDIA;
    private String mCountryName;

    public UserModal() {
    }

    public ArrayList<VideoModal> getVideoArray() {
        return videoArray;
    }

    public void setVideoArray(ArrayList<VideoModal> videoArray) {
        this.videoArray = videoArray;
    }

    public String getProfilePic() {
        return this.profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return this.age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getGender() {
        return this.gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLastActive() {
        return this.lastActive;
    }

    public void setLastActive(String lastActive) {
        this.lastActive = lastActive;
    }

    public String getHeight() {
        return this.height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getMaritalStatus() {
        return this.maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getHaveChildren() {
        return this.haveChildren;
    }

    public void setHaveChildren(String haveChildren) {
        this.haveChildren = haveChildren;
    }

    public String getIncomeStart() {
        return this.incomeStart;
    }

    public void setIncomeStart(String incomeStart) {
        this.incomeStart = incomeStart;
    }

    public String[] getOtherPics() {
        return this.otherPics;
    }

    public void setOtherPics(String[] otherPics) {
        this.otherPics = otherPics;
    }

    public boolean isCelebStatus() {
        return celebStatus;
    }

    public void setCelebStatus(boolean celebStatus) {
        this.celebStatus = celebStatus;
    }

    public boolean isDummySet() {
        return isDummySet;
    }

    public void setDummySet(boolean isDummySet) {
        this.isDummySet = isDummySet;
    }

    public String getBadgeUrl() {
        return badgeUrl;
    }

    public void setBadgeUrl(String badgeUrl) {
        this.badgeUrl = badgeUrl;
    }

    public String getmSparkHash() {
        return mSparkHash;
    }

    public void setmSparkHash(String mSparkHash) {
        this.mSparkHash = mSparkHash;
    }

    public String[] getVideoUrls() {
        return videoUrls;
    }

    public void setVideoUrls(String[] videoUrls) {
        this.videoUrls = videoUrls;
    }

    public String[] getThumbnailUrls() {
        return thumbnailUrls;
    }

    public void setThumbnailUrls(String[] thumbnailUrls) {
        this.thumbnailUrls = thumbnailUrls;
    }

    public boolean isHasLikedBefore() {
        return hasLikedBefore;
    }

    public void setHasLikedBefore(boolean hasLikedBefore) {
        this.hasLikedBefore = hasLikedBefore;
    }

    public int getmCountryId() {
        return mCountryId;
    }

    public void setmCountryId(int mCountryId) {
        this.mCountryId = mCountryId;
    }

    public String getmCountryName() {
        return mCountryName;
    }

    public void setmCountryName(String mCountryName) {
        this.mCountryName = mCountryName;
    }
}