package com.trulymadly.android.app.modal;

/**
 * Created by avin on 29/10/15.
 */
public class AdImageModal {
    private String mUrl;
    private double mAspectRatio;
    private int mHeight;
    private int mWidth;

    public AdImageModal(String url, double aspectRatio, int height, int width){
        mUrl = url;
        mAspectRatio = aspectRatio;
        mHeight = height;
        mWidth = width;
    }

    public String getmUrl() {
        return mUrl;
    }

    public void setmUrl(String mUrl) {
        this.mUrl = mUrl;
    }

    public double getmAspectRatio() {
        return mAspectRatio;
    }

    public void setmAspectRatio(double mAspectRatio) {
        this.mAspectRatio = mAspectRatio;
    }

    public int getmHeight() {
        return mHeight;
    }

    public void setmHeight(int mHeight) {
        this.mHeight = mHeight;
    }

    public int getmWidth() {
        return mWidth;
    }

    public void setmWidth(int mWidth) {
        this.mWidth = mWidth;
    }
}
