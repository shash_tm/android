package com.trulymadly.android.app.modal;

import java.io.Serializable;

public class NudgeClass implements Serializable {
	private static final long serialVersionUID = 6104657406705106229L;
	public String match_name;
	public String match_pic_url;
	public String match_id;
	public Boolean has_played = false;
	public Boolean send_nudge = true;
	public String full_conv_link;

}
