package com.trulymadly.android.app.modal;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by avin on 06/06/16.
 */
public class TMExtrasViewPagerItemModal implements Parcelable{

    private String mImageUrl, mTitle, mText;

    public TMExtrasViewPagerItemModal(String mImageUrl, String mTitle, String mText) {
        this.mImageUrl = mImageUrl;
        this.mTitle = mTitle;
        this.mText = mText;
    }

    protected TMExtrasViewPagerItemModal(Parcel in) {
        mImageUrl = in.readString();
        mTitle = in.readString();
        mText = in.readString();
    }

    public static final Creator<TMExtrasViewPagerItemModal> CREATOR = new Creator<TMExtrasViewPagerItemModal>() {
        @Override
        public TMExtrasViewPagerItemModal createFromParcel(Parcel in) {
            return new TMExtrasViewPagerItemModal(in);
        }

        @Override
        public TMExtrasViewPagerItemModal[] newArray(int size) {
            return new TMExtrasViewPagerItemModal[size];
        }
    };

    public String getmImageUrl() {
        return mImageUrl;
    }

    public void setmImageUrl(String mImageUrl) {
        this.mImageUrl = mImageUrl;
    }

    public String getmText() {
        return mText;
    }

    public void setmText(String mText) {
        this.mText = mText;
    }

    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mImageUrl);
        dest.writeString(mTitle);
        dest.writeString(mText);
    }

}
