package com.trulymadly.android.app.modal;


import android.os.Parcel;
import android.os.Parcelable;


public class LocationModal implements Parcelable {

    public static final Creator<LocationModal> CREATOR = new Creator<LocationModal>() {
        @Override
        public LocationModal createFromParcel(Parcel in) {
            return new LocationModal(in);
        }

        @Override
        public LocationModal[] newArray(int size) {
            return new LocationModal[size];
        }
    };
    private String address;
    private String contactNo;

    public LocationModal() {
    }

    private LocationModal(Parcel in) {
        address = in.readString();
        contactNo = in.readString();
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Describe the kinds of special objects contained in this Parcelable's
     * marshalled representation.
     *
     * @return a bitmask indicating the setString of special object types marshalled
     * by the Parcelable.
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * Flatten this object in to a Parcel.
     *
     * @param dest  The Parcel in which the object should be written.
     * @param flags Additional flags about how the object should be written.
     *              May be 0 or {@link #PARCELABLE_WRITE_RETURN_VALUE}.
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(address);
        dest.writeString(contactNo);
    }
}
