package com.trulymadly.android.app.modal;

/**
 * Created by avin on 03/12/15.
 */
public class ChatsConfig {
    private boolean isWebsocketOnly, isSocketDebugEnabled, isPasscodeEnabled;
    private String mChatDomain, mABPrefix, mChatIpList;
    private int mConnectionRetryAttempts, mChatSendTimeout, mConnectionTimeout, missedMessagesTimeout;

    public boolean isWebsocketOnly() {
        return isWebsocketOnly;
    }

    public void setIsWebsocketOnly(boolean isWebsocketOnly) {
        this.isWebsocketOnly = isWebsocketOnly;
    }

    public boolean isSocketDebugEnabled() {
        return isSocketDebugEnabled;
    }

    public void setIsSocketDebugEnabled(boolean isSocketDebugEnabled) {
        this.isSocketDebugEnabled = isSocketDebugEnabled;
    }

    public boolean isPasscodeEnabled() {
        return isPasscodeEnabled;
    }

    public void setIsPasscodeEnabled(boolean isPasscodeEnabled) {
        this.isPasscodeEnabled = isPasscodeEnabled;
    }

    public String getmChatDomain() {
        return mChatDomain;
    }

    public void setmChatDomain(String mChatDomain) {
        this.mChatDomain = mChatDomain;
    }

    public String getmABPrefix() {
        return mABPrefix;
    }

    public void setmABPrefix(String mABPrefix) {
        this.mABPrefix = mABPrefix;
    }

    public int getmConnectionRetryAttempts() {
        return mConnectionRetryAttempts;
    }

    public void setmConnectionRetryAttempts(int mConnectionRetryAttempts) {
        this.mConnectionRetryAttempts = mConnectionRetryAttempts;
    }

    public int getmChatSendTimeout() {
        return mChatSendTimeout;
    }

    public void setmChatSendTimeout(int mChatSendTimeout) {
        this.mChatSendTimeout = mChatSendTimeout;
    }

    public int getmConnectionTimeout() {
        return mConnectionTimeout;
    }

    public void setmConnectionTimeout(int mConnectionTimeout) {
        this.mConnectionTimeout = mConnectionTimeout;
    }

    public int getMissedMessagesTimeout() {
        return missedMessagesTimeout;
    }

    public void setMissedMessagesTimeout(int missedMessagesTimeout) {
        this.missedMessagesTimeout = missedMessagesTimeout;
    }

    public String getmChatIpList() {
        return mChatIpList;
    }

    public void setmChatIpList(String mChatIpList) {
        this.mChatIpList = mChatIpList;
    }
}
