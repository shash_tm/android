package com.trulymadly.android.app.modal;

public class WorkModal {

    private String workStatus;
    private String designation;
    private String indsutry;
    private String[] companies;
    private String company;

    public WorkModal() {
    }

    public WorkModal(String workStatus, String designation, String indsutry, String[] companies,
                     String company) {
        this.workStatus = workStatus;
        this.designation = designation;
        this.indsutry = indsutry;
        this.companies = companies;
        this.company = company;
    }

    public String getWorkStatus() {
        return this.workStatus;
    }

    public void setWorkStatus(String workStatus) {
        this.workStatus = workStatus;
    }

    public String getDesignation() {
        return this.designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getIndustry() {
        return this.indsutry;
    }

    public void setIndustry(String indsutry) {
        this.indsutry = indsutry;
    }

    public String[] getCompanies() {
        return this.companies;
    }

    public void setCompanies(String[] companies) {
        this.companies = companies;
    }

    public String getCompany() {
        return this.company;
    }

    public void setCompany(String company) {
        this.company = company;
    }
}