package com.trulymadly.android.app.modal;

public class LoginDetail {

	private String email;
	private String password;
	private String push_registration;
	private String fb_token;
	private boolean isFromFB;
	private String referrer = null;

	public LoginDetail() {
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setPushRegistration(String push_registration) {
		this.push_registration = push_registration;
	}

	public void setFBToken(String fb_token) {
		this.fb_token = fb_token;
	}

	public void setIsFromFB(boolean isFromFB) {
		this.isFromFB = isFromFB;
	}

	public String getEmail() {
		return this.email;
	}

	public String getPassword() {
		return this.password;
	}

	public String getPushRegistration() {
		return this.push_registration;
	}

	public String getFBToken() {
		return this.fb_token;
	}

	public boolean getIsFromFB() {
		return this.isFromFB;
	}

	public String getReferrer() {
		return referrer == null ? "" : referrer;
	}

	public void setReferrer(String referrer) {
		this.referrer = referrer;
	}
}