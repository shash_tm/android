package com.trulymadly.android.app.modal;

/**
 * Created by udbhav on 04/04/16.
 */
public class ActiveConversationModal {
    private String match_id, fname, age, message_link, profile_url, profile_pic;
    private boolean isMissTM;

    public ActiveConversationModal(String match_id, boolean isMissTm, String fname, String age, String message_link, String profile_url, String profile_pic) {
        setMatch_id(match_id);
        setIsMissTM(isMissTm);
        setFname(fname);
        setAge(age);
        setMessage_link(message_link);
        setProfile_url(profile_url);
        setProfile_pic(profile_pic);
    }

    public String getMatch_id() {
        return match_id;
    }

    private void setMatch_id(String match_id) {
        this.match_id = match_id;
    }

    public String getFname() {
        return fname;
    }

    private void setFname(String fname) {
        this.fname = fname;
    }

    public String getAge() {
        return age;
    }

    private void setAge(String age) {
        this.age = age;
    }

    public String getMessage_link() {
        return message_link;
    }

    private void setMessage_link(String message_link) {
        this.message_link = message_link;
    }

    public String getProfile_url() {
        return profile_url;
    }

    private void setProfile_url(String profile_url) {
        this.profile_url = profile_url;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    private void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public boolean isMissTM() {
        return isMissTM;
    }

    private void setIsMissTM(boolean isMissTM) {
        this.isMissTM = isMissTM;
    }

}
