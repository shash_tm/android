package com.trulymadly.android.app.modal;

/**
 * Created by avin on 29/10/15.
 */
public class AdNativeListItemModal extends AdNativeModal{
    private String mDescription;

    public String getmDescription() {
        return mDescription;
    }

    public void setmDescription(String mDescription) {
        this.mDescription = mDescription;
    }
}
