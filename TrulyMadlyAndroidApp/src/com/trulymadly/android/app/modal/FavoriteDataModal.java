package com.trulymadly.android.app.modal;

import android.os.Parcel;
import android.os.Parcelable;

import com.facebook.AccessToken;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.utility.Utility;

import static com.trulymadly.android.app.utility.FavoriteUtils.FAVORITE_KEY_BOOKS;
import static com.trulymadly.android.app.utility.FavoriteUtils.FAVORITE_KEY_FOOD;
import static com.trulymadly.android.app.utility.FavoriteUtils.FAVORITE_KEY_MOVIES;
import static com.trulymadly.android.app.utility.FavoriteUtils.FAVORITE_KEY_MUSIC;

/**
 * Created by udbhav on 12/07/16.
 */
public class FavoriteDataModal implements Parcelable {

    public static final Creator<FavoriteDataModal> CREATOR = new Creator<FavoriteDataModal>() {
        @Override
        public FavoriteDataModal createFromParcel(Parcel in) {
            return new FavoriteDataModal(in);
        }

        @Override
        public FavoriteDataModal[] newArray(int size) {
            return new FavoriteDataModal[size];
        }
    };

    String id, name, category, imageUrl;
    boolean isSelected = false, isSelectable = false, isMutualFavorite = false, isSuggestion = false, isFacebookImage = false, isShowMoreItem = false, isEdit = false;
    int placeHolderResId;

    public FavoriteDataModal(String id, String name, String category, String imageUrl, boolean isSelected, boolean isSelectable, boolean isMutualFavorite, boolean isSuggestion, boolean isEdit) {
        this.id = id;
        this.name = name;
        this.category = category;
        setImageAndPlaceHolder(imageUrl);
        this.isSelected = isSelected;
        this.isSelectable = isSelectable;
        this.isMutualFavorite = isMutualFavorite;
        //this.isMutualFavorite = (new Random()).nextInt(2) % 2 == 1 ? true : false;
        this.isSuggestion = isSuggestion;
        this.isEdit = isEdit;
    }

    protected FavoriteDataModal(Parcel in) {
        id = in.readString();
        name = in.readString();
        category = in.readString();
        imageUrl = in.readString();
        isSelected = in.readByte() != 0;
        isSelectable = in.readByte() != 0;
        isMutualFavorite = in.readByte() != 0;
        isSuggestion = in.readByte() != 0;
        isFacebookImage = in.readByte() != 0;
        isShowMoreItem = in.readByte() != 0;
        isEdit = in.readByte() != 0;
        placeHolderResId = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(category);
        dest.writeString(imageUrl);
        dest.writeByte((byte) (isSelected ? 1 : 0));
        dest.writeByte((byte) (isSelectable ? 1 : 0));
        dest.writeByte((byte) (isMutualFavorite ? 1 : 0));
        dest.writeByte((byte) (isSuggestion ? 1 : 0));
        dest.writeByte((byte) (isFacebookImage ? 1 : 0));
        dest.writeByte((byte) (isShowMoreItem ? 1 : 0));
        dest.writeByte((byte) (isEdit ? 1 : 0));
        dest.writeInt(placeHolderResId);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public boolean equals(Object object) {
        boolean isEqual = false;

        if (object != null && object instanceof FavoriteDataModal) {
            isEqual = this.hashCode() == object.hashCode();
        }

        return isEqual;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCategory() {
        return category;
    }

    public int getPlaceHolderResId() {
        return placeHolderResId;
    }

    public String getImageUrl(boolean withFbToken) {
        if (withFbToken && isFacebookImage) {
            AccessToken accessToken = AccessToken.getCurrentAccessToken();
            if (accessToken != null && Utility.isSet(accessToken.getToken())) {
                return this.imageUrl + "&access_token=" + accessToken.getToken();
            } else {
                return imageUrl;
            }
        } else {
            return imageUrl;
        }
    }

    private void setImageAndPlaceHolder(String imageUrl) {
        if (Utility.isSet(imageUrl)) {
            this.imageUrl = imageUrl;
        } else if (Utility.isSet(id) && id.startsWith("fid:")) {
            this.isFacebookImage = true;
            String fid = id.substring(4);
            this.imageUrl = "https://graph.facebook.com/" + fid + "/picture?height=200";
        } else {
            this.imageUrl = null;
        }
        if (FAVORITE_KEY_MOVIES.equals(category)) {
            this.placeHolderResId = R.drawable.vector_favorite_default_movies;
        } else if (FAVORITE_KEY_MUSIC.equals(category)) {
            this.placeHolderResId = R.drawable.vector_favorite_default_music;
        } else if (FAVORITE_KEY_BOOKS.equals(category)) {
            this.placeHolderResId = R.drawable.vector_favorite_default_books;
        } else if (FAVORITE_KEY_FOOD.equals(category)) {
            this.placeHolderResId = R.drawable.vector_favorite_default_food;
        } else {
            this.placeHolderResId = R.drawable.vector_favorite_default_others;
        }

    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isShowMoreItem() {
        return isShowMoreItem;
    }

    public void setShowMoreItem(boolean showMoreItem) {
        isShowMoreItem = showMoreItem;
    }

    public boolean isSelectable() {
        return isSelectable;
    }

    public boolean isMutualFavorite() {
        return isMutualFavorite;
    }

    @Override
    public String toString() {
        return this.getName();
    }

    public boolean isEdit() {
        return isEdit;
    }

    public boolean isSuggestion() {
        return isSuggestion;
    }
}
