package com.trulymadly.android.app.modal;

public class EditPrefBasicDataModal {
	private String id; // for country or income,degree
	private String id2; // for state
	private String id3; // for city
	private String name;
	private boolean isSelected;
	
	private int degreeId;

	public EditPrefBasicDataModal(){}
	public EditPrefBasicDataModal(String id,String id2,String id3, String name, boolean isSelected, int degreeId){
		this.id=id;
		this.id2=id2;
		this.id3=id3;
		this.name=name;
		this.isSelected=isSelected;
		this.degreeId=degreeId;
	}

	public void setIDorCountryID(String id){
		this.id=id;
	}
	public void setStateId(String id2){
		this.id2=id2;
	}
	public void setCityId(String id3){
		this.id3=id3;
	}
	public void setDegreeId(int degreeId){
		this.degreeId=degreeId;
	}
	
	public void setName(String name){
		this.name=name;
	}
	public void setIsSelected(boolean isSelected){
		this.isSelected=isSelected;
	}

	public String getIDorCountryID(){
		return this.id;
	}
	public String getStateId(){
		return this.id2;
	}
	public String getCityId(){
		return this.id3;
	}
	public String getName(){
		return this.name;
	}
	public boolean getIsSelected(){
		return this.isSelected;
	}
	public int getDegreeId(){
		return this.degreeId;
	}
}