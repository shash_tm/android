package com.trulymadly.android.app.modal;

import java.util.ArrayList;
import java.util.Vector;

public class MatchesLatestModal2 {

	private Vector<ProfileNewModal> profileNewModalList;
	private MyDetailModal aMyDetailModal;
	private MatchesSysMesgModal aMatchesSysMesgModal;

	//Female nudge
	private ArrayList<Integer> mFemaleNudgePositionsList;
	private ArrayList<String> mFemaleNudgeMessagesList;

	public MatchesLatestModal2() {
	}

	public MatchesLatestModal2(Vector<ProfileNewModal> profileNewModalList,
							   MyDetailModal aMyDetailModal,
							   MatchesSysMesgModal aMatchesSysMesgModal
	) {
		this.profileNewModalList = profileNewModalList;
		this.aMyDetailModal = aMyDetailModal;
		this.aMatchesSysMesgModal = aMatchesSysMesgModal;
	}

	public ArrayList<Integer> getmFemaleNudgePositionsList() {
		return mFemaleNudgePositionsList;
	}

	public void setmFemaleNudgePositionsList(ArrayList<Integer> mFemaleNudgePositionsList) {
		this.mFemaleNudgePositionsList = mFemaleNudgePositionsList;
	}

	public ArrayList<String> getmFemaleNudgeMessagesList() {
		return mFemaleNudgeMessagesList;
	}

	public void setmFemaleNudgeMessagesList(ArrayList<String> mFemaleNudgeMessagesList) {
		this.mFemaleNudgeMessagesList = mFemaleNudgeMessagesList;
	}

	public Vector<ProfileNewModal> getProfileNewModalList(){
		return this.profileNewModalList;
	}

	public void setProfileNewModalList(Vector<ProfileNewModal> profileNewModalList) {
		this.profileNewModalList = profileNewModalList;
	}

	public MyDetailModal getMyDetailModal(){
		return this.aMyDetailModal;
	}

	public void setMyDetailModal(MyDetailModal aMyDetailModal) {
		this.aMyDetailModal = aMyDetailModal;
	}

	public MatchesSysMesgModal getMatchesSysMesgModal(){
		return this.aMatchesSysMesgModal;
	}

	public void setMatchesSysMesgModal(MatchesSysMesgModal aMatchesSysMesgModal) {
		this.aMatchesSysMesgModal = aMatchesSysMesgModal;
	}
}