/**
 *
 */
package com.trulymadly.android.app.modal;

import android.content.Context;
import android.database.Cursor;

import com.trulymadly.android.app.MessageModal;
import com.trulymadly.android.app.MessageModal.MessageType;
import com.trulymadly.android.app.json.Constants.MessageState;
import com.trulymadly.android.app.json.ConstantsDB;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.TimeUtils;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONObject;

import java.util.Date;

/**
 * @author udbhav
 */
public class ConversationModal {

    private String fname = null, age = null, profile_pic = null,
            match_id = null,
            message_link = null;
    private MessageState lastMessageState;
    private Date tstamp = null;
    private Date clear_chat_tstamp = null;
    private String last_message = null;
    private MessageType last_message_type = MessageType.TEXT;
    private String profile_link = null;
    private boolean isMissTm = false;
    private boolean isChatCleared;

    private boolean isSparkReceived, isMutualSpark;

    private boolean isSelectMatch;

    public ConversationModal() {
    }
    public ConversationModal(JSONObject data, String myId, Context aContext) {

        if (data == null) {
            return;
        }
        JSONObject messageListData = data.optJSONObject("message_list");
        if (messageListData == null) {
            return;
        }

        setFname(messageListData.optString("fname"));
        setAge(messageListData.optString("age"));
        setProfile_pic(messageListData.optString("profile_pic"));

        setMessage_link(messageListData.optString("full_conv_link"));
        setLast_message(messageListData.optString("message"));

        setTstamp(TimeUtils.getParsedTime(messageListData.optString("timestamp")));

        String blockedBy = messageListData.optString("blocked_by");
        String receiver_id = messageListData.optString("receiver_id");
        String sender_id = messageListData.optString("sender_id");
        Boolean isRead = messageListData.optBoolean("seen");
        if (Utility.isSet(blockedBy)) {
            if (Utility.stringCompare(blockedBy, myId) || !messageListData.optString("is_blocked_shown").equals("0")) {
                setLastMessageState(MessageState.BLOCKED_SHOWN);
            } else {
                setLastMessageState(MessageState.BLOCKED_UNSHOWN);
            }
        } else {
            String msgType = messageListData.optString("msg_type");
            setLast_message_type(MessageModal.getMessageTypeFromString(msgType));
            if (last_message_type == MessageType.MUTUAL_LIKE) {
                setLastMessageState(isRead ? MessageState.MATCH_ONLY : MessageState.MATCH_ONLY_NEW);
            } else if (Utility.stringCompare(sender_id, myId)) {
                setLastMessageState(isRead ? MessageState.OUTGOING_READ : MessageState.OUTGOING_SENT);
            } else {
                setLastMessageState(isRead ? MessageState.INCOMING_READ : MessageState.INCOMING_DELIVERED);
            }
        }

        setMatch_id(Utility.stringCompare(myId, receiver_id) ? sender_id : receiver_id);

        //updating the last row update tstamp string in shared preferences
        String server_row_updated_tstamp_string = messageListData.optString("row_updated_tstamp");
        String local_row_updated_tstamp_string = SPHandler.getString(aContext, ConstantsSP.SHARED_KEYS_CONVERSATION_LIST_API_CALL_TIME);
        if (!Utility.isSet(local_row_updated_tstamp_string) || (Utility.isSet(server_row_updated_tstamp_string) && TimeUtils.getParsedTime(server_row_updated_tstamp_string).after(TimeUtils.getParsedTime(local_row_updated_tstamp_string)))) {
            SPHandler.setString(aContext, ConstantsSP.SHARED_KEYS_CONVERSATION_LIST_API_CALL_TIME, server_row_updated_tstamp_string);
        }

        setProfile_link(messageListData.optString("profile_link", null));

        setIsMissTm(messageListData
                .optBoolean("is_miss_tm", false));

        //Spark related
        if(messageListData.optBoolean("is_spark")){
            setSparkReceived(true);
            setMutualSpark(true);
        }else{
            setSparkReceived(false);
            setMutualSpark(false);
        }

        setSelectMatch(messageListData.optBoolean("is_select_match"));


    }

    public static ConversationModal getConversationFromDBCursor(Cursor cursor) {
        ConversationModal c = new ConversationModal();
        c.setMatch_id(cursor.getString(cursor.getColumnIndex("match_id")));

        c.setFname(cursor.getString(cursor.getColumnIndex("fname")));
        c.setAge(cursor.getString(cursor.getColumnIndex("age")));
        c.setMessage_link(cursor.getString(cursor.getColumnIndex("message_link")));
        c.setProfile_pic(cursor.getString(cursor.getColumnIndex("profile_pic")));

        c.setLast_message(cursor.getString(cursor.getColumnIndex("last_message_text")));
        String msgType = cursor.getString(cursor.getColumnIndex("last_message_type"));
        if (Utility.isSet(msgType)) {
            c.setLast_message_type(MessageModal.getMessageTypeFromString(msgType));
        }

        String tstamp = cursor.getString(cursor.getColumnIndex("tstamp"));
        if (Utility.isSet(tstamp)) {
            c.setTstamp(TimeUtils.getParsedTime(tstamp));
        }

        String clear_chat_tstamp = cursor.getString(cursor.getColumnIndex("clear_chat_tstamp"));

        if (Utility.isSet(clear_chat_tstamp)) {
            c.setClear_chat_tstamp(TimeUtils.getParsedTime(clear_chat_tstamp));
        }
        String lastMessageState = cursor.getString(cursor.getColumnIndex("last_message_state"));
        if (Utility.isSet(lastMessageState)) {
            c.setLastMessageState(MessageState.valueOf(lastMessageState));
        } else {
            c.setLastMessageState(MessageState.MATCH_ONLY_NEW);
        }

        if (!Utility.isSet(c.getLast_message()) && c.getLastMessageState() != MessageState.MATCH_ONLY_NEW && c.getLastMessageState() != MessageState.BLOCKED_UNSHOWN) {
            c.setLastMessageState(MessageState.MATCH_ONLY);
        }

        String profileLink = cursor.getString(cursor.getColumnIndex("profile_url"));
        if (Utility.isSet(profileLink)) {
            c.setProfile_link(profileLink);
        }
        String isMissTmString = cursor.getString(cursor.getColumnIndex("is_miss_tm"));
        if (Utility.stringCompare(isMissTmString, "1")) {
            c.setIsMissTm(true);
        }

        c.setSparkReceived(cursor.getInt(cursor.getColumnIndex(ConstantsDB.MESSAGE_METADATA.IS_SPARK_RECEIVED)) == 1);
        c.setMutualSpark(cursor.getInt(cursor.getColumnIndex(ConstantsDB.MESSAGE_METADATA.IS_MUTUAL_SPARK)) == 1);

        c.setSelectMatch(cursor.getInt(cursor.getColumnIndex(ConstantsDB.MESSAGE_METADATA.IS_SELECT_MATCH)) == 1);

        return c;

    }

    public Date getClear_chat_tstamp() {
        return clear_chat_tstamp;
    }

    private void setClear_chat_tstamp(Date clear_chat_tstamp) {
        this.clear_chat_tstamp = clear_chat_tstamp;
    }

    public boolean getIsChatCleared() {
        return isChatCleared;
    }

    public void setIsChatCleared(boolean isChatCleared) {
        this.isChatCleared = isChatCleared;
    }

    public String getFname() {
        return fname;
    }

    private void setFname(String fname) {
        this.fname = fname;
    }

    public String getAge() {
        return age;
    }

    private void setAge(String age) {
        this.age = age;
    }

    public Date getTstamp() {
        return tstamp;
    }

    public void setTstamp(Date tstamp) {
        this.tstamp = tstamp;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    private void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getMatch_id() {
        return match_id;
    }

    private void setMatch_id(String match_id) {
        this.match_id = match_id;
    }

    public String getMessage_link() {
        return message_link;
    }

    public void setMessage_link(String message_link) {
        this.message_link = message_link;
    }

    public String getLast_message() {
        return last_message;
    }

    public void setLast_message(String last_message) {
        this.last_message = last_message;
    }

    public MessageState getLastMessageState() {
        return lastMessageState;
    }

    public void setLastMessageState(MessageState lastMessageState) {
        this.lastMessageState = lastMessageState;
    }

    public MessageType getLast_message_type() {
        return last_message_type;
    }

    private void setLast_message_type(MessageType last_message_type) {
        this.last_message_type = last_message_type;
    }

    public String getProfile_link() {
        return profile_link;
    }

    private void setProfile_link(String profile_link) {
        this.profile_link = profile_link;
    }

    public boolean isMissTm() {
        return isMissTm;
    }

    private void setIsMissTm(boolean isMissTm) {
        this.isMissTm = isMissTm;
    }

    public boolean isSparkReceived() {
        return isSparkReceived;
    }

    public void setSparkReceived(boolean sparkReceived) {
        isSparkReceived = sparkReceived;
    }

    public boolean isMutualSpark() {
        return isMutualSpark;
    }

    public void setMutualSpark(boolean mutualSpark) {
        isMutualSpark = mutualSpark;
    }

    public boolean isSelectMatch() {
        return isSelectMatch;
    }

    public void setSelectMatch(boolean selectMatch) {
        isSelectMatch = selectMatch;
    }
}
