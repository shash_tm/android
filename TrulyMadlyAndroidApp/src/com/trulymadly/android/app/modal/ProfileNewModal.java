package com.trulymadly.android.app.modal;

import java.util.ArrayList;
import java.util.HashMap;

public class ProfileNewModal {
    private UserModal aUser;
    private WorkModal aWork;
    private EducationModal aEducation;
    private String[] interest;
    private TrustBuilderModal trustBuilder;
    private MatchesDetailModal matchesDetailModal;
    private ShareProfileDataModal shareProfileData;
    private MutualConnection[] mutualConnectionArr;
    private ArrayList<FbFriendModal> fbFriendList;
    private int fbMutualFriendCount;
    private String[] commonInterestModal;
    private String[] eventsImages;
    private String[] eventNames;
    private String profileStatus, linkLike, linkHide, linkMsg, likeBtn, msgBtn, hideBtn, fbMutualConCount, locked,
            canUnlock, rigidInterest, profileUrl, user_id, nonMutualLikeMsg, activityDashBoardProfileViews = null,
            activityDashBoardProfileViewsText = null, activityDashBoardPopularityTip = null,
            activityDashBoardClickAction = null;
    private int activityDashBoardPopularity = 0;
    private boolean isLiked, isHide, is_last_tile = false, isActivityDashBoardAvailable = false;
    private boolean isSponsoredProfile = false, isSponsoredEventProfile = false;
    private String mSponsoredEventDetails;
    private String mLandingUrl;
    private ArrayList<String> mImpressionsTrackingUrls;
    private ArrayList<String> mClicksTrackingUrls;
    private boolean isAdProfile = false;
    private boolean photoVisibilityToast;
    //Sparks
    private ArrayList<String> mSparkMessageSuggestions;
    private HashMap<String, ArrayList<FavoriteDataModal>> favoritesDataMap;

    //Select
    private boolean isTMSelectMember;
    private String mSelectCommonString, mSelectCommonImage, mSelectQuote;

    public ProfileNewModal() {
    }

    public String[] getEventNames() {
        return eventNames;
    }

    public void setEventNames(String[] eventNames) {
        this.eventNames = eventNames;
    }

    public int getFbMutualFriendCount() {
        return fbMutualFriendCount;
    }

    public void setFbMutualFriendCount(int fbMutualFriendCount) {
        this.fbMutualFriendCount = fbMutualFriendCount;
    }

    public boolean isPhotoVisibilityToast() {
        return photoVisibilityToast;
    }

    public void setPhotoVisibilityToast(boolean photoVisibilityToast) {
        this.photoVisibilityToast = photoVisibilityToast;
    }

    public String[] getEventsImages() {
        return eventsImages;
    }

    public void setEventsImages(String[] eventsImages) {
        this.eventsImages = eventsImages;
    }

    public String getUserId() {
        return this.user_id;
    }

    public void setUserId(String user_id) {
        this.user_id = user_id;
    }

    public void setIsLastTile(boolean is_last_tile) {
        this.is_last_tile = is_last_tile;
    }

    public boolean isLastTile() {
        return this.is_last_tile;
    }

    public UserModal getUser() {
        return this.aUser;
    }

    public void setUser(UserModal aUser) {
        this.aUser = aUser;
    }

    public WorkModal getWork() {
        return this.aWork;
    }

    public void setWork(WorkModal aWork) {
        this.aWork = aWork;
    }

    public EducationModal getEducation() {
        return this.aEducation;
    }

    public void setEducation(EducationModal aEducation) {
        this.aEducation = aEducation;
    }

    public String[] getInterest() {
        return this.interest;
    }

    public void setInterest(String[] interest) {
        this.interest = interest;
    }

    public TrustBuilderModal getTrustBuilder() {
        return this.trustBuilder;
    }

    public void setTrustBuilder(TrustBuilderModal trustBuilder) {
        this.trustBuilder = trustBuilder;
    }

    public MatchesDetailModal getMatchesDetail() {
        return this.matchesDetailModal;
    }

    public void setMatchesDetail(MatchesDetailModal matchesDetailModal) {
        this.matchesDetailModal = matchesDetailModal;
    }

    public String[] getCommonInterestModal() {
        return this.commonInterestModal;
    }

    public void setCommonInterestModal(String[] commonInterestModal) {
        this.commonInterestModal = commonInterestModal;
    }

    public String getProfileStatus() {
        return this.profileStatus;
    }

    public void setProfileStatus(String profileStatus) {
        this.profileStatus = profileStatus;
    }

    public String getLinkLike() {
        return this.linkLike;
    }

    public void setLinkLike(String linkLike) {
        this.linkLike = linkLike;
    }

    public String getLinkHide() {
        return this.linkHide;
    }

    public void setLinkHide(String linkHide) {
        this.linkHide = linkHide;
    }

    public String getLinkMsg() {
        return this.linkMsg;
    }

    public void setLinkMsg(String linkMsg) {
        this.linkMsg = linkMsg;
    }

    public String getLikeBtn() {
        return this.likeBtn;
    }

    public void setLikeBtn(String likeBtn) {
        this.likeBtn = likeBtn;
    }

    public String getMsgBtn() {
        return this.msgBtn;
    }

    public void setMsgBtn(String msgBtn) {
        this.msgBtn = msgBtn;
    }

    public String getHideBtn() {
        return this.hideBtn;
    }

    public void setHideBtn(String hideBtn) {
        this.hideBtn = hideBtn;
    }

    public String getFBMutualConCount() {
        return this.fbMutualConCount;
    }

    public void setFBMutualConCount(String fbMutualConCount) {
        this.fbMutualConCount = fbMutualConCount;
    }

    public String getLocked() {
        return this.locked;
    }

    public void setLocked(String locked) {
        this.locked = locked;
    }

    public String getCanUnlock() {
        return this.canUnlock;
    }

    public void setCanUnlock(String canUnlock) {
        this.canUnlock = canUnlock;
    }

    public boolean getIsLiked() {
        return this.isLiked;
    }

    public void setIsLiked(boolean isLiked) {
        this.isLiked = isLiked;
    }

    public boolean getIsHide() {
        return this.isHide;
    }

    public void setIsHide(boolean isHide) {
        this.isHide = isHide;
    }

    public MutualConnection[] getMutualConnectionArr() {
        return this.mutualConnectionArr;
    }

    public void setMutualConnectionArr(MutualConnection[] mutualConnectionArr) {
        this.mutualConnectionArr = mutualConnectionArr;
    }

    public String getRigidInterest() {
        return this.rigidInterest;
    }

    public void setRigidInterest(String rigidInterest) {
        this.rigidInterest = rigidInterest;
    }

    public String getProfileUrl() {
        return this.profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    public String getNonMutualLikeMsg() {
        return this.nonMutualLikeMsg;
    }

    public void setNonMutualLikeMsg(String nonMutualLikeMsg) {
        this.nonMutualLikeMsg = nonMutualLikeMsg;
    }

    public String getActivityDashBoardProfileViews() {
        return activityDashBoardProfileViews;
    }

    public void setActivityDashBoardProfileViews(String activityDashBoardProfileViews) {
        this.activityDashBoardProfileViews = activityDashBoardProfileViews;
    }

    public String getActivityDashBoardProfileViewsText() {
        return activityDashBoardProfileViewsText;
    }

    public void setActivityDashBoardProfileViewsText(String activityDashBoardProfileViewsText) {
        this.activityDashBoardProfileViewsText = activityDashBoardProfileViewsText;
    }

    public String getActivityDashBoardPopularityTip() {
        return activityDashBoardPopularityTip;
    }

    public void setActivityDashBoardPopularityTip(String activityDashBoardPopularityTip) {
        this.activityDashBoardPopularityTip = activityDashBoardPopularityTip;
    }

    public int getActivityDashBoardPopularity() {
        return activityDashBoardPopularity;
    }

    public void setActivityDashBoardPopularity(int activityDashBoardPopularity) {
        this.activityDashBoardPopularity = activityDashBoardPopularity;
    }

    public boolean isActivityDashBoardAvailable() {
        return isActivityDashBoardAvailable;
    }

    public void setActivityDashBoardAvailable(boolean isActivityDashBoardAvailable) {
        this.isActivityDashBoardAvailable = isActivityDashBoardAvailable;
    }

    public String getActivityDashBoardClickAction() {
        return activityDashBoardClickAction;
    }

    public void setActivityDashBoardClickAction(String activityDashBoardClickAction) {
        this.activityDashBoardClickAction = activityDashBoardClickAction;
    }

    public ShareProfileDataModal getShareProfileData() {
        return shareProfileData;
    }

    public void setShareProfileData(ShareProfileDataModal shareProfileData) {
        this.shareProfileData = shareProfileData;
    }

    public boolean isSponsoredProfile() {
        return isSponsoredProfile;
    }

    public void setIsSponsoredProfile(boolean isSponsoredProfile) {
        this.isSponsoredProfile = isSponsoredProfile;
    }

    public boolean isSponsoredEventProfile() {
        return isSponsoredEventProfile;
    }

    public void setIsSponsoredEventProfile(boolean sponsoredEventProfile) {
        isSponsoredEventProfile = sponsoredEventProfile;
    }

    public String getmSponsoredEventDetails() {
        return mSponsoredEventDetails;
    }

    public void setmSponsoredEventDetails(String mSponsoredEventDetails) {
        this.mSponsoredEventDetails = mSponsoredEventDetails;
    }

    public String getmLandingUrl() {
        return mLandingUrl;
    }

    public void setmLandingUrl(String mLandingUrl) {
        this.mLandingUrl = mLandingUrl;
    }

    public ArrayList<String> getmImpressionsTrackingUrls() {
        return mImpressionsTrackingUrls;
    }

    public void setmImpressionsTrackingUrls(ArrayList<String> mImpressionsTrackingUrls) {
        this.mImpressionsTrackingUrls = mImpressionsTrackingUrls;
    }

    public ArrayList<String> getmClicksTrackingUrls() {
        return mClicksTrackingUrls;
    }

    public void setmClicksTrackingUrls(ArrayList<String> mClicksTrackingUrls) {
        this.mClicksTrackingUrls = mClicksTrackingUrls;
    }

    public boolean isAdProfile() {
        return isAdProfile;
    }

    public void setIsAdProfile(boolean isAdProfile) {
        this.isAdProfile = isAdProfile;
    }

    public ArrayList<String> getmSparkMessageSuggestions() {
        return mSparkMessageSuggestions;
    }

    public void setmSparkMessageSuggestions(ArrayList<String> mSparkMessageSuggestions) {
        this.mSparkMessageSuggestions = mSparkMessageSuggestions;
    }

    public ArrayList<FbFriendModal> getFbFriendList() {
        return fbFriendList;
    }

    public void setFbFriendList(ArrayList<FbFriendModal> fbFriendList) {
        this.fbFriendList = fbFriendList;
    }

    public HashMap<String, ArrayList<FavoriteDataModal>> getFavoritesDataMap() {
        return favoritesDataMap;
    }

    public void setFavoritesDataMap(HashMap<String, ArrayList<FavoriteDataModal>> favoritesDataMap) {
        this.favoritesDataMap = favoritesDataMap;
    }

    //Select
    public String getmSelectCommonImage() {
        return mSelectCommonImage;
    }

    public void setmSelectCommonImage(String mSelectCommonImage) {
        this.mSelectCommonImage = mSelectCommonImage;
    }

    public String getmSelectCommonString() {
        return mSelectCommonString;
    }

    public void setmSelectCommonString(String mSelectCommonString) {
        this.mSelectCommonString = mSelectCommonString;
    }

    public boolean isTMSelectMember() {
        return isTMSelectMember;
    }

    public void setTMSelectMember(boolean TMSelectMember) {
        isTMSelectMember = TMSelectMember;
    }

    public String getmSelectQuote() {
        return mSelectQuote;
    }

    public void setmSelectQuote(String mSelectQuote) {
        this.mSelectQuote = mSelectQuote;
    }
}
