package com.trulymadly.android.app.modal;

import com.crashlytics.android.Crashlytics;

import org.json.JSONArray;
import org.json.JSONException;

public class Age {

	private String id, name;

	private Age(String _id, String _name) {
		id = _id;
		name = _name;
	}

	public static Age[] getAgeArray() {
		JSONArray ageJson;
		int length = 0;
		try {
			ageJson = new JSONArray(
					"[{\"key\": \"0\",\"value\": \"0\"},{\"key\": \"1\",\"value\": \"1\"},{\"key\": \"2\",\"value\": \"2\"},{\"key\": \"3\",\"value\": \"3\"},{\"key\": \"4\",\"value\": \"4\"},{\"key\": \"5\",\"value\": \"5\"},{\"key\": \"6\",\"value\": \"6\"},{\"key\": \"7\",\"value\": \"7\"},{\"key\": \"8\",\"value\": \"8\"},{\"key\": \"9\",\"value\": \"9\"},{\"key\": \"10\",\"value\": \"10\"},{\"key\": \"11\",\"value\": \"11\"}]");
			if (ageJson != null) {
				length = ageJson.length();
			}
			Age[] age = new Age[length + 1];
			age[0] = new Age("-1", "Age");
			for (int i = 0; i < ageJson.length(); i++) {
				age[i + 1] = new Age(ageJson.optJSONObject(i)
						.optString("key"), ageJson.optJSONObject(i)
						.optString("value"));
			}
			return age;
		} catch (JSONException e) {
			Crashlytics.logException(e);
			Age[] age = new Age[1];
			age[0] = new Age("-1", "Age");
			return age;
		}

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String toString() {
		return (this.name);
	}

}
