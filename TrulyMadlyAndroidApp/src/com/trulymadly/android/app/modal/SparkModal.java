package com.trulymadly.android.app.modal;

import android.content.Context;
import android.database.Cursor;

import com.trulymadly.android.app.json.ConstantsDB;
import com.trulymadly.android.app.utility.TimeUtils;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by avin on 10/05/16.
 */
public class SparkModal {
    private String mUserId, mMatchId, mDesignation, mMessageId, mMessage,
            mTimeStamp, mSortTimeStamp, mHashKey, mCity, mImages;
    private String[] mImagesArray;
    private long mExpiredTimeInSeconds;
    private MatchMessageMetaData mMatchMessageMetaData;
    private SPARK_STATUS mSparkAction;

    private long mStartTime;
//    private boolean isAdded;

    public static SparkModal parseSpark(Cursor cursor) {
        SparkModal sparkModal = null;

        if (cursor != null) {
            sparkModal = new SparkModal();
            sparkModal.setmUserId(cursor.getString(cursor.getColumnIndex(ConstantsDB.SPARKS.USER_ID)));
            sparkModal.setmMatchId(cursor.getString(cursor.getColumnIndex(ConstantsDB.SPARKS.MATCH_ID)));
            sparkModal.setmDesignation(cursor.getString(cursor.getColumnIndex(ConstantsDB.SPARKS.DESIGNATION)));
            sparkModal.setmMessageId(cursor.getString(cursor.getColumnIndex(ConstantsDB.SPARKS.MESSAGE_ID)));
            sparkModal.setmMessage(cursor.getString(cursor.getColumnIndex(ConstantsDB.SPARKS.MESSAGE)));
            sparkModal.setmTimeStamp(cursor.getString(cursor.getColumnIndex(ConstantsDB.SPARKS.TIMESTAMP)));
            sparkModal.setmSortTimeStamp(cursor.getString(cursor.getColumnIndex(ConstantsDB.SPARKS.SORT_TIMESTAMP)));
            sparkModal.setmExpiredTimeInSeconds(cursor.getInt(cursor.getColumnIndex(ConstantsDB.SPARKS.EXPIRE_TIME)));
            sparkModal.setmStartTime(Long.parseLong(cursor.getString(cursor.getColumnIndex(ConstantsDB.SPARKS.START_TIME))));
            sparkModal.setmCity(cursor.getString(cursor.getColumnIndex(ConstantsDB.SPARKS.CITY)));
            sparkModal.setmImages(cursor.getString(cursor.getColumnIndex(ConstantsDB.SPARKS.IMAGES)));
            sparkModal.setmHashKey(cursor.getString(cursor.getColumnIndex(ConstantsDB.SPARKS.HASH)));
        }

        return sparkModal;
    }

    /*
    One sample object
    {
        "sender_id": "492801",
        "create_date": "2016-05-17 10:50:46",
        "status": "sent",
        "message": "aati kya khandala",
        "message_id": "1762223",
        "expiry_time": "300",
        "designation": "Techie",
        "hash": "ae95400cd6606ff960cfd6e46aae3126",
        "fname": "Devesh",
        "age": "23",
        "profile_pic": "https:\/\/djy1s2eqovnmt.cloudfront.net\/files\/images\/profiles\/1459500866768.7_808884247671812736_7466684174.jpg",
        "profile_link": "https:\/\/api.trulymadly.com\/profile.php?pid=6ef739fa5ac56b01dfd3ef4b0e028fef_113406",
        "full_conv_link": "https:\/\/api.trulymadly.com\/msg\/message_full_conv.php?match_id=113406&mesh=6ef739fa5ac56b01dfd3ef4b0e028fef",
        "is_select" : true/false
    }
    * */
    public static SparkModal parseSpark(Context context, JSONObject jsonObject) {
        SparkModal sparkModal = null;

        if (jsonObject != null) {
            sparkModal = new SparkModal();
            sparkModal.setmUserId(Utility.getMyId(context));
            sparkModal.setmMatchId(jsonObject.optString("sender_id"));
            sparkModal.setmDesignation(jsonObject.optString("designation"));
            sparkModal.setmMessageId(jsonObject.optString("message_id"));
            sparkModal.setmMessage(jsonObject.optString("message"));
            sparkModal.setmHashKey(jsonObject.optString("hash"));
            sparkModal.setmTimeStamp(String.valueOf(TimeUtils.getParsedTime(jsonObject.optString("create_date")).getTime()));
            sparkModal.setmSortTimeStamp(sparkModal.getmTimeStamp());
            sparkModal.setmCity(jsonObject.optString("city"));
            JSONArray jsonArray = jsonObject.optJSONArray("images");
            String images = null;
            if (jsonArray != null) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    if (images == null) {
                        images = jsonArray.optString(i);
                    } else {
                        images += "," + jsonArray.optString(i);
                    }
                }
            }
            sparkModal.setmImages(images);
            sparkModal.setmExpiredTimeInSeconds(Integer.parseInt(jsonObject.optString("expiry_time")));
            String status = jsonObject.optString("status");
            long startTime = 0;
            if (Utility.isSet(status) && status.equalsIgnoreCase(SPARK_STATUS.seen.name())) {
                startTime = TimeUtils.getSystemTimeInMiliSeconds();
            }
            sparkModal.setmStartTime(startTime);

            if (sparkModal.getmExpiredTimeInSeconds() > 0) {
                MatchMessageMetaData matchMessageMetaData = new MatchMessageMetaData();
                matchMessageMetaData.generateMatchMessageMetaData(sparkModal.getmUserId(), sparkModal.getmMatchId(),
                        jsonObject.optString("fname"),
                        jsonObject.optString("profile_pic"),
                        jsonObject.optString("profile_link"),
                        jsonObject.optString("full_conv_link"),
                        jsonObject.optString("age"),
                        false, true, false,
                        jsonObject.optBoolean("is_select")
                );
                sparkModal.setmMatchMessageMetaData(matchMessageMetaData);
            } else {
                sparkModal = null;
            }
        }

        return sparkModal;
    }

    public String getmUserId() {
        return mUserId;
    }

    public void setmUserId(String mUserId) {
        this.mUserId = mUserId;
    }

    public String getmMatchId() {
        return mMatchId;
    }

    public void setmMatchId(String mMatchId) {
        this.mMatchId = mMatchId;
    }

    public String getmDesignation() {
        return mDesignation;
    }

    public void setmDesignation(String mDesignation) {
        this.mDesignation = mDesignation;
    }

    public String getmMessageId() {
        return mMessageId;
    }

    public void setmMessageId(String mMessageId) {
        this.mMessageId = mMessageId;
    }

//    public int getmExpireTimeInMinutes() {
//        return mExpireTimeInMinutes;
//    }
//
//    public void setmExpireTimeInMinutes(int mExpireTimeInMinutes) {
//        this.mExpireTimeInMinutes = mExpireTimeInMinutes;
//    }

    public String getmMessage() {
        return mMessage;
    }

    public void setmMessage(String mMessage) {
        this.mMessage = mMessage;
    }

    public long getmStartTime() {
        return mStartTime;
    }

    public void setmStartTime(long mStartTime) {
        this.mStartTime = mStartTime;
    }

    public String getmTimeStamp() {
        return mTimeStamp;
    }

    public void setmTimeStamp(String mTimeStamp) {
        this.mTimeStamp = mTimeStamp;
    }

    public String getmSortTimeStamp() {
        return mSortTimeStamp;
    }

    public void setmSortTimeStamp(String mSortTimeStamp) {
        this.mSortTimeStamp = mSortTimeStamp;
    }

    public MatchMessageMetaData getmMatchMessageMetaData() {
        return mMatchMessageMetaData;
    }

    public void setmMatchMessageMetaData(MatchMessageMetaData mMatchMessageMetaData) {
        this.mMatchMessageMetaData = mMatchMessageMetaData;
    }

    public long getmExpiredTimeInSeconds() {
        return mExpiredTimeInSeconds;
    }

    public void setmExpiredTimeInSeconds(long mExpiredTimeInSeconds) {
        this.mExpiredTimeInSeconds = mExpiredTimeInSeconds;
    }

    public String getmHashKey() {
        return mHashKey;
    }

    public void setmHashKey(String mHashKey) {
        this.mHashKey = mHashKey;
    }

    public SPARK_STATUS getmSparkAction() {
        return mSparkAction;
    }

    public void setmSparkAction(SPARK_STATUS mSparkAction) {
        this.mSparkAction = mSparkAction;
    }

    public String getmCity() {
        return mCity;
    }

    public void setmCity(String mCity) {
        this.mCity = mCity;
    }

    public String getmImages() {
        return mImages;
    }

    public void setmImages(String mImages) {
        this.mImages = mImages;
        if(Utility.isSet(mImages)) {
            mImagesArray = mImages.split(",");
        }
    }

//    public boolean isAdded() {
//        return isAdded;
//    }
//
//    public void setAdded(boolean added) {
//        isAdded = added;
//    }

    public String[] getmImagesArray() {
        return mImagesArray;
    }

    public void setmImagesArray(String[] mImagesArray) {
        this.mImagesArray = mImagesArray;
    }

    public enum SPARK_STATUS {
        seen, accepted, rejected, expired
    }
}
