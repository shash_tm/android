/**
 * 
 */
package com.trulymadly.android.app.modal;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author udbhav
 *
 */
public class LinkedInData {

	private String email, member_id, firstName, lastName, numConnections, linkedin_data;
	// 'trustbuilder', 'dashboard', 'workRegister', 'workLater'
	private String connected_from;
	private String current_designation = null, industry = null;
	private String profile_pic_url = null;

	public static LinkedInData getDataFromNative(JSONObject apiJsonResponse) {

		LinkedInData liData = new LinkedInData();
		if (apiJsonResponse == null) {
			return null;
		}

		liData.setEmail(apiJsonResponse.optString("emailAddress"));
		liData.setMember_id(apiJsonResponse.optString("id"));
		liData.setFirstName(apiJsonResponse.optString("firstName"));
		liData.setLastName(apiJsonResponse.optString("lastName"));
		liData.setNumConnections(apiJsonResponse.optString("numConnections"));

		liData.setConnected_from("trustbuilder");

		if (apiJsonResponse.optJSONObject("positions") != null
				&& apiJsonResponse.optJSONObject("positions").optJSONArray("values") != null
				&& apiJsonResponse.optJSONObject("positions").optJSONArray("values").length() > 0) {
			try {
				liData.setCurrent_designation(apiJsonResponse.optJSONObject("positions").optJSONArray("values")
						.getJSONObject(0).optString("title"));
			} catch (JSONException ignored) {
			}
		}
		liData.setIndustry(apiJsonResponse.optString("industry"));
		if (apiJsonResponse.optJSONObject("pictureUrls") != null
				&& apiJsonResponse.optJSONObject("pictureUrls").optJSONArray("values") != null) {
			liData.setProfile_pic_url(apiJsonResponse.optJSONObject("pictureUrls").optJSONArray("values").optString(0));
		}
		liData.setLinkedin_data(apiJsonResponse.toString());

		return liData;
	}

	public String getProfile_pic_url() {
		return profile_pic_url;
	}

	private void setProfile_pic_url(String profile_pic_url) {
		this.profile_pic_url = profile_pic_url;
	}

	public String getEmail() {
		return email;
	}

	private void setEmail(String email) {
		this.email = email;
	}

	public String getMember_id() {
		return member_id;
	}

	private void setMember_id(String member_id) {
		this.member_id = member_id;
	}

	public String getFirstName() {
		return firstName;
	}

	private void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	private void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getNumConnections() {
		return numConnections;
	}

	private void setNumConnections(String numConnections) {
		this.numConnections = numConnections;
	}

	public String getLinkedin_data() {
		return linkedin_data;
	}

	private void setLinkedin_data(String linkedin_data) {
		this.linkedin_data = linkedin_data;
	}

	public String getConnected_from() {
		return connected_from;
	}

	private void setConnected_from(String connected_from) {
		this.connected_from = connected_from;
	}

	public String getCurrent_designation() {
		return current_designation;
	}

	private void setCurrent_designation(String current_designation) {
		this.current_designation = current_designation;
	}

	public String getIndustry() {
		return industry;
	}

	private void setIndustry(String industry) {
		this.industry = industry;
	}

}
