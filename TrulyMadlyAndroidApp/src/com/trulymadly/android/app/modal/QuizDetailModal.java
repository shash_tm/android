package com.trulymadly.android.app.modal;

public class QuizDetailModal {
	private String userName, matchName, matchImageUrl,userImageUrl;
	private int noOfQuestion = 0;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getMatchName() {
		return matchName;
	}

	public void setMatchName(String matchName) {
		this.matchName = matchName;
	}

	public int getNoOfQuestion() {
		return noOfQuestion;
	}

	public void setNoOfQuestion(int noOfQuestion) {
		this.noOfQuestion = noOfQuestion;
	}

	public String getMatchImageUrl() {
		return matchImageUrl;
	}

	public void setMatchImageUrl(String matchImageUrl) {
		this.matchImageUrl = matchImageUrl;
	}

	public String getUserImageUrl() {
		return userImageUrl;
	}

	public void setUserImageUrl(String userImageUrl) {
		this.userImageUrl = userImageUrl;
	}

	

}
