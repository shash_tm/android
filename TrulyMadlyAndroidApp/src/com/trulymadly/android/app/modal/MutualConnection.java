package com.trulymadly.android.app.modal;

public class MutualConnection {
    private String name;
    private String pic;

    public MutualConnection() {
    }

    public MutualConnection(String name, String pic) {
        this.name = name;
        this.pic = pic;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPic() {
        return this.pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }
}
