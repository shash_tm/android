/**
 *
 */
package com.trulymadly.android.app.modal;

import android.os.Parcel;
import android.os.Parcelable;

import com.trulymadly.android.app.utility.Utility;

import java.util.Locale;

/**
 * @author udbhav
 */
public class NotificationModal implements Parcelable {
    public static final Creator<NotificationModal> CREATOR = new Creator<NotificationModal>() {
        @Override
        public NotificationModal createFromParcel(Parcel in) {
            return new NotificationModal(in);
        }

        @Override
        public NotificationModal[] newArray(int size) {
            return new NotificationModal[size];
        }
    };
    private NotificationType pushType;
    private String title_text = null, pic_url = null, content_text, ticker_text, match_id,
            collapse_key, event_status = null, message_url = null, class_name = null, sub_text = null;
    private boolean isAdmin;

    public NotificationModal(NotificationType pType, String titleText, String contentText, String tickerText, String collapseKey, String className) {
        setPushType(pType);
        setTitle_text(titleText);
        setContent_text(contentText);
        setTicker_text(tickerText);
        setCollapse_key(collapseKey);
        setClass_name(className);
    }


    public NotificationModal(NotificationType pType, String titleText, String contentText, String tickerText, String collapseKey, String matchId, boolean isAdmin, String profilePicUrl) {
        this(pType, titleText, contentText, tickerText, collapseKey, matchId, isAdmin, profilePicUrl, null);

    }

    public NotificationModal(NotificationType pType, String titleText, String contentText, String tickerText, String collapseKey, String matchId, boolean isAdmin, String profilePicUrl, String subtext) {
        setPushType(pType);
        setTitle_text(titleText);
        setContent_text(contentText);
        setTicker_text(tickerText);
        setCollapse_key(collapseKey);
        setMatch_id(matchId);
        setAdmin(isAdmin);
        setPic_url(profilePicUrl);
        setSub_text(subtext);
    }

    private NotificationModal(Parcel in) {
        pushType = in.readParcelable(NotificationType.class.getClassLoader());
        title_text = in.readString();
        pic_url = in.readString();
        content_text = in.readString();
        ticker_text = in.readString();
        sub_text = in.readString();
        match_id = in.readString();
        collapse_key = in.readString();
        event_status = in.readString();
        message_url = in.readString();
        class_name = in.readString();
        isAdmin = in.readByte() != 0;
    }

    public static NotificationType getNotificationTypeFromString(String type) {
        if (!Utility.isSet(type)) {
            return NotificationType.PROMOTE;
        }
        type = type.toUpperCase(Locale.ENGLISH);
        try {
            return NotificationType.valueOf(type);
        } catch (IllegalArgumentException e) {
            return NotificationType.PROMOTE;
        }
    }

    public NotificationType getPushType() {
        return pushType;
    }

    private void setPushType(NotificationType pushType) {
        this.pushType = pushType;
    }

    public String getTitle_text() {
        return title_text;
    }

    public void setTitle_text(String title_text) {
        this.title_text = title_text;
    }

    public String getPic_url() {
        return pic_url;
    }

    public void setPic_url(String pic_url) {
        this.pic_url = pic_url;
    }

    public String getContent_text() {
        return content_text;
    }

    public void setContent_text(String content_text) {
        this.content_text = content_text;
    }

    public String getTicker_text() {
        return ticker_text;
    }

    private void setTicker_text(String ticker_text) {
        this.ticker_text = ticker_text;
    }

    public String getMatch_id() {
        return match_id;
    }

    private void setMatch_id(String match_id) {
        this.match_id = match_id;
    }

    public String getCollapse_key() {
        return collapse_key;
    }

    private void setCollapse_key(String collapse_key) {
        this.collapse_key = collapse_key;
    }

    public String getEvent_status() {
        return event_status;
    }

    public void setEvent_status(String event_status) {
        this.event_status = event_status;
    }

    public String getMessage_url() {
        return message_url;
    }

    public void setMessage_url(String message_url) {
        this.message_url = message_url;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    private void setAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    public String getClass_name() {
        return class_name;
    }

    private void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    /**
     * Describe the kinds of special objects contained in this Parcelable's
     * marshalled representation.
     *
     * @return a bitmask indicating the setString of special object types marshalled
     * by the Parcelable.
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * Flatten this object in to a Parcel.
     *
     * @param dest  The Parcel in which the object should be written.
     * @param flags Additional flags about how the object should be written.
     *              May be 0 or {@link #PARCELABLE_WRITE_RETURN_VALUE}.
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(pushType, flags);
        dest.writeString(title_text);
        dest.writeString(pic_url);
        dest.writeString(content_text);
        dest.writeString(ticker_text);
        dest.writeString(sub_text);
        dest.writeString(match_id);
        dest.writeString(collapse_key);
        dest.writeString(event_status);
        dest.writeString(message_url);
        dest.writeString(class_name);
        dest.writeByte((byte) (isAdmin ? 1 : 0));
    }

    public String getSub_text() {
        return sub_text;
    }

    private void setSub_text(String sub_text) {
        this.sub_text = sub_text;
    }

    public enum NotificationType implements Parcelable {
        MESSAGE, MATCH, PROMOTE, TRUST, VOUCHER, ACTIVITY, SILENT, WEB, QUIZ_REFRESH, MATCHES_IMAGE_MULTI, EVENT, EVENT_LIST, BUY_SPARKS, BUY_SELECT;

        public static final Creator<NotificationType> CREATOR = new Creator<NotificationType>() {
            @Override
            public NotificationType createFromParcel(Parcel in) {
                return NotificationType.values()[in.readInt()];
            }

            @Override
            public NotificationType[] newArray(int size) {
                return new NotificationType[size];
            }
        };

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(ordinal());
        }

        @Override
        public int describeContents() {
            return 0;
        }

    }

    public enum NotificationClickType {
        MATCHES_IMAGE_MULTI_LAUNCH_MATCH, MATCHES_IMAGE_MULTI_ACTION_NEXT, MATCHES_IMAGE_MULTI_ACTION_PREV
    }

}
