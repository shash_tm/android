package com.trulymadly.android.app.modal;

public class QuizImage {
	private String bannerServerLocation;
	private String thumbnailServerLocation;

	public QuizImage(String bannerServerLocation, String thumbnailServerLocation) {
		setBannerServerLocation(bannerServerLocation);
		setThumbnailServerLocation(thumbnailServerLocation);
	}

	public String getBannerServerLocation() {
		return bannerServerLocation;
	}

	private void setBannerServerLocation(String bannerServerLocation) {
		this.bannerServerLocation = bannerServerLocation;
	}

	public String getThumbnailServerLocation() {
		return thumbnailServerLocation;
	}

	private void setThumbnailServerLocation(String thumbnailServerLocation) {
		this.thumbnailServerLocation = thumbnailServerLocation;
	}

}
