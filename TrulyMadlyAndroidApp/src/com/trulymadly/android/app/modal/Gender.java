package com.trulymadly.android.app.modal;

import com.crashlytics.android.Crashlytics;

import org.json.JSONArray;
import org.json.JSONException;

public class Gender {
	private String id, name;

	private Gender(String _id, String _name) {
		id = _id;
		name = _name;
	}

	public static Gender[] getGenderArray() {
		JSONArray genderJson;
		int length = 0;
		try {
			genderJson = new JSONArray(
					"[{\"key\": \"0\",\"value\": \"Male\"},{\"key\": \"1\",\"value\": \"Female\"}]");
			if (genderJson != null) {
				length = genderJson.length();
			}
			Gender[] gender = new Gender[length + 1];
			gender[0] = new Gender("-1", "Gender");
			for (int i = 0; i < genderJson.length(); i++) {
				gender[i + 1] = new Gender(genderJson.optJSONObject(i)
						.optString("key"), genderJson.optJSONObject(i)
						.optString("value"));
			}
			return gender;
		} catch (JSONException e) {
			Crashlytics.logException(e);
			Gender[] gender = new Gender[1];
			gender[0] = new Gender("-1", "gender");
			return gender;
		}

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String toString() {
		return (this.name);
	}

}
