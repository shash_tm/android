package com.trulymadly.android.app.modal;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.trulymadly.android.app.R;
import com.trulymadly.android.app.utility.Utility;

/**
 * Created by avin on 07/04/16.
 */
public class EventChatModal implements Parcelable{
    public static final Creator<EventChatModal> CREATOR = new Creator<EventChatModal>() {
        @Override
        public EventChatModal createFromParcel(Parcel in) {
            return new EventChatModal(in);
        }

        @Override
        public EventChatModal[] newArray(int size) {
            return new EventChatModal[size];
        }
    };
    private String mMessage, mEventId, mName, mImageUrl;

    public EventChatModal(/*String mMessage, */String mEventId, String mName, String mImageUrl) {
//        this.mMessage = mMessage;
        this.mEventId = mEventId;
        this.mName = mName;
        this.mImageUrl = mImageUrl;
    }

    private EventChatModal(Parcel in) {
//        mMessage = in.readString();
        mEventId = in.readString();
        mName = in.readString();
        mImageUrl = in.readString();
    }

    public String getmEventId() {
        return mEventId;
    }

    public void setmEventId(String mEventId) {
        this.mEventId = mEventId;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmImageUrl() {
        return mImageUrl;
    }

    public void setmImageUrl(String mImageUrl) {
        this.mImageUrl = mImageUrl;
    }

//    public String getmMessage() {
//        return mMessage;
//    }
//
//    public void setmMessage(String mMessage) {
//        this.mMessage = mMessage;
//    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
//        dest.writeString(mMessage);
        dest.writeString(mEventId);
        dest.writeString(mName);
        dest.writeString(mImageUrl);
    }

    public String prepareMessage(Context context){
        if(!Utility.isSet(mMessage)){
            mMessage = context.getString(R.string.event_msg_ill_be_at) + " " + mName
                    + context.getString(R.string.event_msg_are_you_interested);
        }

        return mMessage;
    }
}
