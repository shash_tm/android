package com.trulymadly.android.app.modal;

import android.database.Cursor;

import java.io.Serializable;

public class StickerData implements Serializable {

    private static final long serialVersionUID = -5440284164032356438L;
    private int id;
    private int gallery_id;
    private String type;
    private String last_used;


    public StickerData(Cursor cursor) {
        if (cursor == null) {
            return;
        }
        id = cursor.getInt(cursor.getColumnIndex("id"));
        type = cursor.getString(cursor.getColumnIndex("type"));
        if (type.equals("sticker")) {
            gallery_id = cursor.getInt(cursor.getColumnIndex("gallery_id"));
        } else {
            gallery_id = 0;
        }
        last_used = cursor.getString(cursor.getColumnIndex("last_used"));
    }

    public StickerData() {
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getGalleryId() {
        return this.gallery_id;
    }

    public void setGalleryId(int gallery_id) {
        this.gallery_id = gallery_id;
    }


    public String getLastUsed() {
        return last_used;
    }

    public void setLastUsed(String last_used) {
        this.last_used = last_used;
    }

}
