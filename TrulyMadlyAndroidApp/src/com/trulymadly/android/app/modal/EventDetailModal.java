package com.trulymadly.android.app.modal;

import java.util.ArrayList;

/**
 * Created by deveshbatra on 3/18/16.
 */
public class EventDetailModal {

    private String id;
    private String date;
    private String name;
    private String friendlyName;
    private String[] terms;
    private String[] images;
    private ArrayList<UserInfoModal> users;
    private boolean followStatus;
    //    private int locationCount = 0;
    private String location;
    private String address;
    private String phoneNumber;
    private String description;
    private String ticketUrl;
    private String shareUrl;
    private String shareText;
    private String eventTicketText;
    private boolean eventExpired;
    private String eventTicketImage;
    private String multiLocationText;
    private ArrayList<LocationModal> locations;
    private String likeMindedText;
    private int mPrice;
    private String mPriceDesc;
    private String mFBUrl;
    private boolean isTMPresents;
    private boolean isSelectOnly;

    public int getmPrice() {
        return mPrice;
    }

    public void setmPrice(int mPrice) {
        this.mPrice = mPrice;
    }

    public String getmPriceDesc() {
        return mPriceDesc;
    }

    public void setmPriceDesc(String mPriceDesc) {
        this.mPriceDesc = mPriceDesc;
    }

    public String getmFBUrl() {
        return mFBUrl;
    }

    public void setmFBUrl(String mFBUrl) {
        this.mFBUrl = mFBUrl;
    }

    public boolean isTMPresents() {
        return isTMPresents;
    }

    public void setTMPresents(boolean TMPresents) {
        isTMPresents = TMPresents;
    }

    public String getLikeMindedText() {
        return likeMindedText;
    }

    public void setLikeMindedText(String likeMindedText) {
        this.likeMindedText = likeMindedText;
    }

    public String getMultiLocationText() {
        return multiLocationText;
    }

    public void setMultiLocationText(String multiLocationText) {
        this.multiLocationText = multiLocationText;
    }

    public ArrayList<LocationModal> getLocations() {
        return locations;
    }

    public void setLocations(ArrayList<LocationModal> locations) {
        this.locations = locations;
    }

    public String getEventTicketImage() {
        return eventTicketImage;
    }

    public void setEventTicketImage(String eventTicketImage) {
        this.eventTicketImage = eventTicketImage;
    }

    public boolean isEventExpired() {
        return eventExpired;
    }

    public void setEventExpired(boolean eventExpired) {
        this.eventExpired = eventExpired;
    }

    public String getEventTicketText() {
        return eventTicketText;
    }
    public void setEventTicketText(String eventTicketText) {
        this.eventTicketText = eventTicketText;
    }

    public String getTicketUrl() {
        return ticketUrl;
    }

    public void setTicketUrl(String ticketUrl) {
        this.ticketUrl = ticketUrl;
    }

    public ArrayList<UserInfoModal> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<UserInfoModal> users) {
        this.users = users;
    }

    public boolean isFollowStatus() {
        return followStatus;
    }

    public void setFollowStatus(boolean followStatus) {
        this.followStatus = followStatus;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
//    private ArrayList<LocationModal> locations;
//    private String multiLocationText = "";

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public void setFriendlyName(String friendlyName) {
        this.friendlyName = friendlyName;
    }

    public String[] getTerms() {
        return terms;
    }

    public void setTerms(String[] terms) {
        this.terms = terms;
    }

    public String[] getImages() {
        return images;
    }

    public void setImages(String[] images) {
        this.images = images;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getShareUrl() {
        return shareUrl;
    }

    public void setShareUrl(String shareUrl) {
        this.shareUrl = shareUrl;
    }

    public String getShareText() {
        return shareText;
    }

    public void setShareText(String shareText) {
        this.shareText = shareText;
    }

    public boolean isSelectOnly() {
        return isSelectOnly;
    }

    public void setSelectOnly(boolean selectOnly) {
        isSelectOnly = selectOnly;
    }
}
