package com.trulymadly.android.app.modal;

public class QuizStatusModal {

	private int quizId;
	private String status = "NONE";
	private Boolean showFlare;

	public int getQuizId() {
		return quizId;
	}

	public void setQuizId(int quizId) {
		this.quizId = quizId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Boolean getShowFlare() {
		return showFlare;
	}

	public void setShowFlare(Boolean showFlare) {
		this.showFlare = showFlare;
	}

}
