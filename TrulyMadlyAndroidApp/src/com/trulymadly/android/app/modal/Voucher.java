/**
 * 
 */
package com.trulymadly.android.app.modal;

import android.database.Cursor;

import com.trulymadly.android.app.utility.TimeUtils;

import org.json.JSONArray;

import java.util.Date;

/**
 * @author udbhav
 * 
 */
public class Voucher {

	private String voucherName;
	private String voucherPrice;
	private String voucher_code;
	private String user_id;
	private Date expiry;
	private String campaign;
	private String friendly_text;
	private String imageUrl;
	private String displayName;
	private JSONArray terms;

	public Voucher() {
	}

	/**
	 * @param cursor
	 */
	public Voucher(Cursor cursor) {
		setVoucher_code(cursor.getString(0));
		setUser_id(cursor.getString(1));
		setExpiry(TimeUtils.getParsedTime(cursor.getString(2)));
		setCampaign(cursor.getString(3));
		setFriendly_text(cursor.getString(4));
	}

	public String getVoucherPrice() {
		return this.voucherPrice;
	}

	public void setVoucherPrice(String voucherPrice) {
		this.voucherPrice = voucherPrice;
	}

	public String getVoucherName() {
		return this.voucherName;
	}

	public void setVoucherName(String voucherName) {
		this.voucherName = voucherName;
	}

	public String getVoucher_code() {
		return voucher_code;
	}

	private void setVoucher_code(String voucher_code) {
		this.voucher_code = voucher_code;
	}

	public String getUser_id() {
		return user_id;
	}

	private void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public Date getExpiry() {
		return expiry;
	}

	private void setExpiry(Date expiry) {
		this.expiry = expiry;
	}

	public String getCampaign() {
		return campaign;
	}

	private void setCampaign(String campaign) {
		this.campaign = campaign;
	}

	public String getFriendly_text() {
		return friendly_text;
	}

	private void setFriendly_text(String friendly_text) {
		this.friendly_text = friendly_text;
	}

	@Override
	public String toString() {
		return voucher_code + "::" + user_id;
	}

	public JSONArray getTerms() {
		return terms;
	}

	public void setTerms(JSONArray tnc) {
		terms = tnc;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String display_name) {
		displayName = display_name;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imgurl) {
		imageUrl = imgurl;
	}

}
