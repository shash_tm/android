/**
 * 
 */
package com.trulymadly.android.app.modal;

/**
 * @author udbhav
 * 
 */
public class TrustBuilderParamsFacebook {

	private String connectedFrom, accessToken;
	private Boolean fbReimport;

	public TrustBuilderParamsFacebook(String connectedFrom, String accessToken,
			Boolean fbReimport) {
		this.setConnectedFrom(connectedFrom);
		this.setAccessToken(accessToken);
		this.setFbReimport(fbReimport);
	}

	public String getConnectedFrom() {
		return connectedFrom;
	}

	private void setConnectedFrom(String connectedFrom) {
		this.connectedFrom = connectedFrom;
	}

	public String getAccessToken() {
		return accessToken;
	}

	private void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public Boolean getFbReimport() {
		return fbReimport;
	}

	private void setFbReimport(Boolean fbReimport) {
		this.fbReimport = fbReimport;
	}
}
