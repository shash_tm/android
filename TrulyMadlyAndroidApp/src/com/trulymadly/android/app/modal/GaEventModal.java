/**
 * 
 */
package com.trulymadly.android.app.modal;

import com.trulymadly.android.app.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author udbhav
 *
 */
public class GaEventModal {

	private final Map<String, String> gaEvent = new HashMap<>();

	public GaEventModal(String event_category, String event_action,
			String event_label, long time_taken_millis, String gender,
			String city, String age, String userId, String networkClass,
			String ab_prefix) {

		gaEvent.put("event_category", event_category);
		gaEvent.put("event_action", event_action);
		if (Utility.isSet(event_label))
			gaEvent.put("event_label", event_label);
		gaEvent.put("time_taken_millis", time_taken_millis + "");
		if (Utility.isSet(gender))
			gaEvent.put("gender", gender);
		if (Utility.isSet(city))
			gaEvent.put("city", city);
		if (Utility.isSet(age))
			gaEvent.put("age", age);
		if (Utility.isSet(userId))
			gaEvent.put("userId", userId);
		if (Utility.isSet(networkClass))
			gaEvent.put("networkClass", networkClass);
		if (Utility.isSet(ab_prefix))
			gaEvent.put("ab_prefix", ab_prefix);

	}

	public GaEventModal(String jsonString) {
		try {
			JSONObject jsonEvent = new JSONObject(jsonString);
			Iterator<?> keys = jsonEvent.keys();
			while (keys.hasNext()) {
				String key = (String) keys.next();
				gaEvent.put(key, jsonEvent.getString(key));
			}
		} catch (JSONException ignored) {
		}

	}

	public String getJsonStringForEvent() {
		JSONObject jsonEvent = new JSONObject();
		for (String key : gaEvent.keySet()) {
			try {
				jsonEvent.put(key, gaEvent.get(key));
			} catch (JSONException e) {
				return null;
			}
		}

		return jsonEvent.toString();
	}

	public String get(String key) {
		return gaEvent.get(key);
	}

}
