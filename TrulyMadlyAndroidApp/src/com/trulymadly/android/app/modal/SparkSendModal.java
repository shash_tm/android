package com.trulymadly.android.app.modal;

/**
 * Created by avin on 01/06/16.
 */
public class SparkSendModal {
    private String mReceiverId, mUniqueId, mMessage, mUserId, mSparkHash;
    private boolean mHasLikedBefore;

    public String getmReceiverId() {
        return mReceiverId;
    }

    public void setmReceiverId(String mReceiverId) {
        this.mReceiverId = mReceiverId;
    }

    public String getmUniqueId() {
        return mUniqueId;
    }

    public void setmUniqueId(String mUniqueId) {
        this.mUniqueId = mUniqueId;
    }

    public String getmMessage() {
        return mMessage;
    }

    public void setmMessage(String mMessage) {
        this.mMessage = mMessage;
    }

    public String getmUserId() {
        return mUserId;
    }

    public void setmUserId(String mUserId) {
        this.mUserId = mUserId;
    }

    public String getmSparkHash() {
        return mSparkHash;
    }

    public void setmSparkHash(String mSparkHash) {
        this.mSparkHash = mSparkHash;
    }

    public boolean ismHasLikedBefore() {
        return mHasLikedBefore;
    }

    public void setmHasLikedBefore(boolean mHasLikedBefore) {
        this.mHasLikedBefore = mHasLikedBefore;
    }
}
