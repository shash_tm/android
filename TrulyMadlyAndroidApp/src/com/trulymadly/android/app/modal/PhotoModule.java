package com.trulymadly.android.app.modal;

import android.graphics.Bitmap;

class PhotoModule {

	private Bitmap aBitmap;
	private String url;

	public PhotoModule(Bitmap aBitmap, String url){
		this.aBitmap=aBitmap;
		this.url=url;
	}

	public Bitmap getBitmap(){
		return aBitmap;
	}

	public void setBitmap(Bitmap aBitmap) {
		this.aBitmap = aBitmap;
	}

	public String getUrl(){
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}