package com.trulymadly.android.app.modal;

public class MatchesDetailModal {
    private String valuesData;
    private String adaptabilityData;
    private String beliefsData;

    public MatchesDetailModal() {
    }

    public MatchesDetailModal(String valuesData, String adaptabilityData, String beliefsData) {
        this.valuesData = valuesData;
        this.adaptabilityData = adaptabilityData;
        this.beliefsData = beliefsData;
    }

    public String getValuesData() {
        return this.valuesData;
    }

    public void setValuesData(String valuesData) {
        this.valuesData = valuesData;
    }

    public String getAdaptabilityData() {
        return this.adaptabilityData;
    }

    public void setAdaptabilityData(String adaptabilityData) {
        this.adaptabilityData = adaptabilityData;
    }

    public String getBeliefsData() {
        return this.beliefsData;
    }

    public void setBeliefsData(String beliefsData) {
        this.beliefsData = beliefsData;
    }
}