/**
 * 
 */
package com.trulymadly.android.app.modal;

import android.content.res.Resources;

import com.trulymadly.android.app.R;

import java.util.ArrayList;

/**
 * @author udbhav
 * 
 */
public class Degree {
	private String id, name;

	private Degree(String _id, String _name) {
		id = _id;
		name = _name;
	}

	private Degree(int _id, String _name) {
		id = _id + "";
		name = _name;
	}

	public static Degree[] getDegrees(
			ArrayList<EditPrefBasicDataModal> highestDegreesList,
			Resources aResources) {
		int size = 0;
		if (highestDegreesList != null) {
			size = highestDegreesList.size();
		}
		Degree[] degrees = new Degree[size + 1];
		degrees[0] = new Degree("0",
				aResources.getString(R.string.highest_degree));
		for (int i = 0; i < size; i++) {
			degrees[i + 1] = new Degree(highestDegreesList.get(i).getDegreeId(),
					highestDegreesList.get(i).getName());
		}
		return degrees;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String toString() {
		return (this.name);
	}

}
