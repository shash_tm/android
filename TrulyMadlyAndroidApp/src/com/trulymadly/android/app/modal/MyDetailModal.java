package com.trulymadly.android.app.modal;

import android.content.Context;

import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.Utility;

import java.io.Serializable;

public class MyDetailModal implements Serializable {

	private static final long serialVersionUID = 3595982452197997731L;
	private String name;
	private String status;
	private String age;
	private boolean isMale;
	private String city;
	private String profile_pic_url, profile_pic_full_url;
	private int conversation_count;
	private String nonMutualLikeMsg;
	private int trustMeterPercentFacebook = 30, trustMeterPercentLinkedin = 15,
			trustMeterPercentPhotoId = 30, trustMeterPercentPhoneNo = 10,
			trustMeterPercentEndorsement = 30;

	private boolean isFromMatch;

	private String mFavoriteFilled, mInterpersonalFilled, mValuesFilled;

	public MyDetailModal() {
	}

	public String getmFavoriteFilled() {
		return mFavoriteFilled;
	}

	public void setmFavoriteFilled(String mFavoriteFilled) {
		this.mFavoriteFilled = mFavoriteFilled;
	}

	public String getmInterpersonalFilled() {
		return mInterpersonalFilled;
	}

	public void setmInterpersonalFilled(String mInterpersonalFilled) {
		this.mInterpersonalFilled = mInterpersonalFilled;
	}

	public String getmValuesFilled() {
		return mValuesFilled;
	}

	public void setmValuesFilled(String mValuesFilled) {
		this.mValuesFilled = mValuesFilled;
	}

	public boolean getIsMale() {
		return this.isMale;
	}

	public void setIsMale(boolean isMale) {
		this.isMale = isMale;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean getFavoriteFilled(Context context) {

		// added by shashwat
		String values = SPHandler.getString(context,
				"matches_favourite_filled");
		return Utility.isSet(values) && values.equals("true");
	}

	public boolean getValuesFilled(Context context) {
		// added by shashwat we should return this from the Shared Preference
		String values = SPHandler.getString(context,
				"matches_values_filled");
		return Utility.isSet(values) && values.equals("true");
		// return this.values_filled;
	}

	public boolean getInterpersonalFilled(Context context) {
		// added by shashwat
		String values = SPHandler.getString(context,
				"matches_interpersonal_filled");
		return Utility.isSet(values) && values.equals("true");
		// return this.interpersonal_filled;
	}

	public String getNonMutualLikeMsg() {
		return this.nonMutualLikeMsg;
	}

	public void setNonMutualLikeMsg(String nonMutualLikeMsg) {
		this.nonMutualLikeMsg = nonMutualLikeMsg;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getProfile_pic_url() {
		return profile_pic_url;
	}

	public void setProfile_pic_url(String profile_pic_url) {
		this.profile_pic_url = profile_pic_url;
	}

	public boolean getIsFromMatch() {
		return this.isFromMatch;
	}

	public void setIsFromMatch(boolean isFromMatch) {
		this.isFromMatch = isFromMatch;
	}

	public int getConversation_count() {
		return conversation_count;
	}

	public void setConversation_count(int conversation_count) {
		this.conversation_count = conversation_count;
	}

	public int getTrustMeterPercentFacebook() {
		return trustMeterPercentFacebook;
	}

	public void setTrustMeterPercentFacebook(int trustMeterPercentFacebook) {
		this.trustMeterPercentFacebook = trustMeterPercentFacebook;
	}

	public int getTrustMeterPercentLinkedin() {
		return trustMeterPercentLinkedin;
	}

	public void setTrustMeterPercentLinkedin(int trustMeterPercentLinkedin) {
		this.trustMeterPercentLinkedin = trustMeterPercentLinkedin;
	}

	public int getTrustMeterPercentPhotoId() {
		return trustMeterPercentPhotoId;
	}

	public void setTrustMeterPercentPhotoId(int trustMeterPercentPhotoId) {
		this.trustMeterPercentPhotoId = trustMeterPercentPhotoId;
	}

	public int getTrustMeterPercentPhoneNo() {
		return trustMeterPercentPhoneNo;
	}

	public void setTrustMeterPercentPhoneNo(int trustMeterPercentPhoneNo) {
		this.trustMeterPercentPhoneNo = trustMeterPercentPhoneNo;
	}

	public int getTrustMeterPercentEndorsement() {
		return trustMeterPercentEndorsement;
	}

	public void setTrustMeterPercentEndorsement(int trustMeterPercentEndorsement) {
		this.trustMeterPercentEndorsement = trustMeterPercentEndorsement;
	}

	public String getProfile_pic_full_url() {
		return profile_pic_full_url;
	}

	public void setProfile_pic_full_url(String profile_pic_full_url) {
		this.profile_pic_full_url = profile_pic_full_url;
	}

}