/**
 * 
 */
package com.trulymadly.android.app.modal;

/**
 * @author udbhav
 *
 */
public class BlockReason {

	private String reason;
	private boolean isEditableNeeded;

	public BlockReason(String reason, boolean isEditable) {
		this.setReason(reason);
		this.setEditable(isEditable);
	}

	public String getReason() {
		return reason;
	}

	private void setReason(String reason) {
		this.reason = reason;
	}

	public boolean isEditableNeeded() {
		return isEditableNeeded;
	}

	private void setEditable(boolean isEditableNeeded) {
		this.isEditableNeeded = isEditableNeeded;
	}

}
