package com.trulymadly.android.app.modal;

import android.os.Parcel;
import android.os.Parcelable;

public class Photos implements Parcelable {

    public static final Creator<Photos> CREATOR = new Creator<Photos>() {
        @Override
        public Photos createFromParcel(Parcel in) {
            return new Photos(in);
        }

        @Override
        public Photos[] newArray(int size) {
            return new Photos[size];
        }
    };
    private String photo_id;
    private String name;
    private String thumbnail;
    private Boolean is_profile;
    private String status;
    private Boolean admin_approved;
    private Boolean can_profile;
    private boolean isVideo;
    private int duration;
    private String encodedUrl;
    private String video_id;
    private boolean thumbnailOnDisk, isMuted;
    private String encodingStatus;

    public Photos() {
    }

    public Photos(String photo_id, String name, String thumbnail,
                  String is_profile, String status, String admin_approved, String can_profile) {
        this.photo_id = photo_id;
        this.name = name;
        this.thumbnail = thumbnail;
        this.is_profile = is_profile.equalsIgnoreCase("yes");
        this.status = status;
        this.admin_approved = admin_approved.equalsIgnoreCase("yes");
        this.can_profile = (can_profile == null || !can_profile.equalsIgnoreCase("no"));
    }

    protected Photos(Parcel in) {
        photo_id = in.readString();
        name = in.readString();
        thumbnail = in.readString();
        status = in.readString();
        isVideo = in.readByte() != 0;
        duration = in.readInt();
        encodedUrl = in.readString();
        video_id = in.readString();
        thumbnailOnDisk = in.readByte() != 0;
        encodingStatus = in.readString();
        isMuted = in.readByte() != 0;
    }

    public String getVideo_id() {
        return video_id;
    }

    public void setVideo_id(String video_id) {
        this.video_id = video_id;
    }

    public String getPhotoID() {
        return this.photo_id;
    }

    public void setPhotoID(String photo_id) {
        this.photo_id = photo_id;
    }

    public boolean isMuted() {
        return isMuted;
    }

    public void setMuted(boolean muted) {
        isMuted = muted;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getThumbnail() {
        return this.thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Boolean getIsProfile() {
        return this.is_profile;
    }

    public void setIsProfile(Boolean is_profile) {
        this.is_profile = is_profile;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getAdmin_approved() {
        return admin_approved;
    }

    public void setAdmin_approved(Boolean admin_approved) {
        this.admin_approved = admin_approved;
    }

    public Boolean getCan_profile() {
        return can_profile;
    }

    public void setCan_profile(Boolean can_profile) {
        this.can_profile = can_profile;
    }

    public boolean isVideo() {
        return isVideo;
    }

    public void setVideo(boolean video) {
        isVideo = video;
    }

    public String getEncodedUrl() {
        return encodedUrl;
    }

    public void setEncodedUrl(String encodedUrl) {
        this.encodedUrl = encodedUrl;
    }

    public boolean isThumbnailOnDisk() {
        return thumbnailOnDisk;
    }

    public void setThumbnailOnDisk(boolean thumbnailOnDisk) {
        this.thumbnailOnDisk = thumbnailOnDisk;
    }

    public String getEncodingStatus() {
        return encodingStatus;
    }

    public void setEncodingStatus(String encodingStatus) {
        this.encodingStatus = encodingStatus;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Boolean getIs_profile() {
        return is_profile;
    }

    public void setIs_profile(Boolean is_profile) {
        this.is_profile = is_profile;
    }

    public String getPhoto_id() {
        return photo_id;
    }

    public void setPhoto_id(String photo_id) {
        this.photo_id = photo_id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(photo_id);
        parcel.writeString(name);
        parcel.writeString(thumbnail);
        parcel.writeString(status);
        parcel.writeByte((byte) (isVideo ? 1 : 0));
        parcel.writeInt(duration);
        parcel.writeString(encodedUrl);
        parcel.writeString(video_id);
        parcel.writeByte((byte) (thumbnailOnDisk ? 1 : 0));
        parcel.writeString(encodingStatus);
        parcel.writeByte((byte) (isMuted ? 1 : 0));
    }
}
