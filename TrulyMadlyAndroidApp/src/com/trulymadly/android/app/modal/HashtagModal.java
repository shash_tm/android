package com.trulymadly.android.app.modal;

/**
 * Created by deveshbatra on 5/10/16.
 */
public class HashtagModal {
    private String name;
    private boolean isSelected;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
