package com.trulymadly.android.app.modal;

public class FacebookPhotoItem{
	private String pictureUrl;
	private String sourceUrl;
	private String height;
	private String width;

	public FacebookPhotoItem(String pictureUrl,String sourceUrl, String height, String width){
		this.pictureUrl=pictureUrl;
		this.sourceUrl=sourceUrl;
		this.height=height;
		this.width=width;
	}
	public String getHeight(){
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public String getWidth(){
		return width;
	}

	public void setWidth(String width) {
		this.width = width;
	}

	public String getPictureUrl(){
		return pictureUrl;
	}

	public void setPictureUrl(String pictureUrl){
		//fix for Android 2.3
		CharSequence target="\\/";
		CharSequence replace="/";
		this.pictureUrl = pictureUrl.replace(target, replace);
	}

	public String getSourceUrl(){
		return sourceUrl;
	}

	public void setSourceUrl(String sourceUrl){
		this.sourceUrl=sourceUrl;
	}
}