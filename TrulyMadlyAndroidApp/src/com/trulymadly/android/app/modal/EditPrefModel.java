
package com.trulymadly.android.app.modal;

public class EditPrefModel {
	private String name;
	private int id;
	private boolean selected;
	private String strId;

	public EditPrefModel(){}
	public EditPrefModel(String name, int id, boolean selected){
		this.name=name;
		this.id=id;
		this.selected=selected;
	}
	
	public EditPrefModel(String name,String strId,boolean selected){
		this.name=name;
		this.selected=selected;
		this.strId=strId;
	}
	
	public void setName(String name){
		this.name=name;
	}
	public void setID(int id){
		this.id=id;
	}
	public void setSelected(boolean selected){
		this.selected=selected;
	}
	public void setStrID(String strId){
		this.strId=strId;
	}
	
	public String getName(){
		return this.name;
	}
	public int getID(){
		return this.id;
	}
	public boolean isSelected(){
		return this.selected;
	}
	public String getStrID(){
		return this.strId;
	}
}

