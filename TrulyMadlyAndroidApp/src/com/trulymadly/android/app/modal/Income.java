/**
 * 
 */
package com.trulymadly.android.app.modal;

import android.content.res.Resources;

import com.trulymadly.android.app.R;

import java.util.ArrayList;

/**
 * @author udbhav
 * 
 */
public class Income {
	private String id, name;

	private Income(String _id, String _name) {
		id = _id;
		name = _name;
	}

	public static Income[] getIncomes(ArrayList<EditPrefBasicDataModal> incomeList, Resources aResources) {

		int size = incomeList != null ? incomeList.size() : 0;
		Income[] incomes = new Income[size + 2];
		incomes[0] = new Income("0", aResources.getString(R.string.select_income));
		incomes[1] = new Income("1", aResources.getString(R.string.not_applicable));
		for (int i = 0; i < size; i++) {
			incomes[i + 2] = new Income(incomeList.get(i).getIDorCountryID(), incomeList.get(i).getName());
		}
		return incomes;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String toString() {
		return (this.name);
	}

}
