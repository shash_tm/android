package com.trulymadly.android.app.modal;

public class QuestionDetailModal {
	private String questionText, optionType;
	private String matchOptionText, matchOptionImageUrl;
	private String userOptionText, userOptionImageUrl;
	
	public String getQuestionText() {
		return questionText;
	}
	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}
	public String getOptionType() {
		return optionType;
	}
	public void setOptionType(String optionType) {
		this.optionType = optionType;
	}
	public String getMatchOptionText() {
		return matchOptionText;
	}
	public void setMatchOptionText(String matchOptionText) {
		this.matchOptionText = matchOptionText;
	}
	public String getMatchOptionImageUrl() {
		return matchOptionImageUrl;
	}
	public void setMatchOptionImageUrl(String matchOptionImageUrl) {
		this.matchOptionImageUrl = matchOptionImageUrl;
	}
	public String getUserOptionText() {
		return userOptionText;
	}
	public void setUserOptionText(String userOptionText) {
		this.userOptionText = userOptionText;
	}
	public String getUserOptionImageUrl() {
		return userOptionImageUrl;
	}
	public void setUserOptionImageUrl(String userOptionImageUrl) {
		this.userOptionImageUrl = userOptionImageUrl;
	}



}
