package com.trulymadly.android.app.modal;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by avin on 27/10/16.
 */

public class SelectModePackageModal implements Parcelable {
    public static final Creator<SelectModePackageModal> CREATOR = new Creator<SelectModePackageModal>() {
        @Override
        public SelectModePackageModal createFromParcel(Parcel in) {
            return new SelectModePackageModal(in);
        }

        @Override
        public SelectModePackageModal[] newArray(int size) {
            return new SelectModePackageModal[size];
        }
    };
    private boolean isFreeTrial, isQuizAbOn;
    private ArrayList<SelectPackageModal> mSelectPackageModals;
    private String mFreeTrialCTASubText, mFreeTrialCtaText;

    public SelectModePackageModal(boolean isFreeTrial, boolean isQuizAbOn,
                                  ArrayList<SelectPackageModal> mSelectPackageModals) {
        this.isFreeTrial = isFreeTrial;
        this.isQuizAbOn = isQuizAbOn;
        this.mSelectPackageModals = mSelectPackageModals;
    }

    protected SelectModePackageModal(Parcel in) {
        isFreeTrial = in.readByte() != 0;
        isQuizAbOn = in.readByte() != 0;
        mSelectPackageModals = in.createTypedArrayList(SelectPackageModal.CREATOR);
        mFreeTrialCtaText = in.readString();
        mFreeTrialCTASubText = in.readString();
    }

    public boolean isFreeTrial() {
        return isFreeTrial;
    }

    public void setFreeTrial(boolean freeTrial) {
        isFreeTrial = freeTrial;
    }

    public String getmFreeTrialCtaText() {
        return mFreeTrialCtaText;
    }

    public void setmFreeTrialCtaText(String mFreeTrialCtaText) {
        this.mFreeTrialCtaText = mFreeTrialCtaText;
    }

    public String getmFreeTrialCTASubText() {
        return mFreeTrialCTASubText;
    }

    public void setmFreeTrialCTASubText(String mFreeTrialCTASubText) {
        this.mFreeTrialCTASubText = mFreeTrialCTASubText;
    }

    public boolean isQuizAbOn() {
        return isQuizAbOn;
    }

    public ArrayList<SelectPackageModal> getmSelectPackageModals() {
        return mSelectPackageModals;
    }

    public void setmSelectPackageModals(ArrayList<SelectPackageModal> mSelectPackageModals) {
        this.mSelectPackageModals = mSelectPackageModals;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByte((byte) (isFreeTrial ? 1 : 0));
        parcel.writeByte((byte) (isQuizAbOn ? 1 : 0));
        parcel.writeTypedList(mSelectPackageModals);
        parcel.writeString(mFreeTrialCtaText);
        parcel.writeString(mFreeTrialCTASubText);
    }
}
