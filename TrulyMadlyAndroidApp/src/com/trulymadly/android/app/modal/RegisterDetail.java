package com.trulymadly.android.app.modal;

public class RegisterDetail {

	private String email;
	private String password;
	private String push_registration;
	private String fb_token;
	private boolean isFromFB;
	private String gender;
	private String mobile;
	private String firstName;
	private String lastName;
	private int dob_d, dob_m, dob_y;

	public RegisterDetail() {
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPush_registration() {
		return push_registration;
	}

	public void setPush_registration(String push_registration) {
		this.push_registration = push_registration;
	}

	public String getFb_token() {
		return fb_token;
	}

	public void setFb_token(String fb_token) {
		this.fb_token = fb_token;
	}

	public boolean isFromFB() {
		return isFromFB;
	}

	public void setFromFB(boolean isFromFB) {
		this.isFromFB = isFromFB;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getDob_d() {
		return dob_d;
	}

	public void setDob_d(int dob_d) {
		this.dob_d = dob_d;
	}

	public int getDob_m() {
		return dob_m;
	}

	public void setDob_m(int dob_m) {
		this.dob_m = dob_m;
	}

	public int getDob_y() {
		return dob_y;
	}

	public void setDob_y(int dob_y) {
		this.dob_y = dob_y;
	}

}
