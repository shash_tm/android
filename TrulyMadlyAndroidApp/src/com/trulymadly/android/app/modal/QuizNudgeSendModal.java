/**
 * 
 */
package com.trulymadly.android.app.modal;

import com.trulymadly.android.app.MessageModal.MessageType;

/**
 * @author udbhav
 *
 */
public class QuizNudgeSendModal {
	private String matchId, quizName, message, full_conv_link;
	private int quizId;
	private MessageType nudgeType;

	public QuizNudgeSendModal(String matchId, int quizId, String quizName, MessageType nudgeType, String fullconvlink) {
		setMatchId(matchId);
		setQuizId(quizId);
		setNudgeType(nudgeType);
		setQuizName(quizName);
		setFull_conv_link(fullconvlink);
		switch (nudgeType) {
		case NUDGE_DOUBLE:
			setMessage("Let's check our " + quizName + " quotient");
			//setMessage(ctx.getResources().getString(R.string.check_answers));
			break;
		case NUDGE_SINGLE:
		default:
			setMessage("Tap to play " + quizName + ".");
			//setMessage(ctx.getResources().getString(R.string.lets_play_the_quiz_together));
			break;
		}
		
	}

	public String getMatchId() {
		return matchId;
	}

	private void setMatchId(String matchId) {
		this.matchId = matchId;
	}

	public int getQuizId() {
		return quizId;
	}

	private void setQuizId(int quizId) {
		this.quizId = quizId;
	}

	public MessageType getNudgeType() {
		return nudgeType;
	}

	private void setNudgeType(MessageType nudgeType) {
		this.nudgeType = nudgeType;
	}

	public String getQuizName() {
		return quizName;
	}

	private void setQuizName(String quizName) {
		this.quizName = quizName;
	}

	public String getMessage() {
		return message;
	}

	private void setMessage(String message) {
		this.message = message;
	}

	public String getFull_conv_link() {
		return full_conv_link;
	}

	private void setFull_conv_link(String full_conv_link) {
		this.full_conv_link = full_conv_link;
	}

}
