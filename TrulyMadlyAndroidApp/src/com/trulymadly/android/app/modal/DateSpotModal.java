package com.trulymadly.android.app.modal;

import android.content.Context;

import com.trulymadly.android.app.R;
import com.trulymadly.android.app.utility.Utility;

import java.util.ArrayList;

/**
 * Created by deveshbatra on 12/22/15.
 */
public class DateSpotModal {


    private String id;
    private String communicationImage;
    private String communicationText;
    private String name;
    private String friendlyName;
    private String location;
    private String offer;
    private String recommendation;
    private String phoneNo;
    private String address;

    private String dealStatus;
    private String imageProfile;
    private int pricingValue;
    private String[] images;
    private String[] hashtags;
    private String[] menuImages;
    private String[] terms;
    private DateSpotDealType dealType;
    private ArrayList<LocationModal> locations;
    private int locationCount = 0;
    private String multiLocationText = "";
    private boolean isNew = false;
    // 0 for normal
    // 1 for miss_tm - show miss tm recommends
    // 2 for boosted
    private int special_code = 0;
    private String distance;

    //For event info tracking
    private String locationScore, popularityScore, totalScore, rank, zone_id, geo_location;



    public ArrayList<LocationModal> getLocations() {
        return locations;
    }

    public void setLocations(ArrayList<LocationModal> locations) {
        this.locations = locations;
        setLocationCount(locations != null ? locations.size() : 0);
    }

    public String[] getImages() {
        return images;
    }

    public void setImages(String[] images) {
        this.images = images;
    }

    public String[] getHashtags() {
        return hashtags;
    }

    public void setHashtags(String[] hashtags) {
        this.hashtags = hashtags;
    }

    public String[] getMenuImages() {
        return menuImages;
    }

    public void setMenuImages(String[] menuImages) {
        this.menuImages = menuImages;
    }

    public String[] getTerms() {
        return terms;
    }

    public void setTerms(String[] terms) {
        this.terms = terms;
    }


    public String getDealStatus() {
        return dealStatus;
    }

    public void setDealStatus(String dealStatus) {
        this.dealStatus = dealStatus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public void setFriendlyName(String friendlyName) {
        this.friendlyName = friendlyName;
    }

    public String getLocation() {
        return locationCount > 1 ? multiLocationText : location;
    }

    public void setLocation(String location) {
        this.location = location;
    }


    public String getOffer() {
        return offer;
    }

    public void setOffer(String offer) {
        this.offer = offer;
    }

    public String getRecommendation() {
        return recommendation;
    }

    public void setRecommendation(String recommendation) {
        this.recommendation = recommendation;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getImageProfile() {
        return imageProfile;
    }

    public void setImageProfile(String imageProfile) {
        this.imageProfile = imageProfile;
    }

    public int getPricingValue() {
        return pricingValue;
    }

    public void setPricingValue(int pricingValue) {
        this.pricingValue = pricingValue;
    }

    public DateSpotDealType getDealType() {
        return dealType;
    }

    public void setDealType(DateSpotDealType dealType) {
        this.dealType = dealType;
    }

    public String getCommunicationImage() {
        return communicationImage;
    }

    public void setCommunicationImage(String communicationImage) {
        this.communicationImage = communicationImage;
    }

    public String getCommunicationText() {
        return communicationText;
    }

    public void setCommunicationText(String communicationText) {
        this.communicationText = communicationText;
    }

    public String getDefaultCommunicationText(Context ctx) {
        String defaultText = null;

        String locationString = getLocationCount() == 0 ? " " + Utility.toCamelCase(getLocation()) : "";
        if (Utility.isSet(getName()) && Utility.isSet(getFriendlyName())) {
            if (!Utility.isSet(dealStatus) || dealStatus.equalsIgnoreCase("ask_him")) {
                defaultText = ctx.getResources().getString(R.string.ask_out_message);
                defaultText += " " + getName() + locationString + "?";
            } else if (dealStatus.equalsIgnoreCase("ask_her")) {
                defaultText = ctx.getResources().getString(R.string.ask_out_message);
                defaultText += " " + getName() + locationString + "?";
            } else if (dealStatus.equalsIgnoreCase("lets_go")) {
                defaultText = ctx.getResources().getString(R.string.voucher_chat_window_message);
                defaultText += " " + getName() + ". " + ctx.getResources().getString(R.string.enjoy);
            }
        }
        return defaultText;
    }

    public int getLocationCount() {
        return locationCount;
    }

    public void setLocationCount(int locationCount) {
        this.locationCount = locationCount;
    }

    public String getMultiLocationText() {
        return multiLocationText;
    }

    public void setMultiLocationText(String multiLocationText) {
        this.multiLocationText = multiLocationText;
    }

    public boolean isNew() {
        return isNew;
    }

    public void setNew(boolean aNew) {
        isNew = aNew;
    }

    public int getSpecial_code() {
        return special_code;
    }

    public void setSpecial_code(int special_code) {
        this.special_code = special_code;
    }

    public boolean isMissTmRecommends() {
        return special_code == 1;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getLocationScore() {
        return locationScore;
    }

    public void setLocationScore(String locationScore) {
        this.locationScore = locationScore;
    }

    public String getPopularityScore() {
        return popularityScore;
    }

    public void setPopularityScore(String popularityScore) {
        this.popularityScore = popularityScore;
    }

    public String getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(String totalScore) {
        this.totalScore = totalScore;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getZone_id() {
        return zone_id;
    }

    public void setZone_id(String zone_id) {
        this.zone_id = zone_id;
    }

    public String getGeo_location() {
        return geo_location;
    }

    public void setGeo_location(String geo_location) {
        this.geo_location = geo_location;
    }


}
