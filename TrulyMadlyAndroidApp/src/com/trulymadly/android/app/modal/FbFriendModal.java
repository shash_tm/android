package com.trulymadly.android.app.modal;

/**
 * Created by deveshbatra on 6/23/16.
 */
public class FbFriendModal {
    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    String thumbnail , name;
}
