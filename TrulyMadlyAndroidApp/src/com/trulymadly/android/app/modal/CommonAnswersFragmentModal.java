package com.trulymadly.android.app.modal;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

public class CommonAnswersFragmentModal implements Serializable {
	private static final long serialVersionUID = -1567758251370173492L;
	private String matchID;
	private String responseJsonString;

	public String getMatchID() {
		return matchID;
	}

	public void setMatchID(String matchID) {
		this.matchID = matchID;
	}

	public JSONObject getResponse() {
		try {
			return new JSONObject(responseJsonString);
		} catch (JSONException e) {
			return new JSONObject();
		} 
	}

	public void setResponse(JSONObject response) {
		this.responseJsonString = response.toString();
	}

}
