package com.trulymadly.android.app.modal;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by avin on 27/10/16.
 */

public class SelectPackageModal implements Parcelable {
    public static final Creator<SelectPackageModal> CREATOR = new Creator<SelectPackageModal>() {
        @Override
        public SelectPackageModal createFromParcel(Parcel in) {
            return new SelectPackageModal(in);
        }

        @Override
        public SelectPackageModal[] newArray(int size) {
            return new SelectPackageModal[size];
        }
    };
    private String mSku, mTag, mPerUnit, mSubText, mFreeTrialExpiryText, mFreeTrialCtaText;
    private boolean isPopular;
    private int mPrice, mPricePerUnit, mDurationInDays;

    private String mTitle, mDescription;
    private boolean isMatchGauranteed;
    private int mDiscount;

    public SelectPackageModal(String mSku, String mTag, boolean isPopular, int mPrice, int mPricePerUnit,
                              int mDurationInDays, String perUnit, String subText,
                              String freeTrialCtaText, String mFreeTrialExpiryText, String title,
                              boolean matchGaurantee, int mDiscount, String mDescription) {
        this.mSku = mSku;
        this.mTag = mTag;
        this.isPopular = isPopular;
        this.mPrice = mPrice;

        //Second iteration changes
        this.mPricePerUnit = mPricePerUnit;
        this.mPerUnit = perUnit;
        this.mDurationInDays = mDurationInDays;
        this.mSubText = subText;
        this.mFreeTrialExpiryText = mFreeTrialExpiryText;
        this.mFreeTrialCtaText = freeTrialCtaText;

        //Third iteration changes
        this.mTitle = title;
        this.isMatchGauranteed = matchGaurantee;
        this.mDiscount = mDiscount;
        this.mDescription = mDescription;
    }

    protected SelectPackageModal(Parcel in) {
        mSku = in.readString();
        mTag = in.readString();
        isPopular = in.readByte() != 0;
        mPrice = in.readInt();
        mPricePerUnit = in.readInt();
        mDurationInDays = in.readInt();
        mFreeTrialCtaText = in.readString();
        mFreeTrialExpiryText = in.readString();
        mTitle = in.readString();
        isMatchGauranteed = in.readByte() != 0;
        mDiscount = in.readInt();
        mDescription = in.readString();
    }

    public String getmSku() {
        return mSku;
    }

    public String getmTag() {
        return mTag;
    }

    public boolean isPopular() {
        return isPopular;
    }

    public int getmPrice() {
        return mPrice;
    }

    public String getmPerUnit() {
        return mPerUnit;
    }

    public String getmSubText() {
        return mSubText;
    }

    public int getmDurationInDays() {
        return mDurationInDays;
    }

    public int getmPricePerUnit() {
        return mPricePerUnit;
    }


    public String getmFreeTrialCtaText() {
        return mFreeTrialCtaText;
    }

    public void setmFreeTrialCtaText(String mFreeTrialCtaText) {
        this.mFreeTrialCtaText = mFreeTrialCtaText;
    }

    public String getmFreeTrialExpiryText() {
        return mFreeTrialExpiryText;
    }

    public void setmFreeTrialExpiryText(String mFreeTrialExpiryText) {
        this.mFreeTrialExpiryText = mFreeTrialExpiryText;
    }

    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public boolean isMatchGauranteed() {
        return isMatchGauranteed;
    }

    public int getmDiscount() {
        return mDiscount;
    }

    public String getmDescription() {
        return mDescription;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mSku);
        parcel.writeString(mTag);
        parcel.writeByte((byte) (isPopular ? 1 : 0));
        parcel.writeInt(mPrice);
        parcel.writeInt(mPricePerUnit);
        parcel.writeInt(mDurationInDays);
        parcel.writeString(mFreeTrialCtaText);
        parcel.writeString(mFreeTrialExpiryText);
        parcel.writeString(mTitle);
        parcel.writeByte((byte) (isMatchGauranteed?1:0));
        parcel.writeInt(mDiscount);
        parcel.writeString(mDescription);
    }
}
