/**
 * 
 */
package com.trulymadly.android.app.modal;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * @author udbhav
 * 
 */
public class State {
	private String id, name;

	private State(String _id, String _name) {
		id = _id;
		name = _name;
	}

	public static State[] getStatesFromJson(JSONArray stateJson,
			Boolean isGeoLocationApiData, String defaultMessage) {


		int len = 0;
		if (stateJson != null) {
			len = stateJson.length();
		}
		State[] states = new State[len + 1];
		states[0] = new State("0", defaultMessage);
		for (int i = 0; i < len; i++) {
			if (isGeoLocationApiData) {
				states[i + 1] = new State(stateJson.optJSONObject(i).optString(
						"state_id"), stateJson.optJSONObject(i).optString(
						"name"));
			} else {
				states[i + 1] = new State(stateJson.optJSONObject(i).optString(
						"key"), stateJson.optJSONObject(i).optString("value"));
			}
		}
		return states;
	}

	public static State[] getStatesFromList(
			ArrayList<EditPrefBasicDataModal> statesList, String defaultMessage) {
		int len = 0;
		if (statesList != null) {
			len = statesList.size();
		}
		State[] states = new State[len + 1];
		states[0] = new State("0", defaultMessage);
		for (int i = 0; i < len; i++) {
			states[i + 1] = new State(statesList.get(i).getStateId(),
					statesList.get(i).getName());
		}
		return states;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String toString() {
		return (this.name);
	}

}
