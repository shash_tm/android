package com.trulymadly.android.app.modal;

import android.os.Parcel;
import android.os.Parcelable;

import com.trulymadly.android.app.MessageModal;

/**
 * Created by avin on 28/12/15.
 */
public class CuratedDealsChatsModal implements Parcelable {

    public static final Creator<CuratedDealsChatsModal> CREATOR = new Creator<CuratedDealsChatsModal>() {
        @Override
        public CuratedDealsChatsModal createFromParcel(Parcel in) {
            return new CuratedDealsChatsModal(in);
        }

        @Override
        public CuratedDealsChatsModal[] newArray(int size) {
            return new CuratedDealsChatsModal[size];
        }
    };
    private String mDealId, mDateSpotId, mRectUri, mMessage, mAddress;
    private MessageModal.MessageType mMessageType;

    public CuratedDealsChatsModal(MessageModal.MessageType mMessageType, String mDealId, String mDateSpotId,
                                  String mRectUri, String mMessage, String mAddress) {
        this.mMessageType = mMessageType;
        this.mDealId = mDealId;
        this.mDateSpotId = mDateSpotId;
        this.mRectUri = mRectUri;
        this.mMessage = mMessage;
        this.mAddress = mAddress;
    }

    private CuratedDealsChatsModal(Parcel in) {
        mMessageType = MessageModal.MessageType.valueOf(in.readString());
        mDealId = in.readString();
        mDateSpotId = in.readString();
        mRectUri = in.readString();
        mMessage = in.readString();
        mAddress = in.readString();
    }

    public String getmDealId() {
        return mDealId;
    }

    public void setmDealId(String mDealId) {
        this.mDealId = mDealId;
    }

    public String getmDateSpotId() {
        return mDateSpotId;
    }

    public void setmDateSpotId(String mDateSpotId) {
        this.mDateSpotId = mDateSpotId;
    }

    public String getmMessage() {
        return mMessage;
    }

    public void setmMessage(String mMessage) {
        this.mMessage = mMessage;
    }

    public String getmAddress() {
        return mAddress;
    }

    public void setmAddress(String mAddress) {
        this.mAddress = mAddress;
    }

    public MessageModal.MessageType getmMessageType() {
        return mMessageType;
    }

    public void setmMessageType(MessageModal.MessageType mMessageType) {
        this.mMessageType = mMessageType;
    }

    public String getmRectUri() {
        return mRectUri;
    }

    public void setmRectUri(String mRectUri) {
        this.mRectUri = mRectUri;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mMessageType.name());
        dest.writeString(mDealId);
        dest.writeString(mDateSpotId);
        dest.writeString(mRectUri);
        dest.writeString(mMessage);
        dest.writeString(mAddress);
    }

}
