package com.trulymadly.android.app.modal;

public class ShareProfileDataModal {
    private String shareLink;
    private String shareMessage;
    private String alertMessage;
    private String hideAlertMessage;

    public ShareProfileDataModal() {
    }

    public String getShareLink() {
        return shareLink;
    }

    public void setShareLink(String shareLink) {
        this.shareLink = shareLink;
    }

    public String getShareMessage() {
        return shareMessage;
    }

    public void setShareMessage(String shareMessage) {
        this.shareMessage = shareMessage;
    }

    public String getAlertMessage() {
        return alertMessage;
    }

    public void setAlertMessage(String alertMessage) {
        this.alertMessage = alertMessage;
    }

    public String getHideAlertMessage() {
        return hideAlertMessage;
    }

    public void setHideAlertMessage(String hideAlertMessage) {
        this.hideAlertMessage = hideAlertMessage;
    }

}
