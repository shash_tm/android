package com.trulymadly.android.app.modal;

/**
 * Created by avin on 29/10/15.
 */
public class AdNativeModal {
    private String mTitle;
    private String mLandingUrl;
    private String mCta;
    private AdImageModal mAdImageModal;

    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getmLandingUrl() {
        return mLandingUrl;
    }

    public void setmLandingUrl(String mLandingUrl) {
        this.mLandingUrl = mLandingUrl;
    }

    public String getmCta() {
        return mCta;
    }

    public void setmCta(String mCta) {
        this.mCta = mCta;
    }

    public AdImageModal getmAdImageModal() {
        return mAdImageModal;
    }

    public void setmAdImageModal(AdImageModal mAdImageModal) {
        this.mAdImageModal = mAdImageModal;
    }

    public void setmAdImageModal(String url, double aspectRatio, int height, int width) {
        this.mAdImageModal = new AdImageModal(url, aspectRatio, height, width);
    }
}
