package com.trulymadly.android.app.modal;

import com.trulymadly.android.app.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by avin on 21/10/16.
 */

public class SelectQuizModal {
    private int mTotalCompletedQuestions = 0;
    private String mQuizId;
    private ArrayList<SelectQuizQuestionModal> mSelectQuizQuestionModals;

    public SelectQuizModal(String mQuizId, ArrayList<SelectQuizQuestionModal> mSelectQuizQuestionModals) {
        this.mQuizId = mQuizId;
        this.mSelectQuizQuestionModals = mSelectQuizQuestionModals;
    }

    public String getmQuizId() {
        return mQuizId;
    }

    public int getTotalQuestions() {
        return (mSelectQuizQuestionModals != null) ? mSelectQuizQuestionModals.size() : 0;
    }

    public int getmTotalCompletedQuestions() {
        return mTotalCompletedQuestions;
    }

    public void setmTotalCompletedQuestions(int mTotalCompletedQuestions) {
        this.mTotalCompletedQuestions = mTotalCompletedQuestions;
    }

    public SelectQuizQuestionModal getSelectQuizQuestionModal(int index) {
        return (mSelectQuizQuestionModals != null && mSelectQuizQuestionModals.size() > index) ?
                mSelectQuizQuestionModals.get(index) : null;
    }

    public ArrayList<SelectQuizQuestionModal> getSelectQuizQuestionModals() {
        return mSelectQuizQuestionModals;
    }

    public String getAnswerOfQuestion(int index) {
        return mSelectQuizQuestionModals.get(index).getmSelectedOptionId();
    }

    public String getOptionIdOfQuestion(int index, int idIndex) {
        return mSelectQuizQuestionModals.get(index).getOptionIdAt(idIndex);
    }

    public String getAnswersAsJSONString() {
        JSONObject jsonObject = new JSONObject();

        try {
            for (SelectQuizQuestionModal selectQuizQuestionModal : mSelectQuizQuestionModals) {
                jsonObject.put(selectQuizQuestionModal.getmQuesId(), selectQuizQuestionModal.getmSelectedOptionId());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }

    public static class SelectQuizQuestionModal {
        private String mQuesId, mImageUrl, mQuestion;
        private String mSelectedOptionId = null;
        private ArrayList<SelectQuizOptionModal> mSelectQuizOptionModals;

        public SelectQuizQuestionModal(String mQuesId, String mImageUrl, String mQuestion,
                                       ArrayList<SelectQuizOptionModal> mSelectQuizOptionModals) {
            this.mQuesId = mQuesId;
            this.mImageUrl = mImageUrl;
            this.mQuestion = mQuestion;
            this.mSelectQuizOptionModals = mSelectQuizOptionModals;
        }

        public SelectQuizQuestionModal(String mQuesId, String mSelectedOptionId) {
            this.mQuesId = mQuesId;
            this.mSelectedOptionId = mSelectedOptionId;
        }

        public String getmQuesId() {
            return mQuesId;
        }

        public void setmQuesId(String mQuesId) {
            this.mQuesId = mQuesId;
        }

        public String getmImageUrl() {
            return mImageUrl;
        }

        public void setmImageUrl(String mImageUrl) {
            this.mImageUrl = mImageUrl;
        }

        public String getmQuestion() {
            return mQuestion;
        }

        public void setmQuestion(String mQuestion) {
            this.mQuestion = mQuestion;
        }

        public String getmSelectedOptionId() {
            return mSelectedOptionId;
        }

        public void setmSelectedOptionId(String mSelectedOptionId) {
            this.mSelectedOptionId = mSelectedOptionId;
        }

        public String getOptionIdAt(int index) {
            return mSelectQuizOptionModals.get(index).getmOptionId();
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof SelectQuizQuestionModal) {
                return Utility.stringCompare(this.getmQuesId(), ((SelectQuizQuestionModal) obj).getmQuesId());
            }

            return super.equals(obj);
        }

    }

    public static class SelectQuizOptionModal {
        private String mOptionId, mOption;

        public SelectQuizOptionModal(String mOptionId, String mOption) {
            this.mOptionId = mOptionId;
            this.mOption = mOption;
        }

        public String getmOptionId() {
            return mOptionId;
        }

        public String getmOption() {
            return mOption;
        }
    }
}
