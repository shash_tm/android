/**
 * 
 */
package com.trulymadly.android.app.modal;

import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * @author udbhav
 * 
 */
public class City {
	private String id, name;

	private City(String _id, String _name) {
		id = _id;
		name = _name;
	}

	public static City[] getCitiesFromJson(JSONArray cityJson, String defaultMessage) {

		int len = 0;
		if (cityJson != null) {
			len = cityJson.length();
		}
		int totalLength;
		if (cityJson != null) {
			totalLength = len + 2;
		} else {
			totalLength = len + 1;
		}
		City[] cities = new City[totalLength];
		cities[0] = new City("0", defaultMessage);
		for (int i = 0; i < len; i++) {
			cities[i + 1] = new City(cityJson.optJSONObject(i).optString(
					"city_id"), cityJson.optJSONObject(i).optString("name"));
		}

		if (cityJson != null) {
			cities[len + 1] = new City(Constants.LOCATION_OTHER, "Other");
		}
		return cities;
	}

	public static City[] getCitiesFromCityData(String stateId,
			ArrayList<EditPrefBasicDataModal> citiesList, String defaultMessage) {

		int len = 0;
		if (citiesList != null && Utility.isSet(stateId)) {
			for (EditPrefBasicDataModal city : citiesList) {
				if(stateId.equalsIgnoreCase(city.getStateId())){
					len++;
				}
			}
		}
		int totalLength;
		if (citiesList != null && Utility.isSet(stateId)) {
			totalLength = len + 2;
		} else {
			totalLength = len + 1;
		}
		City[] cities = new City[totalLength];
		cities[0] = new City("0", defaultMessage);
		int i = 0;
		if (citiesList != null) {
			for (EditPrefBasicDataModal city : citiesList) {
				if (stateId.equalsIgnoreCase(city.getStateId())) {
					cities[i + 1] = new City(city.getCityId(), city.getName());
					i++;
				}
			}
		}


		if (citiesList != null && Utility.isSet(stateId)) {
			cities[len + 1] = new City(Constants.LOCATION_OTHER, "Other");
		}
		return cities;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String toString() {
		return (this.name);
	}
}
