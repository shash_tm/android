package com.trulymadly.android.app.modal;

public class EditPrefSaveModal {

	public String lookingAgeStart, lookingAgeEnd, marital_status_spouse,
			spouseCountry, spouseState, spouseCity, start_height,
			heightSpouseInchStart, end_height, heightSpouseInchEnd,
			income_spouse_start, income_spouse_end, income, education_spouse;

	public boolean show_us_profiles;

	public EditPrefSaveModal() {
	}

}
