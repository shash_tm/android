package com.trulymadly.android.app.modal;

import com.trulymadly.android.app.ads.AdsConfig;

/**
 * Created by avin on 03/12/15.
 */
public class UserFlags {
    private ChatsConfig mChatsConfig;
//    private InterstitialNativeAdsConfig mInterstitialNativeAdsConfig;
    private boolean isCDEnabled;
    private int mPopularity = -1, mProfileView = -1;
    private String mProfileViewsText;
    private boolean isLastActiveShown = false;
    private boolean showPhotoOnMissTM = false;
    private boolean allowPhotoShareInChat = false;
    private boolean allowMeetups = false;
    private int hardAppVersion = 0, softAppVersion = 0;
    private String hardUpdateText, softUpdateText;
    private String userEmail;
//    private ProfileAdsConfig mProfileAdsConfig;
    private AdsConfig mAdsConfig;
    private boolean isSparksEnabled;
    private boolean isPaytmPaymentEnabled, isGooglePaymentEnabled, isPaytmWalletEnabled,
        isPaytmNBEnabled, isPaytmCCEnabled, isPaytmDCEnabled;

    private boolean whyNotSparksEnabled;
    private int mWhyNotSparksInsteadMatchesCount = 4;
    private int showFavoritesInMatchesIntervalInDays = 30, showFavoritesInMatchesFemalePosition = 10,
            showFavoritesInMatchesFemaleHides = 10;

    private int mSparksCount;
    private String mWhyNotSparkNudgeContent;

    //Select
    private boolean isSelectEnabled;
    private MySelectData mMySelectData;

    private boolean isSelectNudgeEnabled;
    private int mSelectNudgeFrequency;
    private String mSelectNudgeContent;

    private boolean isLikeAllowedOnSelect, isSparkAllowedOnSelect, isLikebackAllowedOnSelect;


//    public ProfileAdsConfig getmProfileAdsConfig() {
//        return mProfileAdsConfig;
//    }
//
//    public void setmProfileAdsConfig(ProfileAdsConfig mProfileAdsConfig) {
//        this.mProfileAdsConfig = mProfileAdsConfig;
//    }

    public AdsConfig getmAdsConfig() {
        return mAdsConfig;
    }

    public void setmAdsConfig(AdsConfig mAdsConfig) {
        this.mAdsConfig = mAdsConfig;
    }

    public boolean isAllowMeetups() {
        return allowMeetups;
    }

    public void setAllowMeetups(boolean allowMeetups) {
        this.allowMeetups = allowMeetups;
    }
    public ChatsConfig getmChatsConfig() {
        return mChatsConfig;
    }

    public void setmChatsConfig(ChatsConfig mChatsConfig) {
        this.mChatsConfig = mChatsConfig;
    }

//    public InterstitialNativeAdsConfig getmInterstitialNativeAdsConfig() {
//        return mInterstitialNativeAdsConfig;
//    }
//
//    public void setmInterstitialNativeAdsConfig(InterstitialNativeAdsConfig mInterstitialNativeAdsConfig) {
//        this.mInterstitialNativeAdsConfig = mInterstitialNativeAdsConfig;
//    }


    public boolean isGooglePaymentEnabled() {
        return isGooglePaymentEnabled;
    }

    public void setGooglePaymentEnabled(boolean googlePaymentEnabled) {
        isGooglePaymentEnabled = googlePaymentEnabled;
    }

    public boolean isPaytmPaymentEnabled() {
        return isPaytmPaymentEnabled;
    }

    public void setPaytmPaymentEnabled(boolean paytmPaymentEnabled) {
        isPaytmPaymentEnabled = paytmPaymentEnabled;
    }

    public boolean isPaytmDCEnabled() {
        return isPaytmDCEnabled;
    }

    public void setPaytmDCEnabled(boolean paytmDCEnabled) {
        isPaytmDCEnabled = paytmDCEnabled;
    }

    public boolean isPaytmCCEnabled() {
        return isPaytmCCEnabled;
    }

    public void setPaytmCCEnabled(boolean paytmCCEnabled) {
        isPaytmCCEnabled = paytmCCEnabled;
    }

    public boolean isPaytmNBEnabled() {
        return isPaytmNBEnabled;
    }

    public void setPaytmNBEnabled(boolean paytmNBEnabled) {
        isPaytmNBEnabled = paytmNBEnabled;
    }

    public boolean isPaytmWalletEnabled() {
        return isPaytmWalletEnabled;
    }

    public void setPaytmWalletEnabled(boolean paytmWalletEnabled) {
        isPaytmWalletEnabled = paytmWalletEnabled;
    }

    public boolean isCDEnabled() {
        return isCDEnabled;
    }

    public void setCDEnabled(boolean isCDEnabled) {
        this.isCDEnabled = isCDEnabled;
    }

    public int getmPopularity() {
        return mPopularity;
    }

    public void setmPopularity(int mPopularity) {
        this.mPopularity = mPopularity;
    }

    public int getmProfileView() {
        return mProfileView;
    }

    public void setmProfileView(int mProfileView) {
        this.mProfileView = mProfileView;
    }

    public String getmProfileViewsText() {
        return mProfileViewsText;
    }

    public void setmProfileViewsText(String mProfileViewsText) {
        this.mProfileViewsText = mProfileViewsText;
    }

    public boolean isLastActiveShown() {
        return isLastActiveShown;
    }

    public void setIsLastActiveShown(boolean isLastLoggedInVisible) {
        this.isLastActiveShown = isLastLoggedInVisible;
    }

    public boolean isShowPhotoOnMissTM() {
        return showPhotoOnMissTM;
    }

    public void setShowPhotoOnMissTM(boolean showPhotoOnMissTM) {
        this.showPhotoOnMissTM = showPhotoOnMissTM;
    }

    public boolean isAllowPhotoShareInChat() {
        return allowPhotoShareInChat;
    }

    public void setAllowPhotoShareInChat(boolean allowPhotoShareInChat) {
        this.allowPhotoShareInChat = allowPhotoShareInChat;
    }

    public int getHardAppVersion() {
        return hardAppVersion;
    }

    public void setHardAppVersion(int hardAppVersion) {
        this.hardAppVersion = hardAppVersion;
    }

    public int getSoftAppVersion() {
        return softAppVersion;
    }

    public void setSoftAppVersion(int softAppVersion) {
        this.softAppVersion = softAppVersion;
    }

    public String getHardUpdateText() {
        return hardUpdateText;
    }

    public void setHardUpdateText(String hardUpdateText) {
        this.hardUpdateText = hardUpdateText;
    }

    public String getSoftUpdateText() {
        return softUpdateText;
    }

    public void setSoftUpdateText(String softUpdateText) {
        this.softUpdateText = softUpdateText;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public boolean isSparksEnabled() {
        return isSparksEnabled;
    }

    public void setSparksEnabled(boolean sparksEnabled) {
        isSparksEnabled = sparksEnabled;
    }

    public int getmWhyNotSparksInsteadMatchesCount() {
        return mWhyNotSparksInsteadMatchesCount;
    }

    public void setmWhyNotSparksInsteadMatchesCount(int mWhyNotSparksInsteadMatchesCount) {
        this.mWhyNotSparksInsteadMatchesCount = mWhyNotSparksInsteadMatchesCount;
    }

    public boolean isWhyNotSparksEnabled() {
        return whyNotSparksEnabled;
    }

    public void setWhyNotSparksEnabled(boolean whyNotSparksEnabled) {
        this.whyNotSparksEnabled = whyNotSparksEnabled;
    }

    public int getShowFavoritesInMatchesIntervalInDays() {
        return showFavoritesInMatchesIntervalInDays;
    }

    public void setShowFavoritesInMatchesIntervalInDays(int showFavoritesInMatchesIntervalInDays) {
        this.showFavoritesInMatchesIntervalInDays = showFavoritesInMatchesIntervalInDays;
    }

    public int getShowFavoritesInMatchesFemalePosition() {
        return showFavoritesInMatchesFemalePosition;
    }

    public void setShowFavoritesInMatchesFemalePosition(int showFavoritesInMatchesFemalePosition) {
        this.showFavoritesInMatchesFemalePosition = showFavoritesInMatchesFemalePosition;
    }

    public int getShowFavoritesInMatchesFemaleHides() {
        return showFavoritesInMatchesFemaleHides;
    }

    public void setShowFavoritesInMatchesFemaleHides(int showFavoritesInMatchesFemaleHides) {
        this.showFavoritesInMatchesFemaleHides = showFavoritesInMatchesFemaleHides;
    }

    public int getmSparksCount() {
        return mSparksCount;
    }

    public void setmSparksCount(int mSparksCount) {
        this.mSparksCount = mSparksCount;
    }

    //Select

    public MySelectData getmMySelectData() {
        return mMySelectData;
    }

    public void setmMySelectData(MySelectData mMySelectData) {
        this.mMySelectData = mMySelectData;
    }

    public boolean isSelectEnabled() {
        return isSelectEnabled;
    }

    public void setSelectEnabled(boolean selectEnabled) {
        isSelectEnabled = selectEnabled;
    }

    public boolean isLikebackAllowedOnSelect() {
        return isLikebackAllowedOnSelect;
    }

    public void setLikebackAllowedOnSelect(boolean likebackAllowedOnSelect) {
        isLikebackAllowedOnSelect = likebackAllowedOnSelect;
    }

    public boolean isSparkAllowedOnSelect() {
        return isSparkAllowedOnSelect;
    }

    public void setSparkAllowedOnSelect(boolean sparkAllowedOnSelect) {
        isSparkAllowedOnSelect = sparkAllowedOnSelect;
    }

    public boolean isLikeAllowedOnSelect() {
        return isLikeAllowedOnSelect;
    }

    public void setLikeAllowedOnSelect(boolean likeAllowedOnSelect) {
        isLikeAllowedOnSelect = likeAllowedOnSelect;
    }

    public String getmSelectNudgeContent() {
        return mSelectNudgeContent;
    }

    public void setmSelectNudgeContent(String mSelectNudgeContent) {
        this.mSelectNudgeContent = mSelectNudgeContent;
    }

    public int getmSelectNudgeFrequency() {
        return mSelectNudgeFrequency;
    }

    public void setmSelectNudgeFrequency(int mSelectNudgeFrequency) {
        this.mSelectNudgeFrequency = mSelectNudgeFrequency;
    }

    public boolean isSelectNudgeEnabled() {
        return isSelectNudgeEnabled;
    }

    public void setSelectNudgeEnabled(boolean selectNudgeEnabled) {
        isSelectNudgeEnabled = selectNudgeEnabled;
    }

    public String getmWhyNotSparkNudgeContent() {
        return mWhyNotSparkNudgeContent;
    }

    public void setmWhyNotSparkNudgeContent(String mWhyNotSparkNudgeContent) {
        this.mWhyNotSparkNudgeContent = mWhyNotSparkNudgeContent;
    }
}
