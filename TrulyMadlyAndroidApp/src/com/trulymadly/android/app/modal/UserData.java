package com.trulymadly.android.app.modal;

import org.json.JSONArray;

import java.io.Serializable;
import java.util.ArrayList;

import static com.google.common.base.Preconditions.checkNotNull;

public class UserData implements Serializable {

	private static final long serialVersionUID = -2287382187645399516L;
	private String fname = null, lname = null, status = null;
	private String fbDesignation = null;
	private int dob_d = 0, dob_m, dob_y;
	private String countryId = null, stateId = null, cityId = null;
	private String height_feet = null, height_inches = null,height;
	private ArrayList<String> companies = null;
	private String currentTitle = null, income = null;
	private String highestDegree = null;
	private ArrayList<String> colleges = null;
	private String dataUrl = null, page = null;
	private Boolean isFb = false, isNotWorking = false;
	private ArrayList<String> interests = null;
	private ArrayList<String> optionalCustomInterests = null;

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	private String cityName;
	
	private Boolean isStepDoneHobby = false;

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getDob_d() {
		return dob_d;
	}

	public void setDob_d(int dob_d) {
		this.dob_d = dob_d;
	}

	public int getDob_m() {
		return dob_m;
	}

	public void setDob_m(int dob_m) {
		this.dob_m = dob_m;
	}

	public int getDob_y() {
		return dob_y;
	}

	public void setDob_y(int dob_y) {
		this.dob_y = dob_y;
	}

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	public String getStateId() {
		return stateId;
	}

	public void setStateId(String stateId) {
		this.stateId = stateId;
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getHeight_feet() {
		return height_feet;
	}

	public void setHeight_feet(String height_feet) {
		this.height_feet = height_feet;
	}

	public String getHeight_inches() {
		return height_inches;
	}

	public void setHeight_inches(String height_inches) {
		this.height_inches = height_inches;
	}

	public ArrayList<String> getCompanies() {
		return companies;
	}

	public void setCompanies(ArrayList<String> companies) {
		this.companies = companies;
	}

	public String getCompaniesForSaving() {
		return getArrayListForSaving(companies, false);
	}

	public String getCurrentTitle() {
		return currentTitle;
	}

	public void setCurrentTitle(String currentTitle) {
		this.currentTitle = currentTitle;
	}

	public String getIncome() {
		return income;
	}

	public void setIncome(String income) {
		this.income = income;
	}

	public String getHighestDegree() {
		return highestDegree;
	}

	public void setHighestDegree(String highestDegree) {
		this.highestDegree = highestDegree;
	}

	public ArrayList<String> getColleges() {
		return colleges;
	}

	public void setColleges(ArrayList<String> colleges) {
		this.colleges = colleges;
	}

	public String getCollegesForSaving() {
		return getArrayListForSaving(colleges, false);
	}

	public String getDataUrl() {
		return dataUrl;
	}

	public void setDataUrl(String dataUrl) {
		this.dataUrl = dataUrl;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public Boolean getIsFb() {
		return isFb;
	}

	public void setIsFb(Boolean isFb) {
		this.isFb = isFb;
	}

	public Boolean getIsNotWorking() {
		return isNotWorking;
	}

	public void setIsNotWorking(Boolean isNotWorking) {
		this.isNotWorking = isNotWorking;
	}

	public ArrayList<String> getInterests() {
		return interests;
	}

	public void setInterests(ArrayList<String> interests) {
		this.interests = interests;
	}

	public String getInterestsForSaving(boolean isUsedInCookie) {
		return getArrayListForSaving(interests, isUsedInCookie);
	}

	public void addLinkedInColleges(ArrayList<String> linkedinColleges) {
		if (linkedinColleges != null) {
			colleges = new ArrayList<>();
			for (String col : linkedinColleges) {
				if (!isCollegeAdded(col)) {
					colleges.add(col);
				}
			}
		}
	}

	private boolean isCollegeAdded(String newCollegeName) {
		for (String collegeName : colleges) {
			if (checkNotNull(collegeName).equalsIgnoreCase(newCollegeName)) {
				return true;
			}
		}
		return false;
	}

	private String getArrayListForSaving(ArrayList<String> list, boolean isUsedInCookie) {
		StringBuilder sb = new StringBuilder();
		String prefix = "";
		sb.append("[");
		for (String s : list) {
			sb.append(prefix);
			prefix = isUsedInCookie ? "%2C" : ",";
			sb.append("\"");
			sb.append(s.replace("\"", "\'"));
			sb.append("\"");
		}
		sb.append("]");
		return sb.toString();
	}

	public Boolean getIsStepDoneHobby() {
		return isStepDoneHobby;
	}

	public void setIsStepDoneHobby(Boolean isStepDoneHobby) {
		this.isStepDoneHobby = isStepDoneHobby;
	}

	public void setStepsCompleted(JSONArray stepsCompletedJsonArray) {
		if (hasStep(stepsCompletedJsonArray, "hobby")) {
			isStepDoneHobby = true;
		}

	}

	private boolean hasStep(JSONArray stepsCompletedJsonArray, String step) {
		if (stepsCompletedJsonArray != null
				&& stepsCompletedJsonArray.length() > 0) {
			for (int i = 0; i < stepsCompletedJsonArray.length(); i++) {
				if (stepsCompletedJsonArray.optString(i).equalsIgnoreCase(step)) {
					return true;
				}
			}
		}
		return false;
	}

	public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public ArrayList<String> getOptionalCustomInterests() {
		return optionalCustomInterests;
	}

	public void setOptionalCustomInterests(ArrayList<String> optionalCustomInterests) {
		this.optionalCustomInterests = optionalCustomInterests;
	}

	public String getFbDesignation() {
		return fbDesignation;
	}

	public void setFbDesignation(String fbDesignation) {
		this.fbDesignation = fbDesignation;
	}
}
