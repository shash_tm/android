package com.trulymadly.android.app.modal;

/**
 * Created by avin on 03/12/15.
 */

/*
* Basic structure:
* "system_flags": {
		"socket_debug_flag": false,
		"chat_ip": "http:\/\/54.169.28.196:80",
		"is_socket_enabled": true
	}
*/
public class SystemFlags {
    private boolean isSocketDebugEnabled, isSocketEnabled;

    public boolean isSocketDebugEnabled() {
        return isSocketDebugEnabled;
    }

    public void setIsSocketDebugEnabled(boolean isSocketDebugEnabled) {
        this.isSocketDebugEnabled = isSocketDebugEnabled;
    }

    public boolean isSocketEnabled() {
        return isSocketEnabled;
    }

    public void setIsSocketEnabled(boolean isSocketEnabled) {
        this.isSocketEnabled = isSocketEnabled;
    }

}