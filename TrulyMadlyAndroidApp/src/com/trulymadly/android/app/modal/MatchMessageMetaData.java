package com.trulymadly.android.app.modal;

import com.trulymadly.android.app.R;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;

public class MatchMessageMetaData {

    private String user_id, match_id, last_seen_receiver_tstamp = null,
            last_fetched_id = null, fname, profile_pic, profile_url = null,
            last_fetched_tstamp = null, message_link = null;
    private Date last_update_tstamp = null, last_chat_read_sent_tstamp = null;
    private String user_last_quiz_action_time = null,
            match_last_quiz_action_time = null;
    private String age = null;
    private boolean isMissTm = false;
    private CDClickableState mCDClickableState = CDClickableState.DISABLED;
    private String doodleUrl = null;
    private boolean isSparkReceived, isMutualSpark;
    private boolean isSelectMatch;

    public MatchMessageMetaData() {
    }

    public String getUserId() {
        return this.user_id;
    }

    public void setUserId(String user_id) {
        this.user_id = user_id;
    }

    public String getMatchId() {
        return this.match_id;
    }

    public void setMatchId(String match_id) {
        this.match_id = match_id;
    }

    public String getLastSeenReceiverTstamp() {
        return this.last_seen_receiver_tstamp;
    }

    public void setLastSeenReceiverTstamp(String last_seen_receiver_tstamp) {
        this.last_seen_receiver_tstamp = last_seen_receiver_tstamp;
    }

    public String getLastFetchedId() {
        return this.last_fetched_id;
    }

    public void setLastFetchedId(String last_fetched_id) {
        this.last_fetched_id = last_fetched_id;
    }

    public String getFName() {
        return this.fname;
    }

    public void setFName(String fname) {
        this.fname = fname;
    }

    public String getProfilePic() {
        return this.profile_pic;
    }

    public void setProfilePic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getProfileUrl() {
        return this.profile_url;
    }

    public void setProfileUrl(String profile_url) {
        this.profile_url = profile_url;
    }

    public String getLastFetchedTstamp() {
        return this.last_fetched_tstamp;
    }

    public void setLastFetchedTstamp(String last_fetched_tstamp) {
        this.last_fetched_tstamp = last_fetched_tstamp;
    }

    public boolean isSetLastSeenReceiverTstamp() {
        return !(this.last_seen_receiver_tstamp == null
                || this.last_seen_receiver_tstamp.equals("") || this.last_seen_receiver_tstamp
                .equals("0000-00-00 00:00:00"));
    }

    public Date getLastUpdateTstamp() {
        return last_update_tstamp;
    }

    public void setLastUpdateTstamp(Date last_update_tstamp) {
        this.last_update_tstamp = last_update_tstamp;
    }

    public Date getLastChatReadSentTstamp() {
        return last_chat_read_sent_tstamp;
    }

    public void setLast_chat_read_sent_tstamp(Date last_chat_read_sent_tstamp) {
        this.last_chat_read_sent_tstamp = last_chat_read_sent_tstamp;
    }

    public void generateMatchMessageMetaDataFromServer(JSONObject object) {
        this.match_id = object.optString("profile_id");
        this.fname = object.optString("fname");
        this.profile_pic = object.optString("profile_pic");
        this.profile_url = object.optString("profile_link");
        this.last_seen_receiver_tstamp = object
                .optString("last_seen_receiver_tstamp");
        this.setLastUpdateTstamp(Calendar.getInstance().getTime());
        setIsMissTm(object
                .optBoolean("is_miss_tm", false));
    }

    public void generateMatchMessageMetaData(String userId, String matchId, String fname, String profile_pic,
                                             String profile_url, String message_link, String age,
                                             boolean isMissTm, boolean isSparkReceived, boolean isMutualSpark) {
        generateMatchMessageMetaData(userId, matchId, fname, profile_pic, profile_url, message_link,
                age, isMissTm, isSparkReceived, isMutualSpark, false);
    }

    public void generateMatchMessageMetaData(String userId, String matchId, String fname, String profile_pic,
                                             String profile_url, String message_link, String age,
                                             boolean isMissTm, boolean isSparkReceived,
                                             boolean isMutualSpark, boolean isSelectMatch) {
        this.match_id = matchId;
        this.fname = fname;
        this.user_id = userId;
        this.profile_pic = profile_pic;
        this.profile_url = profile_url;
        this.message_link = message_link;
        this.age = age;
        setIsMissTm(isMissTm);
        this.isSparkReceived = isSparkReceived;
        this.isMutualSpark = isMutualSpark;
        this.isSelectMatch = isSelectMatch;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getMessage_link() {
        return message_link;
    }

    public void setMessage_link(String message_link) {
        this.message_link = message_link;
    }

    public String getUserLastQuizActionTime() {
        return user_last_quiz_action_time;
    }

    public void setUserLastQuizActionTime(String user_last_quiz_action_time) {
        this.user_last_quiz_action_time = user_last_quiz_action_time;
    }

    public String getMatchLastQuizActionTime() {
        return match_last_quiz_action_time;
    }

    public void setMatchLastQuizActionTime(String match_last_quiz_action_time) {
        this.match_last_quiz_action_time = match_last_quiz_action_time;
    }

    public boolean isMissTm() {
        return isMissTm;
    }

    public void setIsMissTm(boolean isMissTm) {
        this.isMissTm = isMissTm;
    }

    public CDClickableState getmCDClickableState() {
        return mCDClickableState;
    }

    public void setmCDClickableState(int value) {
        mCDClickableState = CDClickableState.createFromInt(value);
    }

    public void setmCDClickableState(CDClickableState mCDClickableState) {
        this.mCDClickableState = mCDClickableState;
    }

    public String getDoodleUrl() {
        return doodleUrl;
    }

    public void setDoodleUrl(String doodleUrl) {
        this.doodleUrl = doodleUrl;
    }

    public boolean isDoodleDead() {
        return !Utility.isSet(doodleUrl);
    }

    public boolean isSparkReceived() {
        return isSparkReceived;
    }

    public void setSparkReceived(boolean sparkReceived) {
        isSparkReceived = sparkReceived;
    }

    public boolean isMutualSpark() {
        return isMutualSpark;
    }

    public void setMutualSpark(boolean mutualSpark) {
        isMutualSpark = mutualSpark;
    }

    public boolean isSelectMatch() {
        return isSelectMatch;
    }

    public void setSelectMatch(boolean selectMatch) {
        isSelectMatch = selectMatch;
    }

    public enum CDClickableState {
        DISABLED(-1), CLICKABLE(1), NOT_CLICKABLE(0);

        private final int key;

        CDClickableState(int key) {
            this.key = key;
        }

        public static CDClickableState createFromInt(int i) {
            switch (i) {
                case -1:
                    return DISABLED;

                case 0:
                    return NOT_CLICKABLE;

                case 1:
                    return CLICKABLE;
            }

            return null;
        }

        public static CDClickableState createFromBoolean(boolean state) {
            if (state) {
                return CLICKABLE;
            } else {
                return NOT_CLICKABLE;
            }
        }

        public int getKey() {
            return key;
        }

        public int getImage() {
            switch (this) {
                case DISABLED:
                    return R.drawable.cd_icon_disabled;

                default:
                    return R.drawable.cd_icon;
            }
        }
    }
}
