package com.trulymadly.android.app.modal;

/**
 * Created by avin on 16/06/16.
 */
public class ImageAndText {
    private String mText;
    private int mDrawable;

    public ImageAndText() {
    }

    public ImageAndText(String mText, int mDrawable) {
        this.mText = mText;
        this.mDrawable = mDrawable;
    }

    public String getmText() {
        return mText;
    }

    public void setmText(String mText) {
        this.mText = mText;
    }

    public int getmDrawable() {
        return mDrawable;
    }

    public void setmDrawable(int mDrawable) {
        this.mDrawable = mDrawable;
    }
}
