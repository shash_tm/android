package com.trulymadly.android.app.modal;

import android.content.Context;

import com.trulymadly.android.app.json.ConstantsRF;
import com.trulymadly.android.app.sqlite.RFHandler;
import com.trulymadly.android.app.utility.TMSelectHandler;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONObject;

/**
 * Created by avin on 03/11/16.
 */

public class MySelectData {
    private boolean isTMSelectMember;
    private int mDaysLeft;
    private boolean isQuizPlayed;
    private String mSideCTA, mProfileCTA, mProfileCTASubtext, mCompatibilityTextDef, mCompatibilityImageDef;

    public MySelectData(boolean isTMSelectMember, int mDaysLeft, boolean isQuizPlayed,
                        String mSideCTA, String mProfileCTA, String mProfileCTASubtext) {
        this.isTMSelectMember = isTMSelectMember;
        this.mDaysLeft = mDaysLeft;
        this.isQuizPlayed = isQuizPlayed;
        this.mSideCTA = mSideCTA;
        this.mProfileCTA = mProfileCTA;
        this.mProfileCTASubtext = mProfileCTASubtext;
    }

    public MySelectData(JSONObject mySelectObject) {
        this.isTMSelectMember = mySelectObject.optBoolean("is_tm_select");
        this.mDaysLeft = mySelectObject.optInt("days_left");
        this.isQuizPlayed = mySelectObject.optBoolean("quiz_played");
        this.mSideCTA = mySelectObject.optString("side_cta");
        this.mProfileCTA = mySelectObject.optString("profile_cta");
        this.mProfileCTASubtext = mySelectObject.optString("profile_cta_text");
        this.mCompatibilityTextDef = mySelectObject.optString("compatibility_text_def");
        this.mCompatibilityImageDef = mySelectObject.optString("compatibility_image_def");
    }

    public boolean isTMSelectMember() {
        return isTMSelectMember;
    }

    public void setTMSelectMember(boolean TMSelectMember) {
        isTMSelectMember = TMSelectMember;
    }

    public int getmDaysLeft() {
        return mDaysLeft;
    }

    public void setmDaysLeft(int mDaysLeft) {
        this.mDaysLeft = mDaysLeft;
    }

    public boolean isQuizPlayed() {
        return isQuizPlayed;
    }

    public void setQuizPlayed(boolean quizPlayed) {
        isQuizPlayed = quizPlayed;
    }

    public String getmSideCTA() {
        return mSideCTA;
    }

    public void setmSideCTA(String mSideCTA) {
        this.mSideCTA = mSideCTA;
    }

    public String getmProfileCTA() {
        return mProfileCTA;
    }

    public void setmProfileCTA(String mProfileCTA) {
        this.mProfileCTA = mProfileCTA;
    }

    public String getmProfileCTASubtext() {
        return mProfileCTASubtext;
    }

    public void setmProfileCTASubtext(String mProfileCTASubtext) {
        this.mProfileCTASubtext = mProfileCTASubtext;
    }

    public String getmCompatibilityImageDef() {
        return mCompatibilityImageDef;
    }

    public void setmCompatibilityImageDef(String mCompatibilityImageDef) {
        this.mCompatibilityImageDef = mCompatibilityImageDef;
    }

    public String getmCompatibilityTextDef() {
        return mCompatibilityTextDef;
    }

    public void setmCompatibilityTextDef(String mCompatibilityTextDef) {
        this.mCompatibilityTextDef = mCompatibilityTextDef;
    }

    public void saveMySelectData(Context context) {
        String userId = Utility.getMyId(context);
        //TODO : Do we need to do this?
//        boolean wasASelectMember = RFHandler.getBool(context, userId, ConstantsRF.RIGID_FIELD_IS_SELECT_MEMBER);
//        if(wasASelectMember != isTMSelectMember()){
//            MatchesDbHandler.clearMatchesCache(context);
//        }
        RFHandler.insert(context, userId, ConstantsRF.RIGID_FIELD_IS_SELECT_MEMBER, isTMSelectMember());
        if (isTMSelectMember()) {
            TMSelectHandler.setPaymentDone(context, true);
        } else {
            TMSelectHandler.setPaymentDone(context, false);
        }
        RFHandler.insert(context, userId, ConstantsRF.RIGID_FIELD_SELECT_DAYS_LEFT, getmDaysLeft());
        RFHandler.insert(context, userId, ConstantsRF.RIGID_FIELD_IS_SELECT_QUIZ_PLAYED, isQuizPlayed());
        RFHandler.insert(context, userId, ConstantsRF.RIGID_FIELD_SELECT_SIDE_CTA_TEXT, getmSideCTA());
        RFHandler.insert(context, userId, ConstantsRF.RIGID_FIELD_SELECT_PROFILE_CTA, getmProfileCTA());
        RFHandler.insert(context, userId, ConstantsRF.RIGID_FIELD_SELECT_PROFILE_CTA_SUBTEXT, getmProfileCTASubtext());
        RFHandler.insert(context, userId, ConstantsRF.RIGID_FIELD_SELECT_COMP_TEXT_DEF, getmCompatibilityTextDef());
        RFHandler.insert(context, userId, ConstantsRF.RIGID_FIELD_SELECT_COMP_IMAGE_DEF, getmCompatibilityImageDef());
    }
}
