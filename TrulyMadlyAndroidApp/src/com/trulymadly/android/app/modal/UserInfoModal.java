package com.trulymadly.android.app.modal;

import java.io.Serializable;

/**
 * Created by deveshbatra on 3/19/16.
 */
public class UserInfoModal implements Serializable {
    private static final long serialVersionUID = -5119865425845038431L;
    private String profile_url;
    private String name;
    private String id;
    private String likeLink;
    private String hideLink;
    private boolean hasLiked;
    private boolean isMutualMatch;
    private boolean likedByMe;
    private boolean sparkedByMe;
    private String profilePicLink;
    private String messageUrl;

    public boolean isLikedByMe() {
        return likedByMe;
    }

    public void setLikedByMe(boolean likedByMe) {
        this.likedByMe = likedByMe;
    }

    public boolean isSparkedByMe() {
        return sparkedByMe;
    }

    public void setSparkedByMe(boolean sparkedByMe) {
        this.sparkedByMe = sparkedByMe;
    }

    public String getMessageUrl() {
        return messageUrl;
    }

    public void setMessageUrl(String messageUrl) {
        this.messageUrl = messageUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLikeLink() {
        return likeLink;
    }

    public void setLikeLink(String likeLink) {
        this.likeLink = likeLink;
    }

    public String getHideLink() {
        return hideLink;
    }

    public void setHideLink(String hideLink) {
        this.hideLink = hideLink;
    }

    public String getProfile_url() {
        return profile_url;
    }

    public void setProfile_url(String profile_url) {
        this.profile_url = profile_url;
    }

    public boolean isHasLiked() {
        return hasLiked;
    }

    public void setHasLiked(boolean hasLiked) {
        this.hasLiked = hasLiked;
    }

    public boolean isMutualMatch() {
        return isMutualMatch;
    }

    public void setIsMutualMatch(boolean isMutualMatch) {
        this.isMutualMatch = isMutualMatch;
    }

    public String getProfilePicLink() {
        return profilePicLink;
    }

    public void setProfilePicLink(String profilePicLink) {
        this.profilePicLink = profilePicLink;
    }
}
