/**
 * 
 */
package com.trulymadly.android.app.modal;

import com.trulymadly.android.app.json.Constants;

import java.util.ArrayList;

/**
 * @author udbhav
 * 
 */
public class Country {
	private String id, name;

	private Country(String _id, String _name) {
		id = _id;
		name = _name;
	}

	@SuppressWarnings("UnusedParameters")
	public static Country[] getCountries(ArrayList<EditPrefBasicDataModal> countriesList) {
		//TODO: country fix here
		Country[] countries = new Country[1];
		countries[0] = new Country(String.valueOf(Constants.COUNTRY_ID_INDIA), "India");
		/*
		Country[] countries = new Country[countriesList.size() + 1];
		countries[0] = new Country("0", "Select current country");
		for (int i = 0; i < countriesList.size(); i++) {
			countries[i + 1] = new Country(countriesList.getBool(i)
					.getIDorCountryID(), countriesList.getBool(i).getName());

		}
		*/
		return countries;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String toString() {
		return (this.name);
	}
}
