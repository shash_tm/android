package com.trulymadly.android.app.modal;

public class EditPreferencePrefillModelNew {

    private boolean show_us_profiles;
    private boolean us_profiles_enabled;
    private String basics_data;
    private String user_id;
    private String start_age;
    private String end_age;
    private String marital_status;
    private String location_preference_for_matching;
    private String countries;
    private String states;
    private String cities;
    private String start_height;
    private String end_height;
    private String working_area;
    private String industry;
    private String education;
    private String income_start;
    private String income_end;

    private String income_details;
    private String incomeIds;

    public EditPreferencePrefillModelNew() {
    }

    public EditPreferencePrefillModelNew(String basics_data,
                                         String user_id, String start_age, String end_age,
                                         String marital_status, String location_preference_for_matching,
                                         String countries, String states, String cities,
                                         String start_height, String end_height, String working_area,
                                         String industry, String education, String income_start,
                                         String income_end, String income_details, String incomeIds,
                                         boolean show_us_profiles, boolean us_profiles_enabled) {

        this.basics_data = basics_data;
        this.user_id = user_id;
        this.start_age = start_age;
        this.end_age = end_age;
        this.marital_status = marital_status;
        this.location_preference_for_matching = location_preference_for_matching;
        this.countries = countries;
        this.states = states;
        this.cities = cities;
        this.start_height = start_height;
        this.end_height = end_height;
        this.working_area = working_area;
        this.industry = industry;
        this.education = education;
        this.income_start = income_start;
        this.income_end = income_end;
        this.income_details = income_details;
        this.incomeIds = incomeIds;
        this.show_us_profiles = show_us_profiles;
        this.us_profiles_enabled = us_profiles_enabled;
    }

    public String getBasicData() {
        return this.basics_data;
    }

    public void setBasicData(String basics_data) {
        this.basics_data = basics_data;
    }

    public String getUserId() {
        return this.user_id;
    }

    public void setUserId(String user_id) {
        this.user_id = user_id;
    }

    public String getStartAge() {
        return this.start_age;
    }

    public void setStartAge(String start_age) {
        this.start_age = start_age;
    }

    public String getEndAge() {
        return this.end_age;
    }

    public void setEndAge(String end_age) {
        this.end_age = end_age;
    }

    public String getMaritalStatus() {
        return this.marital_status;
    }

    public void setMaritalStatus(String marital_status) {
        this.marital_status = marital_status;
    }

    public String getLocPrefForMatching() {
        return this.location_preference_for_matching;
    }

    public void setLocPrefForMatching(String location_preference_for_matching) {
        this.location_preference_for_matching = location_preference_for_matching;
    }

    public String getCountries() {
        return this.countries;
    }

    public void setCountries(String countries) {
        this.countries = countries;
    }

    public String getStates() {
        return this.states;
    }

    public void setStates(String states) {
        this.states = states;
    }

    public String getCities() {
        return this.cities;
    }

    public void setCities(String cities) {
        this.cities = cities;
    }

    public String getStartHeight() {
        return this.start_height;
    }

    public void setStartHeight(String start_height) {
        this.start_height = start_height;
    }

    public String getEndHeight() {
        return this.end_height;
    }

    public void setEndHeight(String end_height) {
        this.end_height = end_height;
    }

    public String getWorkingArea() {
        return this.working_area;
    }

    public void setWorkingArea(String working_area) {
        this.working_area = working_area;
    }

    public String getIndustry() {
        return this.industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getEducation() {
        return this.education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getIncomeStart() {
        return this.income_start;
    }

    public void setIncomeStart(String income_start) {
        this.income_start = income_start;
    }

    public String getIncomeEnd() {
        return this.income_end;
    }

    public void setIncomeEnd(String income_end) {
        this.income_end = income_end;
    }

    public String getIncomeDetail() {
        return this.income_details;
    }

    public void setIncomeDetail(String income_details) {
        this.income_details = income_details;
    }

    public String getIncomeIds() {
        return this.incomeIds;
    }

    public void setIncomeIds(String incomeIds) {
        this.incomeIds = incomeIds;
    }

    public boolean isShow_us_profiles() {
        return show_us_profiles;
    }

    public void setShow_us_profiles(boolean show_us_profiles) {
        this.show_us_profiles = show_us_profiles;
    }

    public boolean isUs_profiles_enabled() {
        return us_profiles_enabled;
    }

    public void setUs_profiles_enabled(boolean us_profiles_enabled) {
        this.us_profiles_enabled = us_profiles_enabled;
    }
}