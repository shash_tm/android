package com.trulymadly.android.app.modal;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by avin on 08/06/16.
 */
public class SimpleDialogModal implements Parcelable {
    public static final Creator<SimpleDialogModal> CREATOR = new Creator<SimpleDialogModal>() {
        @Override
        public SimpleDialogModal createFromParcel(Parcel in) {
            return new SimpleDialogModal(in);
        }

        @Override
        public SimpleDialogModal[] newArray(int size) {
            return new SimpleDialogModal[size];
        }
    };
    private String mHeader, mDesc, mActionText;
    private int mAction, mDrawableId, mDrawableId2;
    private int newSparksAdded = 0, totalSparks = 0;

    public SimpleDialogModal(String mHeader, String mDesc, String mActionText, int mAction, int mDrawableId, int mDrawableId2) {
        this.mHeader = mHeader;
        this.mDesc = mDesc;
        this.mActionText = mActionText;
        this.mAction = mAction;
        this.mDrawableId = mDrawableId;
        this.mDrawableId2 = mDrawableId2;
    }

    protected SimpleDialogModal(Parcel in) {
        mHeader = in.readString();
        mDesc = in.readString();
        mActionText = in.readString();
        mAction = in.readInt();
        mDrawableId = in.readInt();
        newSparksAdded = in.readInt();
        totalSparks = in.readInt();
    }

    public String getmHeader() {
        return mHeader;
    }

    public void setmHeader(String mHeader) {
        this.mHeader = mHeader;
    }

    public String getmDesc() {
        return mDesc;
    }

    public void setmDesc(String mDesc) {
        this.mDesc = mDesc;
    }

    public String getmActionText() {
        return mActionText;
    }

    public void setmActionText(String mActionText) {
        this.mActionText = mActionText;
    }

    public int getmAction() {
        return mAction;
    }

    public void setmAction(int mAction) {
        this.mAction = mAction;
    }

    public int getmDrawableId2() {
        return mDrawableId2;
    }

    public int getmDrawableId() {
        return mDrawableId;
    }

    public int getNewSparksAdded() {
        return newSparksAdded;
    }

    public void setNewSparksAdded(int newSparksAdded) {
        this.newSparksAdded = newSparksAdded;
    }

    public int getTotalSparks() {
        return totalSparks;
    }

    public void setTotalSparks(int totalSparks) {
        this.totalSparks = totalSparks;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mHeader);
        dest.writeString(mDesc);
        dest.writeString(mActionText);
        dest.writeInt(mAction);
        dest.writeInt(mDrawableId);
        dest.writeInt(newSparksAdded);
        dest.writeInt(totalSparks);
    }
}
