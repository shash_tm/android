package com.trulymadly.android.app.modal;

/**
 * Created by avin on 11/08/16.
 */
public class ProfileViewPagerModal {
    private String mSource, mThumbnailUrl;
    private boolean isVideo;
    private boolean isMuted;
    private int mDuration;
    private String mId;


    public ProfileViewPagerModal(String mSource, boolean isVideo,
                                 String thumbnail, boolean muted,
                                 int duration, String video_id) {
        this.mSource = mSource;
        this.isVideo = isVideo;
        this.mThumbnailUrl = thumbnail;
        this.isMuted = muted;
        this.mDuration = duration;
        this.mId = video_id;
    }

    public String getmSource() {
        return mSource;
    }

    public void setmSource(String mSource) {
        this.mSource = mSource;
    }

    public boolean isVideo() {
        return isVideo;
    }

    public void setVideo(boolean video) {
        isVideo = video;
    }

    public String getmThumbnailUrl() {
        return mThumbnailUrl;
    }

    public void setmThumbnailUrl(String mThumbnailUrl) {
        this.mThumbnailUrl = mThumbnailUrl;
    }

    public boolean isMuted() {
        return isMuted;
    }

    public void setMuted(boolean muted) {
        isMuted = muted;
    }

    public int getmDuration() {
        return mDuration;
    }

    public void setmDuration(int mDuration) {
        this.mDuration = mDuration;
    }

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }

    public static ProfileViewPagerModal[] createImagesArrayFromImages(String[] urls){
        ProfileViewPagerModal[] profileViewPagerModals = null;
        if(urls != null && urls.length > 0){
            profileViewPagerModals = new ProfileViewPagerModal[urls.length];
            for(int i = 0; i < urls.length; i++){
                profileViewPagerModals[i] = new ProfileViewPagerModal(urls[i], false, null, false, 0, null);
            }
        }

        return profileViewPagerModals;
    }

    public static ProfileViewPagerModal[] createImagesArrayFromImagesAndVideos(String[] images, String[] videos, String[] thumbnails){
        ProfileViewPagerModal[] profileViewPagerModals = null;
        int totalLength = ((images != null)?images.length:0) + ((videos != null)?videos.length:0);
//        int totalLength = ((images != null)?images.length:0) + ((videos != null)?1:0);
        if(totalLength > 0){
            profileViewPagerModals = new ProfileViewPagerModal[totalLength];
            int count = 0;
            if(images != null && images.length > 0) {
                for (int i = 0; i < images.length; i++) {
                    profileViewPagerModals[i] = new ProfileViewPagerModal(images[i], false, null, false, 0, null);
                }
                count = images.length;
            }

            if(videos != null && videos.length > 0) {
                for (int i = count; i < totalLength; i++) {
                    profileViewPagerModals[i] = new ProfileViewPagerModal(videos[i - count], true, thumbnails[i - count], false, 0, null);
                }
            }
        }

        return profileViewPagerModals;
    }

    public static ProfileViewPagerModal[] createImagesArrayFromImagesAndVideos(String[] images, VideoModal[] videoModals){
        ProfileViewPagerModal[] profileViewPagerModals = null;
        int totalLength = ((images != null)?images.length:0) + ((videoModals != null)?videoModals.length:0);
//        int totalLength = ((images != null)?images.length:0) + ((videos != null)?1:0);
        if(totalLength > 0){
            profileViewPagerModals = new ProfileViewPagerModal[totalLength];
            int count = 0;
            if(images != null && images.length > 0) {
                for (int i = 0; i < images.length; i++) {
                    profileViewPagerModals[i] = new ProfileViewPagerModal(images[i], false, null, false, 0, null);
                }
                count = images.length;
            }

            if(videoModals != null && videoModals.length > 0) {
                for (int i = count; i < totalLength; i++) {
                    profileViewPagerModals[i] = new ProfileViewPagerModal(videoModals[i - count].getUrl(),
                            true, videoModals[i - count].getThumbnail(), videoModals[i-count].isMuted(),
                            videoModals[i-count].getmDuration(), videoModals[i-count].getmId());
                }
            }
        }

        return profileViewPagerModals;
    }
}
