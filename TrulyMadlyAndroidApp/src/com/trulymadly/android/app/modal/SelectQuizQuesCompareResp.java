package com.trulymadly.android.app.modal;

import com.trulymadly.android.app.utility.Utility;

/**
 * Created by avin on 25/10/16.
 */
public class SelectQuizQuesCompareResp {
    private String mQuesId, mQuestion;
    private String mUserAnswerId, mMatchAnswerId;
    private boolean isUserLiked, isMatchLiked;
    private boolean isAnsCommon;
    private int mRank = -1;

    public SelectQuizQuesCompareResp(String mQuesId, String mQuestion, String mUserAnswerId, String mMatchAnswerId) {
        this(mQuesId, mQuestion, mUserAnswerId, mMatchAnswerId, -1);
    }

    public SelectQuizQuesCompareResp(String mQuesId, String mQuestion, String mUserAnswerId,
                                     String mMatchAnswerId, int mRank) {
        this.mQuesId = mQuesId;
        this.mQuestion = mQuestion;
        this.mUserAnswerId = mUserAnswerId;
        this.mMatchAnswerId = mMatchAnswerId;
        this.mRank = mRank;
        isAnsCommon = Utility.stringCompare(mUserAnswerId, mMatchAnswerId);
        isUserLiked = "1".equalsIgnoreCase(mUserAnswerId);
        isMatchLiked = "1".equalsIgnoreCase(mMatchAnswerId);
    }

    public String getmQuesId() {
        return mQuesId;
    }

    public String getmQuestion() {
        return mQuestion;
    }

    public String getmUserAnswerId() {
        return mUserAnswerId;
    }

    public String getmMatchAnswerId() {
        return mMatchAnswerId;
    }

    public boolean isUserLiked() {
        return isUserLiked;
    }

    public boolean isMatchLiked() {
        return isMatchLiked;
    }

    public boolean isAnsCommon() {
        return isAnsCommon;
    }

    public int getmRank() {
        return mRank;
    }
}
