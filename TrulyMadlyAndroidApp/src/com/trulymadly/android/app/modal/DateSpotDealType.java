package com.trulymadly.android.app.modal;

import android.content.Context;
import android.view.View;

import com.trulymadly.android.app.R;
import com.trulymadly.android.app.utility.Utility;

/**
 * Created by udbhav on 04/01/16.
 */
public enum DateSpotDealType {
    ON_THE_HOUSE("on_the_house"),
    HAND_PICKED_MENU("hand_picked_menu"),
    SPECIAL_DISCOUNT("special_discount"),
    OTHER("other");

    private final String key;

    /**
     * @param key
     */
    DateSpotDealType(final String key) {
        this.key = key;
    }

    public static DateSpotDealType createFromString(String value) {
        for (DateSpotDealType v : values())
            if (Utility.stringCompare(v.toString(), value)) return v;
        return OTHER;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return key;
    }

    public String getText(Context c) {
        switch (this) {
            case SPECIAL_DISCOUNT:
                return c.getResources().getString(R.string.special_discounts);
            case ON_THE_HOUSE:
                return c.getResources().getString(R.string.on_the_house);
            case HAND_PICKED_MENU:
                return c.getResources().getString(R.string.hand_picked_menu);
            case OTHER:
                return "";
        }
        return "";
    }

    public int getImageResId() {
        switch (this) {
            case SPECIAL_DISCOUNT:
                return R.drawable.discount;
            case ON_THE_HOUSE:
                return R.drawable.on_the_house;
            case HAND_PICKED_MENU:
                return R.drawable.handpicked_menu;
            case OTHER:
                return R.drawable.handpicked_menu;
        }
        return R.drawable.handpicked_menu;
    }


    public int getVisibility() {
        switch (this) {
            case HAND_PICKED_MENU:
            case ON_THE_HOUSE:
            case SPECIAL_DISCOUNT:
                return View.VISIBLE;
            case OTHER:
                return View.GONE;
        }
        return View.GONE;
    }

    public int getBackgroundColorResId() {
        switch (this) {
            case HAND_PICKED_MENU:
                return R.color.date_list_item_blue;
            case ON_THE_HOUSE:
                return R.color.date_list_item_red;
            case SPECIAL_DISCOUNT:
                return R.color.date_list_item_yellow;
            case OTHER:
                return R.color.date_list_item_blue;
        }
        return R.color.date_list_item_blue;
    }
}
