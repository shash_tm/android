package com.trulymadly.android.app.modal;

import com.trulymadly.android.app.utility.Utility;

/**
 * Created by avin on 28/06/16.
 */
public class SparkBlooperModal {
    private boolean isThereAnyNewSpark;
    private String mNewSparkMatchId;
    private boolean isCurrentSparkNotExpired;
    private boolean isBlooperAnimationRequired;
    private boolean isBlooperRequired;

    public boolean isThereAnyNewSpark() {
        return isThereAnyNewSpark;
    }

    public void setThereAnyNewSpark(boolean thereAnyNewSpark) {
        isThereAnyNewSpark = thereAnyNewSpark;
    }

    public String getmNewSparkMatchId() {
        return mNewSparkMatchId;
    }

    public void setmNewSparkMatchId(String mNewSparkMatchId) {
        this.mNewSparkMatchId = mNewSparkMatchId;
        if(Utility.isSet(mNewSparkMatchId)){
            setBlooperAnimationRequired(true);
        }
    }

    public boolean isCurrentSparkNotExpired() {
        return isCurrentSparkNotExpired;
    }

    public void setCurrentSparkNotExpired(boolean currentSparkNotExpired) {
        isCurrentSparkNotExpired = currentSparkNotExpired;
    }

    public boolean isBlooperAnimationRequired() {
        return isBlooperAnimationRequired;
    }

    public void setBlooperAnimationRequired(boolean blooperAnimationRequired) {
        isBlooperAnimationRequired = blooperAnimationRequired;
        if(blooperAnimationRequired){
            setBlooperRequired(true);
        }
    }

    public boolean isBlooperRequired() {
        return isBlooperRequired;
    }

    public void setBlooperRequired(boolean blooperRequired) {
        isBlooperRequired = blooperRequired;
    }
}
