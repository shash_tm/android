package com.trulymadly.android.app.modal;

import android.database.Cursor;

public class QuizData {

	private int id;
	private String name;
	private int updated_in_version;
	private String image_url;
	private String banner_url;

	public QuizData(Cursor cursor) {
		if (cursor == null) {
			return;
		}
		this.id = cursor.getInt(cursor.getColumnIndex("id"));
		this.name = cursor.getString(cursor.getColumnIndex("name"));
		this.updated_in_version = cursor
				.getInt(cursor.getColumnIndex("updated_in_version"));
		this.image_url = cursor.getString(cursor.getColumnIndex("image_url"));
		this.banner_url = cursor.getString(cursor.getColumnIndex("banner_url"));

	}

	public QuizData() {
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return this.id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getUpdatedInVersion() {
		return updated_in_version;
	}

	public void setUpdatedInVersion(int updated_in_version) {
		this.updated_in_version = updated_in_version;
	}

	public void setImageUrl(String image_url) {
		this.image_url = image_url;
	}

	public String getImageUrl() {
		return this.image_url;
	}

	public void setBannerUrl(String banner_url) {
		this.banner_url = banner_url;
	}

	public String getBannerUrl() {
		return this.banner_url;
	}

	public String getImageCacheKey() {
		return getImageCacheKeyFromId(id);
	}

	public static String getImageCacheKeyFromId(int id) {
		return "quizid" + id;
	}

}
