package com.trulymadly.android.app.modal;

public class EducationModal {
    private String highestEdu;
    private String college;
    private String[] institutes;
    private String institute;

    public EducationModal() {
    }

    public EducationModal(String highestEdu, String college, String[] institutes,
                          String institute) {
        this.highestEdu = highestEdu;
        this.college = college;
        this.institutes = institutes;
        this.institute = institute;
    }

    public void setInstitue(String institute) {
        this.institute = institute;
    }

    public String getHighestEdu() {
        return this.highestEdu;
    }

    public void setHighestEdu(String highestEdu) {
        this.highestEdu = highestEdu;
    }

    public String getCollege() {
        return this.college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    public String[] getInstitutes() {
        return this.institutes;
    }

    public void setInstitutes(String[] institutes) {
        this.institutes = institutes;
    }

    public String getInstitute() {
        return this.institute;
    }
}