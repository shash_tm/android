/**
 * 
 */
package com.trulymadly.android.app.modal;

import org.json.JSONArray;

/**
 * @author udbhav
 * 
 */
public class PhotoIdType {
	private String id, name;

	private PhotoIdType(String _id, String _name) {
		id = _id;
		name = _name;
	}

	public static PhotoIdType[] getPhotoIdType(JSONArray photoIdTypeJson) {
		PhotoIdType[] photoIdTypes = new PhotoIdType[photoIdTypeJson.length() + 1];
		photoIdTypes[0] = new PhotoIdType("0", "Select Photo Id");
		for (int i = 0; i < photoIdTypeJson.length(); i++) {
			photoIdTypes[i + 1] = new PhotoIdType(photoIdTypeJson.optJSONObject(i)
					.optString("key"), photoIdTypeJson.optJSONObject(i).optString(
					"value"));

		}
		return photoIdTypes;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String toString() {
		return (this.name);
	}
}
