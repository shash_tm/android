/**
 * 
 */
package com.trulymadly.android.app.modal;

import com.trulymadly.android.app.utility.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * @author udbhav
 * 
 */
public class TrustBuilderData {

	private boolean isFbVerified = false, isLinkedInVerified = false,
			isPhotoIdVerified = false, isPhoneVerified = false,
			isEndorsementVerified = false;
	private String fbConnection = null, fbMismatch = null;
	private JSONObject fbDiff = null, fb_messages = null;
	private String liConnections = null;
	private boolean isAnyprofilePic = false;
	private String isProofType = null, isRejectReason = null, idStatus = null;
	private JSONObject id_diff_data = null, id_messages = null;
	private String phoneNo = null, rejected_message = null;
	private int noOftrials = 0;
	private int scoreFb, scoreLi, scoreId, scorePh, scoreEn,
			scoreVerifiedThreshold;
	private String enMessage, enSuccessMessage, enLink;
	private int enThresholdCount;
	private ArrayList<EndorserModal> endorsers = null;
	private int trustScore;
	private boolean isAuthentic = false;
	private String diffuseLine1Text1, diffuseLine1Text2, diffuseLine2Text1,
			diffuseLine2Text2, verifiedText, notVerifiedText, femaleNonAuthenticSubtext;
	private String trustScoreMessageZeroScore;
	private String trustScoreMessageHundredScore;
	private String trustScoreMessageZeroToHundredScore;
	private String enProfilePicMessage;
	private String enRequestMessage;
	public TrustBuilderData() {
	}

	public TrustBuilderData(JSONObject data) {

		if (data == null) {
			return;
		}

		setIsAuthentic(data.optString("status").equalsIgnoreCase("authentic"));

		JSONObject trustPointsJson = data.optJSONObject("trustPoints");

		scoreFb = trustPointsJson.optInt("fb");
		scoreLi = trustPointsJson.optInt("linkedin");
		scoreId = trustPointsJson.optInt("photo_id");
		scorePh = trustPointsJson.optInt("phone");
		scoreEn = trustPointsJson.optInt("endorsements");
		scoreVerifiedThreshold = trustPointsJson.optInt("threshold");

		trustScore = data.optInt("trustScore");
		

		JSONObject fbJson = data.optJSONObject("facebook");
		if (fbJson != null) {
			isFbVerified = fbJson.optBoolean("isVerified");
			fbConnection = fbJson.optString("fbConnection");
			fbDiff = fbJson.optJSONObject("TMFBdiff");
			fbMismatch = fbJson.optString("fb_mismatch");
			setFb_messages(fbJson.optJSONObject("fb_messages"));
		}

		JSONObject liJson = data.optJSONObject("linkedin");
		if (liJson != null) {
			isLinkedInVerified = liJson.optBoolean("isVerified");
			liConnections = liJson.optString("linkdesignation");
		}

		isAnyprofilePic = !data.optString("isAnyprofilePic").equalsIgnoreCase(
				"0");
		JSONObject idJson = data.optJSONObject("idProof");
		if (idJson != null) {
			isPhotoIdVerified = idJson.optBoolean("isVerified");
			isProofType = idJson.optString("isProofType");
			idStatus = idJson.optString("status");
			isRejectReason = idJson.optString("isRejectReason");
			id_diff_data = idJson.optJSONObject("id_diff_data");
			id_messages = idJson.optJSONObject("id_messages");
		}

		JSONObject phJson = data.optJSONObject("phone");
		if (phJson != null) {
			isPhoneVerified = phJson.optBoolean("isVerified");
			phoneNo = phJson.optString("phone_number");
			if (Utility.isSet(phJson.optString("number_of_trials"))) {
				noOftrials = Integer.parseInt(phJson
						.optString("number_of_trials"));
			}
			rejected_message = phJson.optString("rejected_message");
		}

		JSONObject enJson = data.optJSONObject("endorsementData");
		if (enJson != null) {
			isEndorsementVerified = enJson.optBoolean("isVerified");
			enLink = enJson.optString("link");
			enMessage = enJson.optString("endorsement_message");
			enSuccessMessage = enJson.optString("endorsement_success_message");
			enThresholdCount = enJson.optInt("endorsement_threshold_count");
			enRequestMessage=enJson.optString("endorsement_send");
			enProfilePicMessage=enJson.optString("endorsement_profile_pic_added_success");

			endorsers = parseEndorsers(enJson.optJSONArray("data"));
		}

		JSONObject messagesJson = data.optJSONObject("messages");
		if (messagesJson != null) {
			JSONObject trustMessage = messagesJson.optJSONObject("trustScoreMessage");
			if(trustMessage != null) {
				trustScoreMessageZeroScore=trustMessage.optString("zero");
				trustScoreMessageHundredScore=trustMessage.optString("hundred");
				trustScoreMessageZeroToHundredScore=trustMessage.optString("zeroToHundred");
			}
			notVerifiedText = messagesJson.optString("non-authentic");
			verifiedText = messagesJson.optString("authentic");
			diffuseLine1Text1 = messagesJson.optString("diffuseline1text1");
			diffuseLine1Text2 = messagesJson.optString("diffuseline1text2");
			diffuseLine2Text1 = messagesJson.optString("diffuseline2text1");
			diffuseLine2Text2 = messagesJson.optString("diffuseline2text2");
			femaleNonAuthenticSubtext = messagesJson.optString("femaleSubText");
		}
	}

	public static ArrayList<EndorserModal> parseEndorsers(JSONArray data) {
		ArrayList<EndorserModal> endorsers = new ArrayList<>();
		if (data != null) {
			for (int i = 0; i < data.length(); i++) {
				EndorserModal newEndorser = EndorserModal
						.createNewEndorser(data
								.optJSONObject(i));
				if (newEndorser != null) {
					endorsers.add(newEndorser);
				}
			}
		}
		return endorsers;
	}

	public boolean getIsFbVerified() {
		return isFbVerified;
	}

	public void setIsFbVerified(boolean isFbVerified) {
		this.isFbVerified = isFbVerified;
	}

	public boolean getIsLinkedInVerified() {
		return isLinkedInVerified;
	}

	public void setIsLinkedInVerified(boolean isLinkedInVerified) {
		this.isLinkedInVerified = isLinkedInVerified;
	}

	public boolean getIsPhotoIdVerified() {
		return isPhotoIdVerified;
	}

	public void setIsPhotoIdVerified(boolean isPhotoIdVerified) {
		this.isPhotoIdVerified = isPhotoIdVerified;
	}

	public boolean getIsPhoneVerified() {
		return isPhoneVerified;
	}

	public void setIsPhoneVerified(boolean isPhoneVerified) {
		this.isPhoneVerified = isPhoneVerified;
	}

	public String getEnMessage() {
		return enMessage;
	}

	public void setEnMessage(String enMessage) {
		this.enMessage = enMessage;
	}

	public String getEnSuccessMessage() {
		return enSuccessMessage;
	}

	public void setEnSuccessMessage(String enSuccessMessage) {
		this.enSuccessMessage = enSuccessMessage;
	}

	public String getEnLink() {
		return enLink;
	}

	public void setEnLink(String enLink) {
		this.enLink = enLink;
	}

	public int getEnThresholdCount() {
		return enThresholdCount;
	}

	public void setEnThresholdCount(int enThresholdCount) {
		this.enThresholdCount = enThresholdCount;
	}

	public String getFbConnection() {
		return fbConnection;
	}

	public void setFbConnection(String fbConnection) {
		this.fbConnection = fbConnection;
	}

	public JSONObject getFbDiff() {
		return fbDiff;
	}

	public void setFbDiff(JSONObject fbDiff) {
		this.fbDiff = fbDiff;
	}

	public JSONObject getFb_messages() {
		return fb_messages;
	}

	private void setFb_messages(JSONObject fb_messages) {
		this.fb_messages = fb_messages;
	}

	public String getFbMismatch() {
		return fbMismatch;
	}

	public void setFbMismatch(String fbMismatch) {
		this.fbMismatch = fbMismatch;
	}

	public String getLiConnections() {
		return liConnections;
	}

	public void setLiConnections(String liConnections) {
		this.liConnections = liConnections;
	}

	public boolean getIsAnyprofilePic() {
		return isAnyprofilePic;
	}

	public void setIsAnyprofilePic(boolean isAnyprofilePic) {
		this.isAnyprofilePic = isAnyprofilePic;
	}

	public String getIsProofType() {
		return isProofType;
	}

	public void setIsProofType(String isProofType) {
		this.isProofType = isProofType;
	}

	public String getIsRejectReason() {
		return isRejectReason;
	}

	public void setIsRejectReason(String isRejectReason) {
		this.isRejectReason = isRejectReason;
	}

	public String getIdStatus() {
		return idStatus;
	}

	public void setIdStatus(String idStatus) {
		this.idStatus = idStatus;
	}

	public JSONObject getId_diff_data() {
		return id_diff_data;
	}

	public void setId_diff_data(JSONObject id_diff_data) {
		this.id_diff_data = id_diff_data;
	}

	public JSONObject getId_messages() {
		return id_messages;
	}

	public void setId_messages(JSONObject id_messages) {
		this.id_messages = id_messages;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getRejected_message() {
		return rejected_message;
	}

	public void setRejected_message(String rejected_message) {
		this.rejected_message = rejected_message;
	}

	public int getNoOftrials() {
		return noOftrials;
	}

	public void setNoOftrials(int noOftrials) {
		this.noOftrials = noOftrials;
	}

	public int getScoreFb() {
		return scoreFb;
	}

	public void setScoreFb(int scoreFb) {
		this.scoreFb = scoreFb;
	}

	public int getScoreLi() {
		return scoreLi;
	}

	public void setScoreLi(int scoreLi) {
		this.scoreLi = scoreLi;
	}

	public int getScoreId() {
		return scoreId;
	}

	public void setScoreId(int scoreId) {
		this.scoreId = scoreId;
	}

	public int getScorePh() {
		return scorePh;
	}

	public void setScorePh(int scorePh) {
		this.scorePh = scorePh;
	}

	public int getScoreEn() {
		return scoreEn;
	}

	public void setScoreEn(int scoreEn) {
		this.scoreEn = scoreEn;
	}

	private int getScoreVerifiedThreshold() {
		return scoreVerifiedThreshold;
	}

	public void setScoreVerifiedThreshold(int scoreVerifiedThreshold) {
		this.scoreVerifiedThreshold = scoreVerifiedThreshold;
	}

	public int getTrustScore() {
		return trustScore;
	}

	public void setTrustScore(int trustScore) {
		this.trustScore = trustScore;
	}

	private boolean getIsAuthentic() {
		return isAuthentic;
	}

	private void setIsAuthentic(boolean isAuthentic) {
		this.isAuthentic = isAuthentic;
	}

	public String getDiffuseLine1Text1() {
		return diffuseLine1Text1;
	}

	public void setDiffuseLine1Text1(String diffuseLine1Text1) {
		this.diffuseLine1Text1 = diffuseLine1Text1;
	}

	public String getDiffuseLine1Text2() {
		return diffuseLine1Text2;
	}

	public void setDiffuseLine1Text2(String diffuseLine1Text2) {
		this.diffuseLine1Text2 = diffuseLine1Text2;
	}

	public String getDiffuseLine2Text1() {
		return diffuseLine2Text1;
	}

	public void setDiffuseLine2Text1(String diffuseLine2Text1) {
		this.diffuseLine2Text1 = diffuseLine2Text1;
	}

	public String getDiffuseLine2Text2() {
		return diffuseLine2Text2;
	}

	public void setDiffuseLine2Text2(String diffuseLine2Text2) {
		this.diffuseLine2Text2 = diffuseLine2Text2;
	}

	public String getVerifiedText() {
		return verifiedText;
	}

	public void setVerifiedText(String verifiedText) {
		this.verifiedText = verifiedText;
	}

    public String getTrustScoreMessageZeroScore()
    {
    	return trustScoreMessageZeroScore;
    }
    public String getTrustScoreMessageHundredScore()
    {
    	return trustScoreMessageHundredScore;
    }
    public String getTrustScoreMessageZeroToHundredScore()
    {
    	return trustScoreMessageZeroToHundredScore;
    }

	public String getNotVerifiedText() {
		return notVerifiedText;
	}

	public void setNotVerifiedText(String notVerifiedText) {
		this.notVerifiedText = notVerifiedText;
	}

	public boolean isVerified() {
		return (getIsAuthentic() || getTrustScore() >= getScoreVerifiedThreshold());
	}

	public ArrayList<EndorserModal> getEndorsers() {
		return endorsers;
	}

	public void setEndorsers(ArrayList<EndorserModal> endorsers) {
		this.endorsers = endorsers;
	}

	public boolean getIsEndorsementVerified() {
		return isEndorsementVerified;
	}

	public void setIsEndorsementVerified(boolean isEndorsementVerified) {
		this.isEndorsementVerified = isEndorsementVerified;
	}

	public String getFemaleNonAuthenticSubtext() {
		return femaleNonAuthenticSubtext;
	}

	public void setFemaleNonAuthenticSubtext(String femaleNonAuthenticSubtext) {
		this.femaleNonAuthenticSubtext = femaleNonAuthenticSubtext;
	}

	public String getEnProfilePicMessage() {
		return enProfilePicMessage;
	}

	public void setEnProfilePicMessage(String enProfilePicMessage) {
		this.enProfilePicMessage = enProfilePicMessage;
	}

	public String getEnRequestMessage() {
		return enRequestMessage;
	}

	public void setEnRequestMessage(String enRequestMessage) {
		this.enRequestMessage = enRequestMessage;
	}

}
