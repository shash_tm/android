package com.trulymadly.android.app.modal;


public class FacebookAlbumItem {
	private String id;
	private String name;
	private String description;
	private String cover_photo_id = null;
	private String cover_url = null;

	public FacebookAlbumItem() {
	}

	public FacebookAlbumItem(String id, String name, String description,
			String cover_photo) {
		this.setId(id);
		this.setName(name);
		this.setDescription(description);
		if (cover_photo != null) {
			this.setCover_photo_id(cover_photo);
		}
	}

	public String getId() {
		return id;
	}

	private void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	private void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	private void setDescription(String description) {
		this.description = description;
	}

	public String getCover_photo_id() {
		return cover_photo_id;
	}

	private void setCover_photo_id(String cover_photo_id) {
		this.cover_photo_id = cover_photo_id;
	}

	public String getCover_url() {
		return cover_url;
	}

	public void setCover_url(String cover_url) {
		this.cover_url = cover_url;
	}

	@Override
	public String toString() {
		return (this.name);
	}

}