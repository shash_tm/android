package com.trulymadly.android.app.modal;

import java.io.Serializable;
import java.util.ArrayList;

public class NudgeListFragmentModal implements Serializable {

	private static final long serialVersionUID = 700774676336433282L;
	private String quizName, matchId;
	private int quizId;
	private ArrayList<NudgeClass> nudgList;

	public String getQuizName() {
		return quizName;
	}

	public void setQuizName(String quizName) {
		this.quizName = quizName;
	}

	public String getMatchId() {
		return matchId;
	}

	public void setMatchId(String matchId) {
		this.matchId = matchId;
	}

	public int getQuizId() {
		return quizId;
	}

	public void setQuizId(int quizId) {
		this.quizId = quizId;
	}

	public ArrayList<NudgeClass> getNudgList() {
		return nudgList;
	}

	public void setNudgList(ArrayList<NudgeClass> nudgList) {
		this.nudgList = nudgList;
	}

}
