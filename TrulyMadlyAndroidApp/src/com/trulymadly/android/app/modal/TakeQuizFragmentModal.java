package com.trulymadly.android.app.modal;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

public class TakeQuizFragmentModal implements Serializable {

	private static final long serialVersionUID = -1387307109275487013L;
	private int quizId;
	private String quizName, matchId, matchName="XYZ";
	private String responseJsonString;

	public int getQuizId() {
		return quizId;
	}

	public void setQuizId(int quizId) {
		this.quizId = quizId;
	}

	public String getQuizName() {
		return quizName;
	}

	public void setQuizName(String quizName) {
		this.quizName = quizName;
	}

	public String getMatchId() {
		return matchId;
	}

	public void setMatchId(String matchId) {
		this.matchId = matchId;
	}

	public JSONObject getResponse() {
		try {
			return new JSONObject(responseJsonString);
		} catch (JSONException e) {
			return new JSONObject();
		} 
	}

	public void setResponse(JSONObject response) {
		this.responseJsonString = response.toString();
	}

	public String getMatchName() {
		return matchName;
	}

	public void setMatchName(String matchName) {
		this.matchName = matchName;
	}

}
