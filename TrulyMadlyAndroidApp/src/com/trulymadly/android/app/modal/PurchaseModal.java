package com.trulymadly.android.app.modal;

import android.database.Cursor;

import com.trulymadly.android.app.json.ConstantsDB;
import com.trulymadly.android.app.json.ConstantsDB.PURCHASES.PurchaseState;

/**
 * Created by udbhav on 24/06/16.
 */
public class PurchaseModal {

    private String sku = null;
    private String token = null;
    private String orderId = null;
    private String developerPayload = null;
    private PurchaseState purchaseState = PurchaseState.INITIALISED;

    public PurchaseModal(String sku, String token, String orderId, String developerPayload, String purchaseState) {
        this.sku = sku;
        this.token = token;
        this.orderId = orderId;
        this.developerPayload = developerPayload;
        try {
            this.purchaseState = PurchaseState.valueOf(purchaseState);
        } catch (IllegalStateException ignored) {
        }
    }

    public PurchaseModal(String sku, String token, String orderId, String developerPayload, PurchaseState purchaseState) {
        this.sku = sku;
        this.token = token;
        this.orderId = orderId;
        this.developerPayload = developerPayload;
        this.purchaseState = purchaseState;
    }

    public PurchaseModal(String sku) {
        this.sku = sku;
    }

    public PurchaseModal(Cursor cursor) {
        this.sku = cursor.getString(cursor.getColumnIndex(ConstantsDB.PURCHASES.SKU));
        this.token = cursor.getString(cursor.getColumnIndex(ConstantsDB.PURCHASES.TOKEN));
        this.orderId = cursor.getString(cursor.getColumnIndex(ConstantsDB.PURCHASES.ORDER_ID));
        this.developerPayload = cursor.getString(cursor.getColumnIndex(ConstantsDB.PURCHASES.DEVELOPER_PAYLOAD));
        try {
            this.purchaseState = PurchaseState.valueOf(cursor.getString(cursor.getColumnIndex(ConstantsDB.PURCHASES.PURCHASE_STATE)));
        } catch (IllegalStateException ignored) {
        }
    }

    public String getSku() {
        return sku;
    }

    public String getToken() {
        return token;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getDeveloperPayload() {
        return developerPayload;
    }

    public PurchaseState getPurchaseState() {
        return purchaseState;
    }
}
