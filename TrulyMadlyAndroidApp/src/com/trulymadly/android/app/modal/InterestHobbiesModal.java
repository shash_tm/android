package com.trulymadly.android.app.modal;

class InterestHobbiesModal {
	
	private String name;
	
	public InterestHobbiesModal(){}
	public InterestHobbiesModal(String name){
		this.name=name;
	}
	
	public String getName(){
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
