package com.trulymadly.android.app.modal;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by avin on 31/05/16.
 */
public class SparkPackageModal implements Parcelable {

    public static final Creator<SparkPackageModal> CREATOR = new Creator<SparkPackageModal>() {
        @Override
        public SparkPackageModal createFromParcel(Parcel in) {
            return new SparkPackageModal(in);
        }

        @Override
        public SparkPackageModal[] newArray(int size) {
            return new SparkPackageModal[size];
        }
    };
    private String mPrice, mTitle, mSubtitle, mPackageSku;
    private int mSparkCount, mExpiryDays;
    private boolean isMostPopular, isChatSessionsAvailable;
    private int mDiscount;
    private String mTag;
    private boolean isMatchGuarantee;

    public SparkPackageModal() {
    }

    protected SparkPackageModal(Parcel in) {
        mPrice = in.readString();
        mTitle = in.readString();
        mSubtitle = in.readString();
        mPackageSku = in.readString();
        mSparkCount = in.readInt();
        mExpiryDays = in.readInt();
        isMostPopular = in.readByte() != 0;
        isChatSessionsAvailable = in.readByte() != 0;
        mDiscount = in.readInt();
        mTag = in.readString();
        isMatchGuarantee = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mPrice);
        dest.writeString(mTitle);
        dest.writeString(mSubtitle);
        dest.writeString(mPackageSku);
        dest.writeInt(mSparkCount);
        dest.writeInt(mExpiryDays);
        dest.writeByte((byte) (isMostPopular ? 1 : 0));
        dest.writeByte((byte) (isChatSessionsAvailable ? 1 : 0));
        dest.writeInt(mDiscount);
        dest.writeString(mTag);
        dest.writeByte((byte) (isMatchGuarantee ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getmPrice() {
        return mPrice;
    }

    public void setmPrice(String mPrice) {
        this.mPrice = mPrice;
    }

    public int getmSparkCount() {
        return mSparkCount;
    }

    public void setmSparkCount(int mSparkCount) {
        this.mSparkCount = mSparkCount;
    }

    public int getmExpiryDays() {
        return mExpiryDays;
    }

    public void setmExpiryDays(int mExpiryDays) {
        this.mExpiryDays = mExpiryDays;
    }

    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getmSubtitle() {
        return mSubtitle;
    }

    public void setmSubtitle(String mSubtitle) {
        this.mSubtitle = mSubtitle;
    }

    public String getmPackageSku() {
        return mPackageSku;
    }

    public void setmPackageSku(String mPackageSku) {
        this.mPackageSku = mPackageSku;
    }

    public boolean isMostPopular() {
        return isMostPopular;
    }

    public void setMostPopular(boolean mostPopular) {
        isMostPopular = mostPopular;
    }

    public boolean isChatSessionsAvailable() {
        return isChatSessionsAvailable;
    }

    public void setChatSessionsAvailable(boolean chatSessionsAvailable) {
        isChatSessionsAvailable = chatSessionsAvailable;
    }

    public int getmDiscount() {
        return mDiscount;
    }

    public void setmDiscount(int mDiscount) {
        this.mDiscount = mDiscount;
    }

    public String getmTag() {
        return mTag;
    }

    public void setmTag(String mTag) {
        this.mTag = mTag;
    }

    public boolean isMatchGuarantee() {
        return isMatchGuarantee;
    }

    public void setMatchGuarantee(boolean matchGuarantee) {
        isMatchGuarantee = matchGuarantee;
    }
}
