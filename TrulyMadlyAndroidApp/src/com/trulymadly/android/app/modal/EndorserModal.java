/**
 * 
 */
package com.trulymadly.android.app.modal;

import com.trulymadly.android.app.utility.Utility;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * @author udbhav
 *
 */
public class EndorserModal implements Serializable {
	private static final long serialVersionUID = -8659823147785826949L;
	private String userId, fname, picUrl;

	private EndorserModal(String userId, String fname, String picUrl) {
		this.userId = userId;
		this.fname = fname;
		this.picUrl = picUrl;
	}

	public static EndorserModal createNewEndorser(JSONObject data) {
		if (data == null) {
			return null;
		}
		String userId = data.optString("user_id");
		String fname = data.optString("fname");
		String pic = data.optString("pic");
		if (Utility.isSet(userId) && Utility.isSet(fname) && Utility.isSet(pic)) {
			return new EndorserModal(userId, fname, pic);
		} else {
			return null;
		}
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

}
