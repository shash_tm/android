package com.trulymadly.android.app.modal;

import android.content.res.Resources;

import com.crashlytics.android.Crashlytics;
import com.trulymadly.android.app.R;

import org.json.JSONArray;
import org.json.JSONException;

public class Height {
	private String id, name;

	private Height(String _id, String _name) {
		id = _id;
		name = _name;
	}

	public static Height[] getHeightArray(Resources aResources) {
		JSONArray heightJson;
		int length = 0;
		try {
			heightJson = new JSONArray(
					"[{\"key\": \"0\",\"value\": \"4 feet \"},{\"key\": \"1\",\"value\": \"4 feet 1 inch\"},{\"key\": \"2\",\"value\": \"4 feet 2 inches\"},{\"key\": \"3\",\"value\": \"4 feet 3 inches\"},{\"key\": \"4\",\"value\": \"4 feet 4 inches\"},{\"key\": \"5\",\"value\": \"4 feet 5 inches\"},{\"key\": \"6\",\"value\": \"4 feet 6 inches\"},{\"key\": \"7\",\"value\": \"4 feet 7 inches\"},{\"key\": \"8\",\"value\": \"4 feet 8 inches\"},{\"key\": \"9\",\"value\": \"4 feet 9 inches\"},{\"key\": \"10\",\"value\": \"4 feet 10 inches\"},{\"key\": \"11\",\"value\": \"4 feet 11 inches\"},"
					+ "{\"key\": \"12\",\"value\": \"5 feet \"},{\"key\": \"13\",\"value\": \"5 feet 1 inch\"},{\"key\": \"14\",\"value\": \"5 feet 2 inches\"},{\"key\": \"15\",\"value\": \"5 feet 3 inches\"},{\"key\": \"16\",\"value\": \"5 feet 4 inches\"},{\"key\": \"17\",\"value\": \"5 feet 5 inches\"},{\"key\": \"18\",\"value\": \"5 feet 6 inches\"},{\"key\": \"19\",\"value\": \"5 feet 7 inches\"},{\"key\": \"20\",\"value\": \"5 feet 8 inches\"},{\"key\": \"21\",\"value\": \"5 feet 9 inches\"},{\"key\": \"22\",\"value\": \"5 feet 10 inches\"},{\"key\": \"23\",\"value\": \"5 feet 11 inches\"},"
					+ "{\"key\": \"24\",\"value\": \"6 feet \"},{\"key\": \"25\",\"value\": \"6 feet 1 inch\"},{\"key\": \"26\",\"value\": \"6 feet 2 inches\"},{\"key\": \"27\",\"value\": \"6 feet 3 inches\"},{\"key\": \"28\",\"value\": \"6 feet 4 inches\"},{\"key\": \"29\",\"value\": \"6 feet 5 inches\"},{\"key\": \"30\",\"value\": \"6 feet 6 inches\"},{\"key\": \"31\",\"value\": \"6 feet 7 inches\"},{\"key\": \"32\",\"value\": \"6 feet 8 inches\"},{\"key\": \"33\",\"value\": \"6 feet 9 inches\"},{\"key\": \"34\",\"value\": \"6 feet 10 inches\"},{\"key\": \"35\",\"value\": \"6 feet 11 inches\"},"
					+ "{\"key\": \"36\",\"value\": \"7 feet \"},{\"key\": \"37\",\"value\": \"7 feet 1 inch\"},{\"key\": \"38\",\"value\": \"7 feet 2 inches\"},{\"key\": \"39\",\"value\": \"7 feet 3 inches\"},{\"key\": \"40\",\"value\": \"7 feet 4 inches\"},{\"key\": \"41\",\"value\": \"7 feet 5 inches\"},{\"key\": \"42\",\"value\": \"7 feet 6 inches\"},{\"key\": \"43\",\"value\": \"7 feet 7 inches\"},{\"key\": \"44\",\"value\": \"7 feet 8 inches\"},{\"key\": \"45\",\"value\": \"7 feet 9 inches\"},{\"key\": \"46\",\"value\": \"7 feet 10 inches\"},{\"key\": \"47\",\"value\": \"7 feet 11 inches\"}]");
			if (heightJson != null) {
				length = heightJson.length();
			}
			Height[] height = new Height[length + 1];
			height[0] = new Height("-1", aResources.getString(R.string.height));
			for (int i = 0; i < heightJson.length(); i++) {
				height[i + 1] = new Height(heightJson.optJSONObject(i)
						.optString("key"), heightJson.optJSONObject(i)
						.optString("value"));
			}
			return height;
		} catch (JSONException e) {
			Crashlytics.logException(e);
			Height[] height = new Height[1];
			height[0] = new Height("-1", aResources.getString(R.string.height));
			return height;
		}

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String toString() {
		return (this.name);
	}

}
