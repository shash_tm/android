package com.trulymadly.android.app.modal;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by deveshbatra on 3/8/16.
 */
public class VenueModal implements Serializable {
    private String location;
    private String mutualMatchMessage;
    private String name;
    private String id;
    private String banner;
    private String distance;
    private Boolean goingStatus;
    private String date;
    private String noOfFollowers;
    private ArrayList<FollowerModal> followers;
    private String multipleLocationText;
    private boolean isNewEvent, isTMEvent;
    private String mTMMessage;

    public boolean isNewEvent() {
        return isNewEvent;
    }

    public void setNewEvent(boolean newEvent) {
        isNewEvent = newEvent;
    }

    public boolean isTMEvent() {
        return isTMEvent;
    }

    public void setTMEvent(boolean TMEvent) {
        isTMEvent = TMEvent;
    }

    public String getmTMMessage() {
        return mTMMessage;
    }

    public void setmTMMessage(String mTMMessage) {
        this.mTMMessage = mTMMessage;
    }

    public String getNoOfFollowers() {
        return noOfFollowers;
    }

    public void setNoOfFollowers(String noOfFollowers) {
        this.noOfFollowers = noOfFollowers;
    }

    public String getMultipleLocationText() {
        return multipleLocationText;
    }

    public void setMultipleLocationText(String multipleLocationText) {
        this.multipleLocationText = multipleLocationText;
    }


    public ArrayList<FollowerModal> getFollowers() {
        return followers;
    }

    public void setFollowers(ArrayList<FollowerModal> followers) {
        this.followers = followers;
    }


//
//    public String getDistanceUnits() {
//        return distanceUnits;
//    }
//
//    public void setDistanceUnits(String distanceUnits) {
//        this.distanceUnits = distanceUnits;
//    }
//
//    private String distanceUnits;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Boolean getGoingStatus() {
        return goingStatus;
    }

    public void setGoingStatus(Boolean goingStatus) {
        this.goingStatus = goingStatus;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getMutualMatchMessage() {
        return mutualMatchMessage;
    }

    public void setMutualMatchMessage(String mutualMatchMessage) {
        this.mutualMatchMessage = mutualMatchMessage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }


}
