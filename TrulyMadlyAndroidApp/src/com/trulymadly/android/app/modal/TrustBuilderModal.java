package com.trulymadly.android.app.modal;

import java.util.ArrayList;

public class TrustBuilderModal {
    private String fbConn;
    private int linkedInConn;
    private String phnNum;
    private String addressProof;
    private String idProof;
    private int trustScore;
    private Boolean isEndorsementVerified;
    private ArrayList<EndorserModal> endorsers;

    public TrustBuilderModal() {
    }

    public String getFBConn() {
        return this.fbConn;
    }

    public void setFBConn(String fbConn) {
        this.fbConn = fbConn;
    }

    public int getLinkedInConn() {
        return this.linkedInConn;
    }

    public void setLinkedInConn(int linkedInConn) {
        this.linkedInConn = linkedInConn;
    }

    public String getPhnNum() {
        return this.phnNum;
    }

    public void setPhnNum(String phnNum) {
        this.phnNum = phnNum;
    }

    public String getAddressProof() {
        return this.addressProof;
    }

    public void setAddressProof(String addressProof) {
        this.addressProof = addressProof;
    }

    public String getIDProof() {
        return this.idProof;
    }

    public void setIDProof(String idProof) {
        this.idProof = idProof;
    }

    public int getTrustScore() {
        return this.trustScore;
    }

    public void setTrustScore(int trustScore) {
        this.trustScore = trustScore;
    }

    public Boolean getIsEndorsementVerified() {
        return isEndorsementVerified;
    }

    public void setIsEndorsementVerified(Boolean isEndorsementVerified) {
        this.isEndorsementVerified = isEndorsementVerified;
    }

    public ArrayList<EndorserModal> getEndorsers() {
        return endorsers;
    }

    public void setEndorsers(ArrayList<EndorserModal> endorsers) {
        this.endorsers = endorsers;
    }

}
