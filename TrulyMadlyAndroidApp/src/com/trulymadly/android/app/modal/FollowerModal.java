package com.trulymadly.android.app.modal;

import java.io.Serializable;

/**
 * Created by deveshbatra on 4/22/16.
 */
public class FollowerModal implements Serializable {
    private String image, id;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
