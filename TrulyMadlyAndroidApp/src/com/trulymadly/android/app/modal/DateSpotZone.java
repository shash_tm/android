package com.trulymadly.android.app.modal;

public class DateSpotZone {


    private String zoneId;
    private String name;
    private boolean isSelected;

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    @SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
    @Override
    public boolean equals(Object o) {
        return o != null && this.getZoneId().equals(((DateSpotZone) o).getZoneId());
    }
}
