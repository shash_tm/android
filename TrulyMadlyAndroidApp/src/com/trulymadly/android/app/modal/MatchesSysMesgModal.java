package com.trulymadly.android.app.modal;

public class MatchesSysMesgModal {

	private String firstTileMsg;
	private String firstTileLink;
	private String lastTileMsg;
	private String lastTileLink;
	private String likeHideActionMsg;
	private boolean showNonAuthenticSurvey;
	private int nonAuthenticSurveyTileCount;
	private String popularityToolTipText;
    private boolean validProfilePic, noPhotos, likedInThePast;

	private String nonMutualLikeMsg;

	public MatchesSysMesgModal() {
	}

	public void setFirstTileMsg(String firstTileMsg) {
		this.firstTileMsg = firstTileMsg;
	}

	public void setFirstTileLink(String firstTileLink) {
		this.firstTileLink = firstTileLink;
	}

	public void setLastTileMsg(String lastTileMsg) {
		this.lastTileMsg = lastTileMsg;
	}

	public void setLastTileLink(String lastTileLink) {
		this.lastTileLink = lastTileLink;
	}

	public void setLikeHideActionMsg(String likeHideActionMsg) {
		this.likeHideActionMsg = likeHideActionMsg;
	}

	public void setNonMutualLikeMsg(String nonMutualLikeMsg) {
		this.nonMutualLikeMsg = nonMutualLikeMsg;
	}

	public String getFirstTileMsg() {
		return this.firstTileMsg;
	}

	public String getFirstTileLink() {
		return this.firstTileLink;
	}

	public String getLastTileMsg() {
		return this.lastTileMsg;
	}

	public String getLastTileLink() {
		return this.lastTileLink;
	}

	public String getLikeHideActionMsg() {
		return this.likeHideActionMsg;
	}

	public String getNonMutualLikeMsg() {
		return this.nonMutualLikeMsg;
	}

	public boolean isShowNonAuthenticSurvey() {
		return showNonAuthenticSurvey;
	}

	public void setShowNonAuthenticSurvey(boolean showNonAuthenticSurvey) {
		this.showNonAuthenticSurvey = showNonAuthenticSurvey;
	}

	public int getNonAuthenticSurveyTileCount() {
		return nonAuthenticSurveyTileCount;
	}

	public void setNonAuthenticSurveyTileCount(int nonAuthenticSurveyTileCount) {
		this.nonAuthenticSurveyTileCount = nonAuthenticSurveyTileCount;
	}

	public String getPopularityToolTipText() {
		return popularityToolTipText;
	}

	public void setPopularityToolTipText(String popularityToolTipText) {
		this.popularityToolTipText = popularityToolTipText;
	}

    public boolean isValidProfilePic() {
        return validProfilePic;
    }

    public void setValidProfilePic(boolean validProfilePic) {
        this.validProfilePic = validProfilePic;
    }

    public boolean isNoPhotos() {
        return noPhotos;
    }

    public void setNoPhotos(boolean noPhotos) {
        this.noPhotos = noPhotos;
    }

    public boolean isLikedInThePast() {
        return likedInThePast;
    }

    public void setLikedInThePast(boolean likedInThePast) {
        this.likedInThePast = likedInThePast;
    }
}
