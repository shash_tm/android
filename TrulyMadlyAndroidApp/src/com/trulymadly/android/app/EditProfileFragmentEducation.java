/**
 * 
 */
package com.trulymadly.android.app;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.crashlytics.android.Crashlytics;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.adapter.CustomSpinnerArrayAdapter;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.Constants.EditProfilePageType;
import com.trulymadly.android.app.listener.ConfirmDialogInterface;
import com.trulymadly.android.app.modal.Degree;
import com.trulymadly.android.app.modal.EditPrefBasicDataModal;
import com.trulymadly.android.app.modal.UserData;
import com.trulymadly.android.app.sqlite.EditPrefDBHandler;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.BadWordsHandler;
import com.trulymadly.android.app.utility.MoEHandler;
import com.trulymadly.android.app.utility.Utility;

import org.apmem.tools.layouts.FlowLayout;

import java.util.ArrayList;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author udbhav
 * 
 */
public class EditProfileFragmentEducation extends Fragment
		implements OnClickListener, OnItemSelectedListener, OnItemClickListener {

	private static final EditProfilePageType currentPageType = EditProfilePageType.EDUCATION;
	private final String collegeTextviewTag = "college_autocomplete_textview";
	private EditProfileActionInterface editProfileActionInterface;
	private Button continueButton;
	private ArrayList<String> colleges;
	private String highestDegree = null;
	private Spinner highestDegreeSpinner;
	private Context aContext;
	private Activity aActivity;
	private View addCollegeButton;
	private EditText collegeAutoCompleteTextView;
	private FlowLayout collegesContainer;
	private InputMethodManager imm;
	private LayoutInflater myInflater;
	private long startTime;

	private ArrayList<EditPrefBasicDataModal> highestDegreesList;
	private UserData userData;
	private ArrayList<String> badWordList;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			editProfileActionInterface = (EditProfileActivity) activity;
		} catch (ClassCastException e) {
			Crashlytics.logException(e);
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle data = getArguments();
		highestDegree = data.getString("highest_degree");
		colleges = data.getStringArrayList("colleges");
		userData = (UserData) data.getSerializable("userData");

		aContext = getActivity();
		aActivity = getActivity();
//		OnBadWordListRefreshedInterface onBadWordListUpdate = new OnBadWordListRefreshedInterface() {
//			@Override
//			public void onRefresh(ArrayList<String> badWordArrayList) {
//				badWordList = badWordArrayList;
//			}
//		};
		badWordList = BadWordsHandler.getBadWordList(aContext);

		highestDegreesList = EditPrefDBHandler.getBasicDataFromDB(Constants.EDIT_PREF_DEGREE, aContext, 3);
		MoEHandler.trackEvent(aContext, MoEHandler.Events.EDU_SCREEN_LAUNCHED);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		myInflater = inflater;
		Resources aResources = getResources();

		editProfileActionInterface.setHeaderText(aResources.getString(R.string.header_education));

		View view = inflater.inflate(R.layout.edit_profile_education, container, false);

		continueButton = (Button) view.findViewById(R.id.continue_education);
		continueButton.setText(R.string.save);
		highestDegreeSpinner = (Spinner) view.findViewById(R.id.degree_spinner);

		addCollegeButton = view.findViewById(R.id.edit_education_add_college);

		collegeAutoCompleteTextView = (EditText) view.findViewById(R.id.autocomplete_new_college);
		TextWatcher collegeNameTextWatcher = new TextWatcher() {
			private String lastString = "";

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (s.toString().trim().equals(lastString)) {
					return;
				}
				lastString = s.toString().trim();
				if (s.length() == 0) {
					addCollegeButton.setVisibility(View.INVISIBLE);
				} else if (BadWordsHandler.hasBadWord(aActivity, s.toString(), badWordList, R.string.bad_word_text_occupation, collegeAutoCompleteTextView)) {

				} else {
					addCollegeButton.setVisibility(View.VISIBLE);
					addCollegeButton.invalidate();
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {

			}
		};

		collegesContainer = (FlowLayout) view.findViewById(R.id.colleges_container);
		if (colleges == null) {
			colleges = new ArrayList<>();
		} else {
			for (int i = 0; i < colleges.size(); i++) {
				displayNewCollege(colleges.get(i));
			}
		}

		imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

		continueButton.setOnClickListener(this);
		addCollegeButton.setOnClickListener(this);
		addCollegeButton.setOnClickListener(this);
		highestDegreeSpinner.setOnItemSelectedListener(this);
		collegeAutoCompleteTextView.addTextChangedListener(collegeNameTextWatcher);

		((EditText) view.findViewById(R.id.autocomplete_new_college))
				.setOnEditorActionListener(new OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
						if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))
								|| (actionId == EditorInfo.IME_ACTION_DONE)) {
							addNewCollege(collegeAutoCompleteTextView.getText().toString());
							validateEducationData(false);
						}
						return false;
					}
				});
		createDegreeSpinner(Degree.getDegrees(highestDegreesList, aResources));

		setStartTime();
		return view;

	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		editProfileActionInterface.onViewCreated();
	}

	private void setStartTime() {
		startTime = (new java.util.Date()).getTime();
	}

	private void setEndTime() {
		long endTime = (new java.util.Date()).getTime();
		long time_diff = endTime - startTime;
		TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyActivities.edit_profile,
				TrulyMadlyEventTypes.register_education, time_diff, null, null, true);

	}

	@Override
	public void onClick(final View v) {
		int id = v.getId();
		Object tag = v.getTag();

		switch (id) {
		case R.id.continue_education:
			addNewCollege(collegeAutoCompleteTextView.getText().toString());
			if (validateEducationData(false)) {
				Bundle responseData = new Bundle();
				responseData.putString("highest_degree", highestDegree);
				responseData.putStringArrayList("colleges", colleges);
				responseData.putSerializable("userData", userData);
				setEndTime();
				editProfileActionInterface.processFragmentSaved(currentPageType, responseData);
			}
			break;
		case R.id.edit_education_add_college:
			addNewCollege(collegeAutoCompleteTextView.getText().toString());
			validateEducationData(false);
			break;
		default:
			break;
		}

		if (tag != null && ((String) tag).equalsIgnoreCase(collegeTextviewTag)) {
			final String collegeName = ((TextView) v).getText().toString();
			//i18n
			AlertsHandler.showConfirmDialog(aContext, "Are you sure you want to remove " + collegeName + " from the list?",
                    R.string.remove, R.string.cancel, new ConfirmDialogInterface() {

                        @Override
                        public void onPositiveButtonSelected() {
                            removeCollege(collegeName, v);
                        }

                        @Override
                        public void onNegativeButtonSelected() {
                        }
                    }, false);
		}

	}

	private void removeCollege(String collegeName, View collegeTv) {
		if (colleges != null) {
			for (int i = 0; i < colleges.size(); i++) {
				if (colleges.get(i).equalsIgnoreCase(collegeName)) {
					colleges.remove(i);
					break;
				}
			}
		}
		if (collegeTv != null && collegeTv.getParent() != null) {
			((ViewGroup) collegeTv.getParent()).removeView(collegeTv);
		}
		validateEducationData(false);
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View v, int pos, long id) {
		switch (parent.getId()) {
		case R.id.degree_spinner:
			highestDegree = ((Degree) highestDegreeSpinner.getSelectedItem()).getId();
			break;
		default:
			break;
		}
		validateEducationData(false);
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {

	}

	private Boolean validateEducationData(boolean showError) {
		Boolean isValid = true;
		int errorMessage = 0;
		if (highestDegree == null || highestDegree.equalsIgnoreCase("0")) {
			isValid = false;
			errorMessage = R.string.please_select_highest_degree;
		}
		if (!isValid && showError) {
			AlertsHandler.showMessage(getActivity(), errorMessage);
		}
		continueButton.setEnabled(isValid);

		if (colleges.size() > 0) {
			collegesContainer.setVisibility(View.VISIBLE);
			collegeAutoCompleteTextView.setHint(R.string.add_more_colleges);
		} else {
			collegesContainer.setVisibility(View.GONE);
			collegeAutoCompleteTextView.setHint(R.string.enter_most_recent_colleges_first);
		}
		return isValid;

	}

	private void addNewCollege(String newCollegeName) {
		newCollegeName = newCollegeName.trim();
		if (newCollegeName.length() > 0) {
			imm.hideSoftInputFromWindow(continueButton.getWindowToken(), 0);
			if(BadWordsHandler.hasBadWord(aActivity, newCollegeName, badWordList, 0, null)) {
				return;
			}
			if (isCollegeAdded(newCollegeName)) {
				AlertsHandler.showMessage(getActivity(), R.string.college_already_added);
			} else {
				colleges.add(newCollegeName);
				displayNewCollege(newCollegeName);
			}
		}
	}

	private void displayNewCollege(String newCollegeName) {

		TextView tv = (TextView) myInflater.inflate(R.layout.custom_item_1, null);

		tv.setTag(collegeTextviewTag);
		tv.setText(newCollegeName);
		tv.setOnClickListener(this);

		collegesContainer.addView(tv);

		collegeAutoCompleteTextView.setText("");

	}

	private boolean isCollegeAdded(String newCollegeName) {
		for (String collegeName : colleges) {
			if (Utility.stringCompare(checkNotNull(collegeName), newCollegeName)) {
				return true;
			}
		}
		return false;
	}

	private void createDegreeSpinner(Degree[] degrees) {
		CustomSpinnerArrayAdapter spinnerDegreeAdapter = new CustomSpinnerArrayAdapter(getActivity(),
				R.layout.custom_spinner_2, degrees);
		spinnerDegreeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		highestDegreeSpinner.setAdapter(spinnerDegreeAdapter);

		if (highestDegree != null) {
			for (int i = 0; i < degrees.length; i++) {
				if (degrees[i].getId().equalsIgnoreCase(highestDegree)) {
					if (spinnerDegreeAdapter.getCount() > i)
						highestDegreeSpinner.setSelection(i);
					break;
				}
			}
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
	}

}
