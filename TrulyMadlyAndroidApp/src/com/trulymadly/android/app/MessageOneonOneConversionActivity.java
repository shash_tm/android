package com.trulymadly.android.app;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.moe.pushlibrary.MoEHelper;
import com.rockerhieu.emojicon.EmojiconEditText;
import com.rockerhieu.emojicon.EmojiconGridFragment;
import com.rockerhieu.emojicon.EmojiconsFragment;
import com.rockerhieu.emojicon.emoji.Emojicon;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.MessageModal.MessageType;
import com.trulymadly.android.app.adapter.MessageOneOnOneConversationAdapter;
import com.trulymadly.android.app.bus.BackPressed;
import com.trulymadly.android.app.bus.BusProvider;
import com.trulymadly.android.app.bus.ChatMessageReadEvent;
import com.trulymadly.android.app.bus.ChatMessageReceivedEvent;
import com.trulymadly.android.app.bus.ChatTypingReceivedEvent;
import com.trulymadly.android.app.bus.GCMMessageReceivedEvent;
import com.trulymadly.android.app.bus.ImageUploadedEvent;
import com.trulymadly.android.app.bus.ImageUploadedProgress;
import com.trulymadly.android.app.bus.NudgeMatchesClickEvent;
import com.trulymadly.android.app.bus.QuizClickEvent;
import com.trulymadly.android.app.bus.QuizLaunchNudgeListEvent;
import com.trulymadly.android.app.bus.SendNudgeToMatchEvent;
import com.trulymadly.android.app.bus.SendQuizMessageEvent;
import com.trulymadly.android.app.bus.ServiceEvent;
import com.trulymadly.android.app.bus.ServiceEvent.SERVICE_EVENT_TYPE;
import com.trulymadly.android.app.bus.SimpleAction;
import com.trulymadly.android.app.bus.StickerClickEvent;
import com.trulymadly.android.app.bus.TriggerChatUpdateEvent;
import com.trulymadly.android.app.bus.TriggerClearChatEvent;
import com.trulymadly.android.app.bus.TriggerConnectingEvent;
import com.trulymadly.android.app.bus.TriggerQuizStatusRefreshEvent;
import com.trulymadly.android.app.custom.SlideListener;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.Constants.FetchSource;
import com.trulymadly.android.app.json.Constants.HttpRequestType;
import com.trulymadly.android.app.json.Constants.MessageState;
import com.trulymadly.android.app.json.ConstantsRF;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.json.ConstantsSocket;
import com.trulymadly.android.app.json.ConstantsSocket.SOCKET_STATE;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.listener.AlertDialogInterface;
import com.trulymadly.android.app.listener.ConfirmDialogInterface;
import com.trulymadly.android.app.listener.CustomOkHttpResponseHandler;
import com.trulymadly.android.app.listener.GetMetaDataCallbackInterface;
import com.trulymadly.android.app.listener.OnActionBarClickedInterface;
import com.trulymadly.android.app.listener.ShowGalleryInterface;
import com.trulymadly.android.app.listener.ThreeButtonDialogInterface;
import com.trulymadly.android.app.modal.BlockReason;
import com.trulymadly.android.app.modal.CommonAnswersFragmentModal;
import com.trulymadly.android.app.modal.CuratedDealsChatsModal;
import com.trulymadly.android.app.modal.EventChatModal;
import com.trulymadly.android.app.modal.MatchMessageMetaData;
import com.trulymadly.android.app.modal.NudgeClass;
import com.trulymadly.android.app.modal.NudgeListFragmentModal;
import com.trulymadly.android.app.modal.QuizNudgeSendModal;
import com.trulymadly.android.app.modal.SparkModal;
import com.trulymadly.android.app.modal.StickerData;
import com.trulymadly.android.app.modal.TakeQuizFragmentModal;
import com.trulymadly.android.app.services.ImageSharingService;
import com.trulymadly.android.app.sqlite.MessageDBHandler;
import com.trulymadly.android.app.sqlite.QuizDBHandler;
import com.trulymadly.android.app.sqlite.RFHandler;
import com.trulymadly.android.app.sqlite.SparksDbHandler;
import com.trulymadly.android.app.sqlite.StickerDBHandler;
import com.trulymadly.android.app.utility.ActionBarHandler;
import com.trulymadly.android.app.utility.ActivityHandler;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.FilesHandler;
import com.trulymadly.android.app.utility.ImageUploader;
import com.trulymadly.android.app.utility.MoEHandler;
import com.trulymadly.android.app.utility.NotificationHandler;
import com.trulymadly.android.app.utility.OkHttpHandler;
import com.trulymadly.android.app.utility.PermissionsHelper;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.SparkTimer;
import com.trulymadly.android.app.utility.SparksHandler;
import com.trulymadly.android.app.utility.TimeUtils;
import com.trulymadly.android.app.utility.TmLogger;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;
import com.trulymadly.android.app.utility.WebviewHandler;
import com.trulymadly.android.chat.CommonAnswersFragment;
import com.trulymadly.android.chat.NudgeMatchesFragment;
import com.trulymadly.android.chat.OnActionBarMenuItemClickedClass;
import com.trulymadly.android.chat.QuizFragment;
import com.trulymadly.android.chat.StickerFragment;
import com.trulymadly.android.chat.TakeQuizFragment;
import com.trulymadly.android.socket.SocketHandler;

import net.frakbot.jumpingbeans.JumpingBeans;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.socket.client.Ack;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities.sparks;

public class MessageOneonOneConversionActivity extends AppCompatActivity
        implements OnClickListener, EmojiconGridFragment.OnEmojiconClickedListener, EmojiconsFragment.OnEmojiconBackspaceClickedListener {


    public static final String ACTIVITY_NAME = "MessageOneOnOne";
    private static final int SHARE_TUTORIAL_MINIMUM_COUNT_LIMIT = 15;
    private static boolean isResumed = false;
    //Photo Message - END
    private final String TAG = "Message One on One Conversion";
    @BindView(R.id.webview_include_view)
    View mWebviewIncludeView;
    @BindView(R.id.empty)
    TextView empty_message;
    @BindView(R.id.doodle_image)
    ImageView doodle_image;
    @BindView(R.id.connecting_container)
    View connecting_container;
    @BindView(R.id.connecting_text)
    TextView connecting_text;
    @BindView(R.id.connecting_progressbar)
    ProgressBar connecting_progressbar;
    @BindView(R.id.message_one_on_one_list)
    RecyclerView mMessageRV;
    @BindView(R.id.cd_tutorial_overlay)
    View mCDTutorialOverlay;
    @BindView(R.id.cd_tutorial_overlay_iv)
    ImageView mCDTutorialOverlayIV;
    @BindView(R.id.cd_tutorial_close_iv)
    ImageView mCDTutorialCloseIV;
    @BindView(R.id.text_to_add)
    EmojiconEditText message_editor;
    @BindView(R.id.send_button)
    ImageView sendButton;
    @BindView(R.id.send_photo_button)
    ImageView sendPhotoButton;
    @BindView(R.id.profile_pic_rejected_toast)
    ImageView profile_pic_rejected_toast;
    @BindView(R.id.quiz_button)
    ImageView quizButton;
    @BindView(R.id.plus_button)
    ImageView stickerButton;
    @BindView(R.id.emoticon_button)
    ImageView emoticonButton;
    @BindView(R.id.typing_indicator_dots_jumping_tv)
    TextView typing_indicator_dots_jumping_tv;
    @BindView(R.id.sticker_fragment_container)
    FrameLayout sticker_fragment_container;
    @BindView(R.id.new_quiz_icon)
    ImageView new_quiz_icon;
    @BindView(R.id.new_gallery_icon)
    ImageView new_gallery_icon;
    @BindView(R.id.share_profile_tutorial_icon)
    ImageView share_profile_tutorial_icon;
    @BindView(R.id.share_tutorial_dialog_bg_view)
    View share_tutorial_dialog_bg_view;
    @BindView(R.id.share_profile_tutorial)
    View share_profile_tutorial;
    @BindView(R.id.typing_indicator)
    View typing_indicator;
    //Photo Message - START
    @BindView(R.id.show_photo_slider_layout)
    View show_photo_slider_layout;
    private String user_tstamp;
    private String match_tstamp;
    private boolean isFromFeedbackPage = false;
    private boolean cameFromPushNotification = false;
    private boolean isUnread = false;
    private String mCameraFileUri;
    private Context aContext;
    private Activity aActivity;
    private ActionBarHandler actionBarHandler;
    //    private MessageOneonOneConversationCursorAdapter_OLD messageListAdapter;
    private MessageOneOnOneConversationAdapter messageListAdapter;
    private String match_id;
    private String message_one_one_conversion_url;
    private MatchMessageMetaData matchMessageMetaData;
    private OnActionBarMenuItemClickedClass onActionBarMenuItemClicked;
    private OnActionBarClickedInterface onActionBarClickedInterface = null;
    private ScheduledThreadPoolExecutor pollingHandler;
    private Handler typingIndicatorHandler = null;
    private Runnable typingIndicatorRunnable = null;
    private boolean nudgeFromChat, nudgeFromCommonAnswers;
    private CURRENT_FRAGMENT fragmentSelected = CURRENT_FRAGMENT.none;
    private Menu menu;
    private MoEHelper mhelper;
    private int current_poll_counter = 0;
    private int error_counter = 1;
    private TakeQuizFragment localTakeQuizFragment = null;
    private QuizFragment localQuizFragment = null;
    //    private FragmentManager fragmentManager;
    private StickerFragment localStickerFragment = null;
    private EmojiconsFragment localEmoticonFragment = null;
    private CommonAnswersFragment localCommonAnswersFragment = null;
    private ProgressDialog mProgressDialog = null;
    private OnClickListener retryMessageClickListener,
            onQuizItemInChatClickedListener, onCDChatItemClickedListener, onBlogLinkItemInChatClickedListener;
    private NudgeMatchesFragment localNudgeListFragment;
    private ConstantsSocket.SOCKET_STATE socketState = SOCKET_STATE.POLLING;

    private boolean mShowSendPhotoButton = true, isCuratedDealsEnabled = false, allowPhotoShareInChat = false, mEnableSendPhotoOnMissTm = true;
    private MatchMessageMetaData.CDClickableState mCDCdClickableState =
            MatchMessageMetaData.CDClickableState.DISABLED;
    private ShowGalleryInterface showGalleryInterface;
    private Boolean isCDChatTutorialShown = null, isCDTutorialVisible = false, isAnyTutorialShownInThisSession = false;
    private WebviewHandler mWebviewHandler = null;
    //Curated deals End

    //Load in slots
    private int mCurrentSlotSize = Constants.DEFAULT_SLOT_NUMBER;
    private int mCurrentPosition = -1;
    private int mOldRecordsCount = -1;
    private int mFirstItemOffset = 0;
    private LinearLayoutManager mLinearLayoutManager;
    private OnClickListener mOnLoadMoreClickedListener;
    private boolean isLoadMoreEnabled = false;

    private boolean isImageLinkWebviewVisible = false;
    private boolean isShareTutorialVisible = false, isPhotoUploadSliderVisible = false;
    private OnClickListener onEventChatItemClickedListener;
    private boolean isInitialized = false;
    private boolean isDoodleHidden = false;

    private Handler mHandler;
    private Runnable sticker_fragment_container_visiblity_runnable, showQuizOverlayFragmentTakeQuizFragmentRunnable,
            showQuizOverlayFragmentNudgeMatchesFragmentRunnable, photoRejectedRunnable;
    private boolean isFemale = false;

    private SparkModal mSparkModal;
    private SparkTimer mSparkTimer;
    private SparkTimer.SparkTimerInterface mSparkTimerInterface;
    private SparksHandler.SparksReceiver mSparksReceiverHandler;
    private SparksHandler.SparksReceiver.SparksReceiverListener mSparksReceiverListener;
    private SlideListener mSlideListener;
    private ArrayList<MessageModal> mPendingMessageModalsSparks;
    private boolean isFromSpark = false;
    private boolean isProfileViewed = false;
    private JumpingBeans mJumpingBeans;
    private boolean isTypingJumpingbeansVisible = false;
    private boolean isMessageDeletedOnce = false;
    private JSONArray mBlockReasons, mBlockEditableFlags;
    private Runnable mExpiryRunnable;

    public static boolean isChatActiveForId(Context context, String id) {
        return (isResumed
                && Utility.stringCompare(SPHandler.getString(context,
                ConstantsSP.SHARED_KEYS_CURRENT_OPENED_MESSAGE_ID), id));
    }

    /* static method to process single message, parse it and save it in db */
    public static MessageModal processSingleMessage(MessageModal messageObj,
                                                    final Context ctx) {

        final String matchId = messageObj.getMatch_id();
        if (!Utility.isSet(matchId)) {
            return null;
        }
        MatchMessageMetaData matchMessageMetaData_thread = MessageDBHandler
                .getMatchMessageMetaData(Utility.getMyId(ctx), matchId, ctx);

        if (!Utility.isSet(matchMessageMetaData_thread.getLastFetchedTstamp())
                || messageObj.getTstamp().compareTo(matchMessageMetaData_thread
                .getLastFetchedTstamp()) > 0) {
            matchMessageMetaData_thread
                    .setLastFetchedTstamp(messageObj.getTstamp());
            matchMessageMetaData_thread
                    .setLastFetchedId(messageObj.getMsg_id());
            MessageDBHandler.insertMessageDetails(matchMessageMetaData_thread,
                    ctx);
        }
        if (messageObj.getMessageState() == MessageState.INCOMING_DELIVERED
                && matchMessageMetaData_thread
                .getLastChatReadSentTstamp() != null
                && messageObj.getTstamp()
                .compareTo(TimeUtils
                        .getFormattedTime(matchMessageMetaData_thread
                                .getLastChatReadSentTstamp())) <= 0) {
            messageObj.setMessageState(MessageState.INCOMING_READ);
        }
        if (Utility
                .isSet(matchMessageMetaData_thread.getLastSeenReceiverTstamp())
                && messageObj.getTstamp().compareTo(matchMessageMetaData_thread
                .getLastSeenReceiverTstamp()) <= 0) {
            if (messageObj.getMessageState() == MessageState.OUTGOING_DELIVERED
                    || messageObj
                    .getMessageState() == MessageState.OUTGOING_SENT) {
                messageObj.setMessageState(MessageState.OUTGOING_READ);
            }
        }

        boolean didMessageExist = MessageDBHandler.insertAMessage(ctx,
                Utility.getMyId(ctx), messageObj, true, FetchSource.SOCKET, true);

        if (!messageObj.isMessageSentByMe() && !didMessageExist) {
            return messageObj;
        } else {
            return null;
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.message_one_one_conversion_layout);

        //For reducing overdraws : Suggested by Romain guy
        //http://www.curious-creature.com/2012/12/01/android-performance-case-study/
        getWindow().setBackgroundDrawable(null);
        ButterKnife.bind(this);
        Utility.disableScreenShot(this);

        aContext = this;
        aActivity = this;

        mHandler = new Handler();

        //mFirstItemOffset is the offset value used when we click on load_more in chat. It is calculated by calculating two values:
        //1. LOAD_MORE item height as indicated by R.dimen.chat_load_more_height
        //2. DATE item (comes as TODAY or any other date right above the item) height as indicated by R.dimen.chat_date_height + 2*R.dimen.chat_date_margin (as there is top as well as bottom margin)
        //We need to modify this whenever we are adding/removing padding/margin/height of the above mentioned items
        mFirstItemOffset = (int) getResources().getDimension(R.dimen.chat_load_more_height)
                + (int) getResources().getDimension(R.dimen.chat_date_height)
                + (int) (2 * getResources().getDimension(R.dimen.chat_date_margin));

        mLinearLayoutManager = new LinearLayoutManager(aContext);
        mLinearLayoutManager.setStackFromEnd(true);
        mMessageRV.setLayoutManager(mLinearLayoutManager);
//        message_list_view.addOnScrollListener(new PicassoOnScrollListener(aContext));
        mOnLoadMoreClickedListener = new OnClickListener() {
            @Override
            public void onClick(View v) {
                onLoadMoreClicked();
            }
        };

        showGalleryInterface = new ShowGalleryInterface() {
            @Override
            public void showBlooper() {
                showGalleryBlooper();
            }
        };

        isCuratedDealsEnabled = RFHandler.getBool(aContext,
                Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_CD_ENABLED);
        allowPhotoShareInChat = RFHandler.getBool(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_ALLOW_PHOTO_IN_CHAT);


        Picasso.with(this)
                .load(R.drawable.quiz_icon).noFade().into(quizButton);
        Picasso.with(this)
                .load(R.drawable.sticker_icon).noFade().into(stickerButton);
        Picasso.with(this)
                .load(R.drawable.smileys).noFade().into(emoticonButton);
//        Picasso.with(this)
//                .load(R.drawable.typing_indicator_dots).noFade().into(typing_indicator_dots_iv);

        mhelper = new MoEHelper(this);
        isFemale = !Utility.isMale(aContext);
        onActionBarClickedInterface = new OnActionBarClickedInterface() {
            @Override
            public void onUserProfileClicked() {
                if (Utility.isSet(matchMessageMetaData.getProfileUrl())) {
                    if (isSparkButNotMutualMatch()) {
                        isProfileViewed = true;
                        HashMap<String, String> eventInfo = new HashMap<>();
                        eventInfo.put("match_id", match_id);
                        eventInfo.put("source", "bar");
                        eventInfo.put("msg_rcd", mSparkModal.getmMessage());
                        eventInfo.put("time_left", String.valueOf(TimeUtils.getTimeoutDifference(
                                mSparkModal.getmStartTime(), mSparkModal.getmExpiredTimeInSeconds()*1000)/1000));
                        TrulyMadlyTrackEvent.trackEvent(aContext, sparks, TrulyMadlyEventTypes.chat, 0,
                                TrulyMadlyEvent.TrulyMadlyEventStatus.view_profile,
                                eventInfo, true, true);
                    }

                    ActivityHandler.startProfileActivity(aActivity,
                            matchMessageMetaData.getProfileUrl(), false, false, false);
                }
            }

            @Override
            public void onConversationsClicked() {
            }

            @Override
            public void onLocationClicked() {
            }

            @Override
            public void onBackClicked() {
                aActivity.onBackPressed();
            }

            @Override
            public void onTitleClicked() {
            }

            @Override
            public void onTitleLongClicked() {
            }

            @Override
            public void onSearchViewOpened() {
            }

            @Override
            public void onSearchViewClosed() {
            }

            @Override
            public void onCategoriesClicked() {

            }

            @Override
            public void onCuratedDealsClicked() {
                if (isCuratedDealsEnabled) {
                    HashMap<String, String> eventInfo = new HashMap<>();
                    eventInfo.put("nudge_shown", TrulyMadlyApplication.cdChatTutorialShownInThisSession ? "true" : "false");
                    TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyEvent.TrulyMadlyActivities.datespots,
                            TrulyMadlyEvent.TrulyMadlyEventTypes.icon_clicked, 0, match_id
                            , eventInfo, true);
                    switch (mCDCdClickableState) {
                        case DISABLED:
                            break;
                        case CLICKABLE:
                            ActivityHandler.startDateSpotListActivity(aActivity, match_id, message_one_one_conversion_url, false);
                            break;
                        case NOT_CLICKABLE:
                            AlertsHandler.showAlertDialog(aActivity, R.string.cd_not_clickable_message);
                            break;
                    }
                }
            }
        };

        connecting_progressbar.getIndeterminateDrawable().setColorFilter(
                getResources().getColor(R.color.white),
                android.graphics.PorterDuff.Mode.SRC_IN);

        sendButton.setOnClickListener(null);
        sendPhotoButton.setOnClickListener(null);
        stickerButton.setOnClickListener(null);
        emoticonButton.setOnClickListener(null);
        quizButton.setOnClickListener(this);
        share_profile_tutorial.setOnClickListener(this);
        show_photo_slider_layout.setOnClickListener(this);

//        message_list_view.setEmptyView(empty_message);
        message_editor.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    public void onFocusChange(View paramAnonymousView,
                                              boolean hasFocus) {
                        removeDefMessage();
                        hideAllFragments();
                    }
                });

        message_editor.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideAllFragments(true);
                return false;
            }
        });

        hideTypingIndicator();
        message_editor.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                mShowSendPhotoButton = s.toString().length() == 0;
                if (!mShowSendPhotoButton) {
                    sendTyping();
                }
                //Toggle SendButton to show the Curated Deals option
                toggleSendButton();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        retryMessageClickListener = new OnClickListener() {

            @Override
            public void onClick(View v) {
                retryMessage(v);

            }
        };

        onQuizItemInChatClickedListener = new OnClickListener() {

            @Override
            public void onClick(View v) {
                onQuizItemInChatClicked(v);

            }
        };

        // forcing menu to show.We can remove it now?
        Utility.showOptionsMenuByForce(aActivity);

        // adding db handlers
        pollingHandler = new ScheduledThreadPoolExecutor(1);
        // setting up the ids
        Intent i = getIntent();
        if (savedInstanceState != null) {
            // setString values from savedInstancestart into intent
            isMessageDeletedOnce = savedInstanceState.getBoolean("isMessageDeletedOnce", false);
            i.putExtra("isFromFeedbackPage",
                    savedInstanceState.getBoolean("isFromFeedbackPage"));
            i.putExtra("match_id", savedInstanceState.getString("match_id"));
            i.putExtra("message_one_one_conversion_url", savedInstanceState
                    .getString("message_one_one_conversion_url"));
            i.putExtra("isPush", savedInstanceState.getBoolean("isPush"));
            i.putExtra("isUnread", savedInstanceState.getBoolean("isUnread"));
            i.putExtra("isFromSpark", savedInstanceState.getBoolean("isFromSpark"));
            if (savedInstanceState.containsKey("camera_uri")) {
                i.putExtra("camera_uri", savedInstanceState.getString("camera_uri"));
            }
        }

        callingIntent(i);
        if (isFromSpark && !Utility.isSet(message_editor.getText().toString())) {
            message_editor.setText(aContext.getString(R.string.hi));
        }

        if (SPHandler.getBool(aContext,
                ConstantsSP.SHARED_KEYS_NEW_QUIZ_ICON)) {
            showQuizBlooper();
        }


        onCDChatItemClickedListener = new OnClickListener() {
            @Override
            public void onClick(View v) {
                onCDChatItemClicked(v);
            }
        };

        onEventChatItemClickedListener = new OnClickListener() {
            @Override
            public void onClick(View v) {
                onEventChatItemClicked(v);
            }
        };

        mCDTutorialOverlay.setOnClickListener(null);
        mCDTutorialCloseIV.setOnClickListener(this);


        sticker_fragment_container.setVisibility(View.GONE);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(-1,
                (int) (0.4D * UiUtils.getScreenHeight(aActivity)));
        sticker_fragment_container.setLayoutParams(params);
        onBlogLinkItemInChatClickedListener = new OnClickListener() {
            @Override
            public void onClick(View v) {
                onBlogLinkChatItemClicked(v);
            }
        };

        try {
            mBlockReasons = new JSONArray(
                    "[\"Inappropriate or offensive content.\",\"Misleading profile information.\",\"Others\"]");
            mBlockEditableFlags = new JSONArray(
                    "[false,true,true]");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    /**
     * Toggles the send button icon depending on the boolean mShowSendPhotoButton
     */
    private void toggleSendButton() {
        int photoIcon, sendIcon;
        if (this.socketState == SOCKET_STATE.FAILED ||
                this.socketState == SOCKET_STATE.CONNECTING) {
            sendIcon = R.drawable.send_icon_disabled;
            photoIcon = R.drawable.send_photo_icon_disabled;
        } else {
            sendIcon = R.drawable.send_icon_blue;
            photoIcon = R.drawable.send_photo_icon;
        }

        //show send button
        //If miss tm and photo upload not allowed via backend user flag, then show send button
        if (allowPhotoShareInChat && !isSparkButNotMutualMatch()
                && mShowSendPhotoButton && mEnableSendPhotoOnMissTm && !isFromFeedbackPage) {
            sendPhotoButton.setVisibility(View.VISIBLE);
            Picasso.with(this)
                    .load(photoIcon).noFade().into(sendPhotoButton);
            sendButton.setVisibility(View.GONE);
        } else {
            sendPhotoButton.setVisibility(View.GONE);
            sendButton.setVisibility(View.VISIBLE);
            Picasso.with(this)
                    .load(sendIcon).noFade().into(sendButton);
        }
    }

    private void enableCuratedDealsIcon() {

        actionBarHandler.toggleCuratedDealsView(isCuratedDealsEnabled, mCDCdClickableState);
        if (isCuratedDealsEnabled) {
            showCDChatTutorial();
        }
    }


    private boolean isCDChatTutorialShown() {
        if (!isCuratedDealsEnabled) {
            return true;
        }
        if (isCDChatTutorialShown == null) {
            isCDChatTutorialShown = RFHandler.getBool(aContext,
                    Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_CD_CHAT_TUTORIAL_SHOWN);
        }
        return isCDChatTutorialShown;
    }

    /**
     * Shows the tutorial for the first time users of curated deals feature
     */
    private void showCDChatTutorial() {
        if (!isAnyTutorialShownInThisSession && !isCDChatTutorialShown()) {
            //int minTwoWayConversationCount = MessageDBHandler.getMinTwoWayConversationCount(aContext, Utility.getMyId(aContext), matchMessageMetaData.getMatchId());
//            if (minTwoWayConversationCount > SHARE_TUTORIAL_MINIMUM_COUNT_LIMIT) {
            Picasso.with(aContext).load(R.drawable.cross).into(mCDTutorialCloseIV);
            Picasso.with(aContext).load(R.drawable.cd_chat_tutorial).into(mCDTutorialOverlayIV, new Callback() {
                @Override
                public void onSuccess() {
                    AlphaAnimation fadeIn = new AlphaAnimation(0, 1);
                    fadeIn.setDuration(1000);
                    fadeIn.setFillAfter(true);
                    mCDTutorialOverlay.setVisibility(View.VISIBLE);
                    mCDTutorialOverlay.startAnimation(fadeIn);
                    isCDTutorialVisible = true;

                    RFHandler.insert(aContext, Utility.getMyId(aContext),
                            ConstantsRF.RIGID_FIELD_CD_CHAT_TUTORIAL_SHOWN, true);
                    isCDChatTutorialShown = true;
                    isAnyTutorialShownInThisSession = true;
                    HashMap<String, String> eventInfo = new HashMap<>();
                    eventInfo.put("nudge_shown", TrulyMadlyApplication.cdChatTutorialShownInThisSession ? "true" : "false");
                    TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyActivities.datespots,
                            TrulyMadlyEventTypes.tutorial, 0, match_id
                            , eventInfo, true);
                }

                @Override
                public void onError() {

                }
            });
            TrulyMadlyApplication.cdChatTutorialShownInThisSession = true;
//            }
        }
    }

    private void hideCDChatTutorial() {
        if (!isCDTutorialVisible)
            return;
        AlphaAnimation fadeOut = new AlphaAnimation(1, 0);
        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mCDTutorialOverlay.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        fadeOut.setDuration(500);
        mCDTutorialOverlay.startAnimation(fadeOut);
        isCDTutorialVisible = false;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu m) {
        menu = m;
        getMenuInflater().inflate(R.menu.action_bar_side_menu, menu);
        updateActionBar();

        boolean showShareView = !Utility.isMale(aContext);
        showShareTutorial(showShareView);

        return true;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        aContext = this;
        super.onNewIntent(intent);
        setIntent(intent);
        callingIntent(intent);
        removeOptionsMenu();
        hideAllFragments();
        resumeActivity(false);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean("isFromFeedbackPage", isFromFeedbackPage);
        outState.putString("match_id", match_id);
        if (Utility.isSet(mCameraFileUri)) {
            outState.putString("camera_uri", mCameraFileUri);
        }
        outState.putString("message_one_one_conversion_url",
                message_one_one_conversion_url);
        outState.putBoolean("isPush", cameFromPushNotification);
        outState.putBoolean("isFromSpark", isFromSpark);
        outState.putBoolean("isMessageDeletedOnce", isMessageDeletedOnce);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStart() {
        super.onStart();
        BusProvider.getInstance().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        BusProvider.getInstance().unregister(this);
    }

    @Override
    protected void onDestroy() {
        if (typingIndicatorHandler != null) {
            typingIndicatorHandler.removeCallbacks(typingIndicatorRunnable);
        }
        if (mHandler != null) {
            mHandler.removeCallbacks(sticker_fragment_container_visiblity_runnable);
            mHandler.removeCallbacks(showQuizOverlayFragmentTakeQuizFragmentRunnable);
            mHandler.removeCallbacks(showQuizOverlayFragmentNudgeMatchesFragmentRunnable);
            mHandler.removeCallbacks(photoRejectedRunnable);
        }
        super.onDestroy();
    }

    @OnClick(R.id.share_profile_tutorial_button)
    public void onShareTutorialShareClicked() {
        hideShareTutorial();
        if (onActionBarMenuItemClicked != null) {
            onActionBarMenuItemClicked.onShareProfileClicked();
        }

    }

    @OnClick({R.id.take_from_camera_layout, R.id.add_from_gallery_layout})
    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.share_profile_tutorial:
                hideShareTutorial();
                break;
            case R.id.show_photo_slider_layout:
                hidePhotoSlidingMenu();
                break;
            case R.id.send_photo_button:
                if (isPhotoUploadSliderVisible) {
                    hidePhotoSlidingMenu();
                } else {
                    TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyActivities.photo_share,
                            TrulyMadlyEventTypes.icon_clicked, 0, match_id, null,
                            true);
                    showPhotoSlidingMenu();
                }
                break;
            case R.id.send_button:
                if (this.socketState != SOCKET_STATE.FAILED &&
                        this.socketState != SOCKET_STATE.CONNECTING) {
                    addMessage(message_editor.getText().toString().trim(),
                            MessageType.TEXT, 0, null, true, null);
                }
                break;
            case R.id.plus_button:
                hideGalleryBlooper();
                if (fragmentSelected == CURRENT_FRAGMENT.localStickerFragment) {
                    hideAllFragments();
                } else {
                    showStickerFragment();
                }
                break;

            case R.id.emoticon_button:
                if (fragmentSelected == CURRENT_FRAGMENT.localEmoticonFragment) {
                    hideAllFragments();
                } else {
                    showEmoticonFragment();
                }
                break;
            case R.id.quiz_button:
                hideQuizBlooper();
                if (fragmentSelected == CURRENT_FRAGMENT.localQuizFragment) {
                    hideAllFragments();
                } else {
                    showQuizFragment();
                }
                break;
            case R.id.cd_tutorial_close_iv:
                hideCDChatTutorial();
                break;

            //PHOTO SHARING- START
            case R.id.take_from_camera_layout:
                hidePhotoSlidingMenu();
                if (!ImageSharingService.isRunning()) {
                    if (PermissionsHelper.checkAndAskForPermission(aActivity, new String[]{android.Manifest.permission.CAMERA,
                                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            PermissionsHelper.REQUEST_CAMERA_STORAGE_PERMISSIONS_CODE)) {
                        launchCamera();
                    }
                } else {
                    AlertsHandler.showMessage(this, R.string.cant_upload_document_yet, false);
                }
                break;
            case R.id.add_from_gallery_layout:
                hidePhotoSlidingMenu();
                if (!ImageSharingService.isRunning()) {
                    if (PermissionsHelper.checkAndAskForPermission(aActivity, android.Manifest.permission.READ_EXTERNAL_STORAGE,
                            PermissionsHelper.REQUEST_STORAGE_PERMISSIONS_CODE)) {
                        launchGallery();
                    }
                } else {
                    AlertsHandler.showMessage(this, R.string.cant_upload_document_yet, false);
                }
                break;
            //PHOTO SHARING- END
        }
    }

    private void launchCamera() {
        mCameraFileUri = FilesHandler.getFilePath(match_id,
                Utility.generateRandomFileName(Utility.getMyId(aContext), match_id) +
                        (Utility.isWebPSupported() ? ".webp" : ".jpg"), aContext);
//                "Pending_" + FilesHandler.getUniqueFileName(match_id, "jpg"), aContext);
        ImageUploader.getImage(this, ImageUploader.IMAGE_SOURCE.camera, mCameraFileUri);
    }

    private void launchGallery() {
        ImageUploader.getImage(this, ImageUploader.IMAGE_SOURCE.gallery, null);
    }

    //PHOTO SHARING- START

    @SuppressLint({"Override", "NewApi"})
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (grantResults == null || grantResults.length == 0) {
            return;
        }
        switch (requestCode) {
            case PermissionsHelper.REQUEST_CAMERA_STORAGE_PERMISSIONS_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && (grantResults.length == 1 || grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
                    launchCamera();
                } else {
                    boolean neverAskAgain = !ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[0]);
                    if (neverAskAgain) {
                        PermissionsHelper.handlePermissionDenied(this, R.string.permission_denied_image_sharing);
                    } else {
                        AlertsHandler.showMessageWithAction(this, R.string.permission_denied_image_sharing,
                                R.string.permission_rejection_action, new OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        PermissionsHelper.checkAndAskForPermission(aActivity,
                                                new String[]{android.Manifest.permission.CAMERA,
                                                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                                PermissionsHelper.REQUEST_CAMERA_STORAGE_PERMISSIONS_CODE);
                                    }
                                }, false, 3);
                    }
                }
                break;

            case PermissionsHelper.REQUEST_STORAGE_PERMISSIONS_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    launchGallery();
                } else {
                    boolean neverAskAgain = !ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[0]);
                    if (neverAskAgain) {
                        PermissionsHelper.handlePermissionDenied(this, R.string.permission_denied_image_sharing);
                    } else {
                        AlertsHandler.showMessageWithAction(this, R.string.permission_denied_image_sharing,
                                R.string.permission_rejection_action, new OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        PermissionsHelper.checkAndAskForPermission(aActivity, android.Manifest.permission.READ_EXTERNAL_STORAGE,
                                                PermissionsHelper.REQUEST_STORAGE_PERMISSIONS_CODE);
                                    }
                                }, false, 3);
                    }
                }
                break;

            case PermissionsHelper.INITIAL_STORAGE_PERMISSION_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    boolean neverAskAgain = !ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[0]);
                    if (neverAskAgain) {
                        PermissionsHelper.handlePermissionDenied(this, R.string.permission_denied_image_downloading);
                    } else {
                        AlertsHandler.showMessageWithAction(this, R.string.permission_denied_image_downloading,
                                R.string.permission_rejection_action, new OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        PermissionsHelper.checkAndAskForPermission(aActivity, android.Manifest.permission.READ_EXTERNAL_STORAGE,
                                                PermissionsHelper.INITIAL_STORAGE_PERMISSION_CODE);
                                    }
                                }, false, 3);
                    }
                }
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    @Subscribe
    public void onImageUploadedprogressReceived(ImageUploadedProgress imageUploadedProgress) {
        if (messageListAdapter != null) {
            messageListAdapter.updateProgress(imageUploadedProgress.getmMesageId(), imageUploadedProgress.getmProgress());
        }
    }

    @Subscribe
    public void onImageUploadedEventReceived(ImageUploadedEvent imageUploadedEvent) {
        MessageModal messageModal = MessageDBHandler.getAMessage(Utility.getMyId(aContext), imageUploadedEvent.getmMessageId(), aContext);
        if (imageUploadedEvent.ismStatus()) {
            if (messageModal != null) {
                sendMessageToServer(messageModal.getDisplayMessage(), messageModal.getMsg_id(),
                        messageModal.getMessageType(), false, messageModal.getQuiz_id(),
                        messageModal.getmCuratedDealsChatsModal(), messageModal.getmMetadataString());
            }
        } else {
            if (messageModal != null) {
                updateChatItem(imageUploadedEvent.getmMessageId());
//                updateChat(false);
            }
            if (Utility.isSet(imageUploadedEvent.getmMessage()))
                AlertsHandler.showMessage(this, imageUploadedEvent.getmMessage(), false);

        }
    }

    private void addAndUploadImage(String url, String messageId, MessageModal messageModal,
                                   String source) {
        if (Utility.isSet(messageId)) {
            if (messageModal == null) {
                messageModal = MessageDBHandler.getAMessage(Utility.getMyId(aContext), messageId, aContext);
            }
            if (messageModal != null) {
                addMessageForDisplay(messageModal.getMessage(), messageId, messageModal.getMessageType(),
                        false, messageModal.getQuiz_id(), messageModal.getmCuratedDealsChatsModal(), messageModal.getmMetadataString(), false);
            }
        } else {
            messageId = addMessage(aContext.getString(R.string.received_image), MessageType.IMAGE, 0, null, false,
                    MessageModal.prepareImageMetadataJsonString(null, null, null, null, url, null, null), false);
        }
        ImageUploader.onImageSelected(aContext, url, messageId, match_id, source);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) {
            mCameraFileUri = null;
            return;
        }

        String uri = null, source = null;
        switch (requestCode) {
            case ImageUploader.CAMERA_REQUEST_CODE:
                uri = mCameraFileUri;
                source = ImageUploader.SOURCE_CAMERA;
//                addAndUploadImage(mCameraFileUri, null, null, ImageUploader.SOURCE_CAMERA);
                mCameraFileUri = null;
                break;

            case ImageUploader.GALLERY_REQUEST_CODE:
                if (data != null && data.getScheme() != null && (data.getScheme().equalsIgnoreCase("content")
                        || data.getScheme().equalsIgnoreCase("file"))) {

                    Uri selectedImage = data.getData();
                    String filePath = FilesHandler.getPath(aContext, selectedImage, data.getScheme());
                    try {
                        File file = FilesHandler.copyFile(aContext, filePath, match_id,
                                Utility.generateRandomFileName(Utility.getMyId(aContext), match_id) +
                                        (Utility.isWebPSupported() ? ".webp" : ".jpg"));
//                                "Pending_" + FilesHandler.getUniqueFileName(match_id, "jpg"));
                        uri = file.getAbsolutePath();
                        source = ImageUploader.SOURCE_GALLERY;
//                        addAndUploadImage(localPath, null, null, ImageUploader.SOURCE_GALLERY);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {

                }
                break;

            case ImageUploader.PICK_FROM_SYSTEM_REQUEST_CODE:
                break;
        }

        if (Utility.isSet(uri)) {
            File file = new File(uri);
            if (file.length() > ImageUploader.MAX_FILE_SIZE) {
                AlertsHandler.showMessage(aActivity, R.string.max_size_8_mb, false);
                file.delete();
            } else {
                addAndUploadImage(uri, null, null, source);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    //PHOTO SHARING- END

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        actionBarHandler.onOptionsItemSelected(item);
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        TmLogger.d(TAG, "Backstack count : " + getSupportFragmentManager().getBackStackEntryCount());
        if (isShareTutorialVisible) {
            hideShareTutorial();
        } else if (isPhotoUploadSliderVisible) {
            hidePhotoSlidingMenu();
        } else if (isImageLinkWebviewVisible) {
            hideWebView();
        } else if (fragmentSelected == CURRENT_FRAGMENT.localTakeQuizFragment) {
            AlertsHandler.showConfirmDialog(aActivity, R.string.sure_leave_quiz,
                    R.string.no, R.string.yes, new ConfirmDialogInterface() {

                        @Override
                        public void onPositiveButtonSelected() {

                        }

                        @Override
                        public void onNegativeButtonSelected() {
                            hideAllFragments();
                        }
                    });
        } else if (fragmentSelected == CURRENT_FRAGMENT.localNudgeList) {
            showNudgeListCancelConfirmation(R.string.nudge_matches_later,
                    R.string.yes, R.string.no,
                    localNudgeListFragment.getQuizId(),
                    localNudgeListFragment.getQuizName());
        } else if (fragmentSelected == CURRENT_FRAGMENT.localCommonAnswersFragment
                || fragmentSelected == CURRENT_FRAGMENT.localStickerFragment
                || fragmentSelected == CURRENT_FRAGMENT.localQuizFragment
                || fragmentSelected == CURRENT_FRAGMENT.localEmoticonFragment) {
            hideAllFragments();

        } else if (fragmentSelected != CURRENT_FRAGMENT.none) {
            hideAllFragments();
            try {
                super.onBackPressed();
            } catch (IllegalStateException ignored) {

            }
        } else if (cameFromPushNotification) {
            ActivityHandler.startConversationListActivity(aActivity);
            finish();
        } else if (isCDTutorialVisible) {
            hideCDChatTutorial();
        } else {
            try {
                super.onBackPressed();
            } catch (IllegalStateException | NullPointerException ignored) {

            }
        }
    }

    private void hideWebView() {
        getWebViewHandler().hide();
        isImageLinkWebviewVisible = false;
    }

    private WebviewHandler getWebViewHandler() {
        if (mWebviewHandler == null) {
            WebviewHandler.WebviewActionsListener mWebviewActionsListener = new WebviewHandler.WebviewActionsListener() {
                @Override
                public boolean shouldOverrideUrlLoading(String url) {
                    mWebviewHandler.loadUrl(url);
                    return true;
                }

                @Override
                public void webViewHiddenOnUrlLoad() {
                    hideWebView();
                }

                @Override
                public void onWebViewCloseClicked() {
                    hideWebView();
                }
            };
            mWebviewHandler = new WebviewHandler(mWebviewIncludeView, mWebviewActionsListener, true, true);
        }
        return mWebviewHandler;
    }

    @Override
    protected void onPause() {
        stopPolling();
        isResumed = false;
        if(mHandler != null) {
            mHandler.removeCallbacks(mExpiryRunnable);
        }

        if(mSlideListener != null){
            mSlideListener.cancelTutorialAnimation();
        }

//        if (mSparkTimer != null) {
//            mSparkTimer.stop();
//        }
        // invalidateOptionsMenu();
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        UiUtils.hideKeyBoard(aActivity);
        isResumed = true;
        Utility.getMyId(aContext);
        resumeActivity(true);
    }

    private void onBlogLinkChatItemClicked(View v) {
        MessageModal blogLinkMessageModal = (MessageModal) v.getTag();
        TmLogger.d(blogLinkMessageModal.getMessageType().toString(), blogLinkMessageModal.getMsgJsonString());
        TmLogger.d(blogLinkMessageModal.getMessageType().toString(), blogLinkMessageModal.getaLinkImageUrl());
        isImageLinkWebviewVisible = true;
        getWebViewHandler().loadUrl(blogLinkMessageModal.getaLinkLandingUrl());
        TrulyMadlyTrackEvent.trackEvent(aContext,
                TrulyMadlyActivities.message_full_conversation,
                TrulyMadlyEventTypes.image_link_click, 0,
                blogLinkMessageModal.getaLinkLandingUrl(), null, true);
        UiUtils.hideKeyBoard(aActivity);
    }

    private void onCDChatItemClicked(View v) {
        MessageModal cdMessageModal = (MessageModal) v.getTag();
        CuratedDealsChatsModal curatedDealsChatsModal = cdMessageModal.getmCuratedDealsChatsModal();
        if (curatedDealsChatsModal != null) {
            ActivityHandler.startDateSpotActivity(aActivity, curatedDealsChatsModal.getmDateSpotId(),
                    curatedDealsChatsModal.getmDealId(),
                    match_id, message_one_one_conversion_url);
        }
    }

    private void onEventChatItemClicked(View v) {
        MessageModal cdMessageModal = (MessageModal) v.getTag();
        EventChatModal eventChatModal = cdMessageModal.getmEventChatModal();
        if (eventChatModal != null) {
            ActivityHandler.startEventProfileActivity(aActivity, eventChatModal.getmEventId(), ACTIVITY_NAME, false);
        }
    }

    private void onQuizItemInChatClicked(View v) {

        MessageModal quizMessageModal = (MessageModal) v.getTag();
        int quiz_id = quizMessageModal.getQuiz_id();
        String quiz_name = QuizDBHandler.getQuizName(aContext, quiz_id);
        if (!quizMessageModal.isMessageSentByMe()) {
            String oldStatus = QuizDBHandler.getQuizStatus(aContext,
                    Utility.getMyId(aContext),
                    match_id, quiz_id);

            if (oldStatus == null)
                oldStatus = "NONE";

            String newStatus = "MATCH";
            if (oldStatus.equalsIgnoreCase("NONE")
                    || oldStatus.equalsIgnoreCase("USER")) {
                if (oldStatus.equalsIgnoreCase("USER")) {
                    newStatus = "BOTH";
                }
                QuizDBHandler.updateQuizStatus(aContext,
                        Utility.getMyId(aContext),
                        match_id, quiz_id, newStatus);
            }
        }
        if (Utility.isSet(quiz_name)) {
            showNextFragment(new QuizClickEvent(quiz_id, quiz_name, true,
                    "NONE", TrulyMadlyEventTypes.quiz_item_in_chat_clicked));
        }

    }

    private void retryMessage(final View v) {
        final MessageModal retryMessageModal = (MessageModal) v.getTag();
        ThreeButtonDialogInterface threeButtonDialogInterface = new ThreeButtonDialogInterface() {

            @Override
            public void onPositiveButtonSelected() {
                v.setOnClickListener(null);

                //1. If MessageType != IMAGE OR
                //2. If MessageType == IMAGE AND JPegUrl is setString i.e. image has been uploaded
                if (retryMessageModal.getMessageType() != MessageType.IMAGE
                        || Utility.isSet(retryMessageModal.getmJpegUrl())) {
                    addMessageForDisplay(retryMessageModal.getDisplayMessage(),
                            retryMessageModal.getMsg_id(),
                            retryMessageModal.getMessageType(), true,
                            retryMessageModal.getQuiz_id(), retryMessageModal.getmCuratedDealsChatsModal(),
                            retryMessageModal.getmMetadataString());
                }
                //If Jpeg Url is not setString i.e. image is not yet uploaded
                else if (!ImageSharingService.isRunning()) {
                    addAndUploadImage(retryMessageModal.getmLocalUrl(), retryMessageModal.getMsg_id(),
                            retryMessageModal, ImageUploader.SOURCE_RETRY);
                } else {
                    AlertsHandler.showMessage(aActivity, R.string.cant_upload_document_yet, false);
                }
            }

            @Override
            public void onNeutralButtonSelected() {
            }

            @Override
            public void onNegativeButtonSelected() {
                v.setOnClickListener(null);
                MessageDBHandler.deleteAMessage(Utility.getMyId(aContext), match_id,
                        retryMessageModal.getMsg_id(), aContext);
                removeChatItem(retryMessageModal.getMsg_id());
//                updateChat(false);
            }
        };
        AlertsHandler.showThreeButtonDialog(aActivity, "Your message was not sent.",
                "Retry", "Delete", "Cancel", threeButtonDialogInterface, true);

    }

    private void callingIntent(Intent i) {
        isFemale = !Utility.isMale(aContext);
        if (i != null) {
            Bundle b = i.getExtras();
            if (b != null) {
                isFromFeedbackPage = b.getBoolean("isFeedbackForm");

                // Filtering admin and not admin variables.
                if (isFromFeedbackPage) {
                    match_id = "admin";
                    message_one_one_conversion_url = ConstantsUrls.get_feedback_url();
                    empty_message.setText(getResources()
                            .getString(R.string.empty_feedback_message));
                    setFooterIcons(isFromFeedbackPage, false, false);
                } else {
                    match_id = b.getString("match_id");
                    message_one_one_conversion_url = b
                            .getString("message_one_one_conversion_url");
                    isFromSpark = b.getBoolean("isFromSpark");

                    LinearLayoutManager layoutManager = (LinearLayoutManager) mMessageRV.getLayoutManager();
//                    if(isSparkButNotMutualMatch()){
                    if (isFromSpark) {
                        layoutManager.setStackFromEnd(false);
                    } else {
                        layoutManager.setStackFromEnd(true);
                    }
                    mMessageRV.setLayoutManager(layoutManager);

                    setFooterIcons(isFromFeedbackPage,
                            matchMessageMetaData != null && matchMessageMetaData.isMissTm(),
                            isFromSpark || isSparkButNotMutualMatch());
                }

                mCurrentSlotSize = 1;
                toggleLoadMore();

                SPHandler.setString(this,
                        ConstantsSP.SHARED_KEYS_CURRENT_OPENED_MESSAGE_ID, match_id);
                cameFromPushNotification = b.getBoolean("isPush", false);
                isUnread = b.getBoolean("isUnread", false);
                if (b.containsKey("camera_uri")) {
                    mCameraFileUri = b.getString("camera_uri");
                }
            }
        }

        MessageDBHandler.markAllSendingMessagesAsNotSent(Utility.getMyId(this),
                match_id, this, ImageSharingService.isRunning());

        if (SocketHandler.isSocketEnabled(this)) {
            GetMetaDataCallbackInterface onMetaDataReceived = new GetMetaDataCallbackInterface() {

                @Override
                public void onMetaDataReceived() {
                    loadMetaDataAndShowChat(false, false);

                }
            };
            JSONArray match_ids = new JSONArray();
            match_ids.put(match_id);
            Utility.fireServiceBusEvent(aContext,
                    new ServiceEvent(SERVICE_EVENT_TYPE.GET_META_DATA,
                            match_ids, onMetaDataReceived));
        }
        stopPolling();
    }

    private void toggleLoadMore() {
        int mTotalRecords = MessageDBHandler.getTotalRecords(Utility.getMyId(aContext), match_id, aContext);
        isLoadMoreEnabled = mTotalRecords > mCurrentSlotSize * Constants.DEFAULT_SLOT_SIZE;
        if (isLoadMoreEnabled) {
            if (messageListAdapter != null) {
                messageListAdapter.toggleLoadMore(true);
            }
        } else {
            if (messageListAdapter != null) {
                messageListAdapter.toggleLoadMore(false);
            }
        }
    }

    private void processIntentForCDMessage() {
        Intent intent = getIntent();
        Bundle b = intent.getExtras();
        if (b != null && b.containsKey(Constants.KEY_CD_MODAL)) {
            CuratedDealsChatsModal curatedDealsChatsModal = b.getParcelable(Constants.KEY_CD_MODAL);
            addMessage(curatedDealsChatsModal.getmMessage(), curatedDealsChatsModal.getmMessageType(), 0,
                    curatedDealsChatsModal, false, null);
            intent.removeExtra(Constants.KEY_CD_MODAL);
        }
    }

    private void processIntentForEventMessage() {
        Intent intent = getIntent();
        Bundle b = intent.getExtras();
        if (b != null && b.containsKey(Constants.KEY_EVENT_MODAL)) {
            EventChatModal eventChatModal = b.getParcelable(Constants.KEY_EVENT_MODAL);
            if (eventChatModal != null) {
                addMessage(eventChatModal.prepareMessage(aContext), MessageType.SHARE_EVENT, 0,
                        null, false, MessageModal.prepareEventMetadataJsonString(eventChatModal));
                intent.removeExtra(Constants.KEY_EVENT_MODAL);
            }
        }
    }

    private void processIntentForSparkAcceptMessage() {
        Intent intent = getIntent();
        Bundle b = intent.getExtras();
        if (b != null && b.containsKey(Constants.KEY_SPARK_ACCEPT_MESSAGE)) {
            String message = b.getString(Constants.KEY_SPARK_ACCEPT_MESSAGE);
            if (Utility.isSet(message)) {
                addMessage(message, MessageType.TEXT, 0, null, false, null);
                intent.removeExtra(Constants.KEY_SPARK_ACCEPT_MESSAGE);
            }
        }
    }

    private void loadMetaDataAndShowChat(boolean fromUiThread, boolean forceSendChatRead) {
        isInitialized = true;
        matchMessageMetaData = MessageDBHandler
                .getMatchMessageMetaData(Utility.getMyId(aContext), match_id, aContext);
        if (isSparkButNotMutualMatch()) {
            mSparkModal = SparksDbHandler.getSpark(aContext, matchMessageMetaData.getUserId(), matchMessageMetaData.getMatchId());
        }

        String url = message_one_one_conversion_url;
        if (fromUiThread) {
            onActionBarMenuItemClicked = new OnActionBarMenuItemClickedClass(
                    this, matchMessageMetaData, url != null ? url : matchMessageMetaData.getMessage_link(),
                    TrulyMadlyActivities.message_full_conversation);
            updateActionBar();

            setCursorAndAdapter();
            sendChatRead(match_id, matchMessageMetaData.getLastFetchedId(),
                    matchMessageMetaData.getLastFetchedTstamp(),
                    forceSendChatRead);

            setBlockOptions(mBlockReasons, mBlockEditableFlags);

        } else {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    if (onActionBarMenuItemClicked != null) {
                        onActionBarMenuItemClicked
                                .setMetaData(matchMessageMetaData);
                    }
                    setHeaderDetails();
                }
            });
        }

    }

    private SparksHandler.SparksReceiver getSparkReceiverhandler() {
        if (mSparksReceiverListener == null) {
            mSparksReceiverListener = new SparksHandler.SparksReceiver.SparksReceiverListener() {

                @Override
                public void loadSpark(SparkModal sparkModal) {

                }

                @Override
                public void onSparkfetchSuccess(int sparksFetched) {

                }

                @Override
                public void onSparkfetchFailure() {

                }

                @Override
                public void onSparkUpdateFailed(String userId, String matchId, SparkModal.SPARK_STATUS sparkStatus) {
                    if (mSparkModal != null && userId.equalsIgnoreCase(mSparkModal.getmUserId()) && matchId.equalsIgnoreCase(mSparkModal.getmMatchId())) {
                        switch (sparkStatus) {
                            case accepted:
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (mPendingMessageModalsSparks != null) {
                                            for (MessageModal messageModal : mPendingMessageModalsSparks) {
                                                sendMessageFailed(checkNotNull(messageModal).getMsg_id());
                                            }
                                            mPendingMessageModalsSparks.clear();
                                        }
                                    }
                                });
                                break;
                            case rejected:
                                break;
                        }
                    }
                }

                @Override
                public void onSparkUpdateSuccess(String userId, String matchId, SparkModal.SPARK_STATUS sparkStatus) {
//                    AlertsHandler.showMessage(aActivity, "Spark update success");
                    if (mSparkModal != null && userId.equalsIgnoreCase(mSparkModal.getmUserId()) && matchId.equalsIgnoreCase(mSparkModal.getmMatchId())) {
                        switch (sparkStatus) {
                            case accepted:
                                acceptSpark();
                                break;
                            case rejected:
                                rejectSpark();
                                break;
                        }
                    }
                }

            };
        }
        if (mSparksReceiverHandler == null) {
            mSparksReceiverHandler = new SparksHandler.SparksReceiver(aContext, mSparksReceiverListener);
        }

        return mSparksReceiverHandler;
    }

    private void updateSparkStatus(SparkModal.SPARK_STATUS sparkStatus, MessageModal messageObj) {
        if (sparkStatus == SparkModal.SPARK_STATUS.rejected) {
            Bundle bundle = new Bundle();
            bundle.putString("remove_spark", mSparkModal.getmMatchId());
            ActivityHandler.startConversationListActivity(aContext, bundle);
            if (mSparkTimer != null) {
                mSparkTimer.stop();
            }
            finish();
            return;
        }
        getSparkReceiverhandler().updateSparkStatus(mSparkModal, sparkStatus);
    }

    private boolean rejectSpark() {
        if (isSparkButNotMutualMatch()) {
//            if (mSparkTimer != null) {
//                mSparkTimer.stop();
//            }
            if(mHandler != null) {
                mHandler.removeCallbacks(mExpiryRunnable);
            }
            finish();
            return true;
        }
        return false;
    }

    private void acceptSpark() {
        if (isSparkButNotMutualMatch()) {
//            if (mSparkTimer != null) {
//                mSparkTimer.stop();
//            }
            if(mHandler != null) {
                mHandler.removeCallbacks(mExpiryRunnable);
            }
            isFromSpark = false;
            Intent intent = getIntent();
            if (intent != null) {
                intent.removeExtra("isFromSpark");
            }
            matchMessageMetaData.setMutualSpark(true);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
//                    if(mPendingMessageModalsSparks != null) {
//                        for (MessageModal messageModal : mPendingMessageModalsSparks) {
//                            sendMessageToServer(messageModal.getDisplayMessage(), messageModal.getMsg_id(),
//                                    messageModal.getMessageType(), false, messageModal.getQuiz_id(),
//                                    messageModal.getmCuratedDealsChatsModal(), messageModal.getmMetadataString());
//                        }
//                        mPendingMessageModalsSparks.clear();
//                    }
                    setFooterIcons(isFromFeedbackPage,
                            (matchMessageMetaData != null && matchMessageMetaData.isMissTm()),
                            isSparkButNotMutualMatch());
                    isCuratedDealsEnabled = RFHandler.getBool(aContext,
                            Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_CD_ENABLED);
                    matchMessageMetaData.setmCDClickableState(MessageDBHandler.isCuratedDealsClickable(
                            Utility.getMyId(aContext), matchMessageMetaData.getMatchId(), aContext));
                    mCDCdClickableState = matchMessageMetaData.getmCDClickableState();
                    enableCuratedDealsIcon();
                    mShowSendPhotoButton = message_editor.getText().length() == 0;
                    toggleSendButton();
                    messageListAdapter.hideSparkHeader();
                    setHeaderDetails();
                    toggleTimer();
                    toggleMenuOptionsVisibility(!isFromFeedbackPage && (matchMessageMetaData == null || !matchMessageMetaData.isMissTm()));
                }
            });
        }
    }

    private void startSparkTimer() {
        if (mSparkModal != null && !matchMessageMetaData.isMutualSpark()) {
//            if (mSparkTimer == null) {
//                mSparkTimerInterface = new SparkTimer.SparkTimerInterface() {
//                    @Override
//                    public void onTimeUpdated(final int mElapsedTimeInSeconds) {
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
////                                actionBarHandler.updateTimer((int) mSparkModal.getmExpiredTimeInSeconds() - mElapsedTimeInSeconds);
//                            }
//                        });
//                    }
//
//                    @Override
//                    public void onTimeExpired() {
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                if (matchMessageMetaData != null) {
//                                    SparkModal sparkModal = SparksDbHandler.getSpark(aContext, Utility.getMyId(aContext), match_id);
//                                    Bundle mBundle = null;
//                                    if (sparkModal != null) {
//                                        mBundle = new Bundle();
//                                        mBundle.putString("remove_spark", sparkModal.getmMatchId());
//                                        mBundle.putBoolean("call_api", false);
//                                    }
//                                    ActivityHandler.startConversationListActivity(aContext, mBundle);
//                                    finish();
//                                }
//                            }
//                        });
//                    }
//                };
//                mSparkTimer = new SparkTimer(1, mSparkModal.getmStartTime(), mSparkModal.getmExpiredTimeInSeconds() * 1000);
//            }
//
//            if (mSparkModal.getmStartTime() > 0) {
//                long timeLeftInMillis = TimeUtils.getTimeoutDifference(mSparkModal.getmStartTime(), mSparkModal.getmExpiredTimeInSeconds()*1000);
//                long timeLeftInSecs = timeLeftInMillis/1000;
//                int interval = 1;
//                if(timeLeftInSecs > 60*60){
//                    interval = 60;
//                }
//
//                mSparkTimer.resetValues(interval, mSparkModal.getmStartTime(), mSparkModal.getmExpiredTimeInSeconds() * 1000);
//                actionBarHandler.reconfigureTimer((int) mSparkModal.getmExpiredTimeInSeconds(), mSparkTimer.getElapsedTimeWrtStartTime());
//                mSparkTimer.start(mSparkTimerInterface);
//            }

            if(mSparkModal.getmStartTime() > 0) {
                long timeLeft = TimeUtils.getTimeoutDifference(
                        mSparkModal.getmStartTime(), mSparkModal.getmExpiredTimeInSeconds() * 1000);

                if (mExpiryRunnable == null) {
                    mExpiryRunnable = new Runnable() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (matchMessageMetaData != null) {
                                        SparkModal sparkModal = SparksDbHandler.getSpark(aContext, Utility.getMyId(aContext), match_id);
                                        Bundle mBundle = null;
                                        if (sparkModal != null) {
                                            mBundle = new Bundle();
                                            mBundle.putString("remove_spark", sparkModal.getmMatchId());
                                            mBundle.putBoolean("call_api", false);
                                        }
                                        ActivityHandler.startConversationListActivity(aContext, mBundle);
                                        finish();
                                    }
                                }
                            });
                        }
                    };
                } else {
                    mHandler.removeCallbacks(mExpiryRunnable);
                }
                mHandler.postDelayed(mExpiryRunnable, timeLeft);
            }
        } else {
            actionBarHandler.toggleTimer(false);
        }
    }

    private void sendChatRead(final String match_id,
                              final String last_fetched_id, final String last_fetched_tstamp,
                              boolean forceSendChatRead) {

        if (!SocketHandler.isSocketEnabled(aContext)) {
            return;
        }
        boolean toSendChatRead = forceSendChatRead;
        if (!forceSendChatRead) {
            toSendChatRead = MessageDBHandler.isAnyUnreadMessage(aContext,
                    Utility.getMyId(aContext), match_id);
        }

        if (toSendChatRead) {
            TmLogger.log(Log.DEBUG, TAG, "sending chat read");
            try {
                JSONObject data = new JSONObject();
                data.put("sender_id", match_id);
                // TODO: SOCKET: wrong last_fetched_tstamp is sent when there is no entry in meta table it is using local value instead of GMT
                data.put("last_seen_msg_id", last_fetched_id);
                data.put("last_seen_msg_tstamp", last_fetched_tstamp);
                Ack ack = new Ack() {

                    @Override
                    public void call(Object... args) {
                        if (args != null && args.length > 0
                                && args[0] != null) {
                            TmLogger.log(Log.DEBUG, "socketResponse",
                                    args[0].toString());
                            try {
                                JSONObject responseJson = (JSONObject) args[0];
                                chatReadComplete(match_id,
                                        responseJson.optString("tstamp"));
                            } catch (ClassCastException e) {
                                Crashlytics.log((String) args[0]);
                                Crashlytics.logException(e);
                            }
                        }
                    }
                };
                Utility.fireServiceBusEvent(aContext,
                        new ServiceEvent(SERVICE_EVENT_TYPE.EMIT,
                                ConstantsSocket.SOCKET_EMITS.chat_read, data, ack, true, 1));
            } catch (JSONException e) {
                Crashlytics.logException(e);
            }
        }
    }

    private void chatReadComplete(String match_id,
                                  String last_chat_read_sent_tstamp) {
        MessageDBHandler.setChatReadIncoming(aContext, Utility.getMyId(aContext),
                match_id);

        if (Utility.isSet(last_chat_read_sent_tstamp)) {
            MessageDBHandler.updateMessageMetaDataValue(aContext,
                    Utility.getMyId(aContext), match_id, "last_chat_read_sent_tstamp",
                    last_chat_read_sent_tstamp);
        }

    }

    private void removeOptionsMenu() {
        invalidateOptionsMenu();
    }

    private void stopPolling() {
        pollingHandler.shutdownNow();
    }

    private void startPolling() {
        current_poll_counter = 0;
        if (isPollingShut()) {
            pollingHandler = new ScheduledThreadPoolExecutor(1);
        }
        if (SocketHandler.isSocketEnabled(aContext)) {
            return;
        }
        pollServerForRealTimeChatting(true, false);
    }

    private boolean isPollingShut() {
        return pollingHandler.isShutdown() || pollingHandler.isTerminated()
                || pollingHandler.isTerminating();
    }

    private void checkAndRestartPolling() {
        current_poll_counter = 0;
        if (isResumed && isPollingShut()
                && !SocketHandler.isSocketEnabled(aContext)) {
            pollingHandler = new ScheduledThreadPoolExecutor(1);
            pollServerForRealTimeChatting(true, false);
        }
    }


    private void showStickerFragment() {
        TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyActivities.sticker,
                TrulyMadlyEventTypes.gallery_shown, 0, "default", null,
                true);
        UiUtils.hideKeyBoard(aActivity);
//        new Handler().postDelayed(
//                new Runnable() {
//                    @Override
//                    public void run() {
//                        setContainerParams(true);
        try {
            setContainerParams(true);
            localStickerFragment = new StickerFragment();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.sticker_fragment_container,
                            localStickerFragment,
                            StickerFragment.class.getSimpleName())
                    .commitAllowingStateLoss();
            fragmentSelected = CURRENT_FRAGMENT.localStickerFragment;


//
//                        } catch (IllegalStateException e) {
//                        }
//                    }
//                }, 1000
//        );
        } catch (IllegalStateException ignored) {
        }
    }

    private boolean isSoftKeyboardShown() {

        InputMethodManager imm = (InputMethodManager) aActivity
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        return imm.isAcceptingText();

    }

    private void setContainerParams(final boolean setDefaultHeight) {


        if (setDefaultHeight) {
            if (sticker_fragment_container_visiblity_runnable == null) {
                sticker_fragment_container_visiblity_runnable = new Runnable() {
                    @Override
                    public void run() {
                        sticker_fragment_container.setVisibility(View.VISIBLE);
                    }
                };
            }
            if (isSoftKeyboardShown()) {
                mHandler.postDelayed(sticker_fragment_container_visiblity_runnable, 300);
            } else {
                sticker_fragment_container.setVisibility(View.VISIBLE);
            }
//                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(-1,
//                            (int) (0.4D * UiUtils.getScreenHeight(aActivity)));
//                    sticker_fragment_container.setLayoutParams(params);
        } else {
            sticker_fragment_container.setVisibility(View.GONE);
//                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(-1, -2);
//                    sticker_fragment_container.setLayoutParams(params);
        }
    }


    private void showEmoticonFragment() {
        removeDefMessage();
        UiUtils.hideKeyBoard(aActivity);
//        setContainerParams(true);
        try {
            setContainerParams(true);
            localEmoticonFragment = EmojiconsFragment.newInstance(false);
//            Bundle bundle = new Bundle();
//            bundle.putBoolean("useSystemDefaults", false);
//            localEmoticonFragment.setArguments(bundle);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.sticker_fragment_container, localEmoticonFragment)
                    .commitAllowingStateLoss();
            fragmentSelected = CURRENT_FRAGMENT.localEmoticonFragment;


        } catch (IllegalStateException ignored) {
        }
    }

    private void showQuizFragment() {
        TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyActivities.quiz,
                TrulyMadlyEventTypes.quiz_icon_click, 0, null, null,
                false);
        UiUtils.hideKeyBoard(aActivity);
//        setContainerParams(true);
        Bundle data = new Bundle();
        data.putString("match_id", match_id);
        setContainerParams(true);
        localQuizFragment = new QuizFragment();
        localQuizFragment.setArguments(data);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.sticker_fragment_container, localQuizFragment,
                        QuizFragment.class.getSimpleName())
                .commitAllowingStateLoss();
        fragmentSelected = CURRENT_FRAGMENT.localQuizFragment;

    }

    /**
     * this will send message to the server and on successful response add that
     * in db as well
     *
     * @param message
     * @param quiz_id
     */
    private void sendMessageToServer(String message,
                                     final String random_message_id, final MessageType messageType,
                                     Boolean isRetry, int quiz_id,
                                     CuratedDealsChatsModal curatedDealsChatsModal, String messageJson) {

        hideDoodle();
        JSONObject metadata = MessageModal.prepareMetadataJsonObject(messageType, curatedDealsChatsModal,
                messageJson, isSparkButNotMutualMatch());

        HashMap<String, String> params = new HashMap<>();
        params.put("msg", message);
        params.put("type", "send_msg");
        params.put("message_type", messageType.toString());
        params.put("unique_id", random_message_id);
        params.put("quiz_id", quiz_id + "");

        if (metadata != null) {
            params.put("metadata", metadata.toString());
        }

        JSONObject paramsSocket = SocketHandler.prepareSocketParams(message, messageType,
                random_message_id, match_id, quiz_id, isFromFeedbackPage, isRetry, metadata);

        if (isSparkButNotMutualMatch()) {

//            match_id, sticker (true/false), def_msg (true/false), msg_rcd, msg_sent , time_left, profile_view (true false)
            HashMap<String, String> eventInfo = new HashMap<>();
            eventInfo.put("match_id", match_id);
            eventInfo.put("sticker", String.valueOf(messageType == MessageType.STICKER));
            eventInfo.put("def_msg", String.valueOf(getString(R.string.hi).equalsIgnoreCase(message)));
            eventInfo.put("msg_rcd", mSparkModal.getmMessage());
            eventInfo.put("msg_sent", message);
            eventInfo.put("profile_view", String.valueOf(isProfileViewed));
            TrulyMadlyTrackEvent.trackEvent(aContext, sparks, TrulyMadlyEventTypes.chat, 0,
                    TrulyMadlyEvent.TrulyMadlyEventStatus.message_sent,
                    eventInfo, true, true);


            params.put("update_spark", String.valueOf(true));
            params.put("hash", mSparkModal.getmHashKey());

            try {
                if (paramsSocket != null) {
                    paramsSocket.put("update_spark", String.valueOf(true));
                    paramsSocket.put("hash", mSparkModal.getmHashKey());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        CustomOkHttpResponseHandler okHttpResponseHandler = new CustomOkHttpResponseHandler(
                aContext) {
            @Override
            public void onRequestSuccess(JSONObject response) {
                if (response != null) {
                    TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyActivities.Socket, "chat_sent_polling", 0, "ack_received", null, false);
                    if (response.optJSONObject("data") != null && response
                            .optJSONObject("data").optBoolean("is_blocked")) {
                        sendMessageBlocked(random_message_id,
                                response.optJSONObject("data")
                                        .optString("match_id", match_id));
                    } else {
                        sendMessageSuccess(response, random_message_id, true);
                        checkAndRestartPolling();
                    }
                } else {
                    TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyActivities.Socket, "chat_sent_polling", 0, "ack_failed_null_response", null, false);
                    sendMessageFailed(random_message_id);
                }
            }

            @Override
            public void onRequestFailure(Exception exception) {
                TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyActivities.Socket, "chat_sent_polling", 0, "ack_failed", null, false);
                sendMessageFailed(random_message_id);
            }
        };

        Ack ack = new Ack() {
            @Override
            public void call(Object... args) {
                if (args != null && args.length > 0 && args[0] != null) {
                    TmLogger.log(Log.DEBUG, "socketResponse",
                            args[0].toString());
                    try {
                        JSONObject responseJson = (JSONObject) args[0];
                        if (Utility.stringCompare("blocked",
                                responseJson.optString("error"))) {
                            // Special case to handle blocked user chat read
                            sendMessageBlocked(random_message_id, responseJson
                                    .optString("match_id", match_id));
                        } else {
                            sendMessageSuccess(responseJson, random_message_id, true);
                        }
                    } catch (ClassCastException e) {
                        Crashlytics.log((String) args[0]);
                        Crashlytics.logException(e);
                    }
                } else {
                    sendMessageFailed(random_message_id);
                    TrulyMadlyApplication.forceRestartService(aContext,
                            ConstantsSocket.SOCKET_END.force_restart_on_chat_sent_failed);
                }
            }
        };

        if (SocketHandler.isSocketEnabled(aContext)) {
            Utility.fireServiceBusEvent(aContext,
                    new ServiceEvent(SERVICE_EVENT_TYPE.EMIT,
                            ConstantsSocket.SOCKET_EMITS.chat_sent, paramsSocket, ack, true,
                            1));
        } else {
            TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyActivities.Socket, "chat_sent_polling", 0, "sent", null, false);
            OkHttpHandler.httpPost(aContext, message_one_one_conversion_url,
                    params, okHttpResponseHandler);
        }
        SPHandler.remove(aContext,
                ConstantsSP.SHARED_KEYS_CHAT_TYPING_LAST_SENT_TIME);
    }

    private void sendTyping() {
        if (socketState != SOCKET_STATE.CONNECTED || !SocketHandler.isSocketEnabled(aContext)
                || !TimeUtils.isTimeoutExpired(aContext,
                ConstantsSP.SHARED_KEYS_CHAT_TYPING_LAST_SENT_TIME,
                Constants.CHAT_TYPING_THRESHOLD)) {
            return;
        }

        SPHandler.setString(aContext,
                ConstantsSP.SHARED_KEYS_CHAT_TYPING_LAST_SENT_TIME,
                TimeUtils.getFormattedTime());

        JSONObject paramsSocket = new JSONObject();
        try {
            paramsSocket.put("receiver_id", match_id);
        } catch (JSONException ignored) {
        }

        Ack ack = new Ack() {
            @Override
            public void call(Object... args) {
                if (args != null && args.length > 0 && args[0] != null) {
                    TmLogger.log(Log.DEBUG, "socketResponse",
                            args[0].toString());
                } else {
                    TmLogger.log(Log.DEBUG, "socketResponse", "NO DATA");
                }
            }
        };

        Utility.fireServiceBusEvent(aContext,
                new ServiceEvent(SERVICE_EVENT_TYPE.EMIT,
                        ConstantsSocket.SOCKET_EMITS.chat_typing, paramsSocket, ack, true, 1));
    }

    private void sendMessageFailed(final String message_id) {
        if (isResumed) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    AlertsHandler.showMessage(aActivity,
                            R.string.ERROR_NETWORK_FAILURE);
                    MessageDBHandler.markMessageAsNotSent(Utility.getMyId(aContext),
                            match_id, message_id, aContext);

                    updateChatItem(message_id);
//                    updateChat(false);
                }
            });
        }
    }

    private void sendMessageBlocked(final String message_id,
                                    final String match_id_to_block) {
        MessageDBHandler.markMessageAsNotSent(Utility.getMyId(aContext),
                match_id_to_block, message_id, aContext);
        if (isResumed && match_id.equals(match_id_to_block)) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    MessageDBHandler.setUserBlocked(aContext,
                            match_id_to_block);
                    AlertsHandler.showAlertDialog(aActivity,
                            getResources().getString(R.string.declined_txt),
                            new AlertDialogInterface() {
                                @Override
                                public void onButtonSelected() {
                                    ActivityHandler.startConversationListActivity(aActivity);
                                    finish();
                                }
                            });
                }
            });
        } else {
            sendMessageFailed(message_id);
        }
    }

    protected void sendMessageSuccess(JSONObject response,
                                      String random_message_id, boolean updateChat) {
        boolean isRemoved = false;
        String finalMessageId = null;
        String random_message_id_server = null;
        // for bad words
        if (!response.isNull("error")) {
            boolean e = response.optBoolean("error");
            if (e) {
                final String error_msg = response.optString("error_msg",
                        aContext.getResources()
                                .getString(R.string.bad_word_text));
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        AlertsHandler.showAlertDialog(aActivity, error_msg, null);
                    }
                });

                if (Utility.isSet(random_message_id)) {
                    isRemoved = true;
                    MessageDBHandler.deleteAMessage(Utility.getMyId(aContext), match_id,
                            random_message_id, aContext);
                }
            }
        } else {
            MessageModal messageObj = new MessageModal(Utility.getMyId(aContext),
                    response);
            if (!Utility.isSet(messageObj.getMatch_id())
                    || isFromFeedbackPage) {
                messageObj.setMatch_id(match_id);
            }
            random_message_id_server = response.optString("unique_id");
            if (Utility.isSet(random_message_id_server)) {
                if (messageObj.getMessageType() == MessageType.CD_VOUCHER) {
                    if (RFHandler.getBool(aContext, Utility.getMyId(aContext),
                            ConstantsRF.RIGID_FIELD_CD_PHONE_NUMBER_TAKEN))
                        AlertsHandler.showMessage(aActivity, R.string.voucher_generated_message, false);
                    else
                        AlertsHandler.showMessage(aActivity,
                                R.string.voucher_generated_message_without_number, false);
                }
                finalMessageId = messageObj.getMsg_id();
                MessageDBHandler.updateAMessage(aContext, Utility.getMyId(aContext),
                        messageObj, random_message_id_server,
                        FetchSource.LOCAL);
                showPhotoRejectedNudge();

                //Message sent successfully -> If it was a reply to a spark message
                //i.e. (if isSparkReceived = true and isMutualSpark is false)
                //then set isMutualSpark to true and toggle the menu options
                if (isSparkButNotMutualMatch()) {
//                    if(mSparkTimer != null) {
//                        mSparkTimer.stop();
//                    }
                    getSparkReceiverhandler().sparkStatusChanged(mSparkModal, SparkModal.SPARK_STATUS.accepted, messageObj);
                }


                JSONObject json = new JSONObject();
                try {
                    json.put("Match Id", match_id);
                } catch (JSONException ignored) {
                }

                MoEHandler.trackEvent(aContext, MoEHandler.Events.MESSAGE_SENT, json, true);
            }
        }

        if (updateChat) {
//            updateChat(false);
            if (isRemoved) {
                removeChatItem(random_message_id);
            } else if (Utility.isSet(finalMessageId)) {
                replaceChatItem(random_message_id_server, finalMessageId);
            }
        }
    }

    private void showPhotoRejectedNudge() {
        //check if female or if not miss or if from feedback page.
        if (!isFemale || matchMessageMetaData.isMissTm() || isFromFeedbackPage) {
            return;
        }
        // check is profile pic is rejected
        if (!SPHandler.getBool(aContext, ConstantsSP.SHARED_KEYS_PROFILE_PIC_REJECTED)) {
            return;
        }
        //check is the entire sequence is compelete
        if (SPHandler.getBool(aContext, ConstantsSP.SHARED_KEYS_PROFILE_PIC_REJECTED_NUDGE_COMPLETED)) {
            return;
        }
        ArrayList<String> profileNudgeRejectedNudgeShownOnMatchIds = new ArrayList<>();
        String value = SPHandler.getString(aContext, ConstantsSP.SHARED_KEYS_PROFILE_PIC_REJECTED_NUDGE_SHOWN_ON_MATCH_IDS);
        if (Utility.isSet(value)) {
            profileNudgeRejectedNudgeShownOnMatchIds = new ArrayList<>(Arrays.asList(value.split(",")));
        }
        //check if shown to current match
        if (profileNudgeRejectedNudgeShownOnMatchIds.contains(match_id)) {
            return;
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Animation shakeCameraIcon = AnimationUtils.loadAnimation(aContext, R.anim.shake);
                sendPhotoButton.startAnimation(shakeCameraIcon);
                profile_pic_rejected_toast.setVisibility(View.VISIBLE);
                Picasso.with(aContext).load(R.drawable.photo_rejected_camera_shake_nudge)
                        .into(profile_pic_rejected_toast);
                photoRejectedRunnable = new Runnable() {
                    @Override
                    public void run() {
                        profile_pic_rejected_toast.setVisibility(View.GONE);
                    }
                };
                mHandler.postDelayed(photoRejectedRunnable, 3000L);
            }
        });


        profileNudgeRejectedNudgeShownOnMatchIds.add(match_id);

        if (profileNudgeRejectedNudgeShownOnMatchIds.size() >= 3) {
            SPHandler.setBool(aContext, ConstantsSP.SHARED_KEYS_PROFILE_PIC_REJECTED_NUDGE_COMPLETED, true);
        }

        SPHandler.setString(aContext, ConstantsSP.SHARED_KEYS_PROFILE_PIC_REJECTED_NUDGE_SHOWN_ON_MATCH_IDS, TextUtils.join(",", profileNudgeRejectedNudgeShownOnMatchIds));

    }

    private void checkConnecting() {
        if (!Utility.isNetworkAvailable(getApplicationContext())) {
            showConnecting(SOCKET_STATE.FAILED, true);
        } else if (!SocketHandler.isSocketEnabled(aContext)) {
            if (!isUnread) {
                showConnecting(SOCKET_STATE.POLLING, false);
            }
        } else {
            Utility.fireServiceBusEvent(aContext,
                    new ServiceEvent(SERVICE_EVENT_TYPE.CHECK_CONNECTING));
        }
    }

    private void showConnecting(ConstantsSocket.SOCKET_STATE socketState, boolean showConnectingText) {
        this.socketState = socketState;
        switch (socketState) {
            case CONNECTING:
            case FAILED:
                actionBarHandler.toggleClearChat(false);
                connecting_container.setVisibility(View.VISIBLE);
                if (socketState == SOCKET_STATE.CONNECTING) {
                    connecting_progressbar.setVisibility(View.VISIBLE);
                    connecting_container.setBackgroundColor(getResources().getColor(R.color.connecting_bg_grey));
                    connecting_text.setText(R.string.connecting);
                    if (showConnectingText) {
                        connecting_text.setVisibility(View.VISIBLE);
                    } else {
                        connecting_text.setVisibility(View.GONE);
                    }

                } else if (socketState == SOCKET_STATE.FAILED) {
                    connecting_progressbar.setVisibility(View.GONE);
                    connecting_container.setBackgroundColor(getResources().getColor(R.color.net_failed_red));
                    connecting_text.setText(R.string.no_internet_connectivity);
                    connecting_text.setVisibility(View.VISIBLE);
                }
                toggleSendButton();
                quizButton.setOnClickListener(null);
                stickerButton.setOnClickListener(null);
                emoticonButton.setOnClickListener(null);
                sendButton.setOnClickListener(null);
                sendPhotoButton.setOnClickListener(null);
                Picasso.with(aContext).load(R.drawable.quiz_icon_disabled)
                        .into(quizButton);
                Picasso.with(aContext).load(R.drawable.sticker_icon_disabled)
                        .into(stickerButton);
                Picasso.with(aContext).load(R.drawable.smileys_disabled)
                        .into(emoticonButton);
                break;
            case CONNECTED:
            case POLLING:
            default:
                actionBarHandler.toggleClearChat(true);
                connecting_container.setVisibility(View.GONE);
                toggleSendButton();
                quizButton.setOnClickListener(this);
                sendButton.setOnClickListener(this);
                sendPhotoButton.setOnClickListener(this);
                stickerButton.setOnClickListener(this);
                emoticonButton.setOnClickListener(this);
                Picasso.with(aContext).load(R.drawable.quiz_icon).into(quizButton);
                Picasso.with(aContext).load(R.drawable.sticker_icon)
                        .into(stickerButton);
                Picasso.with(aContext).load(R.drawable.smileys)
                        .into(emoticonButton);
                break;


        }
    }

    /**
     * Removes a chat item, with the given message id, from the list
     *
     * @param messageId - Id corresponding to the chat item to be removed
     */
    private void removeChatItem(final String messageId) {
        if (isChatActiveForId(aContext, match_id)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (messageListAdapter != null) {
                        messageListAdapter.removeItem(messageId);
                    }
                }
            });
        }
    }

    /**
     * Replaces a chat item -
     * 1. Removes the chat item with the messageId = oldMessageId
     * 2. Calculates/evaluates the newIndex at which the new chat item is to be inserted
     * 3. Inserts the chat item to the newIndex
     *
     * @param oldMessageId - Id corresponding to the chat item to be replaced
     * @param messageId    - Id corresponding to the chat item to be inserted
     */
    private void replaceChatItem(final String oldMessageId, final String messageId) {
        if (isChatActiveForId(aContext, match_id)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (messageListAdapter != null) {
                        ArrayList<MessageModal> messageModals = MessageDBHandler.getAllMessagesModals(
                                Utility.getMyId(aContext), match_id, mCurrentSlotSize, aContext);
                        MessageModal messageModal = MessageDBHandler.getAMessage(
                                Utility.getMyId(aContext.getApplicationContext()), messageId, getApplicationContext());
                        int newIndex = messageModals.indexOf(messageModal);
                        messageListAdapter.replaceAnItem(oldMessageId, messageModal, newIndex);
                    }
                }
            });
        }
    }

    /**
     * Updates a chat item with the new modal obtained from the db
     *
     * @param messageId - Id corresponding to the chat item to be updated
     */
    private void updateChatItem(final String messageId) {
        if (isChatActiveForId(aContext, match_id)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (messageListAdapter != null) {
                        MessageModal messageModal = MessageDBHandler.getAMessage(
                                Utility.getMyId(aContext.getApplicationContext()), messageId, getApplicationContext());
                        messageListAdapter.updateAnItem(messageModal);
                    }
                }
            });
        }
    }

    private void updateChat(final boolean scrollToBottom) {
        if (isChatActiveForId(aContext, match_id)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (messageListAdapter != null) {
//                        Cursor cursor = MessageDBHandler.getAllMessages(
//                                Utility.getMyId(aContext), match_id, mCurrentSlotSize, aContext);
                        ArrayList<MessageModal> messageModals = MessageDBHandler.getAllMessagesModals(
                                Utility.getMyId(aContext), match_id, mCurrentSlotSize, aContext);
                        initDoodle(messageModals.size() > 0);
                        toggleLoadMore();
                        messageListAdapter.changeList(messageModals, isLoadMoreEnabled, isSparkHeaderRequired(), match_id);
                        if (mCurrentPosition != -1) {
                            if (mOldRecordsCount != -1) {
                                mCurrentPosition = messageModals.size() - mOldRecordsCount
                                        + ((isLoadMoreEnabled) ? 1 : 0);
                            }
                            mLinearLayoutManager.scrollToPositionWithOffset(mCurrentPosition, mFirstItemOffset);
                            mCurrentPosition = -1;
                            mOldRecordsCount = -1;
                        } else if (scrollToBottom) {
                            mMessageRV.scrollToPosition(messageModals.size() - ((isLoadMoreEnabled) ? 0 : 1));
                        }
                    }
                }
            });
        }
    }


    private void updateActionBar() {
        if (actionBarHandler == null) {
//            SlidingMenuHandler menuHandler = new SlidingMenuHandler(this,
//                    SlidingMenu.TOUCHMODE_MARGIN);
            actionBarHandler = new ActionBarHandler(this, "", null,
                    onActionBarClickedInterface, false, false, true, false, isSparkButNotMutualMatch());
        }
        actionBarHandler.setOnActionBarClickedInterface(onActionBarClickedInterface);
        actionBarHandler.setOptionsMenuHandler(onActionBarMenuItemClicked);
        actionBarHandler.toggleNotificationCenter(false);
        actionBarHandler.toggleBackButton(true);
        actionBarHandler.toggleMenuVisibility(false);
        // actionBarHandler.toggleMenuVisibility(isFromFeedbackPage);
        // actionBarHandler.toggleBackButton(!isFromFeedbackPage);
        actionBarHandler.showProfilePicInsteadOfTitle(!isFromFeedbackPage);
        actionBarHandler.setUserProfileClick(!isFromFeedbackPage);
        toggleMenuOptionsVisibility(!isFromFeedbackPage && !matchMessageMetaData.isMissTm());
//        toggleTimer();
        setHeaderDetails();
    }

    private void toggleTimer() {
        boolean showTimer = isSparkButNotMutualMatch();
        int totalValue = (showTimer && mSparkModal != null) ? ((int) mSparkModal.getmExpiredTimeInSeconds()) : 100;
        int currentValue = (showTimer && mSparkTimer != null) ? mSparkTimer.getElapsedTimeWrtStartTime() : 100;
        actionBarHandler.toggleTimer(showTimer, totalValue, currentValue);
    }

    private void pollServerForRealTimeChatting(boolean onresumecall,
                                               boolean is_error) {

        if (!isResumed || isPollingShut()
                || SocketHandler.isSocketEnabled(aContext)) {
            return;
        }
        TmLogger.log(Log.DEBUG, TAG, "starting polling");

        current_poll_counter = current_poll_counter + 1;

        int max_poll = 30;
        if (current_poll_counter >= max_poll) {
            stopPolling();
            return;
        }

        if (is_error) {
            error_counter++;
        } else {
            error_counter = 1;
        }
        int polltime = 5;
        int delaytime = polltime * error_counter;
        if (onresumecall) {
            delaytime = 1;
        }

        if (isUnread) {
            showConnecting(SOCKET_STATE.CONNECTING, false);
        }
        try {
            pollingHandler.schedule(new Runnable() {
                public void run() {
                    android.os.Process.setThreadPriority(
                            android.os.Process.THREAD_PRIORITY_BACKGROUND);
                    fetchMessageOneonOneConversationFromServer(
                            Utility.getMyId(aContext), match_id, matchMessageMetaData,
                            isFromFeedbackPage);
                }
            }, delaytime, TimeUnit.SECONDS);
        } catch (OutOfMemoryError ignored) {
        }
    }

    private String getDummyTstampForSending() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
                Locale.ENGLISH);

        Calendar c = Calendar.getInstance();
        String lastMessageTstamp = MessageDBHandler
                .getLastMessageTstampForMatch(Utility.getMyId(aContext), match_id,
                        aContext);
        if (Utility.isSet(lastMessageTstamp)) {
            c.setTime(TimeUtils.getParsedTime(lastMessageTstamp));
            c.add(Calendar.SECOND, 2);
        }
        return sdf.format(c.getTime());
    }

    private String addMessage(String message, MessageType messageType, int quizId,
                              CuratedDealsChatsModal curatedDealsChatsModal,
                              boolean clearMessageEditor, String metadataJson) {
        return addMessage(message, messageType, quizId, curatedDealsChatsModal, clearMessageEditor,
                metadataJson, true);
    }

    private String addMessage(String message, MessageType messageType, int quizId,
                              CuratedDealsChatsModal curatedDealsChatsModal,
                              boolean clearMessageEditor, String metadataJson, boolean sendToServer) {
        String random_message_id = null;
        MessageModal messageModal = null;
        if (Utility.isNetworkAvailable(aContext)) {
            if (Utility.isSet(message)) {
                random_message_id = Utility.generateUniqueRandomId(match_id);
                messageModal = addMessageForDisplay(message, random_message_id, messageType,
                        false, quizId, curatedDealsChatsModal, metadataJson, sendToServer);
                if (clearMessageEditor) {
                    message_editor.setText("");
                }
                return random_message_id;
            } else {
                return random_message_id;
            }
        } else {
            showConnecting(SOCKET_STATE.FAILED, true);
            AlertsHandler.showMessage(aActivity, R.string.ERROR_NETWORK_FAILURE);
            return random_message_id;
        }
    }

    private MessageModal addMessageForDisplay(String message, String random_message_id,
                                              MessageType messageType, Boolean isRetry, int quizId,
                                              CuratedDealsChatsModal curatedDealsChatsModal,
                                              String msgJson) {
        return addMessageForDisplay(message, random_message_id, messageType, isRetry, quizId,
                curatedDealsChatsModal, msgJson, true);
    }

    private MessageModal addMessageForDisplay(String message, String random_message_id,
                                              MessageType messageType, Boolean isRetry, int quizId,
                                              CuratedDealsChatsModal curatedDealsChatsModal,
                                              String metadataJson,
                                              boolean sendToServer) {
        hideDoodle();
        MessageModal msgObject = new MessageModal(Utility.getMyId(aContext));
        msgObject.parseMessage(random_message_id, message,
                getDummyTstampForSending(), messageType.toString(), match_id,
                MessageState.OUTGOING_SENDING, quizId, curatedDealsChatsModal, metadataJson);

//        boolean isPendingSpark = isSparkButNotMutualMatch();

        MessageDBHandler.insertAMessage(aContext, Utility.getMyId(aContext), msgObject,
                true, FetchSource.LOCAL, sendToServer);
        if (messageListAdapter != null && matchMessageMetaData != null) {
            ArrayList<MessageModal> messageModals = MessageDBHandler.getAllMessagesModals(
                    Utility.getMyId(aContext), match_id, mCurrentSlotSize, aContext);
            toggleLoadMore();
            // http://stackoverflow.com/questions/13953773/android-listview-not-refreshing-after-change-cursor
            messageListAdapter.setLastSeenReceiverTstamp(
                    matchMessageMetaData.getLastSeenReceiverTstamp());
            messageListAdapter.changeList(messageModals, isLoadMoreEnabled, isSparkHeaderRequired(), match_id);
            if (!isRetry && messageModals != null && messageModals.size() > 0) {
                mLinearLayoutManager.scrollToPosition(messageModals.size() - ((!isLoadMoreEnabled) ? 1 : 0));
            }

//            if(isPendingSpark){
//                if(mPendingMessageModalsSparks == null){
//                    mPendingMessageModalsSparks = new ArrayList<>();
//                    mPendingMessageModalsSparks.add(msgObject);
//                }
//                updateSparkStatus(SparkModal.SPARK_STATUS.accepted, msgObject);
//                sendToServer = false;
//            }

            if (sendToServer) {
                sendMessageToServer(message, random_message_id, messageType, isRetry,
                        quizId, curatedDealsChatsModal, metadataJson);
            }
        }

        return msgObject;
    }

    private void setHeaderDetails() {
        if (matchMessageMetaData == null && match_id != null) {
            matchMessageMetaData = MessageDBHandler.getMatchMessageMetaData(
                    Utility.getMyId(aContext), match_id, aContext);
//            matchMessageMetaData.setProfilePic("http://media.salon.com/2013/02/house_of_cards2.jpg");
        }
        if (!isFromFeedbackPage && matchMessageMetaData != null
                && actionBarHandler != null) {
            if (Utility.isSet(matchMessageMetaData.getFName())) {
                actionBarHandler.setTitle(matchMessageMetaData.getFName());
            }
            if (Utility.isSet(matchMessageMetaData.getProfilePic())) {
                actionBarHandler.setProfilePic(
                        matchMessageMetaData.getProfilePic(),
                        match_id);
            }
        } else if (isFromFeedbackPage) {
            actionBarHandler
                    .setTitle(getResources().getString(R.string.feedback));
        }
        setFooterIcons(isFromFeedbackPage,
                matchMessageMetaData != null && matchMessageMetaData.isMissTm(),
                isSparkButNotMutualMatch());

        LinearLayoutManager layoutManager = (LinearLayoutManager) mMessageRV.getLayoutManager();
        if (isSparkButNotMutualMatch()) {
            layoutManager.setStackFromEnd(false);
        } else {
            layoutManager.setStackFromEnd(true);
        }
        mMessageRV.setLayoutManager(layoutManager);
    }

    private void setFooterIcons(boolean isFromFeedbackPage, boolean isMissTm, boolean isSpark) {
        if (isFromFeedbackPage) {
            emoticonButton.setVisibility(View.GONE);
            stickerButton.setVisibility(View.GONE);
            quizButton.setVisibility(View.GONE);
            new_quiz_icon.setVisibility(View.GONE);
        } else if (isMissTm) {
            emoticonButton.setVisibility(View.VISIBLE);
            stickerButton.setVisibility(View.VISIBLE);
            quizButton.setVisibility(View.GONE);
            new_quiz_icon.setVisibility(View.GONE);
            mEnableSendPhotoOnMissTm = RFHandler.getBool(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_SHOW_PHOTO_ON_MISS_TM);
        } else if (isSpark) {
            emoticonButton.setVisibility(View.VISIBLE);
            stickerButton.setVisibility(View.VISIBLE);
            quizButton.setVisibility(View.GONE);
            new_quiz_icon.setVisibility(View.GONE);
        } else {
            emoticonButton.setVisibility(View.VISIBLE);
            stickerButton.setVisibility(View.VISIBLE);
            quizButton.setVisibility(View.VISIBLE);
        }
    }

    private void setCursorAndAdapter() {
        // first make call to db to see what data we have there
        ArrayList<MessageModal> messageModals = MessageDBHandler
                .getAllMessagesModals(Utility.getMyId(aContext), match_id, mCurrentSlotSize, aContext);

        toggleLoadMore();

        if (mSlideListener == null) {
            mSlideListener = new SlideListener(mMessageRV) {

                @Override
                public void onRightClicked() {
                    triggerSimpleAction(new SimpleAction(SimpleAction.ACTION_DELETE_SPARK));
//                    if (isSparkButNotMutualMatch()) {
//                        AlertsHandler.showConfirmDialog(aActivity, R.string.are_you_sure_delete_spark,
//                                R.string.yes, R.string.no, new ConfirmDialogInterface() {
//                                    @Override
//                                    public void onPositiveButtonSelected() {
//                                        updateSparkStatus(SparkModal.SPARK_STATUS.rejected, null);
//                                    }
//
//                                    @Override
//                                    public void onNegativeButtonSelected() {
//                                        mSlideListener.hardReset();
//                                    }
//                                });
//                    }
                }

                @Override
                public void onClicked() {
                    if (Utility.isSet(matchMessageMetaData.getProfileUrl())) {
                        if (isSparkButNotMutualMatch()) {
                            isProfileViewed = true;
                            HashMap<String, String> eventInfo = new HashMap<>();
                            eventInfo.put("match_id", match_id);
                            eventInfo.put("source", "card");
                            eventInfo.put("msg_rcd", mSparkModal.getmMessage());
                            eventInfo.put("time_left", String.valueOf(TimeUtils.getTimeoutDifference(
                                    mSparkModal.getmStartTime(), mSparkModal.getmExpiredTimeInSeconds()*1000)/1000));
                            TrulyMadlyTrackEvent.trackEvent(aContext, sparks, TrulyMadlyEventTypes.chat, 0,
                                    TrulyMadlyEvent.TrulyMadlyEventStatus.view_profile,
                                    eventInfo, true, true);
                        }

                        ActivityHandler.startProfileActivity(aActivity,
                                matchMessageMetaData.getProfileUrl(), false, false, false);
                    }
                }
            };
        }

        messageListAdapter = new MessageOneOnOneConversationAdapter(messageModals, this,
                matchMessageMetaData.getLastSeenReceiverTstamp(), UiUtils.getScreenWidth(this),
                isLoadMoreEnabled,
                retryMessageClickListener, onQuizItemInChatClickedListener, onCDChatItemClickedListener,
                onBlogLinkItemInChatClickedListener, mOnLoadMoreClickedListener, onEventChatItemClickedListener,
                match_id,
                isSparkHeaderRequired(), mSlideListener);
        mMessageRV.setAdapter(messageListAdapter);

        initDoodle(messageModals.size() > 0);
    }

    private boolean isSparkButNotMutualMatch() {
        return matchMessageMetaData != null && matchMessageMetaData.isSparkReceived() && !matchMessageMetaData.isMutualSpark();
    }

    private boolean isSparkHeaderRequired() {
        return isSparkButNotMutualMatch();
    }

    private void hideDoodle() {
        isDoodleHidden = true;
        doodle_image.setVisibility(View.GONE);

    }

    private void showDoodle(String doodleUrl) {
        if (!isDoodleHidden) {
            doodle_image.setVisibility(View.VISIBLE);
            Picasso.with(aContext).load(doodleUrl).config(Bitmap.Config.RGB_565).into(doodle_image);
        }
    }

    private void initDoodle(boolean hasChats) {
        if (!hasChats) {
            //check doodle expiry
            if (matchMessageMetaData == null || matchMessageMetaData.isDoodleDead()) {
                fetchDoodleFromServer();
            } else {
                showDoodle(matchMessageMetaData.getDoodleUrl());
            }
        } else {
            hideDoodle();
        }
    }

    private void fetchDoodleFromServer() {

        TmLogger.log(Log.DEBUG, TAG, "fetching doodle");

        String url = message_one_one_conversion_url;

        CustomOkHttpResponseHandler okHttpResponseHandler = new CustomOkHttpResponseHandler(
                aContext) {

            @Override
            public void onRequestSuccess(JSONObject response) {
                String doodleUrl = response.optString("doodle_url");
                if (Utility.isSet(doodleUrl)) {
                    matchMessageMetaData.setDoodleUrl(doodleUrl);
                    MessageDBHandler.updateMessageMetaDataValue(aContext, Utility.getMyId(aContext), match_id, "doodle_url", doodleUrl);
                    showDoodle(doodleUrl);
                } else {
                    hideDoodle();
                }
            }

            @Override
            public void onRequestFailure(Exception e) {
                hideDoodle();
            }
        };

        Map<String, String> params = new HashMap<>();
        params.put("type", "doodle");
        OkHttpHandler.httpPost(aContext, url, params, okHttpResponseHandler);

    }

    private void fetchMessageOneonOneConversationFromServer(
            final String my_id_thread, final String match_id_thread,
            final MatchMessageMetaData messageMetaData_thread,
            final boolean isFromFeedbackPage) {
        if (SocketHandler.isSocketEnabled(aContext)) {
            return;
        }

        TmLogger.log(Log.DEBUG, TAG, "fetching poll data");

        String url = message_one_one_conversion_url;
        String last_fetched_id = null;
        if (Utility.isSet(matchMessageMetaData.getLastFetchedId())) {
            last_fetched_id = matchMessageMetaData.getLastFetchedId();
        }

        CustomOkHttpResponseHandler okHttpResponseHandler = new CustomOkHttpResponseHandler(
                aContext) {

            @Override
            public void onRequestSuccess(JSONObject response) {
                isUnread = false;
                (new ProcessPollingDataAsyncTask(response.optJSONObject("data"), my_id_thread,
                        match_id_thread, messageMetaData_thread,
                        isFromFeedbackPage)).execute();
            }

            @Override
            public void onRequestFailure(Exception e) {
                if (isResumed) {
                    pollServerForRealTimeChatting(false, true);
                }

            }
        };
        OkHttpHandler.httpGet(aContext, url,
                GetOkHttpRequestParams.getHttpRequestParams(
                        HttpRequestType.MESSAGE_ONE_ONE_CONVERSATION,
                        last_fetched_id),
                okHttpResponseHandler);
    }

    private void setBlockOptions(JSONArray blockReasons,
                                 JSONArray blockEditableFlags) {
        if (blockReasons != null && blockEditableFlags != null) {
            ArrayList<BlockReason> allBlockReasons = new ArrayList<>();
            for (int i = 0; i < blockReasons.length(); i++) {
                allBlockReasons.add(new BlockReason(blockReasons.optString(i),
                        blockEditableFlags.optBoolean(i)));
            }
            onActionBarMenuItemClicked.setBlockReasons(allBlockReasons);
        }
    }

    private void checkForEmptyMessages(String match_id_thread) {
        String tstamp = MessageDBHandler.getTstampForEmptyMessageRefresh(
                Utility.getMyId(aContext), match_id_thread, aContext);
        if (Utility.isSet(tstamp) && SocketHandler.isSocketEnabled(this)) {
            Utility.fireServiceBusEvent(aContext, new ServiceEvent(
                    SERVICE_EVENT_TYPE.GET_MISSED_MESSAGES, tstamp, false));
        }
    }

    @SuppressWarnings("UnusedParameters")
    @Subscribe
    public void triggerChatUpdate(
            TriggerChatUpdateEvent triggerChatUpdateEvent) {
        updateChat(false);
    }

    @SuppressWarnings("UnusedParameters")
    @Subscribe
    public void triggerClearChat(
            TriggerClearChatEvent triggerChatUpdateEvent) {
        isDoodleHidden = false;
        updateChat(false);
    }

    @SuppressWarnings("UnusedParameters")
    @Subscribe
    public void triggerSimpleAction(SimpleAction simpleAction) {
        switch (simpleAction.getmAction()) {
            case SimpleAction.ACTION_DELETE_SPARK:
                if (isSparkButNotMutualMatch()) {
                    //match_id, msg_rcd, time_left, profile_view
                    final HashMap<String, String> eventInfo = new HashMap<>();
                    if (mSparkModal != null) {
                        eventInfo.put("match_id", mSparkModal.getmMatchId());
                        eventInfo.put("msg_rcd", mSparkModal.getmMessage());
                        eventInfo.put("time_left", String.valueOf(TimeUtils.getTimeoutDifference(mSparkModal.getmStartTime(),
                                mSparkModal.getmExpiredTimeInSeconds()*1000)/1000));
                    }
                    eventInfo.put("profile_view", String.valueOf(isProfileViewed));
                    TrulyMadlyTrackEvent.trackEvent(aContext, sparks, TrulyMadlyEventTypes.chat, 0,
                            TrulyMadlyEvent.TrulyMadlyEventStatus.delete_clicked,
                            eventInfo, true, true);
                    AlertsHandler.showConfirmDialog(aActivity, R.string.are_you_sure_delete_spark,
                            R.string.yes, R.string.no, new ConfirmDialogInterface() {
                                @Override
                                public void onPositiveButtonSelected() {
                                    TrulyMadlyTrackEvent.trackEvent(aContext, sparks, TrulyMadlyEventTypes.chat, 0,
                                            TrulyMadlyEvent.TrulyMadlyEventStatus.delete_confirmed,
                                            eventInfo, true, true);
                                    updateSparkStatus(SparkModal.SPARK_STATUS.rejected, null);
                                }

                                @Override
                                public void onNegativeButtonSelected() {
                                    mSlideListener.hardReset();
                                }
                            });
                }
                break;
        }
    }

    @Subscribe
    public void triggerConnecting(
            TriggerConnectingEvent triggerConnectingEvent) {
        showConnecting(triggerConnectingEvent.getSocketState(), true);
    }


    @Subscribe
    public void onBackButtonPressed(BackPressed backPressed) {
        if (backPressed.getFromTakeQuizFragement()) {
            hideAllFragments();

            // send nudge or not
            if (backPressed.getSendNudge()) {
                sendNudgeToMatch(new SendNudgeToMatchEvent(
                        backPressed.getMatchId(), backPressed.getQuizId(),
                        backPressed.getQuizName(),
                        QuizDBHandler.isPlayedByMatch(aContext,
                                backPressed.getMatchId(), Utility.getMyId(aContext),
                                backPressed.getQuizId()) ? "BOTH" : "USER"));

            }

            if (backPressed.getShowCommonAnswers()) {
                showCommonAnswers(
                        new QuizClickEvent(backPressed.getQuizId(),
                                backPressed.getQuizName(), false, "BOTH", null),
                        backPressed.getResponse());
            }

        } else {
            onBackPressed();
        }
        updateChat(false);
    }

    @Subscribe
    public void onTriggerQuizStatusRefresh(
            TriggerQuizStatusRefreshEvent aTriggerQuizStatusRefreshEvent) {
        if (isChatActiveForId(aContext,
                aTriggerQuizStatusRefreshEvent.getMatchId())) {
            blooperCall();
        }
    }

    @SuppressWarnings("UnusedParameters")
    @Subscribe
    public void onGCMMessageReceived(
            GCMMessageReceivedEvent gcmMessageReceivedEvent) {
        checkAndRestartPolling();
    }

    @SuppressWarnings("UnusedParameters")
    @Subscribe
    public void onChatTypingReceived(
            ChatTypingReceivedEvent chatMessageReceivedEvent) {
        showTypingIndicator();
    }

    @Subscribe
    public void onChatMessageReceived(
            ChatMessageReceivedEvent chatMessageReceivedEvent) {
        hideTypingIndicator();
        TmLogger.log(Log.DEBUG, TAG,
                chatMessageReceivedEvent.getData().toString());
        MessageModal messageObj = new MessageModal(Utility.getMyId(aContext),
                chatMessageReceivedEvent.getData());
        processSingleMessage(messageObj, aContext);

        String match_id_thread = chatMessageReceivedEvent.getMatchId();
        MatchMessageMetaData matchMessageMetaData_thread = MessageDBHandler
                .getMatchMessageMetaData(Utility.getMyId(aContext), match_id_thread,
                        aContext);

        sendChatRead(match_id_thread,
                matchMessageMetaData_thread.getLastFetchedId(),
                matchMessageMetaData_thread.getLastFetchedTstamp(),
                true);

        runOnUiThread(new UpdateUIThread(true, true, match_id_thread,
                matchMessageMetaData_thread, true));

        //Vibrate slightly when chat is active and new message recieved.
        //NotificationHandler.vibrateOnChatMessageReceived(aActivity);
        NotificationHandler.soundOnChatMessageReceived(aActivity);

    }

    @Subscribe
    public void showNextFragment(final QuizClickEvent quizClickEvent) {

        int loaderMessage = 0;
        String playStatus = quizClickEvent.getPlayedStatus();
        if (playStatus.equals("NONE") || playStatus.equals("MATCH")) {
            loaderMessage = R.string.start_quiz_loader_message;
        } else if (playStatus.equals("BOTH")) {
            loaderMessage = R.string.common_answers_loader_message;
        }
        mProgressDialog = UiUtils.showProgressBar(aActivity, mProgressDialog,
                loaderMessage);
        CustomOkHttpResponseHandler quizClickHandler = new CustomOkHttpResponseHandler(
                aContext, TrulyMadlyActivities.quiz,
                quizClickEvent.getTrkEventType(),
                quizClickEvent.getQuiz_id() + "") {

            @Override
            public void onRequestSuccess(final JSONObject responseJSON) {

                mProgressDialog = UiUtils.hideProgressBar(mProgressDialog);
                if (responseJSON.optString("response_type")
                        .equalsIgnoreCase("nudge_list")) {
                    nudgeFromChat = quizClickEvent.isFromChat();
                    showNudgeListFeature(new QuizLaunchNudgeListEvent(
                            quizClickEvent.getQuiz_id(),
                            quizClickEvent.getQuiz_name(), responseJSON));

                } else if (responseJSON.optString("response_type")
                        .equalsIgnoreCase("questions")) {
                    if (quizClickEvent.isFromChat()) {
                        TrulyMadlyTrackEvent.trackEvent(aContext,
                                TrulyMadlyActivities.quiz,
                                TrulyMadlyEventTypes.quiz_item_in_chat_clicked_call_play_quiz,
                                0, quizClickEvent.getQuiz_id() + "", null,
                                false);
                    }
                    TrulyMadlyTrackEvent.trackEvent(aContext,
                            TrulyMadlyActivities.quiz,
                            TrulyMadlyEventTypes.fetch_quiz, 0,
                            quizClickEvent.getQuiz_id() + "", null,
                            false);
                    hideAllFragments();
                    if (showQuizOverlayFragmentTakeQuizFragmentRunnable != null) {
                        mHandler.removeCallbacks(showQuizOverlayFragmentTakeQuizFragmentRunnable);
                    }
                    showQuizOverlayFragmentTakeQuizFragmentRunnable = new Runnable() {
                        @Override
                        public void run() {
                            TakeQuizFragmentModal takeQuizFragmentModal = new TakeQuizFragmentModal();
                            takeQuizFragmentModal.setMatchId(match_id);
                            takeQuizFragmentModal.setMatchName(
                                    matchMessageMetaData.getFName());
                            takeQuizFragmentModal
                                    .setQuizId(quizClickEvent.getQuiz_id());
                            takeQuizFragmentModal
                                    .setQuizName(quizClickEvent.getQuiz_name());
                            takeQuizFragmentModal.setResponse(responseJSON);
                            Bundle data = new Bundle();
                            data.putSerializable("takeQuizFragmentModal",
                                    takeQuizFragmentModal);
                            localTakeQuizFragment = new TakeQuizFragment();

                            localTakeQuizFragment.setArguments(data);

                            showQuizOverlayFragment(localTakeQuizFragment,
                                    CURRENT_FRAGMENT.localTakeQuizFragment,
                                    TakeQuizFragment.class.getSimpleName());
                        }
                    };
                    mHandler.postDelayed(showQuizOverlayFragmentTakeQuizFragmentRunnable, 70L);
                } else if (responseJSON.optString("response_type")
                        .equalsIgnoreCase("common_answers")) {

                    if (quizClickEvent.isFromChat()) {
                        TrulyMadlyTrackEvent.trackEvent(aContext,
                                TrulyMadlyActivities.quiz,
                                TrulyMadlyEventTypes.quiz_item_in_chat_clicked_call_view_answers,
                                0, quizClickEvent.getQuiz_id() + "", null,
                                false);
                    }
                    showCommonAnswers(quizClickEvent, responseJSON);
                }

            }

            @Override
            public void onRequestFailure(Exception exception) {
                UiUtils.hideProgressBar(mProgressDialog);
                AlertsHandler.showNetworkError(aActivity, exception);
            }
        };

        HashMap<String, String> params = new HashMap<>();
        params.put("action", "quiz_click");
        params.put("quiz_id", quizClickEvent.getQuiz_id() + "");
        params.put("match_id", "" + match_id);
        if (quizClickEvent.isFromChat()) {

            params.put("from_chat", "yes");
        }
        OkHttpHandler.httpGet(aContext, ConstantsUrls.get_quiz_url(), params,
                quizClickHandler);
    }

    private void showCommonAnswers(final QuizClickEvent quizClickEvent,
                                   final JSONObject responseJSON) {
        TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyActivities.quiz,
                TrulyMadlyEventTypes.quiz_fetch_common_answers, 0,
                quizClickEvent.getQuiz_id() + "", null, false);

        CommonAnswersFragmentModal commonAnswersFragmentModal = new CommonAnswersFragmentModal();
        commonAnswersFragmentModal.setResponse(responseJSON);
        commonAnswersFragmentModal.setMatchID(match_id);
        Bundle data = new Bundle();
        data.putSerializable("commonAnswersFragmentModal",
                commonAnswersFragmentModal);
        localCommonAnswersFragment = new CommonAnswersFragment();
        localCommonAnswersFragment.setArguments(data);
        hideAllFragments();
        showQuizOverlayFragment(localCommonAnswersFragment,
                CURRENT_FRAGMENT.localCommonAnswersFragment,
                CommonAnswersFragment.class.getSimpleName());
    }

    @Subscribe
    public void sendNudgeToMatch(SendNudgeToMatchEvent sendNudgeToMatchEvent) {

        ArrayList<QuizNudgeSendModal> quizNudges = new ArrayList<>();
        String playStatus = sendNudgeToMatchEvent.getPlayedStatus();
        if (sendNudgeToMatchEvent.getFromCommonAnswers()) {
            nudgeFromCommonAnswers = true;
            hideAllFragments();
        }
        QuizNudgeSendModal quizNudgeSendModal = new QuizNudgeSendModal(
                sendNudgeToMatchEvent.getMatchId(),
                sendNudgeToMatchEvent.getQuizId(),
                sendNudgeToMatchEvent.getQuizName(),
                playStatus.equalsIgnoreCase("MATCH")
                        || playStatus.equalsIgnoreCase("BOTH")
                        || nudgeFromCommonAnswers ? MessageType.NUDGE_DOUBLE
                        : MessageType.NUDGE_SINGLE,
                matchMessageMetaData.getMessage_link());
        quizNudges.add(quizNudgeSendModal);
        sendQuizNudges(quizNudges);

        // QuizClickEvent quizClickEvent = new QuizClickEvent(
        // backPressed.getQuizId(), backPressed.getQuizName(), false,
        // "BOTH", null);
        // showCommonAnswers(quizClickEvent, backPressed.getResponse());

    }

    @Subscribe
    public void showNudgeListFeature(
            QuizLaunchNudgeListEvent quizLaunchNudgeListEvent) {
        final int quiz_id = quizLaunchNudgeListEvent.getQuiz_id();
        final String quiz_name = quizLaunchNudgeListEvent.getQuiz_name();
        final JSONObject response = quizLaunchNudgeListEvent
                .getNudgeResponseData();

        final ArrayList<NudgeClass> nudgeList = new ArrayList<>();
        JSONArray list = response.optJSONArray("nudge_list");
        String nudgeName = "";
        for (int i = 0; list != null && i < list.length(); i++) {
            NudgeClass nudgeObject = new NudgeClass();
            JSONObject nudgeDetails = list.optJSONObject(i);
            nudgeObject.has_played = nudgeDetails.optBoolean("is_played");
            nudgeObject.match_name = nudgeDetails.optString("name");
            nudgeObject.match_id = nudgeDetails.optString("match_id");
            nudgeObject.match_pic_url = nudgeDetails.optString("profile_pic");
            nudgeObject.full_conv_link = nudgeDetails
                    .optString("full_conv_link");
            nudgeList.add(nudgeObject);
            nudgeName += (i == 0 ? "" : ", ") + nudgeObject.match_name;
        }
        if (response.optBoolean("auto_nudge")) {
            String autoNudgeMessage = "Nudge " + nudgeName + " to play ?";
            if (response.optString("from_chat", "no").equalsIgnoreCase("yes")) {
                autoNudgeMessage = "You have already nudged " + nudgeName
                        + ". Do you want to nudge again?";
            }

            AlertsHandler.showConfirmDialog(aActivity, autoNudgeMessage, R.string.yes,
                    R.string.no, new ConfirmDialogInterface() {
                        @Override
                        public void onPositiveButtonSelected() {
                            hideAllFragments();
                            ArrayList<QuizNudgeSendModal> quizNudges = new ArrayList<>();
                            for (int i = 0; i < nudgeList.size(); i++) {
                                quizNudges.add(new QuizNudgeSendModal(
                                        nudgeList.get(i).match_id, quiz_id,
                                        quiz_name,
                                        nudgeList.get(i).has_played
                                                ? MessageType.NUDGE_DOUBLE
                                                : MessageType.NUDGE_SINGLE,
                                        nudgeList.get(i).full_conv_link));
                            }
                            sendQuizNudges(quizNudges);

                        }

                        @Override
                        public void onNegativeButtonSelected() {
                            hideAllFragments();
                        }
                    }, false);
        } else {
            if (showQuizOverlayFragmentNudgeMatchesFragmentRunnable != null) {
                mHandler.removeCallbacks(showQuizOverlayFragmentNudgeMatchesFragmentRunnable);
            }
            showQuizOverlayFragmentNudgeMatchesFragmentRunnable = new Runnable() {
                @Override
                public void run() {
                    hideAllFragments();
                    NudgeListFragmentModal nudgeListFragmentModal = new NudgeListFragmentModal();
                    nudgeListFragmentModal.setMatchId(match_id);
                    nudgeListFragmentModal.setQuizId(quiz_id);
                    nudgeListFragmentModal.setQuizName(quiz_name);
                    nudgeListFragmentModal.setNudgList(nudgeList);
                    Bundle data = new Bundle();
                    data.putSerializable("nudgeListFragmentModal",
                            nudgeListFragmentModal);

                    localNudgeListFragment = new NudgeMatchesFragment();
                    localNudgeListFragment.setArguments(data);
                    showQuizOverlayFragment(localNudgeListFragment,
                            CURRENT_FRAGMENT.localNudgeList,
                            NudgeMatchesFragment.class.getSimpleName());
                }
            };
            mHandler.postDelayed(showQuizOverlayFragmentNudgeMatchesFragmentRunnable, 70L);
        }
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    }

    private void showQuizOverlayFragment(Fragment f, CURRENT_FRAGMENT type,
                                         String simpleName) {
        try {
            actionBarHandler.toggleActionBar(false);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.take_quiz_container, f, simpleName)
                    .commitAllowingStateLoss();

            // take_quiz_container.setOnClickListener(this);
            fragmentSelected = type;
            toggleMenuOptionsVisibility(false);
            getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        } catch (IllegalStateException ignored) {
        }

    }

    private void removeDefMessage(){
        if(!isMessageDeletedOnce && isSparkButNotMutualMatch()){
            isMessageDeletedOnce = true;
            message_editor.setText("");
        }
    }

    private void hideAllFragments() {
        hideAllFragments(false);
    }

    private void hideAllFragments(boolean dontHideKeyboard) {

        if (actionBarHandler != null) {
            actionBarHandler.toggleActionBar(true);
        }
        if (!dontHideKeyboard) {
            UiUtils.hideKeyBoard(aActivity);
        }


        if (localEmoticonFragment != null) {
            getSupportFragmentManager().beginTransaction().remove(localEmoticonFragment)
                    .commitAllowingStateLoss();
            setContainerParams(false);

        }
        if (localStickerFragment != null) {
            getSupportFragmentManager().beginTransaction().remove(localStickerFragment)
                    .commitAllowingStateLoss();
            setContainerParams(false);
        }

        if (localQuizFragment != null) {
            getSupportFragmentManager().beginTransaction().remove(localQuizFragment)
                    .commitAllowingStateLoss();
            hideQuizBlooper();
            setContainerParams(false);
        }
        if (localTakeQuizFragment != null) {
            getSupportFragmentManager().beginTransaction().remove(localTakeQuizFragment)
                    .commitAllowingStateLoss();
//            setContainerParams(false);
        }
        if (localNudgeListFragment != null) {
            getSupportFragmentManager().beginTransaction().remove(localNudgeListFragment)
                    .commitAllowingStateLoss();
            setContainerParams(false);
        }
        if (localCommonAnswersFragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .remove(localCommonAnswersFragment)
                    .commitAllowingStateLoss();
            setContainerParams(false);

        }
        if (actionBarHandler != null) {
            actionBarHandler.toggleOverlay(false);
        }
        toggleMenuOptionsVisibility(!isFromFeedbackPage && (matchMessageMetaData == null || !matchMessageMetaData.isMissTm()));
        fragmentSelected = CURRENT_FRAGMENT.none;
        hidePhotoSlidingMenu();
    }

    private void hidePhotoSlidingMenu() {
        if (isPhotoUploadSliderVisible) {
            show_photo_slider_layout.setVisibility(View.GONE);
            Animation slideDownAnimation = AnimationUtils.loadAnimation(this, R.anim.slide_down);
            show_photo_slider_layout.startAnimation(slideDownAnimation);
            isPhotoUploadSliderVisible = false;
        }
    }

    private void showPhotoSlidingMenu() {
        hideAllFragments();
        show_photo_slider_layout.setVisibility(View.VISIBLE);
        Animation slideUpAnimation = AnimationUtils.loadAnimation(this, R.anim.slide_up);
        show_photo_slider_layout.startAnimation(slideUpAnimation);
        isPhotoUploadSliderVisible = true;
    }


    @Subscribe
    public void onChatMessageRead(ChatMessageReadEvent chatMessageReadEvent) {

        MatchMessageMetaData matchMessageMetaData_thread = MessageDBHandler
                .getMatchMessageMetaData(Utility.getMyId(aContext),
                        chatMessageReadEvent.getMatchId(), aContext);

        runOnUiThread(new UpdateUIThread(true, true,
                chatMessageReadEvent.getMatchId(),
                matchMessageMetaData_thread, false));

    }

    @Subscribe
    public void onNudgeListRecieved(
            final NudgeMatchesClickEvent nudgeMatchesClickEvent) {

        int nudgeCount = nudgeMatchesClickEvent.getNudgeCount();

        if (nudgeCount == 0) {
            showNudgeListCancelConfirmation(R.string.sure_nudge_anyone,
                    R.string.yes, R.string.no,
                    nudgeMatchesClickEvent.getQuizId(),
                    nudgeMatchesClickEvent.getQuizName());
        } else {
            createQuizNudgeList(nudgeMatchesClickEvent);
        }
    }

    private void createQuizNudgeList(
            NudgeMatchesClickEvent nudgeMatchesClickEvent) {
        hideAllFragments();
        ArrayList<NudgeClass> nudgeList = nudgeMatchesClickEvent.getNudgeList();
        ArrayList<QuizNudgeSendModal> quizNudges = new ArrayList<>();
        for (int i = 0; i < nudgeList.size(); i++) {
            if (nudgeList.get(i).send_nudge) {
                quizNudges.add(new QuizNudgeSendModal(
                        nudgeList.get(i).match_id,
                        nudgeMatchesClickEvent.getQuizId(),
                        nudgeMatchesClickEvent.getQuizName(),
                        nudgeList.get(i).has_played ? MessageType.NUDGE_DOUBLE
                                : MessageType.NUDGE_SINGLE,
                        nudgeList.get(i).full_conv_link));
            }
        }
        if (quizNudges.size() > 0) {
            sendQuizNudges(quizNudges);
        }

        showCommonsAnswersIfPlayed(nudgeMatchesClickEvent.getQuizId(),
                nudgeMatchesClickEvent.getQuizName());
    }

    private void showCommonsAnswersIfPlayed(int quizId, String quizName) {

        if (QuizDBHandler.isPlayedByMatch(aContext, match_id,
                Utility.getMyId(aContext), quizId)) {
            showNextFragment(new QuizClickEvent(quizId, quizName, false, "BOTH",
                    TrulyMadlyEventTypes.quiz_fetch_common_answers));
        }

    }

    @Subscribe
    public void onStickerRecieved(StickerClickEvent stickerClickEvent) {

        StickerData s = stickerClickEvent.getSticker();
        JSONObject sticker_json = new JSONObject();
        try {
            sticker_json.put("id", s.getId());
            sticker_json.put("gallery_id", s.getGalleryId());
            sticker_json.put("type", s.getType());

        } catch (JSONException e) {
            Crashlytics.logException(e);
        }

        String webUrl = FilesHandler.createStickerUrl(s, UiUtils.getDeviceDensity(aActivity));
        String randomMessageId = addMessage(webUrl, MessageType.STICKER, 0, null, false, null);
        if (Utility.isSet(randomMessageId)) {
            String time = TimeUtils.getFormattedTime();
            s.setLastUsed(time);
            StickerDBHandler.updateAttribute(s, "last_used", time, aContext);
        }
        TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyActivities.sticker,
                TrulyMadlyEventTypes.sticker_sent, 0, s.getId() + "", null,
                false);
    }

    @Subscribe
    public void sendQuizMessage(SendQuizMessageEvent sendQuizMessageEvent) {
        hideAllFragments();
        if (match_id.equalsIgnoreCase(sendQuizMessageEvent.getMatch_id())) {
            addMessage(sendQuizMessageEvent.getMessage(),
                    MessageType.QUIZ_MESSAGE,
                    sendQuizMessageEvent.getQuiz_id(), null, false, null);

            TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyActivities.quiz,
                    TrulyMadlyEventTypes.send_quiz_message, 0,
                    sendQuizMessageEvent.getQuiz_id() + "", null, false);
        }
    }

    private void sendQuizNudges(ArrayList<QuizNudgeSendModal> quizNudges) {
        final String random_message_id = Utility
                .generateUniqueRandomId(match_id);
        CustomOkHttpResponseHandler okHttpResponseHandler = new CustomOkHttpResponseHandler(
                aContext) {
            @Override
            public void onRequestSuccess(JSONObject response) {
                if (response != null) {
                    sendMessageSuccess(response, random_message_id, true);
                    checkAndRestartPolling();
                } else {
                    sendMessageFailed(random_message_id);
                }
            }

            @Override
            public void onRequestFailure(Exception exception) {
                sendMessageFailed(random_message_id);
            }
        };

        CustomOkHttpResponseHandler emptyResponseHandler = new CustomOkHttpResponseHandler(
                aContext) {
            @Override
            public void onRequestSuccess(JSONObject responseJSON) {
                sendMessageSuccess(responseJSON, random_message_id, false);
            }

            @Override
            public void onRequestFailure(Exception exception) {
            }
        };

        JSONArray paramsSocketArray = new JSONArray();
        String tstamp = getDummyTstampForSending();
        for (QuizNudgeSendModal nudgeModal : quizNudges) {
            String randomId = (nudgeModal.getMatchId()
                    .equalsIgnoreCase(match_id) ? random_message_id
                    : Utility.generateUniqueRandomId(
                    nudgeModal.getMatchId()));

            MessageModal msgObject = new MessageModal(Utility.getMyId(aContext));
            msgObject.parseMessage(randomId, nudgeModal.getMessage(), tstamp,
                    nudgeModal.getNudgeType().toString(),
                    nudgeModal.getMatchId(), MessageState.OUTGOING_SENDING,
                    nudgeModal.getQuizId(), null, null);

            try {
                JSONObject paramsSocket = new JSONObject();
                paramsSocket.put("msg", nudgeModal.getMessage());
                paramsSocket.put("quiz_id", nudgeModal.getQuizId());
                paramsSocket.put("quiz_name", nudgeModal.getQuizName());
                paramsSocket.put("message_type",
                        nudgeModal.getNudgeType().toString());
                paramsSocket.put("unique_id", randomId);
                paramsSocket.put("receiver_id", nudgeModal.getMatchId());
                paramsSocket.put("is_admin", isFromFeedbackPage);
                paramsSocket.put("isRetry", false);
                paramsSocketArray.put(paramsSocket);
            } catch (Exception ignored) {
            }

            MessageDBHandler.insertAMessage(aContext, Utility.getMyId(aContext),
                    msgObject, true, FetchSource.LOCAL, true);

            if (!SocketHandler.isSocketEnabled(aContext)) {
                HashMap<String, String> params = new HashMap<>();
                params.put("msg", nudgeModal.getMessage());
                params.put("type", "send_msg");
                params.put("message_type",
                        nudgeModal.getNudgeType().toString());
                params.put("quiz_id", nudgeModal.getQuizId() + "");
                params.put("unique_id", randomId);
                if (nudgeModal.getMatchId().equalsIgnoreCase(match_id)) {
                    OkHttpHandler.httpPost(aContext,
                            nudgeModal.getFull_conv_link(), params,
                            okHttpResponseHandler);
                } else {
                    OkHttpHandler.httpPost(aContext,
                            nudgeModal.getFull_conv_link(), params,
                            emptyResponseHandler);
                }
            }
            if (nudgeFromChat) {
                TrulyMadlyTrackEvent.trackEvent(aContext,
                        TrulyMadlyActivities.quiz,
                        TrulyMadlyEventTypes.send_nudge_chat, 0,
                        nudgeModal.getQuizId() + "", null, false);
                nudgeFromChat = false;
            } else if (nudgeFromCommonAnswers) {
                TrulyMadlyTrackEvent.trackEvent(aContext,
                        TrulyMadlyActivities.quiz,
                        TrulyMadlyEventTypes.send_nudge_common_answers, 0,
                        nudgeModal.getQuizId() + "", null, false);
                nudgeFromCommonAnswers = false;
            } else {
                TrulyMadlyTrackEvent.trackEvent(aContext,
                        TrulyMadlyActivities.quiz,
                        TrulyMadlyEventTypes.send_nudge, 0,
                        nudgeModal.getQuizId() + "", null, false);
            }
        }
        Ack ack = new Ack() {
            @Override
            public void call(Object... args) {
                if (args != null && args.length > 0 && args[0] != null) {
                    TmLogger.log(Log.DEBUG, "socketResponse",
                            args[0].toString());
                    try {
                        JSONArray nudgeResponses = (JSONArray) args[0];
                        for (int i = 0; i < nudgeResponses.length(); i++) {
                            // update conversation list only for quiz nudge for
                            // current match id.
                            sendMessageSuccess(nudgeResponses.optJSONObject(i),
                                    null, true);
                        }
                    } catch (ClassCastException e) {
                        Crashlytics.log((String) args[0]);
                        Crashlytics.logException(e);
                    }

                } else {
                }
            }
        };

        if (SocketHandler.isSocketEnabled(aContext)
                && paramsSocketArray.length() > 0) {
            Utility.fireServiceBusEvent(aContext,
                    new ServiceEvent(SERVICE_EVENT_TYPE.EMIT_ARRAY,
                            ConstantsSocket.SOCKET_EMITS.chat_sent_array, paramsSocketArray,
                            ack, true));
        }

        updateChat(true);
    }


    private void showNudgeListCancelConfirmation(int messageResId, int posResId,
                                                 int negResId, final int quizId, final String quizName) {
        AlertsHandler.showConfirmDialog(aActivity, messageResId, posResId, negResId,
                new ConfirmDialogInterface() {

                    @Override
                    public void onPositiveButtonSelected() {
                        hideAllFragments();
                        showCommonsAnswersIfPlayed(quizId, quizName);
                    }

                    @Override
                    public void onNegativeButtonSelected() {

                    }
                });

    }

    private void showQuizBlooper() {
        if (isFromFeedbackPage || (matchMessageMetaData != null && matchMessageMetaData.isMissTm()) || isSparkButNotMutualMatch()) {
            new_quiz_icon.setVisibility(View.GONE);
        } else {
            new_quiz_icon.setVisibility(View.VISIBLE);
        }
    }


    private void showGalleryBlooper() {
//        if (isFromFeedbackPage || (matchMessageMetaData != null && matchMessageMetaData.isMissTm())) {
//            new_gallery_icon.setVisibility(View.GONE);
//        } else {

        if (SPHandler.getBool(aContext,
                ConstantsSP.SHARED_KEYS_NEW_GALLERY_ICON) && !isFromFeedbackPage) {
            new_gallery_icon.setVisibility(View.VISIBLE);
        } else {
            new_gallery_icon.setVisibility(View.GONE);
        }
        stickerButton.setOnClickListener(this);
        emoticonButton.setOnClickListener(this);
    }

    private void hideQuizBlooper() {

        new_quiz_icon.setVisibility(View.GONE);
        SPHandler.setBool(aContext,
                ConstantsSP.SHARED_KEYS_NEW_QUIZ_ICON, false);

        if (Utility.isSet(user_tstamp) && Utility.isSet(match_tstamp))
            MessageDBHandler.updateLastQuizActionTime(user_tstamp, match_tstamp,
                    Utility.getMyId(aContext), match_id, aContext);
        user_tstamp = null;
        match_tstamp = null;
    }


    private void hideGalleryBlooper() {
        new_gallery_icon.setVisibility(View.GONE);
        SPHandler.setBool(aContext,
                ConstantsSP.SHARED_KEYS_NEW_GALLERY_ICON, false);
    }


    private void resumeActivity(boolean fromOnResume) {

        NotificationHandler.clearAllNotificationByCollapseKey(this, match_id);
//        Fixes Chat list jumping to bottom when coming back to this activity plus other bugs because of that too.
        if (!fromOnResume || !isInitialized) {
            loadMetaDataAndShowChat(true, false);
        } else {
            // Fixes the case when activity is in paused state and the user receives a message. The db gets updated but the UI doesn't
            // Fix : Check if there are any unread messages -> If yes, reload the list
            boolean isAnyUnread = MessageDBHandler.isAnyUnreadMessage(aContext,
                    Utility.getMyId(aContext), match_id);
            if (isAnyUnread) {
                loadMetaDataAndShowChat(true, true);
            }
        }
        if (fromOnResume) {
            startSparkTimer();
        }
//            loadMetaDataAndShowChat(true);
        startPolling();
        checkForEmptyMessages(match_id);
        checkConnecting();
        // hideAllFragments();

        // task
        // show animation and blooper
        // send myid , match_id to quiz call and respective time stamp
        // insert in the table quiz status and setString show_animation true for these
        // ids
        if (isFromFeedbackPage || matchMessageMetaData.isMissTm() || isSparkButNotMutualMatch()) {
            isCuratedDealsEnabled = false;
            mCDCdClickableState = MatchMessageMetaData.CDClickableState.DISABLED;
            enableCuratedDealsIcon();
        } else {
            isCuratedDealsEnabled = RFHandler.getBool(aContext,
                    Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_CD_ENABLED);
            mCDCdClickableState = matchMessageMetaData.getmCDClickableState();
            enableCuratedDealsIcon();
        }
        blooperCall();
        processIntentForCDMessage();
        processIntentForEventMessage();
        processIntentForSparkAcceptMessage();
    }

    private void blooperCall() {
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("action", "get_played_quiz_flare");
        parameters.put("match_id", match_id);
        parameters.put("user_id", Utility.getMyId(aContext));
        parameters.put("user_tstamp",
                matchMessageMetaData.getUserLastQuizActionTime());
        parameters.put("match_tstamp",
                matchMessageMetaData.getMatchLastQuizActionTime());
        String lastDealViewedTimeStamp = SPHandler.getString(aContext, ConstantsSP.SHARED_KEY_LAST_DEAL_VIEWED_TIMESTAMP);
        if (Utility.isSet(lastDealViewedTimeStamp)) {
            parameters.put("lastDealViewedTimeStamp",
                    lastDealViewedTimeStamp);
        }

        CustomOkHttpResponseHandler quizOkHttpResponseHandler = new CustomOkHttpResponseHandler(
                aContext, TrulyMadlyActivities.quiz,
                TrulyMadlyEventTypes.get_played_quiz_flare) {

            @Override
            public void onRequestSuccess(JSONObject responseJSON) {

                user_tstamp = responseJSON.optString("user_tstamp");
                match_tstamp = responseJSON.optString("match_tstamp");

                int appQuizVersion = SPHandler.getInt(
                        aContext, ConstantsSP.SHARED_KEYS_QUIZ_VERSION);
                int appStickerVersion = SPHandler.getInt(
                        aContext, ConstantsSP.SHARED_KEYS_STICKER_VERSION);

                if (responseJSON.optInt("sticker_version") > appStickerVersion) {
                    Utility.callStickerApi(aContext, showGalleryInterface);
                } else {
                    showGalleryBlooper();
                }

                // for quiz
                if (responseJSON.optInt("current_version") > appQuizVersion) {
                    Utility.callQuizApi(aContext);
                }

                boolean toUpdateChat = false, toUpdateStatusMap = false;
                LinkedHashMap<Integer, Boolean> flareMap, animationMap;
                flareMap = new LinkedHashMap<>();
                animationMap = new LinkedHashMap<>();
                LinkedHashMap<Integer, String> statusMap = QuizDBHandler
                        .createStatusMap(aContext, Utility.getMyId(aContext), match_id,
                                flareMap, animationMap);
                JSONArray match_quiz_ids = responseJSON
                        .optJSONArray("match_quiz");
                JSONArray user_quiz_ids = responseJSON
                        .optJSONArray("user_quiz");
                JSONArray match_ids_to_animate = new JSONArray();

                for (int i = 0; i < match_quiz_ids.length(); i++) {
                    int match_quiz_id = match_quiz_ids.optInt(i);
                    String oldStatus = statusMap.get(match_quiz_id),
                            newStatus = "MATCH";
                    if (Utility.stringCompare(oldStatus, "USER")
                            || Utility.stringCompare(oldStatus, "BOTH")) {
                        newStatus = "BOTH";
                    }
                    if (!newStatus.equals(oldStatus)
                            || animationMap.get(match_quiz_id)) {
                        match_ids_to_animate.put(match_quiz_id);
                    }
                    statusMap.put(match_quiz_id, newStatus);
                    toUpdateStatusMap = true;
                }
                for (int i = 0; i < user_quiz_ids.length(); i++) {
                    int user_quiz_id = user_quiz_ids.optInt(i);
                    String oldStatus = statusMap.get(user_quiz_id),
                            newStatus = "USER";
                    if (Utility.stringCompare(oldStatus, "MATCH")
                            || Utility.stringCompare(oldStatus, "BOTH")) {
                        newStatus = "BOTH";
                    }
                    statusMap.put(user_quiz_id, newStatus);
                    toUpdateStatusMap = true;
                }
                if (toUpdateStatusMap) {
                    QuizDBHandler.insertStatusMap(aContext, Utility.getMyId(aContext),
                            match_id, statusMap);
                }

                if (match_ids_to_animate.length() > 0) {
                    showQuizBlooper();
                    QuizDBHandler.insertAnimationStatus(aContext,
                            Utility.getMyId(aContext), match_id, match_ids_to_animate);
                    toUpdateChat = true;

                }

                JSONArray flare_ids = responseJSON.optJSONArray("flare");
                if (flare_ids != null && flare_ids.length() > 0) {
                    QuizDBHandler.insertFlareStatus(aContext, Utility.getMyId(aContext),
                            match_id, flare_ids);
                    toUpdateChat = true;
                }

                if (toUpdateChat) {
                    updateChat(false);
                    if (fragmentSelected == CURRENT_FRAGMENT.localQuizFragment) {
                        localQuizFragment.setQuizGridAdapter();
                    }
                }

                if (isFromFeedbackPage || matchMessageMetaData.isMissTm()) {
                    isCuratedDealsEnabled = false;
                    mCDCdClickableState = MatchMessageMetaData.CDClickableState.DISABLED;
                    enableCuratedDealsIcon();
                } else {
                    JSONObject curatedDealsIcon = responseJSON.optJSONObject("curated_deal_icon");
                    if (curatedDealsIcon != null) {
                        boolean cdClickable = curatedDealsIcon.optBoolean("icon_clickable");
                        mCDCdClickableState = MatchMessageMetaData.CDClickableState
                                .createFromBoolean(cdClickable);
                        if (!isSparkButNotMutualMatch()) {
                            enableCuratedDealsIcon();
                        }
                        MessageDBHandler.updateCuratedDealsClickableFlag(
                                Utility.getMyId(aContext), match_id,
                                aContext, mCDCdClickableState);
                        matchMessageMetaData.setmCDClickableState(mCDCdClickableState);
                    }
                }
                mShowSendPhotoButton = message_editor.getText().length() == 0;
                toggleSendButton();

                actionBarHandler.toggleCuratedDealsBlooper(responseJSON.optInt("newDatesCount", 0));
            }

            @Override
            public void onRequestFailure(Exception exception) {

            }

        };
        OkHttpHandler.httpPost(aContext, ConstantsUrls.get_quiz_url(), parameters,
                quizOkHttpResponseHandler);

    }

    private void showTypingIndicator() {
        if (matchMessageMetaData == null) {
            return;
        }
        if (typingIndicatorHandler == null) {
            typingIndicatorHandler = new Handler();
        }
        if (typingIndicatorRunnable == null) {
            typingIndicatorRunnable = new Runnable() {
                public void run() {
                    hideTypingIndicator();
                }
            };
        }
        typing_indicator.setVisibility(View.VISIBLE);
        if (!isTypingJumpingbeansVisible) {
            mJumpingBeans = JumpingBeans.with(typing_indicator_dots_jumping_tv).appendJumpingDots().build();
            isTypingJumpingbeansVisible = true;
        }

        typingIndicatorHandler.removeCallbacks(typingIndicatorRunnable);
        typingIndicatorHandler.postDelayed(typingIndicatorRunnable,
                Constants.CHAT_TYPING_THRESHOLD);

    }

    private void hideTypingIndicator() {
        if (mJumpingBeans != null && isTypingJumpingbeansVisible) {
            mJumpingBeans.stopJumping();
            isTypingJumpingbeansVisible = false;
        }
        typing_indicator.setVisibility(View.GONE);
        if (typingIndicatorHandler != null && typingIndicatorRunnable != null) {
            typingIndicatorHandler.removeCallbacks(typingIndicatorRunnable);
        }
    }


    private void toggleMenuOptionsVisibility(boolean visible) {
        if (menu != null) {
            menu.setGroupVisible(R.id.main_menu_group, visible);
            if (visible) {
                MenuItem unmatchItem = menu.findItem(R.id.menu_item_unmatch_user);
                MenuItem clearChat = menu.findItem(R.id.menu_item_clear_chat);
                MenuItem deleteSpark = menu.findItem(R.id.menu_item_delete_spark);
                MenuItem blockUser = menu.findItem(R.id.menu_item_block_user);
                if (isSparkButNotMutualMatch()) {
                    unmatchItem.setVisible(false);
                    clearChat.setVisible(false);
                    deleteSpark.setVisible(true);
                    blockUser.setVisible(false);
                } else {
                    unmatchItem.setVisible(true);
                    clearChat.setVisible(true);
                    deleteSpark.setVisible(false);
                    blockUser.setVisible(true);
                    actionBarHandler.toggleShareView(isFemale);
                }
            }
        }
    }

    private void showShareTutorial(boolean showShareView) {
        //noinspection PointlessBooleanExpression
        if (false && // Hack to hide share tutorial permanantly
                !isAnyTutorialShownInThisSession && showShareView && isCDChatTutorialShown() && !matchMessageMetaData.isMissTm() && !isFromFeedbackPage) {
            boolean isShareTutorialShowBefore = RFHandler.getBool(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_SHARE_PROFILE_TUTORIAL_SHOWN);
            if (!isShareTutorialShowBefore) {
                UiUtils.setBackground(aActivity, share_tutorial_dialog_bg_view, R.drawable.share_tutorial_dialog_bg);
                Picasso.with(aContext).load(R.drawable.share_tutorial_icon).noFade().into(share_profile_tutorial_icon);
                share_profile_tutorial.setVisibility(View.VISIBLE);
                isShareTutorialVisible = true;
                actionBarHandler.showShareTutorial(true);
                RFHandler.insert(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_SHARE_PROFILE_TUTORIAL_SHOWN, true);
                isAnyTutorialShownInThisSession = true;
            } else {
                hideShareTutorial();
            }
        } else {
            hideShareTutorial();
        }
    }

    private void hideShareTutorial() {
        if (isShareTutorialVisible) {
            actionBarHandler.showShareTutorial(false);
            share_profile_tutorial.setVisibility(View.GONE);
            isShareTutorialVisible = false;
        }
    }

    @Override
    public void onEmojiconBackspaceClicked(View v) {
        EmojiconsFragment.backspace(message_editor);
    }

    @Override
    public void onEmojiconClicked(Emojicon emojicon, String categoryName) {
        EmojiconsFragment.input(message_editor, emojicon);
        String status = emojicon.getEmoji();
        TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyActivities.emoticons, categoryName, 0, status, null, true);
    }

    private void onLoadMoreClicked() {
        mCurrentPosition = Constants.DEFAULT_SLOT_SIZE;
        mOldRecordsCount = messageListAdapter.getItemCount();
        mCurrentSlotSize++;
        updateChat(false);
    }


    private enum CURRENT_FRAGMENT {
        localEmoticonFragment, localStickerFragment, localQuizFragment, localCommonAnswersFragment, localTakeQuizFragment, localNudgeList, none
    }


    private class ProcessPollingDataAsyncTask extends AsyncTask<Void, Void, Boolean> {
        private final boolean isFromFeedbackPage;
        private final MatchMessageMetaData matchMessageMetaData_thread;
        private final String match_id_thread;
        private final String my_id_thread;
        boolean update_message_list = false;
        boolean update_message_meta_data = false;
        private JSONArray message_list;
        private JSONObject receiver_data;

        public ProcessPollingDataAsyncTask(JSONObject data, String my_id_thread,
                                           String match_id_thread,
                                           MatchMessageMetaData matchMessageMetaData_thread,
                                           boolean isFromFeedbackPage) {
            if (data != null && data.length() > 0) {
                receiver_data = data.optJSONObject("receiver");
                message_list = data.optJSONArray("message_list");
            }
            this.my_id_thread = my_id_thread;
            this.match_id_thread = match_id_thread;
            this.matchMessageMetaData_thread = matchMessageMetaData_thread;
            this.isFromFeedbackPage = isFromFeedbackPage;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            boolean isProcessed = false;

            TmLogger.log(Log.DEBUG, TAG, "parse polling response");

            chatReadComplete(match_id, null);

            if (message_list != null && message_list.length() > 0) {
                for (int i = 0; i < message_list.length(); i++) {
                    MessageModal messageObj = new MessageModal(my_id_thread);

                    messageObj.parseMessage(message_list.optJSONObject(i));
                    if (!Utility.isSet(messageObj.getMatch_id())
                            || isFromFeedbackPage) {
                        messageObj.setMatch_id(match_id);
                    }
                    if (messageObj
                            .getMessageState() == MessageState.INCOMING_DELIVERED) {
                        // setting messages state as read in case on manual polling
                        // as it is on the same activity.
                        messageObj.setMessageState(MessageState.INCOMING_READ);
                    }

                    MessageDBHandler.insertAMessage(aContext, my_id_thread,
                            messageObj, true, FetchSource.POLLING, messageObj.getMessageType() != MessageType.SPARK);

                    if (!Utility.isSet(
                            matchMessageMetaData_thread.getLastFetchedTstamp())
                            || messageObj.getTstamp()
                            .compareTo(matchMessageMetaData_thread
                                    .getLastFetchedTstamp()) >= 0) {
                        update_message_list = true;
                        matchMessageMetaData_thread
                                .setLastFetchedTstamp(messageObj.getTstamp());
                        matchMessageMetaData_thread
                                .setLastFetchedId(messageObj.getMsg_id());
                    }
                }
                isProcessed = true;
            }

            if (receiver_data != null) {
                MatchMessageMetaData metaDetails = new MatchMessageMetaData();
                metaDetails.generateMatchMessageMetaDataFromServer(receiver_data);

                if (metaDetails.isSetLastSeenReceiverTstamp()) {
                    if (!Utility.isSet(
                            matchMessageMetaData_thread.getLastSeenReceiverTstamp())
                            || metaDetails.getLastSeenReceiverTstamp()
                            .compareTo(matchMessageMetaData_thread
                                    .getLastSeenReceiverTstamp()) > 0) {
                        update_message_list = true;
                        matchMessageMetaData_thread.setLastSeenReceiverTstamp(
                                metaDetails.getLastSeenReceiverTstamp());
                    }
                }
                // check for photo_upload_nudge_skipped_view as well
                if (!Utility.stringCompare(metaDetails.getProfilePic(),
                        matchMessageMetaData_thread.getProfilePic())) {
                    update_message_meta_data = true;
                    matchMessageMetaData_thread
                            .setProfilePic(metaDetails.getProfilePic());
                }
                if (!Utility.stringCompare(metaDetails.getProfileUrl(),
                        matchMessageMetaData_thread.getProfileUrl())) {
                    update_message_meta_data = true;
                    matchMessageMetaData_thread
                            .setProfileUrl(metaDetails.getProfileUrl());
                }
                if (!Utility.stringCompare(metaDetails.getFName(),
                        matchMessageMetaData_thread.getFName())) {
                    update_message_meta_data = true;
                    matchMessageMetaData_thread.setFName(metaDetails.getFName());
                }
                // check for admin_id
                if (isFromFeedbackPage
                        && !Utility.stringCompare(metaDetails.getMatchId(),
                        matchMessageMetaData_thread.getMatchId())) {
                    update_message_meta_data = true;
                    matchMessageMetaData_thread
                            .setMatchId(metaDetails.getMatchId());
                }

                if (update_message_meta_data || update_message_list) {
                    MessageDBHandler.insertMessageDetails(
                            matchMessageMetaData_thread, aContext);
                }
            }

            return isProcessed;

        }


        @Override
        protected void onPostExecute(Boolean isProcessed) {
            if (isProcessed) {
                checkAndRestartPolling();
            }
            runOnUiThread(new UpdateUIThread(update_message_list,
                    update_message_meta_data, match_id_thread,
                    matchMessageMetaData_thread, isProcessed));
        }
    }

    public class UpdateUIThread implements Runnable {

        final String match_id_thread;
        final MatchMessageMetaData matchMessageMetaData_thread;
        private boolean update_message_list = false;
        private boolean update_message_meta_data = false;
        private boolean jumpToBottom = false;

        public UpdateUIThread(boolean update_message_list,
                              boolean update_message_meta_data, String match_id,
                              MatchMessageMetaData matchMessageMetaData_thread, boolean jumpToBottom) {
            this.update_message_list = update_message_list;
            this.update_message_meta_data = update_message_meta_data;
            this.match_id_thread = match_id;
            this.matchMessageMetaData_thread = matchMessageMetaData_thread;
            this.jumpToBottom = jumpToBottom;
        }

        public void run() {
            if (match_id.equals(match_id_thread) && isResumed) {
                if (update_message_meta_data) {
                    matchMessageMetaData = matchMessageMetaData_thread;
//                    matchMessageMetaData.setProfilePic("http://media.salon.com/2013/02/house_of_cards2.jpg");
                    setHeaderDetails();
                }

                if (update_message_list) {
//                    Cursor cursor = MessageDBHandler.getAllMessages(
//                            Utility.getMyId(aContext), match_id, mCurrentSlotSize, aContext);

                    ArrayList<MessageModal> messageModals = MessageDBHandler.getAllMessagesModals(
                            Utility.getMyId(aContext), match_id, mCurrentSlotSize, aContext);
                    toggleLoadMore();
                    // http://stackoverflow.com/questions/13953773/android-listview-not-refreshing-after-change-cursor
                    messageListAdapter.setLastSeenReceiverTstamp(
                            matchMessageMetaData.getLastSeenReceiverTstamp());
//                    messageListAdapter.changeCursor(cursor);
                    messageListAdapter.changeList(messageModals, isLoadMoreEnabled, isSparkHeaderRequired(), match_id);

                    //To jump to bottom of the list if a new message arrives
                    if (jumpToBottom) {
                        mLinearLayoutManager.scrollToPosition(messageModals.size() - ((isLoadMoreEnabled) ? 0 : 1));
                    }

                    initDoodle(messageModals.size() > 0);
                }
            }
            if (!SocketHandler.isSocketEnabled(aContext)) {
                showConnecting(SOCKET_STATE.POLLING, false);
                pollServerForRealTimeChatting(false, false);
            }
        }
    }

}
