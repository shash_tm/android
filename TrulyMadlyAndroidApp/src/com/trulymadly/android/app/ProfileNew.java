package com.trulymadly.android.app;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Toast;

import com.moe.pushlibrary.MoEHelper;
import com.squareup.otto.Subscribe;
import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.bus.BusProvider;
import com.trulymadly.android.app.bus.NetworkChangeEvent;
import com.trulymadly.android.app.fragments.SelectQuizResultFragment;
import com.trulymadly.android.app.json.Constants.HttpRequestType;
import com.trulymadly.android.app.json.ConstantsRF;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.json.ProfileNewResponseParser;
import com.trulymadly.android.app.listener.CachedDataInterface;
import com.trulymadly.android.app.listener.ConfirmDialogInterface;
import com.trulymadly.android.app.listener.CustomOkHttpResponseHandler;
import com.trulymadly.android.app.listener.OnActionBarClickedInterface;
import com.trulymadly.android.app.listener.OnActionBarMenutItemProfileDeactivateClickedInterface;
import com.trulymadly.android.app.listener.TapToRetryListener;
import com.trulymadly.android.app.modal.FbFriendModal;
import com.trulymadly.android.app.modal.MatchesLatestModal2;
import com.trulymadly.android.app.modal.ProfileNewModal;
import com.trulymadly.android.app.modal.SelectQuizCompareModal;
import com.trulymadly.android.app.modal.UserModal;
import com.trulymadly.android.app.modal.VideoModal;
import com.trulymadly.android.app.sqlite.CachingDBHandler;
import com.trulymadly.android.app.sqlite.RFHandler;
import com.trulymadly.android.app.utility.ActionBarHandler;
import com.trulymadly.android.app.utility.ActivityHandler;
import com.trulymadly.android.app.utility.AdsTrackingHandler;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.CachedDataHandler;
import com.trulymadly.android.app.utility.GooGlUrlShortner;
import com.trulymadly.android.app.utility.GooGlUrlShortner.GooGlUrlShortnerInterface;
import com.trulymadly.android.app.utility.MoEHandler;
import com.trulymadly.android.app.utility.OkHttpHandler;
import com.trulymadly.android.app.utility.TMSelectHandler;
import com.trulymadly.android.app.utility.TapToRetryHandler;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;
import com.trulymadly.android.app.utility.WebviewHandler;
import com.trulymadly.android.app.utility.WebviewHandler.WebviewActionsListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities.select;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes.select_detail_card_profile;


public class ProfileNew extends AppCompatActivity implements View.OnClickListener {

    private static boolean fb_mutual_friend_visible;
    @BindView(R.id.continue_button_profile)
    View continue_button_profile;
    @BindView(R.id.category_nudge_layout)
    View category_nudge_layout;

    @BindView(R.id.indicator2)
    View indicator;

//    @BindView(R.id.mutual_friend_layout)
//    View mutual_friend_layout;

    @BindView(R.id.webview_include_view)
    View mWebviewIncludeView;
    //Select
    @BindView(R.id.select_quiz_result_container)
    View mSelectQuizResultContainer;
    private boolean isMyProfile = false;
    private boolean isPush = false;
    private boolean isFromRegistration = false;    //profile Ad
    private MoEHelper mhelper = null;
    private String profileUrl = "";
    private TapToRetryHandler tapToRetryHandler;
    private MatchesPageSetter aMatchesPageSetter;
    private View matches_page_setter, custom_prog_bar_id;
    private Vector<ProfileNewModal> profilesList;
    private Context aContext;
    private Activity aActivity;
    private ProgressDialog mProgressDialog;
    private ProfileNewModal aProfileNewModal = null;
    private CachedDataInterface callback;
    private WebviewHandler mWebviewHandler;
    private boolean isCategoryNudgeVisible = false;
    private OnClickListener mFBOnClickListener;
    private SelectQuizResultFragment mSelectQuizresultFragment;
    private boolean isSelectQuizCompareVisible = false;
    private SelectQuizResultFragment.SelectQuizCompareListener mSelectQuizCompareListener;

    public static void setFbMutualFriendsVisibilty() {
        fb_mutual_friend_visible = false;
    }

    private static void onShareProfileClicked(final Activity aActivity,
                                              final String user_id, final String share_message,
                                              final String share_link, String alertMessage,
                                              final String event_status) {

        ConfirmDialogInterface confirmDialogInterface = new ConfirmDialogInterface() {

            @Override
            public void onPositiveButtonSelected() {
                GooGlUrlShortnerInterface responseInterface = new GooGlUrlShortnerInterface() {

                    @Override
                    public void onComplete(String shortUrl) {
                        TrulyMadlyTrackEvent.trackEvent(aActivity,
                                TrulyMadlyActivities.ask_friends,
                                TrulyMadlyEventTypes.ask_friend_confirm, 0,
                                event_status, null, true);
                        RFHandler.insert(aActivity,
                                ConstantsRF.RIGID_FIELD_PROFILE_SHARED + user_id, true);
                        ActivityHandler.createTextIntent(aActivity, share_message + " "
                                + shortUrl, false);
                    }
                };
                new GooGlUrlShortner(aActivity, responseInterface, share_link);
            }

            @Override
            public void onNegativeButtonSelected() {
                TrulyMadlyTrackEvent.trackEvent(aActivity,
                        TrulyMadlyActivities.ask_friends,
                        TrulyMadlyEventTypes.ask_friend_cancel, 0,
                        event_status, null, true);
            }
        };
        TrulyMadlyTrackEvent.trackEvent(aActivity,
                TrulyMadlyActivities.ask_friends,
                TrulyMadlyEventTypes.ask_friend_click, 0, event_status, null, true);
        AlertsHandler.showConfirmDialog(aActivity, alertMessage, R.string.ask_friends,
                R.string.cancel, confirmDialogInterface, false);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (profileUrl != null && !profileUrl.equals("")) {
            issueRequest(profileUrl);
        }

        if(aMatchesPageSetter != null){
            aMatchesPageSetter.onResume();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(aMatchesPageSetter != null){
            aMatchesPageSetter.onPause();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.profile_new);
        } catch (OutOfMemoryError e) {
            System.gc();
            finish();
            return;
        }
        //http://www.curious-creature.com/2012/12/01/android-performance-case-study/
        //For reducing overdraws : Suggested by Romain guy
        getWindow().setBackgroundDrawable(null);

        Utility.disableScreenShot(this);
        aContext = this;
        aActivity = this;
        ButterKnife.bind(this);

        mFBOnClickListener = this;


        WebviewActionsListener mWebviewActionsListener = new WebviewActionsListener() {
            @Override
            public boolean shouldOverrideUrlLoading(String url) {
                mWebviewHandler.loadUrl(url);
                return true;
            }

            @Override
            public void webViewHiddenOnUrlLoad() {

            }

            @Override
            public void onWebViewCloseClicked() {

            }
        };
        mWebviewHandler = new WebviewHandler(mWebviewIncludeView, mWebviewActionsListener, true, true);

        Intent i = getIntent();
        Bundle b = i.getExtras();
        if (b != null) {
            profileUrl = getIntent().getStringExtra("profileUrl");

            isMyProfile = Utility.stringCompare(getIntent().getStringExtra("isMyProfile"), "true") || getIntent().getBooleanExtra("isMyProfile", false);
            isFromRegistration = Utility.stringCompare(getIntent().getStringExtra("isFromRegistration"), "true") || getIntent().getBooleanExtra("isFromRegistration", false);
            isPush = Utility.stringCompare(getIntent().getStringExtra("isPush"), "true") || getIntent().getBooleanExtra("isPush", false);

        }

        continue_button_profile.setVisibility(isFromRegistration ? View.VISIBLE
                : View.GONE);

        ActionBarClickImplementation onActionBarClickedInterface = new ActionBarClickImplementation();

        ActionBarHandler abHandler = new ActionBarHandler(this,
                isFromRegistration ? getResources().getString(R.string.how_i_look)
                        : (isMyProfile ? getResources().getString(R.string.my_profile)
                        : getResources().getString(R.string.profile)), null,
                onActionBarClickedInterface, false, false, isMyProfile);
        abHandler.toggleNotificationCenter(false);
        if (isMyProfile) {
            Utility.showOptionsMenuByForce(aContext);
        }

        mhelper = new MoEHelper(this);
        tapToRetryHandler = new TapToRetryHandler(this,
                findViewById(android.R.id.content), new TapToRetryListener() {
            @Override
            public void reInitialize() {
                issueRequest(profileUrl);
            }
        }, null);

        matches_page_setter = findViewById(R.id.matches_page_setter);
        custom_prog_bar_id = findViewById(R.id.custom_prog_bar_id);

        aMatchesPageSetter = new MatchesPageSetter(this, matches_page_setter,
                isMyProfile, !isMyProfile, false, isFromRegistration ? 80 : 0, mFBOnClickListener);
        callback = new CachedDataInterface() {
            @Override
            public void onSuccess(JSONObject response) {
                tapToRetryHandler.onSuccessFull();
                responseParser(response);
            }

            @Override
            public void onError(Exception e) {
                tapToRetryHandler.onNetWorkFailed(e);

            }
        };

        if(aMatchesPageSetter != null){
            aMatchesPageSetter.onCreate();
        }
    }

    @OnClick(R.id.continue_button_profile)
    public void onClickContinue() {
        ActivityHandler.startMatchesActivity(aContext);
        finish();
    }

    private void responseParser(JSONObject response) {
        if (response != null) {
            ProfileNewResponseParser aProfileNewResponseParser = null;
            aProfileNewResponseParser = new ProfileNewResponseParser();
            MatchesLatestModal2 aMatchesLatestModal2 = null;

            aProfileNewResponseParser.setDbInsertionDisabled(true);
            aMatchesLatestModal2 = aProfileNewResponseParser
                    .parseProfileNewResponse(response, getApplicationContext(),
                            false, false, false);

            profilesList = null;
            if (aMatchesLatestModal2 != null) {
                profilesList = aMatchesLatestModal2.getProfileNewModalList();
            }
            if (profilesList != null && profilesList.size() > 0) {
                aProfileNewModal = profilesList.get(0);
            }

            ArrayList<FbFriendModal> firends = ProfileNewResponseParser.parseFBList(response, true);
            aProfileNewModal.setFbFriendList(firends);
            aMatchesPageSetter.getmFBMutualFriendsHandler().setmFbFriendsList(aProfileNewModal.getFbFriendList());

            OnClickListener lis = new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (profilesList == null || profilesList.size() == 0 || profilesList.get(0).getUser() == null)
                        return;

                    ProfileNewModal profileNewModal = profilesList.elementAt(0);
                    if (profileNewModal.isSponsoredProfile()) {

                        //Click Tracking
                        //GA & TRK
                        HashMap<String, String> eventInfo = new HashMap<>();
                        eventInfo.put("source", TrulyMadlyEvent.TrulyMadlyActivities.profile);
                        TrulyMadlyTrackEvent.trackEvent(aActivity, TrulyMadlyEvent.TrulyMadlyActivities.campaigns,
                                TrulyMadlyEventTypes.click, 0, profileNewModal.getUserId(),
                                eventInfo, true);

                        //Tracking URLS from server
                        AdsTrackingHandler.callUrls(profileNewModal.getmClicksTrackingUrls());

                        //If landing url is given -> show it in the webview and return
                        if (Utility.isSet(profileNewModal.getmLandingUrl())) {
                            mWebviewHandler.loadUrl(profileNewModal.getmLandingUrl());
                            return;
                        }
                    }

                    //If (sponsored profile && landing url not present) || (is not sponsored profile)
//                    ActivityHandler.startAlbumFullViewPagerForResult(aActivity,
//                            Integer.parseInt(v.getTag().toString()),
//                            profileNewModal.getUser().getOtherPics(),
//                            profileNewModal.getUser().getVideoUrls(),
//                            profileNewModal.getUser().getThumbnailUrls(),
//                            getResources().getString(R.string.photo_gallery));

                    VideoModal[] videoModals = null;
                    ArrayList<VideoModal> videoModalsList = profileNewModal.getUser().getVideoArray();
                    if(videoModalsList != null && videoModalsList.size() > 0){
                        videoModals = videoModalsList.toArray(new VideoModal[videoModalsList.size()]);
                    }

                    ActivityHandler.startAlbumFullViewPagerForResult(aActivity,
                            Integer.parseInt(v.getTag().toString()),
                            profileNewModal.getUser().getOtherPics(), videoModals,
                            getResources().getString(R.string.photo_gallery), isMyProfile ? TrulyMadlyActivities.my_profile
                                    : TrulyMadlyActivities.profile, profilesList.get(0).getUser().getUserId());
                }
            };

            if (aProfileNewModal != null && aMatchesLatestModal2 != null) {
                custom_prog_bar_id.setVisibility(View.GONE);
                matches_page_setter.setVisibility(View.VISIBLE);
                aMatchesPageSetter.instantiateItem(aProfileNewModal, lis,
                        aMatchesLatestModal2.getMatchesSysMesgModal(),
                        aMatchesLatestModal2.getMyDetailModal(), null);
            }
        }
    }

    private void issueRequest(String url) {

        Map<String, String> event_info;
        event_info = new HashMap<>();
        if (Utility.isSet(url)) {
            event_info.put("profile_url", url);
        }
        custom_prog_bar_id.setVisibility(View.VISIBLE);
        boolean showFailure = CachedDataHandler.showDataFromDb(aContext, url, callback);
        CachedDataHandler cachedDataHandler = new CachedDataHandler(url, callback, this, TrulyMadlyActivities.profile, TrulyMadlyEventTypes.page_load, event_info, showFailure);
        Map<String, String> map = new HashMap<>();
        map.put("hash", CachingDBHandler.getHashValue(aContext, url, Utility.getMyId(aContext)));
        map.put("fav_v2", "true");
        cachedDataHandler.httpGet(this, url, map);
    }

    @Override
    public void onStart() {
        super.onStart();
        BusProvider.getInstance().register(this);

        if(aMatchesPageSetter != null){
            aMatchesPageSetter.onStart();
        }
    }

//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		if (isMyProfile && !isFromRegistration) {
//			abHandler
//					.setOptionsMenuProfileDeativateHandler(onActionBarMenutItemProfileDeactivateClickedInterface);
//			getMenuInflater().inflate(
//					R.menu.action_bar_menu_profile_deactivate, menu);
//		}
//		return true;
//	}
//
//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//		abHandler.onOptionsItemSelected(item);
//		return super.onOptionsItemSelected(item);
//	}

    @Subscribe
    public void onNetworkChanged(NetworkChangeEvent networkChangeEvent){
        if(aMatchesPageSetter != null) {
            aMatchesPageSetter.onNetworkChanged(networkChangeEvent);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        BusProvider.getInstance().unregister(this);

        if(aMatchesPageSetter != null){
            aMatchesPageSetter.onStop();
        }
    }

    private void sendReportProfileDeactivateToServer(String object) {
        mProgressDialog = UiUtils.showProgressBar(aContext, mProgressDialog);

        CustomOkHttpResponseHandler okHttpResponseHandler = new CustomOkHttpResponseHandler(
                aContext) {

            @Override
            public void onRequestSuccess(JSONObject response) {
                mProgressDialog = UiUtils.showProgressBar(aContext,
                        mProgressDialog);
                Utility.logoutSession(aActivity);
            }

            @Override
            public void onRequestFailure(Exception exception) {
                mProgressDialog = UiUtils.hideProgressBar(mProgressDialog);
                AlertsHandler.showNetworkError(aActivity, exception);

            }
        };
        OkHttpHandler.httpGet(aContext, ConstantsUrls.get_profile_deactivate_url(),
                GetOkHttpRequestParams.getHttpRequestParams(
                        HttpRequestType.PROFILE_DEACTIVATE, object),
                okHttpResponseHandler);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == Constants.EDIT_PROFILE_FROM_PROFILE) {
//            if (data != null && data.getExtras() != null
//                    && data.getExtras().getBoolean("isSave")) {
//
//                //issueRequest(profileUrl);
//            }
//        }
//        if (requestCode == Constants.GET_PHOTOS_FROM_ACTIVITY) {
//            if (data != null && data.getExtras() != null
//                    && data.getExtras().getBoolean("isUpdated")) {
//                issueRequest(profileUrl);
//            }
//        }
//        if (requestCode == Constants.GET_TRUST_FROM_ACTIVITY) {
//            if (data != null && data.getExtras() != null
//                    && data.getExtras().getBoolean("isUpdated")) {
//                issueRequest(profileUrl);
//            }
//        }

    }

    @Override
    public void onBackPressed() {
        if (isSelectQuizCompareVisible) {
            isSelectQuizCompareVisible = false;
            mSelectQuizresultFragment.registerListener(null);
            mSelectQuizResultContainer.setVisibility(View.GONE);
            return;
        }
        if (mWebviewHandler != null && mWebviewHandler.isVisible()) {
            mWebviewHandler.hide();
            return;
        }
        if (!isMyProfile && isPush && isTaskRoot()) {
            ActivityHandler.startConversationListActivity(aContext);
            finish();
        } else if (fb_mutual_friend_visible) {
            fb_mutual_friend_visible = false;
            aMatchesPageSetter.toggleMutualFriendsLayout(false, false, null);
        } else {
            try {
                super.onBackPressed();
            } catch (NullPointerException | IllegalStateException | InternalError e) {
                // gionee NullPointerException
            }
        }
    }

    @OnClick(R.id.category_nudge_layout)
    public void onCategoryNudgeClickedClick(View view) {
        isCategoryNudgeVisible = !isCategoryNudgeVisible;
        aMatchesPageSetter.toggleCategoryNudgeVisibility(isCategoryNudgeVisible);
    }


    //        private ArrayList<FbFriendModal>createDummyData()
//    {
//        ArrayList<FbFriendModal> friendList=new ArrayList<FbFriendModal>();
//        for (int i =0;i<15;i++)
//        {
//            FbFriendModal friend= new FbFriendModal();
//            friend.setName("devesh");
//            friend.setThumbnail("http://techstory.in/wp-content/uploads/2015/03/zomato-logo.png");
//            friendList.add(friend);
//        }
//        return friendList;
//    }
    @OnClick(R.id.fb_friend_container)
    public void onClick(View view) {

        int id = view.getId();
        switch (id) {
            case R.id.fb_friend_container:
                if (RFHandler.getBool(aContext, Utility.getMyId(aContext), ConstantsRF.ALL_MUTUAL_FRIENDS)) {
                    fb_mutual_friend_visible = !fb_mutual_friend_visible;
                    final ProfileNewModal profileModal = profilesList.get(0);
                    if (fb_mutual_friend_visible) {
                        aMatchesPageSetter.toggleMutualFriendsLayout(true, false, profileModal.getFbFriendList());
                    } else {
                        aMatchesPageSetter.toggleMutualFriendsLayout(false, true, profileModal.getFbFriendList());
                    }
                }
                break;

            case R.id.mutual_friend_layout:
                fb_mutual_friend_visible = false;
                aMatchesPageSetter.toggleMutualFriendsLayout(false, false, null);
                indicator.setVisibility(View.VISIBLE);
                break;

            case R.id.sub_mutual_friend_layout:
                if (fb_mutual_friend_visible) {
                    fb_mutual_friend_visible = false;
                    aMatchesPageSetter.toggleMutualFriendsLayout(false, false, null);
                }
                break;

            case R.id.select_details_card:
                boolean isSelectMember = TMSelectHandler.isSelectMember(aContext);
                Map<String, String> eventInfoSelect = new HashMap<>();
                eventInfoSelect.put("is_select_member", String.valueOf(isSelectMember));
                eventInfoSelect.put("source", "profile");
                TrulyMadlyTrackEvent.trackEvent(aContext,
                        select, select_detail_card_profile, 0,
                        TrulyMadlyEvent.TrulyMadlyEventStatus.clicked, eventInfoSelect, true);

                if (isSelectMember && TMSelectHandler.isSelectQuizPlayed(aContext)) {
                    UserModal user = profilesList.get(0).getUser();
                    toggleSelectQuizResult(profilesList.get(0).getUserId(),
                            user.getName(),
                            user.getProfilePic(),
                            Utility.getMyProfilePic(aContext),
                            profilesList.get(0).getmSelectCommonString(),
                            profilesList.get(0).getmSelectQuote());
                } else {
                    ActivityHandler.startTMSelectActivity(aContext, select_detail_card_profile + ":profile", false);
                }
                break;

        }
    }

    private boolean toggleSelectQuizResult(String matchId, String matchName, String matchUrl,
                                           String userUrl, String compatibilityString, String quote) {
        if (mSelectQuizresultFragment == null) {
            mSelectQuizCompareListener = new SelectQuizResultFragment.SelectQuizCompareListener() {
                @Override
                public void closeFragment() {
                    onBackPressed();
                }
            };
            mSelectQuizresultFragment = SelectQuizResultFragment.newInstance(new
                            SelectQuizCompareModal(matchId, matchName, matchUrl, userUrl),
                    compatibilityString, quote);
            getSupportFragmentManager().beginTransaction().add(R.id.select_quiz_result_container,
                    mSelectQuizresultFragment).commit();
        } else {
            mSelectQuizresultFragment.reInitialize(new SelectQuizCompareModal(matchId,
                    matchName, matchUrl, userUrl), compatibilityString, quote);
        }
        mSelectQuizresultFragment.registerListener(mSelectQuizCompareListener);
        mSelectQuizResultContainer.setVisibility(View.VISIBLE);
        isSelectQuizCompareVisible = true;
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (aMatchesPageSetter != null) {
            aMatchesPageSetter.onDestroy();
        }
    }

    class ActionBarClickImplementation implements OnActionBarClickedInterface {

        @Override
        public void onUserProfileClicked() {
        }

        @Override
        public void onConversationsClicked() {
        }

        @Override
        public void onLocationClicked() {
        }

        @Override
        public void onBackClicked() {
            try {
                onBackPressed();
            } catch (NullPointerException e) {
                // gionee
            }
        }

        @Override
        public void onTitleClicked() {
        }

        @Override
        public void onTitleLongClicked() {
        }

        @Override
        public void onSearchViewOpened() {

        }

        @Override
        public void onSearchViewClosed() {

        }

        @Override
        public void onCategoriesClicked() {

        }

        @Override
        public void onCuratedDealsClicked() {

        }
    }

    class ReportAbuseOnCheckChangeListenerImplementation implements
            OnCheckedChangeListener {

        final EditText other_comments;

        public ReportAbuseOnCheckChangeListenerImplementation(
                EditText other_comments) {
            this.other_comments = other_comments;
        }

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            int radioButtonID = group.getCheckedRadioButtonId();
            if (radioButtonID == R.id.profileblock6) {
                other_comments.setVisibility(View.VISIBLE);
            } else {
                other_comments.setVisibility(View.GONE);
            }
        }
    }

    class ReportAbuseButtonOnClickListner implements View.OnClickListener {

        final RadioGroup radioGroup;
        final AlertDialog alertDialog;
        final View layout;

        public ReportAbuseButtonOnClickListner(RadioGroup radioGroup,
                                               AlertDialog alertDialog, View layout) {
            this.radioGroup = radioGroup;
            this.alertDialog = alertDialog;
            this.layout = layout;
        }

        @Override
        public void onClick(View v) {

            if (!Utility.isNetworkAvailable(aContext)) {
                AlertsHandler.showMessage(aActivity, R.string.ERROR_NETWORK_FAILURE);
            } else {
                int clickedbutton = radioGroup.getCheckedRadioButtonId();
                EditText other_reason = (EditText) layout
                        .findViewById(R.id.other_reason);

                if (clickedbutton != -1
                        || (other_reason.getText() != null && !((other_reason
                        .getText().toString()).trim()).equals(""))) {
                    RadioButton button = (RadioButton) layout
                            .findViewById(clickedbutton);
                    String reason = button.getText().toString();
                    JSONObject newJson = new JSONObject();
                    try {
                        newJson.put("reason", reason);
                    } catch (JSONException ignored) {
                    }
                    MoEHandler.trackEvent(aContext, MoEHandler.Events.DEACTIVATE, newJson, true);
                    if (reason.equals("Other")) {

                        if ((other_reason.getText() != null && !((other_reason
                                .getText().toString()).trim()).equals(""))) {
                            reason = reason + ", "
                                    + other_reason.getText().toString();
                            alertDialog.dismiss();
                            sendReportProfileDeactivateToServer(reason);

                        } else {
                            Toast.makeText(
                                    aContext,
                                    "Please enter a reason for deactivating the profile",
                                    Toast.LENGTH_LONG).show();
                        }
                    } else {
                        alertDialog.dismiss();
                        sendReportProfileDeactivateToServer(reason);
                    }
                } else {

                    Toast.makeText(aContext, "Please select an option",
                            Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    class ActionBarBlockImplementation implements
            OnActionBarMenutItemProfileDeactivateClickedInterface {

        @Override
        public void onProfileDeactivateClicked() {
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.profile_deactivate_layout,
                    (ViewGroup) findViewById(R.id.profile_deactivate));

            RadioGroup radioGroup = (RadioGroup) layout
                    .findViewById(R.id.profile_deactivate_radio_groups);
            EditText other_comments = (EditText) layout
                    .findViewById(R.id.other_reason);
            radioGroup
                    .setOnCheckedChangeListener(new ReportAbuseOnCheckChangeListenerImplementation(
                            other_comments));
            AlertDialog.Builder builder = new AlertDialog.Builder(aContext)
                    .setView(layout);
            builder.setPositiveButton(R.string.deactivate,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    }).setNegativeButton(R.string.cancel,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });

            AlertDialog alertDialog = builder.create();
            alertDialog.show();
            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
                    .setOnClickListener(
                            new ReportAbuseButtonOnClickListner(radioGroup,
                                    alertDialog, layout));
        }
    }
}
