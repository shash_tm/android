package com.trulymadly.android.app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.crashlytics.android.Crashlytics;
import com.facebook.appevents.AppEventsConstants;
import com.moe.pushlibrary.MoEHelper;
import com.squareup.otto.Subscribe;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.asynctasks.TrackEventToFbAsyncTask;
import com.trulymadly.android.app.bus.BackPressedRegistrationFlow;
import com.trulymadly.android.app.bus.BusProvider;
import com.trulymadly.android.app.bus.ExpandFragmentEvent;
import com.trulymadly.android.app.bus.NextRegistrationFragment;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.listener.CustomOkHttpResponseHandler;
import com.trulymadly.android.app.listener.GetStartTimeInterface;
import com.trulymadly.android.app.modal.UserData;
import com.trulymadly.android.app.utility.ActivityHandler;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.ReferralHandler;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONObject;

public class Registration extends AppCompatActivity implements OnClickListener,
        GetStartTimeInterface {

    View parentView;
    private MoEHelper mHelper = null;
    private CURRENT_FRAGMENT selectedFragment;
    private Button nextButton;
    private Activity aActivity;
    private FragmentManager fragmentManager;
    private MaterialDesignEditHeight localEditHeightFragment = null;
    private MaterialDesignEditHashtags localEditHashtagsFragment = null;
    private View progress_bar_circles;
    private View circle_2, circle_3, circle_4, circle_5;
    private UserData userData;
    private long startTime;
    private boolean isCitySet = false;
    private boolean isFbConnected = false;
    private ProgressDialog mProgressDialog;
    private String fbDesignation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_flow);
        aActivity = this;
        Bundle b = getIntent().getExtras();
        userData = new UserData();
        if (b != null) {
            isCitySet = b.getBoolean("isCitySet", false);
            isFbConnected = b.getBoolean("isFbConnected", false);
            fbDesignation = b.getString("fbDesignation");
        }
        if (Utility.isSet(fbDesignation)) {
            userData.setFbDesignation(fbDesignation);
            userData.setCurrentTitle(fbDesignation);
        }
        mHelper = new MoEHelper(this);
        nextButton = (Button) findViewById(R.id.next_button);
        fragmentManager = getSupportFragmentManager();
        nextButton.setOnClickListener(this);
        progress_bar_circles = findViewById(R.id.progress_bar_males);
        circle_2 = findViewById(R.id.material_design_header_progress_bar_circle_full_2);
        circle_3 = findViewById(R.id.material_design_header_progress_bar_circle_full_3);
        circle_4 = findViewById(R.id.material_design_header_progress_bar_circle_full_4);
        circle_5 = findViewById(R.id.material_design_header_progress_bar_circle_full_5);

        View circle_3_container = findViewById(R.id.material_design_header_progress_bar_circle_full_3_layout);
        if (Utility.isSet(fbDesignation)) {
            circle_3_container.setVisibility(View.GONE);
        }
        View circle_5_container = findViewById(R.id.material_design_header_progress_bar_circle_full_5_layout);
        if (isFbConnected) {
            circle_5_container.setVisibility(View.GONE);
        }
        Bundle params = new Bundle();
        params.putString(AppEventsConstants.EVENT_PARAM_REGISTRATION_METHOD, TrulyMadlyEventTypes.register_start);
        TrackEventToFbAsyncTask.trackEvent(aActivity, AppEventsConstants.EVENT_NAME_COMPLETED_REGISTRATION, params);
        showEditHeightFragment();

    }

    @Override
    public void onBackPressed() {
        if (selectedFragment == CURRENT_FRAGMENT.edit_height) {
            if (localEditHeightFragment.isCityViewExpanded()) {
                localEditHeightFragment.onBackPressed();
            } else {
                Utility.minimizeApp(this);
            }
        } else if (selectedFragment == CURRENT_FRAGMENT.edit_education) {
            showEditHeightFragment();
        } else if (selectedFragment == CURRENT_FRAGMENT.edit_occupation) {
            showEditEducationFragment();
        } else if (selectedFragment == CURRENT_FRAGMENT.edit_hashtags) {
            try {
                boolean flag = localEditHashtagsFragment.isHelpTrayVisible();
                if (flag) {
                    localEditHashtagsFragment.hideHelpTray();
                } else {
                    if (!Utility.isSet(fbDesignation)) {
                        showEditOccupationFragment();
                    } else {
                        showEditEducationFragment();
                    }
                }
            } catch (ClassCastException e) {
                e.printStackTrace();
            }
        }
    }

    private void showEditHeightFragment() {
        try {
            setNextButtonEnabled(false);
            localEditHeightFragment = new MaterialDesignEditHeight();
            Bundle data = new Bundle();
            data.putSerializable("userData", userData);
            data.putBoolean("isCitySet", isCitySet);
            localEditHeightFragment.setArguments(data);
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.card_view_fragment_container,
                            localEditHeightFragment,
                            MaterialDesignEditHeight.class.getSimpleName())
                    .commitAllowingStateLoss();
            selectedFragment = CURRENT_FRAGMENT.edit_height;
            circle_2.setVisibility(View.GONE);
            circle_3.setVisibility(View.GONE);
            circle_4.setVisibility(View.GONE);
        } catch (IllegalStateException ignored) {
        }
    }


    private void showEditEducationFragment() {
        try {
            setNextButtonEnabled(false);
            MaterialDesignEditEducation localEditEducationFragment = new MaterialDesignEditEducation();
            Bundle data = new Bundle();
            data.putSerializable("userData", userData);
            localEditEducationFragment.setArguments(data);
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.card_view_fragment_container,
                            localEditEducationFragment,
                            MaterialDesignEditEducation.class.getSimpleName())
                    .commitAllowingStateLoss();
            circle_2.setVisibility(View.VISIBLE);
            circle_3.setVisibility(View.GONE);
            circle_4.setVisibility(View.GONE);
            selectedFragment = CURRENT_FRAGMENT.edit_education;

        } catch (IllegalStateException ignored) {
        }
    }

    private void showEditOccupationFragment() {
        try {
            setNextButtonEnabled(false);
            MaterialDesignEditOccupation localEditOccupationFragment = new MaterialDesignEditOccupation();
            Bundle data = new Bundle();
            data.putSerializable("userData", userData);
            localEditOccupationFragment.setArguments(data);
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.card_view_fragment_container,
                            localEditOccupationFragment,
                            MaterialDesignEditOccupation.class.getSimpleName())
                    .commitAllowingStateLoss();
            selectedFragment = CURRENT_FRAGMENT.edit_occupation;
            circle_3.setVisibility(View.VISIBLE);
            circle_4.setVisibility(View.GONE);
        } catch (IllegalStateException ignored) {
        }
    }

    private void showEditHashtagsFragment() {
        try {
            setNextButtonEnabled(false);
            nextButton.setVisibility(View.VISIBLE);
            localEditHashtagsFragment = new MaterialDesignEditHashtags();
            Bundle data = new Bundle();
            data.putSerializable("userData", userData);
            localEditHashtagsFragment.setArguments(data);
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.card_view_fragment_container,
                            localEditHashtagsFragment,
                            MaterialDesignEditHashtags.class.getSimpleName())
                    .commitAllowingStateLoss();
            selectedFragment = CURRENT_FRAGMENT.edit_hashtags;
            circle_4.setVisibility(View.VISIBLE);
            circle_5.setVisibility(View.GONE);

        } catch (IllegalStateException ignored) {
        }
    }

    private void setNextButtonEnabled(boolean isEnabled) {
        nextButton.setEnabled(isEnabled);
    }

    @Subscribe
    public void backPressed(BackPressedRegistrationFlow backPressedRegistrationFlow) {
        userData = backPressedRegistrationFlow.getUserData();
        String hashtag = backPressedRegistrationFlow.getHashtag();
        onBackPressed();
    }

    @Subscribe
    public void showNextFragment(NextRegistrationFragment nextFragment) {
        userData = nextFragment.getUserData();
        setNextButtonEnabled(nextFragment.getIsValid());
        if (Utility.isSet(nextFragment.getNextButtonText())) {
            nextButton.setText(nextFragment.getNextButtonText());
        } else {
            nextButton.setText(getResources().getString(R.string.next));
        }
    }

    @Subscribe
    public void expandFragment(ExpandFragmentEvent expandFragmentEvent) {
        if (expandFragmentEvent.isDoExpand()) {
            progress_bar_circles.setVisibility(View.GONE);
            nextButton.setVisibility(View.GONE);
        } else {
            progress_bar_circles.setVisibility(View.VISIBLE);
            nextButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        BusProvider.getInstance().register(this);

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        ReferralHandler.checkAndLoadUrl(this, ReferralHandler.ACTION_POSITION.on_registration_start);
    }

    @Override
    public void onStop() {
        super.onStop();
        BusProvider.getInstance().unregister(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.next_button:
                if (selectedFragment == CURRENT_FRAGMENT.edit_height) {
                    trackfunction(TrulyMadlyActivities.register_basics,
                            TrulyMadlyEventTypes.regiter_basics);
                    Bundle params = new Bundle();
                    params.putString(AppEventsConstants.EVENT_PARAM_REGISTRATION_METHOD, TrulyMadlyEventTypes.regiter_basics);
                    TrackEventToFbAsyncTask.trackEvent(aActivity, AppEventsConstants.EVENT_NAME_COMPLETED_REGISTRATION, params);
                    showEditEducationFragment();
                } else if (selectedFragment == CURRENT_FRAGMENT.edit_education) {
                    trackfunction(TrulyMadlyActivities.register_basics,
                            TrulyMadlyEventTypes.register_education);
                    Bundle params = new Bundle();
                    params.putString(AppEventsConstants.EVENT_PARAM_REGISTRATION_METHOD, TrulyMadlyEventTypes.register_education);
                    TrackEventToFbAsyncTask.trackEvent(aActivity, AppEventsConstants.EVENT_NAME_COMPLETED_REGISTRATION, params);
                    if (!Utility.isSet(fbDesignation)) {
                        showEditOccupationFragment();
                    } else {
                        showEditHashtagsFragment();
                    }
                } else if (selectedFragment == CURRENT_FRAGMENT.edit_occupation) {
                    trackfunction(TrulyMadlyActivities.register_basics,
                            TrulyMadlyEventTypes.register_work);
                    Bundle params = new Bundle();
                    params.putString(AppEventsConstants.EVENT_PARAM_REGISTRATION_METHOD, TrulyMadlyEventTypes.register_work);
                    TrackEventToFbAsyncTask.trackEvent(aActivity, AppEventsConstants.EVENT_NAME_COMPLETED_REGISTRATION, params);
                    showEditHashtagsFragment();
                } else if (selectedFragment == CURRENT_FRAGMENT.edit_hashtags) {
                    mProgressDialog = UiUtils.showProgressBar(aActivity, mProgressDialog, false);
                    trackfunction(TrulyMadlyActivities.register_basics,
                            TrulyMadlyEventTypes.register_education);
                    Bundle params = new Bundle();
                    params.putString(AppEventsConstants.EVENT_PARAM_REGISTRATION_METHOD, TrulyMadlyEventTypes.register_hashtag);
                    TrackEventToFbAsyncTask.trackEvent(aActivity, AppEventsConstants.EVENT_NAME_COMPLETED_REGISTRATION, params);
                    CustomOkHttpResponseHandler responseHandler = new CustomOkHttpResponseHandler(aActivity, TrulyMadlyActivities.register_basics,
                            TrulyMadlyEventTypes.register_server_save_call) {

                        @Override
                        public void onRequestSuccess(JSONObject responseJSON) {
                            Bundle params = new Bundle();
                            params.putString(AppEventsConstants.EVENT_PARAM_REGISTRATION_METHOD, TrulyMadlyEventTypes.register_server_save_call);
                            TrackEventToFbAsyncTask.trackEvent(aActivity, AppEventsConstants.EVENT_NAME_COMPLETED_REGISTRATION, params);

                            UiUtils.hideProgressBar(mProgressDialog);
                            if (isFbConnected) {
                                ActivityHandler.startProfileActivity(aActivity,
                                        ConstantsUrls.get_my_profile_url(), false, true, true);
                            } else {
                                ActivityHandler.startTrustBuilder(aActivity, false, true);

                            }

                            ReferralHandler.checkAndLoadUrl(Registration.this, ReferralHandler.ACTION_POSITION.on_registration_end);

                            finish();

                        }

                        @Override
                        public void onRequestFailure(Exception exception) {
                            UiUtils.hideProgressBar(mProgressDialog);
                            AlertsHandler.showNetworkError(aActivity, exception);


                        }
                    };
                    try {
                        HttpTasks.saveBasicDataOnServer(userData, aActivity,
                                responseHandler);
                    } catch (Exception e) {
                        Crashlytics.logException(e);
                    }
                }

        }

    }

    private void trackfunction(String Activity, String EventType) {
        long endTime = (new java.util.Date()).getTime();
        int time_diff = (int) ((endTime - startTime));
        TrulyMadlyTrackEvent.trackEvent(aActivity, Activity, EventType,
                time_diff, null, null, true);

    }

    @Override
    public void setStartTime(long time) {
        startTime = time;

    }

    private enum CURRENT_FRAGMENT {
        edit_height, edit_occupation, edit_education, edit_hashtags, none
    }

}
