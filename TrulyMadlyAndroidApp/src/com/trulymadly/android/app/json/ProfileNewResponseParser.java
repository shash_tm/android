package com.trulymadly.android.app.json;

import android.content.Context;

import com.trulymadly.android.app.LoginActivity;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.TrulyMadlyApplication;
import com.trulymadly.android.app.modal.EducationModal;
import com.trulymadly.android.app.modal.FavoriteDataModal;
import com.trulymadly.android.app.modal.FbFriendModal;
import com.trulymadly.android.app.modal.MatchesDetailModal;
import com.trulymadly.android.app.modal.MatchesLatestModal2;
import com.trulymadly.android.app.modal.MatchesSysMesgModal;
import com.trulymadly.android.app.modal.MutualConnection;
import com.trulymadly.android.app.modal.MyDetailModal;
import com.trulymadly.android.app.modal.MySelectData;
import com.trulymadly.android.app.modal.ProfileNewModal;
import com.trulymadly.android.app.modal.ShareProfileDataModal;
import com.trulymadly.android.app.modal.SystemFlags;
import com.trulymadly.android.app.modal.TrustBuilderData;
import com.trulymadly.android.app.modal.TrustBuilderModal;
import com.trulymadly.android.app.modal.UserModal;
import com.trulymadly.android.app.modal.VideoModal;
import com.trulymadly.android.app.modal.WorkModal;
import com.trulymadly.android.app.sqlite.MatchesDbHandler;
import com.trulymadly.android.app.utility.FavoriteUtils;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.SparksHandler;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

public class ProfileNewResponseParser {


    private boolean dbInsertionDisabled;

    public static ArrayList<FbFriendModal> parseFBList(JSONObject response, boolean isMutualMatch) {
        ArrayList<FbFriendModal> friendList = new ArrayList<>();
        JSONArray data = null;
        if (isMutualMatch) {

            JSONArray arr;
            JSONObject json, result;
            arr = response.optJSONArray("data");
            if (arr != null && arr.length() > 0) {
                json = arr.optJSONObject(0);
                if (json != null) {
                    result = json.optJSONObject("profile_data");
                    if (result != null)
                        data = result.optJSONArray("fb_mf");
                }
            }


        } else {
            data = response.optJSONArray("data");
        }

        if (data != null && data.length() > 0) {
            for (int i = 0; i < data.length(); i++) {
                JSONObject friendJson = null;
                friendJson = data.optJSONObject(i);
                FbFriendModal friendModal = new FbFriendModal();
                friendModal.setName(friendJson.optString("name"));
                friendModal.setThumbnail(friendJson.optString("url"));
                friendList.add(friendModal);


            }
        }
        return friendList;
    }


    //@Trace(category = MetricCategory.JSON)
    public MatchesLatestModal2 parseProfileNewResponse(JSONObject jsonProfileNewResponse, Context aContext,
                                                       boolean parseSystemFlags, boolean saveSparkCounter,
                                                       boolean saveMySelectData) {
        return processProfileResponse(jsonProfileNewResponse, aContext, parseSystemFlags,
                saveSparkCounter, saveMySelectData);
    }

    //@Trace(category = MetricCategory.JSON)
    private MatchesLatestModal2 processProfileResponse(JSONObject jsonProfileNewResponse, Context aContext,
                                                       boolean parseSystemFlags, boolean saveSparkCounter,
                                                       boolean saveMySelectData) {
        ProfileResponseConfig profileResponseConfig = parseProfileResponse(jsonProfileNewResponse,
                aContext, parseSystemFlags, saveSparkCounter, saveMySelectData);

        if (parseSystemFlags) {
            SystemFlags systemFlags = profileResponseConfig.getmSystemFlags();
            if (systemFlags != null) {
                TrulyMadlyApplication.enableSocket(aContext,
                        systemFlags.isSocketEnabled());
//                TrulyMadlyApplication.enableSocketDebug(aContext,
//                        systemFlags.isSocketDebugEnabled());
            } else {
                TrulyMadlyApplication.enableSocketDebug(aContext, false);
                TrulyMadlyApplication.enableSocket(aContext, false);
            }
        }

        MyDetailModal myDetailModal = null;
        if (profileResponseConfig.getmMatchesLatestModal2() != null)
            myDetailModal = profileResponseConfig.getmMatchesLatestModal2().getMyDetailModal();

        if (myDetailModal != null) {
            SPHandler.setString(aContext, "matches_favourite_filled",
                    myDetailModal.getmFavoriteFilled());
            SPHandler.setString(aContext, "matches_interpersonal_filled",
                    myDetailModal.getmInterpersonalFilled());
            SPHandler.setString(aContext, "matches_values_filled",
                    myDetailModal.getmValuesFilled());
        }

        return profileResponseConfig.getmMatchesLatestModal2();
    }

    //@Trace(category = MetricCategory.JSON)
    public ProfileResponseConfig parseProfileResponse(JSONObject jsonProfileNewResponse, Context aContext,
                                                      boolean parseSystemFlags, boolean saveSparkCounter,
                                                      boolean saveMySelectData) {
        ProfileResponseConfig profileResponseConfig = null;
        if (jsonProfileNewResponse != null) {
            profileResponseConfig = new ProfileResponseConfig();
            MatchesLatestModal2 aMatchesLatestModal2 = new MatchesLatestModal2();
            Vector<ProfileNewModal> profileNewModalList = null;

            MatchesSysMesgModal aMatchesSysMesgModal = null;
            MyDetailModal aMyDetailModal = null;
            SystemFlags systemFlags = null;

            String nonMutualMsg = "";

            if (jsonProfileNewResponse.optString("responseCode", "0").equals("200")) {

                if (parseSystemFlags) {
                    JSONObject sysFlagsJsonObj = jsonProfileNewResponse.optJSONObject("system_flags");
                    systemFlags = SystemFlagsParser.parseSystemFlags(sysFlagsJsonObj);
                }

                JSONObject abString = jsonProfileNewResponse.optJSONObject("abString");
                if (abString != null) {
                    JSONObject aestheticsAB = abString.optJSONObject("AestheticsAB");
                    if (aestheticsAB != null) {
                        JSONArray aesthetics_Nudge = aestheticsAB.optJSONArray("Aesthetics_Nudge");
                        if (aesthetics_Nudge != null && aesthetics_Nudge.length() >= 2) {
                            JSONArray positionsArray = aesthetics_Nudge.optJSONArray(0);
                            JSONArray messagesArray = aesthetics_Nudge.optJSONArray(1);

                            ArrayList<Integer> positionList = new ArrayList<>();
                            ArrayList<String> msgList = new ArrayList<>();
                            if (positionsArray != null) {
                                for (int i = 0; i < positionsArray.length(); i++) {
                                    positionList.add(positionsArray.optInt(i));
                                    msgList.add(messagesArray.optString(i));
                                }
                                aMatchesLatestModal2.setmFemaleNudgePositionsList(positionList);
                                aMatchesLatestModal2.setmFemaleNudgeMessagesList(msgList);
                            }

                        }
                    }
                }

                //Parsing system messages
                JSONObject sysMsgJsonObj = jsonProfileNewResponse.optJSONObject("system_messages");
                aMatchesSysMesgModal = parseSystemMessages(sysMsgJsonObj, aContext);
                if (aMatchesSysMesgModal != null) {
                    nonMutualMsg = aMatchesSysMesgModal.getNonMutualLikeMsg();
                    aMatchesSysMesgModal.setValidProfilePic(jsonProfileNewResponse.optBoolean("validProfilePic", true));
                    aMatchesSysMesgModal.setNoPhotos(jsonProfileNewResponse.optBoolean("noPhotos"));
                    aMatchesSysMesgModal.setLikedInThePast(jsonProfileNewResponse.optBoolean("likedInThePast"));
                }
                aMatchesLatestModal2.setMatchesSysMesgModal(aMatchesSysMesgModal);

                //Parse my profile data
                JSONObject jsonMyDataObj = jsonProfileNewResponse.optJSONObject("my_data");
                aMyDetailModal = parseMyData(jsonMyDataObj, aContext, saveMySelectData);
                if (aMyDetailModal != null) {
                    aMyDetailModal.setNonMutualLikeMsg(nonMutualMsg);
                    aMatchesLatestModal2.setMyDetailModal(aMyDetailModal);
                }

                //Parse matches' profiles
                JSONArray jsonDataArr = jsonProfileNewResponse.optJSONArray("data");
                profileNewModalList = parseProfileModals(jsonDataArr);
                aMatchesLatestModal2.setProfileNewModalList(profileNewModalList);

                if (!dbInsertionDisabled)
                    MatchesDbHandler.insertMatches(aContext, jsonProfileNewResponse);

                profileResponseConfig.setmMatchesLatestModal2(aMatchesLatestModal2);
                profileResponseConfig.setmSystemFlags(systemFlags);

                if (saveSparkCounter && jsonProfileNewResponse.has("sparkCountersLeft")) {
                    int counter = jsonProfileNewResponse.optInt("sparkCountersLeft", 0);
                    SparksHandler.insertSparks(aContext, counter);
                }
            }
        }

        return profileResponseConfig;
    }

    //@Trace(category = MetricCategory.JSON)
    private MatchesSysMesgModal parseSystemMessages(JSONObject sysMsgJsonObj, Context aContext) {

        MatchesSysMesgModal aMatchesSysMesgModal = null;
        if (sysMsgJsonObj != null) {
            aMatchesSysMesgModal = new MatchesSysMesgModal();
            aMatchesSysMesgModal.setFirstTileMsg(sysMsgJsonObj.optString("first_tile"));
            aMatchesSysMesgModal.setFirstTileLink(sysMsgJsonObj.optString("first_tile_link"));
            aMatchesSysMesgModal.setLastTileMsg(sysMsgJsonObj.optString("last_tile_msg"));
            aMatchesSysMesgModal.setLastTileLink(sysMsgJsonObj.optString("last_tile_link"));
            aMatchesSysMesgModal.setLikeHideActionMsg(sysMsgJsonObj.optString("like_hide_action_msg"));
            aMatchesSysMesgModal.setNonMutualLikeMsg(sysMsgJsonObj.optString("non_mutual_like_msg"));
            aMatchesSysMesgModal
                    .setShowNonAuthenticSurvey(sysMsgJsonObj.optBoolean("show_nonAuthentic_survey", false));
            aMatchesSysMesgModal
                    .setNonAuthenticSurveyTileCount(sysMsgJsonObj.optInt("tile_count_for_survey", 0));
            aMatchesSysMesgModal.setPopularityToolTipText(sysMsgJsonObj.optString("popularity_tip",
                    aContext.getResources().getString(R.string.popularity_tip_default)));
        }
        return aMatchesSysMesgModal;
    }

//    private Vector<ProfileNewModal> parseProfileModalsFromCursor(Cursor cursor) {
//        Vector<ProfileNewModal> profileNewModalList = null;
//        if (cursor != null) {
//
//
//            while (cursor.moveToNext()) {
//
//
//                String jsonString = cursor.getString(cursor.getColumnIndex("details"));
//                try {
//                    JSONObject jsonDObj = new JSONObject(jsonString);
//                    profileNewModalList.add(parseProfileModal(jsonDObj));
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//        return profileNewModalList;
//    }

    //@Trace(category = MetricCategory.JSON)
    private MyDetailModal parseMyData(JSONObject jsonMyDataObj, Context aContext, boolean saveMySelectData) {
        MyDetailModal aMyDetailModal = null;
        if (jsonMyDataObj != null) {
            aMyDetailModal = new MyDetailModal();

            aMyDetailModal.setName(jsonMyDataObj.optString("fname"));
            aMyDetailModal.setCity(jsonMyDataObj.optString("city"));
            aMyDetailModal.setAge(jsonMyDataObj.optString("age"));
            aMyDetailModal.setProfile_pic_url(jsonMyDataObj.optString("profile_pic"));
            aMyDetailModal.setProfile_pic_full_url(jsonMyDataObj.optString("profile_pic_full"));
            aMyDetailModal.setStatus(jsonMyDataObj.optString("status"));
            LoginActivity.setUserId(jsonMyDataObj.optString("user_id"), aContext);

            aMyDetailModal.setmFavoriteFilled(jsonMyDataObj.optString("favourite_filled"));
            aMyDetailModal.setmInterpersonalFilled(jsonMyDataObj.optString("interpersonal_filled"));
            aMyDetailModal.setmValuesFilled(jsonMyDataObj.optString("values_filled"));
            aMyDetailModal.setConversation_count(jsonMyDataObj.optInt("conversation_count"));

            aMyDetailModal.setIsFromMatch(jsonMyDataObj.optBoolean("from_match"));
            JSONObject trustMeterObj = jsonMyDataObj.optJSONObject("trustmeter");
            if (trustMeterObj != null) {
                aMyDetailModal.setTrustMeterPercentFacebook(trustMeterObj.optInt("fb", 30));
                aMyDetailModal.setTrustMeterPercentPhotoId(trustMeterObj.optInt("photo_id", 30));
                aMyDetailModal.setTrustMeterPercentLinkedin(trustMeterObj.optInt("linkedin", 15));
                aMyDetailModal.setTrustMeterPercentPhoneNo(trustMeterObj.optInt("phone", 10));
                aMyDetailModal.setTrustMeterPercentEndorsement(trustMeterObj.optInt("endorsements", 30));
            }

            JSONObject mySelectData = jsonMyDataObj.optJSONObject("my_select");
            if (saveMySelectData && mySelectData != null) {
                new MySelectData(mySelectData).saveMySelectData(aContext);
            }
        }

        return aMyDetailModal;
    }

    //@Trace(category = MetricCategory.JSON)
    public Vector<ProfileNewModal> parseProfileModals(JSONArray jsonDataArr) {
        Vector<ProfileNewModal> profileNewModalList = null;
        if (jsonDataArr != null) {

            int len = jsonDataArr.length();


            profileNewModalList = new Vector<>();

            for (int i = 0; i < len; i++) {
                JSONObject jsonDObj = jsonDataArr.optJSONObject(i);
                ProfileNewModal profileNewModal = parseProfileModal(jsonDObj);
                if (profileNewModal != null) {
                    profileNewModalList.add(profileNewModal);
                }
            }

        }
        return profileNewModalList;
    }

    private void appendVideoThumbnails(UserModal user) {
        ArrayList<String> videoUrls = new ArrayList<>();
        if (user != null) {
            String[] otherPics = user.getOtherPics();
            ArrayList<String> pics = new ArrayList<>();
            if (otherPics != null && otherPics.length > 0) {
                for (String otherPic : otherPics) {
                    pics.add(otherPic);
                    videoUrls.add(null);
                }
            }

            ArrayList<VideoModal> videos = user.getVideoArray();
            if (videos != null && videos.size() > 0) {
                for (int i = 0; i < videos.size(); i++) {
                    pics.add(videos.get(i).getThumbnail());
                    videoUrls.add(videos.get(i).getUrl());
                }
            }

            user.setOtherPics(pics.toArray(new String[pics.size()]));
            user.setVideoUrls(videoUrls.toArray(new String[videoUrls.size()]));
        }
    }

    private void setVideos(UserModal user) {
        ArrayList<String> videoUrls = new ArrayList<>();
        if (user != null) {
            String[] otherPics = user.getOtherPics();
            ArrayList<String> pics = new ArrayList<>();
            if (otherPics != null && otherPics.length > 0) {
                for (String otherPic : otherPics) {
                    pics.add(otherPic);
                }
            }

            ArrayList<String> thumbnails = new ArrayList<>();
            ArrayList<VideoModal> videos = user.getVideoArray();
            if (videos != null && videos.size() > 0) {
                for (int i = 0; i < videos.size(); i++) {
                    thumbnails.add(videos.get(i).getThumbnail());
                    videoUrls.add(videos.get(i).getUrl());
                }
            }

            user.setOtherPics(pics.toArray(new String[pics.size()]));
            user.setThumbnailUrls(thumbnails.toArray(new String[thumbnails.size()]));
            user.setVideoUrls(videoUrls.toArray(new String[videoUrls.size()]));
        }
    }

    //@Trace(category = MetricCategory.JSON)
    private ProfileNewModal parseProfileModal(JSONObject jsonDObj) {
        ProfileNewModal aProfile = null;
        if (jsonDObj != null) {
            aProfile = new ProfileNewModal();

            UserModal aUser = new UserModal();
            WorkModal aWork = new WorkModal();
            EducationModal aEducation = new EducationModal();

            aProfile.setUserId(jsonDObj.optString("user_id"));
            JSONObject jsonDataObj = jsonDObj.optJSONObject("profile_data");


            aUser.setUserId(jsonDObj.optString("user_id"));
            aUser.setName(jsonDataObj.optString("fname"));
            aUser.setAge(jsonDataObj.optString("age"));
            aUser.setGender(jsonDataObj.optString("gender"));
            aUser.setLastActive(jsonDataObj.optString("last_login"));
            aUser.setHeight(jsonDataObj.optString("height_inch"));
            aUser.setMaritalStatus(jsonDataObj.optString("marital_status"));
            aUser.setHaveChildren(jsonDataObj.optString("haveChildren"));

            aUser.setIncomeStart(jsonDataObj.optString("income")); // now its income only
            aUser.setCity(jsonDataObj.optString("city"));
            aUser.setProfilePic(jsonDataObj.optString("profile_pic"));
            aUser.setCelebStatus(jsonDataObj.optBoolean("celeb_status"));
            aUser.setDummySet(jsonDataObj.optBoolean("isDummySet"));
            aUser.setBadgeUrl(jsonDataObj.optString("badge_url"));
            aUser.setmSparkHash(jsonDataObj.optString("spark_hash"));
            aUser.setHasLikedBefore(jsonDataObj.optBoolean("has_liked_before", false));
            aUser.setmCountryId(jsonDataObj.optInt("country_id", Constants.COUNTRY_ID_INDIA));
            aUser.setmCountryName(jsonDataObj.optString("country", Utility.getCountryNameFromId(aUser.getmCountryId())));

            // Adding picasso prefetch for caching profile pics.
            // not fetching images in this loop

							/*
                             * if (Utility.isSet(aUser.getProfilePic())) {
							 * Picasso.with(aContext)
							 * .load(aUser.getProfilePic()) .fetch(); }
							 */


            //video url parsing
            JSONArray videoUrlJsonArray = jsonDataObj.optJSONArray("video_profile");
            ArrayList<VideoModal> videoUrls = new ArrayList<>();
            if (videoUrlJsonArray != null && videoUrlJsonArray.length() > 0) {
                for (int i = 0; i < videoUrlJsonArray.length(); i++) {
                    try {
                        JSONObject videoJson = (JSONObject) videoUrlJsonArray.get(i);
                        VideoModal videoModal = new VideoModal();
                        videoModal.setThumbnail(videoJson.optString("thumbnail"));
                        videoModal.setUrl(videoJson.optString("EncodedURL"));
                        videoModal.setMuted(videoJson.optBoolean("mute"));
                        videoModal.setmDuration(videoJson.optInt("duration"));
                        videoModal.setmId(videoJson.optString("video_id"));
                        videoUrls.add(videoModal);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            aUser.setVideoArray(videoUrls);




            String otherPic = jsonDataObj.optString("other_pics");
            String profilePicUrl = aUser.getProfilePic();
            if (Utility.isSet(otherPic)) {
                otherPic = otherPic.replace("[", "");
                otherPic = otherPic.replace("]", "");
                otherPic = otherPic.replace("\\", "");
                otherPic = otherPic.replace("\"", "");

                String[] otherPicArr = null;
                if (Utility.isSet(otherPic)) {
                    otherPicArr = otherPic.split(",");
                }

                String otherPicWithProfilePic[] = null;
                if (otherPicArr != null && otherPicArr.length > 0) {

                    int count = otherPicArr.length;
                    if (Utility.isSet(profilePicUrl)) {
                        otherPicWithProfilePic = new String[count + 1];
                        otherPicWithProfilePic[0] = profilePicUrl;
                        System.arraycopy(otherPicArr, 0, otherPicWithProfilePic, 1, count);

                        aUser.setOtherPics(otherPicWithProfilePic);
                    } else
                        aUser.setOtherPics(otherPicArr);
                } else {
                    if (Utility.isSet(profilePicUrl)) {
                        otherPicWithProfilePic = new String[1];
                        otherPicWithProfilePic[0] = profilePicUrl;
                        aUser.setOtherPics(otherPicWithProfilePic);
                    }
                }
            } else {

                String profilePic = jsonDataObj.optString("profile_pic");
                if (Utility.isSet(profilePic)) {
                    String[] otherPicWithProfilePic = new String[1];
                    otherPicWithProfilePic[0] = profilePic;
                    aUser.setOtherPics(otherPicWithProfilePic);
                }
            }
//            appendVideoThumbnails(aUser);
            setVideos(aUser);
            aProfile.setUser(aUser);

							/* End of User data */


            // setting profile url
            aProfile.setProfileUrl(jsonDataObj.optString("profile_url"));

            //Setting isSponsoredFlag
            parseAndSetSponsoredProfileData(aProfile, jsonDataObj);

            //Setting sparks related data
            JSONArray sparkMessagesSuggestions = jsonDataObj.optJSONArray("commonalities_string");
            if (sparkMessagesSuggestions != null && sparkMessagesSuggestions.length() > 0) {
                ArrayList<String> sparkMessagesSuggestionsList = new ArrayList<>();
                for (int i = 0; i < sparkMessagesSuggestions.length(); i++) {
                    String message = sparkMessagesSuggestions.optString(i);
                    if (Utility.isSet(message)) {
                        sparkMessagesSuggestionsList.add(message);
                    }
                }

                aProfile.setmSparkMessageSuggestions(sparkMessagesSuggestionsList);
            }

            /* Start of Work data * */
            aWork.setWorkStatus(jsonDataObj.optString("work_status"));
            aWork.setDesignation(jsonDataObj.optString("designation"));
            aWork.setIndustry(jsonDataObj.optString("industry"));

            String company = jsonDataObj.optString("companies");
            if (Utility.isSet(company)) {
                company = company.replace("[", "");
                company = company.replace("]", "");
                company = company.replace("\"", "");

                aWork.setCompany(company);

                String companiesArr[] = company.split(",");
                if (companiesArr != null && companiesArr.length > 0)
                    aWork.setCompanies(companiesArr);
            }

            aProfile.setWork(aWork);

							/* Work Data End */

							/* Start of Education * */

            aEducation.setHighestEdu(jsonDataObj.optString("highest_degree"));

            JSONArray mutual_events = jsonDataObj.optJSONArray("mutual_events");
            if (mutual_events != null) {
                ArrayList<String> eventImages = new ArrayList<>();
                ArrayList<String> eventNames = new ArrayList<>();
                for (int i = 0; i < mutual_events.length(); i++) {
                    JSONObject eventJSON = null;
                    try {
                        eventJSON = (JSONObject) mutual_events.get(i);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (eventJSON != null) {
                        eventImages.add(eventJSON.optString("image"));
                        eventNames.add(eventJSON.optString("name"));
                    }
                }
                String[] attribute = new String[eventImages.size()];
                attribute = eventImages.toArray(attribute);
                aProfile.setEventsImages(attribute);
                String[] nameAttribute = new String[eventNames.size()];
                nameAttribute = eventNames.toArray(nameAttribute);
                aProfile.setEventNames(nameAttribute);


            }
            String intitutes = jsonDataObj.optString("institutes");
            if (Utility.isSet(intitutes)) {
                intitutes = intitutes.replace("[", "");
                intitutes = intitutes.replace("]", "");
                intitutes = intitutes.replace("\"", "");

                aEducation.setInstitue(intitutes);
                String intitutesArr[] = intitutes.split(",");

                if (intitutesArr != null && intitutesArr.length > 0)
                    aEducation.setInstitutes(intitutesArr);
            }
            aProfile.setEducation(aEducation);

							/* End of Education ** */

            aProfile.setFBMutualConCount(jsonDataObj.optString("mutual_connections_count"));
            aProfile.setLocked(jsonDataObj.optString("locked"));
            aProfile.setCanUnlock(jsonDataObj.optString("canUnlock"));
            aProfile.setLinkMsg(jsonDataObj.optString("msg_url"));

							/* for mutual connection */
            JSONArray mutualConJsonArr = jsonDataObj.optJSONArray("mutual_connections");
            if (mutualConJsonArr != null) {

                int l = mutualConJsonArr.length();
                MutualConnection[] mutualConnectionArr = new MutualConnection[l];
                for (int j = 0; j < l; j++) {
                    JSONObject mutualConJsonObj = mutualConJsonArr.optJSONObject(j);
                    MutualConnection aMutualConnection = new MutualConnection();
                    aMutualConnection.setName(mutualConJsonObj.optString("name"));
                    aMutualConnection.setPic(mutualConJsonObj.optString("pic"));

                    mutualConnectionArr[j] = aMutualConnection;
                }
                aProfile.setMutualConnectionArr(mutualConnectionArr);
            }

							/* Start of interest and hobbies* */

            aProfile.setPhotoVisibilityToast(jsonDataObj.optBoolean("photo_visibility_toast"));
            String interestAndhobbies = jsonDataObj.optString("interest");

            if (Utility.isSet(interestAndhobbies)) {

                interestAndhobbies = interestAndhobbies.replace("[", "");
                interestAndhobbies = interestAndhobbies.replace("]", "");
                interestAndhobbies = interestAndhobbies.replace("\"", "");

                aProfile.setRigidInterest(interestAndhobbies);

                String interestHobbiesArr[] = interestAndhobbies.split(",");
                if (interestHobbiesArr.length > 0) {
                    aProfile.setInterest(interestHobbiesArr);
                }
            }
                            /* End of interest and hobbies */

							/*
                             * Get Favorite movies , music , books , food ,
							 * misc
							 */
            JSONObject interestHobbies = jsonDataObj.optJSONObject("InterestsHobbies");
            if (interestHobbies != null) {
                HashMap<String, ArrayList<FavoriteDataModal>> favoritesDataMap = FavoriteUtils.parseFavorites(interestHobbies, false, false, false);
                aProfile.setFavoritesDataMap(favoritesDataMap);
            }


            /*
             * End of Favorite movies , music , books , food ,
             * misc
             */

            /* Start of common interest */
            JSONArray commonHobbies = jsonDataObj.optJSONArray("common_likes");
            if (commonHobbies != null && commonHobbies.length() > 0) {
                String[] commonHobbiesArr = new String[commonHobbies.length()];
                for (int j = 0; j < commonHobbies.length(); j++) {
                    commonHobbiesArr[j] = commonHobbies.optString(j);
                }
                if (commonHobbiesArr.length > 0) {
                    aProfile.setCommonInterestModal(commonHobbiesArr);
                }
            }
                            /* End of common interest */

							/* Start of match detail */
            JSONObject jsonMatchDetailObj = jsonDataObj.optJSONObject("match_details");
            if (jsonMatchDetailObj != null) {
                MatchesDetailModal aMatchesDetail = new MatchesDetailModal();
                aMatchesDetail.setValuesData(jsonMatchDetailObj.optString("values"));
                aMatchesDetail.setAdaptabilityData(jsonMatchDetailObj.optString("adaptability"));
                aMatchesDetail.setBeliefsData(jsonMatchDetailObj.optString("beliefs"));

                aProfile.setMatchesDetail(aMatchesDetail);
            }

							/* End of match detail */

							/* Start of share profile details */
            JSONObject shareProfileDetails = jsonDataObj.optJSONObject("share_profile");
            ShareProfileDataModal shareProfileData = null;
            if (shareProfileDetails != null) {
                shareProfileData = new ShareProfileDataModal();
                shareProfileData.setShareLink(shareProfileDetails.optString("share_link"));
                shareProfileData.setShareMessage(shareProfileDetails.optString("share_message"));
                shareProfileData.setAlertMessage(shareProfileDetails.optString("alert_message"));
                shareProfileData
                        .setHideAlertMessage(shareProfileDetails.optString("hide_alert_message"));
            }
            aProfile.setShareProfileData(shareProfileData);
							/* End of share profile details */

							/* Start of trust score */
            JSONObject jsonTrustMeterObj = jsonDataObj.optJSONObject("trustMeter");
            TrustBuilderModal aTrustMeter = parseTrustBuilderModal(jsonTrustMeterObj,
                    jsonDataObj.optBoolean("is_endorsement_verified", false),
                    jsonDataObj.optJSONArray("endorsementData"));
            if (aTrustMeter != null) {
                aProfile.setTrustBuilder(aTrustMeter);
            }
							/* End of trust score */

            /* Start of activity Dashboard */
            JSONObject activityData = jsonDataObj.optJSONObject("activityData");
            if (activityData != null) {
                aProfile.setActivityDashBoardAvailable(true);
                aProfile.setActivityDashBoardProfileViews(activityData.optString("activity"));
                aProfile.setActivityDashBoardProfileViewsText(activityData.optString("activity_text"));
                aProfile.setActivityDashBoardPopularity(activityData.optInt("popularity"));
                aProfile.setActivityDashBoardPopularityTip(activityData.optString("popularity_text"));
                aProfile.setActivityDashBoardClickAction(activityData.optString("click_action"));
            } else {
                aProfile.setActivityDashBoardAvailable(false);
            }
            /* End of activity Dashboard */

            if (!jsonDataObj.isNull("like_url")) {
                aProfile.setLinkLike(jsonDataObj.optString("like_url"));
            }
            if (!jsonDataObj.isNull("hide_url")) {
                aProfile.setLinkHide(jsonDataObj.optString("hide_url"));
            }
            aProfile.setFbMutualFriendCount(jsonDataObj.optInt("fb_mf_count"));


            /* Start of TM Select Data */
            JSONObject selectDetails = jsonDataObj.optJSONObject("select");
            if (selectDetails != null) {
                aProfile.setTMSelectMember(selectDetails.optBoolean("is_tm_select"));
                aProfile.setmSelectCommonString(selectDetails.optString("common_string"));
                aProfile.setmSelectCommonImage(selectDetails.optString("common_image"));
                aProfile.setmSelectQuote(selectDetails.optString("quote"));
            }

        }
        return aProfile;
    }

    //@Trace(category = MetricCategory.JSON)
    private void parseAndSetSponsoredProfileData(ProfileNewModal profileNewModal, JSONObject jsonObject) {
        profileNewModal.setIsSponsoredProfile(jsonObject.optBoolean("isProfileAdCampaign", false));
        profileNewModal.setIsSponsoredEventProfile(jsonObject.optBoolean("isProfileAdCampaignForScenes", false));
        //Setting Dummy Data
//		profileNewModal.setIsSponsoredProfile(true);
//		String urlGoogle = "http://www.google.com";
//		profileNewModal.setmLandingUrl(urlGoogle);
//		ArrayList<String> impressionLinks = new ArrayList<>();
//		impressionLinks.add(urlGoogle);
//		ArrayList<String> clickUrtls = new ArrayList<>();
//		clickUrtls.add(urlGoogle);
//		profileNewModal.setmImpressionsTrackingUrls(impressionLinks);
//		profileNewModal.setmClicksTrackingUrls(clickUrtls);
        //End
        if (profileNewModal.isSponsoredProfile()) {
            profileNewModal.setmSponsoredEventDetails(jsonObject.optString("sponsoredEventDetails"));
            JSONObject campaignLinks = jsonObject.optJSONObject("campaign_links");
            if (campaignLinks != null) {
                profileNewModal.setmLandingUrl(campaignLinks.optString("landing_url"));

                JSONArray impressionsUrls = campaignLinks.optJSONArray("impression_urls");
                if (impressionsUrls != null) {
                    ArrayList<String> impressionsUrlsList = new ArrayList<>();
                    for (int i = 0; i < impressionsUrls.length(); i++) {
                        impressionsUrlsList.add(impressionsUrls.optString(i));
                    }
                    profileNewModal.setmImpressionsTrackingUrls(impressionsUrlsList);
                }

                JSONArray clicksUrls = campaignLinks.optJSONArray("click_urls");
                if (clicksUrls != null) {
                    ArrayList<String> clicksUrlsList = new ArrayList<>();
                    for (int i = 0; i < clicksUrls.length(); i++) {
                        clicksUrlsList.add(clicksUrls.optString(i));
                    }
                    profileNewModal.setmClicksTrackingUrls(clicksUrlsList);
                }

            }
        }
    }

    //@Trace(category = MetricCategory.JSON)
    private TrustBuilderModal parseTrustBuilderModal(JSONObject jsonTrustMeterObj,
                                                     boolean isEndorsementVerified, JSONArray endorsementData) {
        TrustBuilderModal aTrustMeter = null;
        if (jsonTrustMeterObj != null) {
            aTrustMeter = new TrustBuilderModal();
            aTrustMeter.setFBConn(jsonTrustMeterObj.optString("fb_connections"));

            aTrustMeter.setLinkedInConn(jsonTrustMeterObj.optInt("linkedin_connections"));

//            aTrustMeter.setLinkedInConn(jsonTrustMeterObj.optString("linkedin_designation"));

            aTrustMeter.setPhnNum(jsonTrustMeterObj.optString("mobile_number"));
            // Address removed
            // aTrustMeter.setAddressProof(jsonTrustMeterObj.optString("address"));
            // aTrustMeter.setIDProof(jsonTrustMeterObj.optString("id_proof"));
            aTrustMeter.setIDProof(jsonTrustMeterObj.optString("id_proof_type"));
            // aTrustMeter.setEmployementProof(jsonTrustMeterObj.optString("employment"));
            aTrustMeter.setIsEndorsementVerified(isEndorsementVerified);
            aTrustMeter.setEndorsers(TrustBuilderData.parseEndorsers(endorsementData));
            aTrustMeter.setTrustScore(jsonTrustMeterObj.optInt("trust_score", 0));
        }
        return aTrustMeter;
    }

    public boolean isDbInsertionDisabled() {
        return dbInsertionDisabled;
    }

    public void setDbInsertionDisabled(boolean dbInsertionDisabled) {
        this.dbInsertionDisabled = dbInsertionDisabled;
    }


    public class ProfileResponseConfig {
        private MatchesLatestModal2 mMatchesLatestModal2;
        private SystemFlags mSystemFlags;

        public MatchesLatestModal2 getmMatchesLatestModal2() {
            return mMatchesLatestModal2;
        }

        public void setmMatchesLatestModal2(MatchesLatestModal2 mMatchesLatestModal2) {
            this.mMatchesLatestModal2 = mMatchesLatestModal2;
        }

        public SystemFlags getmSystemFlags() {
            return mSystemFlags;
        }

        public void setmSystemFlags(SystemFlags mSystemFlags) {
            this.mSystemFlags = mSystemFlags;
        }
    }
}
