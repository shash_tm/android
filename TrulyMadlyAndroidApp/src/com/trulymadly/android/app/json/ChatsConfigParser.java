package com.trulymadly.android.app.json;

import android.content.Context;

import com.trulymadly.android.app.TrulyMadlyApplication;
import com.trulymadly.android.app.json.ConstantsSocket.SOCKET_END;
import com.trulymadly.android.app.modal.ChatsConfig;
import com.trulymadly.android.app.sqlite.RFHandler;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONObject;

/**
 * Created by avin on 03/12/15.
 */
class ChatsConfigParser {
    public static ChatsConfig parseChatsConfig(JSONObject chatsConfigJO) {
        ChatsConfig parsedChatsConfig = null;
        if (chatsConfigJO != null) {
            parsedChatsConfig = new ChatsConfig();
            parsedChatsConfig.setIsPasscodeEnabled(chatsConfigJO.optBoolean("is_passcode_enabled", false));
            parsedChatsConfig.setmChatDomain(chatsConfigJO.optString("chat_domain",
                    ConstantsSocket.SOCKET_URL_DEFAULT));
            parsedChatsConfig.setmChatIpList(chatsConfigJO.optString("chat_ip_list",
                    ConstantsSocket.SOCKET_IP_LIST_DEFAULT));

            parsedChatsConfig.setIsWebsocketOnly(chatsConfigJO.optBoolean("websocket_only",
                    ConstantsSocket.SOCKET_TRANPORT_ONLY_WEBSOCKET_BOOL_DEFAULT));

            parsedChatsConfig.setmConnectionRetryAttempts(chatsConfigJO.optInt("connection_retry_attempts",
                    ConstantsSocket.MAX_RETRY_ATTEMPT_DEFAULT));
            parsedChatsConfig.setmChatSendTimeout(chatsConfigJO.optInt("chat_send_timeout",
                    ConstantsSocket.EMIT_THRESHOLD_TIMEOUT_CHAT_SENT_DEFAULT_SECONDS) * 1000);
            parsedChatsConfig.setmConnectionTimeout(chatsConfigJO.optInt("connection_timeout",
                    ConstantsSocket.SOCKET_CONNECTION_TIMEOUT_DEFAULT_SECONDS) * 1000);
            parsedChatsConfig.setMissedMessagesTimeout(chatsConfigJO.optInt("get_missed_messages_timeout",
                    ConstantsSocket.EMIT_THRESHOLD_TIMEOUT_GMM_DEFAULT_SECONDS) * 1000);
            parsedChatsConfig.setIsSocketDebugEnabled(chatsConfigJO.optBoolean("socket_debug_flag", false));
            parsedChatsConfig.setmABPrefix(chatsConfigJO.optString("ab_prefix", null));

        }

        return parsedChatsConfig;
    }

    public static void processChatsConfig(Context aContext, String userId, ChatsConfig chatsConfig) {

        boolean isAnyFlagChanged = false;
        if (chatsConfig != null) {
            if (chatsConfig.isPasscodeEnabled() != RFHandler.getBool(aContext,
                    userId, ConstantsRF.RIGID_FIELD_IS_PASSCODE_PROTECTION_ENABLED)) {
                SPHandler.setInt(aContext, ConstantsSP.SHARED_KEYS_SWIPE_LEFT_OVERLAY_SHOWN_COUNT,
                        0);
            }


            RFHandler.insert(aContext, userId,
                    ConstantsRF.RIGID_FIELD_IS_PASSCODE_PROTECTION_ENABLED, chatsConfig.isPasscodeEnabled());
            if (!chatsConfig.isPasscodeEnabled()) {
                RFHandler.insert(aContext, userId, ConstantsRF.RIGID_FIELD_CONVERSATIONS_HIDDEN, false);
                RFHandler.insert(aContext, userId, ConstantsRF.RIGID_FIELD_HIDE_CONVERSATIONS_PASSCODE, "");
            }

            if (!Utility.stringCompare(chatsConfig.getmChatDomain(),
                    SPHandler.getString(aContext,
                            ConstantsSP.SHARED_KEYS_SOCKET_URL)) && Utility.isSet(chatsConfig.getmChatDomain())) {
                SPHandler.setString(aContext, ConstantsSP.SHARED_KEYS_SOCKET_URL,
                        chatsConfig.getmChatDomain());
                isAnyFlagChanged = true;
            }

            if (!Utility.stringCompare(chatsConfig.getmChatIpList(),
                    SPHandler.getString(aContext, ConstantsSP.SHARED_KEYS_SOCKET_IP_LIST)) && Utility.isSet(chatsConfig.getmChatIpList())) {
                SPHandler.setString(aContext, ConstantsSP.SHARED_KEYS_SOCKET_IP_LIST, chatsConfig.getmChatIpList());
                isAnyFlagChanged = true;
            }

            if (chatsConfig.isWebsocketOnly() != SPHandler
                    .getBool(aContext, ConstantsSP.SHARED_KEYS_SOCKET_TRANPORT_ONLY_WEBSOCKET_BOOL)) {
                SPHandler.setBool(aContext, ConstantsSP.SHARED_KEYS_SOCKET_TRANPORT_ONLY_WEBSOCKET_BOOL,
                        chatsConfig.isWebsocketOnly());
                isAnyFlagChanged = true;
            }

            if (chatsConfig.getmConnectionRetryAttempts() != SPHandler
                    .getInt(aContext, ConstantsSP.SHARED_KEYS_MAX_RETRY_ATTEMPT)) {
                SPHandler.setInt(aContext, ConstantsSP.SHARED_KEYS_MAX_RETRY_ATTEMPT, chatsConfig.getmConnectionRetryAttempts());
                isAnyFlagChanged = true;
            }

            if (chatsConfig.getmChatSendTimeout() != SPHandler
                    .getInt(aContext, ConstantsSP.SHARED_KEYS_EMIT_THRESHOLD_TIMEOUT_CHAT_SENT)) {
                SPHandler.setInt(aContext, ConstantsSP.SHARED_KEYS_EMIT_THRESHOLD_TIMEOUT_CHAT_SENT,
                        chatsConfig.getmChatSendTimeout());
                isAnyFlagChanged = true;
            }

            if (chatsConfig.getmConnectionTimeout() != SPHandler
                    .getInt(aContext, ConstantsSP.SHARED_KEYS_SOCKET_CONNECTION_TIMEOUT)) {
                SPHandler.setInt(aContext,
                        ConstantsSP.SHARED_KEYS_SOCKET_CONNECTION_TIMEOUT, chatsConfig.getmConnectionTimeout());
                isAnyFlagChanged = true;
            }

            if (chatsConfig.getMissedMessagesTimeout() != SPHandler
                    .getInt(aContext, ConstantsSP.SHARED_KEYS_EMIT_THRESHOLD_TIMEOUT_GMM)) {
                SPHandler.setInt(aContext, ConstantsSP.SHARED_KEYS_EMIT_THRESHOLD_TIMEOUT_GMM,
                        chatsConfig.getMissedMessagesTimeout());
                isAnyFlagChanged = true;
            }

            TrulyMadlyApplication.enableSocketDebug(aContext,
                    chatsConfig.isSocketDebugEnabled());

            if (chatsConfig.getmABPrefix() != null) {
                SPHandler.setString(aContext, ConstantsSP.SHARED_KEYS_CHAT_AB_PREFIX,
                        chatsConfig.getmABPrefix());
            } else {
                SPHandler.remove(aContext, ConstantsSP.SHARED_KEYS_CHAT_AB_PREFIX);
            }

            if (isAnyFlagChanged) {
                TrulyMadlyApplication.forceRestartService(aContext, SOCKET_END.force_restart_user_flags_change);
            }
        }
    }
}
