package com.trulymadly.android.app.json;

import android.os.Environment;

import com.trulymadly.android.app.BuildConfig;
import com.trulymadly.android.app.utility.TimeUtils;

import java.util.Arrays;
import java.util.List;


public class Constants {

    public static final String RELEASE_STORE = BuildConfig.RELEASE_STORE;
    public static final Boolean isLive = BuildConfig.isLive;
    public static final Boolean isT1 = BuildConfig.isT1;
    public static final Boolean isDev = (!isLive && !isT1);
    public static final Boolean isHTTPS = BuildConfig.isHTTPS;

    public static final String FB_APP_ID_DEV = "637809759576128";
    public static final String FB_APP_ID_T1 = "637809759576128";
    public static final String FB_APP_ID_LIVE = "1442995922596983";
    public static final String LINKEDIN_CONSUMER_KEY_DEV = "75wjtgy2qolino";
    public static final String LINKEDIN_CONSUMER_KEY_T1 = "75wjtgy2qolino";
    public static final String LINKEDIN_CONSUMER_KEY_LIVE = "75dcgzg3oegb0g";
    public static final String SENDER_ID_DEV = "133388128041";
    public static final String SENDER_ID_T1 = "133388128041";
    public static final String SENDER_ID_LIVE = "133388128041";
    public static final String GA_ID_DEV = "UA-45604694-3";
    public static final String GA_ID_T1 = "UA-45604694-2";
    public static final String GA_ID_LIVE = "UA-45604694-4";
    public static final String APP_DYNAMICS_KEY = "AD-AAB-AAB-URX";


    public static final String APP_ID = (isLive ? FB_APP_ID_LIVE
            : (isT1 ? FB_APP_ID_T1 : FB_APP_ID_DEV));
    public static final String SENDER_ID = (isLive ? SENDER_ID_LIVE
            : (isT1 ? SENDER_ID_T1 : SENDER_ID_DEV));
    public static final String LINKEDIN_CONSUMER_KEY = (isLive ? LINKEDIN_CONSUMER_KEY_LIVE
            : (isT1 ? LINKEDIN_CONSUMER_KEY_T1 : LINKEDIN_CONSUMER_KEY_DEV));
    public static final String GA_API_ID = (isLive ? GA_ID_LIVE
            : (isT1 ? GA_ID_T1 : GA_ID_DEV));

    public static final String FACEBOOK_URL_ALBUMS = "https://graph.facebook.com/me/albums?access_token=[ACCESS_TOKEN]&format=json";
    public static final String FACEBOOK_URL_PHOTOS = "https://graph.facebook.com/[ALBUM_ID]/photos?access_token=[ACCESS_TOKEN]&format=json";
    public static final List<String> FACEBOOK_READ_PERMISSIONS = Arrays.asList(
            "email", "user_photos", "user_relationships", "user_actions.music",
            "user_actions.books", "user_actions.video", "user_friends",
            "user_birthday", "user_likes", "user_work_history",
            "user_education_history", "user_location");
    public static final String LINKEDIN_SCOPE = "r_basicprofile r_emailaddress";
    public static final String BAD_WORD_SHEETSU_API_URL = "https://sheetsu.com/apis/40d75ed2/column/bad_word_list";
    public static final String TM_PACKAGE_BASE = "com.trulymadly.android.app";
    public static final String disk_path = Environment
            .getExternalStorageDirectory().getAbsolutePath()
            + "/Android/data/com.trulymadly.android.app" + ((isT1) ? ".t1" : ((isDev) ? ".dev" : ""));
    public static final String IMAGES_ASSETS_FOLDER = "images_assets";
    public static final String VIDEO_ASSETS_FOLDER = "video_assets";
    public static final int AUTHENTICATE = 1;
    public static final int AUTH_FROM_FB = 111;
    public static final int REGISTER_VIA_FB = 120;
    public static final int REGISTER_VIA_EMAIL = 121;
    public static final int FETCH_DATA = 122;
    public static final int ALL_MATCHES = 2;
    public static final int MATCHES_Q_LIKE = 3;
    public static final int MATCHES_Q_FAVORITE = 4;
    public static final int MATCHES_Q_DECLINED = 5;
    public static final int PROFILE = 9;
    public static final int LIKE = 10;
    public static final int MATCHES_SORT_BY_ACTIVE_DATE = 11;
    public static final int MATCHES_SORT_BY_TRUST = 12;
    public static final int MATCHES_SORT_BY_AGE = 13;
    public static final int PUSH_NOTIFICATION_MESSAGE = 14;
    public static final int DASHBOARD_HEADER = 15;
    public static final int PHOTOS = 16;
    public static final int LOGIN_FB = 24;
    public static final int NEW_MESSAGE_ID = 25;
    public static final int MATCHES_LIKE_ME = 26;
    public static final int MATCHES_HIDE_ME = 27;
    public static final int EDIT_PREFERENCE_PREFILL = 28;
    public static final int EDIT_PREFERENCE_SAVE = 29;
    public static final int EDIT_PREFERENCE_PREFILL_BASIC_DATA = 31;
    public static final int MESG_CONV = 0;
    public static final int MESG_CONV_SENT = 1;
    public static final int MESG_CONV_NEW = 2;
    public static final int MESG_NEW_ALERT = 3;
    // for message compose
    public static final int MESG_BLOCK = 4;
    public static final int PROFILE_LINK = 0;
    public static final int PROFILE_BOTTOM_TAB_LINK = 1;
    public static final int MY_PROFILE_LINK = 2;
    public static final int PROFILE_MSG_NON_AUTH = 100;
    public static final int PROFILE_MSG_AUTH_LOCKED = 101;
    public static final int PROFILE_FB_MUTUAL_LINK = 102;
    public static final int GET_PHOTOS_FROM_ACTIVITY = 1002;
    public static final int GET_TRUST_FROM_ACTIVITY = 1003;
    public static final int FROM_FULL_ALBUM_PAGE = 1005;
    public static final int OPEN_WEB_LINK = 1006;
    public static final int INVITE_FRIENDS_ACTIVITY = 1007;
    //public static final int INVITE_FRIENDS_ACTIVITY=1007;
    public static final int CONNECT_LI = 1008;
    public static final int EDIT_PROFILE_FROM_PROFILE = 1009;
    public static final int SHARE_VIA_TEXT_INTENT = 1010;
    public static final int EDIT_FAVORITES_FROM_MATCHES = 1011;
    public static final int PICK_FROM_CAMERA_OWN = 1100;
    public static final int PICK_FROM_GALLERY = 1101;
    public static final int PICK_IMAGE_FROM_SYSTEM = 1102;
    public static final int PICK_FILE_FROM_SYSTEM = 1103;
    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static final int LIKE_OR_HIDE_OF_SINGLE_MATCH = 1200;
    public static final int LIKE_SINGLE_MATCH = 1201;
    public static final int HIDE_SINGLE_MATCH = 1202;
    public static final int REQUEST_CHECK_SETTINGS = 1203;
    public static final int FOLLOW_EVENT = 1204;
    public static final int EVENT_FOLLOWED = 1205;
    public static final int EVENT_UNFOLLOWED = 1206;
    public static final int MUTUAL_MATCH = 1207;
    public static final int SPARK_SINGLE_MATCH = 1208;
    public static final int PURCHASE_SPARKS_REQUEST_CODE = 1301;
    public static final int BUY_PACKAGES_REQUEST_CODE = 1302;
    public static final int TRIM_VIDEO_REQUEST_CODE = 1303;

    public static final String INMOBI_ACCOUNT_ID = "b8a601bbfa744436bd4b32e002c6d9a8";
    public static final long INMOBI_PLACEMENT_MATCH_ID = 1446951061310L;
    public static final long INMOBI_PLACEMENT_CONVERSATIONS_ITEM_ID = 1449072249597L;
    public static final long INMOBI_PLACEMENT_MATCHES_INTERSTITIAL_ID = 1431975915771248L;
    public static final long INMOBI_PLACEMENT_CONV_INTERSTITIAL_ID = 1431977725392002L;
    public static final String NO_OF_TIMES_UPLOAD_PIC_DIALOG_SHOWN = "NO_OF_UPLOAD_PIC_DIALOG_SHOWN";

    public static final int ADS_INTERSTITIAL_UPPER_CAP = 10;
    public static final int ADS_MATCHES_INTERSTITIAL_UPPER_CAP = 8;
    public static final int ADS_CONVERSATION_INTERSTITIAL_UPPER_CAP = 8;
    public static final long ADS_INTERSTITIAL_TIMEOUT = 10000;//1*60*60*1000; //1 hour
    public static final long ADS_NATIVE_TIMEOUT = 10000;//1*60*60*1000; //1 hour

    public static final String SOUND_OPTION = "SOUND_OPTION_PERSISTANT";
    public static final String VIBRATION_OPTION = "VIBRATION_OPTION_PERSISTANT";
    public static final String DISCOVERY_OPTION = "DISCOVERY_OPTION_PERSISTANT";
    public static final String SHOW_DISCOVERY_OPTION = "SHOW_DISCOVERY_PERSISTANT";


    public static final String LOCATION_OTHER = "999999";
    public static final long PI_TIMEOUT = 24*60*60*1000;
    public static final String PI_NUDGE_USER_PHOTOS_KEY = "from_pi_nudge";
    public static final int NEW_PASSCODE_TUTORIAL_MAX_COUNT = 2;
    public static final String EDIT_PREF_COUNTRY = "EDIT_PREF_COUNTRY";
    public static final String EDIT_PREF_STATE = "EDIT_PREF_STATE";
    public static final String EDIT_PREF_CITY = "EDIT_PREF_CITY";
    public static final String EDIT_PREF_DEGREE = "EDIT_PREF_DEGREE";
    public static final String EDIT_PREF_INCOME = "EDIT_PREF_INCOME";
    public static final String EDIT_PROFILE_HASTAG = "EDIT_PROFILE_HASTAG";
    public static final String EDIT_PROFILE_INDUSTRY = "EDIT_PROFILE_INDUSTRY";
    public static final String NUM_FRND_INVITED = "Number of friends invited";
    public static final String FIDS = "fids";
    public static final String GA_LABEL = "android_app";
    public static final String GOO_GL_API_KEY = "AIzaSyDMb_XVyxqcFDjINJddbViZYXTlS0i15hI";
    public static final long TIMEOUT_STICKER_API = 60 * 60 * 1000;
    public static final long TIMEOUT_QUIZ_API = 60 * 60 * 1000;
    public static final long CHAT_TYPING_THRESHOLD = 5 * 1000;
    public static final String phoneNoPattern = "[1-9]{1}\\d{9}";
    public static final String photos_share_action = "upload_image";


    public static final String gallery_suffix = "images/stickers/StickersGalleries";
    public static final String old_gallery_suffix = "images/stickers";
    public static final String gallery_url = ConstantsUrls.getCDNURL()
            + gallery_suffix;
    public static final String ping_url = "https://djy1s2eqovnmt.cloudfront.net/files/ping_new.json";
    public static final String crobo_postback_url = "http://tracking.crobo.com/aff_lsr";
    public static final String CD_KEY_DEAL_ID = "deal_id";
    public static final String CD_KEY_DATESPOT_ID = "datespot_id";
    public static final String CD_KEY_IMAGE_URI = "image_uri";
    public static final String CD_KEY_CLICKABLE = "cd_clickable";
    public static final String KEY_CD_MODAL = "curated_deals";
    public static final String KEY_EVENT_MODAL = "event";
    //Slots
    public static final int DEFAULT_SLOT_NUMBER = 1;
    public static final int DEFAULT_SLOT_SIZE = 15;
    //    public static final int MATCHES_PROFILE_AD_DEF_FINAL_POSIITON = 4;
    public static final String ADS_79_PUBLISHER_ID = "4494";
    public static final String ADS_79_PLACEMENT_ID_MATCHES_VIDEO_AD = "17964";
    public static final String ADS_79_PLACEMENT_ID_MATCHES_END_BANNER_AD = "18960";
//    public static final String ADS_79_PLACEMENT_ID_MATCHES_END_INTERSTITIAL_AD = "19484";
    public static final String ADS_79_PLACEMENT_ID_MATCHES_END_INTERSTITIAL_AD = "19954";
    public static final String ADS_79_PLACEMENT_ID_CONV_NATIVE_AD = "19488";
    //EventSendWorker Constants
    public static final int EMPTY_QUEUE_SLEEP_TIME = 5000;
    public static final int QUEUE_SLEEP_TIME = 2000;
    public static final String goo_gl_api_shortener_url = "https://www.googleapis.com/urlshortener/v1/url";
    public static final String default_share_short_url = "http://goo.gl/IKUk2r";
    // public static String default_share_long_url =
    // "https://120630.api-03.com/serve?action=click&publisher_id=120630&site_id=74758&destination_id=144860&my_publisher=android";
    // public static String default_share_long_url =
    // "https://play.google.com/store/apps/details?id=com.trulymadly.android.app&referrer=utm_source%3Dapp_share";
    public static final String default_share_long_url = "http://www.trulymadly.com/redirect.html?referrer=utm_source%3Dapp_share";
    public static final String packageLinkedIn = "com.linkedin.android";
    public static final String packageFacebookmessenger = "com.facebook.orca";
    public static final String packageFacebook = "com.facebook.katana";
    public static final String packageWhatsapp = "com.whatsapp";
    public static final String packageHangouts = "com.google.android.talk";
    public static final String packageGoogleMarket = "com.google.market";
    public static final String packageGooglePlay = "com.android.vending";
    public static final String packageInstagram = "com.instagram.android";
    public static final String instagramTmProfile = "thetrulymadly";
    public static final int STICKER_RECENT_GALLERY_ID = 0;
    public static final int PASSCODE_DELAY = 2 * 60 * 1000;
    public static final long TOKEN_UPDATE_INTERVAL = (TimeUtils.HOURS_24_IN_MILLIS);
    public static final int IMAGE_CACHE_CLEANUP_TIMEOUT = (int) (30 * TimeUtils.HOURS_24_IN_MILLIS);
    public static final String NULL_USER_ID = "000000";
    //Youtube
    public static final String YOUTUBE_PACKAGE_NAME = "com.google.android.youtube";
    public static final String YOUTUBE_API_KEY = "AIzaSyDVNALshvagU7Ih7sC9ByO7gj7E508qXUs";

//    public static final String ADS_79_PUBLISHER_ID = "4790";
//    public static final String ADS_79_PLACEMENT_ID_MATCHES_VIDEO_AD = "18964";
//    public static final String ADS_79_PLACEMENT_ID_MATCHES_END_BANNER_AD = "18960";

    public static final String KEY_SPARK_ACCEPT_MESSAGE = "spark_accepted_message";

    public static final int SPARK_TIMER_UPDATE_INTERVAL = 5;
    public static final int SPARK_RECEIVER_TUTORIAL_MAX_COUNT = 2;
    public static final int SPARK_RECEIVER_CONV_ANIM_MAX_COUNT = 2;
    public static final int SPARK_RECEIVER_MSG_ONEONONE_ANIM_MAX_COUNT = 2;

    public static final int MATCHES_COUNT_AFTER_WHICH_SPARK_NUDGE_SHOWN = 4;

    //Countries
    public static final int COUNTRY_ID_INDIA = 113;
    public static final int COUNTRY_ID_US = 254;
    public static final String COUNTRY_NAME_INDIA = "India";
    public static final String COUNTRY_NAME_US = "United States";

    public enum FetchSource {
        POLLING, GCM, SOCKET, LOCAL, CONVERSATION
    }

    public enum AlarmRequestStatus {
        EXPIRED, IN_PROGRESS, NOT_SET
    }

    public enum ActivityName {
        LOGIN, REGISTRATION_FLOW, EDIT_PROFILE, USER_PHOTOS, MATCHES, ONE_ONE_MESSAGE, TRUST_BUILDER,
        EDIT_PREFERENCES, MY_PROFILE, CONVERSATION, SETTINGS
    }

    public enum EditProfilePageType {
        AGE, PROFESSION, EDUCATION, INTERESTS
    }

    public enum PhotoUploadType {
        CAMERA, GALLERY, FACEBOOK
    }

    public enum LoginPageType {
        LOGIN, REGISTER
    }

    public enum MessageState {
        BLOCKED_UNSHOWN, BLOCKED_SHOWN, MATCH_ONLY_NEW, MATCH_ONLY, INCOMING_DELIVERED, INCOMING_READ,
        OUTGOING_SENDING, OUTGOING_SENT, OUTGOING_FAILED, OUTGOING_DELIVERED, OUTGOING_READ, GCM_FETCHED,
        CONVERSATION_FETCHED, CONVERSATION_FETCHED_NEW
    }

    public enum HttpRequestType {
        GET_USER_DATA, LOGIN_EMAIL, LOGIN_FB, REGISTER_EMAIL, FETCH_USER_DATA, LOGOUT, SAVE_DATA_BASIC,
        FB_TRUSTBUILDER, FB_PHOTO_SELECT, PHOTO_DELETE, PHOTO_PROFILE_SET, PROFILE, MATCHES_LIKE,
        MATCHES_HIDE, DISPLAY_PICS, UPLOAD_PIC_GALLERY, UPLOAD_PIC_CAMERA, EDIT_PREF, SAVE_DATA_PHOTO,
        SAVE_DATA_PSYCHO1, SAVE_DATA_PSYCHO2, SAVE_DATA_HOBBY, MATCH, EDIT_PREF_SAVE, TRUST_BUILDER,
        FETCH_PSYCHO_DATA, FETCH_PSYCHO1_DATA, FETCH_PSYCHO2_DATA, FETCH_HOBBY_DATA, MESSAGE_ONE_ONE_CONVERSATION,
        SEND_REG_ID, FETCH_USER_DATA_EDIT, SUSPEND_PROFILE, VERIFY_PHONE, POLL_PHONE, VERIFY_PHOTOID_PASSWORD,
        VERIFY_PHOTOID_MISMATCH_SYNC, VERIFY_EMPLOYMENT_PASSWORD, PROFILE_DEACTIVATE, ACTIVATE,
        EDIT_PREF_BASIC_DATA, CONTACT_US, GET_REG_IDS, PASSCODE,
        //Sparks Parameters
        SPARKS_GET, SPARKS_UPDATE, SPARKS_RESET, SPARKS_SENT, GET_PACKAGES, GET_COMMONALITIES_SPARK, GET_PAYTM_PURORDER,
        //Select Parameters
        GET_QUIZ, SUBMIT_QUIZ, COMPARE_QUIZ, SELECT_FREE_TRIAL
    }

}
