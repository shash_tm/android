package com.trulymadly.android.app.json;

import com.trulymadly.android.app.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

public class MatchLikeHideResponseParser {
    //@Trace(category = MetricCategory.JSON)
    public boolean parseMatchLikeHideResponse(JSONObject parseData) {
        try {
            if (!parseData.isNull("response_code")) {
                if (parseData.getInt("response_code") == 200 && !parseData.isNull("return_action")) {
                    JSONObject returnActionObject = parseData.getJSONObject("return_action");
                    String notificationCount = returnActionObject.optString("conversation_count");
                    if (Utility.isSet(notificationCount))
                        return true;
                }
            }
        } catch (JSONException ignored) {
        }
        return false;
    }
}