package com.trulymadly.android.app.json;

import android.content.Context;

import com.trulymadly.android.app.modal.StickerData;
import com.trulymadly.android.app.sqlite.StickerDBHandler;
import com.trulymadly.android.app.utility.FilesHandler;
import com.trulymadly.android.app.utility.ImageCacheHelper;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

public class StickerParser {
    private final Context ctx;
    private String prefix = "tm_";
    private boolean result = true;

    public StickerParser(Context aContext) {
        ctx = aContext;

    }

    //@Trace(category = MetricCategory.JSON)
    private StickerData createSticker(JSONObject stickerJsonObject, int galleryId, String type) {
        StickerData sticker = new StickerData();
        if (stickerJsonObject != null) {
            sticker.setId(stickerJsonObject.optInt("id"));
            sticker.setType(type);
            sticker.setGalleryId(galleryId);

        }
        return sticker;

    }

    // inserting and writing on disk
    //@Trace(category = MetricCategory.JSON)
    public void insert(JSONObject stickerJsonObject) {
        int latest_sticker_version = stickerJsonObject.optInt("sticker_version");
        int app_sticker_version = SPHandler.getInt(ctx,
                ConstantsSP.SHARED_KEYS_STICKER_VERSION);
        if (latest_sticker_version <= app_sticker_version)
            return;

        FilesHandler.createImageCacheFolder();

        String oppositegender = "m";
        if (Utility.isMale(ctx)) {
            oppositegender = "f";
        }
        JSONArray galleries = stickerJsonObject.optJSONArray("gallery");
        if (galleries != null) {

            for (int i = 0; i < galleries.length(); i++) {

                JSONObject gallery = galleries.optJSONObject(i);

                if (gallery != null) {
                    if (oppositegender.equalsIgnoreCase(gallery.optString("gender")))
                        continue;
                }

                StickerData type_gallery = new StickerData();
                int gallery_id = gallery.optInt("id");
                type_gallery = createSticker(gallery, 0, "gallery");

                insertAndWrite(type_gallery);

                JSONArray stickers = gallery.optJSONArray("stickers");
                if (stickers != null) {
                    for (int j = 0; j < stickers.length(); j++) {

                        JSONObject sticker_json = stickers.optJSONObject(j);
                        StickerData type_sticker = new StickerData();
                        type_sticker = createSticker(sticker_json, gallery_id, "sticker");
                        insertAndWrite(type_sticker);

                    }
                    }
                }
            }
        if (result) {
            SPHandler.setInt(ctx,
                    ConstantsSP.SHARED_KEYS_STICKER_VERSION, latest_sticker_version);
        }

    }

    private void insertAndWrite(StickerData sticker) {
        String url, stickerKey;


        if (sticker.getType().equalsIgnoreCase("gallery")) {
            if (!StickerDBHandler.isStickerExists("" + sticker.getId(), ctx)) {
                url = FilesHandler.createStickerUrl(sticker, null);
                stickerKey = FilesHandler.getStickerKey(url);
                if (!StickerDBHandler.insertSticker(sticker, ctx))
                    result = false;
                ImageCacheHelper.with(ctx).loadWithKey(url, stickerKey).fetch();

            }
        } else {
            url = FilesHandler.createStickerUrl(sticker, "thumbnail");
            stickerKey = FilesHandler.getStickerKey(url);
            if (!StickerDBHandler.insertSticker(sticker, ctx))
                result = false;
            ImageCacheHelper.with(ctx).loadWithKey(url, stickerKey).fetch();

        }


    }


}
