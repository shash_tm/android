package com.trulymadly.android.app.json;

import com.trulymadly.android.app.billing.PaymentFor;
import com.trulymadly.android.app.billing.PaymentMode;

/**
 * Created by udbhav on 18/01/16.
 */
public class ConstantsDB {
    //Curated Deals Chats Modes
    public static final String DATE_SPOT_TABLE = "date_spots";
    public static final String MESSAGES_TABLE = "messages";
    public static final String CONVERSATION_LIST_TABLE = "conversation_list";
    public static final String MESSAGE_METADATA_TABLE = "message_meta_data";
    public static final String MATCHES_DATA_TABLE = "matches_cached_data";
    public static final String SPARKS_TABLE = "sparks_data";
    public static final String PURCHASES_TABLE = "purchases";

    public static class SPARKS {
        public static final String USER_ID = "user_id";
        public static final String MATCH_ID = "match_id";
        public static final String DESIGNATION = "designation";
        public static final String MESSAGE_ID = "message_id";
        public static final String MESSAGE = "message";
        public static final String TIMESTAMP = "tstamp";
        public static final String SORT_TIMESTAMP = "sort_tstamp";
        public static final String EXPIRE_TIME = "expire_time";
        public static final String START_TIME = "start_time";
        public static final String CITY = "city";
        public static final String IMAGES = "images";
        public static final String HASH = "hash";

        public static String getCreateQuery() {
            return "CREATE TABLE IF NOT EXISTS " + ConstantsDB.SPARKS_TABLE + "(" +
                    USER_ID + " varchar(100), " +
                    MATCH_ID + " varchar(100), " +
                    DESIGNATION + " text, " +
                    MESSAGE_ID + " varchar(100), " +
                    MESSAGE + " text, " +
                    TIMESTAMP + " varchar(100), " +
                    SORT_TIMESTAMP + " varchar(100), " +
                    EXPIRE_TIME + " int, " +
                    START_TIME + " varchar(100), " +
                    CITY + " varchar(100), " +
                    IMAGES + " varchar(500), " +
                    HASH + " varchar(200), " +
                    "UNIQUE(" + USER_ID + "," + MATCH_ID + ") on CONFLICT IGNORE);";
        }
    }

    public static class PURCHASES {
        public static final String USER_ID = "user_id";
        public static final String DEVELOPER_PAYLOAD = "developer_payload";
        public static final String PURCHASE_STATE = "purchase_state";
        public static final String SKU = "sku";
        public static final String TOKEN = "token";
        public static final String PURCHASE_TIMESTAMP = "purchase_tstamp";
        public static final String ORDER_ID = "order_id";
        public static final String PAYMENT_MODE = "payment_mode";
        public static final String PAYMENT_FOR = "payment_for";

        public static String getCreateQuery() {
            return "CREATE TABLE IF NOT EXISTS " + ConstantsDB.PURCHASES_TABLE + "(" +
                    USER_ID + " varchar(100), " +
                    DEVELOPER_PAYLOAD + " varchar(512), " +
                    PURCHASE_STATE + " varchar(100) default " + PurchaseState.INITIALISED.toString() + "," +
                    SKU + " varchar(100), " +
                    TOKEN + " varchar(512), " +
                    PURCHASE_TIMESTAMP + " DATETIME, " +
                    ORDER_ID + " varchar(256), " +
                    "UNIQUE(" + USER_ID + "," + DEVELOPER_PAYLOAD + ") on CONFLICT IGNORE);";
        }

        //UpgradeTo44
        public static String addPaymentModeColumn() {
            return "ALTER TABLE " + ConstantsDB.PURCHASES_TABLE + " ADD COLUMN " + PAYMENT_MODE + " int default " + PaymentMode.google.getmKey();
        }

        public static String addPaymentForColumn() {
            return "ALTER TABLE " + ConstantsDB.PURCHASES_TABLE + " ADD COLUMN " + PAYMENT_FOR + " int default " + PaymentFor.spark.getmKey();
        }

        public enum PurchaseState {
            INITIALISED, PURCHASED_STORE, PURCHASED_SERVER, CONSUMED_STORE, CONSUMED_SERVER, CANCELLED
        }
    }

    public static class MESSAGE_METADATA {
        public static final String IS_SPARK_RECEIVED = "is_spark_received";
        public static final String IS_MUTUAL_SPARK = "is_mutual_spark";
        public static final String MATCH_ID = "match_id";
        public static final String IS_SELECT_MATCH = "is_select_match";
    }
}
