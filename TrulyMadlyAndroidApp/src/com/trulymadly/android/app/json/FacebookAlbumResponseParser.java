package com.trulymadly.android.app.json;

import com.trulymadly.android.app.modal.FacebookAlbumItem;
import com.trulymadly.android.app.modal.FacebookPhotoItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Vector;

public class FacebookAlbumResponseParser {

	public static String parseLatestStatus(String json) {
		String latestStatus = "";
		String startTag = "\"message\":\"";
		int indexOf = json.indexOf(startTag);
		if (indexOf > 0) {
			int start = indexOf + startTag.length();
			String endTag = "\",";
			return (json.substring(start, json.indexOf(endTag, start)));
		}
		return (latestStatus);
	}

    //@Trace(category = MetricCategory.JSON)
    public static Vector<FacebookAlbumItem> parseAlbums(JSONObject rootObj)
			throws JSONException {
		Vector<FacebookAlbumItem> albums = new Vector<>();
		JSONArray itemList = rootObj.getJSONArray("data");
		int albumCount = itemList.length();
		for (int albumIndex = 0; albumIndex < albumCount; albumIndex++) {
			JSONObject album = itemList.getJSONObject(albumIndex);
			if(album.optInt("count", 0) > 0) {
				albums.add(new FacebookAlbumItem(album.optString("id"), album
						.optString("name"), album.optString("description"), album
						.optString("cover_photo", null)));
			}
		}
		return (albums);
	}

    //@Trace(category = MetricCategory.JSON)
    public static Vector<FacebookPhotoItem> parsePhotos(JSONObject rootObj)
			throws JSONException {
		Vector<FacebookPhotoItem> photos = new Vector<>();
		JSONArray itemList = rootObj.getJSONArray("data");
		int photoCount = itemList.length();

		for (int photoIndex = 0; photoIndex < photoCount; photoIndex++) {
			JSONObject photo = itemList.getJSONObject(photoIndex);
			photos.add(new FacebookPhotoItem(photo.getString("picture"), photo
					.getString("source"), photo.optString("height"), photo
					.optString("width")));
		}
		return (photos);
	}
}