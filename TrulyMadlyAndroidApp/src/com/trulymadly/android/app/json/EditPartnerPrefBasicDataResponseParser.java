package com.trulymadly.android.app.json;

import com.crashlytics.android.Crashlytics;
import com.trulymadly.android.app.modal.EditPrefBasicDataModal;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class EditPartnerPrefBasicDataResponseParser {

	public static ArrayList<EditPrefBasicDataModal> getEditPrefBasicDataList(
			String parseData, int n) {
		// n=0 : for country or income, interests and anything else
		// n=1 : for state
		// n=2 : for city
		// n=3 : for highest degree id is storing as int

		if (Utility.isSet(parseData)) {
			JSONArray jsonEditPrefBasicArr = null;
			try {
				jsonEditPrefBasicArr = new JSONArray(parseData);

				if (jsonEditPrefBasicArr != null
						&& jsonEditPrefBasicArr.length() > 0) {
					int len = jsonEditPrefBasicArr.length();
					ArrayList<EditPrefBasicDataModal> editPrefBasicDataList = null;
					editPrefBasicDataList = new ArrayList<>();
					for (int i = 0; i < len; i++) {
						JSONObject jsonEditPrefBasicObj = jsonEditPrefBasicArr
								.optJSONObject(i);

						EditPrefBasicDataModal aEditPrefBasicDataModal = null;
						aEditPrefBasicDataModal = new EditPrefBasicDataModal();

						if (!jsonEditPrefBasicObj.isNull("key")) {
							if (n == 0)
								aEditPrefBasicDataModal
										.setIDorCountryID(jsonEditPrefBasicObj
												.optString("key"));
							else if (n == 1)
								aEditPrefBasicDataModal
										.setStateId(jsonEditPrefBasicObj
												.optString("key"));
							else if (n == 3)
								aEditPrefBasicDataModal.setDegreeId(Integer
										.parseInt(jsonEditPrefBasicObj
												.optString("key")));
							else
								aEditPrefBasicDataModal
										.setIDorCountryID(jsonEditPrefBasicObj
												.optString("key"));
						}

						if (!jsonEditPrefBasicObj.isNull("state_id"))
							aEditPrefBasicDataModal
									.setStateId(jsonEditPrefBasicObj
											.optString("state_id"));

						if (!jsonEditPrefBasicObj.isNull("city_id"))
							aEditPrefBasicDataModal
									.setCityId(jsonEditPrefBasicObj
											.optString("city_id"));

						if (!jsonEditPrefBasicObj.isNull("country_id"))
							aEditPrefBasicDataModal
									.setIDorCountryID(jsonEditPrefBasicObj
											.optString("country_id"));

						if (!jsonEditPrefBasicObj.isNull("value"))
							aEditPrefBasicDataModal
									.setName(jsonEditPrefBasicObj
											.optString("value"));
						else if (!jsonEditPrefBasicObj.isNull("name"))
							aEditPrefBasicDataModal
									.setName(jsonEditPrefBasicObj
											.optString("name"));

						aEditPrefBasicDataModal.setIsSelected(false);
						editPrefBasicDataList.add(aEditPrefBasicDataModal);
					}
					return editPrefBasicDataList;
				}
			} catch (JSONException e) {
				Crashlytics.logException(e);
				return null;
			} catch (OutOfMemoryError e) {
				return null;
			}
			return null;
		} else
			return null;
	}
}
