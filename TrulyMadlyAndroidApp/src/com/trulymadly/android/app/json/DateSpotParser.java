package com.trulymadly.android.app.json;

import android.content.Context;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.trulymadly.android.app.modal.DateSpotDealType;
import com.trulymadly.android.app.modal.DateSpotModal;
import com.trulymadly.android.app.modal.DateSpotZone;
import com.trulymadly.android.app.modal.LocationModal;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by deveshbatra on 12/22/15.
 */
public class DateSpotParser {


    public static DateSpotModal parseDateSpotResponse(JSONObject response, Context ctx) {

        DateSpotModal dateSpotModal = new DateSpotModal();

        JSONObject data = response.optJSONObject("data");
        JSONObject deal_details = response.optJSONObject("deal_details");
        if (data != null) {
            dateSpotModal.setId(data.optString("datespot_id"));
            dateSpotModal.setFriendlyName(data.optString("friendly_name"));
            dateSpotModal.setName(data.optString("name"));
            dateSpotModal.setLocation(data.optString("location"));
            dateSpotModal.setOffer(data.optString("offer"));
            dateSpotModal.setRecommendation(data.optString("recommendation"));
//            dateSpotModal.setPhoneNo(data.optString("phone_number"));
//            dateSpotModal.setAddress(data.optString("address"));

            dateSpotModal.setImages(createAttribute(data.optJSONArray("images")));
            dateSpotModal.setMenuImages(createAttribute(data.optJSONArray("menu_images")));
            dateSpotModal.setHashtags(createAttribute(data.optJSONArray("hashtags")));
            dateSpotModal.setTerms(createAttribute(data.optJSONArray("terms_and_conditions")));
            dateSpotModal.setCommunicationImage(data.optString("communication_image"));

            dateSpotModal.setLocations(createLocations(data.optJSONArray("locations")));
            dateSpotModal.setMultiLocationText(data.optString("multiple_locations_text"));


            try {
                dateSpotModal.setPhoneNo(dateSpotModal.getLocations().get(0).getContactNo());
                dateSpotModal.setAddress(dateSpotModal.getLocations().get(0).getAddress());
            } catch (IndexOutOfBoundsException e) {
                Crashlytics.log(Log.DEBUG, "DATE_SPOTS_PARSER", "datespot_id : " + data.optString("datespot_id"));
                Crashlytics.log(Log.DEBUG, "DATE_SPOTS_PARSER", "friendly_name : " + data.optString("friendly_name"));
                Crashlytics.log(Log.DEBUG, "DATE_SPOTS_PARSER", "location : " + data.optString("location"));
                Crashlytics.log(Log.DEBUG, "DATE_SPOTS_PARSER", "Locations size : " + dateSpotModal.getLocations().size());
                Crashlytics.logException(e);
            }

            //dateSpotModal.setAddress(data.optString("address"));


        }
        if (deal_details != null) {
            dateSpotModal.setDealStatus(deal_details.optString("deal_status"));
            dateSpotModal.setCommunicationText(deal_details.optString("communication_text", dateSpotModal.getDefaultCommunicationText(ctx)));

        }

        return dateSpotModal;

    }


    private static ArrayList<LocationModal> createLocations(JSONArray array) {

        ArrayList<LocationModal> locations = new ArrayList<>();
        for (int i = 0; array != null && i < array.length(); i++) {
            LocationModal location = new LocationModal();
            location.setContactNo(array.optJSONObject(i).optString("phone_number"));
            location.setAddress(array.optJSONObject(i).optString("address"));
            locations.add(location);
        }
        return locations;

    }

    private static String[] createAttribute(JSONArray array) {
        ArrayList<String> str = new ArrayList<>();

        if (array != null) {
            for (int i = 0; i < array.length(); i++) {
                str.add(array.optString(i));
            }
        }

        String[] attribute = new String[str.size()];
        attribute = str.toArray(attribute);

        return attribute;
    }


    public static ArrayList<DateSpotModal> parseDateSpotListResponse(JSONObject response) {
        ArrayList<DateSpotModal> dateSpotList = new ArrayList<>();

        if (response == null) {
            return dateSpotList;
        }
        JSONArray dates = response.optJSONArray("dates");
        int len = 0;
        if (dates != null) {
            len = dates.length();
        }
        for (int i = 0; i < len; i++) {
            JSONObject dateSpotJson = dates.optJSONObject(i);
            DateSpotModal dateSpotModal = new DateSpotModal();
            dateSpotModal.setId(dateSpotJson.optString("datespot_id"));
            dateSpotModal.setFriendlyName(dateSpotJson.optString("friendly_name"));
            dateSpotModal.setName(dateSpotJson.optString("name"));
            dateSpotModal.setLocation(dateSpotJson.optString("location"));
            dateSpotModal.setPricingValue(dateSpotJson.optInt("pricing"));
            dateSpotModal.setImageProfile(dateSpotJson.optString("image"));
            dateSpotModal.setHashtags(createAttribute(dateSpotJson.optJSONArray("hashtags")));
            dateSpotModal.setDealType(DateSpotDealType.createFromString(dateSpotJson.optString("deal_type")));
            dateSpotModal.setLocationCount(dateSpotJson.optInt("location_count"));
            dateSpotModal.setMultiLocationText(dateSpotJson.optString("multiple_locations_text"));
            dateSpotModal.setNew(dateSpotJson.optInt("isNew") != 0);
            dateSpotModal.setSpecial_code(dateSpotJson.optInt("special_code"));
            String dist = dateSpotJson.optString("dist"), unit = dateSpotJson.optString("unit");
            if (Utility.isSet(dist) && Utility.isSet(unit)) {
                dateSpotModal.setDistance(dist + " " + unit);
            }
            dateSpotModal.setLocationScore(dateSpotJson.optString("locationScore"));
            dateSpotModal.setPopularityScore(dateSpotJson.optString("popularityScore"));
            dateSpotModal.setTotalScore(dateSpotJson.optString("totalScore"));
            dateSpotModal.setZone_id(dateSpotJson.optString("zone_id"));
            dateSpotModal.setGeo_location(dateSpotJson.optString("geo_location"));
            dateSpotModal.setRank(dateSpotJson.optString("rank"));
            dateSpotList.add(dateSpotModal);
        }
        return dateSpotList;
    }

    public static ArrayList<DateSpotZone> parseDateSpotListForZonesResponse(JSONObject response) {
        ArrayList<DateSpotZone> dateSpotZones = new ArrayList<>();

        JSONArray zones = response.optJSONArray("zones");
        int len = 0;
        if (zones != null) {
            len = zones.length();
        }
        for (int i = 0; i < len; i++) {
            JSONObject zoneJson = zones.optJSONObject(i);
            DateSpotZone dateSpotZone = new DateSpotZone();
            dateSpotZone.setZoneId(zoneJson.optString("zone_id"));
            dateSpotZone.setName(zoneJson.optString("name"));
            dateSpotZone.setIsSelected(zoneJson.optBoolean("selected"));
            dateSpotZones.add(dateSpotZone);
        }
        return dateSpotZones;
    }
}
