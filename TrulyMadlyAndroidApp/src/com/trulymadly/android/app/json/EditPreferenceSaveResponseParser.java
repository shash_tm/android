package com.trulymadly.android.app.json;

import com.crashlytics.android.Crashlytics;

import org.json.JSONException;
import org.json.JSONObject;

public class EditPreferenceSaveResponseParser {

	public static boolean parseEditPreferenceSaveResponse(String parseData)
	{
		if(CheckJSONObject.isJSONStringValid(parseData) && parseData!=null){
			JSONObject jsonEditPreferenceSaveResponse;
			boolean isEditPreDataSaved=false; 
			try {  
				jsonEditPreferenceSaveResponse = new JSONObject(parseData);
 
				if(!jsonEditPreferenceSaveResponse.isNull("responseCode")){

					String responseCode = jsonEditPreferenceSaveResponse.getString("responseCode");
					if(responseCode!=null && responseCode.equals("200"))
						isEditPreDataSaved=true;

					return isEditPreDataSaved;
				}
				return isEditPreDataSaved;
			} catch (JSONException e) {
				 Crashlytics.logException(e);
				return isEditPreDataSaved;
			}
		}
		else{
		}
		return false;
	}
}