package com.trulymadly.android.app.json;

import android.content.Context;

import com.crashlytics.android.Crashlytics;
import com.trulymadly.android.app.UserPhotos;
import com.trulymadly.android.app.modal.Photos;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PhotoResponseParser {

    //@Trace(category = MetricCategory.JSON)
    public static ArrayList<Photos> parsePhotoResponse(String parseData,
			Context aContext) {
		ArrayList<Photos> photosList = null;
		if (Utility.isSet(parseData)) {
			try {
				JSONArray photoArray = new JSONArray(parseData);
				int size = photoArray.length();
				if (size > 0) {
					photosList = new ArrayList<>();
					for (int i = 0; i < size; i++) {
						JSONObject aPhotoJson = photoArray.getJSONObject(i);
						Photos photo = new Photos(
								aPhotoJson.optString("photo_id"),
								aPhotoJson.optString("name"),
								aPhotoJson.optString("thumbnail"),
								aPhotoJson.optString("is_profile"),
								aPhotoJson.optString("status"),
								aPhotoJson.optString("admin_approved"),
								aPhotoJson.optString("can_profile"));

						if ((aPhotoJson.optString("is_profile"))
								.equalsIgnoreCase("yes")) {
							UserPhotos.profilePhoto = photo;
							SPHandler.setString(
									aContext,
									ConstantsSP.SHARED_KEYS_USER_PROFILE_THUMB_URL,
									UserPhotos.profilePhoto.getThumbnail());
							SPHandler.setString(
									aContext,
									ConstantsSP.SHARED_KEYS_USER_PROFILE_FULL_URL,
									UserPhotos.profilePhoto.getName());
						} else {
                            photosList.add(photo);
						}
					}
					return photosList;
				}
			} catch (JSONException e) {
				Crashlytics.logException(e);
			}
		}
		return photosList;
	}

	public static ArrayList<Photos> parseVideosResponse(String parseData,
														Context aContext) {
		//TODO : Implement this
		return null;
	}
}