package com.trulymadly.android.app.json;

import android.content.Context;

import com.trulymadly.android.app.modal.ChatsConfig;
import com.trulymadly.android.app.modal.MySelectData;
import com.trulymadly.android.app.modal.UserFlags;
import com.trulymadly.android.app.sqlite.RFHandler;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.SparksHandler;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by avin on 03/12/15.
 */
public class UserFlagsParser {
    //@Trace(category = MetricCategory.JSON)
    public static UserFlags parseUserFlags(Context aContext, JSONObject userFlagsJO) {
        UserFlags userFlags = null;
        if (userFlagsJO != null) {
            userFlags = new UserFlags();
            userFlags.setmChatsConfig(ChatsConfigParser.parseChatsConfig(
                    userFlagsJO.optJSONObject("chat_configurations")));
//            userFlags.setmInterstitialNativeAdsConfig(AdsFlagsParser.parseInterstitialNativeAdsFlags(
//                    userFlagsJO.optJSONObject("native_ad_flags")));
//
//            userFlags.setmProfileAdsConfig(AdsFlagsParser.parseProfileAdsFlags(
//                    userFlagsJO.optJSONObject("matches_ads_flags")));

            userFlags.setmAdsConfig(AdsFlagsParser.parseAdsFlags(
                    userFlagsJO.optJSONObject("matches_ads_flags")));

//            JSONObject curatedDealsIcon = userFlagsJO.optJSONObject("curated_deal_icon");
//            if (curatedDealsIcon != null) {
//                userFlags.setCDEnabled(curatedDealsIcon.optBoolean("icon_shown"));
//            }
            //Hack to disable curated deals
            userFlags.setCDEnabled(false);

            userFlags.setAllowPhotoShareInChat(userFlagsJO.optBoolean("allow_photo_share", false));
            userFlags.setShowPhotoOnMissTM(userFlagsJO.optBoolean("allow_photo_share_on_miss_tm", true));
            if (userFlagsJO.has("meetups_enabled")) {
                userFlags.setAllowMeetups(userFlagsJO.optBoolean("meetups_enabled"));
            } else {
                userFlags.setAllowMeetups(RFHandler.getBool(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_ALLOW_MEETUPS));
            }

            JSONObject activityData = null;
            try {
                activityData = userFlagsJO.optJSONObject("activityData");
                if (activityData != null) {
                    userFlags.setmPopularity(activityData.getInt("popularity"));
                    userFlags.setmProfileView(activityData.getInt("activity"));
                    userFlags.setmProfileViewsText(activityData.getString("activity_text"));
                }
            } catch (JSONException e) {
                return userFlags;
            }

            //isLastActiveShown
            userFlags.setIsLastActiveShown(userFlagsJO.optBoolean("shouldInactivityTimeBeShown", false));

            String email = userFlagsJO.optString("user_email");
            if (Utility.isSet(email)) {
                userFlags.setUserEmail(email);
            }

            //App update params, hard and soft
            JSONObject update_version_flags = userFlagsJO.optJSONObject("update_version_flags");
            if (update_version_flags != null) {
                userFlags.setHardAppVersion(update_version_flags.optInt("hard_app_version_android", 0));
                userFlags.setSoftAppVersion(update_version_flags.optInt("soft_app_version_android", 0));
                userFlags.setHardUpdateText(update_version_flags.optString("hard_update_text_android", "Please update the app."));
                userFlags.setSoftUpdateText(update_version_flags.optString("soft_update_text_android", "Please update the app."));
            }


            JSONObject favoritesJsonObject = userFlagsJO.optJSONObject("favourites");
            if (favoritesJsonObject != null) {
                userFlags.setShowFavoritesInMatchesIntervalInDays(favoritesJsonObject.optInt("interval_in_day", 30));
                userFlags.setShowFavoritesInMatchesFemalePosition(favoritesJsonObject.optInt("female_position", 10));
                userFlags.setShowFavoritesInMatchesFemaleHides(favoritesJsonObject.optInt("female_hides", 10));
            }


            //Checks and enables/disables individual payments : Google/Paytm
            //Makes sure that at least one payment option is always active
            JSONObject jsonObject = userFlagsJO.optJSONObject("payments");
            boolean atLeastOneEnabled = false;
            if(jsonObject != null){
                userFlags.setGooglePaymentEnabled(jsonObject.optBoolean("google"));
                userFlags.setPaytmPaymentEnabled(jsonObject.optBoolean("paytm"));
                userFlags.setPaytmWalletEnabled(jsonObject.optBoolean("paytm_wallet"));
                userFlags.setPaytmNBEnabled(jsonObject.optBoolean("paytm_nb"));
                userFlags.setPaytmCCEnabled(jsonObject.optBoolean("paytm_cc"));
                userFlags.setPaytmDCEnabled(jsonObject.optBoolean("paytm_dc"));

//                userFlags.setPaytmPaymentEnabled(false);
//                userFlags.setPaytmWalletEnabled(true);
//                userFlags.setPaytmNBEnabled(true);
//                userFlags.setPaytmCCEnabled(true);
//                userFlags.setPaytmDCEnabled(true);

                atLeastOneEnabled = userFlags.isGooglePaymentEnabled() || userFlags.isPaytmPaymentEnabled() ||
                        userFlags.isPaytmWalletEnabled() || userFlags.isPaytmCCEnabled() ||
                        userFlags.isPaytmDCEnabled() || userFlags.isPaytmNBEnabled();
            }
            if(!atLeastOneEnabled){
                userFlags.setGooglePaymentEnabled(true);
            }

            //Why not spark instead dialog configuration
            JSONObject whyNotSpark = userFlagsJO.optJSONObject("why_not_sparks");
            if(whyNotSpark != null){
                userFlags.setWhyNotSparksEnabled(whyNotSpark.optBoolean("enabled"));
                userFlags.setmWhyNotSparksInsteadMatchesCount(whyNotSpark.optInt("matches_count"));
                userFlags.setmWhyNotSparkNudgeContent(whyNotSpark.optString("content"));
            }

            userFlags.setSparksEnabled(userFlagsJO.optBoolean("sparks_active"));
            userFlags.setmSparksCount(userFlagsJO.optInt("sparkCountersLeft", -1));


            //TM Select
            /*
            * select_flags:
                {
                    my_select:
                    {
                        price: "999",
                        days_left: "66",
                        quiz_played: false,
                        is_tm_select: true,
                        side_cta: "Join",
                        profile_cta: "Join",
                        profile_cta_text: "Rs. 999 after first month"
                    }
                }

            select_flags: {
                my_select: {
                    price: "999",
                    days_left: "66",
                    quiz_played: false,
                    is_tm_select: true,
                    side_cta: "Join",
                    profile_cta: "Join",
                    profile_cta_text: "Rs. 999 after first month"
                    compatibility_text_def : "",
                    compatibility_image_def : ""
                },
                select_nudge : {
                  enabled : true/false,
                  frequency : <int (in hours)>
                },
                action_nudge : {
                  allow_like : true/false,
                  allow_spark : true/false,
                  allow_likeback : true/false
                }
            }

            */
            userFlags.setSelectEnabled(userFlagsJO.optBoolean("select_active"));
            JSONObject selectFlags = userFlagsJO.optJSONObject("select_flags");
            if (selectFlags != null) {
                JSONObject mySelect = selectFlags.optJSONObject("my_select");
                if (mySelect != null) {
                    userFlags.setmMySelectData(new MySelectData(mySelect));
                }
                JSONObject selectNudge = selectFlags.optJSONObject("select_nudge");
                if (selectNudge != null) {
                    userFlags.setSelectNudgeEnabled(selectNudge.optBoolean("enabled"));
                    userFlags.setmSelectNudgeContent(selectNudge.optString("content"));
                    userFlags.setmSelectNudgeFrequency(selectNudge.optInt("frequency"));
                }
                JSONObject selectActionNudge = selectFlags.optJSONObject("action_nudge");
                if (selectActionNudge != null) {
                    userFlags.setLikeAllowedOnSelect(selectActionNudge.optBoolean("allow_like"));
                    userFlags.setSparkAllowedOnSelect(selectActionNudge.optBoolean("allow_spark"));
                    userFlags.setLikebackAllowedOnSelect(selectActionNudge.optBoolean("allow_likeback"));
                }
            }

        }
        return userFlags;
    }

    //@Trace(category = MetricCategory.JSON)
    private static UserFlags processUserFlags(Context aContext, UserFlags userFlags) {
        if (userFlags != null) {
            String userId = Utility.getMyId(aContext);
            ChatsConfig chatsConfig = userFlags.getmChatsConfig();
            ChatsConfigParser.processChatsConfig(aContext, userId, chatsConfig);
//            InterstitialNativeAdsConfig interstitialNativeAdsConfig = userFlags.getmInterstitialNativeAdsConfig();
//            AdsFlagsParser.processInterstitialNativeAdsFlags(aContext, userId, interstitialNativeAdsConfig);
//            AdsFlagsParser.processProfileAdsFlags(aContext, userFlags.getmProfileAdsConfig());

            AdsFlagsParser.processAdsFlags(aContext, userFlags.getmAdsConfig());

            RFHandler.insert(aContext,
                    Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_CD_ENABLED, userFlags.isCDEnabled());

            RFHandler.insert(aContext,
                    Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_SHOW_PHOTO_ON_MISS_TM, userFlags.isShowPhotoOnMissTM());

            RFHandler.insert(aContext,
                    Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_ALLOW_PHOTO_IN_CHAT, userFlags.isAllowPhotoShareInChat());

            RFHandler.insert(aContext,
                    Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_ALLOW_MEETUPS, userFlags.isAllowMeetups());

//            if(userFlags.getmPopularity() != -1){
//                RFHandler.insert(aContext, Utility.getMyId(aContext),
//                        Constants.RIGID_FIELD_USER_PI_VALUE, userFlags.getmPopularity());
//            }

            RFHandler.insert(aContext, Utility.getMyId(aContext),
                    ConstantsRF.RIGID_FIELD_LAST_ACTIVE, userFlags.isLastActiveShown());

            if (Utility.isSet(userFlags.getUserEmail())) {
                SPHandler.setString(aContext, ConstantsSP.SHARED_KEYS_USER_EMAIL, userFlags.getUserEmail());
            } else {
                SPHandler.remove(aContext, ConstantsSP.SHARED_KEYS_USER_EMAIL);
            }

            //Decides the following things:
            //1. Sliding menu : TM Frills toggling
            //2. Matches : Spark icon toggling
            //3. Matches: Fetcj sparks call for blooper
            //4. Conversations: Spark icon toggling
            //5. Events : Spark icon toggling?
            RFHandler.insert(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_SPARK_ENABLED, userFlags.isSparksEnabled());

            RFHandler.insert(aContext, userId, ConstantsRF.RIGID_FIELD_IS_GOOGLE_PAYMENT_ENABLED, userFlags.isGooglePaymentEnabled());
            RFHandler.insert(aContext, userId, ConstantsRF.RIGID_FIELD_IS_PAYTM_PAYMENT_ENABLED, userFlags.isPaytmPaymentEnabled());
            RFHandler.insert(aContext, userId, ConstantsRF.RIGID_FIELD_IS_PAYTM_WALLET_PAYMENT_ENABLED, userFlags.isPaytmWalletEnabled());
            RFHandler.insert(aContext, userId, ConstantsRF.RIGID_FIELD_IS_PAYTM_NB_PAYMENT_ENABLED, userFlags.isPaytmNBEnabled());
            RFHandler.insert(aContext, userId, ConstantsRF.RIGID_FIELD_IS_PAYTM_CC_PAYMENT_ENABLED, userFlags.isPaytmCCEnabled());
            RFHandler.insert(aContext, userId, ConstantsRF.RIGID_FIELD_IS_PAYTM_DC_PAYMENT_ENABLED, userFlags.isPaytmDCEnabled());


            RFHandler.insert(aContext, userId, ConstantsRF.RIGID_FIELD_SHOW_FAVORITES_IN_MATCHES_INTERVAL_DAYS, userFlags.getShowFavoritesInMatchesIntervalInDays());
            RFHandler.insert(aContext, userId, ConstantsRF.RIGID_FIELD_SHOW_FAVORITES_IN_MATCHES_FEMALE_POSITION, userFlags.getShowFavoritesInMatchesFemalePosition());
            RFHandler.insert(aContext, userId, ConstantsRF.RIGID_FIELD_SHOW_FAVORITES_IN_MATCHES_FEMALE_HIDES, userFlags.getShowFavoritesInMatchesFemaleHides());

            SPHandler.setBool(aContext, ConstantsSP.SHARED_KEY_WHY_NOT_SPARK_INSTEAD_ENABLED, userFlags.isWhyNotSparksEnabled());
            SPHandler.setInt(aContext, ConstantsSP.SHARED_KEY_WHY_NOT_SPARK_INSTEAD_COUNT, userFlags.getmWhyNotSparksInsteadMatchesCount());
            SPHandler.setString(aContext, ConstantsSP.SHARED_KEY_WHY_NOT_SPARK_INSTEAD_MESSAGE, userFlags.getmWhyNotSparkNudgeContent());
            if (userFlags.getmSparksCount() >= 0) {
                SparksHandler.insertSparks(aContext, userFlags.getmSparksCount());
            }

            //Select Flags
            RFHandler.insert(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_SELECT_ENABLED, userFlags.isSelectEnabled());

            MySelectData mySelectData = userFlags.getmMySelectData();
            if (mySelectData != null) {
                mySelectData.saveMySelectData(aContext);
            }
            //Select Nudge
            RFHandler.insert(aContext, userId, ConstantsRF.RIGID_FIELD_SELECT_NUDGE_ENABLED, userFlags.isSelectNudgeEnabled());
            RFHandler.insert(aContext, userId, ConstantsRF.RIGID_FIELD_SELECT_NUDGE_CONTENT, userFlags.getmSelectNudgeContent());
            RFHandler.insert(aContext, userId, ConstantsRF.RIGID_FIELD_SELECT_NUDGE_FREQUENCY, userFlags.getmSelectNudgeFrequency());
            //Select Action Nudge
            RFHandler.insert(aContext, userId, ConstantsRF.RIGID_FIELD_SELECT_ACTION_ALLOW_LIKE, userFlags.isLikeAllowedOnSelect());
            RFHandler.insert(aContext, userId, ConstantsRF.RIGID_FIELD_SELECT_ACTION_ALLOW_SPARK, userFlags.isSparkAllowedOnSelect());
            RFHandler.insert(aContext, userId, ConstantsRF.RIGID_FIELD_SELECT_ACTION_ALLOW_LIKEBACK, userFlags.isLikebackAllowedOnSelect());

        }

        return userFlags;
    }

    //@Trace(category = MetricCategory.JSON)
    public static UserFlags processAndParseUserFlags(Context aContext, JSONObject userFlagsJO) {
        return processUserFlags(aContext, parseUserFlags(aContext, userFlagsJO));
    }
}
