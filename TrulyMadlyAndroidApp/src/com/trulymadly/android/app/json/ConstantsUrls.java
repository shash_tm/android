package com.trulymadly.android.app.json;

import android.content.Context;

import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.Utility;

/**
 * Created by udbhav on 09/05/16.
 */
public class ConstantsUrls {
    public static final String COOKIE_DOMAIN = ".trulymadly.com";
    private static final String CDN_DOMAIN = "cdn" + COOKIE_DOMAIN;
    private static final String SUFFIX_LIVE = "/";
    private static final String PREFIX_LIVE_HTTPS = "api";
    private static final String PREFIX_LIVE = "www";
    private static final String PREFIX_T1 = "t1";
    private static final String PREFIX_DEV = "dev";
    private static final String PREFIX = (Constants.isLive ? (Constants.isHTTPS ? PREFIX_LIVE_HTTPS : PREFIX_LIVE)
            : (Constants.isT1 ? PREFIX_T1 : PREFIX_DEV));
    private static final String DOMAIN = PREFIX + COOKIE_DOMAIN;
    private static final String SCHEME_HTTP = "http:";
    private static final String SCHEME_HTTPS = "https:";
    /* based on isLive and isT1 */
    private static final String SCHEME = ((Constants.isLive || Constants.isT1) && Constants.isHTTPS) ? SCHEME_HTTPS : SCHEME_HTTP;

    //private static String SUFFIX_DEV = "/trulymadly/";
    private static final String SUFFIX_DEV_DEFAULT = "trulymadly";
    private static final String SUFFIX_T1_DEFAULT = "";
    private static String SUFFIX_DEV_DYNAMIC = SUFFIX_DEV_DEFAULT;
    private static String SUFFIX_T1_DYNAMIC = SUFFIX_T1_DEFAULT;

    public static String getPaytmChecksumGenerationUrl() {
        return getBASEURL() + "spark/paytm.php?action=generate_checksum";
    }

    public static String getPaytmChecksumVerificationUrl() {
        return getBASEURL() + "spark/Paytm_App_Checksum_Kit_PHP/verifyChecksum.php";
    }

    //Sparks
    public static String getSparksApiUrl() {
        return getBASEURL() + "spark/spark.php";
    }

    //TM Select
    public static String getSelectApiUrl() {
        return getBASEURL() + "select/select.php";
    }

    public static String get_delete_api_url() {
        return getBASEURL() + "delete_api.php";
    }

    public static String get_otp_url() {
        return getBASEURL() + "trustbuilder_api.php";
    }

    public static String get_category_url() {
        return getBASEURL() + "curatedDeals/events.php";
    }

    public static String get_date_spot_url() {
        return getBASEURL() + "curatedDeals/datespot.php";
    }

    public static String get_datespotListUrl() {
        return getBASEURL()
                + "curatedDeals/dates.php";
    }


    public static String get_MutualFriendListUrl() {
        return getBASEURL()
                + "mutual_friends/get_mutual_friends.php";
    }
    public static String get_getAskFriendsUrl() {
        return getBASEURL()
                + "utilities/shortenurl.php";
    }

    public static String get_sticker_url() {
        return getBASEURL()
                + "msg/stickersStaticApiV2.php";
    }

    public static String get_quiz_url() {
        return getBASEURL() + "quiz/quiz_data.php";
    }

    public static String get_matches_utils_url() {
        return getBASEURL()
                + "matches/matchesUtilites.php";
    }

    public static String get_forget_password_url() {
        return getBASEURL()
                + "login/resetPass.php";
    }

    public static String get_true_compatibility_url() {
        return getBASEURL()
                + "truecompatibility.php";
    }

    public static String get_trust_security_url() {
        return getBASEURL()
                + "trustnsecurity.php";
    }

    public static String get_safety_url() {
        return getBASEURL() + "guidelines.php";
    }

    public static String get_privacy_url() {
        return getBASEURL() + "policy.php";
    }

    public static String get_terms_url() {
        return getBASEURL() + "terms.php";
    }

    public static String get_faq_url() {
        return getBASEURL() + "faq.php";
    }

    public static String get_USER_FLAGS_URL() {
        return getBASEURL() + "user/userFlags.php";
    }

    public static String get_NOTIFICATION_URL() {
        return getBASEURL()
                + "utilities/notifications.php";
    }

    public static String get_EVENT_TRK_URL() {
        return getBASEURL() + "trk.php";
    }

    public static String get_profile_deactivate_url() {
        return getBASEURL()
                + "logging/deactivateProfile.php";
    }

    public static String get_gcm_url() {
        return getBASEURL() + "mobile_utilities/gcm.php";
    }

    public static String get_trust_builder_api_url() {
        return getBASEURL()
                + "trustbuilder_api.php";
    }

    public static String getPasscodeTrackUrl(){
        return getBASEURL()
                + "passcode_track.php";
    }

    public static String get_dictionary_url() {
        return getBASEURL() + "dictionary.php";
    }

    public static String get_hashtag_url() {
        return getBASEURL() + "hashtag_api.php";
    }

    //public static String get_geolocations_url() { return getBASEURL() + "geolocations.php"; }
    public static String get_colleges_url() {
        return getBASEURL() + "colleges.php";
    }

    public static String get_user_hobby_url() {
        return getBASEURL() + "psycho.php";
    }

    public static String get_favorites_url() {
        return getBASEURL() + "facebook_likes/facebook_likes.php";
    }

    public static String get_user_update_url() {
        return getBASEURL() + "update.php";
    }

    public static String get_user_edit_url() {
        return getBASEURL() + "editprofile.php";
    }

    public static String get_user_data_url() {
        return getBASEURL() + "register.php";
    }

    public static String get_linkedinOauthUrl() {
        return getBASEURL() + "linkedinOAuth.php";
    }

    public static String get_linkedin_url() {
        return getBASEURL() + "linkedin2.php";
    }

    public static String get_feedback_url() {
        return getBASEURL() + "contact.php";
    }

    public static String get_editpreference_new_url() {
        return getBASEURL()
                + "editpreference_app.php";
    }

    public static String get_editpreference_url() {
        return getBASEURL()
                + "editpreference.php";
    }

    public static String get_photos_share_url() {
        return getBASEURL() + "photo_share.php";
    }

    public static String get_post_video_url() {
        return getBASEURL() + "videos/PostVideo.php";
    }

    public static String get_photos_url() {
        return getBASEURL() + "photo.php";
    }

    public static String get_logout_url() {
        return getBASEURL() + "logout.php";
    }

    private static String message_full_conv_url() {
        return getBASEURL()
                + "msg/message_full_conv.php";
    }

    public static String get_message_url() {
        return getBASEURL() + "msg/messages.php";
    }

    public static String get_my_profile_url() {
        return getBASEURL() + "profile.php?me=true";
    }

    private static String profile_url() {
        return getBASEURL() + "profile.php";
    }

    public static String get_matches_url() {
        return getBASEURL() + "matches.php";
    }

    public static String get_basic_data_url_default() {
        return getBASEURL()
                + "register_data.json?ver=666";
    }

    public static String get_login_url() {
        return getBASEURL() + "index.php";
    }

    public static String get_update_acces_token_url() {
        return getBASEURL() + "update_access_token.php";
    }

    private static String getSUFFIX_DEV() {
        return "/" + SUFFIX_DEV_DYNAMIC + "/";
    }

    public static String setSUFFIX_DEV(Context aContext, String newSUFFIX_DEV) {
        if (!Utility.isSet(newSUFFIX_DEV) && Constants.isDev) {
            newSUFFIX_DEV = SPHandler.getString(aContext, ConstantsSP.SHARED_KEY_SUFFIX_DEV);
        }
        if (Utility.isSet(newSUFFIX_DEV)) {
            SUFFIX_DEV_DYNAMIC = newSUFFIX_DEV.trim().toLowerCase();
        } else {
            SUFFIX_DEV_DYNAMIC = SUFFIX_DEV_DEFAULT;
        }
        SPHandler.setString(aContext, ConstantsSP.SHARED_KEY_SUFFIX_DEV, SUFFIX_DEV_DYNAMIC);
        return SUFFIX_DEV_DYNAMIC;
    }

    private static String getSUFFIX_T1() {
        return Utility.isSet(SUFFIX_T1_DYNAMIC) ? "/" + SUFFIX_T1_DYNAMIC + "/" : "/";
    }

    public static String setSUFFIX_T1(Context aContext, String newSUFFIX_T1) {
        if (!Utility.isSet(newSUFFIX_T1)) {
            newSUFFIX_T1 = SUFFIX_T1_DEFAULT;
        }
        if (Constants.isT1) {
            newSUFFIX_T1 = SPHandler.getString(aContext, ConstantsSP.SHARED_KEY_SUFFIX_T1);
        }
        SUFFIX_T1_DYNAMIC = newSUFFIX_T1.trim().toLowerCase();
        SPHandler.setString(aContext, ConstantsSP.SHARED_KEY_SUFFIX_T1, SUFFIX_T1_DYNAMIC);
        return SUFFIX_T1_DYNAMIC;
    }

    //    private static final String BASEURL = SCHEME + "//" + DOMAIN
//            + (Constants.isLive ? SUFFIX_LIVE : (Constants.isT1 ? SUFFIX_T1 : SUFFIX_DEV));
    private static String getBASEURL() {
        return SCHEME + "//" + DOMAIN + (Constants.isLive ? SUFFIX_LIVE : (Constants.isT1 ? getSUFFIX_T1() : getSUFFIX_DEV()));
    }

    //    public static final String CDNURL = SCHEME_HTTP + "//" + (Constants.isLive ? CDN_DOMAIN : DOMAIN)
//            + (Constants.isLive ? SUFFIX_LIVE : (Constants.isT1 ? SUFFIX_T1 : SUFFIX_DEV));
    public static String getCDNURL() {
        return SCHEME_HTTP + "//" + (Constants.isLive ? CDN_DOMAIN : DOMAIN)
                + (Constants.isLive ? SUFFIX_LIVE : (Constants.isT1 ? getSUFFIX_T1() : getSUFFIX_DEV()));
    }
}
