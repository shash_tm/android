package com.trulymadly.android.app.json;

import com.trulymadly.android.app.modal.AdNativeFullScreenModal;
import com.trulymadly.android.app.modal.AdNativeListItemModal;
import com.trulymadly.android.app.modal.AdNativeModal;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by avin on 27/10/15.
 */
public class AdParser {

    public static AdNativeFullScreenModal parseNativeFullScreenAd(JSONObject jsonObject){

        AdNativeFullScreenModal adNativeFullScreenModal = null;
        try {
            adNativeFullScreenModal = new AdNativeFullScreenModal();
            adNativeFullScreenModal.setmTitle(jsonObject.getString("title"));
            adNativeFullScreenModal.setmLandingUrl(jsonObject.getString("landingURL"));
            adNativeFullScreenModal.setmCta(jsonObject.getString("cta"));
            JSONObject screenShotJson = jsonObject.getJSONObject("screenshots");
            String imageUrl = screenShotJson.getString("url");
            Double aspectRadio = screenShotJson.getDouble("aspectRatio");
            int height = screenShotJson.getInt("height");
            int width = screenShotJson.getInt("width");

            adNativeFullScreenModal.setmAdImageModal(imageUrl, aspectRadio, height, width);

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

        return adNativeFullScreenModal;
    }

    public static AdNativeListItemModal parseNativeListItemAd(JSONObject jsonObject){

        AdNativeListItemModal adNativeListItemModal = null;
        try {
            adNativeListItemModal = new AdNativeListItemModal();
            adNativeListItemModal.setmTitle(jsonObject.getString("title"));
            adNativeListItemModal.setmLandingUrl(jsonObject.getString("landingURL"));
            adNativeListItemModal.setmCta(jsonObject.getString("cta"));
            adNativeListItemModal.setmDescription(jsonObject.getString("description"));
            JSONObject screenShotJson = jsonObject.getJSONObject("icon");
            String imageUrl = screenShotJson.getString("url");
            Double aspectRadio = screenShotJson.getDouble("aspectRatio");
            int height = screenShotJson.getInt("height");
            int width = screenShotJson.getInt("width");

            adNativeListItemModal.setmAdImageModal(imageUrl, aspectRadio, height, width);

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

        return adNativeListItemModal;
    }

    public static AdNativeModal parseNativeAd(JSONObject jsonObject){

        AdNativeModal adNativeModal = null;
        try {
            adNativeModal = new AdNativeFullScreenModal();
            adNativeModal.setmTitle(jsonObject.getString("title"));
            adNativeModal.setmLandingUrl(jsonObject.getString("landingURL"));
            adNativeModal.setmCta(jsonObject.getString("cta"));
            JSONObject screenShotJson = jsonObject.getJSONObject("screenshots");
            String imageUrl = screenShotJson.getString("url");
            Double aspectRadio = screenShotJson.getDouble("aspectRatio");
            int height = screenShotJson.getInt("height");
            int width = screenShotJson.getInt("width");

            adNativeModal.setmAdImageModal(imageUrl, aspectRadio, height, width);

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

        return adNativeModal;
    }

}
