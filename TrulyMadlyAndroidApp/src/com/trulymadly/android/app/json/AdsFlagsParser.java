package com.trulymadly.android.app.json;

import android.content.Context;

import com.trulymadly.android.app.ads.AdsConfig;
import com.trulymadly.android.app.ads.AdsHelper;
import com.trulymadly.android.app.sqlite.RFHandler;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.TmLogger;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONObject;

/**
 * Created by avin on 03/12/15.
 */
public class AdsFlagsParser {
    private static final String TAG = "AdsFlagsParser";

    public static void processAndParseAdsFlags(Context context, JSONObject adsConfigJO) {
        processAdsFlags(context, parseAdsFlags(adsConfigJO));
    }

    public static AdsConfig parseAdsFlags(JSONObject adsConfigJO){
        /*
        * Expected structure:
         {
            "is_79_enabled": true,

            "profile_ads_final_position": "10",
            "profile_ads_enabled": "true",
            "repeat": "true",
            "profile_ads_time_interval": "100",

            "global_insterstitial_counter": 2000,
            "matches_insterstitial_counter": 2000,
            "interstitial_time_interval": 10,

            "native_time_interval": 10,

            "is_mediation_enabled": "true"
        }
        */

        AdsConfig adsConfig = null;
        if (adsConfigJO != null) {
            adsConfig = new AdsConfig();

            //Enable/Disable flag
            adsConfig.setAdsEnabled(adsConfigJO.optBoolean("is_79_enabled", false));

            //Profile Ads
            adsConfig.setProfileAdEnabled(adsConfigJO.optBoolean("profile_ads_enabled"));
            adsConfig.setmFinalAdPosition(adsConfigJO.optInt("profile_ads_final_position", -1));
            adsConfig.setRepeatEnabled(adsConfigJO.optBoolean("repeat", false));
            adsConfig.setmProfileAdsTimeIntervalInMillis(adsConfigJO.optLong("profile_ads_time_interval", 0L) * 1000);

            //Interstitials and Native Ads
            adsConfig.setmGlobalInterstitialCounter(adsConfigJO.optLong("global_insterstitial_counter", 0L));
            adsConfig.setmMatchesInterstitialCounter(adsConfigJO.optLong("matches_insterstitial_counter", 0L));
            long interstitialTimeInterval = adsConfigJO.optLong("interstitial_time_interval", -1L);
            if (interstitialTimeInterval != -1L) {
                interstitialTimeInterval *= 1000;
            }
            adsConfig.setmInterstitialTimeout(interstitialTimeInterval);
            long nativeTimeInterval = adsConfigJO.optLong("native_time_interval", -1L);
            if (nativeTimeInterval != -1L) {
                nativeTimeInterval *= 1000;
            }
            adsConfig.setmNativeAdTimeout(nativeTimeInterval);

            adsConfig.setMediationEnabled(adsConfigJO.optBoolean("is_mediation_enabled"));

            TmLogger.d(TAG, "ad_flags : " + adsConfigJO.toString());
        } else {
            TmLogger.d(TAG, "ad_flags : null");
        }
        return adsConfig;
    }

    public static void processAdsFlags(Context context, AdsConfig adsConfig){

        //Hard coded enabled ads:
//        adsConfig = new AdsConfig();
//        adsConfig.setAdsEnabled(true);
//        adsConfig.setRepeatEnabled(true);
//        adsConfig.setProfileAdEnabled(true);
//        adsConfig.setmProfileAdsTimeIntervalInMillis(10000);
//        adsConfig.setmFinalAdPosition(3);
//        adsConfig.setmGlobalInterstitialCounter(2000);
//        adsConfig.setmMatchesInterstitialCounter(2000);
//        adsConfig.setmInterstitialTimeout(10*1000);
//        adsConfig.setmNativeAdTimeout(10*1000);

        String userId = Utility.getMyId(context);
        if (adsConfig != null) {

            RFHandler.insert(context, userId,
                    ConstantsRF.RIGID_FILED_IS_79_ADS_ENABLED, adsConfig.isAdsEnabled());
            if(adsConfig.isAdsEnabled()){
                AdsHelper.initialize79SDK(context);
            }else{
                AdsHelper.resetAdsData(context);
                return;
            }

            //Profile Ads
            if(adsConfig.isProfileAdEnabled()) {
                RFHandler.insert(context, userId,
                        ConstantsRF.RIGID_FILED_IS_PROFILE_ADS_ENABLED, adsConfig.isProfileAdEnabled());
                RFHandler.insert(context, userId,
                        ConstantsRF.RIGID_FILED_IS_PROFILE_ADS_REPEATABLE, adsConfig.isRepeatEnabled());
                RFHandler.insert(context, userId,
                        ConstantsRF.RIGID_FILED_IS_PROFILE_ADS_TIME_INTERVAL, adsConfig.getmProfileAdsTimeIntervalInMillis());
                int currentvalue = RFHandler.getInt(context, userId,
                        ConstantsRF.RIGID_FILED_FINAL_PROFILE_AD_POSITION, -1);
                if (currentvalue != adsConfig.getmFinalAdPosition()) {
                    RFHandler.insert(context, userId,
                            ConstantsRF.RIGID_FILED_FINAL_PROFILE_AD_POSITION, adsConfig.getmFinalAdPosition());
                    int currentPosition = SPHandler.getInt(context,
                            ConstantsSP.SHARED_KEYS_MATCHES_BEFORE_AD, -1);
                    if (currentPosition > adsConfig.getmFinalAdPosition()) {
                        SPHandler.setInt(context,
                                ConstantsSP.SHARED_KEYS_MATCHES_BEFORE_AD, adsConfig.getmFinalAdPosition());
                    }
                }
            }else{
                AdsHelper.resetProfileAdsData(context);
            }


            //Interstitial and Native Ads:
            RFHandler.insert(context, userId,
                    ConstantsRF.RIGID_FIELD_INTERSTITIAL_UPPER_CAP,
                    adsConfig.getmGlobalInterstitialCounter());
            RFHandler.insert(context, userId,
                    ConstantsRF.RIGID_FIELD_MATCHES_INTERSTITIAL_UPPER_CAP,
                    adsConfig.getmMatchesInterstitialCounter());
            RFHandler.insert(context, userId,
                    ConstantsRF.RIGID_FIELD_INTERSTITIAL_AD_TIMEINTERVAL,
                    adsConfig.getmInterstitialTimeout());
            RFHandler.insert(context, userId,
                    ConstantsRF.RIGID_FIELD_NATIVE_AD_TIMEINTERVAL,
                    adsConfig.getmNativeAdTimeout());

            //Mediation
            RFHandler.insert(context, userId,
                    ConstantsRF.RIGID_FIELD_79_MEDIATION_ENABLED,
                    adsConfig.isMediationEnabled());
        } else {
            AdsHelper.resetAdsData(context);
        }
    }

//    public static ProfileAdsConfig parseProfileAdsFlags(JSONObject adsConfigJO){
//        ProfileAdsConfig profileAdsConfig = null;
//
//        if (adsConfigJO != null) {
//            profileAdsConfig = new ProfileAdsConfig();
//            profileAdsConfig.setIsProfileAdEnabled(adsConfigJO.optBoolean("profile_ads_enabled"));
//            profileAdsConfig.setmFinalAdPosition(adsConfigJO.optInt("profile_ads_final_position", -1));
//            profileAdsConfig.setIsRepeatEnabled(adsConfigJO.optBoolean("repeat", false));
//            profileAdsConfig.setmAdsTimeIntervalInMillis(adsConfigJO.optLong("profile_ads_time_interval", 0L) * 1000);
//            TmLogger.d(TAG, "ad_flags : " + adsConfigJO.toString());
//        } else {
//            TmLogger.d(TAG, "ad_flags null");
//        }
//        return profileAdsConfig;
//    }
//
//    public static void processProfileAdsFlags(Context context, ProfileAdsConfig profileAdsConfig) {
//        if (profileAdsConfig != null) {
//            RFHandler.insert(context, Utility.getMyId(context),
//                    ConstantsRF.RIGID_FILED_IS_PROFILE_ADS_ENABLED, profileAdsConfig.isProfileAdEnabled());
//            RFHandler.insert(context, Utility.getMyId(context),
//                    ConstantsRF.RIGID_FILED_IS_PROFILE_ADS_REPEATABLE, profileAdsConfig.isRepeatEnabled());
//            RFHandler.insert(context, Utility.getMyId(context),
//                    ConstantsRF.RIGID_FILED_IS_PROFILE_ADS_TIME_INTERVAL, profileAdsConfig.getmAdsTimeIntervalInMillis());
//            int currentvalue = RFHandler.getInt(context, Utility.getMyId(context),
//                    ConstantsRF.RIGID_FILED_FINAL_PROFILE_AD_POSITION, -1);
//
//            if (currentvalue != profileAdsConfig.getmFinalAdPosition()) {
//                RFHandler.insert(context, Utility.getMyId(context),
//                        ConstantsRF.RIGID_FILED_FINAL_PROFILE_AD_POSITION, profileAdsConfig.getmFinalAdPosition());
//                int currentPosition = SPHandler.getInt(context,
//                        ConstantsSP.SHARED_KEYS_MATCHES_BEFORE_AD, -1);
//                if (currentPosition > profileAdsConfig.getmFinalAdPosition()) {
//                    SPHandler.setInt(context,
//                            ConstantsSP.SHARED_KEYS_MATCHES_BEFORE_AD, profileAdsConfig.getmFinalAdPosition());
//                }
//            }
//        } else {
//            RFHandler.insert(context, Utility.getMyId(context),
//                    ConstantsRF.RIGID_FILED_IS_PROFILE_ADS_ENABLED, false);
//            RFHandler.insert(context, Utility.getMyId(context),
//                    ConstantsRF.RIGID_FILED_FINAL_PROFILE_AD_POSITION, -1);
//            RFHandler.insert(context, Utility.getMyId(context),
//                    ConstantsRF.RIGID_FILED_IS_PROFILE_ADS_REPEATABLE, false);
//            RFHandler.insert(context, Utility.getMyId(context),
//                    ConstantsRF.RIGID_FILED_IS_PROFILE_ADS_TIME_INTERVAL, 0L);
//        }
//    }

//    public static InterstitialNativeAdsConfig parseInterstitialNativeAdsFlags(JSONObject interstitialNativeAdsConfigJO) {
//        InterstitialNativeAdsConfig mInterstitialNativeAdsConfig = null;
//
//        if (interstitialNativeAdsConfigJO != null) {
//            mInterstitialNativeAdsConfig = new InterstitialNativeAdsConfig();
//            mInterstitialNativeAdsConfig.setmGlobalInterstitialCounter(interstitialNativeAdsConfigJO.optLong("global_insterstitial_counter", 0L));
//            mInterstitialNativeAdsConfig.setmMatchesInterstitialCounter(interstitialNativeAdsConfigJO.optLong("matches_insterstitial_counter", 0L));
//            mInterstitialNativeAdsConfig.setmConversationsInterstitialCounter(interstitialNativeAdsConfigJO.optLong("conversation_insterstitial_counter", 0L));
//            long interstitialTimeInterval = interstitialNativeAdsConfigJO.optLong("interstitial_time_interval", -1L);
//            if (interstitialTimeInterval != -1L)
//                interstitialTimeInterval *= 1000;
//            mInterstitialNativeAdsConfig.setmInterstitialTimeout(interstitialTimeInterval);
//            long nativeTimeInterval = interstitialNativeAdsConfigJO.optLong("native_time_interval", -1L);
//            if (nativeTimeInterval != -1L)
//                nativeTimeInterval *= 1000;
//            mInterstitialNativeAdsConfig.setmNativeAdTimeout(nativeTimeInterval);
//
//            TmLogger.d(TAG, "native_ad_flags : " + interstitialNativeAdsConfigJO.toString());
//        }else{
//            TmLogger.d(TAG, "native_ad_flags null");
//        }
//        return mInterstitialNativeAdsConfig;
//    }
//
//    public static void processInterstitialNativeAdsFlags(Context aContext, String userId,
//                                                         InterstitialNativeAdsConfig interstitialNativeAdsConfig) {
//        if (interstitialNativeAdsConfig != null) {
//            RFHandler.insert(aContext, userId,
//                    ConstantsRF.RIGID_FIELD_INTERSTITIAL_UPPER_CAP,
//                    interstitialNativeAdsConfig.getmGlobalInterstitialCounter());
//            RFHandler.insert(aContext, userId,
//                    ConstantsRF.RIGID_FIELD_MATCHES_INTERSTITIAL_UPPER_CAP,
//                    interstitialNativeAdsConfig.getmMatchesInterstitialCounter());
//
//            RFHandler.insert(aContext, userId,
//                    ConstantsRF.RIGID_FIELD_INTERSTITIAL_AD_TIMEINTERVAL,
//                    interstitialNativeAdsConfig.getmInterstitialTimeout());
//            RFHandler.insert(aContext, userId,
//                    ConstantsRF.RIGID_FIELD_NATIVE_AD_TIMEINTERVAL,
//                    interstitialNativeAdsConfig.getmNativeAdTimeout());
//        } else {
//            AdsHelper.resetInterstitialNativeAdsData(aContext);
//        }
//    }

}
