package com.trulymadly.android.app.json;

import com.trulymadly.android.app.modal.EventDetailModal;
import com.trulymadly.android.app.modal.LocationModal;
import com.trulymadly.android.app.modal.UserInfoModal;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by deveshbatra on 3/18/16.
 */
public class EventDetailsParser {
    public static EventDetailModal parseEventDetailsResponse(JSONObject response) {

        EventDetailModal eventDetailModal = new EventDetailModal();
        JSONObject eventDetails = response.optJSONObject("event_details");

        if (eventDetailModal != null) {

            eventDetailModal.setId(eventDetails.optString("datespot_id"));
            eventDetailModal.setName(eventDetails.optString("name"));
            eventDetailModal.setFriendlyName(eventDetails.optString("friendly_name"));
//            eventDetailModal.setLocationCount(eventDetails.optInt("location_count"));
            eventDetailModal.setDate(eventDetails.optString("pretty_date"));
//            eventDetailModal.setMultiLocationText(eventDetails.optString("multiple_locations_text"));
//            eventDetailModal.setMultiLocationText(eventDetails.optString("multiple_locations_text"));
            eventDetailModal.setImages(createAttribute(eventDetails.optJSONArray("images")));
            eventDetailModal.setTerms(createAttribute(eventDetails.optJSONArray("terms_and_conditions")));
//            eventDetailModal.setLocations(createLocations(eventDetails.optJSONArray("locations")));
            eventDetailModal.setFollowStatus(eventDetails.optBoolean("follow_status"));
            eventDetailModal.setUsers(createUsersList(eventDetails.optJSONArray("following_users")));
            eventDetailModal.setDescription(eventDetails.optString("details"));
            eventDetailModal.setTicketUrl(eventDetails.optString("event_ticket_url"));
            eventDetailModal.setShareUrl(eventDetails.optString("share_url"));
            eventDetailModal.setShareText(eventDetails.optString("share_text"));
            eventDetailModal.setEventTicketText(eventDetails.optString("event_ticket_text"));
            eventDetailModal.setEventExpired(eventDetails.optBoolean("event_expired"));
            eventDetailModal.setEventTicketImage(eventDetails.optString("event_ticket_image"));
            eventDetailModal.setLocations(createLocations(eventDetails.optJSONArray("locations")));
            eventDetailModal.setMultiLocationText(eventDetails.optString("multiple_locations_text"));
            eventDetailModal.setLikeMindedText(eventDetails.optString("like_minded_text"));

            eventDetailModal.setTMPresents(eventDetails.optBoolean("is_trulymadly"));
            eventDetailModal.setSelectOnly(eventDetails.optBoolean("is_select_only"));
            eventDetailModal.setmPrice(eventDetails.optInt("ticket_price"));
            eventDetailModal.setmPriceDesc(eventDetails.optString("tm_status"));
            eventDetailModal.setmFBUrl(eventDetails.optString("fb_page_url"));

            try {
                JSONObject location = (JSONObject) eventDetails.optJSONArray("locations").get(0);
                eventDetailModal.setLocation(location.optString("location"));
                eventDetailModal.setAddress(location.optString("address"));
                eventDetailModal.setPhoneNumber(location.optString("phone_number"));
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }


        // creating junk data
//        eventDetailModal.setMultiLocationText("Multiple Locations");
//        ArrayList<LocationModal> array = new ArrayList<LocationModal>();
//        for (int i=0;i<4;i++)
//        {
//            LocationModal location= new LocationModal();
//            location.setContactNo("8802463404");
//            location.setAddress("lado sarai");
//            array.add(location);
//
//        }
//        eventDetailModal.setLocations(array);

        return eventDetailModal;


    }

    private static ArrayList<LocationModal> createLocations(JSONArray array) {

        ArrayList<LocationModal> locations = new ArrayList<>();
        for (int i = 0; array != null && i < array.length(); i++) {
            LocationModal location = new LocationModal();
            location.setContactNo(array.optJSONObject(i).optString("phone_number"));
            location.setAddress(array.optJSONObject(i).optString("address"));
            locations.add(location);
        }

        return locations;

    }

    private static ArrayList<UserInfoModal> createUsersList(JSONArray array) {

        JSONObject userJson = new JSONObject();
        ArrayList<UserInfoModal> userList = new ArrayList<>();
        for (int i = 0; array != null && i < array.length(); i++) {

            try {
                UserInfoModal user = new UserInfoModal();
                userJson = (JSONObject) array.get(i);
                user.setName(userJson.optString("fname"));
                user.setId(userJson.optString("user_id"));
                user.setProfilePicLink(userJson.optString("thumbnail"));
                user.setHideLink(userJson.optString("hide_url"));
                user.setLikeLink(userJson.optString("like_url"));
                user.setIsMutualMatch(userJson.optBoolean("isMutualMatch"));
                user.setLikedByMe(userJson.optBoolean("likedByMe"));
                user.setSparkedByMe(userJson.optBoolean("sparkedByMe"));
                user.setHasLiked(userJson.optBoolean("hasLiked"));
                user.setProfile_url(userJson.optString("profile_url"));
                user.setMessageUrl(userJson.optString("message_url"));

                //Hard set liked by me to false.
//                user.setLikedByMe(false);

                userList.add(user);


            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

        return userList;

    }

    private static String[] createAttribute(JSONArray array) {
        ArrayList<String> str = new ArrayList<>();

        if (array != null) {
            for (int i = 0; i < array.length(); i++) {
                str.add(array.optString(i));
            }
        }

        String[] attribute = new String[str.size()];
        attribute = str.toArray(attribute);

        return attribute;
    }

}

