package com.trulymadly.android.app.json;

import com.trulymadly.android.app.modal.CategoryModal;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by deveshbatra on 3/14/16.
 */
public class CategoryListResponseParser {

    public ArrayList<CategoryModal> parseResponse(JSONObject response) {
        ArrayList<CategoryModal> eventList = new ArrayList<>();
        JSONArray array = response.optJSONArray("categories");
        if (array != null) {
            for (int i = 0; i < array.length(); i++) {
                JSONObject eventJSON = array.optJSONObject(i);
                CategoryModal event = new CategoryModal();
                if (eventJSON != null) {
                    event.setId(eventJSON.optString("category_id"));
                    event.setBanner(eventJSON.optString("background_image"));
                    event.setListings(eventJSON.optString("event_count"));
                    event.setName(eventJSON.optString("title"));
                    event.setIcon(eventJSON.optString("icon_image"));
                    event.setNew(eventJSON.optBoolean("new_event"));
                    eventList.add(event);
                }

            }
        }
        return eventList;
    }

}
