package com.trulymadly.android.app.json;

import android.content.Context;

import com.trulymadly.android.app.modal.QuizData;
import com.trulymadly.android.app.sqlite.QuizDBHandler;
import com.trulymadly.android.app.utility.FilesHandler;
import com.trulymadly.android.app.utility.ImageCacheHelper;
import com.trulymadly.android.app.utility.SPHandler;

import org.json.JSONArray;
import org.json.JSONObject;

public class QuizParser {
    private final Context ctx;

    public QuizParser(Context aContext) {
        this.ctx = aContext;
    }

    //@Trace(category = MetricCategory.JSON)
    private QuizData createQuiz(JSONObject quizJsonObject) {
        QuizData quiz = new QuizData();
        if (quizJsonObject != null) {
            quiz.setId(quizJsonObject.optInt("quiz_id"));
            quiz.setName(quizJsonObject.optString("display_name"));
            quiz.setImageUrl(quizJsonObject.optString("image"));
            quiz.setBannerUrl(quizJsonObject.optString("banner_url"));

        }
        return quiz;

    }

    //@Trace(category = MetricCategory.JSON)
    public void insert(JSONObject responseJSON) {
        int current_version = responseJSON
                .optInt("current_version");
        FilesHandler.createImageCacheFolder();
        JSONArray quizzes = responseJSON.optJSONArray("quizzes");
        for (int i = 0; i < quizzes.length(); i++) {
            JSONObject quizJson = quizzes.optJSONObject(i);
            QuizData quizData = createQuiz(quizJson);
            quizData.setUpdatedInVersion(current_version);
            QuizDBHandler.insertQuiz(quizData, ctx);
            writeOnDisk(quizData);
        }
        SPHandler.setInt(ctx,
                ConstantsSP.SHARED_KEYS_QUIZ_VERSION, current_version);
    }

    private void writeOnDisk(final QuizData quizData) {
        downloadImage(quizData, "image");
        downloadImage(quizData, "banner_image");
    }

    private void downloadImage(final QuizData quizData, String type) {
        String memoryLocation, webUrl;
        if (type.equals("image")) {
            webUrl = quizData.getImageUrl();
        } else {
            webUrl = quizData.getBannerUrl();
        }

        ImageCacheHelper.with(ctx).loadWithKey(webUrl, quizData.getImageCacheKey()).fetch();
    }

}
