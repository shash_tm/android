/**
 * 
 */
package com.trulymadly.android.app.json;

import com.crashlytics.android.Crashlytics;
import com.trulymadly.android.app.modal.UserData;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * @author udbhav
 * 
 */
public class ParseUserData {
    //@Trace(category = MetricCategory.JSON)
    public UserData parseRegisterData(JSONObject dataJsonObject) {
		UserData userData = new UserData();

		JSONObject basicsData = dataJsonObject.optJSONObject("basics_data");
		if (basicsData != null) {
			userData.setDataUrl(basicsData.optString("url", null));
		}

		userData.setPage(dataJsonObject.optString("page"));

		JSONObject userDataJsonObject = dataJsonObject.optJSONObject("data");

		if (userDataJsonObject != null) {
			userData.setFname(userDataJsonObject.optString("fname"));
			userData.setLname(userDataJsonObject.optString("lname"));
			
			userData.setIsFb(userDataJsonObject.optString("is_fb_connected")
					.equalsIgnoreCase("Y"));

			String dob = userDataJsonObject.optString("dob", null);
			if (dob != null) {
				String[] dobStrings = dob.split("-");
				if (dobStrings.length == 3) {
					userData.setDob_y(Integer.parseInt(dobStrings[0]));
					userData.setDob_m(Integer.parseInt(dobStrings[1]));
					userData.setDob_d(Integer.parseInt(dobStrings[2]));
				}
			}

			JSONObject locationJsonObject = userDataJsonObject
					.optJSONObject("location");
			if (locationJsonObject != null) {
				JSONObject tempLocationJsonObject = locationJsonObject
						.optJSONObject("country");
				if (tempLocationJsonObject != null)
					userData.setCountryId(tempLocationJsonObject.optString(
							"country_id", String.valueOf(Constants.COUNTRY_ID_INDIA)));

				tempLocationJsonObject = locationJsonObject
						.optJSONObject("state");
				if (tempLocationJsonObject != null)
					userData.setStateId(tempLocationJsonObject.optString(
							"state_id", "0"));

				tempLocationJsonObject = locationJsonObject
						.optJSONObject("city");
				if (tempLocationJsonObject != null)
					userData.setCityId(tempLocationJsonObject.optString(
							"city_id", "0"));
			}

			JSONObject work_details = userDataJsonObject
					.optJSONObject("work_details");
			if (work_details != null) {
				if (!work_details.isNull("designation")) {
					userData.setCurrentTitle(work_details.optString(
							"designation", null));
				}
				userData.setCompanies(getArrayListFromString(work_details
						.optString("companies", null)));
			}

			JSONObject instituteDetailsJsonObject = userDataJsonObject
					.optJSONObject("institute_details");
			if (instituteDetailsJsonObject != null) {
				userData.setColleges(getArrayListFromString(instituteDetailsJsonObject
						.optString("institute", null)));
			}
		}

		return userData;
	}

    //@Trace(category = MetricCategory.JSON)
    public UserData parseEditData(JSONObject dataJsonObject) {
		UserData userData = new UserData();

		JSONObject basicsData = dataJsonObject.optJSONObject("basics_data");
		if (basicsData != null) {
			userData.setDataUrl(basicsData.optString("url", null));
		}
		
		userData.setStatus(dataJsonObject.optString("status"));

		JSONObject userDataJsonObject = dataJsonObject
				.optJSONObject("user_data");

		
		if (userDataJsonObject != null) {
			userData.setFname(userDataJsonObject.optString("fname"));
			userData.setLname(userDataJsonObject.optString("lname"));
			
			String dob = userDataJsonObject.optString("DateOfBirth", null);
			if (dob != null) {
				String[] dobStrings = dob.split("-");
				if (dobStrings.length == 3) {
					userData.setDob_y(Integer.parseInt(dobStrings[0]));
					userData.setDob_m(Integer.parseInt(dobStrings[1]));
					userData.setDob_d(Integer.parseInt(dobStrings[2]));
				}
			}

			userData.setCountryId(userDataJsonObject.optString("stay_country",
					String.valueOf(Constants.COUNTRY_ID_INDIA)));
			userData.setStateId(userDataJsonObject.optString("stay_state", "0"));
			userData.setCityId(userDataJsonObject.optString("stay_city",
					Constants.LOCATION_OTHER));

			userData.setHeight(userDataJsonObject.optString("height"));
			userData.setHeight_feet(userDataJsonObject.optString("height_foot"));
			userData.setHeight_inches(userDataJsonObject
					.optString("height_inch"));

			userData.setIncome(userDataJsonObject.optString("income_start")
					+ "-" + userDataJsonObject.optString("income_end"));

			if (Utility.isSet(userDataJsonObject.optString("designation"))) {
				userData.setCurrentTitle(userDataJsonObject
						.optString("designation"));
			} else {
				userData.setCurrentTitle(null);
			}
			if (Utility.isSet(userDataJsonObject.optString("company_name"))) {
				userData.setCompanies(getArrayListFromString(userDataJsonObject
						.optString("company_name")));
			} else {
				userData.setCompanies(null);
			}

			userData.setIsNotWorking(userDataJsonObject
					.optString("work_status").equalsIgnoreCase("no"));

			userData.setHighestDegree(userDataJsonObject
					.optString("highest_degree"));

			if (Utility
					.isSet(userDataJsonObject.optString("institute_details"))) {
				userData.setColleges(getArrayListFromString(userDataJsonObject
						.optString("institute_details")));
			} else {
				userData.setColleges(null);
			}

			if (Utility.isSet(userDataJsonObject.optString("hashtags"))) {
				userData.setInterests(getArrayListFromString(userDataJsonObject
						.optString("hashtags")));
			} else {
				userData.setInterests(null);
			}

		}

		return userData;
	}

    //@Trace(category = MetricCategory.JSON)
    private ArrayList<String> getArrayListFromString(String optString) {

		if (Utility.isSet(optString )) {
			JSONArray array;
			try {
				// array = new JSONArray(URLDecoder.decode(optString, "UTF-8"));
				array = new JSONArray(optString);
				ArrayList<String> list = new ArrayList<>();
				for (int i = 0; i < array.length(); i++) {
					if(Utility.isSet(array.optString(i))){
						list.add(array.optString(i));
					}
				}
				if(list.size()>0){
					return list;
				} else {
					return null;
				}
			} catch (JSONException e) {
				 Crashlytics.logException(e);
			}	
		}
		return null;
	}

}
