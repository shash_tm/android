package com.trulymadly.android.app.json;

import android.content.Context;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.trulymadly.android.app.utility.OkHttpHandler;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.Utility;
import com.trulymadly.android.socket.SocketHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Created by udbhav on 07/01/16.
 */
public class ConstantsSocket {
    public static final String SOCKET_URL_DEFAULT = (Constants.isLive
            ? "https://chat2.trulymadly.com:443"
            : (Constants.isT1 ? "https://t2chat.trulymadly.com:8080"
            : "http://chat_t.trulymadly.com:8080"));
    public static final String SOCKET_IP_LIST_DEFAULT = (Constants.isLive
            ? "http://54.169.234.204:80,http://54.251.147.193:80"
            : (Constants.isT1 ? "http://52.77.216.80:8080"
            : "http://54.169.222.210:8080"));
    public static final boolean SOCKET_TRANPORT_ONLY_WEBSOCKET_BOOL_DEFAULT = true;
    public static final int SOCKET_CONNECTION_TIMEOUT_DEFAULT_SECONDS = 25;
    /* Defaults Chat constants */
    // dev : 52.74.40.23 -> 52.74.103.245 -> 54.169.28.196
    // t1 : 52.74.15.38 -> 52.74.130.184 -> 52.76.19.195
    // live: http://chat.trulymadly.com -> http://chat1.trulymadly.com ||
    public static final int MAX_RETRY_ATTEMPT_DEFAULT = 1;
    public static final int EMIT_THRESHOLD_TIMEOUT_GMM_DEFAULT_SECONDS = 15;
    public static final int EMIT_THRESHOLD_TIMEOUT_CHAT_SENT_DEFAULT_SECONDS = 10;
    private static final int POLLING_EXPIRATION_TIMEOUT_DEFAULT_SECONDS = 10
            * 60;
    private static final int SOCKET_RECONNECTION_DELAY_DEFAULT_SECONDS = 2;
    private static final int SOCKET_RECONNECTION_DELAY_MAX_DEFAULT_SECONDS = 2;
    private static final int EMIT_THRESHOLD_TIMEOUT_DEFAULT_SECONDS = 60;
    private static final int EMIT_THRESHOLD_TIMEOUT_FOR_PING_DEFAULT_SECONDS = 5;
    private static final int EMIT_THRESHOLD_TIMEOUT_FOR_PING_2G_DEFAULT_SECONDS = 15;
    private static final int EMIT_THRESHOLD_TIMEOUT_CHAT_TYPING_DEFAULT_SECONDS = 2000;
    /**
     * Static constants
     */
    public static String SOCKET_URL = SOCKET_URL_DEFAULT;
    public static String SOCKET_IP_LIST = SOCKET_IP_LIST_DEFAULT;
    public static boolean SOCKET_TRANPORT_ONLY_WEBSOCKET_BOOL = SOCKET_TRANPORT_ONLY_WEBSOCKET_BOOL_DEFAULT;
    public static int POLLING_EXPIRATION_TIMEOUT = POLLING_EXPIRATION_TIMEOUT_DEFAULT_SECONDS
            * 1000;
    public static int SOCKET_CONNECTION_TIMEOUT = SOCKET_CONNECTION_TIMEOUT_DEFAULT_SECONDS
            * 1000;
    public static int EMIT_THRESHOLD_TIMEOUT = EMIT_THRESHOLD_TIMEOUT_DEFAULT_SECONDS
            * 1000;
    public static int EMIT_THRESHOLD_TIMEOUT_GMM = EMIT_THRESHOLD_TIMEOUT_GMM_DEFAULT_SECONDS
            * 1000;
    public static int EMIT_THRESHOLD_TIMEOUT_CHAT_SENT = EMIT_THRESHOLD_TIMEOUT_CHAT_SENT_DEFAULT_SECONDS
            * 1000;
    public static int EMIT_THRESHOLD_TIMEOUT_FOR_PING = EMIT_THRESHOLD_TIMEOUT_FOR_PING_DEFAULT_SECONDS
            * 1000;
    public static int EMIT_THRESHOLD_TIMEOUT_FOR_PING_2G = EMIT_THRESHOLD_TIMEOUT_FOR_PING_2G_DEFAULT_SECONDS
            * 1000;
    public static int EMIT_THRESHOLD_TIMEOUT_CHAT_TYPING = EMIT_THRESHOLD_TIMEOUT_CHAT_TYPING_DEFAULT_SECONDS
            * 1000;
    private static int MAX_RETRY_ATTEMPT = MAX_RETRY_ATTEMPT_DEFAULT;
    private static int SOCKET_RECONNECTION_DELAY = SOCKET_RECONNECTION_DELAY_DEFAULT_SECONDS
            * 1000;
    private static int SOCKET_RECONNECTION_DELAY_MAX = SOCKET_RECONNECTION_DELAY_MAX_DEFAULT_SECONDS
            * 1000;

    public static void setUpConstants(Context ctx) {
        SOCKET_URL = getConstant(ctx, ConstantsSP.SHARED_KEYS_SOCKET_URL, SOCKET_URL, SOCKET_URL_DEFAULT);
        SOCKET_IP_LIST = getConstant(ctx, ConstantsSP.SHARED_KEYS_SOCKET_IP_LIST, SOCKET_IP_LIST, SOCKET_IP_LIST_DEFAULT);
        Crashlytics.log(Log.DEBUG, SocketHandler.tag, "SOCKET_IP_LIST : " + SOCKET_IP_LIST);
        SOCKET_TRANPORT_ONLY_WEBSOCKET_BOOL = getConstant(ctx,
                ConstantsSP.SHARED_KEYS_SOCKET_TRANPORT_ONLY_WEBSOCKET_BOOL,
                SOCKET_TRANPORT_ONLY_WEBSOCKET_BOOL);
        POLLING_EXPIRATION_TIMEOUT = getConstant(ctx,
                ConstantsSP.SHARED_KEYS_POLLING_EXPIRATION_TIMEOUT,
                POLLING_EXPIRATION_TIMEOUT);
        SOCKET_CONNECTION_TIMEOUT = getConstant(ctx,
                ConstantsSP.SHARED_KEYS_SOCKET_CONNECTION_TIMEOUT,
                SOCKET_CONNECTION_TIMEOUT);
        MAX_RETRY_ATTEMPT = getConstant(ctx, ConstantsSP.SHARED_KEYS_MAX_RETRY_ATTEMPT,
                MAX_RETRY_ATTEMPT);
        SOCKET_RECONNECTION_DELAY = getConstant(ctx,
                ConstantsSP.SHARED_KEYS_SOCKET_RECONNECTION_DELAY,
                SOCKET_RECONNECTION_DELAY);
        SOCKET_RECONNECTION_DELAY_MAX = getConstant(ctx,
                ConstantsSP.SHARED_KEYS_SOCKET_RECONNECTION_DELAY_MAX,
                SOCKET_RECONNECTION_DELAY_MAX);
        EMIT_THRESHOLD_TIMEOUT = getConstant(ctx,
                ConstantsSP.SHARED_KEYS_EMIT_THRESHOLD_TIMEOUT,
                EMIT_THRESHOLD_TIMEOUT);
        EMIT_THRESHOLD_TIMEOUT_GMM = getConstant(ctx,
                ConstantsSP.SHARED_KEYS_EMIT_THRESHOLD_TIMEOUT_GMM,
                EMIT_THRESHOLD_TIMEOUT_GMM);
        EMIT_THRESHOLD_TIMEOUT_CHAT_SENT = getConstant(ctx,
                ConstantsSP.SHARED_KEYS_EMIT_THRESHOLD_TIMEOUT_CHAT_SENT,
                EMIT_THRESHOLD_TIMEOUT_CHAT_SENT);
        EMIT_THRESHOLD_TIMEOUT_FOR_PING = getConstant(ctx,
                ConstantsSP.SHARED_KEYS_EMIT_THRESHOLD_TIMEOUT_FOR_PING,
                EMIT_THRESHOLD_TIMEOUT_FOR_PING);
        EMIT_THRESHOLD_TIMEOUT_FOR_PING_2G = getConstant(ctx,
                ConstantsSP.SHARED_KEYS_EMIT_THRESHOLD_TIMEOUT_FOR_PING_2G,
                EMIT_THRESHOLD_TIMEOUT_FOR_PING_2G);
        EMIT_THRESHOLD_TIMEOUT_CHAT_TYPING = getConstant(ctx,
                ConstantsSP.SHARED_KEYS_EMIT_THRESHOLD_TIMEOUT_CHAT_TYPING,
                EMIT_THRESHOLD_TIMEOUT_CHAT_TYPING);
    }

    private static String getConstant(Context aContext, String key, String curVal, String defaultVal) {
        String savedVal = SPHandler.getString(aContext, key);
        if (!Utility.isSet(savedVal)) {
            if (Utility.isSet(curVal)) {
                SPHandler.setString(aContext, key, curVal);
            } else {
                curVal = defaultVal;
            }
        } else {
            curVal = savedVal;
        }
        return curVal;
    }

    private static int getConstant(Context aContext, String key, int curVal) {
        if (!SPHandler.isExists(aContext, key)) {
            SPHandler.setInt(aContext, key, curVal);
        } else {
            curVal = SPHandler.getInt(aContext, key);
        }
        return curVal;
    }

    private static boolean getConstant(Context aContext, String key, boolean curVal) {
        if (!SPHandler.isExists(aContext, key)) {
            SPHandler.setBool(aContext, key, curVal);
        } else {
            curVal = SPHandler.getBool(aContext, key);
        }
        return curVal;
    }

    public static String getRandomSocketIpFromCommaString(String socketIpList) {
        if (!Utility.isSet(socketIpList)) {
            return null;
        }
        List<String> items = Arrays.asList(socketIpList.split("\\s*,\\s*"));
        int len = items.size();
        if (len > 0) {
            if (len > 1) {
                return items.get((new Random()).nextInt(len));
            } else {
                return items.get(0);
            }
        }
        return null;
    }

    public static String getQueryHeader(Context aContext) {
        JSONObject header = new JSONObject();
        JSONArray cookies = new JSONArray();
        JSONObject cookiePhpsessId = new JSONObject();
        try {
            cookiePhpsessId.put("key", "PHPSESSID");
            cookiePhpsessId.put("value",
                    OkHttpHandler.getCookieByName(aContext, "PHPSESSID"));

            cookies.put(cookiePhpsessId);
            header.put("cookie", cookies);
            header.put("device_id", Utility.getDeviceId(aContext));
            header.put("app_version_code", Utility.getAppVersionName(aContext));
            header.put("os_version_code", Utility.getOsVersion());
            header.put("source", "android_app");

        } catch (JSONException ignored) {
        }
        Crashlytics.log(Log.DEBUG, SocketHandler.tag, header.toString());
        return "header=" + header.toString();
    }

    public enum SOCKET_STATE {
        CONNECTING, CONNECTED, POLLING, FAILED
    }

    public enum SOCKET_START_TYPE {
        NORMAL, IP_FALLBACK_ON_ERROR, ACK_TIMED_OUT
    }

    public class SOCKET_EMITS {
        public static final String get_missed_messages = "get_missed_messages";
        public static final String get_user_metadata = "get_user_metadata";
        public static final String get_chat_metadata = "get_chat_metadata";
        public static final String chat_read = "chat_read";
        public static final String chat_sent = "chat_sent";
        public static final String chat_sent_array = "chat_sent_array";
        public static final String ping = "ping";
        public static final String chat_typing = "chat_typing";
    }

    public class SOCKET_END {
        public static final String logout = "logout";
        public static final String disabled = "disabled";
        public static final String no_network = "no_network";
        public static final String restart_on_login = "login-restarting";
        public static final String force_restart_on_connectivity_change = "connectivity_change-restarting";
        public static final String force_restart_on_chat_sent_failed = "failed_chat_sent-restarting";
        public static final String force_restart_on_ping_failed = "ping_failed-restarting";
        public static final String force_restart_on_server_emit_restart = "server_emit_restart-restarting";
        public static final String force_restart_user_flags_change = "force_restart_user_flags_change";
        public static final String network_class_change = "network_class_change-restarting";
        public static final String failed_reconnections_limit_reached = "failed_reconnections-switching_to_polling";
        public static final String get_missed_messages_failed = "failed_get_missed_messages-switching_to_polling";
        public static final String exception502 = "java.io.IOException: 502 - switching_to_polling";
        public static final String service_idle_timeout = "service_idle_timeout";
        public static final String error_1 = "error_1";
        public static final String ack_timed_out_1 = "ack_timed_out_1";
        public static final String ack_timed_out_2_ack_timed_out = "ack_timed_out_2_ack_timed_out";
        public static final String ack_timed_out_2_error = "ack_timed_out_2_error";
        public static final String error_2_ack_timed_out = "error_2_ack_timed_out";
        public static final String error_2_error = "error_2_error";
    }
}
