package com.trulymadly.android.app.json;

import com.crashlytics.android.Crashlytics;

import org.json.JSONException;
import org.json.JSONObject;

class CheckJSONObject {
	static boolean isJSONStringValid(String parseData)
	{
		boolean isValid=false;
		try {
			new JSONObject(parseData);
			isValid = true;
		}
		catch(JSONException e) {
			 Crashlytics.logException(e);
			isValid = false;
		}
		return isValid;
	}

}
