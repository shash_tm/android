package com.trulymadly.android.app.json;

import com.crashlytics.android.Crashlytics;
import com.trulymadly.android.app.modal.EditPreferencePrefillModelNew;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

public class EditPreferencePrefillNewResponseParser {

    public static EditPreferencePrefillModelNew parseEditPreferencePrefillResponse(
            String parseData) {

        if (CheckJSONObject.isJSONStringValid(parseData) && parseData != null) {
            JSONObject jsonEditPreferencePrefillResponse;

            EditPreferencePrefillModelNew aEditPreferencePrefillModelNew = null;

            String basics_data = "", user_id = "", start_age = "", end_age = "", marital_status = "", location_preference_for_matching = "", countries = "", states = "", cities = "", start_height = "", end_height = "", working_area = "", industry = "", education = "", income_start = "", income_end = "", income_details = "";
            boolean show_us_profiles = false, us_profiles_enabled = false;

            String income = null;

            try {
                jsonEditPreferencePrefillResponse = new JSONObject(parseData);

                us_profiles_enabled = jsonEditPreferencePrefillResponse.optBoolean("us_profiles_enabled", us_profiles_enabled);
                show_us_profiles = jsonEditPreferencePrefillResponse.optBoolean("show_us_profiles", show_us_profiles);

                if (!jsonEditPreferencePrefillResponse.isNull("basics_data")) {

                    JSONObject basicDataJSON = jsonEditPreferencePrefillResponse
                            .optJSONObject("basics_data");
                    basics_data = basicDataJSON.optString("url");
                }

                if (!jsonEditPreferencePrefillResponse.isNull("demo")) {

                    JSONObject jsonDemo = jsonEditPreferencePrefillResponse
                            .getJSONObject("demo");
                    user_id = jsonDemo.optString("user_id");
                    start_age = jsonDemo.optString("start_age");
                    end_age = jsonDemo.optString("end_age");

					marital_status = jsonDemo.optString("marital_status_new");
					location_preference_for_matching = jsonDemo
							.optString("location_preference_for_matching");
					countries = jsonDemo.optString("countries");
					if(!Utility.isSet(countries)){
						countries = String.valueOf(Constants.COUNTRY_ID_INDIA);
					}
					states = jsonDemo.optString("states");
					cities = jsonDemo.optString("cities");
				}
				if (!jsonEditPreferencePrefillResponse.isNull("trait")) {

                    JSONObject jsonTrait = jsonEditPreferencePrefillResponse
                            .getJSONObject("trait");

                    start_height = jsonTrait.optString("start_height");
                    end_height = jsonTrait.optString("end_height");
                    income_details = jsonTrait.optString("income_details");
                }
                if (!jsonEditPreferencePrefillResponse.isNull("work")) {

                    JSONObject jsonWork = jsonEditPreferencePrefillResponse
                            .getJSONObject("work");

                    working_area = jsonWork.optString("working_area");
                    industry = jsonWork.optString("industry");
                    education = jsonWork.optString("education");
                    income_start = jsonWork.optString("income_start");
                    income_end = jsonWork.optString("income_end");

                    if (!jsonWork.isNull("income")) {
                        String inc = jsonWork.optString("income");
                        inc = inc.replace("[", "");
                        inc = inc.replace("]", "");
                        inc = inc.replaceAll("\"", "");
                        income = inc;
                    }
                }

                aEditPreferencePrefillModelNew = new EditPreferencePrefillModelNew(
                        basics_data, user_id, start_age, end_age,
                        marital_status, location_preference_for_matching,
                        countries, states, cities, start_height, end_height,
                        working_area, industry, education, income_start,
                        income_end, income_details, income, show_us_profiles, us_profiles_enabled);
                return aEditPreferencePrefillModelNew;
            } catch (JSONException e) {
                Crashlytics.logException(e);
                return null;
            }
        } else {
            return null;
        }
    }
}