package com.trulymadly.android.app.json;

import com.trulymadly.android.app.modal.DateSpotZone;
import com.trulymadly.android.app.modal.FollowerModal;
import com.trulymadly.android.app.modal.VenueModal;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by deveshbatra on 3/14/16.
 */

public class VenueListResponseParser {

    private ArrayList<VenueModal> venueList;

    //    public ArrayList<VenueModal> getFollowingVenueList() {
//        return followingVenueList;
//    }
//
//    public void setFollowingVenueList(ArrayList<VenueModal> followingVenueList) {
//        this.followingVenueList = followingVenueList;
//    }
//    private ArrayList<VenueModal> followingVenueList;
    private ArrayList<DateSpotZone> zoneList;
    private String city_name;

    public String getCity_name() {
        return city_name;
    }

    public ArrayList<VenueModal> getVenueList() {
        return venueList;
    }


    public ArrayList<DateSpotZone> getZoneList() {
        return zoneList;
    }


    private ArrayList<FollowerModal> parserFollowers(JSONArray jsonArray) {
        ArrayList<FollowerModal> followers = new ArrayList<>();
        if (jsonArray == null || jsonArray.length() == 0)
            return null;

        for (int i = 0; i < jsonArray.length() && i < 3; i++) {
            try {
                JSONObject obj = (JSONObject) jsonArray.get(i);
                FollowerModal follower = new FollowerModal();
                follower.setId(obj.optString("user_id"));
                follower.setImage(obj.optString("image_url"));
                followers.add(follower);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return followers;

    }

    public void parseResponse(JSONObject response) {
        venueList = new ArrayList<>();
//        followingVenueList = new ArrayList<>();
        zoneList = new ArrayList<>();
        city_name = response.optString("city_name");
        JSONArray array = response.optJSONArray("events");
        if (array != null) {
            for (int i = 0; i < array.length(); i++) {
                JSONObject venueJSON = array.optJSONObject(i);
                VenueModal venue = new VenueModal();
                if (venueJSON != null) {
                    venue.setNewEvent(venueJSON.optBoolean("new_event"));
                    venue.setTMEvent(venueJSON.optBoolean("is_trulymadly"));
                    venue.setId(venueJSON.optString("datespot_id"));
                    venue.setBanner(venueJSON.optString("image"));
                    venue.setName(venueJSON.optString("name"));
                    venue.setMutualMatchMessage(venueJSON.optString("mutualCount"));
                    venue.setLocation(venueJSON.optString("location"));
                    venue.setGoingStatus(venueJSON.optBoolean("going_status"));
                    String dist = venueJSON.optString("geo_location_distance"), unit = venueJSON.optString("distance_unit");
                    if (Utility.isSet(dist) && Utility.isSet(unit)) {
                        venue.setDistance(dist + " " + unit);
                    }
                    venue.setDate(venueJSON.optString("pretty_date"));
                    venue.setMultipleLocationText(venueJSON.optString("multiple_locations_text"));
//                    venue.setMultipleLocationText("Multiple Locations");
                    venue.setFollowers(parserFollowers(venueJSON.optJSONArray("mutual_matches")));
                    venue.setNoOfFollowers(venueJSON.optString("user_count"));
                    venue.setmTMMessage(venueJSON.optString("tm_status"));
                    venueList.add(venue);
                }

            }
        }
        array = response.optJSONArray("zones");
        if (array != null) {
            for (int i = 0; i < array.length(); i++) {
                JSONObject zoneJSON = array.optJSONObject(i);
                if (zoneJSON != null) {
                    DateSpotZone zone = new DateSpotZone();
                    zone.setZoneId(zoneJSON.optString("zone_id"));
                    zone.setName(zoneJSON.optString("name"));
                    zone.setIsSelected(zoneJSON.optBoolean("selected"));
                    zoneList.add(zone);
                }
            }

        }

    }


}
