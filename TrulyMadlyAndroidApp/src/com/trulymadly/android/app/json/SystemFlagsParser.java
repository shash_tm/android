package com.trulymadly.android.app.json;

import com.trulymadly.android.app.modal.SystemFlags;

import org.json.JSONObject;

/**
 * Created by avin on 03/12/15.
 */
class SystemFlagsParser {
    //@Trace(category = MetricCategory.JSON)
    public static SystemFlags parseSystemFlags(JSONObject systemFlagsJO){
        SystemFlags systemFlags = null;
        if(systemFlagsJO != null){
            systemFlags = new SystemFlags();
            systemFlags.setIsSocketEnabled(systemFlagsJO.optBoolean("is_socket_enabled", false));
            systemFlags.setIsSocketDebugEnabled(systemFlagsJO.optBoolean("socket_debug_flag", false));
        }
        return systemFlags;
    }
}
