package com.trulymadly.android.app.json;

/**
 * Created by udbhav on 24/05/16.
 */
public class ConstantsRF {
    //Interstitial Ads
    public static final String RIGID_FIELD_INTERSTITIAL_COUNTER = "INTERSTITIAL_AD_COUNTER";
    public static final String RIGID_FIELD_INTERSTITIAL_MATCHES_COUNTER = "MATCHES_INTERSTITIAL_AD_COUNTER";
//    public static final String RIGID_FIELD_INTERSTITIAL_CONVERSATION_COUNTER = "CONVERSATION_INTERSTITIAL_AD_COUNTER";
    public static final String RIGID_FIELD_INTERSTITIAL_UPPER_CAP = "INTERSTITIAL_AD_UPPER_CAP";
    public static final String RIGID_FIELD_MATCHES_INTERSTITIAL_UPPER_CAP = "MATCHES_INTERSTITIAL_AD_UPPER_CAP";
//    public static final String RIGID_FIELD_CONVERSATION_INTERSTITIAL_UPPER_CAP = "CONVERSATION_INTERSTITIAL_AD_UPPER_CAP";
    public static final String RIGID_FIELD_LAST_INTERSTITIAL_AD_TIMESTAMP = "LAST_INTERSTITIAL_AD_TIMESTAMP";

    //Native Ads - Conversation List Item
    public static final String RIGID_FIELD_LAST_NATIVE_AD_TIMESTAMP = "LAST_NATIVE_AD_TIMESTAMP";

    public static final String RIGID_FIELD_LAST_MATCHES_CALL_TSTAMP = "RIGID_FIELD_LAST_MATCHES_CALL_TSTAMP";
    public static final String RIGID_FIELD_LAST_UPLOAD_PIC_AFTER_LIKE_TSTAMP = "RIGID_FIELD_LAST_UPLOAD_PIC_AFTER_LIKE_TSTAMP";
    public static final String RIGID_FIELD_UPLOAD_FIRST_TIME_TSTAMP = "RIGID_FIELD_UPLOAD_FIRST_TIME_TSTAMP";
    public static final String RIGID_FIELD_MATCHES_CALL_INTERVAL = "RIGID_FIELD_MATCHES_CALL_INTERVAL";
    public static final String RIGID_FIELD_NO_OF_MATCHES = "RIGID_FIELD_NO_OF_MATCHES";
    public static final String RIGID_FIELD_HAS_MORE_MATCHES = "RIGID_FIELD_HAS_MORE_MATCHES";
    public static final String RIGID_FIELD_CD_ENABLED = "curated_deals_enabled";
    public static final String RIGID_FIELD_CD_PHONE_NUMBER_TAKEN = "phone_number_taken";
    public static final String RIGID_FIELD_CD_CHAT_TUTORIAL_SHOWN = "cd_chat_tutorial_shown";
    public static final String RIGID_FIELD_DATESPOT_PREFERENCE_TUTORIAL_SHOWN = "datespot_preference_tutorial_shown";
    public static final String RIGID_FIELD_SHARE_PROFILE_TUTORIAL_SHOWN = "share_profile_tutorial_shown";
    public static final String RIGID_FIELD_SHOW_PHOTO_ON_MISS_TM = "SHOW_PHOTO_ON_MISS_TM";
    public static final String RIGID_FIELD_ALLOW_PHOTO_IN_CHAT = "ALLOW_PHOTO_IN_CHAT";
    public static final String RIGID_FIELD_UNREAD_COUNTER_HACK = "UNREAD_COUNTER_HACK";
    public static final String RIGID_FIELD_SOCIALS_TUTORIAL_SHOWN = "socials_tutorial_shown";
    public static final String RIGID_FIELD_FOLLOW_TUTORIAL_SHOWN_COUNT = "follow_tutorial_shown";
    public static final String RIGID_FIELD_AFTER_FOLLOW_TUTORIAL_SHOWN = "after_follow_tutorial_shown";
    public static final String RIGID_FIELD_LAST_CONV_COUNT = "last_conv_count";
    public static final String RIGID_FIELD_FIRST_HIDE_SINGLE_MATCH = "hide_single_match";
    public static final String RIGID_FIELD_ALLOW_MEETUPS = "allow_meetups";
    public static final String RIGID_FIELD_LAST_SOFT_UPDATE_SHOWN_TSTAMP = "last_soft_update_shown_tstamp";
    // Ads Constants
//	public static final String INMOBI_ACCOUNT_ID = "d2770f1b5aa34fd09e88b60b92f77a6f";
//	public static final long INMOBI_PLACEMENT_MATCH_ID = 1446951061310l;
//	public static final long INMOBI_PLACEMENT_CONVERSATIONS_ITEM_ID = 1443670453462l;
//	public static final long INMOBI_PLACEMENT_INTERSTITIAL_ID = 1447540785369l;
    public static final String RIGID_FIELD_79_MEDIATION_ENABLED = "79_MEDIATION_ENABLED";
    public static final String RIGID_FIELD_NATIVE_AD_TIMEINTERVAL = "NATIVE_AD_TIMEINTERVAL";
    public static final String RIGID_FIELD_INTERSTITIAL_AD_TIMEINTERVAL = "INTERSTITIAL_AD_TIMEINTERVAL";
    public static final String RIGID_FIELD_HIDE_FIRST_TIME = "likeHideFirstTime";
    public static final String RIGID_FIELD_LIKE_HIDE_FIRST_TIME = "likeHideFirstTimeTutorial";
    public static final String RIGID_FIELD_VOUCHER_FIRST_TIME = "voucherFirstTimeTutorial";
    public static final String RIGID_FIELD_PROFILE_SHARED = "profileShared_"; // append profile id here.
    public static final String RIGID_FIELD_CONVERSATION_VIEW_FIRST_TIME = "conversationViewFirstTime";
    public static final String RIGID_FIELD_CONVERSATION_LIST_DB_MIGRATED = "conversationListDbMigrated"; // append user id here.
    public static final String RIGID_FIELD_LIKED_FIRST_TIME = "likedFirstTime";
    public static final String RIGID_FIELD_MATCHES_SHOWN_FIRST_TIME = "matchesShownFirstTime";
    //PI Nudge related

    public static final String AB_TRACKING_KEY = "ab_tracking_key";
    public static final String RIGID_FIELD_TM_SCENES_AB_ENABLED = "tm_scenes_ab_enabled";
    public static final String RIGID_FIELD_TM_SCENES_CATEGORY_ID = "tm_scenes_category_id";
    public static final String RIGID_FIELD_PI_NUDGE_COUNTER = "pi_nudge_counter";
    public static final String RIGID_FIELD_PI_NUDGE_SHOWN_TS = "pi_nudge_timestamp";
    public static final String RIGID_FIELD_CONVERSATIONS_HIDDEN = "IS_CONVERSATIONS_HIDDEN";
    public static final String RIGID_FIELD_HIDE_CONVERSATIONS_PASSCODE = "HIDE_CONVERSATIONS_PASSCODE";
    public static final String RIGID_FIELD_CONVERSATIONS_PASSCODE_REQUESTED_TIMESTAMP = "HIDE_CONVERSATIONS_PASSCODE_REQUESTED_TIMESTAMP";
    public static final String RIGID_FIELD_IS_PASSCODE_PROTECTION_ENABLED = "IS_PASSCODE_PROTECTION_ENABLED";
    //Last active
    public static final String RIGID_FIELD_TM_SCENES_CURRENT_TSTAMP = "CURRENT_TSTAMP_TM_SCENES";
    public static final String RIGID_FIELD_TM_SCENES_NEXT_TSTAMP = "NEXT_TSTAMP_TM_SCENES";
    public static final String RIGID_FIELD_LAST_ACTIVE = "LAST_ACTIVE";
    public static final String RIGID_FIELD_GA_QUEUE_TIMESTAMP = "GA_QUEUE_TIMESTAMP";
    public static final String RIGID_FIELD_IS_MISS_TM_CREATED = "IS_MISS_TM_CREATED";
    public static final String RIGID_FIELD_CURATED_DEALS_CD_ASK_UPDATE_CHECKED = "CURATED_DEALS_CD_ASK_UPDATE_CHECKED";
    public static final String RIGID_FIELD_FIRST_SET_TUTORIALS_OVER = "FIRST_SET_TUTORIALS_OVER";
    //Ads Framework - 79
    public static final String RIGID_FILED_IS_79_ADS_ENABLED = "IS_79_ADS_ENABLED";

    public static final String RIGID_FIELD_LAST_PROFILE_AD_TS = "LAST_PROFILE_AD_TS";
    public static final String RIGID_FILED_IS_PROFILE_ADS_TIME_INTERVAL = "IS_PROFILE_ADS_TIME_INTERVAL";
    public static final String RIGID_FILED_IS_PROFILE_ADS_REPEATABLE = "IS_PROFILE_ADS_REPEATABLE";
    public static final String RIGID_FILED_IS_PROFILE_ADS_ENABLED = "IS_PROFILE_ADS_ENABLED";
    public static final String RIGID_FILED_FINAL_PROFILE_AD_POSITION = "FINAL_PROFILE_AD_POSITION";
    public static final String RIGID_FIELD_RATING_POPUP_SHOWN = "RIGID_FIELD_RATING_POPUP_SHOWN";

    //Sparks
    public static final String RIGID_FIELD_SPARK_COUNTER = "RIGID_FIELD_SPARK_COUNTER";
    public static final String RIGID_FIELD_SPARK_ENABLED = "RIGID_FIELD_SPARK_ENABLED";
    public static final String RIGID_FIELD_SENT_SPARKS_SHOWN_ONCE = "RIGID_FIELD_SENT_SPARKS_SHOWN_ONCE";


    // Fb Token
    public static final String LAST_FB_TOKEN_UPDATE_TIME = "LAST_FB_UPDATE_TIME";
    public static final String FB_TOKEN_EXPIRED_LOGOUT = "FB_TOKEN_EXPIRED";
    public static final String ALL_MUTUAL_FRIENDS = "ALL_MUTUAL_FRIENDS";

    public static final String RIGID_FIELD_SHOW_TM_SCENES_BLOOPER = "RIGID_FIELD_SHOW_TM_BLOOPER";

    //Payments
    public static final String RIGID_FIELD_IS_GOOGLE_PAYMENT_ENABLED = "RIGID_FIELD_GOOGLE_PAYMENT_ENABLED";
    public static final String RIGID_FIELD_IS_PAYTM_PAYMENT_ENABLED = "RIGID_FIELD_PAYTM_PAYMENT_ENABLED";
    public static final String RIGID_FIELD_IS_PAYTM_WALLET_PAYMENT_ENABLED = "RIGID_FIELD_PAYTM_WALLET_PAYMENT_ENABLED";
    public static final String RIGID_FIELD_IS_PAYTM_NB_PAYMENT_ENABLED = "RIGID_FIELD_PAYTM_NB_PAYMENT_ENABLED";
    public static final String RIGID_FIELD_IS_PAYTM_CC_PAYMENT_ENABLED = "RIGID_FIELD_PAYTM_CC_PAYMENT_ENABLED";
    public static final String RIGID_FIELD_IS_PAYTM_DC_PAYMENT_ENABLED = "RIGID_FIELD_PAYTM_DC_PAYMENT_ENABLED";

    public static final String RIGID_FIELD_SHOW_FAVORITES_IN_MATCHES_INTERVAL_DAYS = "SHOW_FAVORITES_IN_MATCHES_INTERVAL_DAYS";
    public static final String RIGID_FIELD_SHOW_FAVORITES_IN_MATCHES_FEMALE_POSITION = "SHOW_FAVORITES_IN_MATCHES_FEMALE_POSITION";
    public static final String RIGID_FIELD_SHOW_FAVORITES_IN_MATCHES_FEMALE_HIDES = "SHOW_FAVORITES_IN_MATCHES_FEMALE_HIDES";
    public static final String RIGID_FIELD_EDIT_FAVORITES_SHOWN_IN_MATCHES_TIMESTAMP = "EDIT_FAVORITES_SHOWN_IN_MATCHES_TIMESTAMP";

    //Select
    public static final String RIGID_FIELD_SELECT_ENABLED = "SELECT_ENABLED";
    public static final String RIGID_FIELD_IS_SELECT_MEMBER = "IS_SELECT_MEMBER";
    public static final String RIGID_FIELD_IS_SELECT_QUIZ_PLAYED = "IS_SELECT_QUIZ_PLAYED";
    public static final String RIGID_FIELD_SELECT_SIDE_CTA_TEXT = "SELECT_SIDE_CTA_TEXT";
    public static final String RIGID_FIELD_SELECT_DAYS_LEFT = "SELECT_DAYS_LEFT";
    public static final String RIGID_FIELD_SELECT_PROFILE_CTA = "SELECT_PROFILE_CTA";
    public static final String RIGID_FIELD_SELECT_PROFILE_CTA_SUBTEXT = "SELECT_PROFILE_CTA_SUBTEXT";
    public static final String RIGID_FIELD_SELECT_COMP_TEXT_DEF = "SELECT_COMP_TEXT_DEF";
    public static final String RIGID_FIELD_SELECT_COMP_IMAGE_DEF = "SELECT_COMP_IMAGE_DEF";

    public static final String RIGID_FIELD_SELECT_NUDGE_LAST_TS = "SELECT_NUDGE_LAST_TS";
    public static final String RIGID_FIELD_SELECT_NUDGE_ENABLED = "SELECT_NUDGE_ENABLED";
    public static final String RIGID_FIELD_SELECT_NUDGE_CONTENT = "SELECT_NUDGE_CONTENT";
    public static final String RIGID_FIELD_SELECT_NUDGE_FREQUENCY = "SELECT_NUDGE_FREQUENCY";
    public static final String RIGID_FIELD_SELECT_ACTION_ALLOW_LIKE = "SELECT_ACTION_ALLOW_LIKE";
    public static final String RIGID_FIELD_SELECT_ACTION_ALLOW_SPARK = "SELECT_ACTION_ALLOW_SPARK";
    public static final String RIGID_FIELD_SELECT_ACTION_ALLOW_LIKEBACK = "SELECT_ACTION_ALLOW_LIKEBACK";

    public static final String RIGID_FIELD_SELECT_PAYMENT_DONE = "SELECT_PAYMENT_DONE";
    public static final String RIGID_FIELD_SELECT_QUIZ_ID = "SELECT_QUIZ_ID";
    public static final String RIGID_FIELD_SELECT_QUIZ_DATA = "SELECT_QUIZ_DATA";



}
