package com.trulymadly.android.app;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.stetho.Stetho;
import com.google.android.gms.analytics.Tracker;
import com.moe.pushlibrary.MoEHelper;
import com.trulymadly.android.analytics.GAEventTrackerV4;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.asynctasks.TrackEventToFbAsyncTask;
import com.trulymadly.android.app.bus.ServiceEvent;
import com.trulymadly.android.app.bus.ServiceEvent.SERVICE_EVENT_TYPE;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.json.ConstantsSocket.SOCKET_END;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.TimeUtils;
import com.trulymadly.android.app.utility.Utility;
import com.trulymadly.android.socket.SocketHandler;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.RejectedExecutionException;

import io.fabric.sdk.android.Fabric;

//import com.inmobi.sdk.InMobiSdk;

public class TrulyMadlyApplication extends MultiDexApplication implements Application.ActivityLifecycleCallbacks {

    public static final ConcurrentHashMap<TrackerName, Tracker> mTrackers = new ConcurrentHashMap<>();
    private static final String env_dev = "SERVER_DEV";
    private static final String env_t1 = "SERVER_T1";
    private static final String env_www = "SERVER_WWW";
    private static final String env = (Constants.isLive ? env_www
            : (Constants.isT1 ? env_t1 : env_dev));
    public static boolean wasInBackground = true;
    public static long lastGaStartTime = 0;
    public static boolean cdChatTutorialShownInThisSession = false;
    private static boolean applicationVisible = false;
    private static String visibleActivityName = "";
    private Timer mActivityTransitionTimer;
    private TimerTask mActivityTransitionTimerTask;

    public static boolean isApplicationVisible() {
        return applicationVisible;
    }

    public static String getVisibleActivityName() {
        return visibleActivityName;
    }

    synchronized public static void createService(Context aContext) {
        if (Utility.isUserLoggedIn(aContext) && SocketHandler.isSocketEnabled(aContext)) {
            if (!TrulyMadlyService.isInstanceCreated()) {
                Intent msgIntent = new Intent(aContext, TrulyMadlyService.class);
                aContext.startService(msgIntent);
            } else {
                Utility.fireBusEvent(aContext, true,
                        new ServiceEvent(SERVICE_EVENT_TYPE.ACTIVITY_RESUMED));
            }
        }
    }

    public static void enableSocket(Context aContext, boolean isSocketEnabled) {
        if (isSocketEnabled) {
            SPHandler.setString(aContext, ConstantsSP.SHARED_KEYS_SOCKET_ENABLED, "true");
            createService(aContext);
        } else {
            SPHandler.remove(aContext, ConstantsSP.SHARED_KEYS_SOCKET_ENABLED);
            TrulyMadlyApplication.stopService(aContext, SOCKET_END.disabled);
        }
    }

    public static void enableSocketDebug(Context ctx, boolean isSocketDebug) {
        if (SocketHandler.isSocketEnabled(ctx)) {
            SPHandler.setBool(ctx, ConstantsSP.SHARED_KEYS_SOCKET_DEBUG, isSocketDebug);
        }
    }


    public static void activityResumedMatches(Activity activity) {
        ((TrulyMadlyApplication) activity.getApplication()).stopActivityTransitionTimer();
    }

    public static void activityPausedMatches(Activity activity) {
        ((TrulyMadlyApplication) activity.getApplication()).startActivityTransitionTimer();
    }

    public static void updateLastEmittedOrReceivedTimestamp(Context aContext) {
        SPHandler.setString(aContext, ConstantsSP.SHARED_KEYS_LAST_SOCKET_UPDATE, TimeUtils.getFormattedTime());
        Utility.fireServiceBusEvent(aContext,
                new ServiceEvent(SERVICE_EVENT_TYPE.UPDATE_LAST_EMIT_TSTAMP));
    }

    public static void stopService(Context aContext, String msg) {
        Utility.fireBusEvent(aContext, true,
                new ServiceEvent(SERVICE_EVENT_TYPE.STOP_SERVICE, msg));
    }

    public static void restartService(Context aContext, String se) {
        stopService(aContext, se);
        createService(aContext);
    }

    public static void forceRestartService(final Context aContext, String event_status) {
        HashMap<String, String> map = new HashMap<>();
        map.put("local_tstamp", TimeUtils.getFormattedTime());
        TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyActivities.Socket, TrulyMadlyEventTypes.socket_disconnection, 0, event_status, map, true);
        Utility.fireBusEvent(aContext, true,
                new ServiceEvent(SERVICE_EVENT_TYPE.STOP_SERVICE));

        final Context context = aContext.getApplicationContext();
        (new Handler(Looper.getMainLooper())).postDelayed(new Runnable() {
            @Override
            public void run() {
                createService(context);
            }
        }, 1000);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (Constants.isDev) {
            ConstantsUrls.setSUFFIX_DEV(getApplicationContext(), null);
        } else if (Constants.isT1) {
            //T1 suffix debug
            //ConstantsUrls.setSUFFIX_T1(getApplicationContext(), null);
        }
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        FacebookSdk.setApplicationId(Constants.APP_ID);
        AppEventsLogger.activateApp(this);
        Fabric.with(this, new Crashlytics());
        Crashlytics.setString("ENVIRONMENT", env);

        //Initializing Inmobi
//        try {
//            InMobiSdk.init(this, Constants.INMOBI_ACCOUNT_ID);
//        } catch (AndroidRuntimeException ignored) {
//
//        }
//        InMobiSdk.setLogLevel(InMobiSdk.LogLevel.DEBUG);

        registerActivityLifecycleCallbacks(this);

        MoEHelper.getInstance(getApplicationContext()).autoIntegrate(this);

        //Initilizing code for non live builds.
        if (!Utility.isLiveRelease()) {
            Stetho.initializeWithDefaults(this);
//            FacebookSdk.setIsDebugEnabled(true);
//            FacebookSdk.addLoggingBehavior(LoggingBehavior.APP_EVENTS);
        }

        fixFanNumberFormatCrash();

    }

    private void fixFanNumberFormatCrash() {
        SharedPreferences sp = getSharedPreferences("com.facebook.ads.FEATURE_CONFIG", 0);
        fixFbBug();
        SharedPreferences.OnSharedPreferenceChangeListener listener = new SharedPreferences.OnSharedPreferenceChangeListener() {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                fixFbBug();
            }
        };
        sp.registerOnSharedPreferenceChangeListener(listener);
    }

    private void fixFbBug() {
        SharedPreferences sp = getSharedPreferences("com.facebook.ads.FEATURE_CONFIG", 0);
        String visible_area_percentage = sp.getString("visible_area_percentage", "0");
        try {
            Integer.valueOf(visible_area_percentage);
        } catch (Exception e) {
            Crashlytics.log(Log.ERROR, "FAN", "FAN Crash fixed");
            Crashlytics.logException(e);
            sp.edit().putString("visible_area_percentage", "50").commit();
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    private void startActivityTransitionTimer() {
        this.mActivityTransitionTimer = new Timer();
        this.mActivityTransitionTimerTask = new TimerTask() {
            public void run() {
                wasInBackground = true;
            }
        };

        long MAX_ACTIVITY_TRANSITION_TIME_MS = 4000;
        this.mActivityTransitionTimer.schedule(mActivityTransitionTimerTask,
                MAX_ACTIVITY_TRANSITION_TIME_MS);
    }

    private void stopActivityTransitionTimer() {
        if (this.mActivityTransitionTimerTask != null) {
            this.mActivityTransitionTimerTask.cancel();
        }

        if (this.mActivityTransitionTimer != null) {
            this.mActivityTransitionTimer.cancel();
        }

        wasInBackground = false;
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        //NetworkHandler.createNetworkSampler();
        Bundle params = new Bundle();
        params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, activity.getLocalClassName());
        TrackEventToFbAsyncTask.trackEvent(activity, AppEventsConstants.EVENT_NAME_VIEWED_CONTENT, params);
    }

    @Override
    public void onActivityStarted(Activity activity) {
        TrulyMadlyTrackEvent.onStart(activity);
    }

    @Override
    public void onActivityResumed(Activity activity) {
        applicationVisible = true;
        visibleActivityName = activity.getLocalClassName();
        //toggleFacebookAppEventsLogger(activity, true);
        createService(activity);
        updateLastEmittedOrReceivedTimestamp(activity);
        GAEventTrackerV4.checkGAQueueAlarmAndSet(activity);
        //NetworkHandler.getmConnectionClassManager().register(NetworkHandler.getmListener());
    }

    synchronized private void toggleFacebookAppEventsLogger(Activity activity, boolean toActivate) {
        try {
            if (toActivate) {
                AppEventsLogger.activateApp(activity, Constants.APP_ID);
            } else {
                AppEventsLogger.deactivateApp(activity, Constants.APP_ID);
            }
        } catch (RejectedExecutionException ignored) {

        }


    }

    @Override
    public void onActivityPaused(Activity activity) {
        applicationVisible = false;
        visibleActivityName = "";
        //toggleFacebookAppEventsLogger(activity, false);
        //NetworkHandler.getmConnectionClassManager().remove(NetworkHandler.getmListener());
    }

    @Override
    public void onActivityStopped(Activity activity) {
        TrulyMadlyTrackEvent.onStop(activity);
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
    }

    public enum TrackerName {
        APP_TRACKER, // Tracker used only in this app.
        GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg:
        // roll-up tracking.
        ECOMMERCE_TRACKER, // Tracker used by all ecommerce transactions from a
        // company.
    }

}
