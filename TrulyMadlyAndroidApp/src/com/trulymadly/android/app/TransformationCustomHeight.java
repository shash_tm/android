package com.trulymadly.android.app;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;

public class TransformationCustomHeight implements
		com.squareup.picasso.Transformation {
	private final int w;
	private final int h;
	private final int padding;

	public TransformationCustomHeight(final int w, final int h,
			final int padding) {
		this.w = w;
		this.h = h;
		this.padding = padding;
	}

	@Override
	public Bitmap transform(Bitmap source) {

		int w1 = source.getWidth();
		int h1 = source.getHeight();

		Bitmap test;

		if (h1 + padding > w1) {

			test = scaledBitmap(source, true);
		} else
			test = scaledBitmap(source, false);

		Bitmap result = null;
		final Paint paint = new Paint();
		paint.setAntiAlias(true);
		try {
			paint.setShader(new BitmapShader(test, Shader.TileMode.CLAMP,
					Shader.TileMode.CLAMP));
			result = Bitmap.createBitmap(w, h, Config.ARGB_8888);
		} catch (NullPointerException ignored) {
		}
		if (result == null) {
			result = source;
		} else if (result != source) {
			source.recycle();
		}
		try {
			Canvas canvas = new Canvas(result.isMutable() ? result
					: result.copy(Config.ARGB_8888, true));
			canvas.drawRoundRect(new RectF(0, 0, w, h), 0, 0, paint);
		} catch (RuntimeException ignored) {
		}
		
		return result;
	}

	@Override
	public String key() {
		return "customWidthHeight";
	}

	private Bitmap scaledBitmap(Bitmap source, boolean isWidth) {

		int targetWidth, targetHeight;
		double aspectRatio;
		if (isWidth) {
			targetWidth = w;
			aspectRatio = (double) source.getHeight()
					/ (double) source.getWidth();
			targetHeight = (int) (targetWidth * aspectRatio);
		} else {
			targetHeight = h;
			aspectRatio = (double) source.getWidth()
					/ (double) source.getHeight();
			targetWidth = (int) (targetHeight * aspectRatio);
		}
		Bitmap result;

		try {
			result = Bitmap.createScaledBitmap(source, targetWidth,
					targetHeight, false);
		} catch (NullPointerException e) {
			return source;
		}
		if (source != result) {
			source.recycle();
		}
		return result;
	}
}
