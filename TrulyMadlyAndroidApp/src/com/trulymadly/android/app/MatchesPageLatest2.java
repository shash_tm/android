package com.trulymadly.android.app;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Bitmap.Config;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.InflateException;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.facebook.AccessToken;
import com.facebook.FacebookException;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.koushikdutta.ion.Ion;
import com.moe.pushlibrary.MoEHelper;
import com.moe.pushlibrary.utils.MoEHelperConstants;
import com.moengage.widgets.NudgeView;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventStatus;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.ads.AdsHelper;
import com.trulymadly.android.app.ads.AdsSource;
import com.trulymadly.android.app.ads.AdsType;
import com.trulymadly.android.app.ads.AdsUtility;
import com.trulymadly.android.app.ads.SimpleAdsListener;
import com.trulymadly.android.app.asynctasks.PhotoUploader;
import com.trulymadly.android.app.asynctasks.PhotoUploader.PhotoUploaderRequestInterface;
import com.trulymadly.android.app.asynctasks.UnreadCounterHackAsyncTask;
import com.trulymadly.android.app.billing.PaymentMode;
import com.trulymadly.android.app.billing.SparksBillingController;
import com.trulymadly.android.app.bus.BusProvider;
import com.trulymadly.android.app.bus.EventInfoUpdateEvent;
import com.trulymadly.android.app.bus.NetworkChangeEvent;
import com.trulymadly.android.app.bus.SimpleAction;
import com.trulymadly.android.app.bus.TriggerConversationListUpdateEvent;
import com.trulymadly.android.app.fragments.BuySparkDialogFragment;
import com.trulymadly.android.app.fragments.BuySparkDialogFragmentNew;
import com.trulymadly.android.app.fragments.BuySparkEventListener;
import com.trulymadly.android.app.fragments.SelectQuizResultFragment;
import com.trulymadly.android.app.fragments.SendSparkFragment;
import com.trulymadly.android.app.fragments.SimpleDialogFragment;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.Constants.AlarmRequestStatus;
import com.trulymadly.android.app.json.Constants.HttpRequestType;
import com.trulymadly.android.app.json.ConstantsRF;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.json.MatchLikeHideResponseParser;
import com.trulymadly.android.app.json.PhotoResponseParser;
import com.trulymadly.android.app.json.ProfileNewResponseParser;
import com.trulymadly.android.app.json.UserFlagsParser;
import com.trulymadly.android.app.listener.ActivityEventsListener;
import com.trulymadly.android.app.listener.AlertDialogInterface;
import com.trulymadly.android.app.listener.CachedDataInterface;
import com.trulymadly.android.app.listener.ConfirmDialogInterface;
import com.trulymadly.android.app.listener.CustomOkHttpResponseHandler;
import com.trulymadly.android.app.listener.OnActionBarClickedInterface;
import com.trulymadly.android.app.listener.OnConversationListFetchedInterface;
import com.trulymadly.android.app.listener.TrackingBuySparkEventListener;
import com.trulymadly.android.app.modal.FbFriendModal;
import com.trulymadly.android.app.modal.MatchesLatestModal2;
import com.trulymadly.android.app.modal.MatchesSysMesgModal;
import com.trulymadly.android.app.modal.MyDetailModal;
import com.trulymadly.android.app.modal.MySelectData;
import com.trulymadly.android.app.modal.NotificationModal;
import com.trulymadly.android.app.modal.ProfileNewModal;
import com.trulymadly.android.app.modal.SelectQuizCompareModal;
import com.trulymadly.android.app.modal.SparkBlooperModal;
import com.trulymadly.android.app.modal.SparkPackageModal;
import com.trulymadly.android.app.modal.UserFlags;
import com.trulymadly.android.app.modal.UserModal;
import com.trulymadly.android.app.modal.VideoModal;
import com.trulymadly.android.app.sqlite.MatchesDbHandler;
import com.trulymadly.android.app.sqlite.MessageDBHandler;
import com.trulymadly.android.app.sqlite.RFHandler;
import com.trulymadly.android.app.utility.ABHandler;
import com.trulymadly.android.app.utility.ActionBarHandler;
import com.trulymadly.android.app.utility.ActivityHandler;
import com.trulymadly.android.app.utility.AdsTrackingHandler;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.CirclePageIndicator2;
import com.trulymadly.android.app.utility.FilesHandler;
import com.trulymadly.android.app.utility.MoEHandler;
import com.trulymadly.android.app.utility.NotificationHandler;
import com.trulymadly.android.app.utility.OkHttpHandler;
import com.trulymadly.android.app.utility.PermissionsHelper;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.SlidingMenuHandler;
import com.trulymadly.android.app.utility.SparksHandler;
import com.trulymadly.android.app.utility.TMSelectHandler;
import com.trulymadly.android.app.utility.TimeUtils;
import com.trulymadly.android.app.utility.TmLogger;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;
import com.trulymadly.android.app.utility.WebviewHandler;
import com.trulymadly.android.app.views.ArcLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static butterknife.ButterKnife.findById;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities.select;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities.sparks;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventInfoKeys.ad_feasible;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes.favorites_popup_matches;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes.hide_matchpage;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes.like_matchpage;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes.select_detail_card_profile;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes.select_on_action_nudge;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes.select_serious_intent_nudge;
import static com.trulymadly.android.app.fragments.SendSparkFragment.SendSparkEventListener;

public class MatchesPageLatest2 extends AppCompatActivity
        implements OnClickListener, OnCheckedChangeListener {

    private static final int MATCH_NEW = 30;
    private static final int USER_FLAGS = 31;
    private static final int UPDATE_NOTIFICATION_COUNTER = 35;
    private static final int NO_OF_MATCHES_DEFAULT = 10;
    private static final long MATCHES_CALL_INTERVAL_DEFAULT = 30 * 60 * 1000;
    private static final long UPLOAD_PROFILE_PIC_AFTER_LIKE_INTERVAL = 30 * 60 * 1000;
    private static final long UPLOAD_PROFILE_PIC_FIRST_TIME = TimeUtils.HOURS_24_IN_MILLIS;
    private static final long MAX_NO_OF_TIMES_DIALOG_SHOWN = 3;
    public static int isLikeHide = 0, likeHidePos = 0;
    private static boolean isResumed = false;
    private static boolean isCreated = false;

    private static boolean fb_mutual_friend_visible;
    private final int INITIAL_AD_POSITION = 2;
    @BindView(R.id.indicator2)
    CirclePageIndicator2 indicator2;
    @BindView(R.id.miss_tm_arcloader)
    ArcLoader miss_tm_arcloader;
    @BindView(R.id.matches_overlay_button_id)
    TextView matches_overlay_button_id;
    @BindView(R.id.miss_tm_icon)
    ImageView miss_tm_icon;
    @BindView(R.id.pi_nudge_overlay_stub)
    ViewStub mPiNudgeOverlayStub;
    @BindView(R.id.error_message)
    TextView error_message;
    //profile Ad
    @BindView(R.id.webview_include_view)
    View mWebviewIncludeView;
    @BindView(R.id.custom_prog_bar_id)
    View custom_prog_bar_id;
    @BindView(R.id.progress_bar_layout_id)
    View progress_bar_layout_id;
    @BindView(R.id.retry_lay)
    View retry_lay;
    @BindView(R.id.tutorial_liked_first_time_stub)
    ViewStub tutorial_liked_first_time_stub;
    @BindView(R.id.tutorial_like_nope_first_time_stub)
    ViewStub tutorial_like_nope_first_time_stub;
    @BindView(R.id.tutorial_socials_stub)
    ViewStub tutorial_socials_stub;
    @BindView(R.id.matches_page_setter)
    View matches_page_setter;
    @BindView(R.id.photo_and_pv_nudge_stub)
    ViewStub photo_and_pv_nudge_stub;
    @BindView(R.id.crop_layout_matches_page)
    View crop_view;
    @BindView(R.id.profile_like_hide_lay)
    LinearLayout profile_like_hide_lay;
    @BindView(R.id.auth_swipe_lay)
    View auth_swipe_lay;
    @BindView(R.id.saving_text)
    TextView saving_text;
    @BindView(R.id.matches_complt_lay)
    RelativeLayout matches_complt_lay;
    @BindView(R.id.auth_swipe_ok_btn)
    Button auth_swipe_ok_btn;
    @BindView(R.id.spark_dialog_container)
    View mSparksDialogContainer;
    @BindView(R.id.sparks_score_tv)
    TextView mSparksScoreTV;
    @BindView(R.id.spark_lay)
    FloatingActionButton mSparkFAB;
    @BindView(R.id.spark_lay_icon_large_ab)
    ImageView spark_lay_icon_large_ab;
    @BindView(R.id.like_lay)
    View mLikeFAB;
    @BindView(R.id.spark_coachmark_tv)
    TextView mSparkCoachmarkTv;
    @BindView(R.id.hide_lay)
    View mHideFAB;
    @BindView(R.id.category_nudge_layout)
    View category_nudge_layout;
    @BindView(R.id.why_not_spark_toast_iv)
    View why_not_spark_toast_iv;
    //Select
    @BindView(R.id.select_quiz_result_container)
    View mSelectQuizResultContainer;
    private Runnable mHideWhynotSparkToastRunnable;
    private SelectQuizResultFragment mSelectQuizresultFragment;
    private boolean isSelectQuizCompareVisible = false;
    private SelectQuizResultFragment.SelectQuizCompareListener mSelectQuizCompareListener;

    private View mPiNudgeOverlay;
    private View mChangePictureButton;
    private View mCountViewsContainer;
    private TextView mCountViewsTV;
    private TextView mCOuntViewsTextTV;
    private View mPopularityBarContainer;
    private View mBlueBar;
    private ImageView mTooltipIV;
    private View mTooltipView;
    private ImageView mShutterIV;
    private ImageView mLeftLineIV;
    private ImageView mRightLineIV;
    private ImageView mExpressPassionIV;
    private ImageView mCandidIV;
    private ImageView mNaturalIV;
    //int popularity, int profileViews, String profileViewsText
    private int mPopularity, mProfileViews;
    private String mProfileViewsText;
    private RadioButton profile_visibility_options_everyone;
    private RadioGroup profile_visibility_options_radiogroup;
    private RadioButton profile_visibility_options_only_likes;
    private TextView photo_upload_text;
    private View photo_pv_close_button_skip;
    private View photo_pv_close_button_done;
    private View photo_upload_nudge_first_overlay;
    private ScrollView photo_upload_nudge_main_view;
    private View photo_upload_nudge_skipped_view;
    private View photo_and_pv_nudge_skip_add_profile_pic;
    private View photo_and_pv_nudge_skip_cancel;
    private TextView hey_tv;
    private View go_ahead_container;
    private TextView got_it_tv;
    private TextView not_now_tv;
    private TextView go_ahead_tv;
    //    @BindView(R.id.photo_and_pv_nudge)
    private View photo_and_pv_nudge;
    private View photo_upload_slider_layout;
    private View profile_visibility_container_overlay;
    private View photo_upload_iv;
    private View upload_image_box;
    private ProgressBar photo_upload_progressbar;
    private TextView profile_visibility_options_header_text;
    private ImageView profile_uploaded_image;
    private RelativeLayout photo_upload_image_container;
    private View tutorial_liked_first_time;
    private View tutorial_like_nope_first_time;
    private View tutorial_socials;
    private boolean isSparkDialogVisible = false;
    private SparksHandler.Sender mSparkSender;
    private Fragment mCurrentFragment;
    private SendSparkEventListener mSendSparkEventListener;
    private TrackingBuySparkEventListener mTrackingBuySparkEventListener;
    private SimpleDialogFragment.SimpleDialogActionListener mSimpleDialogActionListener;
    private int matches_swiped;
    private Dialog likeHideIconDialog = null;
    private int NO_OF_MATCHES = NO_OF_MATCHES_DEFAULT;
    private long MATCHES_CALL_INTERVAL = MATCHES_CALL_INTERVAL_DEFAULT;
    private Context aContext;
    private Activity aActivity;
    private ProgressDialog mProgressDialog = null;
    private MatchesLatestModal2 aMatchesLatestModal2 = null;
    private MyDetailModal aMyDetailModal = null;
    private Vector<ProfileNewModal> profilesList = null;
    private ActionBarHandler actionBarHandler;
    private MatchesSysMesgModal aMatchesSysMesgModal = null;
    private boolean isLikeHideClickable = true, isPhotoAndPvNudgeShowing = false;
    private MatchesPageSetter aMatchesPageSetter;
    private MoEHelper mhelper = null;
    private boolean isProfilePicRejectedFemale = false;
    private ScrollView matches_page_setter_scrollview;
    private boolean isPhotoUploadSliderShowing = false, saveDiscoveryOptionOnServer = false, isDiscoveryOn, isPhotoNudgeSkip = true;
    private PhotoUploader photoUploader = null;
    private View main_view;
    private int lastFetchedIndex = -1;
    private View mFemaleNudge;
    private TextView mFemaleNudgeMessage;
    private ArrayList<Integer> mNudgeIndexes;
    private ArrayList<String> mNudgeMessages;
    private Handler mHandler;
    private Runnable mHideFemaleNudgeRunnable, mShowFemaleNudgeRunnable;
    //Native ads
//    private AdsHandler mFullScreenAdsHandler;
    private boolean isAdFetched = false;
    private boolean showAd = false;
    private boolean isAdShown = false;
    private boolean isAdRequested = false;
    private Runnable mShowAdRunnable;
    //    private View mTransparencyOverlay;
    private boolean isFromOnActivityResult = false;
    //PI Nudge
    private Boolean mShowPINudge = null, isPINudgeEnabled = false;
    private CachedDataInterface mCachedDataCallback;
    private WebviewHandler mWebviewHandler;
    private Animation.AnimationListener mTutorialAnimationListener;
    private Animator.AnimatorListener mRevealAnimationListener;
    private boolean isTutorialAnimationInProgress = false;
    private boolean isAnyTutorialPending = true;
    private Runnable tutorial_liked_first_time_hide_runnable, tutorial_socials_hide_runnable, tutorial_like_nope_first_time_show_runnable,
            tutorial_like_nope_first_time_hide_runnable;
    private SlidingMenuHandler slidingMenu;
    private AdsHelper mAdsHelper;
    private int mNextAdPosition = INITIAL_AD_POSITION;
    private boolean isProfileAdVisible = false;
    private int mMatchesBeforeWeShowProfileAd = -1;//Constants.MATCHES_PROFILE_AD_DEF_FINAL_POSIITON;
    private int mFinalPositionForProfileAds = -1;//Constants.MATCHES_PROFILE_AD_DEF_FINAL_POSIITON;
    //    private boolean isVideoTutorialRunning = false;
//    private boolean isVideoTutorialShownOnce = false;
    //    private boolean isVideoTutorialTriedOnce = false;
    private android.app.FragmentManager mFragmentManager;
    private boolean isFirstSetOfTutorialsOver = false;
    private boolean newValueFirstSetOfTutorialsOver = false;
    private boolean isAppUpdatePopupShowing = false;
    private String hardUpdateText;
    private int hardAppVersion = 0;
    private boolean isProfileAdShownOnce = false;
    private boolean showSecondScreenOfPhotoNudge = true;
    //    private FragmentManager mSupportFragmentManager;
    private ImageView likeHideIcon;
    //Sparks related
//    private GooglePlayBillingHandler googlePlayBillingHandler;
    private SparksBillingController mSparksBillingController;
    private SparksHandler.SparksReceiver.SimpleSparkReceiverListener mSparksReceiverListener;
    private SparksHandler.SparksReceiver mSparksReceiverHandler;
    private boolean areSparksFetchedOnce = false;
    private boolean isCategoryNudgeVisible;
    private boolean makeSecondMatchesCall, secondCallInProgress;
    private String mNewSparkmatchId;
    private int mSparkEnabledColor, mSparkDisabledColor;
    private int likes_done = 0, hides_done = 0, sparks_done = 0, hides_done_consecutive = 0;

    private Runnable mShowAdRunnableNew;
    private boolean isPINudgeVisible = false;
    private String current_match_id;
    //    private boolean canIntAdBeShown = false;
    private boolean isSparkScoreAnimationShownOnce = false,
            isHasLikedBeforeAnimationShownForThisMatch = false,
            isHasLikedBeforeNudgeShownForThisMatch = false;
    private ScaleAnimation mShowScoreScaleUpAnimation, mShowScoreScaleDownAnimation,
            mHideScoreScaleUpAnimation, mHideScoreScaleDownAnimation, mSimpleScaleDownAnimation;
    private HashMap<String, String> mTrackingMap;
    private int mAnimationCount = 0;

    private int mDuration = 218, mDelay = 2500, mDelay1Sec = 1000;
    private boolean favoritesShownJustNow = false;

    private OnClickListener mFBOnClickListener;

    private boolean isSelectActivityStarted, isSelectNudgeVisible, isAlreadymatchAnimationRunning;
    private boolean isUSprofileDialogSeen;
    private boolean isInterstitialToBeShown;



    public static void setFbMutualFriendsVisibilty() {
        fb_mutual_friend_visible = false;
    }

    private void prefetchPhotos(PREFETCH_MODE prefetchMode, int totalToFetch) {
        if (getProfilesList() == null || getProfilesList().size() == 0 || getProfilesList().get(0).getUser() == null)
            return;

        ArrayList<String> picsToFetch = new ArrayList<>();

        String url = null;

        String[] otherPicsArray = getProfilesList().get(0).getUser().getOtherPics();
        if (otherPicsArray != null && otherPicsArray.length > 0) {
            picsToFetch.add(getProfilesList().get(0).getUser().getOtherPics()[0]);
            totalToFetch--;
        }

        switch (prefetchMode) {
            case initial:
                for (int i = 0; i < totalToFetch && i < getProfilesList().size() - 1; i++) {
                    url = getProfilesList().get(i).getUser().getProfilePic();
                    if (Utility.isSet(url))
                        picsToFetch.add(url);
                    lastFetchedIndex++;
                }
                break;
            case next:
                if (lastFetchedIndex < getProfilesList().size() - 1) {
                    url = getProfilesList().get(lastFetchedIndex).getUser().getProfilePic();
                    if (Utility.isSet(url))
                        picsToFetch.add(url);
                }
                break;
            case swipe:
                break;
        }

        Utility.prefetchPhotos(aContext, picsToFetch, TrulyMadlyActivities.matches);
    }

    private boolean checkAndShowFemaleNudge() {
        //matches_swiped
        if (mNudgeMessages != null && mNudgeIndexes != null && mNudgeIndexes.contains(matches_swiped)) {
            mHandler.postDelayed(mShowFemaleNudgeRunnable, 350);
            return true;
        }
        return false;
    }

    private void showFemaleNudge(final int position) {
        final int index = mNudgeIndexes.indexOf(position);
        if (index == -1 || index >= mNudgeMessages.size() || !Utility.isSet(mNudgeMessages.get(index)))
            return;

        Animation inFromLeft = UiUtils.inFromLeftAnimation();
        inFromLeft.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                mFemaleNudge.setVisibility(View.VISIBLE);
                mFemaleNudgeMessage.setText(mNudgeMessages.get(index));
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mHandler.postDelayed(mHideFemaleNudgeRunnable, 3000);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        mFemaleNudge.startAnimation(inFromLeft);
    }

    private void hideFemaleNudge() {
        Animation outToLeft = UiUtils.outToLeftAnimation();
        outToLeft.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mFemaleNudge.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        mFemaleNudge.startAnimation(outToLeft);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        isCreated = false;
        super.onCreate(savedInstanceState);
        mTrackingMap = new HashMap<>();


        try {
            setContentView(R.layout.matchespagelatest2);
        } catch (OutOfMemoryError | InflateException ignored) {
            System.gc();
            finish();
            return;
        }

        //http://www.curious-creature.com/2012/12/01/android-performance-case-study/
        getWindow().setBackgroundDrawable(null);

        ButterKnife.bind(this);
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("isSparkScoreAnimationShownOnce")) {
                isSparkScoreAnimationShownOnce = savedInstanceState.getBoolean("isSparkScoreAnimationShownOnce");
            }
            if (savedInstanceState.containsKey("isHasLikedBeforeAnimationShownForThisMatch")) {
                isHasLikedBeforeAnimationShownForThisMatch = savedInstanceState.getBoolean("isHasLikedBeforeAnimationShownForThisMatch");
            }
            if (savedInstanceState.containsKey("isHasLikedBeforeNudgeShownForThisMatch")) {
                isHasLikedBeforeNudgeShownForThisMatch = savedInstanceState.getBoolean("isHasLikedBeforeNudgeShownForThisMatch");
            }
            if (savedInstanceState.containsKey("isSelectQuizCompareVisible")) {
                isSelectQuizCompareVisible = savedInstanceState.getBoolean("isSelectQuizCompareVisible");
                if (isSelectQuizCompareVisible) {
                    mSelectQuizresultFragment = (SelectQuizResultFragment) getSupportFragmentManager().findFragmentById(R.id.select_quiz_result_container);
                    if (mSelectQuizresultFragment != null) {
                        getSupportFragmentManager().beginTransaction().remove(mSelectQuizresultFragment).commit();
                        mSelectQuizResultContainer.setVisibility(View.GONE);
                        mSelectQuizresultFragment = null;
                    }
                    isSelectQuizCompareVisible = false;
                }
            }
        }

        mFBOnClickListener = this;

        mAdsHelper = new AdsHelper(getApplicationContext(), new SimpleAdsListener() {
            @Override
            public void onAdStarted(AdsType adsType) {
                if (adsType == AdsType.MATCHES_MID_PROFILE_VIDEO) {
                    if (isProfileAdVisible && aMatchesPageSetter != null) {
                        aMatchesPageSetter.loadHashTagsForAds(mAdsHelper.getHashTags());
                    }
                }
            }

            @Override
            public void onAdFinished(AdsType adsType) {
                if (adsType == AdsType.MATCHES_MID_PROFILE_VIDEO) {
                    aMatchesPageSetter.reloadProfileAd(mAdsHelper);
                }
            }

            @Override
            public void onAdClicked(AdsType adsType) {
                if (adsType == AdsType.MATCHES_MID_PROFILE_VIDEO) {
                    aMatchesPageSetter.setIsAdClicked(true);
                }
            }
        }, AdsSource.SEVENTY_NINE);
//        mAdsHelper.initialize(AdsType.MATCHES_MID_PROFILE_VIDEO, TrulyMadlyActivities.matches);
//        mAdsHelper.initialize(AdsType.MATCHES_END_INTERSTITIAL, TrulyMadlyActivities.matches);

        aActivity = this;
        aContext = this;

        // after 1 day
        if (TimeUtils.isDifferenceGreater(aContext, ConstantsRF.LAST_FB_TOKEN_UPDATE_TIME, Constants.TOKEN_UPDATE_INTERVAL)) {
            final AccessToken accessToken = AccessToken.getCurrentAccessToken();

            // access token exists
            if (accessToken != null) {
                AccessToken.AccessTokenRefreshCallback callback = new AccessToken.AccessTokenRefreshCallback() {
                    @Override
                    public void OnTokenRefreshed(AccessToken accessToken) {
                        updateFBToken(accessToken);
                    }

                    @Override
                    public void OnTokenRefreshFailed(FacebookException exception) {
                        updateFBToken(accessToken);
                    }
                };
                AccessToken.refreshCurrentAccessTokenAsync(callback);
            }
        }

        //ABHandler.initAB(this, ABHandler.ABTYPE.AB_SPARKS_ICON);
        ABHandler.setImageIconForSparkFABIcon(aContext, mSparkFAB, spark_lay_icon_large_ab);

        mhelper = new MoEHelper(this);
        FilesHandler.createImageCacheFolder();

        NudgeView nv = findById(this, R.id.nudge);
        nv.initialiseNudgeView(this);
        isFirstSetOfTutorialsOver = RFHandler.getBool(aContext,
                Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_FIRST_SET_TUTORIALS_OVER);
        if (!isFirstSetOfTutorialsOver) {
            isFirstSetOfTutorialsOver = RFHandler.getBool(aContext,
                    ConstantsRF.RIGID_FIELD_LIKE_HIDE_FIRST_TIME)
                    && SPHandler.getBool(aContext,
                    ConstantsSP.SHARED_PREF_NEW_TS_TUTORIAL);
        }
        newValueFirstSetOfTutorialsOver = isFirstSetOfTutorialsOver;

        WebviewHandler.WebviewActionsListener mWebviewActionsListener = new WebviewHandler.WebviewActionsListener() {
            @Override
            public boolean shouldOverrideUrlLoading(String url) {
                mWebviewHandler.loadUrl(url);
                return true;
            }

            @Override
            public void webViewHiddenOnUrlLoad() {

            }

            @Override
            public void onWebViewCloseClicked() {

            }
        };
        mWebviewHandler = new WebviewHandler(mWebviewIncludeView, mWebviewActionsListener, true, true);

        Picasso.with(this)
                .load(R.drawable.miss_tm_nudge).noFade().into((ImageView) findById(this, R.id.nudge_iv));

//        Picasso.with(this)
//                .load(R.drawable.like).noFade().into((ImageView) findById(this, R.id.like));
//        Picasso.with(this)
//                .load(R.drawable.hide).noFade().into((ImageView) findById(this, R.id.hide));

        custom_prog_bar_id.setOnClickListener(this);
        custom_prog_bar_id.setVisibility(View.GONE);
        category_nudge_layout.setOnClickListener(this);

        Utility.disableScreenShot(aActivity);
//        Utility.callQuizApi(aContext);

        MoEHandler.trackEvent(aContext, MoEHandler.Events.MATCHES_LAUNCHED);
        OnActionBarClickedInterface actionBarClickedInterface = new OnActionBarClickedInterface() {
            @Override
            public void onConversationsClicked() {
//                if(true){
//                    showOnActionSelectNudge("http://65.media.tumblr.com/avatar_501bd484cae1_128.png",
//                            "http://media.todaybirthdays.com/thumb_x256x256/upload/2015/11/10/deepika-padukone3.jpg");
//                    return;
//                }
                ActivityHandler.startConversationListActivity(aActivity);
            }

            @Override
            public void onLocationClicked() {
            }


            @Override
            public void onCuratedDealsClicked() {

            }

            @Override
            public void onBackClicked() {
            }

            @Override
            public void onUserProfileClicked() {
            }

            @Override
            public void onTitleClicked() {
            }

            @Override
            public void onTitleLongClicked() {
            }

            @Override
            public void onSearchViewOpened() {

            }

            @Override
            public void onSearchViewClosed() {

            }

            @Override
            public void onCategoriesClicked() {
                ActivityHandler.startCategoriesActivity(aActivity);
            }
        };
        slidingMenu = new SlidingMenuHandler(this, SlidingMenu.TOUCHMODE_MARGIN);
        actionBarHandler = new ActionBarHandler(this, "", slidingMenu,
                actionBarClickedInterface, false, false, false);
        actionBarHandler.toggleNotificationCenter(true);
        actionBarHandler.setToolbarTransparent(false, getString(R.string.profiles));
//        actionBarHandler.toggleCategories(RFHandler.getBool(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_ALLOW_MEETUPS));
        actionBarHandler.toggleCategories(false);

        progress_bar_layout_id.setOnClickListener(this);
        retry_lay.setOnClickListener(this);

        //Glide.with(aContext).load(R.drawable.miss_tm_loader).into(miss_tm_loader);
//        Ion.with(miss_tm_loader).load(UiUtils.resIdToUri(aContext, R.drawable.miss_tm_loader).toString());
        Picasso.with(this).load(R.drawable.miss_tm_grey_bg).noFade().into(miss_tm_icon);

        auth_swipe_lay.setVisibility(View.GONE);
        auth_swipe_lay.setOnClickListener(this);


        auth_swipe_ok_btn.setOnClickListener(this);

        mHideFAB.setOnClickListener(this);
        mLikeFAB.setOnClickListener(this);

        matches_page_setter_scrollview = (ScrollView) matches_page_setter.findViewById(R.id.matches_page_setter_scrollview);

        profile_like_hide_lay.setVisibility(View.GONE);
        aMatchesPageSetter = new MatchesPageSetter(this, matches_page_setter, false, false, false, 50, mFBOnClickListener);

        String userId = Utility.getMyId(aContext);
        String passcode = RFHandler.getString(aContext, userId,
                ConstantsRF.RIGID_FIELD_HIDE_CONVERSATIONS_PASSCODE);
        if (passcode != null) {
            // Re-check passcode alarm status
            AlarmRequestStatus alarmStatus = Utility.getAlarmRequestStatus(
                    aContext, userId,
                    ConstantsRF.RIGID_FIELD_CONVERSATIONS_PASSCODE_REQUESTED_TIMESTAMP,
                    Constants.PASSCODE_DELAY);

            switch (alarmStatus) {
                case EXPIRED:
                case IN_PROGRESS:
                    Long difference = TimeUtils.getTimeoutDifference(aContext,
                            ConstantsRF.RIGID_FIELD_CONVERSATIONS_PASSCODE_REQUESTED_TIMESTAMP,
                            userId, Constants.PASSCODE_DELAY);
                    if (difference == null) {

                    } else if (difference != 0) {
                        Utility.generatePasscodeAlarm(aContext, passcode,
                                difference);
                    }
                    break;
                case NOT_SET:
                default:
                    break;
            }
        }

        PhotoUploaderRequestInterface photoUploaderRequestInterface = new PhotoUploaderRequestInterface() {

            @Override
            public void onSuccess(String jsonResponseString) {
                PhotoResponseParser.parsePhotoResponse(jsonResponseString, aContext);
                if (UserPhotos.profilePhoto != null) {
                    Picasso.with(aContext).load(UserPhotos.profilePhoto.getName()).config(Config.RGB_565).into(profile_uploaded_image, new Callback() {

                        @Override
                        public void onSuccess() {
                            onSuccessUploadPhoto();
                        }

                        @Override
                        public void onError() {
                            onFail(getString(R.string.ERROR_NETWORK_FAILURE));
                        }
                    });
                }


            }

            @Override
            public void onFail(String failMessage) {
                showPhotoUploadUi();
                AlertsHandler.showMessage(aActivity, failMessage);
            }

            @Override
            public void onComplete() {
            }

            @Override
            public void onActivityResultSuccess(HttpRequestType type, String filePath) {
                issueRequestUploadPhoto(type, filePath);
            }

            @Override
            public void onActivityResultSuccessFile(HttpRequestType httpRequestType, Uri selectedImage, String scheme) {

            }
        };
        photoUploader = new PhotoUploader(aActivity, photoUploaderRequestInterface, matches_complt_lay, crop_view);

        initializeNudgesData();

        mHandler = new Handler();

        initializeAd();

        NO_OF_MATCHES = RFHandler.getInt(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_NO_OF_MATCHES, NO_OF_MATCHES_DEFAULT);
        MATCHES_CALL_INTERVAL = RFHandler.getLong(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_MATCHES_CALL_INTERVAL, MATCHES_CALL_INTERVAL_DEFAULT);

//        mSupportFragmentManager = getSupportFragmentManager();
//        showScenesVideoTutorial();

        callingIntent(getIntent());

        initializeSparkFields();

//        fetchSparksForSparkBlooper();

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            RelativeLayout.LayoutParams sparksParams = (RelativeLayout.LayoutParams) mSparkFAB.getLayoutParams();
            sparksParams.setMargins(0, 0, 0, 0); // get rid of margins since shadow area is now the margin
            mSparkFAB.setLayoutParams(sparksParams);

            LinearLayout.LayoutParams likeParams = (LinearLayout.LayoutParams) mLikeFAB.getLayoutParams();
            likeParams.setMargins(0, 0, 0, 0); // get rid of margins since shadow area is now the margin
            mLikeFAB.setLayoutParams(likeParams);

            LinearLayout.LayoutParams hideParams = (LinearLayout.LayoutParams) mHideFAB.getLayoutParams();
            hideParams.setMargins(0, 0, 0, 0); // get rid of margins since shadow area is now the margin
            mHideFAB.setLayoutParams(hideParams);
        }

        if (aMatchesPageSetter != null) {
            aMatchesPageSetter.onCreate();
        }

        //Removing video directory from tm assets
        FilesHandler.deleteFileOrDirectory(FilesHandler.getVideoDirectoryPath(false));

        mHideWhynotSparkToastRunnable = new Runnable() {
            @Override
            public void run() {
                why_not_spark_toast_iv.setVisibility(View.GONE);
            }
        };
    }

    //update on backend
    private void updateFBToken(AccessToken accessToken) {
        HashMap<String, String> params = new HashMap<>();
        params.put("token", accessToken.getToken());
        // sending token to server
        CustomOkHttpResponseHandler tokenUpdateHandler = new CustomOkHttpResponseHandler(aActivity) {
            @Override
            public void onRequestSuccess(JSONObject responseJSON) {
                RFHandler.insert(aContext, Utility.getMyId(aContext), ConstantsRF.LAST_FB_TOKEN_UPDATE_TIME, TimeUtils.getSystemTimeInMiliSeconds());

            }

            @Override
            public void onRequestFailure(Exception exception) {

            }
        };
        OkHttpHandler.httpPost(aContext, ConstantsUrls.get_update_acces_token_url(), params, tokenUpdateHandler);
    }

    private void initializeSparkFields() {
        mSparkEnabledColor = ActivityCompat.getColor(aContext, R.color.colorTertiary);
        mSparkDisabledColor = ActivityCompat.getColor(aContext, R.color.gray_all_d);
        mSendSparkEventListener = new SendSparkEventListener() {
            @Override
            public void closeFragment() {
                toggleFragment(false, null, 0, 0, false, false, false);
            }

            @Override
            public void sparkSentSuccess() {
                customDialogLikeHideIcon(true, true);
                if (SparksHandler.getSparksLeft(aContext) == 1) {
                    toggleFragment(true, SparksHandler.SparksDialogType.one_spark_left, 0, 0, false, false, false);
                }
            }

            @Override
            public void sparkSentFailed() {

            }
        };

        BuySparkEventListener mBuySparkEventListener = new BuySparkEventListener() {
            @Override
            public void closeFragment() {
                toggleFragment(false, null, 0, 0, false, false, false);
            }

            @Override
            public void onBuySparkClicked(Object packageModal, String matchId) {
                SparkPackageModal sparkPackageModal = (SparkPackageModal) packageModal;
                TmLogger.d("SparksBillingHandler", "Spark buy clicked in Adapter : " + sparkPackageModal.getmPackageSku());
//                mSparksBillingController.launchPurchaseFlow(PaymentMode.google, sparkPackageModal.getmPackageSku(), matchId);
//                mSparksBillingController.launchPurchaseFlow(PaymentMode.paytm, sparkPackageModal.getmPackageSku(), matchId,
//                        sparkPackageModal.getmPrice());
                mSparksBillingController.askForPaymentOption(sparkPackageModal.getmPackageSku(), matchId,
                        sparkPackageModal.getmSparkCount(), sparkPackageModal.getmPrice());
            }

            @Override
            public void onBuySparkSuccess(PaymentMode mPaymentMode, String sku, int newSparksAdded, int totalSparks, boolean isRelationshipExpertAdded, MySelectData mySelectData) {
                if (isResumed) {
                    toggleFragment(true, SparksHandler.SparksDialogType.spark_purchased, newSparksAdded, totalSparks, false, isRelationshipExpertAdded, false);
                }
            }

            @Override
            public void restorePurchasesClicked() {
                mSparksBillingController.restorePurchases(false);
            }

            @Override
            public void onRegistered() {
                mSparksBillingController.restorePurchases(true);
            }
        };

        mTrackingBuySparkEventListener = new TrackingBuySparkEventListener(aContext, mBuySparkEventListener);

        mSimpleDialogActionListener = new SimpleDialogFragment.SimpleDialogActionListener() {
            @Override
            public void onActionButtonClicked(int action) {
                boolean hasLikedBefore = false;
                hasLikedBefore = getProfilesList().get(0).getUser() != null &&
                        getProfilesList().get(0).getUser().isHasLikedBefore();

                switch (action) {
                    case SimpleDialogFragment.SIMPLE_DIALOG_INTRODUCING_SPARK:
                        if (getProfilesList() == null) {
                            toggleFragment(false, null, 0, 0, false, false, hasLikedBefore);
                            return;
                        }
                        //Tracking : Intro sparks - clicked
                        HashMap<String, String> eventInfo = new HashMap<>();
                        if (getProfilesList() != null && getProfilesList().size() > 0) {
                            eventInfo.put("match_id", getProfilesList().get(0).getUserId());
                        }
                        eventInfo.put("sparks_left", String.valueOf(SparksHandler.getSparksLeft(aContext)));
                        eventInfo.put("from_scenes", String.valueOf(false));
                        eventInfo.put("likes_done", String.valueOf(likes_done));
                        eventInfo.put("hides_done", String.valueOf(hides_done));
                        eventInfo.put("sparks_done", String.valueOf(sparks_done));
                        TrulyMadlyTrackEvent.trackEvent(aContext, sparks, TrulyMadlyEventTypes.intro, 0,
                                TrulyMadlyEventStatus.tryit_clicked,
                                eventInfo, true, true);

                        if (!SparksHandler.isSparksPresent(aContext)) {
                            MoEHandler.trackEvent(aContext, MoEHandler.Events.BUY_PAGE_OPENED);
                            toggleFragment(true, SparksHandler.SparksDialogType.buy_spark, 0, 0, true, false, hasLikedBefore);
                        } else {
                            MoEHandler.trackEvent(aContext, MoEHandler.Events.SPARK_PAGE_OPENED);
                            toggleFragment(true, SparksHandler.SparksDialogType.send_spark, 0, 0, true, false, hasLikedBefore);
                        }
                        break;
                    case SimpleDialogFragment.SIMPLE_DIALOG_ONE_LEFT:
                        toggleFragment(true, SparksHandler.SparksDialogType.buy_spark, 0, 0, false, false, hasLikedBefore);
                        break;
                    case SimpleDialogFragment.SIMPLE_DIALOG_PURCHASE_COMPLETE:
                        toggleFragment(false, SparksHandler.SparksDialogType.spark_purchased, 0, 0, false, false, hasLikedBefore);
                        break;

                }
            }

            @Override
            public void closeFragment() {
                toggleFragment(false, null, 0, 0, false, false, false);
            }
        };

//        sparksBillingHandler = new SparksBillingHandler(this, mBuySparkEventListener);
        mSparksBillingController = new SparksBillingController(this, null, mTrackingBuySparkEventListener);

        mCurrentFragment = getSupportFragmentManager().findFragmentById(R.id.spark_dialog_container);
        if (mCurrentFragment != null) {
            SparksHandler.SparksDialogType dialogType = SparksHandler.SparksDialogType.introducing_sparks;
            if (mCurrentFragment instanceof BuySparkDialogFragment || mCurrentFragment instanceof BuySparkDialogFragmentNew) {
//                ((ActivityEventsListener) mCurrentFragment).registerListener(mBuySparkEventListener);
                ((ActivityEventsListener) mCurrentFragment).registerListener(mTrackingBuySparkEventListener);
            } else if (mCurrentFragment instanceof SimpleDialogFragment) {
                ((ActivityEventsListener) mCurrentFragment).registerListener(mSimpleDialogActionListener);
            } else if (mCurrentFragment instanceof SendSparkFragment) {
                ((ActivityEventsListener) mCurrentFragment).registerListener(mSendSparkEventListener);
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        callingIntent(intent);
    }

    private void callingIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        boolean startSparksOnLaunch, startSelectOnLaunch;
        if (extras != null) {
            startSparksOnLaunch = extras.getBoolean("startSparksOnLaunch", false);
            startSelectOnLaunch = extras.getBoolean("startSelectOnLaunch", false);
        } else {
            startSparksOnLaunch = false;
            startSelectOnLaunch = false;
        }

        if (startSparksOnLaunch) {
            ActivityHandler.startBuyPackagesActivity(aActivity);
        } else if (startSelectOnLaunch) {
            ActivityHandler.startTMSelectActivity(aContext, "on_matches_launch", false);
        }
        intent.removeExtra("startSparksOnLaunch");
        intent.removeExtra("startSelectOnLaunch");
        setIntent(intent);
    }

    private void initializeNudgesData() {
        mFemaleNudge = findById(this, R.id.female_nudge);
        mFemaleNudgeMessage = findById(this, R.id.nudge_message_tv);
        mHandler = new Handler();
        mHideFemaleNudgeRunnable = new Runnable() {
            @Override
            public void run() {
                hideFemaleNudge();
            }
        };
        mShowFemaleNudgeRunnable = new Runnable() {
            @Override
            public void run() {
                showFemaleNudge(matches_swiped);
            }
        };
    }

    private void startTutorials() {
        if (isProfileAdVisible || isTutorialAnimationInProgress || !isAnyTutorialPending) {
//            if (!isAnyTutorialPending && !SPHandler.getBool(aContext,
//                    ConstantsSP.SHARED_PREF_SELECT_NUDGE_TUTORIAL, false) && TMSelectHandler.isSelectEnabled(aContext)) {
//                SPHandler.setBool(aContext,
//                        ConstantsSP.SHARED_PREF_SELECT_NUDGE_TUTORIAL, true);
//                showBuySelectNudge();
//            }
            return;
        }

        if (mTutorialAnimationListener == null) {
            mTutorialAnimationListener = new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    isTutorialAnimationInProgress = false;
                    startTutorials();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            };
        }

        //Hack to Disabling like hide tutorial
        @SuppressWarnings("PointlessBooleanExpression") boolean isLikeHideTutorialShown = true || RFHandler.getBool(aContext, ConstantsRF.RIGID_FIELD_LIKE_HIDE_FIRST_TIME);
        boolean isTSTutorialShown = SPHandler.getBool(aContext,
                ConstantsSP.SHARED_PREF_NEW_TS_TUTORIAL);
        @SuppressWarnings("PointlessBooleanExpression") boolean isEventsTutorialShown = true || RFHandler.getBool(aContext, Utility.getMyId(aContext),
                ConstantsRF.RIGID_FIELD_SOCIALS_TUTORIAL_SHOWN);
        newValueFirstSetOfTutorialsOver = isLikeHideTutorialShown && isTSTutorialShown;
        isAnyTutorialPending = !isLikeHideTutorialShown || !isTSTutorialShown || !isEventsTutorialShown;
        if (!isLikeHideTutorialShown && getProfilesList() != null && getProfilesList().size() > 1) {
            isTutorialAnimationInProgress = true;
            showLikeHideFirstTimeTutorial();
        } else {
            if (!isTSTutorialShown && aMatchesPageSetter != null && getProfilesList() != null && getProfilesList().size() > 1 &&
                    !getProfilesList().get(0).isSponsoredProfile()) {
                isTutorialAnimationInProgress = true;
                aMatchesPageSetter.startTSTutorial(mTutorialAnimationListener);
            } else {
                if (!isEventsTutorialShown) {
                    if (RFHandler.getBool(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_ALLOW_MEETUPS)) {
                        isTutorialAnimationInProgress = true;
                        showSocialsTutorial(/*true*/);
                    }
                }
            }
        }
    }

    private void initializeAd() {

        isAdShown = false;

        mShowAdRunnableNew = new Runnable() {
            @Override
            public void run() {
                if (!isResumed)
                    return;
                mAdsHelper.loadAd(aActivity, AdsType.MATCHES_END_INTERSTITIAL,
                        null, !mAdsHelper.shouldShowAd(AdsType.MATCHES_END_INTERSTITIAL, true));
            }
        };

//        mTransparencyOverlay = findById(this, R.id.transparency_overlay);
    }

    private void enablePINudge(int popularity, int profileViews, String profileViewsText) {
        boolean isMale = Utility.isMale(aContext);
        int currentCounter = RFHandler.getInt(aContext,
                Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_PI_NUDGE_COUNTER, 0);
        if (!isMale || currentCounter == -1 || popularity == -1) {
            isPINudgeEnabled = false;
            mShowPINudge = false;
        } else {
            isPINudgeEnabled = true;
            setPopularityData(popularity, profileViews, profileViewsText);
        }
    }

    /**
     * @param recalculate If recalculate is on, recalculate the boolean used to trigger the PI Nudge when the user
     *                    reaches end of matches
     */
    private boolean shouldPINudgeBeShown(boolean recalculate) {
        if (recalculate || mShowPINudge == null) {
            int currentCounter = RFHandler.getInt(aContext,
                    Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_PI_NUDGE_COUNTER, 0);
            mShowPINudge = currentCounter < 2;
        }

        return mShowPINudge;
    }

    private void setPopularityData(int popularity, int profileViews, String profileViewsText) {
        mPopularity = popularity;
        mProfileViews = profileViews;
        mProfileViewsText = profileViewsText;

        if (mPiNudgeOverlay != null) {
            mCountViewsContainer.setVisibility(View.VISIBLE);
            mCountViewsTV.setText(String.valueOf(profileViews));
            mCOuntViewsTextTV.setText(profileViewsText);

            mPopularityBarContainer.setVisibility(View.VISIBLE);
            mBlueBar.setLayoutParams(new LinearLayout.LayoutParams(UiUtils.dpToPx(10),
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    popularity));
        }
    }

    private boolean showPINudgeOverlay() {
        int counter = RFHandler.getInt(
                aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_PI_NUDGE_COUNTER, 0);
        if (counter >= 2 && !checkIfPINudgeTimeSatisfied()) {
            mShowPINudge = false;
            return false;
        }

        if (mPiNudgeOverlay == null) {
            mPiNudgeOverlay = mPiNudgeOverlayStub.inflate();
            mCountViewsContainer = findById(mPiNudgeOverlay, R.id.pi_count_views_container);
            mCountViewsTV = findById(mPiNudgeOverlay, R.id.pi_count_views);
            mCOuntViewsTextTV = findById(mPiNudgeOverlay, R.id.pi_count_views_text);
            mPopularityBarContainer = findById(mPiNudgeOverlay, R.id.pi_popularity_bar_container);
            mBlueBar = findById(mPiNudgeOverlay, R.id.pi_blue_bar);
            mTooltipIV = findById(mPiNudgeOverlay, R.id.pi_tooltip_iv);
            mTooltipView = findById(mPiNudgeOverlay, R.id.pi_activity_dashboard_tooltip_box);

            mShutterIV = findById(mPiNudgeOverlay, R.id.shutter_iv);
            mLeftLineIV = findById(mPiNudgeOverlay, R.id.left_line_iv);
            mRightLineIV = findById(mPiNudgeOverlay, R.id.right_line_iv);
            mCandidIV = findById(mPiNudgeOverlay, R.id.candid_iv);
            mExpressPassionIV = findById(mPiNudgeOverlay, R.id.express_passion_iv);
            mNaturalIV = findById(mPiNudgeOverlay, R.id.natural_iv);
            mChangePictureButton = findById(mPiNudgeOverlay, R.id.pi_nudge_change_picture_button);
            Picasso.with(aContext).load(R.drawable.camera_shutter).noFade().into(mShutterIV);
            Picasso.with(aContext).load(R.drawable.pi_line_left).noFade().into(mLeftLineIV);
            Picasso.with(aContext).load(R.drawable.pi_line_right).noFade().into(mRightLineIV);
            Picasso.with(aContext).load(R.drawable.express_passion).noFade().into(mExpressPassionIV);
            Picasso.with(aContext).load(R.drawable.candid_photo).noFade().into(mCandidIV);
            Picasso.with(aContext).load(R.drawable.natural_bg).noFade().into(mNaturalIV);
            Picasso.with(aContext).load(R.drawable.tooltip).noFade().into(mTooltipIV);

            mTooltipIV.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    mTooltipView.setVisibility(View.VISIBLE);
                }
            });
            mTooltipView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    //View.GONE is making the layout to change size. So, using View.INVISIBLE instead
                    mTooltipView.setVisibility(View.INVISIBLE);
                }
            });
            mChangePictureButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(Constants.PI_NUDGE_USER_PHOTOS_KEY, true);
                    ActivityHandler.startPhotosForResult(aActivity, false, bundle);
                }
            });

            mTooltipView.getLayoutParams().width = (int) (2 * (UiUtils.getScreenWidth(this) - UiUtils.dpToPx(32)) / 3);
        }
        setPopularityData(mPopularity, mProfileViews, mProfileViewsText);

        TrulyMadlyTrackEvent.trackEvent(aContext,
                TrulyMadlyActivities.matches, TrulyMadlyEventTypes.pi_activity_shown, 0,
                TrulyMadlyEventStatus.shown, null, true);
        mPiNudgeOverlay.setVisibility(View.VISIBLE);
        isPINudgeVisible = true;
        RFHandler.insert(aContext, Utility.getMyId(aContext),
                ConstantsRF.RIGID_FIELD_PI_NUDGE_SHOWN_TS, TimeUtils.getSystemTimeInMiliSeconds());
        counter++;
        RFHandler.insert(aContext, Utility.getMyId(aContext),
                ConstantsRF.RIGID_FIELD_PI_NUDGE_COUNTER, counter);
        if (counter >= 2) {
            mShowPINudge = false;
        }

        return true;
    }

    private boolean checkIfPINudgeTimeSatisfied() {
        long time = RFHandler.getLong(aContext, Utility.getMyId(aContext),
                ConstantsRF.RIGID_FIELD_PI_NUDGE_SHOWN_TS, -1L);
        return time == -1 || TimeUtils.isDifferenceGreater(aContext, ConstantsRF.RIGID_FIELD_PI_NUDGE_SHOWN_TS, Constants.PI_TIMEOUT);

    }

    private void hidePhotoAndPvNudge() {
        isPhotoAndPvNudgeShowing = false;
        actionBarHandler.setToolbarTransparent(true, "");
        if (photo_and_pv_nudge != null) {
            photo_and_pv_nudge.setVisibility(View.GONE);
        }
    }

    private void showPhotoAndPvNudgeSecondScreen(boolean showUploadSlider) {
        photo_upload_nudge_first_overlay.setVisibility(View.GONE);
        photo_upload_nudge_main_view.setVisibility(View.VISIBLE);
        photo_upload_nudge_main_view.fullScroll(View.FOCUS_UP);
        photo_upload_nudge_skipped_view.setVisibility(View.GONE);
        showPhotoUploadUi();

        if (showUploadSlider) {
            togglePhotoUploadSlider(true, true);
        }

        ViewGroup.LayoutParams params = photo_upload_image_container.getLayoutParams();
        params.height = (int) UiUtils.getScreenWidth(aActivity) - UiUtils.dpToPx(32);
        photo_upload_image_container.setLayoutParams(params);
    }

    private void showPhotoUploadUi() {
        photo_pv_close_button_skip.setVisibility(View.VISIBLE);
        photo_pv_close_button_done.setVisibility(View.GONE);
        photo_upload_iv.setVisibility(View.VISIBLE);
        photo_upload_progressbar.setVisibility(View.GONE);
        photo_upload_text.setText(R.string.upload_profile_pic);
    }

    private void showPhotoAndPvNudgeSkippedScreen() {
        photo_upload_nudge_first_overlay.setVisibility(View.GONE);
        photo_upload_nudge_main_view.setVisibility(View.GONE);
        photo_upload_nudge_skipped_view.setVisibility(View.VISIBLE);
    }

    private void showPhotoAndPvNudge(boolean onLikedClicked) {

        if (photo_and_pv_nudge == null) {
            photo_and_pv_nudge = photo_and_pv_nudge_stub.inflate();
            photo_upload_nudge_first_overlay = photo_and_pv_nudge.findViewById(R.id.photo_upload_nudge_first_overlay);
            photo_upload_nudge_first_overlay.setOnClickListener(this);
            hey_tv = (TextView) photo_and_pv_nudge.findViewById(R.id.hey_tv);
            not_now_tv = (TextView) photo_and_pv_nudge.findViewById(R.id.not_now_tv);
            not_now_tv.setOnClickListener(this);
            go_ahead_tv = (TextView) photo_and_pv_nudge.findViewById(R.id.go_ahead_tv);
            go_ahead_tv.setOnClickListener(this);
            got_it_tv = (TextView) photo_and_pv_nudge.findViewById(R.id.got_it_tv);
            got_it_tv.setOnClickListener(this);
            go_ahead_container = photo_and_pv_nudge.findViewById(R.id.go_ahead_container);
            photo_upload_nudge_main_view = (ScrollView) photo_and_pv_nudge.findViewById(R.id.photo_upload_nudge_main_view);
            photo_upload_image_container = (RelativeLayout) photo_and_pv_nudge.findViewById(R.id.photo_upload_image_container);
            upload_image_box = photo_and_pv_nudge.findViewById(R.id.upload_image_box);
            upload_image_box.setOnClickListener(this);
            photo_upload_iv = photo_and_pv_nudge.findViewById(R.id.photo_upload_iv);
            Picasso.with(this)
                    .load(R.drawable.camera).noFade().into((ImageView) photo_upload_iv);
            photo_upload_progressbar = (ProgressBar) photo_and_pv_nudge.findViewById(R.id.photo_upload_progressbar);
            photo_upload_text = (TextView) photo_and_pv_nudge.findViewById(R.id.photo_upload_text);
            profile_uploaded_image = (ImageView) photo_and_pv_nudge.findViewById(R.id.profile_uploaded_image);
            profile_uploaded_image.setOnClickListener(this);
            profile_visibility_options_header_text = (TextView) photo_and_pv_nudge.findViewById(R.id.profile_visibility_options_header_text);
            profile_visibility_options_radiogroup = (RadioGroup) photo_and_pv_nudge.findViewById(R.id.profile_visibility_options_radiogroup);
            profile_visibility_options_radiogroup.setOnCheckedChangeListener(this);
            profile_visibility_options_everyone = (RadioButton) photo_and_pv_nudge.findViewById(R.id.profile_visibility_options_everyone);
            profile_visibility_options_only_likes = (RadioButton) photo_and_pv_nudge.findViewById(R.id.profile_visibility_options_only_likes);
            profile_visibility_options_everyone.setChecked(true);
            if (SPHandler.getBool(aContext,
                    Constants.SHOW_DISCOVERY_OPTION)
                    && !Utility.isMale(aContext)) {
                if (!SPHandler.getBool(aContext,
                        Constants.DISCOVERY_OPTION, true)) {
                    profile_visibility_options_only_likes.setChecked(true);
                }
                saveDiscoveryOptionOnServer = false;
            }
            profile_visibility_container_overlay = photo_and_pv_nudge.findViewById(R.id.profile_visibility_container_overlay);
            profile_visibility_container_overlay.setOnClickListener(this);
            photo_pv_close_button_done = photo_and_pv_nudge.findViewById(R.id.photo_pv_close_button_done);
            photo_pv_close_button_done.setOnClickListener(this);
            photo_pv_close_button_skip = photo_and_pv_nudge.findViewById(R.id.photo_pv_close_button_skip);
            photo_pv_close_button_skip.setOnClickListener(this);
            photo_upload_slider_layout = photo_and_pv_nudge.findViewById(R.id.photo_upload_slider_layout);
            photo_upload_nudge_skipped_view = photo_and_pv_nudge.findViewById(R.id.photo_upload_nudge_skipped_view);
            photo_upload_nudge_skipped_view.setOnClickListener(this);
            photo_and_pv_nudge_skip_add_profile_pic = photo_and_pv_nudge.findViewById(R.id.photo_and_pv_nudge_skip_add_profile_pic);
            photo_and_pv_nudge_skip_add_profile_pic.setOnClickListener(this);
            photo_and_pv_nudge_skip_cancel = photo_and_pv_nudge.findViewById(R.id.photo_and_pv_nudge_skip_cancel);
            photo_and_pv_nudge_skip_cancel.setOnClickListener(this);
            View add_from_gallery_layout = photo_and_pv_nudge.findViewById(R.id.add_from_gallery_layout);
            add_from_gallery_layout.setOnClickListener(this);
            View take_from_camera_layout = photo_and_pv_nudge.findViewById(R.id.take_from_camera_layout);
            take_from_camera_layout.setOnClickListener(this);
        }

        String event_type = "overlay_like_button";
        int messageId = R.string.hey_you_need_a_profile_pic;
        if (!onLikedClicked) {

            if (!aMatchesSysMesgModal.isValidProfilePic() && showUploadPicDialogFirstTime()) {
                if (!aMatchesSysMesgModal.isNoPhotos()) {
                    if (aMatchesSysMesgModal.isLikedInThePast()) {
                        messageId = R.string.hey_couldnt_approve_pic_has_prev_like;
                        event_type = "overlay_previous_like";
                    } else {
                        messageId = R.string.hey_couldnt_approve_pic_has_prev_like;
                        event_type = "overlay_no_previous_like";
                    }
                } else {
                    messageId = R.string.no_profile_pic_message;
                    event_type = "overlay_no_uploaded_pic";

                }
            } else {
                return;
            }
        }

        hey_tv.setText(messageId);
        if (messageId == R.string.hey_you_need_a_profile_pic) {
            got_it_tv.setVisibility(View.GONE);
            go_ahead_container.setVisibility(View.VISIBLE);

        } else {
            got_it_tv.setVisibility(View.VISIBLE);
            go_ahead_container.setVisibility(View.GONE);
        }
        photo_upload_nudge_first_overlay.setVisibility(View.VISIBLE);
        photo_upload_nudge_main_view.setVisibility(View.GONE);
        photo_upload_nudge_skipped_view.setVisibility(View.GONE);

        TrulyMadlyTrackEvent.trackEvent(aContext,
                TrulyMadlyActivities.photo_flow, event_type, 0,
                null, null, true);
        actionBarHandler.setToolbarTransparent(false, getString(R.string.profiles));
        photo_and_pv_nudge.setVisibility(View.VISIBLE);
        isPhotoAndPvNudgeShowing = true;
        isPhotoNudgeSkip = true;
        profile_visibility_container_overlay.setVisibility(View.VISIBLE);
        profile_uploaded_image.setVisibility(View.GONE);
        photo_upload_progressbar.setVisibility(View.GONE);
    }

    private void showOverLay(String text, String button_type) {

        mProgressDialog = UiUtils.hideProgressBar(mProgressDialog);
        custom_prog_bar_id.setVisibility(View.VISIBLE);
        actionBarHandler.setToolbarTransparent(false, getString(R.string.profiles));
        if (Utility.isSet(text)) {
            saving_text.setText(text);
        }

        switch (button_type) {
            case "matches_loader":
                progress_bar_layout_id.setVisibility(View.VISIBLE);
//            miss_tm_loader.setVisibility(View.VISIBLE);
                miss_tm_arcloader.start();
                miss_tm_arcloader.setVisibility(View.VISIBLE);
                matches_overlay_button_id.setVisibility(View.GONE);
                retry_lay.setVisibility(View.GONE);
                if (mPiNudgeOverlay != null) {
                    mPiNudgeOverlay.setVisibility(View.GONE);
                }
                isPINudgeVisible = false;
                break;
            case "retry":
                progress_bar_layout_id.setVisibility(View.GONE);
//            miss_tm_loader.setVisibility(View.GONE);
                miss_tm_arcloader.stop();
                miss_tm_arcloader.setVisibility(View.GONE);
                matches_overlay_button_id.setVisibility(View.GONE);
                retry_lay.setVisibility(View.VISIBLE);
                error_message.setText(text);
                if (mPiNudgeOverlay != null) {
                    mPiNudgeOverlay.setVisibility(View.GONE);
                }
                isPINudgeVisible = false;
                break;
            case "upload_photo":
                progress_bar_layout_id.setVisibility(View.VISIBLE);
//            miss_tm_loader.setVisibility(View.GONE);
                miss_tm_arcloader.stop();
                miss_tm_arcloader.setVisibility(View.GONE);
                retry_lay.setVisibility(View.GONE);
                matches_overlay_button_id.setVisibility(View.VISIBLE);
                matches_overlay_button_id.setText(R.string.upload_photo);
                matches_overlay_button_id.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        ActivityHandler.startPhotosForResult(aActivity, false);
                    }
                });
                if (mPiNudgeOverlay != null) {
                    mPiNudgeOverlay.setVisibility(View.GONE);
                }
                isPINudgeVisible = false;
                break;
            case "invite_friends":
                progress_bar_layout_id.setVisibility(View.VISIBLE);
//            miss_tm_loader.setVisibility(View.GONE);
                miss_tm_arcloader.stop();
                miss_tm_arcloader.setVisibility(View.GONE);
                retry_lay.setVisibility(View.GONE);
                matches_overlay_button_id.setVisibility(View.GONE);
                if (mPiNudgeOverlay != null) {
                    mPiNudgeOverlay.setVisibility(View.GONE);
                }
                isPINudgeVisible = false;
                break;
            default:
                progress_bar_layout_id.setVisibility(View.VISIBLE);
//            miss_tm_loader.setVisibility(View.GONE);
                miss_tm_arcloader.stop();
                miss_tm_arcloader.setVisibility(View.GONE);
                retry_lay.setVisibility(View.GONE);
                matches_overlay_button_id.setVisibility(View.GONE);
                if (mPiNudgeOverlay != null) {
                    mPiNudgeOverlay.setVisibility(View.GONE);
                }
                isPINudgeVisible = false;
                break;
        }
    }

    private void responseParser(JSONObject response, boolean fetchProfileListFromDB) {
        if (response.optBoolean("isNewRecommendations") && !fetchProfileListFromDB) {
            MatchesDbHandler.clearMatchesCache(aContext);
        }

        if (!fetchProfileListFromDB) {
            RFHandler.insert(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_LAST_MATCHES_CALL_TSTAMP, TimeUtils.getSystemTimeInMiliSeconds());
        }

        SPHandler.setInt(aContext, ConstantsSP.SHARED_KEYS_APP_RATE_DIALOG, 0);
        //CompetitorTrackingHandling.trackCompetitors(this);

        profilesList = null;
        aMatchesLatestModal2 = null;
        ProfileNewResponseParser aProfileNewResponseParser = new ProfileNewResponseParser();
        aMatchesLatestModal2 = aProfileNewResponseParser.parseProfileNewResponse(response, aContext,
                true, !fetchProfileListFromDB, !fetchProfileListFromDB);

        mNudgeIndexes = aMatchesLatestModal2.getmFemaleNudgePositionsList();
        mNudgeMessages = aMatchesLatestModal2.getmFemaleNudgeMessagesList();

        aMyDetailModal = null;
        if (aMatchesLatestModal2 != null && aMatchesLatestModal2.getMyDetailModal() != null) {
            aMyDetailModal = aMatchesLatestModal2.getMyDetailModal();
        }

        if (fetchProfileListFromDB) {
            FetchMatchesFromDb task = new FetchMatchesFromDb(response);
            // to run  in parallel mode
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            if (aMatchesLatestModal2 != null && aMatchesLatestModal2.getProfileNewModalList() != null
                    && aMatchesLatestModal2.getProfileNewModalList().size() > 0) {
                profilesList = aMatchesLatestModal2.getProfileNewModalList();
            }
            calculateMatchesBeforeWeShowAd(getProfilesList());
            responseParserContinuation(response);
        }
    }

    private void calculateMatchesBeforeWeShowAd(Vector<ProfileNewModal> profileList) {
        if (profileList == null) {
            mMatchesBeforeWeShowProfileAd = 0;
        } else if (profileList.size() > 0) {
            if (profileList.size() <= getmMatchesBeforeWeShowProfileAd()) {
                // Values can be : 1, 2, 3, ... , FINAL_POSITION
                // corresponding to the profile list size 1, 2, 3, ... , some_value
                mMatchesBeforeWeShowProfileAd = profileList.size();
            }
        } else {
            mMatchesBeforeWeShowProfileAd = 0;
        }
    }

    private void responseParserContinuation(JSONObject response) {
        miss_tm_arcloader.stop();
        miss_tm_arcloader.setVisibility(View.GONE);
        custom_prog_bar_id.setVisibility(View.GONE);
        mProgressDialog = UiUtils.hideProgressBar(mProgressDialog);

        if (aMyDetailModal != null) {
            //CITY
            settingMoengageAttribute("City", aMyDetailModal.getCity());
            SPHandler.setString(aContext, ConstantsSP.SHARED_KEYS_USER_CITY, aMyDetailModal.getCity());

            //STATE
            String state = response.optJSONObject("my_data").optString("state");
            settingMoengageAttribute("State", state);

            // NAME
            SPHandler.setString(aContext, ConstantsSP.SHARED_KEYS_USER_NAME, aMyDetailModal.getName());
            settingMoengageAttribute(MoEHelperConstants.USER_ATTRIBUTE_USER_NAME, aMyDetailModal.getName());
            settingMoengageAttribute("UserStatus", SPHandler.getString(aContext, ConstantsSP.SHARED_KEYS_USER_STATUS));


            // GENDER
            boolean isGenderMale = response.optJSONObject("my_data").optString("gender").equalsIgnoreCase("m");
            if (isGenderMale) {
                settingMoengageAttribute(MoEHelperConstants.USER_ATTRIBUTE_USER_GENDER,
                        MoEHelperConstants.GENDER_MALE);
//                InMobiSdk.setGender(InMobiSdk.Gender.MALE);
            } else {
//                InMobiSdk.setGender(InMobiSdk.Gender.FEMALE);
                settingMoengageAttribute(MoEHelperConstants.USER_ATTRIBUTE_USER_GENDER,
                        MoEHelperConstants.GENDER_FEMALE);
            }

            //Age
            settingMoengageAttribute("Age", aMyDetailModal.getAge());
            SPHandler.setString(aContext, ConstantsSP.SHARED_KEYS_USER_AGE, aMyDetailModal.getAge());
            try {
                int age = Integer.parseInt(aMyDetailModal.getAge());
//                InMobiSdk.setAge(age);
            } catch (NumberFormatException ignored) {
            }

            //PROFILE PIC
            SPHandler.setString(aContext, ConstantsSP.SHARED_KEYS_USER_PROFILE_THUMB_URL,
                    aMyDetailModal.getProfile_pic_url());
            SPHandler.setString(aContext, ConstantsSP.SHARED_KEYS_USER_PROFILE_FULL_URL,
                    aMyDetailModal.getProfile_pic_full_url());


            // updating user for registration  event
            if (!Utility.isSet(SPHandler.getString(aContext, ConstantsSP.SHARED_KEYS_IS_REGISTRATION_COMPLETE))
                    && aMyDetailModal.getStatus().equalsIgnoreCase("authentic")) {
                SPHandler.setString(aContext, ConstantsSP.SHARED_KEYS_IS_REGISTRATION_COMPLETE, "true");

                if (SPHandler.getBool(aContext, ConstantsSP.SHARED_KEYS_IS_NEW_USER, false)) {
                    SPHandler.setBool(aContext, ConstantsSP.SHARED_KEYS_IS_NEW_USER, false);
                }
            }
            SPHandler.setString(aContext, ConstantsSP.SHARED_KEYS_USER_STATUS, response.optJSONObject("my_data").optString("status"));
            if (aMyDetailModal.getStatus().equalsIgnoreCase("non-authentic")) {
                ActivityHandler.startTrustBuilder(aActivity, false, true);
                finish();
            }
        }
        aMatchesSysMesgModal = null;

        if (aMatchesLatestModal2 != null && aMatchesLatestModal2.getMatchesSysMesgModal() != null)
            aMatchesSysMesgModal = aMatchesLatestModal2.getMatchesSysMesgModal();

        if (aMatchesSysMesgModal != null && Utility.isSet(aMatchesSysMesgModal.getLastTileMsg())) {
            // check if profile_list is null in that case we need to
            // create one manually
//            if (getProfilesList() == null) {
//                profilesList = new Vector<>();
//            }
//            ProfileNewModal aDummyProfile = new ProfileNewModal();
//            aDummyProfile.setIsLastTile(true);
//            profilesList.add(aDummyProfile);
        }

        if (getProfilesList() != null && getProfilesList().size() > 0) {

            // for app rate
            if (getProfilesList().size() > 1) {
                int isAppRate = SPHandler.getInt(aContext, ConstantsSP.SHARED_KEYS_APP_RATE_DIALOG);
                if (isAppRate != 1)
                    SPHandler.setInt(aContext, ConstantsSP.SHARED_KEYS_APP_RATE_DIALOG, 1);
            }

            // fetching 4 profile pic url
//			countUrls = 0;
//
//			while (countUrls < 4) {
//				prefetchPhotos();
//			}

            lastFetchedIndex = -1;
            prefetchPhotos(PREFETCH_MODE.initial, 4);

            setRootLayout();
            matches_swiped = 0;
            likes_done = 0;
            hides_done = 0;
            hides_done_consecutive = 0;
            sparks_done = 0;
            mNextAdPosition = INITIAL_AD_POSITION;

            if (isProfileAdVisible || !showLastTile()) {
                profile_like_hide_lay.setVisibility(View.VISIBLE);
            }

            if (aMatchesSysMesgModal != null && Utility.isSet(aMatchesSysMesgModal.getFirstTileMsg())) {
                String text = aMatchesSysMesgModal.getFirstTileMsg();
                String button_type = aMatchesSysMesgModal.getFirstTileLink();

                if (button_type.equals("upload_photo")) {
                    if (Utility.isMale(aContext)) {
                        showOverLay(text, button_type);
                        SPHandler.setBool(aContext, Constants.SHOW_DISCOVERY_OPTION, false);
                    } else {
                        isProfilePicRejectedFemale = true;
                        SPHandler.setBool(aContext, Constants.SHOW_DISCOVERY_OPTION, true);
                        showPhotoAndPvNudge(false);
                        if (getProfilesList().size() > 1 && isResumed) {
                            startTutorials();
                        }
                        SPHandler.setBool(aContext, ConstantsSP.SHARED_KEYS_PROFILE_PIC_REJECTED, true);
                    }
                } else {
                    SPHandler.setBool(aContext, ConstantsSP.SHARED_KEYS_PROFILE_PIC_REJECTED, false);
                    showOverLay(text, button_type);
                }
            } else if (getProfilesList().size() > 1 && isResumed) {
                SPHandler.setBool(aContext, ConstantsSP.SHARED_KEYS_PROFILE_PIC_REJECTED, false);
                startTutorials();
            }

            // setting weekly 7 day timeout for this un hide me message.
            if (SPHandler.getBool(aContext, Constants.SHOW_DISCOVERY_OPTION)
                    && !SPHandler.getBool(aContext, Constants.DISCOVERY_OPTION, true)
                    && (TimeUtils.isTimeoutExpired(aContext, ConstantsSP.SHARED_KEYS_LAST_HIDE_ME_OFF_TOAST_TIMESTAMP,
                    7 * TimeUtils.HOURS_24_IN_MILLIS))) {
                SPHandler.setString(aContext, ConstantsSP.SHARED_KEYS_LAST_HIDE_ME_OFF_TOAST_TIMESTAMP,
                        TimeUtils.getFormattedTime());
                AlertsHandler.showMessage(aActivity, R.string.turn_on_profile_visibility, false);
            }
        }

    }

    private void settingMoengageAttribute(String key, String value) {
        if (Utility.isSet(value)) {
            MoEHelper.getInstance(aContext).setUserAttribute(key, value);
        }
    }

    private HashMap<String, String> createParamsForMatchesCall(int batchCount) {
        HashMap<String, String> params = new HashMap<>();

        params.put("isMessageMatchMergedList", "true");
        params.put("mutual_friends", "true");
        params.put("device_id", Utility.getDeviceId(aContext));
        params.put("batchCount", "" + batchCount);
        params.put("fav_v2", "true");
        String matchId = SPHandler.getString(aContext, ConstantsSP.SHARED_KEY_SELECT_SAVED_MATCH_ID);
        String hash = SPHandler.getString(aContext, ConstantsSP.SHARED_KEY_SELECT_SAVED_SELECT_HASH);
        if (Utility.isSet(matchId) && TMSelectHandler.isSelectMember(aContext)
                && TMSelectHandler.isSelectQuizPlayed(aContext) && Utility.isSet(hash)) {
            params.put("select_match_id", matchId);
            params.put("select_profile_hash", hash);
        }
        if (batchCount == 2) {
            params.put("fetch_all_matches", "true");
        }
        return params;
    }

    private Vector<ProfileNewModal> getProfilesList() {
        if (profilesList == null) {
            profilesList = new Vector<>();
        }

        if (profilesList.size() == 0 || !profilesList.get(profilesList.size() - 1).isLastTile()) {
            ProfileNewModal aDummyProfile = new ProfileNewModal();
            aDummyProfile.setIsLastTile(true);
            profilesList.add(profilesList.size(), aDummyProfile);
        }

        return profilesList;
    }

    private void addProfile(ProfileNewModal profileNewModal) {
        if (profileNewModal != null && !profileNewModal.isLastTile()) {
            getProfilesList().add(getProfilesList().size() - 1, profileNewModal);
        }
    }

    private void issueRequest(final int n) {
        CustomOkHttpResponseHandler okHttpResponseHandler = new CustomOkHttpResponseHandler(aContext,
                TrulyMadlyActivities.matches, TrulyMadlyEventTypes.page_load) {

            @Override
            public void onRequestSuccess(JSONObject response) {
                switch (n) {
                    case MATCH_NEW:
                        try {
                            RFHandler.insert(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_HAS_MORE_MATCHES, true);
                            makeSecondMatchesCall = true;
                            responseParser(response, false);
                            response.put("data", new JSONArray());
                            SPHandler.setString(aContext, ConstantsSP.SHARED_KEY_MATCHES_RESPONSE, response.toString());

                            if (TMSelectHandler.isSelectMember(aContext)
                                    && TMSelectHandler.isSelectQuizPlayed(aContext)) {
                                SPHandler.remove(aContext, ConstantsSP.SHARED_KEY_SELECT_SAVED_MATCH_ID);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        break;
                    case UPDATE_NOTIFICATION_COUNTER:
                        parseNotificationCounterResponse(response);
                        break;
                    case USER_FLAGS:
                        parseUserFlags(response);
                        actionBarHandler.setScenesBlooperVisibility(RFHandler.getBool(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_SHOW_TM_SCENES_BLOOPER));
                        //issueRequest(MATCH_NEW);
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onRequestFailure(Exception exception) {
                switch (n) {
                    case MATCH_NEW:
                        showOverLay(getString(Utility.getNetworkErrorUiStringResId(aActivity, exception)), "retry");
                        break;
//                    case USER_FLAGS:
//                        //issueRequest(MATCH_NEW);
//                        break;
                    default:
                        break;
                }

            }

        };

        Map<String, String> params;
        params = new HashMap<>();
        switch (n) {
            case MATCH_NEW:
                showOverLay(getString(R.string.matches_loading_text), "matches_loader");
                // check to make call or not
                if (makeMatchesCall()) {
                    OkHttpHandler.httpGet(aContext, ConstantsUrls.get_matches_url(), createParamsForMatchesCall(1), okHttpResponseHandler);
                } else {

                    String sharedString = SPHandler.getString(aContext, ConstantsSP.SHARED_KEY_MATCHES_RESPONSE);
                    if (Utility.isSet(sharedString)) {
                        JSONObject response = null;
                        try {
                            response = new JSONObject(sharedString);
                            if (response != null) {
                                responseParser(response, true);
                            } else {
                                OkHttpHandler.httpGet(aContext, ConstantsUrls.get_matches_url(), createParamsForMatchesCall(1), okHttpResponseHandler);
                            }
                        } catch (JSONException e) {
                            OkHttpHandler.httpGet(aContext, ConstantsUrls.get_matches_url(), createParamsForMatchesCall(1), okHttpResponseHandler);

                        }
                    } else {
                        OkHttpHandler.httpGet(aContext, ConstantsUrls.get_matches_url(), createParamsForMatchesCall(1), okHttpResponseHandler);
                    }

                }
                break;
            case UPDATE_NOTIFICATION_COUNTER:
                params.put("get_notification_counter", "1");
                params.put("isMessageMatchMergedList", "true");
                params.put("device_id", Utility.getDeviceId(aContext));
                okHttpResponseHandler.setActivity("");
                OkHttpHandler.httpGet(aContext, ConstantsUrls.get_NOTIFICATION_URL(), params, okHttpResponseHandler);
                break;
            case USER_FLAGS:
                String time = RFHandler.getString(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_TM_SCENES_NEXT_TSTAMP);
                RFHandler.insert(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_TM_SCENES_CURRENT_TSTAMP, time);
                params.put("last_seen_scene", time);
                showOverLay(getString(R.string.matches_loading_text), "matches_loader");
                okHttpResponseHandler.setActivity("");
                OkHttpHandler.httpGet(aContext, ConstantsUrls.get_USER_FLAGS_URL(), params, okHttpResponseHandler);
                issueRequest(MATCH_NEW);
                break;
            default:
                break;
        }
    }

    private boolean makeMatchesCall() {
        return TimeUtils.isDifferenceGreater(aContext, ConstantsRF.RIGID_FIELD_LAST_MATCHES_CALL_TSTAMP, MATCHES_CALL_INTERVAL);
    }

    private boolean showUploadPicDialog() {
        return TimeUtils.isDifferenceGreater(aContext, ConstantsRF.RIGID_FIELD_LAST_UPLOAD_PIC_AFTER_LIKE_TSTAMP, UPLOAD_PROFILE_PIC_AFTER_LIKE_INTERVAL);
    }

    private boolean showUploadPicDialogFirstTime() {
        int count = RFHandler.getInt(aContext, Constants.NO_OF_TIMES_UPLOAD_PIC_DIALOG_SHOWN, 0);
        if (count < MAX_NO_OF_TIMES_DIALOG_SHOWN && TimeUtils.isDifferenceGreater(aContext, ConstantsRF.RIGID_FIELD_UPLOAD_FIRST_TIME_TSTAMP, UPLOAD_PROFILE_PIC_FIRST_TIME)) {
            RFHandler.insert(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_UPLOAD_FIRST_TIME_TSTAMP, TimeUtils.getSystemTimeInMiliSeconds());
            RFHandler.insert(aContext, Utility.getMyId(aContext), Constants.NO_OF_TIMES_UPLOAD_PIC_DIALOG_SHOWN, count + 1);
            return true;
        } else
            return false;
    }

    private boolean makeBackgroundMatchesCall() {
        return RFHandler.getBool(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_HAS_MORE_MATCHES);
    }

    private void networkRetryHandle() {
        if (Utility.isNetworkAvailable(aContext)) {
            issueRequest(MATCH_NEW);
        } else
            AlertsHandler.showMessage(aActivity, R.string.ERROR_NETWORK_FAILURE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        isFromOnActivityResult = false;


        if (mSparksBillingController != null && mSparksBillingController.handleActivityResult(requestCode, resultCode, data)) {
            isFromOnActivityResult = true;
            TmLogger.d("SparksBillingHandler", "onActivityResult handled by SparkBillingHandler.");
            return;
        }

        switch (requestCode) {
            case Constants.GET_PHOTOS_FROM_ACTIVITY:
                isFromOnActivityResult = true;
                if (data != null && data.getExtras() != null) {
                    if (isPINudgeEnabled && data.getBooleanExtra(Constants.PI_NUDGE_USER_PHOTOS_KEY, false)) {
                        TrulyMadlyTrackEvent.trackEvent(aContext,
                                TrulyMadlyActivities.matches, TrulyMadlyEventTypes.pi_activity_continue_photo, 0,
                                TrulyMadlyEventStatus.success, null, true);
                        RFHandler.insert(aContext, Utility.getMyId(aContext),
                                ConstantsRF.RIGID_FIELD_PI_NUDGE_COUNTER, -1);
                        isPINudgeEnabled = false;
                        mShowPINudge = false;
                        if (mPiNudgeOverlay != null) {
                            mPiNudgeOverlay.setVisibility(View.GONE);
                        }
                        isPINudgeVisible = false;
                    }
                    if (data.getExtras().getInt("photosLen") != 0) {
                        issueRequest(USER_FLAGS);
                    }
                }
                break;
            case Constants.PICK_FROM_CAMERA_OWN:
            case Constants.PICK_IMAGE_FROM_SYSTEM:
            case Constants.PICK_FROM_GALLERY:
                photoUploader.onActivityResult(requestCode, resultCode, data);
                break;
            case Constants.FROM_FULL_ALBUM_PAGE:
            case Constants.BUY_PACKAGES_REQUEST_CODE:
                isFromOnActivityResult = true;
                break;

            default:
                break;
        }
    }

    // for like hide
    private void showLikeHideDialog(final HttpRequestType httpReqType) {

        if (getProfilesList() != null && getProfilesList().size() > 1 && isLikeHideClickable) {

            final String linkLike = getProfilesList().elementAt(0).getLinkLike();
            final String linkHide = getProfilesList().elementAt(0).getLinkHide();
            final int mf_count = getProfilesList().elementAt(0).getFbMutualFriendCount();
            String userId = getProfilesList().elementAt(0).getUser().getUserId();
            boolean hasLikedBefore = getProfilesList().get(0).getUser() != null &&
                    getProfilesList().elementAt(0).getUser().isHasLikedBefore();
            int count = getProfilesList().elementAt(0).getEventsImages() == null ? 0 : getProfilesList().elementAt(0).getEventsImages().length;
            boolean isSelectMember = getProfilesList().get(0).isTMSelectMember();

            if (httpReqType == HttpRequestType.MATCHES_LIKE) {
//                if (isProfilePicRejectedFemale) {
//                    showPhotoAndPvNudge(true);
//                } else {

                customDialogLikeHideIcon(true, false);
                issueRequestLikeHide(httpReqType, linkLike, TrulyMadlyActivities.matches, count, userId, mf_count, hasLikedBefore, isSelectMember);
//                }
            } else if (httpReqType == HttpRequestType.MATCHES_HIDE) {

                customDialogLikeHideIcon(false, false);
                issueRequestLikeHide(httpReqType, linkHide, TrulyMadlyActivities.matches, count, userId, mf_count, hasLikedBefore, isSelectMember);

            }
        } else {
            showLastTile();
        }

    }

    private void dismissLikeHideIconDialog() {
        if (likeHideIconDialog != null && likeHideIconDialog.isShowing()) {
            try {
                likeHideIconDialog.dismiss();
            } catch (IllegalArgumentException ignored) {

            }
        }
    }

    private void issueRequestLikeHide(final HttpRequestType httpReqType, String url, String eventActivity,
                                      final int noOfMutualEvents, final String matchId,
                                      final int mf_count, boolean hasLikedBefore, boolean isSelectMember) {
        if (getProfilesList() != null && getProfilesList().size() < 1) {
            showLastTile();
        } else {

            String eventType = null;

            if (httpReqType == HttpRequestType.MATCHES_LIKE)
                eventType = TrulyMadlyEventTypes.like;
            else if (httpReqType == HttpRequestType.MATCHES_HIDE)
                eventType = TrulyMadlyEventTypes.hide;

            if (!mTrackingMap.containsKey(EventInfoUpdateEvent.getKeyString(EventInfoUpdateEvent.KEY_VIDEOS_VIEWED))) {
                mTrackingMap.put(EventInfoUpdateEvent.getKeyString(EventInfoUpdateEvent.KEY_VIDEOS_VIEWED), "false");
            }

            if (!mTrackingMap.containsKey(EventInfoUpdateEvent.getKeyString(EventInfoUpdateEvent.KEY_PHOTOS_COUNT))) {
                mTrackingMap.put(EventInfoUpdateEvent.getKeyString(EventInfoUpdateEvent.KEY_PHOTOS_COUNT), "0");
            }

            if (!mTrackingMap.containsKey(EventInfoUpdateEvent.getKeyString(EventInfoUpdateEvent.KEY_VIDEOS_COUNT))) {
                mTrackingMap.put(EventInfoUpdateEvent.getKeyString(EventInfoUpdateEvent.KEY_VIDEOS_COUNT), "0");
            }

            if (!mTrackingMap.containsKey(EventInfoUpdateEvent.getKeyString(EventInfoUpdateEvent.KEY_FAVORITES_VIEWED))) {
                mTrackingMap.put(EventInfoUpdateEvent.getKeyString(EventInfoUpdateEvent.KEY_FAVORITES_VIEWED), "false");
            }

            if (!mTrackingMap.containsKey(EventInfoUpdateEvent.getKeyString(EventInfoUpdateEvent.KEY_TRUST_SCORE_VIEWED))) {
                mTrackingMap.put(EventInfoUpdateEvent.getKeyString(EventInfoUpdateEvent.KEY_TRUST_SCORE_VIEWED), "false");
            }

            if (!mTrackingMap.containsKey(EventInfoUpdateEvent.getKeyString(EventInfoUpdateEvent.KEY_TRUST_SCORE))) {
                mTrackingMap.put(EventInfoUpdateEvent.getKeyString(EventInfoUpdateEvent.KEY_TRUST_SCORE), "0");
            }

            if (!mTrackingMap.containsKey("has_liked_before")) {
                mTrackingMap.put("has_liked_before", String.valueOf(hasLikedBefore));
            }

            if (noOfMutualEvents > 0) {
                mTrackingMap.put("event_list", "" + noOfMutualEvents);
            }

            mTrackingMap.put("is_select_member", String.valueOf(TMSelectHandler.isSelectMember(aContext)));
            mTrackingMap.put("is_profile_select", String.valueOf(isSelectMember));

            @SuppressWarnings("unchecked") HashMap<String, String> eventInfo = (HashMap<String, String>) mTrackingMap.clone();
            CustomOkHttpResponseHandler okHttpResponseHandler = new CustomOkHttpResponseHandler(aContext, eventActivity,
                    eventType, null, eventInfo) {

                @Override
                public void onRequestSuccess(JSONObject response) {

                    MatchLikeHideResponseParser aMatchLikeHideResponseParser = new MatchLikeHideResponseParser();
                    boolean isMutualMatchConverted = aMatchLikeHideResponseParser.parseMatchLikeHideResponse(response);
                    HashMap<String, String> eventInfo = new HashMap<>();
                    eventInfo.put("event_list", "" + noOfMutualEvents);
                    if (noOfMutualEvents > 0) {
                        if (httpReqType == HttpRequestType.MATCHES_HIDE) {
                            TrulyMadlyTrackEvent.trackEvent(aActivity,
                                    TrulyMadlyEvent.TrulyMadlyActivities.matches, hide_matchpage, 0,
                                    matchId, eventInfo, true);
                        } else if (httpReqType == HttpRequestType.MATCHES_LIKE) {
                            TrulyMadlyTrackEvent.trackEvent(aActivity,
                                    TrulyMadlyEvent.TrulyMadlyActivities.matches, like_matchpage, 0,
                                    matchId, eventInfo, true);
                        }
                    }
                    if (isMutualMatchConverted) {
                        if (httpReqType == HttpRequestType.MATCHES_LIKE) {
                            actionBarHandler.incConversationsCount();
                            fetchSparksForSparkBlooper(false);

                        } else if (httpReqType == HttpRequestType.MATCHES_HIDE) {

                        }
                    }
                }

                @Override
                public void onRequestFailure(Exception exception) {
                    AlertsHandler.showNetworkError(aActivity, exception);
                }

            };

            Map<String, String> params = GetOkHttpRequestParams.getHttpRequestParams(httpReqType);


            if ((httpReqType == HttpRequestType.MATCHES_HIDE || httpReqType == HttpRequestType.MATCHES_LIKE))
                params.put("mf_shown", mf_count > 0 ? "true" : "false");

            params.put("has_liked_before", String.valueOf(hasLikedBefore));

            OkHttpHandler.httpGet(aContext, url, params,
                    okHttpResponseHandler);
        }
    }

    private void issueRequestUploadPhoto(HttpRequestType type, String filePath) {
        if (photoUploader.isInProgress()) {
            //Utility.showMessage(aActivity, R.string.cant_upload_photo_yet);
            return;
        }
        photo_upload_iv.setVisibility(View.INVISIBLE);
        photo_upload_text.setText(R.string.uploading_profile_pic);
        photo_upload_progressbar.setVisibility(View.VISIBLE);
        photo_pv_close_button_skip.setVisibility(View.GONE);
        photo_pv_close_button_done.setVisibility(View.GONE);
        photoUploader.startPhotoUpload(filePath, type, null, TrulyMadlyActivities.matches, PhotoUploader.UPLOAD_TYPE.PICTURE, null);
    }

    private void onSuccessUploadPhoto() {
        isPhotoNudgeSkip = false;
        isProfilePicRejectedFemale = false;
        profile_visibility_container_overlay.setVisibility(View.GONE);

        //UiUtils.setBackground(aContext, profile_visibility_options_everyone, R.drawable.item_with_no_border_white_bg);
        //UiUtils.setBackground(aContext, profile_visibility_options_only_likes, R.drawable.item_with_no_border_white_bg);
        profile_visibility_options_header_text.setTextColor(getResources().getColor(R.color.text_grey_enabled));
        profile_visibility_options_everyone.setTextColor(getResources().getColor(R.color.text_grey_enabled));
        profile_visibility_options_only_likes.setTextColor(getResources().getColor(R.color.text_grey_enabled));
        profile_visibility_options_everyone.setCompoundDrawablesWithIntrinsicBounds(R.drawable.all_match_enabled, 0, R.drawable.green_checkbox, 0);
        profile_visibility_options_only_likes.setCompoundDrawablesWithIntrinsicBounds(R.drawable.only_my_matches_enabled, 0, R.drawable.green_checkbox, 0);

        profile_uploaded_image.setVisibility(View.VISIBLE);
        photo_upload_progressbar.setVisibility(View.GONE);
        photo_pv_close_button_skip.setVisibility(View.GONE);
        photo_pv_close_button_done.setVisibility(View.VISIBLE);
        TrulyMadlyTrackEvent.trackEvent(aContext,
                TrulyMadlyActivities.photo_flow, "photo_upload", 0,
                null, null, true);


    }

    private boolean showLastTile() {
        dismissLikeHideIconDialog();
        if (getProfilesList() != null && getProfilesList().size() == 1) {
            MatchesDbHandler.clearMatchesCache(aContext);
            actionBarHandler.setToolbarTransparent(false, getString(R.string.profiles));
            profile_like_hide_lay.setVisibility(View.GONE);
            HashMap<String, String> eventInfo = new HashMap<>();
            eventInfo.put(ad_feasible, String.valueOf(setAdAndShow()));
            TrulyMadlyTrackEvent.trackEvent(aContext,
                    TrulyMadlyActivities.matches, TrulyMadlyEventTypes.matches_last, 0,
                    null, eventInfo, true);
            return true;
        }
        return false;
    }

    private boolean setAdAndShow() {
        if (getProfilesList() != null && getProfilesList().size() == 1 && !isProfileAdVisible) {
            if ((!isPINudgeEnabled || !shouldPINudgeBeShown(false))
                    && !isPINudgeVisible && !isSelectNudgeVisible) {
                if(mAdsHelper.areConstraintsSatisfied(AdsType.MATCHES_END_INTERSTITIAL
                        , TrulyMadlyActivities.matches)) {
                    isInterstitialToBeShown = false;
                    showAd = true;
                    mHandler.postDelayed(mShowAdRunnableNew, 0);
                    return true;
                }else{
                    isInterstitialToBeShown = true;
                }
            }
        }

        return false;
    }

    private Object getListener(SparksHandler.SparksDialogType dialogType) {
        Object object = null;
        switch (dialogType) {
            case introducing_sparks:
            case one_spark_left:
            case spark_purchased:
                object = mSimpleDialogActionListener;
                break;
            case buy_spark:
                object = mTrackingBuySparkEventListener;
                break;
            case send_spark:
                object = mSendSparkEventListener;
                break;
        }

        return object;
    }

    private void toggleFragment(boolean showFragment, SparksHandler.SparksDialogType dialogType,
                                int newSparksAdded, int totalSparks, boolean fromIntro,
                                boolean isRelationshipExpertAdded, boolean mHasLikedBefore) {
        if (showFragment) {
            String sparkHash = null;
            try {
                sparkHash = getProfilesList().get(0).getUser().getmSparkHash();
            } catch (NullPointerException ignored) {
            }
            mCurrentFragment = SparksHandler.toggleFragment(this, mCurrentFragment, getListener(dialogType),
                    getProfilesList().get(0).getUserId(), sparkHash,
                    showFragment, dialogType, newSparksAdded, totalSparks, fromIntro,
                    false, null, likes_done, hides_done, sparks_done,
                    Utility.stringCompare(mTrackingMap.get(EventInfoUpdateEvent.getKeyString(EventInfoUpdateEvent.KEY_FAVORITES_VIEWED)), "true"),
                    isRelationshipExpertAdded, mHasLikedBefore, getProfilesList().get(0).isTMSelectMember());

            mSparksDialogContainer.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            mSparksDialogContainer.setVisibility(View.VISIBLE);
            isSparkDialogVisible = true;
            slidingMenu.toggleVisibility(false);
        } else {
            isSparkDialogVisible = false;
            mSparksDialogContainer.setVisibility(View.GONE);
            slidingMenu.toggleVisibility(true);
        }
    }

    @OnClick({R.id.spark_lay, R.id.fb_friend_container})
    @Override
    public void onClick(View arg0) {
        int id = arg0.getId();
        int AD_INTERVAL = 2;
        switch (id) {
            case R.id.not_now_tv:
                photo_upload_nudge_first_overlay.setVisibility(View.GONE);
                hidePhotoAndPvNudge();
                break;
            case R.id.go_ahead_tv:
            case R.id.got_it_tv:
                showPhotoAndPvNudgeSecondScreen(false);
                break;
            case R.id.photo_and_pv_nudge_skip_cancel:
                TrulyMadlyTrackEvent.trackEvent(aContext,
                        TrulyMadlyActivities.photo_flow, "privacy_no_thanks", 0,
                        null, null, true);
                hidePhotoAndPvNudge();
                break;
            case R.id.photo_and_pv_nudge_skip_add_profile_pic:
                TrulyMadlyTrackEvent.trackEvent(aContext,
                        TrulyMadlyActivities.photo_flow, "privacy_add_pic", 0,
                        null, null, true);
                showPhotoAndPvNudgeSecondScreen(true);
                break;

            case R.id.retry_lay:
                networkRetryHandle();
                break;

            case R.id.auth_swipe_lay:
                auth_swipe_lay.setVisibility(View.GONE);
                break;

            case R.id.like_lay:
                if (getProfilesList() != null && getProfilesList().size() > 0 && getProfilesList().get(0) != null
                        && !getProfilesList().get(0).isSponsoredProfile() && !isProfileAdVisible) {
                    UserModal user = getProfilesList().get(0).getUser();
                    if (TMSelectHandler.isSelectEnabled(aContext) && getProfilesList().get(0).isTMSelectMember() &&
                            !TMSelectHandler.isSelectMember(aContext) && !TMSelectHandler.isLikeAllowedOnSelect(aContext)) {
                        showOnActionSelectNudge(
                                SPHandler.getString(aContext,
                                        ConstantsSP.SHARED_KEYS_USER_PROFILE_FULL_URL),
                                getProfilesList().get(0).getUser().getProfilePic(), "like");
                        return;
                    } else if (user != null && user.isHasLikedBefore()) {
                        toggleWhyNotSparkInsteadToast(true);
                        return;
                    } else if (SPHandler.getBool(aContext, ConstantsSP.SHARED_KEY_WHY_NOT_SPARK_INSTEAD_ENABLED)) {
                        int count = SPHandler.getInt(aContext, ConstantsSP.SHARED_KEY_WHY_NOT_SPARK_INSTEAD_COUNT,
                                Constants.MATCHES_COUNT_AFTER_WHICH_SPARK_NUDGE_SHOWN);
                        if (user != null && user.isHasLikedBefore() && !isHasLikedBeforeNudgeShownForThisMatch) {
                            isHasLikedBeforeNudgeShownForThisMatch = true;
                            showWhyNotSparkInsteadDialog(user);
                            return;
                        } else if (likes_done >= count) {
                            long lastTimeStamp = -1L;
                            String lastTimeStampString = SPHandler.getString(aContext, ConstantsSP.SHARED_KEY_WHY_NOT_SPARK_INSTEAD_TS);
                            if (Utility.isSet(lastTimeStampString)) {
                                lastTimeStamp = Long.parseLong(lastTimeStampString);
                            }
                            if (TimeUtils.isDayChanged(lastTimeStamp)) {
                                SPHandler.remove(aContext, ConstantsSP.SHARED_KEY_WHY_NOT_SPARK_INSTEAD_TS);
                                lastTimeStamp = -1;
                            }
                            if (lastTimeStamp == -1) {
                                SPHandler.setString(aContext, ConstantsSP.SHARED_KEY_WHY_NOT_SPARK_INSTEAD_TS,
                                        String.valueOf(Calendar.getInstance().getTimeInMillis()));
                                showWhyNotSparkInsteadDialog(user);

                                return;
                            }
                        }
                    }
                }

//                if(likes_done > 0 && likes_done%10 == 0 && !isAsked) {
//                    AlertsHandler.showConfirmDialog(this, R.string.why_not_sparks_instead,
//                            R.string.send_spark_button, R.string.send_like_button, new ConfirmDialogInterface() {
//                                @Override
//                                public void onPositiveButtonSelected() {
//                                    onClick(mSparkFAB);
//                                    isAsked = false;
//                                }
//
//                                @Override
//                                public void onNegativeButtonSelected() {
//                                    onClick(mLikeFAB);
//                                    isAsked = false;
//                                }
//                            });
//                    isAsked = true;
//                    return;
//                }
//                isAsked = false;

                resetSparksFAB();
                if (isProfileAdVisible) {
                    mNextAdPosition += AD_INTERVAL;
                    customDialogLikeHideIcon(true, false);
                    break;
                }
                if (Utility.isNetworkAvailable(this)) {
                    if (aMyDetailModal != null && Utility.isSet(aMyDetailModal.getStatus())
                            && aMyDetailModal.getStatus().equals("authentic")) {

                        showLikeHideDialog(HttpRequestType.MATCHES_LIKE);
                    }
                } else {
                    AlertsHandler.showMessage(aActivity, R.string.ERROR_NETWORK_FAILURE);
                }

                break;

            case R.id.spark_lay:
                if (getProfilesList() != null && getProfilesList().size() > 0 && getProfilesList().get(0) != null
                        && !getProfilesList().get(0).isSponsoredProfile() && !isProfileAdVisible
                        && TMSelectHandler.isSelectEnabled(aContext)
                        && getProfilesList().get(0).isTMSelectMember() && !TMSelectHandler.isSelectMember(aContext)
                        && !TMSelectHandler.isSparkAllowedOnSelect(aContext)) {

                    showOnActionSelectNudge(
                            Utility.getMyProfilePic(aContext),
                            getProfilesList().get(0).getUser().getProfilePic(), "spark");
                    return;
                }
                boolean hasLikedBefore = false;
                hasLikedBefore = getProfilesList().get(0).getUser() != null &&
                        getProfilesList().get(0).getUser().isHasLikedBefore();
                MoEHandler.trackEvent(aContext, MoEHandler.Events.SPARK_ICON_CLICKED);

                if (!SPHandler.getBool(aContext, ConstantsSP.SHARED_PREF_INTRODUCING_SPARK_TUTORIAL_SHOWN)) {
                    SPHandler.setBool(aContext, ConstantsSP.SHARED_PREF_INTRODUCING_SPARK_TUTORIAL_SHOWN, true);
                    toggleFragment(true, SparksHandler.SparksDialogType.introducing_sparks, 0, 0, false, false, hasLikedBefore);
                    return;
                }
                if (!SparksHandler.isSparksPresent(aContext)) {
                    MoEHandler.trackEvent(aContext, MoEHandler.Events.BUY_PAGE_OPENED);
                    toggleFragment(true, SparksHandler.SparksDialogType.buy_spark, 0, 0, false, false, hasLikedBefore);
                } else {
                    MoEHandler.trackEvent(aContext, MoEHandler.Events.SPARK_PAGE_OPENED);
                    toggleFragment(true, SparksHandler.SparksDialogType.send_spark, 0, 0, false, false, hasLikedBefore);
                }
                break;

            case R.id.hide_lay:
                resetSparksFAB();
                if (isProfileAdVisible) {
                    mNextAdPosition += AD_INTERVAL;
                    customDialogLikeHideIcon(false, false);
                    break;
                }
                if (Utility.isNetworkAvailable(this)) {
                    if (aMyDetailModal != null && Utility.isSet(aMyDetailModal.getStatus())
                            && aMyDetailModal.getStatus().equals("authentic")) {

                        if (!RFHandler.getBool(aContext,
                                ConstantsRF.RIGID_FIELD_HIDE_FIRST_TIME)) {
                            RFHandler.insert(aContext, ConstantsRF.RIGID_FIELD_HIDE_FIRST_TIME,
                                    true);
                            hidePopUp();
                        } else {
                            showLikeHideDialog(HttpRequestType.MATCHES_HIDE);
                        }
                    }
                } else
                    AlertsHandler.showMessage(aActivity, R.string.ERROR_NETWORK_FAILURE);

                break;
            case R.id.upload_image_box:
                togglePhotoUploadSlider(!isPhotoUploadSliderShowing, true);
                break;

//
//            case R.id.go_ahead_tv:
//            case R.id.got_it_tv:

            case R.id.photo_pv_close_button_skip:
                TrulyMadlyTrackEvent.trackEvent(aContext,
                        TrulyMadlyActivities.photo_flow, "pv_nudge_skip", 0,
                        null, null, true);
                if (showSecondScreenOfPhotoNudge)
                    showPhotoAndPvNudgeSkippedScreen();
                else
                    photo_upload_nudge_main_view.setVisibility(View.GONE);
                break;
            case R.id.photo_pv_close_button_done:
                if (saveDiscoveryOptionOnServer) {
                    updateDiscoveryOptionsOnServer();
                } else {
                    hidePhotoAndPvNudge();
                }
                break;

            case R.id.take_from_camera_layout:
                togglePhotoUploadSlider(false, true);
                photoUploader.takeFromCamera(PhotoUploader.UPLOAD_TYPE.PICTURE);
                break;

            case R.id.mutual_friend_layout:
                fb_mutual_friend_visible = false;
                aMatchesPageSetter.toggleMutualFriendsLayout(false, false, null);
                indicator2.setVisibility(View.VISIBLE);
                break;

            case R.id.add_from_gallery_layout:
                togglePhotoUploadSlider(false, true);
                photoUploader.fetchFromGallery(PhotoUploader.UPLOAD_TYPE.PICTURE);
                break;
            case R.id.category_nudge_layout:
                isCategoryNudgeVisible = !isCategoryNudgeVisible;
                aMatchesPageSetter.toggleCategoryNudgeVisibility(isCategoryNudgeVisible);
                break;

            case R.id.sub_mutual_friend_layout:
                if (fb_mutual_friend_visible) {
                    fb_mutual_friend_visible = false;
                    aMatchesPageSetter.toggleMutualFriendsLayout(false, false, null);
                }

                break;
            case R.id.fb_friend_container:
                if (RFHandler.getBool(aContext, Utility.getMyId(aContext), ConstantsRF.ALL_MUTUAL_FRIENDS)) {
                    fb_mutual_friend_visible = !fb_mutual_friend_visible;
                    final ProfileNewModal profileModal = getProfilesList().get(0);
                    if (fb_mutual_friend_visible) {
                        if (Utility.isSet(profileModal.getUserId()) && profileModal.getFbFriendList() == null) {
                            aMatchesPageSetter.getmFBMutualFriendsHandler().setmCommonConnString("");
                            aMatchesPageSetter.getmFBMutualFriendsHandler().setmFbFriendsList(null);
                            aMatchesPageSetter.getmFBMutualFriendsHandler().toggleProgressbar(true);
                            indicator2.setVisibility(View.GONE);
                            CustomOkHttpResponseHandler customHandler = new CustomOkHttpResponseHandler(aContext) {

                                public void onRequestSuccess(JSONObject responseJSON) {

                                    if (Utility.stringCompare(current_match_id, responseJSON.optString("target_user_id"))) {
                                        aMatchesPageSetter.getmFBMutualFriendsHandler().toggleProgressbar(false);
                                        ArrayList<FbFriendModal> list = ProfileNewResponseParser.parseFBList(responseJSON, false);
                                        if (list != null && list.size() > 0) {
                                            profileModal.setFbFriendList(list);
                                            aMatchesPageSetter.toggleMutualFriendsLayout(true, true, profileModal.getFbFriendList());
                                        }

                                    }
//
                                }

                                @Override
                                public void onRequestFailure(Exception exception) {
                                    aMatchesPageSetter.getmFBMutualFriendsHandler().toggleProgressbar(false);
                                    aMatchesPageSetter.toggleMutualFriendsLayout(false, false, null);
                                    fb_mutual_friend_visible = false;

                                }
                            };

                            HashMap<String, String> params = new HashMap<>();
                            params.put("target_user_id", profileModal.getUserId());
                            OkHttpHandler.httpPost(aContext, ConstantsUrls.get_MutualFriendListUrl(), params, customHandler);
                        } else {
                            aMatchesPageSetter.toggleMutualFriendsLayout(true, false, profileModal.getFbFriendList());
                        }
                    } else {
                        aMatchesPageSetter.toggleMutualFriendsLayout(false, true, profileModal.getFbFriendList());
                    }
                }
                break;

            case R.id.select_details_card:
                slidingMenu.toggleVisibility(false);
                boolean isSelectMember = TMSelectHandler.isSelectMember(aContext);
                Map<String, String> eventInfoSelect = new HashMap<>();
                eventInfoSelect.put("is_select_member", String.valueOf(isSelectMember));
                eventInfoSelect.put("source", "matches");
                TrulyMadlyTrackEvent.trackEvent(aContext,
                        select, select_detail_card_profile, 0,
                        TrulyMadlyEventStatus.clicked, eventInfoSelect, true);

                if (isSelectMember && TMSelectHandler.isSelectQuizPlayed(aContext)) {
                    UserModal user = getProfilesList().get(0).getUser();
                    toggleSelectQuizResult(getProfilesList().get(0).getUserId(),
                            user.getName(),
                            user.getProfilePic(),
                            Utility.getMyProfilePic(aContext),
                            getProfilesList().get(0).getmSelectCommonString()
                            , getProfilesList().get(0).getmSelectQuote());
                } else {
                    UserModal user = getProfilesList().get(0).getUser();
                    if (user != null && Utility.isSet(user.getmSparkHash())) {
                        SPHandler.setString(aContext, ConstantsSP.SHARED_KEY_SELECT_SAVED_SELECT_HASH,
                                user.getmSparkHash());
                    } else {
                        SPHandler.remove(aContext, ConstantsSP.SHARED_KEY_SELECT_SAVED_SELECT_HASH);
                    }
                    ActivityHandler.startTMSelectActivity(aContext, select_detail_card_profile + ":matches", false, likes_done, hides_done, sparks_done,
                            Utility.stringCompare(mTrackingMap.get(
                                    EventInfoUpdateEvent.getKeyString(EventInfoUpdateEvent.KEY_FAVORITES_VIEWED)), "true")
                            , getProfilesList().get(0).getUserId());
                }
                break;

            default:
                break;
        }
    }

    private void showUSProfileNudge() {
        isUSprofileDialogSeen = SPHandler.getBool(aContext, ConstantsSP.SHARED_KEY_US_PROFILE_DIALOG_SEEN);
        if (!isUSprofileDialogSeen) {
            isUSprofileDialogSeen = true;
            SPHandler.setBool(aContext, ConstantsSP.SHARED_KEY_US_PROFILE_DIALOG_SEEN, true);
            AlertsHandler.showUSProfileDialog(this, getString(R.string.us_profile_dialog_content),
                    getString(R.string.got_it), getString(R.string.edit_caps), new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            switch (view.getId()) {
                                case R.id.positive_button_tv:
                                    break;

                                case R.id.negative_button_tv:
                                    ActivityHandler.startEditPartnerPreferencesActivity(aContext);
                                    break;
                            }
                        }
                    }, null);
        }
    }

    private void toggleWhyNotSparkInsteadToast(boolean show) {
        if (show) {
            why_not_spark_toast_iv.setVisibility(View.VISIBLE);
            why_not_spark_toast_iv.postDelayed(mHideWhynotSparkToastRunnable, 3500);
        } else {
            why_not_spark_toast_iv.removeCallbacks(mHideWhynotSparkToastRunnable);
            why_not_spark_toast_iv.setVisibility(View.GONE);
        }
    }

    private void showWhyNotSparkInsteadDialog(UserModal user) {
        String matchId = null;
        String theirProfileUrl = null;
        if (user != null) {
            matchId = user.getUserId();
            theirProfileUrl = user.getProfilePic();
        }

        final HashMap<String, String> eventInfo = new HashMap<>();
        eventInfo.put("likes_done", String.valueOf(likes_done));
        eventInfo.put("from_scenes", String.valueOf(false));
        eventInfo.put("hides_done", String.valueOf(hides_done));
        eventInfo.put("sparks_done", String.valueOf(sparks_done));
        eventInfo.put("sparks_count", String.valueOf(SparksHandler.getSparksLeft(aContext)));
        if (Utility.isSet(matchId)) {
            eventInfo.put("match_id", String.valueOf(matchId));
        }

        TrulyMadlyTrackEvent.trackEvent(aContext,
                TrulyMadlyActivities.sparks, TrulyMadlyEventTypes.why_not_spark, 0,
                TrulyMadlyEventStatus.viewed, eventInfo, true);

        AlertsHandler.showWhyNotSparkInsteadDialog(aContext,
                SPHandler.getString(aContext,
                        ConstantsSP.SHARED_KEYS_USER_PROFILE_FULL_URL),
                theirProfileUrl, user.isHasLikedBefore(), SparksHandler.getWhyNotSparkInsteadContent(aContext),
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        switch (v.getId()) {
                            case R.id.send_spark_button:
                                TrulyMadlyTrackEvent.trackEvent(aContext,
                                        TrulyMadlyActivities.sparks, TrulyMadlyEventTypes.why_not_spark, 0,
                                        TrulyMadlyEventStatus.send_spark, eventInfo, true);
                                MatchesPageLatest2.this.onClick(mSparkFAB);
                                break;
                            case R.id.maybe_later_button:
                                TrulyMadlyTrackEvent.trackEvent(aContext,
                                        TrulyMadlyActivities.sparks, TrulyMadlyEventTypes.why_not_spark, 0,
                                        TrulyMadlyEventStatus.maybe_later, eventInfo, true);
                                break;
                        }
                    }
                });
    }

    //
//    private ArrayList<FbFriendModal>createDummyData()
//    {
//        ArrayList<FbFriendModal>friendList=new ArrayList<FbFriendModal>();
//        for (int i =0;i<15;i++)
//        {
//            FbFriendModal friend= new FbFriendModal();
//            friend.setName("devesh");
//            friend.setThumbnail("http://techstory.in/wp-content/uploads/2015/03/zomato-logo.png");
//            friendList.add(friend);
//        }
//        return friendList;
//    }
    private void updateDiscoveryOptionsOnServer() {
        CustomOkHttpResponseHandler okHttpResponseHandler = new CustomOkHttpResponseHandler(aContext) {

            @Override
            public void onRequestSuccess(JSONObject responseJSON) {
                mProgressDialog = UiUtils.hideProgressBar(mProgressDialog);
                saveDiscoveryOptionOnServer = false;
                SPHandler.setBool(aContext, Constants.DISCOVERY_OPTION, isDiscoveryOn);
                hidePhotoAndPvNudge();

            }

            @Override
            public void onRequestFailure(Exception exception) {
                mProgressDialog = UiUtils.hideProgressBar(mProgressDialog);
                AlertsHandler.showConfirmDialog(aActivity, R.string.couldnt_update_network_issue, R.string.no, R.string.yes, new ConfirmDialogInterface() {

                    @Override
                    public void onPositiveButtonSelected() {
                        updateDiscoveryOptionsOnServer();
                    }

                    @Override
                    public void onNegativeButtonSelected() {
                    }
                });
            }
        };
        mProgressDialog = UiUtils.showProgressBar(aActivity, mProgressDialog, R.string.updating);
        Utility.sendDiscoveryOptions(isDiscoveryOn, aContext, okHttpResponseHandler);
    }

    private void togglePhotoUploadSlider(boolean showSlider, Boolean animate) {
        isPhotoUploadSliderShowing = showSlider;
        if (showSlider) {
            if (photoUploader.isInProgress()) {
                //Utility.showMessage(aActivity, R.string.cant_upload_photo_yet);
                togglePhotoUploadSlider(false, false);
            } else {
                TrulyMadlyTrackEvent.trackEvent(aContext,
                        TrulyMadlyActivities.photo_flow, "add_photo_click", 0,
                        null, null, true);
                photo_upload_slider_layout.setVisibility(View.VISIBLE);
                if (animate) {
                    Animation slideUpAnimation = AnimationUtils.loadAnimation(this, R.anim.slide_up);
                    photo_upload_slider_layout.startAnimation(slideUpAnimation);
                }
            }
        } else {
            photo_upload_slider_layout.setVisibility(View.GONE);
            if (animate) {
                Animation slideDownAnimation = AnimationUtils.loadAnimation(this, R.anim.slide_down);
                photo_upload_slider_layout.startAnimation(slideDownAnimation);
            }
        }
    }

    private void hidePopUp() {
        ConfirmDialogInterface confirmDialogInterface = new ConfirmDialogInterface() {
            @Override
            public void onPositiveButtonSelected() {
                showLikeHideDialog(HttpRequestType.MATCHES_HIDE);
            }

            @Override
            public void onNegativeButtonSelected() {
            }
        };
        AlertsHandler.showConfirmDialog(aActivity, R.string.hide_detail, R.string.yes, R.string.cancel,
                confirmDialogInterface);
    }


    private void setRootLayout() {
        boolean shouldShowProfileAd = shouldShowProfileAd();

        //Initializes and loads the AdsHandler
//        initializeAdsHandler();
//        checkIfAdCanBeShown();

        ProfileNewModal aProfileNewModal = getProfilesList().get(0);
        if (aProfileNewModal != null) {
            current_match_id = aProfileNewModal.getUserId();
        } else {
            current_match_id = "";
        }

        toggleWhyNotSparkInsteadToast(false);

		/* Check if this the last tile */
        if (!shouldShowProfileAd && aProfileNewModal.isLastTile() && Utility.isSet(aMatchesSysMesgModal.getLastTileMsg())) {

            //clearing the cache
            MatchesDbHandler.clearMatchesCache(aContext);
            if (aMatchesPageSetter != null && aMatchesPageSetter.isCurrentProfileAd()) {
                aMatchesPageSetter.stopProfileAd();
            }
            if (!(isPINudgeEnabled && shouldPINudgeBeShown(false) && showPINudgeOverlay())) {
                isProfileAdShownOnce = false;
                dismissLikeHideIconDialog();
                profile_like_hide_lay.setVisibility(View.GONE);
                matches_page_setter.setVisibility(View.GONE);
                showOverLay(getString(R.string.match_last_tile_txt), aMatchesSysMesgModal.getLastTileLink());
                HashMap<String, String> eventInfo = new HashMap<>();
                eventInfo.put(ad_feasible, String.valueOf(false));
                TrulyMadlyTrackEvent.trackEvent(aContext,
                        TrulyMadlyActivities.matches, TrulyMadlyEventTypes.matches_last, 0,
                        null, eventInfo, true);
                // show edit favorites activity for result for male here.
                showEditFavoritesInMatches(Utility.isMale(aContext), true);
                return;
            }
        }

        //remove insterstitial flag here - this means a profile is currently visible, so no need to show ad
        //even if the sdk was initialized properly
        isInterstitialToBeShown = false;

        // doing this in tutorial
        // profile_like_hide_lay.setVisibility(View.VISIBLE);
        matches_page_setter.setVisibility(View.VISIBLE);

        aMatchesPageSetter.toggleMutualFriendsLayout(false, false, null);
        fb_mutual_friend_visible = false;

        OnClickListener lis;
        MatchesSysMesgModal aMatchesSysMesgModal = this.aMatchesSysMesgModal;
        MyDetailModal aMyDetailModal = this.aMyDetailModal;
        if (!shouldShowProfileAd) {
            lis = new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getProfilesList() == null || getProfilesList().size() == 0 ||
                            getProfilesList().get(0).getUser() == null)
                        return;

                    int position = Integer.parseInt(v.getTag().toString());

                    ProfileNewModal profileNewModal = getProfilesList().elementAt(0);
                    if (profileNewModal.isSponsoredProfile()) {

                        //Click Tracking
                        //GA & TRK
                        HashMap<String, String> eventInfo = new HashMap<>();
                        eventInfo.put("source", TrulyMadlyActivities.matches);
                        TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyActivities.campaigns,
                                TrulyMadlyEventTypes.click, 0, profileNewModal.getUserId(),
                                eventInfo, true);

                        //Tracking URLS from server
                        AdsTrackingHandler.callUrls(profileNewModal.getmClicksTrackingUrls());

                        //If landing url is given -> show it in the webview and return
                        if (Utility.isSet(profileNewModal.getmLandingUrl())) {
                            mWebviewHandler.loadUrl(profileNewModal.getmLandingUrl());
                            return;
                        }
                    }

                    //If (sponsored profile && landing url not present) || (is not sponsored profile)
                    VideoModal[] videoModals = null;
                    ArrayList<VideoModal> videoModalsList = profileNewModal.getUser().getVideoArray();
                    if (videoModalsList != null && videoModalsList.size() > 0) {
                        videoModals = videoModalsList.toArray(new VideoModal[videoModalsList.size()]);
                    }

//                    ActivityHandler.startAlbumFullViewPagerForResult(aActivity,
//                            position,
//                            profileNewModal.getUser().getOtherPics(),
//                            profileNewModal.getUser().getVideoUrls(), profileNewModal.getUser().getThumbnailUrls(),
//                            getResources().getString(R.string.photo_gallery));

                    ActivityHandler.startAlbumFullViewPagerForResult(aActivity,
                            position, profileNewModal.getUser().getOtherPics(), videoModals,
                            getResources().getString(R.string.photo_gallery), TrulyMadlyActivities.matches,
                            profileNewModal.getUser().getUserId());
                }
            };
            isProfileAdVisible = false;
            UserModal user = aProfileNewModal.getUser();
            if (user != null && user.getmCountryId() == Constants.COUNTRY_ID_US && !isUSprofileDialogSeen) {
                showUSProfileNudge();
            }

        } else {
            RFHandler.insert(aContext, Utility.getMyId(aContext),
                    ConstantsRF.RIGID_FIELD_LAST_PROFILE_AD_TS,
                    String.valueOf(Calendar.getInstance().getTimeInMillis()));
            isProfileAdShownOnce = true;
            mMatchesBeforeWeShowProfileAd = getmFinalPositionForProfileAds();
            isProfileAdVisible = true;
            lis = null;
            aProfileNewModal = AdsUtility.prepareProfileNewModal();
            aMatchesSysMesgModal = null;
            aMyDetailModal = null;
        }

        actionBarHandler.setToolbarTransparent(true, "");
        matches_page_setter_scrollview = (ScrollView) matches_page_setter.findViewById(R.id.matches_page_setter_scrollview);
        mTrackingMap.clear();
        isHasLikedBeforeAnimationShownForThisMatch = false;
        isHasLikedBeforeNudgeShownForThisMatch = false;
        aMatchesPageSetter.instantiateItem(aProfileNewModal, lis, aMatchesSysMesgModal, aMyDetailModal,
                mAdsHelper);

        toggleSparksFAB(SparksHandler.isSparksEnabled(aContext), aProfileNewModal.isSponsoredProfile(),
                shouldShowProfileAd, aProfileNewModal.getUser() != null && aProfileNewModal.getUser().isHasLikedBefore());

        changeLikeFABAlpha(aProfileNewModal.isSponsoredProfile(),
                shouldShowProfileAd, aProfileNewModal.isTMSelectMember(),
                aProfileNewModal.getUser() != null && aProfileNewModal.getUser().isHasLikedBefore());

        if (matches_page_setter_scrollview != null) {
            matches_page_setter_scrollview.fullScroll(ScrollView.FOCUS_UP);
        }

        if (!shouldShowProfileAd) {
            boolean isFemaleNudgeShown = checkAndShowFemaleNudge();
            if ((makeSecondMatchesCall || makeBackgroundMatchesCall()) && !secondCallInProgress) {
                new FetchMatches().execute();
            }
            // show edit favorites activity for result for female here.
            showEditFavoritesInMatches(!Utility.isMale(aContext) &&
                    hides_done_consecutive == RFHandler.getInt(aContext, ConstantsRF.RIGID_FIELD_SHOW_FAVORITES_IN_MATCHES_FEMALE_HIDES, 10), false);
        } else {
            profile_like_hide_lay.setVisibility(View.VISIBLE);
        }
    }

    private void showEditFavoritesInMatches(boolean toShowFavorites, boolean isLastTile) {
        if (toShowFavorites && !favoritesShownJustNow &&
                TimeUtils.isRigidTimeoutExpired(aContext, ConstantsRF.RIGID_FIELD_EDIT_FAVORITES_SHOWN_IN_MATCHES_TIMESTAMP,
                        RFHandler.getInt(aContext, ConstantsRF.RIGID_FIELD_SHOW_FAVORITES_IN_MATCHES_INTERVAL_DAYS, 30) * TimeUtils.HOURS_24_IN_MILLIS)) {
            RFHandler.insert(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_EDIT_FAVORITES_SHOWN_IN_MATCHES_TIMESTAMP, TimeUtils.getFormattedTime());
            favoritesShownJustNow = true;
            if (isLastTile) {
                TrulyMadlyTrackEvent.trackEvent(aContext,
                        TrulyMadlyActivities.matches, favorites_popup_matches, 0,
                        "from_last_tile", null, true);
                ActivityHandler.startEditFavoritesActivityForResult(aActivity, Constants.EDIT_FAVORITES_FROM_MATCHES, 0, true);
            } else {
                TrulyMadlyTrackEvent.trackEvent(aContext,
                        TrulyMadlyActivities.matches, favorites_popup_matches, 0,
                        "popup_shown", null, true);
                AlertsHandler.showConfirmDialogCustomView(aActivity, R.layout.custom_alert_edit_favorites, R.string.update_now, R.string.later,
                        R.color.pink_text_color, false, new ConfirmDialogInterface() {
                            @Override
                            public void onPositiveButtonSelected() {
                                TrulyMadlyTrackEvent.trackEvent(aContext,
                                        TrulyMadlyActivities.matches, favorites_popup_matches, 0,
                                        "popup_confirm", null, true);
                                ActivityHandler.startEditFavoritesActivityForResult(aActivity, Constants.EDIT_FAVORITES_FROM_MATCHES, 0, true);
                            }

                            @Override
                            public void onNegativeButtonSelected() {
                                TrulyMadlyTrackEvent.trackEvent(aContext,
                                        TrulyMadlyActivities.matches, favorites_popup_matches, 0,
                                        "popup_cancelled", null, true);
                            }
                        });
            }
        } else {
            favoritesShownJustNow = false;
        }
    }

    private boolean isProfileAdsEnabled() {
        return RFHandler.getBool(aContext, ConstantsRF.RIGID_FILED_IS_PROFILE_ADS_ENABLED);
    }

    private int getmFinalPositionForProfileAds() {
        if (mFinalPositionForProfileAds == -1) {
            mFinalPositionForProfileAds = RFHandler.getInt(aContext, Utility.getMyId(aContext),
                    ConstantsRF.RIGID_FILED_FINAL_PROFILE_AD_POSITION, -1);
        }

        return mFinalPositionForProfileAds;
    }

    private int getmMatchesBeforeWeShowProfileAd() {
        if (mMatchesBeforeWeShowProfileAd == -1) {
            mMatchesBeforeWeShowProfileAd = SPHandler.getInt(aContext,
                    ConstantsSP.SHARED_MATCHES_BEFORE_AD, -1);
            if (mMatchesBeforeWeShowProfileAd == -1) {
                mMatchesBeforeWeShowProfileAd = getmFinalPositionForProfileAds();
            } else if (mMatchesBeforeWeShowProfileAd == 0) {
                //TODO: ADS: What to do when mMatchesBeforeWeShowProfileAd is 0 - should we reset it?
            }
        }

        return mMatchesBeforeWeShowProfileAd;
    }

    private boolean shouldShowProfileAd() {
        return
                //Checks if profile ad is enabled or not
                isProfileAdsEnabled()
                        //If profile ad is repeatable or not:
                        //If no, if profile ad has been shown once or not
                        && (isProfileAdsRepeatEnabled() || !isProfileAdShownOnce)
                        //If profile list is empty or not
                        //If no matches swiped > 0 or not
                        && ((getProfilesList() == null || getProfilesList().size() == 0 ||
                        getProfilesList().get(0).isLastTile() || matches_swiped > 0))
                        //If profile ad is shown once or not
                        //If not -> Is timestamp satisfied
                        && (isProfileAdShownOnce || isProfileAdTimeStampSatisfied())
                        //If current counter value == 0
                        && (checkNotNull(getmMatchesBeforeWeShowProfileAd()) == 0)
                        //Are constraints satisfied
                        && mAdsHelper.areConstraintsSatisfied(AdsType.MATCHES_MID_PROFILE_VIDEO
                        , TrulyMadlyActivities.matches)
                        //Is ad ready?
                        && mAdsHelper.shouldShowAd(AdsType.MATCHES_MID_PROFILE_VIDEO, true);
    }

    @Override
    public void onBackPressed() {
        if (isSelectQuizCompareVisible) {
            slidingMenu.toggleVisibility(true);
            isSelectQuizCompareVisible = false;
            mSelectQuizresultFragment.registerListener(null);
            mSelectQuizResultContainer.setVisibility(View.GONE);
        } else if (isPhotoUploadSliderShowing) {
            togglePhotoUploadSlider(false, true);
        } else if (crop_view.getVisibility() == View.VISIBLE) {
            crop_view.setVisibility(View.GONE);
            matches_complt_lay.setVisibility(View.VISIBLE);
        } else if (isPhotoAndPvNudgeShowing) {
            TrulyMadlyTrackEvent.trackEvent(aContext,
                    TrulyMadlyActivities.photo_flow, "pv_nudge_back", 0,
                    null, null, true);
            hidePhotoAndPvNudge();
        } else if (mWebviewHandler != null && mWebviewHandler.isVisible()) {
            mWebviewHandler.hide();
        } else if (isSparkDialogVisible && mCurrentFragment != null) {
            ((ActivityEventsListener) mCurrentFragment).onBackPressedActivity();
        } else if (fb_mutual_friend_visible) {
            fb_mutual_friend_visible = false;
            aMatchesPageSetter.toggleMutualFriendsLayout(false, false, null);
        } else {
            Utility.minimizeApp(this);
            //super.onBackPressed();
        }
    }

    private boolean createLikeHideDialog() {
        if (likeHideIconDialog == null) {
            likeHideIconDialog = new Dialog(this);
            likeHideIconDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            likeHideIconDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            likeHideIconDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT);

            try {
                likeHideIconDialog.setContentView(R.layout.like_hide_icon_pop_dialog_layout);
            } catch (InflateException e) {
                dismissLikeHideIconDialog();
                likeHideIconDialog = null;
                return false;
            }
            likeHideIconDialog.setCancelable(false);

            likeHideIcon = findById(likeHideIconDialog, R.id.like_hide_iv);
        }

        return likeHideIconDialog != null;
    }

    private void customDialogLikeHideIcon(final boolean isLiked, final boolean isSparked) {
        isLikeHideClickable = false;

        if (!isSparked && !createLikeHideDialog()) {
            return;
        }

        if (!isSparked) {
            try {
                if (isLiked) {
                    likeHideIcon.setBackgroundResource(R.drawable.like);
                } else {
                    likeHideIcon.setBackgroundResource(R.drawable.hide);
                }
            } catch (OutOfMemoryError ignored) {
            }
        }

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                if (!isSparked) {
                    dismissLikeHideIconDialog();

                    if (isLiked) {
                        showLikedFirstTimeTutorial();
                    }
                }

                isLikeHideClickable = true;

                if (getProfilesList() != null && getProfilesList().size() > 1) {
                    if (!isProfileAdVisible) {
                        MatchesDbHandler.updateActionToTrue(aContext, getProfilesList().get(0).getUserId());
                        getProfilesList().remove(0);
                        prefetchPhotos(PREFETCH_MODE.next, -1);
                        matches_swiped++;
                        if (isSparked) {
                            sparks_done++;
                            hides_done_consecutive = 0;
                        } else if (isLiked) {
                            likes_done++;
                            hides_done_consecutive = 0;
                        } else {
                            hides_done++;
                            hides_done_consecutive++;
                        }
                        if ((isProfileAdsRepeatEnabled() || !isProfileAdShownOnce) && getmMatchesBeforeWeShowProfileAd() > 0) {
                            mMatchesBeforeWeShowProfileAd--;
                        }
                    } else {
                        TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyEvent.TrulyMadlyActivities.matches,
                                TrulyMadlyEvent.TrulyMadlyEventTypes.profile_ads, 0,
                                TrulyMadlyEventStatus.ad_closed, null, true);
                        mMatchesBeforeWeShowProfileAd = getmFinalPositionForProfileAds();
                        isProfileAdVisible = false;
                    }
                    setRootLayout();
                    if (isProfilePicRejectedFemale && isLiked && showUploadPicDialog()) {
                        RFHandler.insert(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_LAST_UPLOAD_PIC_AFTER_LIKE_TSTAMP, TimeUtils.getSystemTimeInMiliSeconds());
                        showSecondScreenOfPhotoNudge = false;
                        showPhotoAndPvNudge(true);
                    }

                } else if (isProfileAdVisible) {
                    TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyEvent.TrulyMadlyActivities.matches,
                            TrulyMadlyEvent.TrulyMadlyEventTypes.profile_ads, 0,
                            TrulyMadlyEventStatus.ad_closed, null, true);
                    mMatchesBeforeWeShowProfileAd = getmFinalPositionForProfileAds();
                    isProfileAdVisible = false;
                    setRootLayout();
                }

                if (getProfilesList() != null) {
                    if (isProfileAdVisible || !showLastTile()) {
                        profile_like_hide_lay.setVisibility(View.VISIBLE);
                    }
                }
            }
        };

        if (!isSparked) {
            try {
                likeHideIconDialog.show();
            } catch (RuntimeException ignored) {
            }
        }
        (new Handler()).postDelayed(runnable, ((!isSparked) ? 350 : 0));
    }

    private boolean isProfileAdsRepeatEnabled() {
        return RFHandler.getBool(aContext, Utility.getMyId(aContext),
                ConstantsRF.RIGID_FILED_IS_PROFILE_ADS_REPEATABLE);
    }

    private boolean isProfileAdTimeStampSatisfied() {
        long timeout = RFHandler.getLong(aContext, Utility.getMyId(aContext),
                ConstantsRF.RIGID_FILED_IS_PROFILE_ADS_TIME_INTERVAL, 0);
        Long difference = TimeUtils.getTimeoutDifference(aContext,
                ConstantsRF.RIGID_FIELD_LAST_PROFILE_AD_TS,
                Utility.getMyId(aContext), 0);
        if (difference == null) {
            return true;
        } else if (Math.abs(difference) >= timeout) {
            return true;
        }
        return false;
    }

    private void showLikedFirstTimeTutorial() {
        if (!RFHandler.getBool(aContext, ConstantsRF.RIGID_FIELD_LIKED_FIRST_TIME)) {
            RFHandler.insert(aContext, ConstantsRF.RIGID_FIELD_LIKED_FIRST_TIME, true);

            if (tutorial_liked_first_time == null) {
                tutorial_liked_first_time = tutorial_liked_first_time_stub.inflate();
                Picasso.with(this)
                        .load(R.drawable.tutorial_like_top).noFade().into((ImageView) findById(tutorial_liked_first_time, R.id.tutorial_like_top_iv));
            }

            tutorial_liked_first_time.setVisibility(View.VISIBLE);
            tutorial_liked_first_time
                    .startAnimation(AnimationUtils.loadAnimation(aContext, R.anim.slide_up_tutorial_first_liked));
            if (tutorial_liked_first_time_hide_runnable == null) {
                tutorial_liked_first_time_hide_runnable = new Runnable() {
                    @Override
                    public void run() {
                        tutorial_liked_first_time.startAnimation(
                                AnimationUtils.loadAnimation(aContext, R.anim.slide_down_tutorial_first_liked));
                        tutorial_liked_first_time.setVisibility(View.GONE);
                    }
                };
            }
            if (mHandler == null) {
                mHandler = new Handler();
            }
            mHandler.postDelayed(tutorial_liked_first_time_hide_runnable, 4000);
        }
    }


    private void showSocialsTutorial(/*boolean justTheCoachmark*/) {

        RFHandler.insert(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_SOCIALS_TUTORIAL_SHOWN, true);

        if (tutorial_socials == null) {
            tutorial_socials = tutorial_socials_stub.inflate();
            Picasso.with(this)
                    .load(R.drawable.coach_mark_social).noFade().into((ImageView) findById(tutorial_socials, R.id.tutorial_socials_iv));
        }
        tutorial_socials.setVisibility(View.VISIBLE);
        tutorial_socials
                .startAnimation(AnimationUtils.loadAnimation(aContext, R.anim.slide_up_tutorial_first_liked));
        if (tutorial_socials_hide_runnable == null) {
            tutorial_socials_hide_runnable = new Runnable() {
                @Override
                public void run() {
                    Animation animation = AnimationUtils.loadAnimation(aContext, R.anim.slide_down_tutorial_first_liked);
                    animation.setAnimationListener(mTutorialAnimationListener);
                    tutorial_socials.startAnimation(animation);
                    tutorial_socials.setVisibility(View.GONE);
                }
            };
        }
        if (mHandler == null) {
            mHandler = new Handler();
        }
        mHandler.postDelayed(tutorial_socials_hide_runnable, 4000);
//        }
    }

    private void showLikeHideFirstTimeTutorial() {
        profile_like_hide_lay.setVisibility(View.VISIBLE);
        if (!RFHandler.getBool(aContext, ConstantsRF.RIGID_FIELD_LIKE_HIDE_FIRST_TIME)) {
            RFHandler.insert(aContext, ConstantsRF.RIGID_FIELD_LIKE_HIDE_FIRST_TIME, true);
            profile_like_hide_lay
                    .startAnimation(AnimationUtils.loadAnimation(this, R.anim.scale_up_tutorial_first_like_hide));

            if (tutorial_like_nope_first_time == null) {
                tutorial_like_nope_first_time = tutorial_like_nope_first_time_stub.inflate();
                Picasso.with(this)
                        .load(R.drawable.tutorial_like_bottom).noFade().into((ImageView) findById(tutorial_like_nope_first_time, R.id.tutorial_like_bottom_iv));
            }

            if (tutorial_like_nope_first_time_hide_runnable == null) {
                tutorial_like_nope_first_time_hide_runnable = new Runnable() {
                    @Override
                    public void run() {
                        Animation animation = AnimationUtils.loadAnimation(aContext, R.anim.slide_up_tutorial_first_like_hide);
                        animation.setAnimationListener(mTutorialAnimationListener);
                        tutorial_like_nope_first_time.startAnimation(animation);
                        tutorial_like_nope_first_time.setVisibility(View.GONE);
                    }
                };
            }
            if (tutorial_like_nope_first_time_show_runnable == null) {
                tutorial_like_nope_first_time_show_runnable = new Runnable() {
                    @Override
                    public void run() {
                        profile_like_hide_lay.clearAnimation();
                        tutorial_like_nope_first_time.setVisibility(View.VISIBLE);
                        tutorial_like_nope_first_time.startAnimation(
                                AnimationUtils.loadAnimation(aContext, R.anim.slide_down_tutorial_first_like_hide));
                        mHandler.postDelayed(tutorial_like_nope_first_time_hide_runnable, 3000);
                    }
                };
            }
            if (mHandler == null) {
                mHandler = new Handler();
            }
            mHandler.postDelayed(tutorial_like_nope_first_time_show_runnable, 2000);
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
//        if (mCurrentFragment != null) {
//            if(mFragmentManager == null){
//                mFragmentManager = getFragmentManager();
//            }
//            mFragmentManager.beginTransaction().remove(mCurrentFragment).commitAllowingStateLoss();
//        }
//        mCurrentFragment = null;
//        toggleFragment(false, null, 0, 0, false);

        outState.putBoolean("isSparkScoreAnimationShownOnce", isSparkScoreAnimationShownOnce);
        outState.putBoolean("isHasLikedBeforeAnimationShownForThisMatch", isHasLikedBeforeAnimationShownForThisMatch);
        outState.putBoolean("isHasLikedBeforeNudgeShownForThisMatch", isHasLikedBeforeNudgeShownForThisMatch);
        outState.putBoolean("isSelectQuizCompareVisible", isSelectQuizCompareVisible);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        if (mHandler != null) {
            mHandler.removeCallbacks(tutorial_liked_first_time_hide_runnable);
            mHandler.removeCallbacks(tutorial_socials_hide_runnable);
            mHandler.removeCallbacks(tutorial_like_nope_first_time_show_runnable);
            mHandler.removeCallbacks(tutorial_like_nope_first_time_hide_runnable);
            mHandler.removeCallbacks(mHideFemaleNudgeRunnable);
//            mHandler.removeCallbacks(video_tutorial_close_enable_runnable);
        }

        if (!isFirstSetOfTutorialsOver && newValueFirstSetOfTutorialsOver) {
            RFHandler.insert(aContext, Utility.getMyId(aContext),
                    ConstantsRF.RIGID_FIELD_FIRST_SET_TUTORIALS_OVER, newValueFirstSetOfTutorialsOver);
        }
        if (mSparksReceiverHandler != null) {
            mSparksReceiverHandler.register(null);
        }

        super.onDestroy();

        if (mCurrentFragment != null && mCurrentFragment instanceof ActivityEventsListener) {
            ((ActivityEventsListener) mCurrentFragment).registerListener(null);
        }

        if (mSparksBillingController != null) {
            mSparksBillingController.onDestroy();
        }

        if (aMatchesPageSetter != null) {
            aMatchesPageSetter.onDestroy();
        }

    }

    @Override
    public void onResume() {
        super.onResume();

        NotificationHandler.clearAllNotificationByCollapseKey(this, NotificationModal.NotificationType.MATCHES_IMAGE_MULTI.toString());

        fetchSparksForSparkBlooper(true);


        fetchConversationData();

        boolean letTheProfileAdReload = true;
        showAd = false;
        if (!isPhotoAndPvNudgeShowing && !isFromOnActivityResult) {
            if (TrulyMadlyApplication.wasInBackground || !isCreated) {
                //We dont want the ad to reload in case when the list refreshes
                // e.g. when user clicked on the ad and comes back on the matches page after 4 seconds
                if (aMatchesPageSetter != null) {
                    aMatchesPageSetter.setIsAdClicked(false);
                    letTheProfileAdReload = false;
                    aMatchesPageSetter.stopProfileAd();
                }
                issueRequest(USER_FLAGS);
            } else {
                setAdAndShow();
            }
        }
        TrulyMadlyApplication.activityResumedMatches(this);
        isResumed = true;
        isCreated = true;
        isFromOnActivityResult = false;

//        toggleTransparency(false);
        startTutorials();

        if (letTheProfileAdReload && aMatchesPageSetter != null && aMatchesPageSetter.isCurrentProfileAd()) {
            aMatchesPageSetter.reloadProfileAd(mAdsHelper);
        }

        showAppUpdateScreen(hardAppVersion, 0, hardUpdateText, null);

        if (aMatchesPageSetter != null) {
            aMatchesPageSetter.onResume();
        }

        blockUserForSelectQuizIfRequired();

    }

    private void blockUserForSelectQuizIfRequired() {
        //DONE :: remove following line
        //Disabling the blocking code for now.
        //isSelectActivityStarted = true;

        if (TMSelectHandler.blockUser(aContext) && !isSelectActivityStarted) {
            isSelectActivityStarted = true;

            UserModal user = getProfilesList().get(0).getUser();
            if (user != null && Utility.isSet(user.getmSparkHash())) {
                SPHandler.setString(aContext, ConstantsSP.SHARED_KEY_SELECT_SAVED_SELECT_HASH,
                        user.getmSparkHash());
            } else {
                SPHandler.remove(aContext, ConstantsSP.SHARED_KEY_SELECT_SAVED_SELECT_HASH);
            }
            ActivityHandler.startTMSelectActivity(aContext, "matches", true, likes_done, hides_done, sparks_done,
                    Utility.stringCompare(mTrackingMap.get(
                            EventInfoUpdateEvent.getKeyString(EventInfoUpdateEvent.KEY_FAVORITES_VIEWED)), "true")
                    , getProfilesList().get(0).getUserId());
        }
    }

    private void fetchConversationData() {
        //New hack for conversation list.
        Utility.getConversationListFromServer(aContext, new OnConversationListFetchedInterface() {
            @Override
            public void onSuccess(int unreadConversationsCount) {
                actionBarHandler.setConversationsCount(unreadConversationsCount);
                fetchSparksForSparkBlooper(false);
//                actionBarHandler.showConversationBadge(false);
            }

            @Override
            public void onFail(Exception exception) {
                actionBarHandler.setConversationsCount(MessageDBHandler.getUnreadConversationCount(Utility.getMyId(aContext),
                        aContext));
                fetchSparksForSparkBlooper(false);
//                actionBarHandler.showConversationBadge(false);
            }
        });
        //Old hack for conversation list
        //issueRequest(UPDATE_NOTIFICATION_COUNTER);
    }

    @Override
    public void onPause() {
//        if (mFullScreenAdsHandler != null)
//            mFullScreenAdsHandler.unregister();
        isResumed = false;
        TrulyMadlyApplication.activityPausedMatches(this);
        SPHandler.setInt(aContext, ConstantsSP.SHARED_KEYS_MATCHES_BEFORE_AD, mMatchesBeforeWeShowProfileAd);


        //Fixing issue: https://trulymadly.atlassian.net/browse/ANDROID-2495
        if (isSparkDialogVisible && mCurrentFragment != null && mCurrentFragment instanceof SendSparkFragment) {
            UiUtils.hideKeyBoard(aContext);
        }

        try {
            super.onPause();
        } catch (IllegalStateException ignored) {
        }

        if (aMatchesPageSetter != null) {
            aMatchesPageSetter.onPause();
        }

        toggleWhyNotSparkInsteadToast(false);

    }

    @Override
    public void onStart() {
        super.onStart();
        BusProvider.getInstance().register(this);

        if (aMatchesPageSetter != null) {
            aMatchesPageSetter.onStart();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        BusProvider.getInstance().unregister(this);

        if (aMatchesPageSetter != null) {
            aMatchesPageSetter.onStop();
        }
    }

    @Subscribe
    public void onEventInfoUpdateEventFired(EventInfoUpdateEvent eventInfoUpdateEvent) {
        if (eventInfoUpdateEvent != null) {
            String key = EventInfoUpdateEvent.getKeyString(eventInfoUpdateEvent.getmKey());
            if (key != null) {
                String value = eventInfoUpdateEvent.getmValue();
                if (Utility.isSet(value)) {
                    mTrackingMap.put(key, eventInfoUpdateEvent.getmValue());
                }
            }
        }
    }

    @Subscribe
    public void onSimpleActionEventReceived(SimpleAction simpleAction) {
        if (simpleAction != null) {
            switch (simpleAction.getmAction()) {
                case SimpleAction.ACTION_OPEN_BUY_SPARKS:
                    boolean hasLikedBefore = false;
                    hasLikedBefore = getProfilesList().get(0).getUser() != null &&
                            getProfilesList().get(0).getUser().isHasLikedBefore();
                    toggleFragment(true, SparksHandler.SparksDialogType.buy_spark, 0, 0, false, false, hasLikedBefore);
                    break;
                case SimpleAction.ACTION_79_SDK_INIT:
                    if(isInterstitialToBeShown){
                        isInterstitialToBeShown = false;
                        setAdAndShow();
                    }
                    break;
            }
        }
    }

    @Subscribe
    public void onNetworkChanged(NetworkChangeEvent networkChangeEvent) {
        if (aMatchesPageSetter != null) {
            aMatchesPageSetter.onNetworkChanged(networkChangeEvent);
        }
    }

    @SuppressWarnings("UnusedParameters")
    @Subscribe
    public void triggerUpdateConversationList(TriggerConversationListUpdateEvent triggerConversationListUpdateEvent) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                fetchConversationData();
            }
        });
    }

    private void parseUserFlags(JSONObject response) {
        UserFlags userFlags = UserFlagsParser.processAndParseUserFlags(aContext, response);

        if (slidingMenu != null) {
            slidingMenu.toggleTMFrills(SparksHandler.isSparksEnabled(aContext));
            if (userFlags.isSparksEnabled() && !areSparksFetchedOnce) {
                fetchSparksForSparkBlooper(true);
            }

            if (getProfilesList() != null && getProfilesList().size() > 0) {
                ProfileNewModal aProfileNewModal = getProfilesList().get(0);
                toggleSparksFAB(userFlags.isSparksEnabled(), aProfileNewModal.isSponsoredProfile(), isProfileAdVisible,
                        aProfileNewModal.getUser() != null && aProfileNewModal.getUser().isHasLikedBefore());
            }

            slidingMenu.toggleScenes(RFHandler.getBool(aContext,
                    Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_ALLOW_MEETUPS));
        }

//        actionBarHandler.toggleCategories(RFHandler.getBool(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_ALLOW_MEETUPS));
        actionBarHandler.toggleCategories(false);
        RFHandler.insert(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_SHOW_TM_SCENES_BLOOPER, response.optBoolean("new_event_added"));
//        actionBarHandler.toggleCategories(true);
        JSONObject scenes_ab = response.optJSONObject("scenes_ab");
        if (scenes_ab != null) {
            RFHandler.insert(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_TM_SCENES_AB_ENABLED, scenes_ab.optBoolean("scenes_ab"));
            RFHandler.insert(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_TM_SCENES_CATEGORY_ID, scenes_ab.optString("scenes_ab_category"));
            RFHandler.insert(aContext, Utility.getMyId(aContext), ConstantsRF.AB_TRACKING_KEY, scenes_ab.optString("scenes_ab_tracking_key"));
        }

        RFHandler.insert(aContext, Utility.getMyId(aContext), ConstantsRF.ALL_MUTUAL_FRIENDS, response.optBoolean("all_mutual_friends"));

        JSONObject matches_cache_optimisations = response.optJSONObject("matches_cache_optimisations");

        if (matches_cache_optimisations != null) {
            NO_OF_MATCHES = matches_cache_optimisations.optInt("secondBatchTriggerPoint", NO_OF_MATCHES);
            MATCHES_CALL_INTERVAL = matches_cache_optimisations.optLong("appCacheExpiryTime", MATCHES_CALL_INTERVAL_DEFAULT);
            RFHandler.insert(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_NO_OF_MATCHES, NO_OF_MATCHES);
            RFHandler.insert(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_MATCHES_CALL_INTERVAL, MATCHES_CALL_INTERVAL);
        }
        if (userFlags != null) {
            enablePINudge(userFlags.getmPopularity(), userFlags.getmProfileView(), userFlags.getmProfileViewsText());
            if (aMatchesPageSetter != null) {
                aMatchesPageSetter.setIsLastActiveShown(userFlags.isLastActiveShown());
            }
        } else {
            enablePINudge(-1, -1, null);
        }

        showAppUpdateScreen(userFlags.getHardAppVersion(), userFlags.getSoftAppVersion(), userFlags.getHardUpdateText(), userFlags.getSoftUpdateText());

        blockUserForSelectQuizIfRequired();

        if (isResumed && (getProfilesList() == null || getProfilesList().size() <= 1
                || SPHandler.getBool(aContext, ConstantsSP.SHARED_PREF_NEW_TS_TUTORIAL))
                && !isAlreadymatchAnimationRunning
                && TMSelectHandler.shouldShowSelectNudge(aContext)) {
            RFHandler.insert(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_SELECT_NUDGE_LAST_TS,
                    String.valueOf(Calendar.getInstance().getTimeInMillis()));
            //Fix for : https://fabric.io/trulymadly/android/apps/com.trulymadly.android.app/issues/58385bbd0aeb16625b4743a7
            try {
                showBuySelectNudge();
            } catch (Exception ex) {
                Crashlytics.logException(ex);
            }
        }
    }

    private void showAppUpdateScreen(int hardAppVersion, int softAppVersion, String hardUpdateText, @Nullable String softUpdateText) {
        if (isAppUpdatePopupShowing) {
            return;
        }
        int appVersionCode = 0;
        try {
            appVersionCode = Integer.parseInt(Utility.getAppVersionCode(aContext));
        } catch (NumberFormatException e) {
            return;
        }
        if (hardAppVersion > appVersionCode && Utility.isSet(hardUpdateText)) {
            TmLogger.i("APP_UPDATE", "Showing hard update screen");
            this.hardUpdateText = hardUpdateText;
            this.hardAppVersion = hardAppVersion;
            isAppUpdatePopupShowing = true;
            AlertsHandler.showAlertDialog(aActivity, hardUpdateText, R.string.update_now, new AlertDialogInterface() {
                @Override
                public void onButtonSelected() {
                    isAppUpdatePopupShowing = false;
                    ActivityHandler.launchPlayStoreTrulymadly(aActivity);
                }
            });
        } else if (softAppVersion > appVersionCode && Utility.isSet(softUpdateText)) {
            long showSoftUpdatePopupFrequency = TimeUtils.HOURS_24_IN_MILLIS;
            //check daily timer and if yes then show soft update screen
            if (TimeUtils.isDifferenceGreater(aContext, ConstantsRF.RIGID_FIELD_LAST_SOFT_UPDATE_SHOWN_TSTAMP, showSoftUpdatePopupFrequency)) {
                TmLogger.i("APP_UPDATE", "Showing soft update screen");
                isAppUpdatePopupShowing = true;
                AlertsHandler.showConfirmDialog(aContext, softUpdateText, R.string.update_now, R.string.may_be_later, new ConfirmDialogInterface() {
                    @Override
                    public void onPositiveButtonSelected() {
                        isAppUpdatePopupShowing = false;
                        ActivityHandler.launchPlayStoreTrulymadly(aActivity);
                    }

                    @Override
                    public void onNegativeButtonSelected() {
                        isAppUpdatePopupShowing = false;
                    }
                }, false);
                RFHandler.insert(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_LAST_SOFT_UPDATE_SHOWN_TSTAMP, TimeUtils.getSystemTimeInMiliSeconds());
            }
        }
    }

    private void parseNotificationCounterResponse(JSONObject response) {
        JSONObject responseObj = response.optJSONObject("counters");
        if (responseObj != null) {
            actionBarHandler.setConversationsCount(responseObj.optInt("conversation_count", 0));
            new UnreadCounterHackAsyncTask(aContext, responseObj.optJSONArray("unread_messages")).execute();
        }
    }

    private void fetchSparksForSparkBlooper(boolean fetchFromServer) {
        if (SparksHandler.isSparksEnabled(aContext)) {
            SparkBlooperModal sparkBlooperModal = SparksHandler.checkForSparkBlooper(aContext);
            if (!sparkBlooperModal.isBlooperRequired()) {
                if (!fetchFromServer) {
                    actionBarHandler.showConversationBadge(false, false);
                    return;
                }
                areSparksFetchedOnce = true;
                if (mSparksReceiverListener == null) {
                    mSparksReceiverListener = new SparksHandler.SparksReceiver.SimpleSparkReceiverListener() {
                        @Override
                        public void onSparkfetchSuccess(int sparksFetched) {
                            if (sparksFetched > 0) {
                                SparkBlooperModal sparkBlooperModal = SparksHandler.checkForSparkBlooper(aContext);
                                if (sparkBlooperModal.isBlooperRequired()) {
                                    if (sparkBlooperModal.isBlooperAnimationRequired() &&
                                            !Utility.stringCompare(sparkBlooperModal.getmNewSparkMatchId(), mNewSparkmatchId)) {
                                        mNewSparkmatchId = sparkBlooperModal.getmNewSparkMatchId();
                                        actionBarHandler.showConversationBadge(true, true);
                                    } else {
                                        actionBarHandler.showConversationBadge(true, false);
                                    }
                                } else {
                                    actionBarHandler.showConversationBadge(false, false);
                                }
                            } else {
                                actionBarHandler.showConversationBadge(false, false);
                            }
                        }
                    };
                }

                if (mSparksReceiverHandler == null) {
                    mSparksReceiverHandler = new SparksHandler.SparksReceiver(aContext, mSparksReceiverListener);
                }

                mSparksReceiverHandler.fetchSparks(false);
            } else {
                areSparksFetchedOnce = true;
                if (sparkBlooperModal.isBlooperAnimationRequired() &&
                        !Utility.stringCompare(sparkBlooperModal.getmNewSparkMatchId(), mNewSparkmatchId)) {
                    mNewSparkmatchId = sparkBlooperModal.getmNewSparkMatchId();
                    actionBarHandler.showConversationBadge(true, true);
                } else {
                    actionBarHandler.showConversationBadge(true, false);
                }
            }
        } else {
            actionBarHandler.showConversationBadge(false, false);
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup parent, int checkedId) {
        switch (checkedId) {
            case R.id.profile_visibility_options_everyone:
                saveDiscoveryOptionOnServer = true;
                isDiscoveryOn = true;
                AlertsHandler.hideMessage();
                break;
            case R.id.profile_visibility_options_only_likes:
                saveDiscoveryOptionOnServer = true;
                isDiscoveryOn = false;
                AlertsHandler.showMessage(aActivity, R.string.visible_to_matches_only, false);
                break;
            default:
                break;
        }
    }

    @SuppressLint({"Override", "NewApi"})
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (grantResults == null || grantResults.length == 0) {
            return;
        }
        switch (requestCode) {
            case PermissionsHelper.REQUEST_CAMERA_STORAGE_PERMISSIONS_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && (grantResults.length == 1 || grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
                    //TODO: FILE HANDLING: Modify this line to have a better implementation
                    FilesHandler.createImageCacheFolder();
                    photoUploader.onPermissionGranted(requestCode);
                } else {
                    photoUploader.onPermissionRejected();
                }
                break;

            case PermissionsHelper.REQUEST_STORAGE_PERMISSIONS_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //TODO: FILE HANDLING: Modify this line to have a better implementation
                    FilesHandler.createImageCacheFolder();
                    photoUploader.onPermissionGranted(requestCode);
                } else {
                    photoUploader.onPermissionRejected();
                }
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    private void resetSparksFAB() {
        mSparkFAB.clearAnimation();
        //mSparkFAB.setImageResource(R.drawable.ic_spark);
        ABHandler.setImageIconForSparkFABIcon(aContext, mSparkFAB, spark_lay_icon_large_ab);
        mSparksScoreTV.setVisibility(View.GONE);

        mSparkCoachmarkTv.clearAnimation();
        mSparkCoachmarkTv.setVisibility(View.GONE);
    }

    private void toggleSparksFAB(boolean isSparksEnabled, boolean isSponsoredProfile, boolean isProfileAd,
                                 boolean hasLikedBefore) {
        //Hiding spark button when
        //1. the given profile is a sponsored profile
        //2. the given profile is a video ad (SeventyNine)
        //3. sparks are not enabled yet

        mSparkFAB.setVisibility((!isSparksEnabled) ? View.GONE : View.VISIBLE);

        if (isSparksEnabled) {
            if (isSponsoredProfile || isProfileAd) {
                mSparkFAB.setBackgroundTintList(ColorStateList.valueOf(mSparkDisabledColor));
                mSparkFAB.setOnClickListener(null);
            } else {
                mSparkFAB.setBackgroundTintList(ColorStateList.valueOf(mSparkEnabledColor));
                mSparkFAB.setOnClickListener(this);


                final AnimationSet translateDownAnimation =
                        (AnimationSet) AnimationUtils.loadAnimation(aContext, R.anim.slide_down_spark_tutorial);
                final AnimationSet translateUpAnimation = (AnimationSet)
                        AnimationUtils.loadAnimation(aContext, R.anim.slide_up_spark_tutorial);
                translateUpAnimation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        isAlreadymatchAnimationRunning = false;
                        mSparkCoachmarkTv.setVisibility(View.GONE);
                        ABHandler.setImageIconForSparkFABIcon(aContext, mSparkFAB, spark_lay_icon_large_ab);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                translateDownAnimation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        mSparkCoachmarkTv.startAnimation(translateUpAnimation);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

                int sparksLeft = SparksHandler.getSparksLeft(aContext);
                if (hasLikedBefore && !isHasLikedBeforeAnimationShownForThisMatch) {
                    isAlreadymatchAnimationRunning = true;
                    isHasLikedBeforeAnimationShownForThisMatch = true;
                    translateDownAnimation.setStartOffset(0);
                    ViewGroup.LayoutParams params = mSparkCoachmarkTv.getLayoutParams();
                    params.width = UiUtils.dpToPx(310);
                    mSparkCoachmarkTv.setLayoutParams(params);
                    mSparkCoachmarkTv.setText(R.string.send_direct_msg_has_liked_before);
                    mSparkCoachmarkTv.setVisibility(View.VISIBLE);
                    mSparkCoachmarkTv.startAnimation(translateDownAnimation);
                    spark_lay_icon_large_ab.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                    Ion.with(spark_lay_icon_large_ab).load(UiUtils.resIdToUri(aContext, R.drawable.spark_icon_animation).toString());
                } else if (!isSparkScoreAnimationShownOnce) {
                    isSparkScoreAnimationShownOnce = true;
                    translateDownAnimation.setStartOffset(500);
                    mSparksScoreTV.setText(String.valueOf(sparksLeft));
                    if (mHideScoreScaleDownAnimation == null) {
                        mHideScoreScaleDownAnimation = new ScaleAnimation(
                                1.2f, 1f,
                                1.2f, 1f,
                                Animation.RELATIVE_TO_SELF, 0.5f,
                                Animation.RELATIVE_TO_SELF, 1
                        );
                        mHideScoreScaleDownAnimation.setDuration(mDuration);
                        mHideScoreScaleDownAnimation.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                                //mSparkFAB.setImageResource(R.drawable.ic_spark);
                                ABHandler.setImageIconForSparkFABIcon(aContext, mSparkFAB, spark_lay_icon_large_ab);
                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });
                    }

                    if (mHideScoreScaleUpAnimation == null) {
                        mHideScoreScaleUpAnimation = new ScaleAnimation(
                                1f, 1.2f,
                                1f, 1.2f,
                                Animation.RELATIVE_TO_SELF, 0.5f,
                                Animation.RELATIVE_TO_SELF, 1
                        );
                        mHideScoreScaleUpAnimation.setDuration(mDuration);
                        mHideScoreScaleUpAnimation.setStartOffset(mDelay);
                        mHideScoreScaleUpAnimation.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                mSparkFAB.startAnimation(mHideScoreScaleDownAnimation);
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });
                    }

                    if (mShowScoreScaleDownAnimation == null) {
                        mShowScoreScaleDownAnimation = new ScaleAnimation(
                                1.2f, 1f,
                                1.2f, 1f,
                                Animation.RELATIVE_TO_SELF, 0.5f,
                                Animation.RELATIVE_TO_SELF, 1
                        );
                        mShowScoreScaleDownAnimation.setDuration(mDuration);
                        mShowScoreScaleDownAnimation.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                                mSparkFAB.setImageResource(0);
                                spark_lay_icon_large_ab.setImageResource(0);
                                mSparksScoreTV.setVisibility(View.VISIBLE);
                                mSparksScoreTV.setAlpha(0);
                                mSparksScoreTV.animate().alpha(1).setDuration(mDuration);
                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                mSparkFAB.startAnimation(mHideScoreScaleUpAnimation);
                                mSparksScoreTV.setAlpha(1);
                                mSparksScoreTV.animate().alpha(0).setStartDelay(mDelay).setDuration(mDuration).setListener(new Animator.AnimatorListener() {
                                    @Override
                                    public void onAnimationStart(Animator animation) {

                                    }

                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        mSparksScoreTV.setVisibility(View.GONE);

                                        if (!SPHandler.getBool(aContext,
                                                ConstantsSP.SHARED_KEY_SPARK_COACHMARK_SHOWN)) {
                                            SPHandler.setBool(aContext,
                                                    ConstantsSP.SHARED_KEY_SPARK_COACHMARK_SHOWN, true);
                                            mSparkCoachmarkTv.setText(R.string.send_direct_msg);
                                            ViewGroup.LayoutParams params = mSparkCoachmarkTv.getLayoutParams();
                                            params.width = UiUtils.dpToPx(200);
                                            mSparkCoachmarkTv.setLayoutParams(params);
                                            mSparkCoachmarkTv.setVisibility(View.VISIBLE);
                                            mSparkCoachmarkTv.startAnimation(translateDownAnimation);
                                        }
//                                            mSparkCoachmark.startAnimation(translateUpAnimation);
                                    }

                                    @Override
                                    public void onAnimationCancel(Animator animation) {

                                    }

                                    @Override
                                    public void onAnimationRepeat(Animator animation) {

                                    }
                                });
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });
                    }

                    if (mSimpleScaleDownAnimation == null) {
                        mSimpleScaleDownAnimation = new ScaleAnimation(
                                1.2f, 1f,
                                1.2f, 1f,
                                Animation.RELATIVE_TO_SELF, 0.5f,
                                Animation.RELATIVE_TO_SELF, 1
                        );
                        mSimpleScaleDownAnimation.setDuration(mDuration);
                        mSimpleScaleDownAnimation.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                if (mAnimationCount == 0) {
                                    mAnimationCount++;
                                    mShowScoreScaleUpAnimation.setStartOffset(0);
                                    mSparkFAB.startAnimation(mShowScoreScaleUpAnimation);
                                } else {
                                    if (!SPHandler.getBool(aContext,
                                            ConstantsSP.SHARED_KEY_SPARK_COACHMARK_SHOWN)) {
                                        SPHandler.setBool(aContext,
                                                ConstantsSP.SHARED_KEY_SPARK_COACHMARK_SHOWN, true);
                                        mSparkCoachmarkTv.setVisibility(View.VISIBLE);
                                        mSparkCoachmarkTv.startAnimation(translateDownAnimation);
                                    }
//                                        mSparkCoachmark.startAnimation(translateUpAnimation);
                                }
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });
                    }

                    if (mShowScoreScaleUpAnimation == null) {
                        mShowScoreScaleUpAnimation = new ScaleAnimation(
                                1, 1.2f,
                                1, 1.2f,
                                Animation.RELATIVE_TO_SELF, 0.5f,
                                Animation.RELATIVE_TO_SELF, 1
                        );
                        mShowScoreScaleUpAnimation.setDuration(mDuration);
                        mShowScoreScaleUpAnimation.setStartOffset(2500);
                        mShowScoreScaleUpAnimation.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                if (SparksHandler.getSparksLeft(aContext) > 0) {
                                    mSparkFAB.startAnimation(mShowScoreScaleDownAnimation);
                                } else {
                                    mSparkFAB.startAnimation(mSimpleScaleDownAnimation);
                                }
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });
                    }

                    mSparkFAB.startAnimation(mShowScoreScaleUpAnimation);
                }
            }
        } else {
            mSparkFAB.setOnClickListener(null);
        }
    }

    private void changeLikeFABAlpha(boolean isSponsoredProfile, boolean isProfileAd,
                                    boolean isSelectProfile, boolean hasLikedBefore) {
        if (!isSponsoredProfile && !isProfileAd && !isSelectProfile && hasLikedBefore) {
            mLikeFAB.setAlpha(0.2f);
        } else {
            mLikeFAB.setAlpha(1f);
        }
    }

    private void showBuySelectNudge() {
        isSelectNudgeVisible = true;
        boolean isSelectMember = TMSelectHandler.isSelectMember(aContext);
        final Map<String, String> eventInfoSelect = new HashMap<>();
        eventInfoSelect.put("is_select_member", String.valueOf(isSelectMember));
        eventInfoSelect.put("source", "matches");
        TrulyMadlyTrackEvent.trackEvent(aContext, select, select_serious_intent_nudge, 0,
                TrulyMadlyEventStatus.view,
                eventInfoSelect, true);
        String content = RFHandler.getString(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_SELECT_NUDGE_CONTENT);
        AlertsHandler.showSimpleSelectDialog(aContext, (Utility.isSet(content)) ? content : aContext.getString(R.string.select_nudge_text),
                aContext.getString(R.string.learn_more), aContext.getString(R.string.no_thanks_caps), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        switch (view.getId()) {
                            case R.id.positive_button_tv:
                                isSelectNudgeVisible = false;
                                TrulyMadlyTrackEvent.trackEvent(aContext, select, select_serious_intent_nudge, 0,
                                        TrulyMadlyEventStatus.clicked_yes,
                                        eventInfoSelect, true);

                                UserModal user = getProfilesList().get(0).getUser();
                                if (user != null && Utility.isSet(user.getmSparkHash())) {
                                    SPHandler.setString(aContext, ConstantsSP.SHARED_KEY_SELECT_SAVED_SELECT_HASH,
                                            user.getmSparkHash());
                                } else {
                                    SPHandler.remove(aContext, ConstantsSP.SHARED_KEY_SELECT_SAVED_SELECT_HASH);
                                }

                                ActivityHandler.startTMSelectActivity(aContext, select_serious_intent_nudge + ":matches", false, likes_done, hides_done, sparks_done,
                                        Utility.stringCompare(mTrackingMap.get(
                                                EventInfoUpdateEvent.getKeyString(EventInfoUpdateEvent.KEY_FAVORITES_VIEWED)), "true")
                                        , getProfilesList().get(0).getUserId());
                                break;

                            case R.id.negative_button_tv:
                                isSelectNudgeVisible = false;
                                TrulyMadlyTrackEvent.trackEvent(aContext, select, select_serious_intent_nudge, 0,
                                        TrulyMadlyEventStatus.clicked_no,
                                        eventInfoSelect, true);
                                break;
                        }
                    }
                }, new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        isSelectNudgeVisible = false;
                    }
                });
    }

    private void showOnActionSelectNudge(String myImage, String matchImage, String onAction) {
        boolean isSelectMember = TMSelectHandler.isSelectMember(aContext);
        final Map<String, String> eventInfoSelect = new HashMap<>();
        eventInfoSelect.put("on_action", onAction);
        eventInfoSelect.put("source", "matches");
        eventInfoSelect.put("is_select_member", String.valueOf(TMSelectHandler.isSelectMember(aContext)));
        eventInfoSelect.put("is_profile_select", String.valueOf(getProfilesList().get(0).isTMSelectMember()));
        TrulyMadlyTrackEvent.trackEvent(aContext,
                select, select_on_action_nudge, 0,
                TrulyMadlyEventStatus.view, eventInfoSelect, true);

        AlertsHandler.showOnActionSelectDialog(aContext, myImage,
                matchImage, R.string.please_join_tm_select_toview, TMSelectHandler.getSelectProfileCta(aContext),
                aContext.getString(R.string.may_be_later),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        switch (view.getId()) {
                            case R.id.join_now_button:
                                TrulyMadlyTrackEvent.trackEvent(aContext,
                                        select, select_on_action_nudge, 0,
                                        TrulyMadlyEventStatus.clicked_yes, eventInfoSelect, true);
                                UserModal user = getProfilesList().get(0).getUser();
                                if (user != null && Utility.isSet(user.getmSparkHash())) {
                                    SPHandler.setString(aContext, ConstantsSP.SHARED_KEY_SELECT_SAVED_SELECT_HASH,
                                            user.getmSparkHash());
                                } else {
                                    SPHandler.remove(aContext, ConstantsSP.SHARED_KEY_SELECT_SAVED_SELECT_HASH);
                                }
                                ActivityHandler.startTMSelectActivity(aContext, select_on_action_nudge + ":matches", false, likes_done, hides_done, sparks_done,
                                        Utility.stringCompare(mTrackingMap.get(
                                                EventInfoUpdateEvent.getKeyString(EventInfoUpdateEvent.KEY_FAVORITES_VIEWED)), "true")
                                        , getProfilesList().get(0).getUserId());
                                break;

                            case R.id.maybe_later_button:
                                TrulyMadlyTrackEvent.trackEvent(aContext,
                                        select, select_on_action_nudge, 0,
                                        TrulyMadlyEventStatus.clicked_no, eventInfoSelect, true);
                                break;
                        }
                    }
                });
    }

    private boolean toggleSelectQuizResult(String matchId, String matchName, String matchUrl,
                                           String userUrl, String selectCompatibilityString,
                                           String selectQuote) {
        if (mSelectQuizresultFragment == null) {
            if (mSelectQuizCompareListener == null) {
                mSelectQuizCompareListener = new SelectQuizResultFragment.SelectQuizCompareListener() {
                    @Override
                    public void closeFragment() {
                        onBackPressed();
                    }
                };
            }
            mSelectQuizresultFragment = SelectQuizResultFragment.newInstance(new
                            SelectQuizCompareModal(matchId, matchName, matchUrl, userUrl),
                    selectCompatibilityString, selectQuote);
            getSupportFragmentManager().beginTransaction().add(R.id.select_quiz_result_container,
                    mSelectQuizresultFragment).commit();
        } else if (Utility.stringCompare(matchId, mSelectQuizresultFragment.getMatchId())) {
            mSelectQuizresultFragment.reInitialize(new SelectQuizCompareModal(matchId,
                    matchName, matchUrl, userUrl), selectCompatibilityString, selectQuote);
        } else {
            mSelectQuizresultFragment.registerListener(null);
            getSupportFragmentManager().beginTransaction().remove(mSelectQuizresultFragment).commit();
            mSelectQuizresultFragment = null;
            return toggleSelectQuizResult(matchId, matchName, matchUrl, userUrl, selectCompatibilityString, selectQuote);
        }
        mSelectQuizresultFragment.registerListener(mSelectQuizCompareListener);
        mSelectQuizResultContainer.setVisibility(View.VISIBLE);
        isSelectQuizCompareVisible = true;
        return true;
    }

    private enum PREFETCH_MODE {
        initial, next, swipe
    }

    private class FetchMatchesFromDb extends AsyncTask<Void, Void, Vector<ProfileNewModal>> {

        private final JSONObject response;

        public FetchMatchesFromDb(JSONObject response) {
            this.response = response;
        }

        @Override
        protected Vector<ProfileNewModal> doInBackground(Void... voids) {

            Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
            JSONArray array = MatchesDbHandler.getProfileList(aContext);
            if (array != null && array.length() > 0) {
                ProfileNewResponseParser aProfileNewResponseParser = new ProfileNewResponseParser();
                return aProfileNewResponseParser.parseProfileModals(array);
            } else {
                return null;
            }
        }


        @Override
        protected void onPostExecute(Vector<ProfileNewModal> newMatches) {

            if (newMatches != null && newMatches.size() > 0) {
                profilesList = newMatches;
                aMatchesLatestModal2.setProfileNewModalList(getProfilesList());
                responseParserContinuation(this.response);
            } else {
                //INFINITE LOOP ISSUE - Find the correct fix and remove the following hack
                MatchesDbHandler.clearMatchesCache(aContext);
                issueRequest(MATCH_NEW);
            }
        }
    }

    private class FetchMatches extends AsyncTask<Void, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(Void... voids) {

            Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
            secondCallInProgress = true;
            try {
                String jsonString = OkHttpHandler.httpGetSynchronous(aContext, ConstantsUrls.get_matches_url(), createParamsForMatchesCall(2));


                if (Utility.isSet(jsonString)) {
                    return new JSONObject(jsonString);
                } else {
                    return null;
                }
            } catch (JSONException e) {
                return null;
            }


        }

        @Override
        protected void onPostExecute(JSONObject response) {
            secondCallInProgress = false;
            if (response != null && response.optString("responseCode").equalsIgnoreCase("200")) {
                makeSecondMatchesCall = false;
                RFHandler.insert(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_HAS_MORE_MATCHES, false);
            }
            if (response != null && getProfilesList() != null && getProfilesList().size() > 1) {
                String id = getProfilesList().get(0).getUserId();
                MatchesDbHandler.deleteDataAfterCertainId(aContext, id);
                MatchesDbHandler.insertMatches(aContext, response);

                ProfileNewResponseParser profileNewResponseParser = new ProfileNewResponseParser();
                Vector<ProfileNewModal> newList = profileNewResponseParser.parseProfileModals(MatchesDbHandler.getProfileList(aContext));


                if (newList != null) {

                    int i = getProfilesList().size() - 1;

                    while (i > 0) {
                        getProfilesList().remove(i);
                        i--;
                    }

                    for (i = 1; i < newList.size(); i++) {
                        addProfile(newList.get(i));
//                        getProfilesList().add(newList.get(i));
                    }
                }

            }

        }

    }

}
