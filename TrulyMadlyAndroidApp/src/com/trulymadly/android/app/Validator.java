package com.trulymadly.android.app;

import android.widget.EditText;

import com.trulymadly.android.app.utility.Utility;

class Validator {

	private final String defaultEmptyErrorMessage = "This field cannot be empty.";

	public Validator() {
	}

	private Boolean nameValidator(String name) {
		name = name.trim();
		return !name.isEmpty();
	}
	public Boolean nameValidator(String name, EditText nameEditText) {
		return this.nameValidator(name, nameEditText, defaultEmptyErrorMessage);
	}

	private Boolean nameValidator(String name, EditText nameEditText, String optEmptyErrorMessage) {
		if(this.nameValidator(name)){
			nameEditText.setError(null);
			return true;
		} else { 
			nameEditText.setError(optEmptyErrorMessage);
			requestFocus(nameEditText);
			return false;
		}
	}

	public boolean genderValidator(String gender) {
		return gender != null && (gender.equalsIgnoreCase("m") || gender.equalsIgnoreCase("f"));
	}
	
	public boolean emailValidator(String email, EditText emailEditText) {
		return this.emailValidator(email, emailEditText, defaultEmptyErrorMessage);
	}
	
	public Boolean emailValidator(String email, EditText emailEditText, String optEmptyErrorMessage) {
		if(Utility.checkEmail(email)){
			emailEditText.setError(null);
			return true;
		} else { 
			emailEditText.setError(optEmptyErrorMessage);
			requestFocus(emailEditText);
			return false;
		}
	}

	public boolean passwordValidator(String passwd,
			EditText passwdEditText) {
		if(!this.nameValidator(passwd)){
			passwdEditText.setError(defaultEmptyErrorMessage);
			requestFocus(passwdEditText);
			return false;
		} else if(passwd.length()<6){
			passwdEditText.setError("Password must be minimium 6 characters long.");
			requestFocus(passwdEditText);
			return false;
		} else {
			passwdEditText.setError(null);
			return true;
		}
	}
	
	public boolean passwordValidator(String passwd, String passwdConfirm,
			EditText passwdEditText, EditText passwdConfirmEditText) {
		if(!this.nameValidator(passwd)){
			passwdEditText.setError(defaultEmptyErrorMessage);
			requestFocus(passwdEditText);
			return false;
		} else if(passwd.length()<6){
			passwdEditText.setError("Password must be minimium 6 characters long.");
			requestFocus(passwdEditText);
			return false;
		} else if(!passwd.equalsIgnoreCase(passwdConfirm)){
			String passwordConfirmFail = "Passwords do not match";
			passwdConfirmEditText.setError(passwordConfirmFail);
			requestFocus(passwdEditText);
			return false;
		} else {
			passwdEditText.setError(null);
			return true;
		}
	}

	private void requestFocus(EditText editText) {
		try {
			editText.requestFocus();
		} catch (ClassCastException ignored) {

		}
	}

}
