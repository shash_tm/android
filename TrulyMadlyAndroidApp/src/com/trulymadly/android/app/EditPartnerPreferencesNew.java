package com.trulymadly.android.app;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.moe.pushlibrary.MoEHelper;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes;
import com.trulymadly.android.app.adapter.EditPrefAdapter;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.Constants.HttpRequestType;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.json.EditPreferencePrefillNewResponseParser;
import com.trulymadly.android.app.json.EditPreferenceSaveResponseParser;
import com.trulymadly.android.app.listener.CachedDataInterface;
import com.trulymadly.android.app.listener.CustomOkHttpResponseHandler;
import com.trulymadly.android.app.listener.OnActionBarClickedInterface;
import com.trulymadly.android.app.listener.TapToRetryListener;
import com.trulymadly.android.app.modal.EditPrefBasicDataModal;
import com.trulymadly.android.app.modal.EditPrefModel;
import com.trulymadly.android.app.modal.EditPrefSaveModal;
import com.trulymadly.android.app.modal.EditPreferencePrefillModelNew;
import com.trulymadly.android.app.sqlite.CachingDBHandler;
import com.trulymadly.android.app.sqlite.EditPrefDBHandler;
import com.trulymadly.android.app.sqlite.MatchesDbHandler;
import com.trulymadly.android.app.utility.ActionBarHandler;
import com.trulymadly.android.app.utility.ActivityHandler;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.CachedDataHandler;
import com.trulymadly.android.app.utility.OkHttpHandler;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.TapToRetryHandler;
import com.trulymadly.android.app.utility.Utility;
import com.trulymadly.android.app.views.MenuSlideLayout;
import com.trulymadly.android.app.views.RangeSeekBar;
import com.trulymadly.android.app.views.RangeSeekBar.OnRangeSeekBarChangeListener;

import org.apmem.tools.layouts.FlowLayout;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

import static butterknife.ButterKnife.findById;
import static com.google.common.base.Preconditions.checkNotNull;

public class EditPartnerPreferencesNew extends AppCompatActivity {

    public static final int ANNUAL_INCOME = 6;
    private static boolean isDataModified = false;
    private final int COUNTRY = 2;
    private final int STATE = 3;
    private final int CITY = 4;
    private final int HIGHEST_DEGREE = 5;
    @BindView(R.id.menu_slide_layout)
    MenuSlideLayout mLayout;
    @BindView(R.id.menu_listview)
    ListView lvMenu;
    @BindView(R.id.continue_btn)
    Button continue_btn;
    @BindView(R.id.age_min)
    TextView age_min;
    @BindView(R.id.age_max)
    TextView age_max;
    @BindView(R.id.height_min)
    TextView height_min;
    @BindView(R.id.height_max)
    TextView height_max;
    @BindView(R.id.status_single)
    CheckBox status_single;
    @BindView(R.id.status_married_before)
    CheckBox status_married_before;
    @BindView(R.id.status_any)
    CheckBox status_any;
    @BindView(R.id.other_lay_2)
    RelativeLayout other_lay_2;
    @BindView(R.id.other_lay_3)
    RelativeLayout other_lay_3;
    @BindView(R.id.name_1)
    TextView name_1;
    @BindView(R.id.name_2)
    TextView name_2;
    @BindView(R.id.name_3)
    TextView name_3;
    @BindView(R.id.other_row_cb_1)
    CheckBox other_row_cb_1;
    @BindView(R.id.other_row_cb_2)
    CheckBox other_row_cb_2;
    @BindView(R.id.other_row_cb_3)
    CheckBox other_row_cb_3;
    @BindView(R.id.country_tv)
    TextView country_tv;
    @BindView(R.id.state_tv)
    TextView state_tv;
    @BindView(R.id.city_tv)
    TextView city_tv;
    @BindView(R.id.highest_degree_tv)
    TextView highest_degree_tv;
    @BindView(R.id.income_tv)
    TextView income_tv;
    @BindView(R.id.city_lay)
    RelativeLayout city_lay;
    @BindView(R.id.us_profile_edit_pref_container)
    View us_profile_edit_pref_container;
    @BindView(R.id.us_profile_edit_pref_switch)
    SwitchCompat us_profile_edit_pref_switch;
    private EditPreferencePrefillModelNew aEditPreferencePrefillModelNew = null;
    private RangeSeekBar<Integer> ageSeekBar;
    private RangeSeekBar<Integer> heightSeekBar;
    private double min_height_val = 4.0;
    private double max_height_val = 8.0;
    private int selectedItem = 0;
    private ArrayList<EditPrefModel> editPrefArrayList;
    private EditPrefAdapter editPrefAdapter;
    private String countryIds = "", stateIds = "", cityIds = "",
            highestDegreeIds = "", incomeIds = "";
    private String start_height = "48", end_height = "96";
    private int min_age_val = 18, max_age_val = 70;
    private ArrayList<EditPrefBasicDataModal> countriesList = null;
    private ArrayList<EditPrefBasicDataModal> statesList = null;
    private ArrayList<EditPrefBasicDataModal> citiesList = null;
    private ArrayList<EditPrefBasicDataModal> highestDegreesList = null;
    private ArrayList<EditPrefBasicDataModal> incomesList = null;
    private ArrayList<EditPrefBasicDataModal> citiesListNew = null;
    private HashMap<String, Boolean> selectedCountryIDsMap = null;
    private HashMap<String, Boolean> selectedStateIDsMap = null;
    private HashMap<String, Boolean> selectedCitiesIDsMap = null;
    private HashMap<String, Boolean> selectedHighestDegreeIDsMap = null;
    private HashMap<String, Boolean> selectedIncomeIDsMap = null;
    private String[] tiles = null;
    private FlowLayout countries_container, states_container, cities_container,
            highest_degrees_container, annual_incomes_container;
    private ArrayList<TextView> allCountriesTextViews = null;
    private ArrayList<TextView> allStatesTextViews = null;
    private ArrayList<TextView> allCitiesTextViews = null;
    private ArrayList<TextView> allDegreesTextViews = null;
    private ArrayList<TextView> allAnnualIncomesTextViews = null;
    private Context aContext;
    private Activity aActivity;
    private String bachelorIds = null, masterIds = null;

    private TapToRetryHandler tapToRetryHandler = null;
    private MoEHelper mHelper = null;
    private CachedDataInterface callback;
    private String prefillUrl;

    private Handler mHandler;
    private Runnable startMatchesActivityRunnable;
    private boolean show_us_profiles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        aContext = this;
        aActivity = this;
        mHandler = new Handler();
        try {
            setContentView(R.layout.editpartner_per_new);
            ButterKnife.bind(this);
        } catch (OutOfMemoryError e) {
            System.gc();
            finish();
            return;
        }

        mHelper = new MoEHelper(this);
        isDataModified = false;

        countries_container = findById(mLayout, R.id.countries_container);
        states_container = findById(mLayout, R.id.states_container);
        cities_container = findById(mLayout, R.id.cities_container);
        highest_degrees_container = findById(mLayout, R.id.highest_degrees_container);
        annual_incomes_container = findById(mLayout, R.id.annual_incomes_container);

        continue_btn.setEnabled(false);

        city_lay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                onCityClick();
            }
        });

        ageSeekBar = new RangeSeekBar<>(18, 70, this);
        ageSeekBar
                .setOnRangeSeekBarChangeListener(new OnRangeSeekBarChangeListener<Integer>() {
                    @Override
                    public void onRangeSeekBarValuesChanged(
                            Integer minValue,
                            Integer maxValue) {

                        setDataModified();

                        min_age_val = minValue;
                        max_age_val = maxValue;

                        age_min.setText(min_age_val + "");
                        age_max.setText(max_age_val + "");
                    }

                    @Override
                    public void onRangeSeekBaarTouchValue(Integer minValue,
                                                          Integer maxValue) {

                        age_min.setText(minValue + "");
                        age_max.setText(maxValue + "");
                    }
                });

        // add RangeSeekBar to pre-defined layout
        ViewGroup age_layout = findById(this, R.id.age_seekbar_lay);
        age_layout.addView(ageSeekBar);

        try {
            heightSeekBar = new RangeSeekBar<>(48, 96, this);
            heightSeekBar
                    .setOnRangeSeekBarChangeListener(new OnRangeSeekBarChangeListener<Integer>() {
                        @Override
                        public void onRangeSeekBarValuesChanged(
                                Integer minValue,
                                Integer maxValue) {

                            setDataModified();

                            min_height_val = minValue;
                            int feet = minValue / 12;
                            int inch = minValue % 12;
                            height_min.setText(feet + "\'" + inch + "\"");
                            max_height_val = maxValue;
                            feet = maxValue / 12;
                            inch = maxValue % 12;
                            // String endH = feet + "." + inch;
                            height_max.setText(feet + "\'" + inch + "\"");

                            start_height = Integer.toString(minValue);
                            end_height = Integer.toString(maxValue);
                        }

                        @Override
                        public void onRangeSeekBaarTouchValue(Integer minValue,
                                                              Integer maxValue) {

                            min_height_val = minValue;
                            int feet = minValue / 12;
                            int inch = minValue % 12;
                            // String startH = feet + "." + inch;
                            height_min.setText(feet + "\'" + inch + "\"");

                            max_height_val = maxValue;

                            feet = maxValue / 12;
                            inch = maxValue % 12;
                            // String endH = feet + "." + inch;
                            height_max.setText(feet + "\'" + inch + "\"");
                        }
                    });

            ViewGroup height_layout = findById(this, R.id.height_seekbar_lay);
            height_layout.addView(heightSeekBar);
        } catch (OutOfMemoryError ignored) {
        }

        new ActionBarHandler(this, getResources().getString(R.string.edit_preferences), null, new OnActionBarClickedInterface() {
            @Override
            public void onBackClicked() {
                onBackPressed();
            }

            @Override
            public void onLocationClicked() {

            }

            @Override
            public void onUserProfileClicked() {

            }

            @Override
            public void onConversationsClicked() {

            }

            @Override
            public void onTitleClicked() {

            }

            @Override
            public void onTitleLongClicked() {

            }

            @Override
            public void onSearchViewOpened() {

            }

            @Override
            public void onSearchViewClosed() {

            }

            @Override
            public void onCategoriesClicked() {

            }

            @Override
            public void onCuratedDealsClicked() {

            }
        }, false, false, false);

        tapToRetryHandler = new TapToRetryHandler(this,
                findViewById(android.R.id.content), new TapToRetryListener() {
            @Override
            public void reInitialize() {
                issueRequest(Constants.EDIT_PREFERENCE_PREFILL,
                        ConstantsUrls.get_editpreference_new_url(), null);
            }
        }, null);


        callback = new CachedDataInterface() {
            @Override
            public void onSuccess(JSONObject response) {
                if (tapToRetryHandler != null)
                    tapToRetryHandler.onSuccessFull();
                responseParser(response, Constants.EDIT_PREFERENCE_PREFILL, ConstantsUrls.get_editpreference_new_url());

            }

            @Override
            public void onError(Exception e) {
                tapToRetryHandler.onNetWorkFailed(e);

            }
        };

        issueRequest(Constants.EDIT_PREFERENCE_PREFILL,
                ConstantsUrls.get_editpreference_new_url(), null);
//        if (Utility.isNetworkAvailable(aContext))
//            issueRequest(Constants.EDIT_PREFERENCE_PREFILL,
//                    Constants.editpreference_new_url);
//        else
//            tapToRetryHandler.onNetWorkFailed(null);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void displayNewItems(String newItemName, int n) {

        TextView tv = (TextView) LayoutInflater.from(this).inflate(
                R.layout.custom_item_2, null);

        tv.setText(newItemName);
        tv.setClickable(false);

        switch (n) {
            case COUNTRY:
                allCountriesTextViews.add(tv);
                countries_container.addView(tv, 0);
                break;

            case STATE:
                allStatesTextViews.add(tv);
                states_container.addView(tv, 0);
                break;

            case CITY:
                allCitiesTextViews.add(tv);
                cities_container.addView(tv, 0);
                break;

            case HIGHEST_DEGREE:
                allDegreesTextViews.add(tv);
                highest_degrees_container.addView(tv, 0);
                break;

            case ANNUAL_INCOME:
                allAnnualIncomesTextViews.add(tv);
                annual_incomes_container.addView(tv, 0);
                break;

            default:
                break;
        }
    }

    private void removeAllDynamicItems(int n) {

        switch (n) {
            case COUNTRY:
                if (allCountriesTextViews != null)
                    allCountriesTextViews.clear();
                countries_container.removeAllViews();
                allCountriesTextViews = null;
                break;

            case STATE:
                if (allStatesTextViews != null)
                    allStatesTextViews.clear();
                states_container.removeAllViews();
                allStatesTextViews = null;
                break;

            case CITY:
                if (allCitiesTextViews != null)
                    allCitiesTextViews.clear();
                cities_container.removeAllViews();
                allCitiesTextViews = null;
                break;

            case HIGHEST_DEGREE:
                if (allDegreesTextViews != null)
                    allDegreesTextViews.clear();
                highest_degrees_container.removeAllViews();
                allDegreesTextViews = null;
                break;
            case ANNUAL_INCOME:
                if (allAnnualIncomesTextViews != null)
                    allAnnualIncomesTextViews.clear();
                annual_incomes_container.removeAllViews();
                allAnnualIncomesTextViews = null;
                break;

            default:
                break;
        }
    }

    private void toggleMenu() {
        mLayout.toggleMenu();
    }

    private void populateSideMenu(int n) {

        boolean isEmpty = true;

        if (n == HIGHEST_DEGREE) {
            other_lay_3.setVisibility(View.VISIBLE);
            other_lay_2.setVisibility(View.VISIBLE);
            name_2.setText(R.string.any_bachelors);
            name_3.setText(R.string.any_masters);
            name_1.setText(R.string.doesnt_matter);
        } else {
            other_lay_2.setVisibility(View.GONE);
            other_lay_3.setVisibility(View.GONE);
            name_1.setText(R.string.doesnt_matter);
        }

        editPrefArrayList = null;
        editPrefArrayList = new ArrayList<>();
        int size = 0;
        switch (n) {
            case COUNTRY:
                if (countriesList != null && countriesList.size() > 0)
                    size = countriesList.size();
                break;

            case STATE:
                if (statesList != null && statesList.size() > 0)
                    size = statesList.size();
                break;

            case CITY:
                if (citiesListNew != null && citiesListNew.size() > 0) {
                    size = citiesListNew.size();
                } else {
                    if (citiesList != null && citiesList.size() > 0) {
                        citiesListNew = new ArrayList<>();
                        if (selectedStateIDsMap != null
                                && selectedStateIDsMap.size() > 0) {
                            for (EditPrefBasicDataModal aCity : citiesList)
                                if (selectedStateIDsMap.containsKey(aCity
                                        .getStateId()))
                                    citiesListNew.add(aCity);
                        } else {
                            if (stateIds != null && !stateIds.equals("")) {
                                String[] statesArr = stateIds.split(",");
                                int len = statesArr.length;
                                for (String aStatesArr : statesArr) {
                                    String[] countryState = aStatesArr.split("-");
                                    for (EditPrefBasicDataModal c : citiesList) {
                                        if (c.getIDorCountryID().equals(
                                                countryState[0])
                                                && c.getStateId().equals(
                                                countryState[1]))
                                            citiesListNew.add(c);
                                    }
                                }
                            } else
                                citiesListNew = citiesList;
                        }
                        size = citiesListNew.size();
                    }
                }
                break;

            case HIGHEST_DEGREE:
                if (highestDegreesList != null && highestDegreesList.size() > 0)
                    size = highestDegreesList.size();
                break;

            case ANNUAL_INCOME:
                if (incomesList != null && incomesList.size() > 0)
                    size = incomesList.size();
                break;

            default:
                break;
        }
        for (int i = 0; i < size; i++) {
            EditPrefModel aEditPrefModel = null;

            if (n == COUNTRY) {
                aEditPrefModel = createEditPrefModel(n, countriesList.get(i)
                                .getName(), countriesList.get(i).getIDorCountryID(),
                        countriesList.get(i).getIsSelected());
            } else if (n == STATE) {
                aEditPrefModel = createEditPrefModel(n, statesList.get(i)
                        .getName(), statesList.get(i).getStateId(), statesList
                        .get(i).getIsSelected());
            } else if (n == CITY) {
                aEditPrefModel = createEditPrefModel(n, citiesListNew.get(i)
                                .getName(), citiesListNew.get(i).getCityId(),
                        citiesListNew.get(i).getIsSelected());
            } else if (n == HIGHEST_DEGREE) {
                aEditPrefModel = createEditPrefModel(n,
                        highestDegreesList.get(i).getName(),
                        Integer.toString(highestDegreesList.get(i)
                                .getDegreeId()), highestDegreesList.get(i)
                                .getIsSelected());
            } else if (n == ANNUAL_INCOME && incomesList != null) {
                aEditPrefModel = createEditPrefModel(n, incomesList.get(i)
                                .getName(), incomesList.get(i).getIDorCountryID(),
                        incomesList.get(i).getIsSelected());
            }
            if (aEditPrefModel != null) {
                editPrefArrayList.add(aEditPrefModel);
                if (isEmpty && aEditPrefModel.isSelected())
                    isEmpty = false;
            }
        }
        other_row_cb_1.setChecked(isEmpty);
    }

    private EditPrefModel createEditPrefModel(int n, String name, String id,
                                              boolean isSelected) {
        if (Utility.isSet(id)) {
            if (n == ANNUAL_INCOME) {
                return new EditPrefModel(name, id, isSelected);
            } else {
                return new EditPrefModel(name, Integer.parseInt(id), isSelected);
            }
        }
        return null;
    }

    private void initializeSideMenu(int n) {
        populateSideMenu(n);

        editPrefAdapter = new EditPrefAdapter(this, editPrefArrayList,
                other_row_cb_1, other_row_cb_2, other_row_cb_3);
        lvMenu.setAdapter(editPrefAdapter);
        lvMenu.setItemsCanFocus(false);
    }

    private void setSelectedItemText(int n) {

        String[] selectItems = editPrefAdapter.getSelectedItems(n);

        if (selectItems != null && Utility.isSet(selectItems[0])) {
            tiles = null;
            tiles = selectItems[0].split(",");
            int l;
            switch (n) {

                case COUNTRY:

                    country_tv.setVisibility(View.GONE);
                    countries_container.setVisibility(View.VISIBLE);
                    removeAllDynamicItems(n);
                    allCountriesTextViews = null;
                    allCountriesTextViews = new ArrayList<>();
                    l = tiles.length;

                    for (int i = 0; i < l; i++)
                        displayNewItems(tiles[i], n);

                    boolean isStateVisible = false;

                    if (!selectItems[1].equals("")) {
                        countryIds = selectItems[1];
                        selectedCountryIDsMap = null;
                        selectedCountryIDsMap = editPrefAdapter
                                .getSelectedItemIDs(n);

                        isStateVisible = selectedCountryIDsMap != null
                                && selectedCountryIDsMap
								.containsKey(String.valueOf(Constants.COUNTRY_ID_INDIA));

                        int count = 0;
                        for (EditPrefBasicDataModal aCountry : countriesList) {
                            if (selectedCountryIDsMap != null
                                    && selectedCountryIDsMap.containsKey(aCountry
                                    .getIDorCountryID()))
                                countriesList.get(count).setIsSelected(true);
                            else
                                countriesList.get(count).setIsSelected(false);
                            count++;
                        }
                    }
                    if (isStateVisible) {
                        if (!Utility.isSet(stateIds)) {
                            state_tv.setText("");
                            state_tv.setHint(R.string.select_state);
                        }
                    } else {
                        stateIds = null;
                        cityIds = null;
                        // state_dyn_lay.removeAllViews();
                        removeAllDynamicItems(STATE);
                        // city_dyn_lay.removeAllViews();
                        removeAllDynamicItems(CITY);

                        int count = 0;

                        if (statesList != null && statesList.size() > 0) {
                            for (EditPrefBasicDataModal s : statesList) {
                                if (s.getIsSelected()) {
                                    statesList.get(count).setIsSelected(false);
                                }
                                count++;
                            }
                        }

                        count = 0;
                        if (citiesList != null && citiesList.size() > 0) {
                            for (EditPrefBasicDataModal c : citiesList) {
                                if (c.getIsSelected()) {
                                    citiesList.get(count).setIsSelected(false);
                                }
                                count++;
                            }
                        }

                        count = 0;
                        if (citiesListNew != null && citiesListNew.size() > 0) {
                            for (EditPrefBasicDataModal c : citiesListNew) {
                                if (c.getIsSelected()) {
                                    citiesListNew.get(count).setIsSelected(false);
                                }
                                count++;
                            }
                        }
                    }
                    setCitySelectorEnabled(false);
                    cityIds = null;

                    break;

                case STATE:
                    state_tv.setVisibility(View.GONE);
                    states_container.setVisibility(View.VISIBLE);
                    removeAllDynamicItems(n);
                    allStatesTextViews = null;
                    allStatesTextViews = new ArrayList<>();

                    l = tiles.length;

                    for (int i = 0; i < l; i++)
                        displayNewItems(tiles[i], n);
                    if (selectItems[1] != null && !selectItems[1].equals("")) {

                        stateIds = selectItems[1];
                        // getCities(countryIds,stateIds);
                        if (!selectItems[1].equals("")) {
                            stateIds = selectItems[1];
                            // getStates(selectItems[1]);
                            selectedStateIDsMap = null;
                            selectedStateIDsMap = editPrefAdapter
                                    .getSelectedItemIDs(n);
                            int count = 0;
                            for (EditPrefBasicDataModal aState : statesList) {
                                if (selectedStateIDsMap != null
                                        && selectedStateIDsMap.containsKey(aState
                                        .getStateId()))
                                    statesList.get(count).setIsSelected(true);
                                else
                                    statesList.get(count).setIsSelected(false);

                                count++;
                            }

                            ArrayList<EditPrefBasicDataModal> localCitiesList = new ArrayList<>();
                            count = 0;

                            if (citiesList != null && citiesList.size() > 0) {
                                for (EditPrefBasicDataModal city : citiesList) {
                                    if (selectedStateIDsMap != null
                                            && selectedStateIDsMap.size() > 0
                                            && selectedStateIDsMap.containsKey(city
                                            .getStateId())) {
                                        localCitiesList.add(city);
                                    } else {
                                        citiesList.get(count).setIsSelected(false);
                                    }
                                    count++;
                                }
                            }
                            // if(localCitiesList!=null && localCitiesList.size()>0)
                            citiesListNew = null;
                            citiesListNew = localCitiesList;
                            // }
                        }
                    }

                    setCitySelectorEnabled(true);

                    String cityTxt = getSelectedCities();

                    if (Utility.isSet(cityTxt)) {
                        tiles = null;
                        tiles = cityTxt.split(",");
                        cities_container.setVisibility(View.VISIBLE);
                        city_tv.setVisibility(View.GONE);
                        cities_container.setVisibility(View.VISIBLE);
                        removeAllDynamicItems(CITY);
                        allCitiesTextViews = null;
                        allCitiesTextViews = new ArrayList<>();
                        l = tiles.length;
                        for (int i = 0; i < l; i++)
                            displayNewItems(tiles[i], CITY);
                    } else {
                        city_tv.setVisibility(View.VISIBLE);
                        city_tv.setText("");
                        city_tv.setHint(R.string.select_city);
                        cities_container.setVisibility(View.GONE);
                    }
                    break;

                case CITY:
                    city_tv.setVisibility(View.GONE);
                    cities_container.setVisibility(View.VISIBLE);
                    removeAllDynamicItems(n);
                    allCitiesTextViews = null;
                    allCitiesTextViews = new ArrayList<>();
                    l = tiles.length;
                    for (int i = 0; i < l; i++)
                        displayNewItems(tiles[i], n);

                    if (Utility.isSet(selectItems[1])) {
                        cityIds = selectItems[1];
                        selectedCitiesIDsMap = null;
                        selectedCitiesIDsMap = editPrefAdapter
                                .getSelectedItemIDs(n);
                    }

                    int count = 0;
                    if (citiesListNew != null) {
                        for (EditPrefBasicDataModal aCity : citiesListNew) {

                            if (selectedStateIDsMap != null) {
                                if (selectedStateIDsMap.containsKey(aCity
                                        .getStateId())
                                        && selectedCitiesIDsMap != null
                                        && selectedCitiesIDsMap.containsKey(aCity
                                        .getCityId())) {

                                    citiesListNew.get(count).setIsSelected(true);
                                } else {
                                    citiesListNew.get(count).setIsSelected(false);
                                }
                            } else {
                                if (stateIds != null && !stateIds.equals("")) {
                                    String stateArr[] = stateIds.split(",");
                                    int len = stateArr.length;

                                    for (String aStateArr : stateArr) {

                                        String countryStateArr[] = aStateArr.split("-");

                                        if (aCity.getStateId().equals(
                                                countryStateArr[1])
                                                && selectedCitiesIDsMap != null
                                                && selectedCitiesIDsMap
                                                .containsKey(aCity
                                                        .getCityId())) {
                                            citiesListNew.get(count).setIsSelected(
                                                    true);
                                        } else {
                                            citiesListNew.get(count).setIsSelected(
                                                    false);
                                        }
                                    }
                                }
                            }
                            count++;
                        }
                    }
                    break;

                case HIGHEST_DEGREE:

                    if (selectItems[0] != null && !(selectItems[0]).equals("")) {

                        highest_degree_tv.setVisibility(View.GONE);
                        highest_degrees_container.setVisibility(View.VISIBLE);
                        removeAllDynamicItems(n);
                        allDegreesTextViews = null;
                        allDegreesTextViews = new ArrayList<>();
                        l = tiles.length;
                        for (int i = 0; i < l; i++)
                            displayNewItems(tiles[i], n);
                    } else {
                        highest_degree_tv.setVisibility(View.VISIBLE);
                        highest_degree_tv.setText("");
                        highest_degree_tv.setHint(R.string.select_highest_degree);
                        highest_degrees_container.setVisibility(View.GONE);
                    }

                    if (!selectItems[1].equals("")) {

                        highestDegreeIds = selectItems[1];

                        selectedHighestDegreeIDsMap = null;
                        selectedHighestDegreeIDsMap = editPrefAdapter
                                .getSelectedItemIDs(n);

                        count = 0;
                        if (highestDegreesList != null) {
                            for (EditPrefBasicDataModal aDegree : highestDegreesList) {
                                if (selectedHighestDegreeIDsMap != null
                                        && selectedHighestDegreeIDsMap
                                        .containsKey(Integer
                                                .toString(aDegree
                                                        .getDegreeId()))) {
                                    highestDegreesList.get(count).setIsSelected(
                                            true);
                                } else
                                    highestDegreesList.get(count).setIsSelected(
                                            false);

                                count++;
                            }
                        }
                    }
                    break;

                case ANNUAL_INCOME:

                    if (selectItems[0] != null && !(selectItems[0]).equals("")
                            && !(selectItems[0]).equals("null")) {
                        income_tv.setVisibility(View.GONE);
                        annual_incomes_container.setVisibility(View.VISIBLE);
                        removeAllDynamicItems(n);
                        allAnnualIncomesTextViews = null;
                        allAnnualIncomesTextViews = new ArrayList<>();
                        l = tiles.length;
                        for (int i = 0; i < l; i++)
                            displayNewItems(tiles[i], n);
                    } else {
                        income_tv.setVisibility(View.VISIBLE);
                        income_tv.setText("");
                        income_tv.setHint(R.string.select_annual_income);
                        annual_incomes_container.setVisibility(View.GONE);
                    }
                    if (!selectItems[1].equals("")) {
                        incomeIds = selectItems[1];
                        selectedIncomeIDsMap = null;
                        selectedIncomeIDsMap = editPrefAdapter
                                .getSelectedItemIDs(n);
                        count = 0;
                        for (EditPrefBasicDataModal aIncome : incomesList) {
                            if (selectedIncomeIDsMap != null
                                    && selectedIncomeIDsMap.containsKey(aIncome
                                    .getIDorCountryID()))
                                incomesList.get(count).setIsSelected(true);
                            else
                                incomesList.get(count).setIsSelected(false);
                            count++;
                        }
                    }
                    break;

                default:
                    break;
            }
        } else {
            tiles = null;
            tiles = new String[1];
            tiles[0] = getResources().getString(R.string.doesnt_matter);
            int count = 0;
            switch (n) {
                case COUNTRY:

                    countryIds = null;
                    selectedCountryIDsMap = null;

                    // state_dyn_lay.removeAllViews();
                    removeAllDynamicItems(STATE);
                    removeAllDynamicItems(CITY);
                    // city_dyn_lay.removeAllViews();
                    setCitySelectorEnabled(false);

                    stateIds = null;
                    cityIds = null;

                    count = 0;
                    if (countriesList != null && countriesList.size() > 0) {
                        for (EditPrefBasicDataModal aCountry : countriesList) {
                            if (aCountry.getIsSelected())
                                countriesList.get(count).setIsSelected(false);
                            count++;
                        }
                    }

                    count = 0;

                    if (statesList != null && statesList.size() > 0) {
                        for (EditPrefBasicDataModal s : statesList) {
                            if (s.getIsSelected()) {
                                statesList.get(count).setIsSelected(false);
                            }
                            count++;
                        }
                    }
                    count = 0;
                    if (citiesList != null && citiesList.size() > 0) {
                        for (EditPrefBasicDataModal c : citiesList) {
                            if (c.getIsSelected()) {
                                citiesList.get(count).setIsSelected(false);
                            }
                            count++;
                        }
                    }
                    count = 0;
                    if (citiesListNew != null && citiesListNew.size() > 0) {
                        for (EditPrefBasicDataModal c : citiesListNew) {
                            if (c.getIsSelected()) {
                                citiesListNew.get(count).setIsSelected(false);
                            }
                            count++;
                        }
                    }

                    if (other_row_cb_1.isChecked()) {
                        country_tv.setVisibility(View.GONE);
                        countries_container.setVisibility(View.VISIBLE);
                        removeAllDynamicItems(n);
                        allCountriesTextViews = null;
                        allCountriesTextViews = new ArrayList<>();
                        displayNewItems(tiles[0], n);
                    } else {
                        country_tv.setVisibility(View.VISIBLE);
                        country_tv.setText("");
                        country_tv.setHint(R.string.select_country);
                        countries_container.setVisibility(View.GONE);
                    }
                    break;

                case STATE:

                    stateIds = null;
                    removeAllDynamicItems(CITY);
                    // city_dyn_lay.removeAllViews();
                    setCitySelectorEnabled(false);

                    count = 0;

                    if (statesList != null && statesList.size() > 0) {
                        for (EditPrefBasicDataModal s : statesList) {
                            if (s.getIsSelected()) {
                                statesList.get(count).setIsSelected(false);
                            }
                            count++;
                        }
                    }
                    count = 0;
                    if (citiesList != null && citiesList.size() > 0) {
                        for (EditPrefBasicDataModal c : citiesList) {
                            if (c.getIsSelected()) {
                                citiesList.get(count).setIsSelected(false);
                            }
                            count++;
                        }
                    }
                    count = 0;
                    if (citiesListNew != null && citiesListNew.size() > 0) {
                        for (EditPrefBasicDataModal c : citiesListNew) {
                            if (c.getIsSelected()) {
                                citiesListNew.get(count).setIsSelected(false);
                            }
                            count++;
                        }
                    }

                    if (other_row_cb_1.isChecked()) {

                        states_container.setVisibility(View.VISIBLE);
                        state_tv.setVisibility(View.GONE);
                        removeAllDynamicItems(n);
                        allStatesTextViews = null;
                        allStatesTextViews = new ArrayList<>();
                        displayNewItems(tiles[0], n);
                    } else {
                        state_tv.setVisibility(View.VISIBLE);
                        state_tv.setText("");
                        state_tv.setHint(R.string.select_state);
                        states_container.setVisibility(View.GONE);
                    }
                    break;
                case CITY:

                    cityIds = null;

                    count = 0;
                    if (citiesList != null && citiesList.size() > 0) {
                        for (EditPrefBasicDataModal c : citiesList) {
                            if (c.getIsSelected()) {
                                citiesList.get(count).setIsSelected(false);
                            }
                            count++;
                        }
                    }
                    count = 0;
                    if (citiesListNew != null && citiesListNew.size() > 0) {
                        for (EditPrefBasicDataModal c : citiesListNew) {
                            if (c.getIsSelected()) {
                                citiesListNew.get(count).setIsSelected(false);
                            }
                            count++;
                        }
                    }

                    if (other_row_cb_1.isChecked()) {
                        cities_container.setVisibility(View.VISIBLE);
                        city_tv.setVisibility(View.GONE);
                        removeAllDynamicItems(n);
                        allCitiesTextViews = null;
                        allCitiesTextViews = new ArrayList<>();
                        displayNewItems(tiles[0], n);
                    } else {
                        city_tv.setVisibility(View.VISIBLE);
                        city_tv.setText("");
                        city_tv.setHint(R.string.select_city);
                        cities_container.setVisibility(View.GONE);
                    }
                    break;
                case HIGHEST_DEGREE:

                    highestDegreeIds = null;

                    count = 0;
                    if (highestDegreesList != null && highestDegreesList.size() > 0) {
                        for (EditPrefBasicDataModal d : highestDegreesList) {
                            if (d.getIsSelected()) {
                                highestDegreesList.get(count).setIsSelected(false);
                            }
                            count++;
                        }
                    }

                    if (other_row_cb_1.isChecked()) {
                        highest_degrees_container.setVisibility(View.VISIBLE);
                        highest_degree_tv.setVisibility(View.GONE);
                        removeAllDynamicItems(n);
                        allDegreesTextViews = null;
                        allDegreesTextViews = new ArrayList<>();
                        displayNewItems(tiles[0], n);

                    } else {
                        highest_degree_tv.setVisibility(View.VISIBLE);
                        highest_degree_tv.setText("");
                        highest_degree_tv.setHint(R.string.select_highest_degree);
                        highest_degrees_container.setVisibility(View.GONE);
                    }
                    break;

                case ANNUAL_INCOME:

                    count = 0;
                    if (incomesList != null && incomesList.size() > 0) {
                        for (EditPrefBasicDataModal income : incomesList) {
                            if (income.getIsSelected()) {
                                incomesList.get(count).setIsSelected(false);
                            }
                            count++;
                        }
                    }
                    incomeIds = null;
                    if (other_row_cb_1.isChecked()) {
                        income_tv.setVisibility(View.GONE);
                        annual_incomes_container.setVisibility(View.VISIBLE);
                        removeAllDynamicItems(n);
                        allAnnualIncomesTextViews = null;
                        allAnnualIncomesTextViews = new ArrayList<>();
                        displayNewItems(tiles[0], n);
                    } else {
                        income_tv.setVisibility(View.VISIBLE);
                        income_tv.setText("");
                        income_tv.setHint(R.string.select_annual_income);
                        annual_incomes_container.setVisibility(View.GONE);
                    }
                    break;

                default:
                    break;
            }
        }
    }

    private void setCitySelectorEnabled(boolean toEnable) {
        if (toEnable) {
            cities_container.setVisibility(View.VISIBLE);
            city_tv.setVisibility(View.GONE);
            city_lay.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    onCityClick();
                }
            });
        } else {
            cities_container.setVisibility(View.GONE);
            city_tv.setVisibility(View.VISIBLE);
            city_tv.setText("");
            city_tv.setHint(R.string.select_state_first);
            city_lay.setOnClickListener(null);
        }

    }

    private String getSelectedCities() {
        // selectedStateIDsMap

        String selectedCities = "";
        if (citiesListNew != null && citiesListNew.size() > 0) {
            boolean isFirstTime = true;
            for (EditPrefBasicDataModal aCity : citiesListNew) {
                if (aCity.getIsSelected()) {
                    if (isFirstTime) {
                        selectedCities += aCity.getName();
                        isFirstTime = false;
                    } else
                        selectedCities += "," + aCity.getName();
                }
            }
        }
        return selectedCities;
    }

    private void editPrefSavedCall() {

        EditPrefSaveModal aEditPrefSaveModal = new EditPrefSaveModal();

        aEditPrefSaveModal.lookingAgeStart = min_age_val + "";
        aEditPrefSaveModal.lookingAgeEnd = max_age_val + "";

        aEditPrefSaveModal.start_height = start_height;
        aEditPrefSaveModal.end_height = end_height;

        String marital_status_val = "";

        if (status_single.isChecked())
            marital_status_val = "Never Married";
        if (status_married_before.isChecked()) {
            if (!marital_status_val.equals(""))
                marital_status_val += "," + "Married Before";
            else
                marital_status_val += "Married Before";
        }
        if (status_any.isChecked()) {
            marital_status_val = "";
        }

        if (!Utility.isSet(marital_status_val))
            aEditPrefSaveModal.marital_status_spouse = null;
        else
            aEditPrefSaveModal.marital_status_spouse = marital_status_val;

        if (Utility.isSet(incomeIds))
            aEditPrefSaveModal.income_spouse_start = incomeIds;

		if (Utility.isSet(countryIds)) {
			aEditPrefSaveModal.spouseCountry = countryIds;
		} else {
			aEditPrefSaveModal.spouseCountry = String.valueOf(Constants.COUNTRY_ID_INDIA);
		}

        if (Utility.isSet(stateIds) && statesList != null
                && statesList.size() > 0) {
            String selectedStates = "";
            boolean isFirstTime = true;
            for (EditPrefBasicDataModal aState : statesList) {
                if (aState.getIsSelected()) {
                    if (isFirstTime) {
                        selectedStates += aState.getIDorCountryID() + "-"
                                + aState.getStateId();
                        isFirstTime = false;
                    } else
                        selectedStates += "," + aState.getIDorCountryID() + "-"
                                + aState.getStateId();
                }
            }
            if (Utility.isSet(selectedStates))
                aEditPrefSaveModal.spouseState = selectedStates;
        }

        if (Utility.isSet(cityIds)) {
            boolean isFirstTime = true;
            String selectedCities = "";
            if (citiesListNew != null && citiesListNew.size() > 0) {
                for (EditPrefBasicDataModal aCity : citiesListNew) {
                    if (aCity.getIsSelected()) {
                        if (isFirstTime) {
                            selectedCities += aCity.getIDorCountryID() + "-"
                                    + aCity.getStateId() + "-"
                                    + aCity.getCityId();
                            isFirstTime = false;
                        } else
                            selectedCities += "," + aCity.getIDorCountryID()
                                    + "-" + aCity.getStateId() + "-"
                                    + aCity.getCityId();
                    }
                }
            } else {
                if (citiesList != null && citiesList.size() > 0) {
                    for (EditPrefBasicDataModal aCity : citiesList) {
                        if (aCity.getIsSelected()) {
                            if (isFirstTime) {
                                selectedCities += aCity.getIDorCountryID()
                                        + "-" + aCity.getStateId() + "-"
                                        + aCity.getCityId();
                                isFirstTime = false;
                            } else
                                selectedCities += ","
                                        + aCity.getIDorCountryID() + "-"
                                        + aCity.getStateId() + "-"
                                        + aCity.getCityId();
                        }
                    }
                }
            }

            if (Utility.isSet(selectedCities))
                aEditPrefSaveModal.spouseCity = selectedCities;
            else
                aEditPrefSaveModal.spouseCity = null;
        }
        if (Utility.isSet(highestDegreeIds))
            aEditPrefSaveModal.education_spouse = highestDegreeIds;

        if (Utility.isSet(incomeIds))
            aEditPrefSaveModal.income = incomeIds;

        aEditPrefSaveModal.show_us_profiles = show_us_profiles;

        issueRequest(Constants.EDIT_PREFERENCE_SAVE, ConstantsUrls.get_editpreference_url(), aEditPrefSaveModal);
        //Utility.saveEditPreferencesData(getApplicationContext(), aEditPrefSaveModal);
    }

    @OnClick(R.id.country_lay)
    void onCountryClick() {
        if (selectedItem != COUNTRY)
            other_row_cb_1.setChecked(false);
        selectedItem = COUNTRY;
        initializeSideMenu(COUNTRY);
        toggleMenu();
    }

    @OnClick(R.id.state_lay)
    void onStateClick() {
        if (selectedItem != STATE)
            other_row_cb_1.setChecked(false);
        selectedItem = STATE;
        initializeSideMenu(STATE);
        toggleMenu();
    }

    void onCityClick() {
        if (selectedItem != CITY)
            other_row_cb_1.setChecked(false);
        selectedItem = CITY;
        initializeSideMenu(CITY);
        toggleMenu();
    }

    @OnClick(R.id.highest_degree_lay)
    void onHighestDegreeClick() {
        if (selectedItem != HIGHEST_DEGREE)
            other_row_cb_1.setChecked(false);
        selectedItem = HIGHEST_DEGREE;
        if (other_row_cb_2.isChecked())
            selectOtherOption(selectedItem, 1);
        if (other_row_cb_3.isChecked())
            selectOtherOption(selectedItem, 2);
        initializeSideMenu(HIGHEST_DEGREE);
        toggleMenu();
    }

    @OnClick(R.id.annual_income_lay)
    void onAnnualIncomeClick() {
        if (selectedItem != ANNUAL_INCOME)
            other_row_cb_1.setChecked(false);
        selectedItem = ANNUAL_INCOME;
        initializeSideMenu(ANNUAL_INCOME);
        toggleMenu();
    }

    @OnClick(R.id.other_lay_1)
    void onOtherLay1Click() {
        other_row_cb_2.setChecked(false);
        other_row_cb_3.setChecked(false);
        if (!other_row_cb_1.isChecked()) {
            other_row_cb_1.setChecked(true);
            disableEditPrefArrList();
        } else
            other_row_cb_1.setChecked(false);
    }

    @OnClick(R.id.other_lay_2)
    void onOtherLay2Click() {
        other_row_cb_1.setChecked(false);
        if (!other_row_cb_2.isChecked()) {
            other_row_cb_2.setChecked(true);
            selectOtherOption(selectedItem, 1);
        } else {
            diselectBachelors(1);
            other_row_cb_2.setChecked(false);
        }
    }

    @OnClick(R.id.other_lay_3)
    void onOtherLay3Click() {
        other_row_cb_1.setChecked(false);
        if (!other_row_cb_3.isChecked()) {
            other_row_cb_3.setChecked(true);
            selectOtherOption(selectedItem, 2);
        } else {
            diselectBachelors(2);
            other_row_cb_3.setChecked(false);
        }
    }

    @OnClick(R.id.status_single)
    void onStatusSingleClick() {
        setStatusCheckboxes(status_single, status_married_before);
    }

    @OnClick(R.id.status_married_before)
    void onStatusMarriedBeforeClick() {
        setStatusCheckboxes(status_married_before, status_single);
    }

    @OnClick(R.id.status_any)
    void onStatusAnyClick() {
        setAllChecked();
    }

    @OnClick(R.id.cancel_btn)
    void onCancelButtonClick() {
        other_row_cb_1.setChecked(false);
        toggleMenu();
    }

    @OnClick(R.id.save_btn)
    void onSaveButtonClick() {
        setDataModified();
        if (other_row_cb_1.isChecked())
            disableSelectedItemList(selectedItem);
        setSelectedItemText(selectedItem);
        toggleMenu();
    }

    @OnClick(R.id.continue_btn)
    void onContinueClick() {
        if (Utility.isNetworkAvailable(aContext)) {
            if (isDataModified) {
                if (tapToRetryHandler != null)
                    tapToRetryHandler.showLoader();
                editPrefSavedCall();
                // clear the cache of matches
                MatchesDbHandler.clearMatchesCache(aContext);
            }
        } else
            AlertsHandler.showMessage(aActivity, R.string.ERROR_NETWORK_FAILURE);
    }

    @OnCheckedChanged(R.id.us_profile_edit_pref_switch)
    void onUsProfileEditCheckChange(boolean checked) {
        setDataModified();
        show_us_profiles = checked;
    }


    private void setDataModified() {
        isDataModified = true;
        continue_btn.setEnabled(true);
    }

    private void setStatusCheckboxes(CheckBox selected_elm, CheckBox other_elm) {
        setDataModified();
        if (selected_elm.isChecked()) {
            selected_elm.setTextColor(getResources().getColor(R.color.white));
            other_elm.setChecked(false);
            other_elm.setTextColor(getResources().getColor(R.color.black));
            status_any.setChecked(false);
            status_any.setTextColor(getResources().getColor(R.color.black));
        } else {
            selected_elm.setChecked(true);
        }
    }

    private void setAllChecked() {
        setDataModified();
        status_any.setChecked(true);
        status_any.setTextColor(getResources().getColor(R.color.white));
        status_single.setChecked(false);
        status_single.setTextColor(getResources().getColor(R.color.black));
        status_married_before.setChecked(false);
        status_married_before.setTextColor(getResources().getColor(
                R.color.black));
    }

    private void diselectBachelors(int n) {
        int count;
        if (n == 1) {
            count = 0;
            for (EditPrefModel ep : editPrefArrayList) {
                if (EditPrefAdapter.isIdBetweenLimits(ep.getID(), bachelorIds)) {
                    editPrefArrayList.get(count).setSelected(false);
                }
                count++;
            }
        }

        if (n == 2) {
            count = 0;
            for (EditPrefModel ep : editPrefArrayList) {
                if (EditPrefAdapter.isIdBetweenLimits(ep.getID(), masterIds)) {
                    editPrefArrayList.get(count).setSelected(false);
                }
                count++;
            }
        }
        editPrefAdapter.notifyDataSetChanged();
    }

    private void selectOtherOption(int selectedItem, int n) { // n is for any
        // bachelors(1)
        // and any
        // masters(2)
        int count = 0;
        if (selectedItem == HIGHEST_DEGREE) {
            if (n == 1) {
                count = 0;
                for (EditPrefModel ep : editPrefArrayList) {
                    if (EditPrefAdapter.isIdBetweenLimits(ep.getID(),
                            bachelorIds)) {
                        editPrefArrayList.get(count).setSelected(true);
                    }
                    count++;
                }
            } else if (n == 2) {
                count = 0;
                for (EditPrefModel ep : editPrefArrayList) {
                    if (EditPrefAdapter
                            .isIdBetweenLimits(ep.getID(), masterIds)) {
                        editPrefArrayList.get(count).setSelected(true);
                    }
                    count++;
                }
            } else {
                count = 0;
                if (highestDegreesList != null) {
                    for (EditPrefBasicDataModal d : highestDegreesList) {
                        if (d.getIsSelected()) {
                            highestDegreesList.get(count).setIsSelected(false);
                        }
                        count++;
                    }
                }
                highestDegreeIds = null;
                selectedHighestDegreeIDsMap = null;

                count = 0;
                for (EditPrefModel ep : editPrefArrayList) {
                    if (checkNotNull(ep).isSelected())
                        editPrefArrayList.get(count).setSelected(false);
                    count++;
                }
            }
        } else {
            count = 0;
            for (EditPrefModel ep : editPrefArrayList) {
                if (checkNotNull(ep).isSelected())
                    editPrefArrayList.get(count).setSelected(false);
                count++;
            }
        }

        switch (selectedItem) {
            case COUNTRY:
                count = 0;
                for (EditPrefBasicDataModal c : countriesList) {
                    if (c.getIsSelected())
                        countriesList.get(count).setIsSelected(false);
                    count++;
                }
                countryIds = null;
                selectedCountryIDsMap = null;

                count = 0;
                for (EditPrefBasicDataModal s : statesList) {
                    if (s.getIsSelected())
                        statesList.get(count).setIsSelected(false);
                    count++;
                }
                stateIds = null;
                selectedStateIDsMap = null;

                count = 0;
                for (EditPrefBasicDataModal city : citiesList) {
                    if (city.getIsSelected())
                        citiesList.get(count).setIsSelected(false);
                    count++;
                }
                cityIds = null;
                selectedCitiesIDsMap = null;
                citiesListNew = null;

                state_tv.setVisibility(View.GONE);
                city_tv.setVisibility(View.GONE);
                break;

            case STATE:
                count = 0;
                for (EditPrefBasicDataModal s : statesList) {
                    if (s.getIsSelected())
                        statesList.get(count).setIsSelected(false);
                    count++;
                }
                stateIds = null;
                selectedStateIDsMap = null;

                count = 0;
                for (EditPrefBasicDataModal city : citiesList) {
                    if (city.getIsSelected())
                        citiesList.get(count).setIsSelected(false);
                    count++;
                }
                cityIds = null;
                selectedCitiesIDsMap = null;
                citiesListNew = null;

                city_tv.setVisibility(View.GONE);

                break;

            case CITY:

                count = 0;
                for (EditPrefBasicDataModal city : citiesList) {
                    if (city.getIsSelected())
                        citiesList.get(count).setIsSelected(false);
                    count++;
                }
                cityIds = null;
                selectedCitiesIDsMap = null;
                citiesListNew = null;
                break;

            case HIGHEST_DEGREE:

                // count =0;
                // selectedHighestDegreeIDsMap=null;
                if (n == 1) {
                    count = 0;
                    if (highestDegreesList != null) {
                        for (EditPrefBasicDataModal d : highestDegreesList) {
                            if (EditPrefAdapter.isIdBetweenLimits(d.getDegreeId(),
                                    bachelorIds)) {
                                highestDegreesList.get(count).setIsSelected(true);
                            }
                            count++;
                        }
                    }
                } else if (n == 2) {
                    count = 0;
                    if (highestDegreesList != null) {
                        for (EditPrefBasicDataModal d : highestDegreesList) {
                            if (EditPrefAdapter.isIdBetweenLimits(d.getDegreeId(),
                                    masterIds)) {
                                highestDegreesList.get(count).setIsSelected(true);
                            }
                            count++;
                        }
                    }
                }
                break;

            case ANNUAL_INCOME:
                count = 0;
                for (EditPrefBasicDataModal income : incomesList) {
                    if (income.getIsSelected())
                        incomesList.get(count).setIsSelected(false);
                    count++;
                }
                incomeIds = null;
                selectedIncomeIDsMap = null;
                break;
            default:
                break;
        }
        editPrefAdapter.notifyDataSetChanged();
    }

    private void disableSelectedItemList(int selectedItem) {

        int count = 0;
        switch (selectedItem) {
            case COUNTRY:
                count = 0;
                if (countriesList != null && countriesList.size() > 0) {
                    for (EditPrefBasicDataModal c : countriesList) {
                        if (c.getIsSelected())
                            countriesList.get(count).setIsSelected(false);
                        count++;
                    }
                }
                countryIds = null;
                selectedCountryIDsMap = null;

                count = 0;
                if (statesList != null && statesList.size() > 0) {
                    for (EditPrefBasicDataModal s : statesList) {
                        if (s.getIsSelected())
                            statesList.get(count).setIsSelected(false);
                        count++;
                    }
                }
                stateIds = null;
                selectedStateIDsMap = null;

                count = 0;
                if (citiesList != null && citiesList.size() > 0) {
                    for (EditPrefBasicDataModal city : citiesList) {
                        if (city.getIsSelected())
                            citiesList.get(count).setIsSelected(false);
                        count++;
                    }
                }
                cityIds = null;
                selectedCitiesIDsMap = null;
                citiesListNew = null;

                state_tv.setVisibility(View.GONE);
                city_tv.setVisibility(View.GONE);
                break;

            case STATE:
                count = 0;

                if (statesList != null && statesList.size() > 0) {
                    for (EditPrefBasicDataModal s : statesList) {
                        if (s.getIsSelected())
                            statesList.get(count).setIsSelected(false);
                        count++;
                    }
                }
                stateIds = null;
                selectedStateIDsMap = null;

                count = 0;
                if (citiesList != null && citiesList.size() > 0) {
                    for (EditPrefBasicDataModal city : citiesList) {
                        if (city.getIsSelected())
                            citiesList.get(count).setIsSelected(false);
                        count++;
                    }
                }
                cityIds = null;
                selectedCitiesIDsMap = null;
                citiesListNew = null;

                city_tv.setVisibility(View.GONE);

                break;

            case CITY:
                count = 0;
                if (citiesList != null && citiesList.size() > 0) {
                    for (EditPrefBasicDataModal city : citiesList) {
                        if (city.getIsSelected())
                            citiesList.get(count).setIsSelected(false);
                        count++;
                    }
                }
                cityIds = null;
                selectedCitiesIDsMap = null;
                citiesListNew = null;
                break;

            case HIGHEST_DEGREE:
                count = 0;
                if (highestDegreesList != null && highestDegreesList.size() > 0) {
                    for (EditPrefBasicDataModal d : highestDegreesList) {
                        if (d.getIsSelected()) {
                            highestDegreesList.get(count).setIsSelected(false);
                        }
                        count++;
                    }
                }
                highestDegreeIds = null;
                selectedHighestDegreeIDsMap = null;
                break;

            case ANNUAL_INCOME:
                count = 0;
                if (incomesList != null && incomesList.size() > 0) {
                    for (EditPrefBasicDataModal income : incomesList) {
                        if (income.getIsSelected())
                            incomesList.get(count).setIsSelected(false);
                        count++;
                    }
                }
                incomeIds = null;
                selectedIncomeIDsMap = null;
                break;
            default:
                break;
        }
    }

    private void disableEditPrefArrList() { // n is for any bachelors(1) and any
        // masters(2)
        int count = 0;
        if (editPrefArrayList != null && editPrefArrayList.size() > 0) {
            for (EditPrefModel ep : editPrefArrayList) {
                if (checkNotNull(ep).isSelected())
                    editPrefArrayList.get(count).setSelected(false);
                count++;
            }
            editPrefAdapter.notifyDataSetChanged();
        }
    }

    private void responseParser(JSONObject response, int n, String urlCalled) {
        if (n == Constants.EDIT_PREFERENCE_PREFILL) {

            // GAEventTracker.sendTrackerToGA(context,
            // TrulyMadlyActivities.edit_preference,
            // TrulyMadlyEventTypes.page_load, Constants.GA_LABEL, 0,null);

            aEditPreferencePrefillModelNew = null;
            aEditPreferencePrefillModelNew = EditPreferencePrefillNewResponseParser
                    .parseEditPreferencePrefillResponse(response.toString()
                    );
            if (aEditPreferencePrefillModelNew != null) {
                us_profile_edit_pref_container.setVisibility(aEditPreferencePrefillModelNew.isUs_profiles_enabled() ? View.VISIBLE : View.GONE);
                show_us_profiles = aEditPreferencePrefillModelNew.isShow_us_profiles();
                us_profile_edit_pref_switch.setChecked(show_us_profiles);
                String url = EditPrefDBHandler.checkBasicDataExists(
                        aEditPreferencePrefillModelNew.getBasicData(), aContext);
                if (!Utility.isSet(url)) {
                    initializePreFillData(aEditPreferencePrefillModelNew);
                } else {
                    issueRequest(Constants.EDIT_PREFERENCE_PREFILL_BASIC_DATA,
                            url, null);
                }
            }
        } else if (n == Constants.EDIT_PREFERENCE_SAVE) {

            CachingDBHandler.deleteURL(aContext, prefillUrl, Utility.getMyId(aContext));
            CachingDBHandler.deleteURL(aContext, ConstantsUrls.get_my_profile_url(), Utility.getMyId(aContext));

            if (EditPreferenceSaveResponseParser
                    .parseEditPreferenceSaveResponse(response.toString()
                    )) {
                AlertsHandler.showMessage(aActivity, R.string.saved);
                if (startMatchesActivityRunnable == null) {
                    startMatchesActivityRunnable = new Runnable() {
                        @Override
                        public void run() {
                            ActivityHandler.startMatchesActivity(aContext);
                            finish();
                        }
                    };
                }
                mHandler.postDelayed(startMatchesActivityRunnable, 2000);
            }
        } else if (n == Constants.EDIT_PREFERENCE_PREFILL_BASIC_DATA) {
            EditPrefDBHandler.insertBasicDataInDB(response, urlCalled, aContext);
            initializePreFillData(aEditPreferencePrefillModelNew);
        }
    }

    private void initializeEditPrefBasicData() {
        bachelorIds = SPHandler.getString(aContext,
                ConstantsSP.SHARED_KEYS_BACHELORS);
        masterIds = SPHandler.getString(aContext,
                ConstantsSP.SHARED_KEYS_MASTERS);
        countriesList = EditPrefDBHandler.getBasicDataFromDB(Constants.EDIT_PREF_COUNTRY,
                aContext, 0);
        statesList = EditPrefDBHandler.getBasicDataFromDB(Constants.EDIT_PREF_STATE,
                aContext, 1);
        citiesList = EditPrefDBHandler.getBasicDataFromDB(Constants.EDIT_PREF_CITY,
                aContext, 2);
        incomesList = EditPrefDBHandler.getBasicDataFromDB(Constants.EDIT_PREF_INCOME,
                aContext, 0);
        highestDegreesList = EditPrefDBHandler.getBasicDataFromDB(
                Constants.EDIT_PREF_DEGREE, aContext, 3);
    }

    private void issueRequest(final int n, final String url, Object aEditPrefSaveModal) {
        String eventType = null;

        if (n == Constants.EDIT_PREFERENCE_PREFILL) {

            boolean showFailure = CachedDataHandler.showDataFromDb(aContext, url, callback);
            CachedDataHandler cachedDataHandler = new CachedDataHandler(url, callback, this, TrulyMadlyActivities.edit_preference, eventType, null, showFailure);
            Map<String, String> map = GetOkHttpRequestParams
                    .getHttpRequestParams(HttpRequestType.EDIT_PREF);
            map.put("hash", CachingDBHandler.getHashValue(aContext, url, Utility.getMyId(aContext)));
            cachedDataHandler.httpPost(aContext, url, map);
            prefillUrl = url;


        } else {
            eventType = n == Constants.EDIT_PREFERENCE_SAVE ? TrulyMadlyEventTypes.save_call : TrulyMadlyEventTypes.page_load;
            CustomOkHttpResponseHandler okHttpResponseHandler = new CustomOkHttpResponseHandler(
                    aContext, TrulyMadlyActivities.edit_preference, eventType) {

                @Override
                public void onRequestSuccess(JSONObject response) {
                    if (tapToRetryHandler != null)
                        tapToRetryHandler.onSuccessFull();
                    responseParser(response, n, url);
                }

                @Override
                public void onRequestFailure(Exception exception) {
                    tapToRetryHandler.onNetWorkFailed(exception);
                }
            };
            if (n == Constants.EDIT_PREFERENCE_SAVE) {
                tapToRetryHandler.showLoaderWithCustomText("Saving...");
                OkHttpHandler
                        .httpPost(aContext, url, GetOkHttpRequestParams
                                        .getHttpRequestParams(
                                                HttpRequestType.EDIT_PREF_SAVE,
                                                aEditPrefSaveModal),
                                okHttpResponseHandler);
            } else {
                tapToRetryHandler.showLoaderWithCustomText(null);
                OkHttpHandler.httpGet(aContext, url, okHttpResponseHandler);

            }
        }

    }

    private void initializePreFillData(
            EditPreferencePrefillModelNew aEditPreferencePrefillModelNew) {

        initializeEditPrefBasicData();
        if (aEditPreferencePrefillModelNew != null) {

            if (Utility.isSet(aEditPreferencePrefillModelNew.getStartAge())
                    && !(aEditPreferencePrefillModelNew.getStartAge())
                    .equals("0")) {
                min_age_val = Integer.parseInt(aEditPreferencePrefillModelNew
                        .getStartAge());
                age_min.setText(aEditPreferencePrefillModelNew.getStartAge());
                ageSeekBar
                        .setSelectedMinValue(Integer
                                .parseInt(aEditPreferencePrefillModelNew
                                        .getStartAge()));
            }
            if (Utility.isSet(aEditPreferencePrefillModelNew.getEndAge())
                    && !(aEditPreferencePrefillModelNew.getEndAge())
                    .equals("0")) {
                max_age_val = Integer.parseInt(aEditPreferencePrefillModelNew
                        .getEndAge());
                age_max.setText(aEditPreferencePrefillModelNew.getEndAge());
                ageSeekBar.setSelectedMaxValue(Integer
                        .parseInt(aEditPreferencePrefillModelNew.getEndAge()));
            }
            if (Utility.isSet(aEditPreferencePrefillModelNew.getStartHeight())
                    && !(aEditPreferencePrefillModelNew.getStartHeight())
                    .equals("0")) {

                start_height = aEditPreferencePrefillModelNew.getStartHeight();
                int startHeightInch = Integer
                        .parseInt(aEditPreferencePrefillModelNew
                                .getStartHeight());

                if (startHeightInch != 0) {
                    min_height_val = startHeightInch;
                    int feet = startHeightInch / 12;
                    int inch = startHeightInch % 12;
                    // String startH = feet + "." + inch;
                    // double startHeightFt = Double.parseDouble(startH);
                    height_min.setText(feet + "\'" + inch + "\"");
                    if (heightSeekBar != null)
                        heightSeekBar.setSelectedMinValue(startHeightInch);
                }
            }
            if (Utility.isSet(aEditPreferencePrefillModelNew.getEndHeight())
                    && !(aEditPreferencePrefillModelNew.getEndHeight())
                    .equals("0")) {
                end_height = aEditPreferencePrefillModelNew.getEndHeight();
                int endHeightInch = Integer
                        .parseInt(aEditPreferencePrefillModelNew.getEndHeight());
                if (endHeightInch != 0) {
                    max_height_val = endHeightInch;

                    int feet = endHeightInch / 12;
                    int inch = endHeightInch % 12;
                    // String endH = feet + "." + inch;
                    // double endHeightInchFt = Double.parseDouble(endH);
                    height_max.setText(feet + "\'" + inch + "\"");
                    if (heightSeekBar != null)
                        heightSeekBar.setSelectedMaxValue(endHeightInch);
                }
            }
            // prefill marital status
            if (Utility
                    .isSet(aEditPreferencePrefillModelNew.getMaritalStatus())) {
                String marital_status_val = aEditPreferencePrefillModelNew
                        .getMaritalStatus();
                if (marital_status_val.equalsIgnoreCase("Never Married"))
                    status_single.setChecked(true);
                else if (marital_status_val.equalsIgnoreCase("Married Before"))
                    status_married_before.setChecked(true);
                else
                    status_any.setChecked(true);
            } else {
                status_any.setChecked(true);
            }

            if (Utility.isSet(aEditPreferencePrefillModelNew.getIncomeIds())) {
                incomeIds = null;
                incomeIds = aEditPreferencePrefillModelNew.getIncomeIds();

                String[] incomeIdsArr = incomeIds.split(",");

                String incomeNames = "";

                int len = incomeIdsArr.length;
                int count;
                if (incomesList != null && incomesList.size() > 0) {
                    for (int i = 0; i < len; i++) {
                        count = 0;
                        for (EditPrefBasicDataModal income : incomesList) {
                            if ((income.getIDorCountryID())
                                    .equals(incomeIdsArr[i])) {
                                incomesList.get(count).setIsSelected(true);
                                if (i != len - 1)
                                    incomeNames += income.getName() + ",";
                                else
                                    incomeNames += income.getName();
                                break;
                            }
                            count++;
                        }
                    }
                }

                if (Utility.isSet(incomeNames)) {
                    tiles = null;
                    tiles = incomeNames.split(",");
                    income_tv.setVisibility(View.GONE);
                    annual_incomes_container.setVisibility(View.VISIBLE);
                    removeAllDynamicItems(ANNUAL_INCOME);
                    allAnnualIncomesTextViews = null;
                    allAnnualIncomesTextViews = new ArrayList<>();
                    int l = tiles.length;
                    for (String tile : tiles) displayNewItems(tile, ANNUAL_INCOME);
                }
                // else
                // displayNewItems(doesNotMatter, ANNUAL_INCOME);
            } else {
                income_tv.setVisibility(View.GONE);
                annual_incomes_container.setVisibility(View.VISIBLE);
                removeAllDynamicItems(ANNUAL_INCOME);
                allAnnualIncomesTextViews = null;
                allAnnualIncomesTextViews = new ArrayList<>();
                displayNewItems(getResources().getString(R.string.doesnt_matter), ANNUAL_INCOME);
            }

            // prefill countries
            if (Utility.isSet(aEditPreferencePrefillModelNew.getCountries())) {

                countryIds = null;
                countryIds = aEditPreferencePrefillModelNew.getCountries();

                String[] countryIdsArr = (aEditPreferencePrefillModelNew
                        .getCountries()).split(",");
                int len = countryIdsArr.length;
                String countryNames = "";

                int count = 0;

                if (countriesList != null && countriesList.size() > 0) {
                    for (int i = 0; i < len; i++) {
                        count = 0;
                        for (EditPrefBasicDataModal aCountry : countriesList) {
                            if (aCountry.getIDorCountryID().equals(
                                    countryIdsArr[i])) {
                                countriesList.get(count).setIsSelected(true);
                                if (i != len - 1)
                                    countryNames += aCountry.getName() + ",";
                                else
                                    countryNames += aCountry.getName();

                                break;
                            }
                            count++;
                        }
                    }
                }

                if (Utility.isSet(countryNames)) {
                    tiles = null;
                    tiles = countryNames.split(",");
                    country_tv.setVisibility(View.GONE);

                    countries_container.setVisibility(View.VISIBLE);
                    removeAllDynamicItems(COUNTRY);
                    allCountriesTextViews = null;
                    allCountriesTextViews = new ArrayList<>();
                    int l = tiles.length;
                    for (String tile : tiles) displayNewItems(tile, COUNTRY);
                }
                // else
                // displayNewItems(doesNotMatter, COUNTRY);

            } else {
                country_tv.setVisibility(View.GONE);
                countries_container.setVisibility(View.VISIBLE);
                removeAllDynamicItems(COUNTRY);
                allCountriesTextViews = null;
                allCountriesTextViews = new ArrayList<>();
                displayNewItems(getResources().getString(R.string.doesnt_matter), COUNTRY);

            }
            // end of prefill countries

            // prefill cities
            if (Utility.isSet(aEditPreferencePrefillModelNew.getCities())) {

                cityIds = null;
                cityIds = aEditPreferencePrefillModelNew.getCities();

                String[] cityIdsArr = (aEditPreferencePrefillModelNew
                        .getCities()).split(",");
                int len = cityIdsArr.length;
                String cityNames = "";

                int count;
                if (citiesList != null && citiesList.size() > 0) {
                    for (int i = 0; i < len; i++) {
                        count = 0;
                        for (EditPrefBasicDataModal aCity : citiesList) {
                            String[] countryStateCityId = cityIdsArr[i].split("-"); // 113-2169-16871
                            if (aCity.getIDorCountryID().equals(
                                    countryStateCityId[0])
                                    && aCity.getStateId().equals(
                                    countryStateCityId[1])
                                    && aCity.getCityId().equals(
                                    countryStateCityId[2])) {

                                citiesList.get(count).setIsSelected(true);
                                if (i != len - 1)
                                    cityNames += aCity.getName() + ",";
                                else
                                    cityNames += aCity.getName();
                                break;
                            }
                            count++;
                        }
                    }
                }

                if (Utility.isSet(cityNames)) {
                    tiles = null;
                    tiles = cityNames.split(",");
                    city_tv.setVisibility(View.GONE);
                    cities_container.setVisibility(View.VISIBLE);
                    removeAllDynamicItems(CITY);
                    allCitiesTextViews = null;
                    allCitiesTextViews = new ArrayList<>();

                    int l = tiles.length;
                    for (String tile : tiles) displayNewItems(tile, CITY);
                }
                // else
                // displayNewItems(doesNotMatter, CITY);
            } else {
                city_tv.setVisibility(View.GONE);
                cities_container.setVisibility(View.VISIBLE);
                removeAllDynamicItems(CITY);
                allCitiesTextViews = null;
                allCitiesTextViews = new ArrayList<>();
                displayNewItems(getResources().getString(R.string.doesnt_matter), CITY);
            }

            // prefill states
            if (Utility.isSet(aEditPreferencePrefillModelNew.getStates())) {
                stateIds = null;
                stateIds = aEditPreferencePrefillModelNew.getStates();
                String[] stateIdsArr = (aEditPreferencePrefillModelNew
                        .getStates()).split(","); // 113-2174,113-2169
                int len = stateIdsArr.length;

                String stateNames = "";
                int count;
                if (statesList != null && statesList.size() > 0) {
                    for (int i = 0; i < len; i++) {
                        count = 0;
                        for (EditPrefBasicDataModal aState : statesList) {
                            String[] countryStataId = stateIdsArr[i].split("-");

                            if (aState.getIDorCountryID().equals(
                                    countryStataId[0])
                                    && aState.getStateId().equals(
                                    countryStataId[1])) {

                                statesList.get(count).setIsSelected(true);
                                if (i != len - 1)
                                    stateNames += aState.getName() + ",";
                                else
                                    stateNames += aState.getName();

                                break;
                            }
                            count++;
                        }
                    }
                }

                if (Utility.isSet(stateNames)) {
                    tiles = null;
                    tiles = stateNames.split(",");
                    state_tv.setVisibility(View.GONE);

                    states_container.setVisibility(View.VISIBLE);
                    removeAllDynamicItems(STATE);
                    allStatesTextViews = null;
                    allStatesTextViews = new ArrayList<>();
                    int l = tiles.length;
                    for (String tile : tiles) displayNewItems(tile, STATE);

                    setCitySelectorEnabled(true);
                } else {
                    setCitySelectorEnabled(false);
                }
                // else
                // displayNewItems(doesNotMatter, STATE);
                // getCities(aEditPreferencePrefillModel.getCountries(),
                // aEditPreferencePrefillModel.getStates());
            } else {
                state_tv.setVisibility(View.GONE);
                states_container.setVisibility(View.VISIBLE);
                removeAllDynamicItems(STATE);
                allStatesTextViews = null;
                allStatesTextViews = new ArrayList<>();
                displayNewItems(getResources().getString(R.string.doesnt_matter), STATE);
                setCitySelectorEnabled(false);
            }
            // end of prefill states

            // prefill highest degree

            if (Utility.isSet(aEditPreferencePrefillModelNew.getEducation())) {
                highestDegreeIds = null;
                highestDegreeIds = aEditPreferencePrefillModelNew
                        .getEducation();

                String educationNames = "";
                if (highestDegreesList != null && highestDegreesList.size() > 0) {
                    String[] educationIdsArr = (aEditPreferencePrefillModelNew
                            .getEducation()).split(",");
                    int len = educationIdsArr.length;

                    int count;
                    for (int i = 0; i < len; i++) {
                        count = 0;
                        for (EditPrefBasicDataModal aDegree : highestDegreesList) {
                            if (Utility.isSet(educationIdsArr[i])
                                    && aDegree.getDegreeId() == Integer
                                    .parseInt(educationIdsArr[i])) {
                                highestDegreesList.get(count).setIsSelected(
                                        true);

                                if (i != len - 1)
                                    educationNames += aDegree.getName() + ",";
                                else
                                    educationNames += aDegree.getName();
                                break;
                            }
                            count++;
                        }
                    }

                    if (Utility.isSet(educationNames)) {

                        tiles = null;
                        tiles = educationNames.split(",");
                        highest_degree_tv.setVisibility(View.GONE);

                        highest_degrees_container.setVisibility(View.VISIBLE);
                        removeAllDynamicItems(HIGHEST_DEGREE);

                        allDegreesTextViews = null;
                        allDegreesTextViews = new ArrayList<>();

                        int l = tiles.length;
                        for (String tile : tiles) displayNewItems(tile, HIGHEST_DEGREE);
                    }
                    // else
                    // displayNewItems(doesNotMatter, HIGHEST_DEGREE);
                }
            } else {
                highest_degree_tv.setVisibility(View.GONE);
                highest_degrees_container.setVisibility(View.VISIBLE);
                removeAllDynamicItems(HIGHEST_DEGREE);
                allDegreesTextViews = null;
                allDegreesTextViews = new ArrayList<>();
                displayNewItems(getResources().getString(R.string.doesnt_matter), HIGHEST_DEGREE);
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        if (mHandler != null) {
            mHandler.removeCallbacks(startMatchesActivityRunnable);
        }
        super.onDestroy();
    }
}
