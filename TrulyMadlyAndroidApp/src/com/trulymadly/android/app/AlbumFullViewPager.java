package com.trulymadly.android.app;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.moe.pushlibrary.MoEHelper;
import com.squareup.otto.Subscribe;
import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.app.adapter.ImagePagerAdapter;
import com.trulymadly.android.app.bus.BusProvider;
import com.trulymadly.android.app.bus.NetworkChangeEvent;
import com.trulymadly.android.app.listener.OnActionBarClickedInterface;
import com.trulymadly.android.app.modal.ProfileViewPagerModal;
import com.trulymadly.android.app.modal.VideoModal;
import com.trulymadly.android.app.utility.ActionBarHandler;
import com.trulymadly.android.app.utility.CirclePageIndicator2;
import com.trulymadly.android.app.utility.Utility;

import java.util.ArrayList;
import java.util.HashMap;


public class AlbumFullViewPager extends AppCompatActivity {

    public static String[] pics;
    public static VideoModal[] videoModals;

    private int pos = 0;
    private MoEHelper mhelper = null;
    private ImagePagerAdapter mImagePagerAdapter;
    private String mTrkActivity, mMatchId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.album_fullview_pager);
        } catch (OutOfMemoryError e) {
            System.gc();
            finish();
            return;
        }
        Utility.disableScreenShot(this);
        mhelper = new MoEHelper(this);
        String title = getIntent().getStringExtra("title");
        mTrkActivity = getIntent().getStringExtra("trkActivity");
        mMatchId = getIntent().getStringExtra("user_id");
        Activity aActivity = this;

        Utility.prefetchPhotos(aActivity, pics, 0, TrulyMadlyEvent.TrulyMadlyActivities.matches);

        OnActionBarClickedInterface onActionBarClickedInterface = new OnActionBarClickedInterface() {

            @Override
            public void onConversationsClicked() {
            }

            @Override
            public void onLocationClicked() {

            }

            @Override
            public void onCuratedDealsClicked() {

            }

            @Override
            public void onBackClicked() {
                try {
                    onBackPressed();
                } catch (IllegalStateException | NullPointerException ignored) {

                }
            }

            @Override
            public void onUserProfileClicked() {
            }

            @Override
            public void onTitleClicked() {
            }

            @Override
            public void onTitleLongClicked() {
            }

            @Override
            public void onSearchViewOpened() {

            }

            @Override
            public void onSearchViewClosed() {

            }

            @Override
            public void onCategoriesClicked() {

            }
        };

        if (Utility.isSet(title))
            new ActionBarHandler(this, title, null, onActionBarClickedInterface, false, false, false);

        else
            new ActionBarHandler(this, getResources().getString(R.string.photo_gallery), null, onActionBarClickedInterface, false, false, false);


        ViewPager viewPager = (ViewPager) findViewById(R.id.album_fullview_pager);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            pos = b.getInt("POS", 0);
        }



        ProfileViewPagerModal[] profileViewPagerModals = ProfileViewPagerModal.createImagesArrayFromImagesAndVideos(pics, videoModals);

        mImagePagerAdapter = new ImagePagerAdapter(
                this, profileViewPagerModals, mMatchId, mTrkActivity);

        viewPager.setAdapter(mImagePagerAdapter);
        viewPager.setCurrentItem(pos);
        CirclePageIndicator2 indicator = (CirclePageIndicator2) findViewById(R.id.indicator2);

        HashMap<CirclePageIndicator2.ITEM_TYPE, CirclePageIndicator2.ItemTypeModal> map = new HashMap<>();
        map.put(CirclePageIndicator2.ITEM_TYPE.CIRCLE,
                new CirclePageIndicator2.ItemTypeModal(R.layout.pager_indicator_image, R.drawable.pager_circle_selected,
                        R.drawable.pager_circle_unselected));
        map.put(CirclePageIndicator2.ITEM_TYPE.CAMERA,
                new CirclePageIndicator2.ItemTypeModal(R.layout.pager_indicator_image, R.drawable.pager_camera_selected,
                        R.drawable.pager_camera_unselected));
        ArrayList<CirclePageIndicator2.ITEM_TYPE> items = new ArrayList<>();

        if(profileViewPagerModals != null){
            for(ProfileViewPagerModal profileViewPagerModal : profileViewPagerModals){
                if(profileViewPagerModal.isVideo()) {
                    items.add(CirclePageIndicator2.ITEM_TYPE.CAMERA);
                }else{
                    items.add(CirclePageIndicator2.ITEM_TYPE.CIRCLE);
                }
            }
        }

        indicator.setViewPager(viewPager, map, items);
        int position = viewPager.getCurrentItem();
        //TODO : Not working???
        if(position > 0) {
            mImagePagerAdapter.prePlay(position);
        }

        viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener(){
            @Override
            public void onPageSelected(int position) {
                if(mImagePagerAdapter != null){
                    mImagePagerAdapter.onPageSelected(position);
                }
            }
        });

        if(mImagePagerAdapter != null){
            mImagePagerAdapter.onCreate();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if(mImagePagerAdapter != null){
            mImagePagerAdapter.onResume();
        }
    }

    @Subscribe
    public void onNetworkChanged(NetworkChangeEvent networkChangeEvent){
        if(mImagePagerAdapter != null) {
            mImagePagerAdapter.onNetworkChanged(networkChangeEvent);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if(mImagePagerAdapter != null){
            mImagePagerAdapter.onPause();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        BusProvider.getInstance().register(this);

        if(mImagePagerAdapter != null){
            mImagePagerAdapter.onStart();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        BusProvider.getInstance().unregister(this);

        if(mImagePagerAdapter != null){
            mImagePagerAdapter.onStop();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(mImagePagerAdapter != null){
            mImagePagerAdapter.onDestroy();
        }
    }
}
