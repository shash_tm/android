package com.trulymadly.android.app.utility;

import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.trulymadly.android.app.R;
import com.trulymadly.android.app.listener.TapToRetryListener;

public class TapToRetryHandler implements OnClickListener {

	private final Activity aActivity;
	private final OnClickListener onTapToRetryClickListener;

	private final RelativeLayout custom_prog_bar_id;
	private final RelativeLayout progress_bar_layout_id;
	private final TextView saving_text;
	private final RelativeLayout retry_lay;
	private TapToRetryListener aTabToRetryListener = null;
	private String savingText = null;

	public TapToRetryHandler(Activity act, View v,
			TapToRetryListener aTabToRetryListener, String txt) {
		this(act, v, aTabToRetryListener, txt, true);
	}

	public TapToRetryHandler(Activity act, View v,
			TapToRetryListener aTabToRetryListener, String txt,
			Boolean defaultVisible) {
		this.aActivity = act;
		this.aTabToRetryListener = aTabToRetryListener;
		if (Utility.isSet(txt)) {
			this.savingText = txt;
		}
		custom_prog_bar_id = (RelativeLayout) v
				.findViewById(R.id.custom_prog_bar_id);
		progress_bar_layout_id = (RelativeLayout) v
				.findViewById(R.id.progress_bar_layout_id);
		progress_bar_layout_id.setOnClickListener(this);
		saving_text = (TextView) v.findViewById(R.id.saving_text);
		retry_lay = (RelativeLayout) v.findViewById(R.id.retry_lay);
		onTapToRetryClickListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				onTabToRetryClicked(v);
			}
		};
		retry_lay.setOnClickListener(onTapToRetryClickListener);

		if (defaultVisible) {
			showLoader();
		}
	}

	public void showLoader() {
		custom_prog_bar_id.setVisibility(View.VISIBLE);
		if (Utility.isSet(savingText)) {
			saving_text.setText(savingText);
		} else {
			saving_text.setText(R.string.loading);
		}
		retry_lay.setVisibility(View.GONE);
		progress_bar_layout_id.setVisibility(View.VISIBLE);
	}

	public void showLoaderWithCustomText(String txt) {
		custom_prog_bar_id.setVisibility(View.VISIBLE);
		if (Utility.isSet(txt)) {
			saving_text.setText(txt);
		} else {
			saving_text.setText(R.string.loading);
		}
		retry_lay.setVisibility(View.GONE);
		progress_bar_layout_id.setVisibility(View.VISIBLE);
	}

	public void onNetWorkFailed(Exception e) {
		retry_lay.setVisibility(View.VISIBLE);
		progress_bar_layout_id.setVisibility(View.GONE);
		AlertsHandler.showNetworkError(aActivity, e);
		((TextView) retry_lay.findViewById(R.id.error_message)).setText(Utility.getNetworkErrorUiStringResId(aActivity, e));
	}

	public void onSuccessFull() {
		custom_prog_bar_id.setVisibility(View.GONE);
	}

	private void onTabToRetryClicked(View v) {
		switch (v.getId()) {
		case R.id.retry_lay:
			if (onTapToRetryClickListener != null) {

				if (Utility.isNetworkAvailable(aActivity)) {
					showLoader();
					retry_lay.setVisibility(View.GONE);
					progress_bar_layout_id.setVisibility(View.VISIBLE);
					aTabToRetryListener.reInitialize();
				} else {
					AlertsHandler.showMessage(aActivity,
                            R.string.ERROR_NETWORK_FAILURE);
				}
			}
			break;
		}
	}

	@Override
	public void onClick(View arg0) {
		
	}
}