package com.trulymadly.android.app.utility;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.Snackbar;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.trulymadly.android.app.CircleTransformation;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.listener.AlertDialogInterface;
import com.trulymadly.android.app.listener.ConfirmDialogImpl;
import com.trulymadly.android.app.listener.ConfirmDialogInterface;
import com.trulymadly.android.app.listener.ThreeButtonDialogInterface;

import java.util.ArrayList;

/**
 * Created by udbhav on 10/11/15.
 */
public class AlertsHandler {
    private static Toast toast = null;
    private static Snackbar snackbar = null;

    public static void hideMessage() {
        if (toast != null) {
            toast.cancel();
        }
        if (snackbar != null) {
            snackbar.dismiss();
        }
    }

    public static void showToast(Context ctx, int msg_res_id) {
        hideMessage();
        try {
            toast = Toast.makeText(ctx, msg_res_id, Toast.LENGTH_SHORT);
            toast.show();
        } catch (InflateException ignored) {
            Log.d("ShowToast", ignored.getMessage());
        }
    }

    public static void showToast(Context ctx, String message) {
        hideMessage();
        try {
            toast = Toast.makeText(ctx, message, Toast.LENGTH_SHORT);
            toast.show();
        } catch (InflateException ignored) {
            Log.d("ShowToast", ignored.getMessage());
        }
    }

    public static void showNetworkError(Activity aActivity, Exception e) {
        showMessage(aActivity, Utility.getNetworkErrorStringResid(aActivity, e));
    }

    public static void showMessage(Activity aActivity, int msg_res_rd) {
        showMessage(aActivity, msg_res_rd, true);
    }

    public static void showMessage(Activity aActivity, int msg_res_rd,
                                   boolean durationShort) {
        hideMessage();
        View rootView = getRootView(aActivity);
        if (rootView != null) {
            snackbar = Snackbar.make(rootView,
                    msg_res_rd, durationShort ? Snackbar.LENGTH_SHORT
                            : Snackbar.LENGTH_LONG);
            showSnackBar();
        }
    }

    public static void showMessageInDialog(View dialogWindowDecorView, String msg,
                                           boolean durationShort) {
        hideMessage();
        snackbar = Snackbar.make(dialogWindowDecorView,
                msg, durationShort ? Snackbar.LENGTH_SHORT
                        : Snackbar.LENGTH_LONG);
        showSnackBar();
    }

    public static void showMessageInDialog(View dialogWindowDecorView, int msg_res_rd,
                                           boolean durationShort) {
        hideMessage();
        snackbar = Snackbar.make(dialogWindowDecorView,
                msg_res_rd, durationShort ? Snackbar.LENGTH_SHORT
                        : Snackbar.LENGTH_LONG);
        showSnackBar();
    }

    public static void showMessage(Activity aActivity, String msg) {
        showMessage(aActivity, msg, true);
    }

    public static void showMessage(Activity aActivity, String msg,
                                   boolean durationShort) {
        hideMessage();
        View rootView = getRootView(aActivity);
        if (rootView != null) {
            snackbar = Snackbar.make(rootView, msg,
                    durationShort ? Snackbar.LENGTH_SHORT : Snackbar.LENGTH_LONG);
            showSnackBar();
        }
    }

    public static void showMessage(Activity aActivity, int msg_res_rd,
                                   boolean durationShort, int numberOfLines) {
        hideMessage();
        View rootView = getRootView(aActivity);
        if (rootView != null) {
            snackbar = Snackbar.make(rootView,
                    msg_res_rd, durationShort ? Snackbar.LENGTH_SHORT
                            : Snackbar.LENGTH_LONG);
            showSnackBar(numberOfLines);
        }
    }

    private static View getRootView(Activity aActivity) {
        if (aActivity == null) {
            return null;
        }
        View rootView = aActivity.findViewById(R.id.home_coordinator_layout);
        if (rootView == null)
            rootView = aActivity.getWindow().getDecorView();
        return rootView;
    }

    public static void showAlertDialog(Activity aActivity, int message_res_id) {
        showAlertDialog(aActivity, message_res_id, null);
    }

    public static void showAlertDialog(Activity aActivity, int message_res_id,
                                       AlertDialogInterface alertDialogInterface) {
        showAlertDialog(aActivity, aActivity.getResources().getString(message_res_id), alertDialogInterface);
    }


    public static void showAlertDialog(Activity aActivity, String message,
                                       AlertDialogInterface alertDialogInterface) {
        showAlertDialog(aActivity, message, R.string.ok_text, alertDialogInterface);
    }

    public static void showAlertDialog(Activity aActivity, String message, int pos_button_text_res_id,
                                       final AlertDialogInterface alertDialogInterface) {
        if (!aActivity.isFinishing()) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    aActivity);
            alertDialogBuilder.setTitle(null);
            alertDialogBuilder
                    .setMessage(message)
                    .setCancelable(false)
                    .setPositiveButton(pos_button_text_res_id,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    if (alertDialogInterface != null) {
                                        alertDialogInterface.onButtonSelected();
                                    }
                                    dialog.cancel();
                                }
                            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            try {
                alertDialog.show();
            } catch (InflateException ignored) {
            }
        }
    }

    public static void showMessageWithAction(Activity aActivity, String msg, String action,
                                             View.OnClickListener onClickListener) {
        showMessageWithAction(aActivity, msg, action, onClickListener, true);
    }

    private static void showMessageWithAction(Activity aActivity, String msg, String action,
                                              View.OnClickListener onClickListener, boolean durationShort) {
        hideMessage();
        View rootView = getRootView(aActivity);
        if (rootView != null) {
            snackbar = Snackbar.make(rootView, msg,
                    durationShort ? Snackbar.LENGTH_SHORT : Snackbar.LENGTH_LONG).setAction(action, onClickListener);
            showSnackBar();
        }
    }

    public static void showMessageWithAction(Activity aActivity, int msgId, int actionId,
                                             View.OnClickListener onClickListener) {
        showMessageWithAction(aActivity, msgId, actionId, onClickListener, true);
    }

    public static void showMessageWithAction(Activity aActivity, int msgId, int actionId,
                                             View.OnClickListener onClickListener, int numOfLines) {
        showMessageWithAction(aActivity, msgId, actionId, onClickListener, true, numOfLines);
    }

    public static void showMessageWithAction(Activity aActivity, int msgId, int actionId,
                                             View.OnClickListener onClickListener,
                                             boolean durationShort, int numOfLines) {
        hideMessage();
        View rootView = getRootView(aActivity);
        if (rootView != null) {
            snackbar = Snackbar.make(rootView, msgId,
                    durationShort ? Snackbar.LENGTH_SHORT : Snackbar.LENGTH_LONG).setAction(actionId, onClickListener);
            showSnackBar(numOfLines);
        }
    }

    private static void showMessageWithAction(Activity aActivity, int msgId, int actionId,
                                              View.OnClickListener onClickListener, boolean durationShort) {
        hideMessage();
        View rootView = getRootView(aActivity);
        if (rootView != null) {
            snackbar = Snackbar.make(rootView, msgId,
                    durationShort ? Snackbar.LENGTH_SHORT : Snackbar.LENGTH_LONG).setAction(actionId, onClickListener);
            showSnackBar();
        }
    }

    public static void showMessage(Activity aActivity, String msg,
                                   boolean durationShort, int numberOfLines) {
        hideMessage();
        View rootView = getRootView(aActivity);
        if (rootView != null) {
            snackbar = Snackbar.make(rootView, msg,
                    durationShort ? Snackbar.LENGTH_SHORT : Snackbar.LENGTH_LONG);
            showSnackBar(numberOfLines);
        }
    }

    private static void showSnackBar() {
        View view = snackbar.getView();
        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
        snackbar.show();
    }

    private static void showSnackBar(int numberOfLines) {
        View view = snackbar.getView();
        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
        tv.setMaxLines(numberOfLines);
        snackbar.show();
    }

    public static void showThreeButtonDialog(Context ctx, String message,
                                             String positiveText, String negativeText, String neutralText,
                                             final ThreeButtonDialogInterface threeButtonDialogInterface,
                                             boolean isCancelable) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctx);
        alertDialogBuilder.setTitle(null);
        alertDialogBuilder
                .setMessage(message)
                .setCancelable(isCancelable)
                .setPositiveButton(positiveText,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                threeButtonDialogInterface
                                        .onPositiveButtonSelected();
                                dialog.cancel();
                            }
                        })
                .setNegativeButton(negativeText,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                threeButtonDialogInterface
                                        .onNegativeButtonSelected();
                                dialog.cancel();
                            }
                        })
                .setNeutralButton(neutralText,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                threeButtonDialogInterface
                                        .onNeutralButtonSelected();
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        try {
            alertDialog.show();
        } catch (WindowManager.BadTokenException | OutOfMemoryError ignored) {

        }
    }

    public static void showConfirmDialog(Context ctx, int messageResId,
                                         int positiveTextResId, int negativeTextResId,
                                         ConfirmDialogInterface confirmDialogInterface) {
        showConfirmDialog(ctx, messageResId, positiveTextResId, negativeTextResId,
                confirmDialogInterface, false);
    }

    public static void showConfirmDialog(Context ctx, int messageResId,
                                         int positiveTextResId, int negativeTextResId,
                                         final ConfirmDialogInterface confirmDialogInterface,
                                         boolean isCancelable) {
        showConfirmDialog(ctx, ctx.getResources().getString(messageResId), positiveTextResId, negativeTextResId,
                confirmDialogInterface, isCancelable);

    }

    public static void showConfirmDialog(Context ctx, String message,
                                         int positiveTextResId, int negativeTextResId,
                                         final ConfirmDialogInterface confirmDialogInterface,
                                         boolean isCancelable) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctx);
        alertDialogBuilder.setTitle(null);
        alertDialogBuilder
                .setMessage(message)
                .setCancelable(isCancelable)
                .setPositiveButton(positiveTextResId,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                confirmDialogInterface
                                        .onPositiveButtonSelected();
                                dialog.cancel();
                            }
                        })
                .setNegativeButton(negativeTextResId,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                confirmDialogInterface
                                        .onNegativeButtonSelected();
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        try {
            alertDialog.show();
        } catch (WindowManager.BadTokenException ignored) {
        }
    }

    public static void showConfirmDialogCustomView(Activity aActivity, int layoutResId, int positiveTextResId, int negativeTextResId,
                                                   int postiveButtonTextColorId, boolean isCancelable, final ConfirmDialogInterface confirmDialogInterface) {
        if (!aActivity.isFinishing()) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    aActivity);
            alertDialogBuilder.setTitle(null);
            View customView = aActivity.getLayoutInflater().inflate(layoutResId, null);
            alertDialogBuilder.setView(customView);
            alertDialogBuilder
                    .setCancelable(isCancelable)
                    .setPositiveButton(positiveTextResId,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    if (confirmDialogInterface != null) {
                                        confirmDialogInterface.onPositiveButtonSelected();
                                    }
                                    dialog.cancel();
                                }
                            })
                    .setNegativeButton(negativeTextResId,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    confirmDialogInterface
                                            .onNegativeButtonSelected();
                                    dialog.cancel();
                                }
                            });
            AlertDialog alertDialog = alertDialogBuilder.create();

            try {
                alertDialog.show();
                Button postiveButton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                if (postiveButton != null && postiveButtonTextColorId > 0) {
                    postiveButton.setTextColor(aActivity.getResources().getColor(postiveButtonTextColorId));
                    //postiveButton.setBackgroundColor(Color.BLACK);
                }
            } catch (InflateException | WindowManager.BadTokenException ignored) {
            }
        }
    }


    public static void showConfirmDialog(Context ctx, Spanned message,
                                         int positiveTextResId, int negativeTextResId,
                                         final ConfirmDialogInterface confirmDialogInterface,
                                         boolean isCancelable) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctx);
        alertDialogBuilder.setTitle(null);

        alertDialogBuilder
                .setMessage(message)
                .setCancelable(isCancelable)
                .setPositiveButton(positiveTextResId,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                confirmDialogInterface
                                        .onPositiveButtonSelected();
                                dialog.cancel();
                            }
                        })
                .setNegativeButton(negativeTextResId,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                confirmDialogInterface
                                        .onNegativeButtonSelected();
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        try {
            alertDialog.show();
        } catch (WindowManager.BadTokenException ignored) {
        }
    }

    public static void showRadioButtonDialog(Context context,
                                             ArrayList<String> options,
                                             final RadioGroup.OnCheckedChangeListener onCheckedChangeListener) {
        // custom dialog
        if (options == null || options.size() == 0) {
            throw new IllegalArgumentException("Parameter options should not be null or of size 0");
        }
        if (onCheckedChangeListener == null) {
            throw new IllegalArgumentException("onCheckedChangeListener should not be null");
        }
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_radio_options);
        RadioGroup rg = (RadioGroup) dialog.findViewById(R.id.radio_group);

        for (int i = 0; i < options.size(); i++) {
            RadioButton rb = new RadioButton(context);
            rb.setText(options.get(i));
            rb.setTag(options.get(i));
            rg.addView(rb);
        }
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (onCheckedChangeListener != null) {
                    onCheckedChangeListener.onCheckedChanged(group, checkedId);
                }
                dialog.cancel();
            }
        });
        dialog.show();
    }

    public static void showPaymentModeDialog(Context context, String headerText,
                                             final View.OnClickListener onClickListener,
                                             boolean isGoogleEnabled,
                                             boolean isDefaultPaytmEnabled,
                                             boolean isPaytmWalletEnabled,
                                             boolean isPaytmNBEnabled,
                                             boolean isPaytmCCEnabled,
                                             boolean isPaytmDCEnabled) {
        showPaymentModeDialog(context,
                headerText, false, onClickListener, isGoogleEnabled, isDefaultPaytmEnabled, isPaytmWalletEnabled, isPaytmNBEnabled, isPaytmCCEnabled, isPaytmDCEnabled);
    }

    public static void showPaymentModeDialog(Context context, String headerText,
                                             boolean isSelect,
                                             final View.OnClickListener onClickListener,
                                             boolean isGoogleEnabled,
                                             boolean isDefaultPaytmEnabled,
                                             boolean isPaytmWalletEnabled,
                                             boolean isPaytmNBEnabled,
                                             boolean isPaytmCCEnabled,
                                             boolean isPaytmDCEnabled) {
        // custom dialog
        if (onClickListener == null) {
            throw new IllegalArgumentException("onClickListener should not be null");
        }

        final Dialog dialog = new Dialog(context, R.style.FullScreenDialog);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
//        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
//                WindowManager.LayoutParams.MATCH_PARENT);
        dialog.setContentView(R.layout.dialog_payment_selection2);

        dialog.findViewById(R.id.select_icon).setVisibility((isSelect) ? View.VISIBLE : View.GONE);

        TextView headerTV = (TextView) dialog.findViewById(R.id.header_tv);
        if (Utility.isSet(headerText)) {
            headerTV.setVisibility(View.VISIBLE);
            headerTV.setText(Html.fromHtml(headerText));
        } else {
            headerTV.setVisibility(View.GONE);
        }

        View google = dialog.findViewById(R.id.google_payment_button);
        View paytm = dialog.findViewById(R.id.paytm_payment_button);
        View paytmWallet = dialog.findViewById(R.id.paytm_wallet_payment_button);
        View paytmNB = dialog.findViewById(R.id.paytm_nb_payment_button);
        View paytmCC = dialog.findViewById(R.id.paytm_cc_payment_button);
        View paytmDC = dialog.findViewById(R.id.paytm_dc_payment_button);

        google.setVisibility((isGoogleEnabled) ? View.VISIBLE : View.GONE);
        if (isGoogleEnabled) {
            google.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickListener.onClick(v);
                    dialog.cancel();
                }
            });
        }
        paytm.setVisibility((isDefaultPaytmEnabled) ? View.VISIBLE : View.GONE);
        if (isDefaultPaytmEnabled) {
            paytm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickListener.onClick(v);
                    dialog.cancel();
                }
            });
        }
        paytmWallet.setVisibility((isPaytmWalletEnabled) ? View.VISIBLE : View.GONE);
        if (isPaytmWalletEnabled) {
            paytmWallet.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickListener.onClick(v);
                    dialog.cancel();
                }
            });
        }
        paytmNB.setVisibility((isPaytmNBEnabled) ? View.VISIBLE : View.GONE);
        if (isPaytmNBEnabled) {
            paytmNB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickListener.onClick(v);
                    dialog.cancel();
                }
            });
        }
        paytmCC.setVisibility((isPaytmCCEnabled) ? View.VISIBLE : View.GONE);
        if (isPaytmCCEnabled) {
            paytmCC.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickListener.onClick(v);
                    dialog.cancel();
                }
            });
        }
        paytmDC.setVisibility((isPaytmDCEnabled) ? View.VISIBLE : View.GONE);
        if (isPaytmDCEnabled) {
            paytmDC.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickListener.onClick(v);
                    dialog.cancel();
                }
            });
        }

        dialog.show();
    }

    public static void showWhyNotSparkInsteadDialog(Context context, String myImageLink,
                                                    String theirImageLink, boolean hasLikedBefore,
                                                    String message,
                                                    final View.OnClickListener onClickListener) {
        // custom dialog
        if (onClickListener == null) {
            throw new IllegalArgumentException("onClickListener should not be null");
        }

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_why_not_sparks_instead);

        ImageView borderIV = (ImageView) dialog.findViewById(R.id.border_iv);
        Picasso.with(context).load(R.drawable.image_placeholder).into(borderIV);

        ImageView myImageView = (ImageView) dialog.findViewById(R.id.my_iv);
        ImageView matchImageView = (ImageView) dialog.findViewById(R.id.match_iv);
        if (Utility.isSet(myImageLink)) {
            Picasso.with(context).load(myImageLink).transform(new CircleTransformation()).into(myImageView);
        } else {
            int dummyImage = (Utility.isMale(context)) ? R.drawable.dummy_male : R.drawable.dummy_female;
            Picasso.with(context).load(dummyImage).transform(new CircleTransformation()).into(myImageView);
        }

        if (Utility.isSet(theirImageLink)) {
            Picasso.with(context).load(theirImageLink).transform(new CircleTransformation()).into(matchImageView);
        } else {
            int dummyImage = (!Utility.isMale(context)) ? R.drawable.dummy_male : R.drawable.dummy_female;
            Picasso.with(context).load(dummyImage).transform(new CircleTransformation()).into(matchImageView);
        }

        TextView maybeLater = (TextView) dialog.findViewById(R.id.maybe_later_button);
        View sendSpark = dialog.findViewById(R.id.send_spark_button);
        TextView textViewSendSparks5xTV = (TextView) dialog.findViewById(R.id.send_spark_5x_tv);
        TextView whyStopTV = (TextView) dialog.findViewById(R.id.why_stop_tv);
        maybeLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener.onClick(v);
                dialog.cancel();
            }
        });
        sendSpark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener.onClick(v);
                dialog.cancel();
            }
        });


        int messageResId;
        if (hasLikedBefore) {
            messageResId = Utility.isMale(context) ? R.string.send_a_spark_for_5x_chances_liked_before_male : R.string.send_a_spark_for_5x_chances_liked_before_female;
            whyStopTV.setVisibility(View.GONE);
            maybeLater.setText(R.string.no_thanks);
        } else {
            whyStopTV.setVisibility(View.VISIBLE);
            maybeLater.setText(R.string.may_be_later);
            Integer[] sendSpark5xMessagesMale = new Integer[]{R.string.send_a_spark_for_5x_chances_1, R.string.send_a_spark_for_5x_chances_2, R.string.send_a_spark_for_5x_chances_3_male, R.string.send_a_spark_for_5x_chances_4_male, R.string.send_a_spark_for_5x_chances_5_male};
            Integer[] sendSpark5xMessagesFemale = new Integer[]{R.string.send_a_spark_for_5x_chances_1, R.string.send_a_spark_for_5x_chances_2, R.string.send_a_spark_for_5x_chances_3_female, R.string.send_a_spark_for_5x_chances_4_female, R.string.send_a_spark_for_5x_chances_5_female};
            int len = sendSpark5xMessagesFemale.length;
            int indexOfSendSparkMessages = 0;
            indexOfSendSparkMessages = SPHandler.getInt(context, ConstantsSP.SHARED_KEY_WHY_NOT_SPARK_INSTEAD_MESSAGE_INDEX, 0);
            if (indexOfSendSparkMessages < 0 && indexOfSendSparkMessages >= len) {
                indexOfSendSparkMessages = 0;
            }
            messageResId = Utility.isMale(context) ? sendSpark5xMessagesMale[indexOfSendSparkMessages] : sendSpark5xMessagesFemale[indexOfSendSparkMessages];
            indexOfSendSparkMessages++;
            if (indexOfSendSparkMessages >= len) {
                indexOfSendSparkMessages = 0;
            }
            SPHandler.setInt(context, ConstantsSP.SHARED_KEY_WHY_NOT_SPARK_INSTEAD_MESSAGE_INDEX, indexOfSendSparkMessages);
        }

        if (Utility.isSet(message)) {
            textViewSendSparks5xTV.setText(message);
        } else {
            textViewSendSparks5xTV.setText(messageResId);
        }
        dialog.show();
    }


    public static void showSimpleSelectDialog(Context context, int description,
                                              int positiveButtonText, int negativeButtonText,
                                              View.OnClickListener onClickListener) {
        showSimpleSelectDialog(context, context.getResources().getString(description),
                context.getResources().getString(positiveButtonText), context.getResources().getString(negativeButtonText),
                onClickListener);

    }

    public static void showSimpleSelectDialog(Context context, String description,
                                              String positiveButtonText, String negativeButtonText,
                                              final View.OnClickListener onClickListener,
                                              final DialogInterface.OnCancelListener onCancelledListener) {
        if (!TMSelectHandler.isSelectEnabled(context)) {
            return;
        }

        // custom dialog
        if (onClickListener == null) {
            throw new IllegalArgumentException("onClickListener should not be null");
        }

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_simple_select);

        TextView descriptionTV = (TextView) dialog.findViewById(R.id.description_tv);
        descriptionTV.setText(description);

        TextView positiveText = (TextView) dialog.findViewById(R.id.positive_button_tv);
        final TextView negativeText = (TextView) dialog.findViewById(R.id.negative_button_tv);
        positiveText.setText(positiveButtonText);
        negativeText.setText(negativeButtonText);

        positiveText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener.onClick(v);
                dialog.dismiss();
            }
        });
        negativeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener.onClick(v);
                dialog.dismiss();
            }
        });
        dialog.setOnCancelListener(onCancelledListener);
        dialog.show();
    }

    public static void showUSProfileDialog(Context context, String description,
                                           String positiveButtonText, String negativeButtonText,
                                           final View.OnClickListener onClickListener,
                                           final DialogInterface.OnCancelListener onCancelledListener) {
        if (onClickListener == null) {
            throw new IllegalArgumentException("onClickListener should not be null");
        }

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_simple_select);

        dialog.findViewById(R.id.icon_iv).setVisibility(View.GONE);
        TextView descriptionTV = (TextView) dialog.findViewById(R.id.description_tv);
        descriptionTV.setText(description);

        TextView positiveText = (TextView) dialog.findViewById(R.id.positive_button_tv);
        final TextView negativeText = (TextView) dialog.findViewById(R.id.negative_button_tv);
        positiveText.setText(positiveButtonText);
        negativeText.setText(negativeButtonText);

        positiveText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener.onClick(v);
                dialog.dismiss();
            }
        });
        negativeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener.onClick(v);
                dialog.dismiss();
            }
        });
        dialog.setOnCancelListener(onCancelledListener);
        dialog.show();
    }

    public static void showSimpleSelectDialog(Context context, String description,
                                              String positiveButtonText, String negativeButtonText,
                                              final View.OnClickListener onClickListener) {
        showSimpleSelectDialog(context, description, positiveButtonText, negativeButtonText,
                onClickListener, null);
    }

    public static void showOnActionSelectDialog(Context context, String myImageLink,
                                                String theirImageLink,
                                                int description,
                                                final View.OnClickListener onClickListener) {
        showOnActionSelectDialog(context, myImageLink, theirImageLink, description,
                context.getString(R.string.join_now_ex), context.getString(R.string.may_be_later),
                onClickListener);
    }

    public static void showOnActionSelectDialog(Context context, String myImageLink,
                                                String theirImageLink,
                                                int description, String positiveButtonText,
                                                String negativeButtonText,
                                                final View.OnClickListener onClickListener) {
        // custom dialog
        if (onClickListener == null) {
            throw new IllegalArgumentException("onClickListener should not be null");
        }

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_onaction_select_dialog);

        ImageView borderIV = (ImageView) dialog.findViewById(R.id.border_iv);
        Picasso.with(context).load(R.drawable.select_placeholder).into(borderIV);

        ImageView myImageView = (ImageView) dialog.findViewById(R.id.my_iv);
        ImageView matchImageView = (ImageView) dialog.findViewById(R.id.match_iv);
        if (Utility.isSet(myImageLink)) {
            Picasso.with(context).load(myImageLink).transform(new CircleTransformation()).into(myImageView);
        } else {
            int dummyImage = (Utility.isMale(context)) ? R.drawable.dummy_male : R.drawable.dummy_female;
            Picasso.with(context).load(dummyImage).transform(new CircleTransformation()).into(myImageView);
        }

        if (Utility.isSet(theirImageLink)) {
            Picasso.with(context).load(theirImageLink).transform(new CircleTransformation()).into(matchImageView);
        } else {
            int dummyImage = (!Utility.isMale(context)) ? R.drawable.dummy_male : R.drawable.dummy_female;
            Picasso.with(context).load(dummyImage).transform(new CircleTransformation()).into(matchImageView);
        }

        TextView maybeLater = (TextView) dialog.findViewById(R.id.maybe_later_button);
        if (Utility.isSet(negativeButtonText)) {
            maybeLater.setText(negativeButtonText);
        } else {
            maybeLater.setText(R.string.may_be_later);
        }
        TextView joinNow = (TextView) dialog.findViewById(R.id.join_now_button);
        if (Utility.isSet(positiveButtonText)) {
            joinNow.setText(positiveButtonText);
        } else {
            joinNow.setText(R.string.join_now_ex);
        }
        TextView whyStopTV = (TextView) dialog.findViewById(R.id.why_stop_tv);
        whyStopTV.setText(Html.fromHtml(context.getString(description)));
        maybeLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener.onClick(v);
                dialog.cancel();
            }
        });
        joinNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener.onClick(v);
                dialog.cancel();
            }
        });


        dialog.show();
    }


    public static void showAppRateDialog(final Activity activity, String message,
                                         int positiveTextResId, int negativeTextResId,
                                         final ConfirmDialogImpl confirmDialogImpl,
                                         boolean isCancelable) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
        alertDialogBuilder.setTitle(null);
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.app_rate_dialog, null);
        alertDialogBuilder.setView(dialogView);

        final RatingBar ratingBar = (RatingBar) dialogView.findViewById(R.id.rating_bar);

        alertDialogBuilder
                .setMessage(message)
                .setCancelable(isCancelable)
                .setPositiveButton(positiveTextResId,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent data = null;
                                if (ratingBar != null) {
                                    data = new Intent();
                                    Float rating = ratingBar.getRating();
                                    data.putExtra("app_rating", rating.intValue());
                                }

                                confirmDialogImpl
                                        .onPositiveButtonSelected(data);
                                dialog.cancel();
                            }
                        })
                .setNegativeButton(negativeTextResId,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                confirmDialogImpl
                                        .onNegativeButtonSelected();
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        try {
            alertDialog.show();
            TextView messageText = (TextView) alertDialog.findViewById(android.R.id.message);
            if (messageText != null)
                messageText.setGravity(Gravity.CENTER);
        } catch (WindowManager.BadTokenException ignored) {
        }
    }
}
