package com.trulymadly.android.app.utility;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.trulymadly.android.app.R;
import com.trulymadly.android.app.adapter.BuySparkDialogViewPagerAdapter;
import com.trulymadly.android.app.modal.ImageAndText;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;

import static butterknife.ButterKnife.findById;

/**
 * Created by avin on 16/11/16.
 */

public class TMSelectMainContentUIHandler {

    private Context mContext;
    private View mMasterView;
    private ViewPager mSelectBuyViewPager;
    private CirclePageIndicator mSelectBuyCirclePageIndicator;
    private BuySparkDialogViewPagerAdapter mBuySelectViewPagerAdapter;

    private ArrayList<String> mDescStrings;
    private ArrayList<Integer> mDescImages;
    private ArrayList<ImageAndText> mImageAndTexts;

    private Timer mTimer;
    private int mCurrentPage = 0;
    private boolean isFromAutoScroll = true;
    private ViewPager.OnPageChangeListener mOnPageChangeListener;
    private WeakReference<Context> mContextWeakReference;

    public TMSelectMainContentUIHandler(Context mContext, View mMasterView) {
        this.mContext = mContext;
        this.mMasterView = mMasterView;

        mSelectBuyViewPager = findById(mMasterView, R.id.select_buy_viewpager);
        mSelectBuyCirclePageIndicator = findById(mMasterView, R.id.select_buy_viewpager_indicator);

        ArrayList<String> mDescStrings = getDescStrings();
        ArrayList<Integer> mDescImages = getDescImages();

        mContextWeakReference = new WeakReference<>(mContext);

        mImageAndTexts = new ArrayList<>();
        for (int i = 0; i < mDescStrings.size(); i++) {
            ImageAndText imageAndText = new ImageAndText();
            imageAndText.setmText(mDescStrings.get(i));
            imageAndText.setmDrawable(mDescImages.get(i));
            mImageAndTexts.add(imageAndText);
        }

        mBuySelectViewPagerAdapter = new BuySparkDialogViewPagerAdapter(mImageAndTexts, mContext,
                ActivityCompat.getColor(mContext, R.color.tm_select_text_color));
        mSelectBuyViewPager.setAdapter(mBuySelectViewPagerAdapter);

        mSelectBuyCirclePageIndicator.setStrokeColor(
                ActivityCompat.getColor(mContext, R.color.gray_63));
        mSelectBuyCirclePageIndicator.setFillColor(
                ActivityCompat.getColor(mContext, R.color.gray_63));
        mSelectBuyCirclePageIndicator.setViewPager(mSelectBuyViewPager);


        if (mOnPageChangeListener == null) {
            mOnPageChangeListener = new ViewPager.SimpleOnPageChangeListener() {
                @Override
                public void onPageSelected(int position) {
                    super.onPageSelected(position);
                    if (!isFromAutoScroll) {
                        if (mTimer != null) {
                            mTimer.cancel();
                        }
                    }

                    isFromAutoScroll = false;
                }
            };
        }
        mSelectBuyViewPager.addOnPageChangeListener(mOnPageChangeListener);
        startTimer();
    }

    private ArrayList<String> getDescStrings() {
        if (mDescStrings == null) {
            String[] strings = mContext.getResources().getStringArray(R.array.select_desc_packages);
            mDescStrings = new ArrayList<>(Arrays.asList(strings));
        }

        return mDescStrings;
    }

    private ArrayList<Integer> getDescImages() {
        if (mDescImages == null) {
            TypedArray typedArray = mContext.getResources().obtainTypedArray(R.array.select_desc_packages_drawables);
            mDescImages = new ArrayList<>();
            for (int i = 0; i < typedArray.length(); i++) {
                mDescImages.add(i, typedArray.getResourceId(i, -1));
            }
            typedArray.recycle();
        }

        return mDescImages;
    }

    private void startTimer() {
        mTimer = new Timer();
        TimerTask mTimerTask = new TimerTask() {
            @Override
            public void run() {
                Context context = mContextWeakReference.get();
                if (mSelectBuyViewPager != null && mImageAndTexts != null && mImageAndTexts.size() > 1
                        && context != null) {
                    mCurrentPage = (mCurrentPage + 1) % mImageAndTexts.size();
                    mSelectBuyViewPager.post(new Runnable() {
                        @Override
                        public void run() {
                            if (mSelectBuyViewPager != null) {
                                isFromAutoScroll = true;
                                mSelectBuyViewPager.setCurrentItem(mCurrentPage, true);
                            }
                        }
                    });
                } else {
                    mTimer.cancel();
                }
            }
        };

        mTimer.schedule(mTimerTask, 5000, 5000);
    }

    public void onDestroy() {
        if (mTimer != null) {
            mTimer.cancel();
        }
    }


}
