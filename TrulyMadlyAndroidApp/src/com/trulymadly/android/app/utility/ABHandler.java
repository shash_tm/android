package com.trulymadly.android.app.utility;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.trulymadly.android.app.R;
import com.trulymadly.android.app.json.ConstantsSP;

import java.util.Random;

import static com.trulymadly.android.app.utility.Utility.isSet;

/**
 * Created by udbhav on 01/12/15.
 */
public class ABHandler {

    private static String tag = "ABHANDLER";

    private static void initAB(Context ctx, ABTYPE type) {
        switch (type) {
            case AB_EMAIL_SIGNUP_DISABLED:
                //disabling this AB
                return;
            case AB_SPARKS_ICON:
                //disabling this AB
                return;
            case AB_SPARKS_MATCH_GUARANTEE_TAG:
                //disabling this AB
                return;
            default:
                if (!SPHandler.isExists(ctx, type.toString())) {
                    setAbConstant(ctx, type, createAbValue(ctx, type));
                }
        }

    }

    public static boolean getABValue(Context ctx, ABTYPE type) {
        initAB(ctx, type);
        switch (type) {
            case AB_EMAIL_SIGNUP_DISABLED:
                //disabling this AB
                return true;
            case AB_SPARKS_ICON:
                //disabling this AB
                return true;
            case AB_SPARKS_MATCH_GUARANTEE_TAG:
                //disabling this AB, but using the overrirde flag
                if (SPHandler.isExists(ctx, ConstantsSP.SHARED_KEY_AB_SPARKS_MATCH_GUARANTEE_TAG_OVERRIDE)) {
                    return SPHandler.getBool(ctx, ConstantsSP.SHARED_KEY_AB_SPARKS_MATCH_GUARANTEE_TAG_OVERRIDE);
                } else {
                    return true;
                }
            default:
                return SPHandler.getBool(ctx, type.toString());
        }

    }

    private static void setAbConstant(Context ctx, ABTYPE type, boolean value) {
        SPHandler.setBool(ctx, type.toString(), value);
    }

    @SuppressWarnings("UnusedParameters")
    private static boolean createAbValue(Context ctx, ABTYPE type) {
        switch (type) {
            case AB_EMAIL_SIGNUP_DISABLED:
                return true;
            case AB_SPARKS_ICON:
                return true;//getRandomValueByDeviceId(ctx);
            case AB_SPARKS_MATCH_GUARANTEE_TAG:
                return true;//getRandomValueByDeviceId(ctx);
            default:
                return false;
        }
    }

    /**
     * Get random 50-50 value and taking device id into account
     *
     * @return
     */

    private static boolean getRandomValueByDeviceId(Context ctx) {
        try {
            //Get the first two digitof Device Id in hex and converts to decimal to get a random value
            String deviceId = Utility.getDeviceId(ctx);
            int randomInt = Integer.parseInt(deviceId.substring(0, 2), 16);
            return (randomInt % 2) == 0;
        } catch (NumberFormatException | StringIndexOutOfBoundsException e) {
            return getRandomValue(50);
        }
    }

    /**
     * Get random value based on percentage
     *
     * @param percent Value between 0 and 100
     * @return
     */
    private static boolean getRandomValue(int percent) {
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(100);
        return percent >= randomInt + 1;

    }

    public static void setVisibility(Context aContext, ABTYPE type, View view) {
        if (getABValue(aContext, type)) {
            view.setVisibility(View.GONE);
        } else {
            view.setVisibility(View.VISIBLE);
        }
    }

    public static void setImageIconForSparkFABIcon(Context aContext, ImageView mSparkFAB, ImageView spark_lay_icon_large_ab) {
        spark_lay_icon_large_ab.setScaleType(ImageView.ScaleType.FIT_XY);
        if (getABValue(aContext, ABTYPE.AB_SPARKS_ICON)) {
            mSparkFAB.setImageResource(0);
            spark_lay_icon_large_ab.setImageResource(R.drawable.ic_spark_ab);
            spark_lay_icon_large_ab.setVisibility(View.VISIBLE);
        } else {
            mSparkFAB.setImageResource(R.drawable.ic_spark);
            spark_lay_icon_large_ab.setVisibility(View.GONE);
        }
    }

    public static String getABPrefix(Context ctx) {

        String chat_ab_prefix = SPHandler.getString(ctx,
                ConstantsSP.SHARED_KEYS_CHAT_AB_PREFIX);

        String abHandler_prefix = "";
        //disabling this AB
        //abHandler_prefix += (isSet(abHandler_prefix) ? "-" : "") + ABTYPE.AB_EMAIL_SIGNUP_DISABLED.toString() + ":" + getABValue(ctx, ABTYPE.AB_EMAIL_SIGNUP_DISABLED);

        //disabling this AB
        //abHandler_prefix += (isSet(abHandler_prefix) ? "-" : "") + ABTYPE.AB_SPARKS_ICON.toString() + ":" + getABValue(ctx, ABTYPE.AB_SPARKS_ICON);

        //disabling this AB
        //abHandler_prefix += (isSet(abHandler_prefix) ? "-" : "") + ABTYPE.AB_SPARKS_MATCH_GUARANTEE_TAG.toString() + ":" + getABValue(ctx, ABTYPE.AB_SPARKS_MATCH_GUARANTEE_TAG);

        String ab_prefix = "";
        if (isSet(chat_ab_prefix)) {
            ab_prefix += (isSet(ab_prefix) ? "-" : "") + chat_ab_prefix;
        }
        if (isSet(abHandler_prefix)) {
            ab_prefix += (isSet(ab_prefix) ? "-" : "") + abHandler_prefix;
        }
        return ab_prefix;
    }

    public enum ABTYPE {
        AB_EMAIL_SIGNUP_DISABLED, AB_SPARKS_ICON, AB_SPARKS_MATCH_GUARANTEE_TAG
    }
}
