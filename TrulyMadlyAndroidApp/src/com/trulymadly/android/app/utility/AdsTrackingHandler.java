package com.trulymadly.android.app.utility;

import com.crashlytics.android.Crashlytics;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by avin on 25/02/16.
 */
public class AdsTrackingHandler {
//    private static final String USER_AGENT_STRING = "NETWORK : ";
    private static final int DEFAULT_HTTP_CONNECT_TIMEOUT = 30000;
    private static final int DEFAULT_HTTP_SOCKET_TIMEOUT = 30000;
    private static final String tag = "AdsTrackingHandler";

    private static void performHttpRequest(String uri) {

        Callback callback = new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                TmLogger.d(tag, "Tracking url - failure");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                TmLogger.d(tag, "Tracking url - success");
            }

        };

        try {
            OkHttpClient.Builder clientBuilder = OkHttpHandler.getOkHttpBuilder(DEFAULT_HTTP_CONNECT_TIMEOUT, DEFAULT_HTTP_SOCKET_TIMEOUT, 0);
            OkHttpClient client = clientBuilder.build();

            Request.Builder builder = new Request.Builder().url(uri);

//            Map<String, String> params = new HashMap<String, String>();
//            params.put("data", body);
//            params.put("content_type", "application/json");
            Request request = builder.get().build();
//            Request request = builder.post(createHttpPostFormBody(params)).build();
            client.newCall(request).enqueue(callback);
            TmLogger.d(tag, "Tracking url called");
        } catch (IllegalStateException | IllegalArgumentException e) {
            TmLogger.e(tag, "Error communicating with server.", e);
            Crashlytics.logException(e);
        }
    }

    public static void callUrls(ArrayList<String> uris) {
        if(uris == null || uris.size() == 0){
            return;
        }
        for (String uri : uris) {
            performHttpRequest(uri);
        }
    }

}
