/**
 *
 */
package com.trulymadly.android.app.utility;

import android.app.Activity;
import android.content.Context;
import android.view.InflateException;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;

import com.cesards.cropimageview.CropImageView;
import com.cesards.cropimageview.CropImageView.CropType;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu.OnOpenedListener;
import com.squareup.picasso.Callback.EmptyCallback;
import com.squareup.picasso.Picasso;
import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.activities.VoucherActivity;
import com.trulymadly.android.app.json.ConstantsRF;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.sqlite.RFHandler;

import java.util.HashMap;

import static butterknife.ButterKnife.findById;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities.sparks;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventStatus.view;

/**
 * @author udbhav
 */
public class SlidingMenuHandler {

    private final Context aContext;
    private final Activity aActivity;
    private final int touchMode;
    private SlidingMenu menu = null;
    private boolean hasVouchers = false;
    private View sliding_menu_item_tm_frills, menu_select_label, sliding_menu_item_vouchers,
            menu_select_container, menu_bottom_border, sliding_menu_item_scenes;
    private TextView menu_select_expires_tv, menu_select_buy_button, menu_name, menu_age;
    private CropImageView profilePicIv;

    public SlidingMenuHandler(Activity aActivity) {
        this(aActivity, SlidingMenu.TOUCHMODE_FULLSCREEN);
    }

    public SlidingMenuHandler(Activity act, int TOUCHMODE) {
        aContext = act;
        this.aActivity = act;
        touchMode = TOUCHMODE;

        try {

            menu = new SlidingMenu(aActivity);
            menu.setMode(SlidingMenu.LEFT);
            menu.setTouchModeAbove(touchMode);
            menu.setBehindScrollScale(0);
            menu.setBehindOffsetRes(R.dimen.sliding_menu_right_offset);
            // aActivity.getResources().getDimension(R.dimen.sliding_menu_width))
            // menu.setBehindOffset((int) Utility.getScreenWidth(aActivity) -
            // Utility.dpToPx(120));
            menu.setFadeDegree(.35f);
            menu.setFadeEnabled(true);
            menu.attachToActivity(aActivity, SlidingMenu.SLIDING_CONTENT, true);
            menu.setMenu(R.layout.sliding_menu);
        } catch (InflateException | VerifyError | OutOfMemoryError e) {
            aActivity.finish();
            return;
        }

        hasVouchers = VoucherActivity.getVouchersFromDatabase(aContext).size() > 0;

        View sliding_menu_item_share = findById(menu, R.id.sliding_menu_item_share);
        sliding_menu_item_tm_frills = findById(menu, R.id.sliding_menu_item_tm_frills);
        sliding_menu_item_scenes = findById(menu, R.id.sliding_menu_item_scenes);
        sliding_menu_item_vouchers = findById(menu, R.id.sliding_menu_item_vouchers);
        menu_bottom_border = findById(menu, R.id.menu_bottom_border);
        menu_select_container = findById(menu, R.id.menu_select_container);
        menu_select_label = findById(menu, R.id.menu_select_label);
        menu_select_expires_tv = findById(menu, R.id.menu_select_expires_tv);
        menu_select_buy_button = findById(menu, R.id.menu_select_buy_button);
        menu_name = findById(menu, R.id.menu_name);
        menu_age = findById(menu, R.id.menu_age);
        profilePicIv = findById(menu, R.id.menu_profile_pic);

        showUserData();


        OnClickListener onSlidingMenuClick = new OnClickListener() {

            @Override
            public void onClick(View v) {
                int id = v.getId();

                if (getMenu() != null && getMenu().isShown()) {
                    getMenu().toggle(false);
                }

                switch (id) {
                    case R.id.sliding_menu_item_help:
                        TrulyMadlyTrackEvent.trackEvent(aContext,
                                TrulyMadlyEvent.TrulyMadlyActivities.menu, TrulyMadlyEvent.TrulyMadlyEventTypes.terms_of_service_clicked, 0,
                                null, null, true);
                        ActivityHandler.startHelpActivity(aContext);
                        break;
                    case R.id.menu_profile_container:
                        TrulyMadlyTrackEvent.trackEvent(aContext,
                                TrulyMadlyEvent.TrulyMadlyActivities.menu, TrulyMadlyEvent.TrulyMadlyEventTypes.my_profile_clicked, 0,
                                null, null, true);
                        ActivityHandler.startProfileActivity(aContext,
                                ConstantsUrls.get_my_profile_url(), false, true, false);
                        break;
                    case R.id.sliding_menu_item_home:
                        TrulyMadlyTrackEvent.trackEvent(aContext,
                                TrulyMadlyEvent.TrulyMadlyActivities.menu, TrulyMadlyEvent.TrulyMadlyEventTypes.matches_clicked, 0,
                                null, null, true);
                        ActivityHandler.startMatchesActivity(aContext);
                        break;
                    case R.id.sliding_menu_item_preferences:
                        TrulyMadlyTrackEvent.trackEvent(aContext,
                                TrulyMadlyEvent.TrulyMadlyActivities.menu, TrulyMadlyEvent.TrulyMadlyEventTypes.preferences_clicked, 0,
                                null, null, true);
                        ActivityHandler.startEditPartnerPreferencesActivity(aContext);
                        break;
                    case R.id.sliding_menu_item_conversations:
                        TrulyMadlyTrackEvent.trackEvent(aContext,
                                TrulyMadlyEvent.TrulyMadlyActivities.menu, TrulyMadlyEvent.TrulyMadlyEventTypes.conversations_clicked, 0,
                                null, null, true);
                        ActivityHandler.startConversationListActivity(aContext);
                        break;
                    case R.id.sliding_menu_item_share:
                        TrulyMadlyTrackEvent.trackEvent(aContext,
                                TrulyMadlyEvent.TrulyMadlyActivities.menu, TrulyMadlyEvent.TrulyMadlyEventTypes.share_clicked, 0,
                                null, null, true);
                        ActivityHandler.shareViaTextIntent(aActivity);
                        break;
                    case R.id.sliding_menu_item_vouchers:
                        ActivityHandler.startVoucherActivity(aContext, false);
                        break;
                    case R.id.sliding_menu_item_settings:
                        TrulyMadlyTrackEvent.trackEvent(aContext,
                                TrulyMadlyEvent.TrulyMadlyActivities.menu, TrulyMadlyEvent.TrulyMadlyEventTypes.settings_clicked, 0,
                                null, null, true);
                        ActivityHandler.startSettingsActivity(aContext);
                        break;

                    case R.id.sliding_menu_item_faq:
                        TrulyMadlyTrackEvent.trackEvent(aContext,
                                TrulyMadlyEvent.TrulyMadlyActivities.menu, TrulyMadlyEvent.TrulyMadlyEventTypes.faq_clicked, 0,
                                null, null, true);
                        ActivityHandler.startFaqsActivity(aContext);
                        break;
                    case R.id.sliding_menu_item_tm_frills:
                        MoEHandler.trackEvent(aContext, MoEHandler.Events.SPARK_SIDE_MENU_CLICKED);
                        MoEHandler.trackEvent(aContext, MoEHandler.Events.BUY_PAGE_OPENED);
                        HashMap<String, String> eventInfoSparks = new HashMap<>();
                        eventInfoSparks.put("match_id", TrulyMadlyEvent.TrulyMadlyActivities.sparks);
                        eventInfoSparks.put("sparks_left", String.valueOf(SparksHandler.getSparksLeft(aContext)));
                        eventInfoSparks.put("from_intro", String.valueOf(false));
                        eventInfoSparks.put("from_scenes", String.valueOf(false));
                        TrulyMadlyTrackEvent.trackEvent(aContext, sparks, TrulyMadlyEvent.TrulyMadlyEventTypes.buy, 0, view,
                                eventInfoSparks, true, true);
                        ActivityHandler.startBuyPackagesActivity(aActivity);
//                        ActivityHandler.startBuyPackagesActivityNew(aActivity);
                        break;
                    case R.id.menu_add_video_icon:
                        TrulyMadlyTrackEvent.trackEvent(aContext,
                                TrulyMadlyEvent.TrulyMadlyActivities.menu, TrulyMadlyEvent.TrulyMadlyEventTypes.add_video_clicked, 0,
                                null, null, true);
                        ActivityHandler.startPhotos(aActivity, false, null);
                        break;
//					case R.id.sliding_menu_item_categories:
//						ActivityHandler.startCategoriesActivity(aContext);
//						break;
                    case R.id.menu_select_container:
                    case R.id.menu_select_buy_button:
                        HashMap<String, String> eventInfoSelect = new HashMap<>();
                        boolean isSelectMember = TMSelectHandler.isSelectMember(aContext);
                        eventInfoSelect.put("is_select_member", String.valueOf(isSelectMember));
                        if (TMSelectHandler.isSelectMember(aContext)) {
                            eventInfoSelect.put("expiry_in_days", String.valueOf(TMSelectHandler.getSelectDaysLeft(aContext)));
                        }
                        TrulyMadlyTrackEvent.trackEvent(aContext,
                                TrulyMadlyEvent.TrulyMadlyActivities.select, TrulyMadlyEvent.TrulyMadlyEventTypes.tm_select_menu_clicked, 0,
                                null, eventInfoSelect, true);
                        ActivityHandler.startTMSelectActivity(aContext, "menu", false);
                        break;
                    case R.id.sliding_menu_item_scenes:
                        TrulyMadlyTrackEvent.trackEvent(aContext,
                                TrulyMadlyEvent.TrulyMadlyActivities.menu, TrulyMadlyEvent.TrulyMadlyEventTypes.tm_scenes_clicked, 0,
                                null, null, true);
                        ActivityHandler.startCategoriesActivity(aContext);
                        break;
                    default:
                        break;
                }
            }
        };

        findById(menu, R.id.sliding_menu_item_home).setOnClickListener(
                onSlidingMenuClick);
        findById(menu, R.id.sliding_menu_item_preferences)
                .setOnClickListener(onSlidingMenuClick);
        findById(menu, R.id.sliding_menu_item_conversations)
                .setOnClickListener(onSlidingMenuClick);
        sliding_menu_item_share.setOnClickListener(
                onSlidingMenuClick);
        sliding_menu_item_vouchers.setOnClickListener(
                onSlidingMenuClick);
        findById(menu, R.id.sliding_menu_item_settings).setOnClickListener(
                onSlidingMenuClick);
        findById(menu, R.id.menu_profile_container).setOnClickListener(
                onSlidingMenuClick);
        findById(menu, R.id.sliding_menu_item_help).setOnClickListener(
                onSlidingMenuClick);
        findById(menu, R.id.sliding_menu_item_faq).setOnClickListener(
                onSlidingMenuClick);
        findById(menu, R.id.menu_add_video_icon).setOnClickListener(
                onSlidingMenuClick);
        menu_select_buy_button.setOnClickListener(
                onSlidingMenuClick);
        menu_select_container.setOnClickListener(
                onSlidingMenuClick);
        sliding_menu_item_scenes.setOnClickListener(
                onSlidingMenuClick);
        sliding_menu_item_tm_frills.setOnClickListener(onSlidingMenuClick);
//		ButterKnife.findById(menu, R.id.sliding_menu_item_categories).setOnClickListener(
//				onSlidingMenuClick);

        menu.setOnOpenedListener(
                new OnOpenedListener() {
                    @Override
                    public void onOpened() {
                        showUserData();
                    }
                });

    }

    private void showUserData() {
        String name = SPHandler.getString(aContext,
                ConstantsSP.SHARED_KEYS_USER_NAME);
        if (Utility.isSet(name)) {
            menu_name.setText(name);
        }
        String age = SPHandler.getString(aContext,
                ConstantsSP.SHARED_KEYS_USER_AGE);
        if (Utility.isSet(age)) {
            menu_age.setText(", " + age);
        }
//        String city = SPHandler.getString(aContext,
//                ConstantsSP.SHARED_KEYS_USER_CITY);
//        if (Utility.isSet(city)) {
//             ((TextView) findById(menu, R.id.menu_city)).setText(city);
//        }

        String thumbUrl = SPHandler.getString(aContext,
                ConstantsSP.SHARED_KEYS_USER_PROFILE_THUMB_URL);
        String profileUrl = SPHandler.getString(aContext,
                ConstantsSP.SHARED_KEYS_USER_PROFILE_FULL_URL);
        if (Utility.isSet(profileUrl) || Utility.isSet(thumbUrl)) {
            String imageUrlToLoad = Utility.isSet(profileUrl) ? profileUrl
                    : thumbUrl;
            Picasso.with(aContext).load(imageUrlToLoad)
                    .into(profilePicIv, new EmptyCallback() {
                        @Override
                        public void onSuccess() {
                            profilePicIv.setCropType(CropType.CENTER_TOP);
                        }

                        @Override
                        public void onError() {
                        }
                    });
        } else {
            String gender = SPHandler.getString(aContext,
                    ConstantsSP.SHARED_KEYS_USER_GENDER);
            int dummyImage;
            if (Utility.isSet(gender)) {
                dummyImage = Utility.isMale(aContext) ? R.drawable.dummy_male
                        : R.drawable.dummy_female;
            } else {
                dummyImage = R.drawable.dummyuser;
            }
            Picasso.with(aContext)
                    .load(dummyImage)
                    .into(profilePicIv, new EmptyCallback() {
                        @Override
                        public void onSuccess() {
                            profilePicIv.setScaleType(ScaleType.CENTER_INSIDE);
                        }

                        @Override
                        public void onError() {
                        }
                    });
        }
        sliding_menu_item_vouchers.setVisibility(
                hasVouchers ? View.VISIBLE : View.GONE);

        toggleTMFrills(SparksHandler.isSparksEnabled(aContext));
        toggleScenes(RFHandler.getBool(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_ALLOW_MEETUPS));

        if (TMSelectHandler.isSelectEnabled(aContext)) {
            menu_bottom_border.setVisibility(View.VISIBLE);
            menu_select_container.setVisibility(View.VISIBLE);
            menu_select_buy_button.setText(TMSelectHandler.getSelectSideCta(aContext));
            if (TMSelectHandler.isSelectMember(aContext)) {
                menu_select_label.setVisibility(View.VISIBLE);
                menu_select_expires_tv.setVisibility(View.VISIBLE);
                menu_select_expires_tv.setText(getExpiryDaysString(TMSelectHandler.getSelectDaysLeft(aContext)));
            } else {
                menu_select_label.setVisibility(View.GONE);
                menu_select_expires_tv.setVisibility(View.GONE);
            }
        } else {
            menu_select_label.setVisibility(View.GONE);
            menu_bottom_border.setVisibility(View.GONE);
            menu_select_container.setVisibility(View.GONE);
        }
    }

    private String getExpiryDaysString(int daysLeft) {
        return String.format(aContext.getString((daysLeft > 1) ? R.string.expires_in_days : R.string.expires_in_day),
                String.valueOf(daysLeft));
    }

    public SlidingMenu getMenu() {
        return menu;
    }

    public void toggleVisibility(boolean showMenu) {
        menu.setTouchModeAbove(showMenu ? touchMode
                : SlidingMenu.TOUCHMODE_NONE);
    }


    public void toggleTMFrills(boolean allowTMFrills) {
        sliding_menu_item_tm_frills.setVisibility(
                allowTMFrills ? View.VISIBLE : View.GONE);
    }

    public void toggleScenes(boolean allowScenes) {
        sliding_menu_item_scenes.setVisibility(
                allowScenes ? View.VISIBLE : View.GONE);
    }

}
