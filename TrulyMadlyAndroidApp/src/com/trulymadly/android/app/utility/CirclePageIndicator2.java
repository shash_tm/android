package com.trulymadly.android.app.utility;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.HashMap;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by avin on 19/08/16.
 */
public class CirclePageIndicator2 extends LinearLayout implements ViewPager.OnPageChangeListener{

    private ArrayList<View> mViews;
    private ViewPager mViewPager;
    private HashMap<ITEM_TYPE, ItemTypeModal> mTypeToModalMap;
    private ArrayList<ITEM_TYPE> mItems;
    private int mCurrentSelection = 0;

    public CirclePageIndicator2(Context context) {
        super(context);
        setGravity(Gravity.CENTER_HORIZONTAL);
    }

    public CirclePageIndicator2(Context context, AttributeSet attrs) {
        super(context, attrs);
        setGravity(Gravity.CENTER_HORIZONTAL);
    }

    public CirclePageIndicator2(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setGravity(Gravity.CENTER_HORIZONTAL);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CirclePageIndicator2(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setGravity(Gravity.CENTER_HORIZONTAL);
    }

    public void setViewPager(ViewPager viewPager, HashMap<ITEM_TYPE, ItemTypeModal> mTypeToModalMap,
                             ArrayList<ITEM_TYPE> mItems){
        if(mViewPager != null){
            mViewPager.removeOnPageChangeListener(this);
        }

        if(viewPager == null){
            mViewPager = null;
        }else{
            mCurrentSelection = 0;
            mViewPager = viewPager;
            int currentPosition = mViewPager.getCurrentItem();
            if(currentPosition > 0){
                mCurrentSelection = currentPosition;
            }
            mViewPager.addOnPageChangeListener(this);

            if (mViewPager.getAdapter() == null) {
                throw new IllegalStateException(
                        "ViewPager does not have adapter instance.");
            }

            this.mTypeToModalMap = mTypeToModalMap;
            this.mItems = mItems;
        }

        prepareViews();
        invalidate();
    }

    private void prepareViews(){
        removeAllViews();
        if(mViews == null){
            mViews = new ArrayList<>();
        }else{
            mViews.clear();
        }

        for(int i = 0; i < mItems.size(); i++){
            ITEM_TYPE item_type = mItems.get(i);
//            ImageView view = (ImageView) LayoutInflater.from(getContext()).inflate(
//                    mTypeToModalMap.get(item_type).getmLayoutResourceId(), null);
            ImageView view = (ImageView) LayoutInflater.from(getContext()).inflate(
                    checkNotNull(mTypeToModalMap.get(item_type)).getmLayoutResourceId(), this, false);
            if((mCurrentSelection != i)) {
                view.setImageResource(checkNotNull(mTypeToModalMap.get(item_type)).getmUnselectedResourceId());
            }else {
                view.setImageResource(checkNotNull(mTypeToModalMap.get(item_type)).getmSelectedResourceId());
            }

            mViews.add(view);
            addView(view);
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if(mViews != null && position < mViews.size()){
            ImageView selectedView = (ImageView) mViews.get(mCurrentSelection);
            selectedView.setImageResource(checkNotNull(mTypeToModalMap.get(mItems.get(mCurrentSelection))).getmUnselectedResourceId());

            ImageView view = (ImageView) mViews.get(position);
            view.setImageResource(checkNotNull(mTypeToModalMap.get(mItems.get(position))).getmSelectedResourceId());
            mCurrentSelection = position;
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public enum ITEM_TYPE{
        CIRCLE, CAMERA
    }

    public static class ItemTypeModal{
        private int mLayoutResourceId, mSelectedResourceId, mUnselectedResourceId;

        public ItemTypeModal(int mLayoutResourceId, int mSelectedResourceId, int mUnselectedResourceId) {
            this.mLayoutResourceId = mLayoutResourceId;
            this.mSelectedResourceId = mSelectedResourceId;
            this.mUnselectedResourceId = mUnselectedResourceId;
        }

        public int getmLayoutResourceId() {
            return mLayoutResourceId;
        }

        public void setmLayoutResourceId(int mLayoutResourceId) {
            this.mLayoutResourceId = mLayoutResourceId;
        }

        public int getmSelectedResourceId() {
            return mSelectedResourceId;
        }

        public void setmSelectedResourceId(int mSelectedResourceId) {
            this.mSelectedResourceId = mSelectedResourceId;
        }

        public int getmUnselectedResourceId() {
            return mUnselectedResourceId;
        }

        public void setmUnselectedResourceId(int mUnselectedResourceId) {
            this.mUnselectedResourceId = mUnselectedResourceId;
        }
    }
}
