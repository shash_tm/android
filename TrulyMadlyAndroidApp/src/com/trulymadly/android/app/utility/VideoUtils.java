package com.trulymadly.android.app.utility;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build;

import com.amazonaws.regions.Regions;

import java.util.ArrayList;

/**
 * Created by avin on 28/09/16.
 */
public class VideoUtils {

    public static final int VIDEO_ROTATION_DEFAULT = 0;
    public static final int VIDEO_DURATION_MAX_SECS_DEFAULT = 15;
    public static final int VIDEO_DURATION_MIN_SECS_DEFAULT = 5;
    public static final int VIDEO_BITRATE_DEFAULT = 40000;
    public static final int VIDEO_FRAME_RATE_DEFAULT = 30;
    public static final String VIDEO_SOURCE_CAMERA = "camera";
    public static final String VIDEO_SOURCE_GALLERY = "gallery";
    public static final String VIDEO_SOURCE_DEFAULT = VIDEO_SOURCE_CAMERA;
    public static final String VIDEO_CAMERA_FRONT = "front";
    public static final String VIDEO_CAMERA_BACK = "back";
    public static final String VIDEO_CAMERA_DEFAULT = VIDEO_CAMERA_FRONT;
    public static final int VIDEO_IFRAME_INTERVAL_DEFAULT = 1;
    public static final int VIDEO_UPLOAD_TRANSCODE_PERCENT_LIMIT = 20;
    public static final int VIDEO_UPLOAD_AWS_UPLOAD_PERCENT_LIMIT = 95;
    public static final int VIDEO_DIMENSION_MIN_SIDE = 240;
    public static final String VIDEO_MIME_TYPE_DEFAULT = "video/avc";
    public static final String AUDIO_MIME_TYPE_DEFAULT = "audio/mp4a-latm";
    public static final int AUDIO_SAMPLE_RATE_DEFAULT = 44100;
    public static final int AUDIO_CHANNEL_COUNT_DEFAULT = 1;
    public static final int AUDIO_BIT_RATE_DEFAULT = 96 * 1024;
    public static final Regions AWS_REGION_DEFAULT = Regions.US_EAST_1;
    public static final int AWS_CONNECTION_TIMEOUT_DEFAULT = 60000;
    public static final int AWS_SOCKET_TIMEOUT_DEFAULT = 60000;
    public static final ArrayList<String> UNSUPPORTED_VIDEO_FORMATS;


    static {
        UNSUPPORTED_VIDEO_FORMATS = new ArrayList<>();
        UNSUPPORTED_VIDEO_FORMATS.add("3gp");
        UNSUPPORTED_VIDEO_FORMATS.add("3gpp");
        UNSUPPORTED_VIDEO_FORMATS.add("gif");
        UNSUPPORTED_VIDEO_FORMATS.add("3g2");
    }

    public static int getRotation(String filePath) {
        int degrees = 0;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            retriever.setDataSource(filePath);
            String rotation = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION);
            retriever.release();
            if (rotation != null) {
                degrees = Integer.parseInt(rotation);
            }
        }

        return degrees;
    }

    public static long getVideoDurationMs(String filePath) {
        long durationInSeconds = -1;

        if (Utility.isSet(filePath)) {
            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            retriever.setDataSource(filePath);
            String duration = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            retriever.release();
            if (duration != null) {
                durationInSeconds = Long.parseLong(duration);
            }
        }

        return durationInSeconds;
    }

    public static long getVideoDurationMs(Context context, Uri uri) {
        long durationInMilliSeconds = -1;

        try {
            if (uri != null && Utility.isSet(uri.toString())) {
                MediaMetadataRetriever retriever = new MediaMetadataRetriever();
                retriever.setDataSource(context, uri);
                String duration = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                retriever.release();
                if (duration != null) {
                    durationInMilliSeconds = Long.parseLong(duration);
                }
            }
        } catch (IllegalArgumentException ignored) {
            return durationInMilliSeconds;
        }


        return durationInMilliSeconds;
    }

    public static boolean isVideoFormatSupported(String extension) {
        return Utility.isSet(extension) && !UNSUPPORTED_VIDEO_FORMATS.contains(extension);
    }

    public static VideoDimension getVideoDimensions(String filePath) {
        VideoDimension videoDimension = new VideoDimension(1280, 720);

        MediaMetadataRetriever retreiver = new MediaMetadataRetriever();
        retreiver.setDataSource(filePath);
        String heightString = retreiver.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT);
        String widthString = retreiver.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH);

        if (!Utility.isSet(heightString) || !Utility.isSet(widthString)) {
            Bitmap bmp = retreiver.getFrameAtTime(0, MediaMetadataRetriever.OPTION_CLOSEST);
            if (bmp != null) {
                videoDimension.init(bmp.getHeight(), bmp.getWidth());
            }
        } else {
            videoDimension.init(Integer.parseInt(heightString), Integer.parseInt(widthString));
        }

        retreiver.release();
        return videoDimension;
    }

    public static class VideoDimension {
        public int mHeight, mWidth;

        public VideoDimension(int mHeight, int mWidth) {
            this.mHeight = mHeight;
            this.mWidth = mWidth;
        }

        public void init(int height, int width) {
            mHeight = height;
            mWidth = width;
        }
    }
}
