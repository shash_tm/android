package com.trulymadly.android.app.utility;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;

import java.util.ArrayList;

/**
 * Created by avin on 22/03/16.
 * <p/>
 * TODO : PERMISSIONS: Will replace PermissionsHelper with this class once its ready
 */
public class PermissionHelper {
    private static final int REQUEST_GET_ACCOUNTS_PERMISSIONS_CODE = 101;
    private static final int REQUEST_CAMERA_PERMISSIONS_CODE = 102;
    private static final int REQUEST_STORAGE_PERMISSIONS_CODE = 103;
    private static final int REQUEST_FINE_LOCATION_ACCESS = 104;

    public static PermissionRequestCreator with(Context context) {
        return new PermissionRequestCreator(context);
    }

    private static class PermissionRequestCreator {

        private final Context mContext;
        private ArrayList<String> mPermissions;
        private PermissionsRequestHandler mPermissionsRequestHandler;

        public PermissionRequestCreator(Context context) {
            this.mContext = context;
        }

        private String getPermissionFromCode(int code) {
            switch (code) {
                case REQUEST_GET_ACCOUNTS_PERMISSIONS_CODE:
                    return Manifest.permission.GET_ACCOUNTS;

                case REQUEST_CAMERA_PERMISSIONS_CODE:
                    return Manifest.permission.CAMERA;

                case REQUEST_STORAGE_PERMISSIONS_CODE:
                    return Manifest.permission.WRITE_EXTERNAL_STORAGE;

                case REQUEST_FINE_LOCATION_ACCESS:
                    return Manifest.permission.ACCESS_FINE_LOCATION;
            }

            return null;
        }

        public PermissionRequestCreator add(int permissionCode) {
            if (mPermissions == null) {
                mPermissions = new ArrayList<>();
            }
            String permission = getPermissionFromCode(permissionCode);
            if (!hasPermission(permission)) {
                mPermissions.add(permission);
            }
            return this;
        }

        public PermissionsRequestHandler issueRequest(Activity activity, int requestCode) {
            if(mPermissions != null && mPermissions.size() > 0){
                ActivityCompat.requestPermissions(activity, mPermissions.toArray(new String[mPermissions.size()]), requestCode);
            }
            if(mPermissionsRequestHandler == null){
                mPermissionsRequestHandler = new PermissionsRequestHandler();
            }
            return mPermissionsRequestHandler;
        }

        private void askForPermissions(Activity activity) {
//            ActivityCompat.requestPermissions(activity, mPermissions.toArray(new String[0]), requestCode);
        }

        private boolean hasPermission(int permissionCode) {
            return hasPermission(getPermissionFromCode(permissionCode));
        }

        private boolean hasPermission(String permission) {
            return (PermissionChecker.checkSelfPermission(mContext, permission))
                    == PermissionChecker.PERMISSION_GRANTED;
        }

    }

    public static class PermissionsRequestHandler{
        public void onRequestPermissionRejected(){

        }
    }

}
