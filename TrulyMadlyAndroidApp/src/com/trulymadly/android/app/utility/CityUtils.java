package com.trulymadly.android.app.utility;

import android.app.Activity;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Spinner;

import com.trulymadly.android.app.R;
import com.trulymadly.android.app.adapter.CustomSpinnerArrayAdapter;
import com.trulymadly.android.app.json.ConstantsCities;
import com.trulymadly.android.app.modal.Height;

import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by udbhav on 05/07/16.
 */
public class CityUtils {

    public static HashMap<String, String> getCityAndStateId(String cityString) {
        HashMap<String, String> returnVal = new HashMap<>();
        if (Utility.isSet(cityString)) {
            int index = Arrays.asList(ConstantsCities.cityNames).indexOf(cityString);
            if (index != -1) {
                returnVal.put("cityId", ConstantsCities.cityIds[index]);
                int sum = 0;
                for (int i = 0; i < ConstantsCities.stateFrequency.length; i++) {
                    sum += ConstantsCities.stateFrequency[i];
                    if (sum >= index + 1) {
                        returnVal.put("stateId", ConstantsCities.stateIds[i]);
                        break;
                    }
                }
            } else {
                returnVal.put("cityId", null);
                returnVal.put("stateId", null);
            }
        } else {
            returnVal.put("cityId", null);
            returnVal.put("stateId", null);
        }
        return returnVal;
    }

    public static void createHeightSpinner(Height[] heightArray, Activity aActivity, Spinner heightSpinner, String height, View.OnTouchListener onTouchListener) {
        CustomSpinnerArrayAdapter spinnerHeightAdapter = new CustomSpinnerArrayAdapter(
                aActivity, R.layout.custom_spinner_2, heightArray);
        spinnerHeightAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        heightSpinner.setAdapter(spinnerHeightAdapter);
        heightSpinner.setOnTouchListener(onTouchListener);
        if (Utility.isSet(height)) {
            for (int i = 0; i < heightArray.length; i++) {
                if (heightArray[i].getId().equalsIgnoreCase(height)) {
                    if (spinnerHeightAdapter.getCount() > i)
                        heightSpinner.setSelection(i);
                    break;
                }
            }
        }
    }

    public static boolean hidePopularCities(boolean popularCitiesVisible, View popular_cities_container) {
        if (popularCitiesVisible) {
            popularCitiesVisible = false;
            popular_cities_container.setVisibility(View.GONE);
        }
        return popularCitiesVisible;
    }

    public static boolean showPopularCities(boolean popularCitiesVisible, boolean isCityViewExpanded, Activity aActivity, View popular_cities_container, AutoCompleteTextView cityAutocompleteTv) {
        if (!popularCitiesVisible && isCityViewExpanded && !Utility.isSet(cityAutocompleteTv.getText().toString())) {
            UiUtils.hideKeyBoard(aActivity);
            popular_cities_container.setVisibility(View.VISIBLE);
            popularCitiesVisible = true;
            cityAutocompleteTv.dismissDropDown();
            cityAutocompleteTv.setHint(R.string.enter_city);
        }
        return popularCitiesVisible;
    }

}
