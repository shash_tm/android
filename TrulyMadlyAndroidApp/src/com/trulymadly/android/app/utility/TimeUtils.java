package com.trulymadly.android.app.utility;

import android.content.Context;
import android.support.annotation.NonNull;

import com.crashlytics.android.Crashlytics;
import com.trulymadly.android.app.sqlite.RFHandler;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by udbhav on 10/11/15.
 */
public class TimeUtils {
    public static final String TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final long HOURS_24_IN_MILLIS = 24 * 60 * 60 * 1000;
    private static final String DATE_FORMAT = "dd-MMM-yyyy";
    private static final String LEFT_TIME_FORMAT = "mm:ss";

    /**
     * this expects the format to be "yyyy-MM-dd HH:mm:ss" and return the format
     * to "MMM dd, h:mm a" else it will return the same id
     */
    public static String GMTToMyTime(@NonNull String tstamp) {
        try {
            // http://stackoverflow.com/questions/14853389/how-to-convert-utc-timestamp-to-device-local-time-in-android
            // http://stackoverflow.com/questions/2818086/android-get-current-utc-time
            // http://stackoverflow.com/questions/10725246/java-android-convert-a-gmt-time-string-to-local-time?rq=1

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
                    Locale.ENGLISH);
            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
            Date date = sdf.parse(tstamp);
            Calendar cal = Calendar.getInstance();
            TimeZone tz = cal.getTimeZone();

            SimpleDateFormat sdf_mytime = new SimpleDateFormat(
                    "MMM dd, h:mm a", Locale.ENGLISH);
            sdf_mytime.setTimeZone(tz);

            // sdf.setTimeZone(tz);
            tstamp = sdf_mytime.format(date);

            // 9:00
        } catch (Exception e) {
            Crashlytics.logException(e);

        }
        return tstamp;
    }

    public static String getFormattedTime() {
        return getFormattedTime(Calendar.getInstance().getTime());
    }

    public static String getFormattedTime(Date time) {
        if (time == null) {
            time = Calendar.getInstance().getTime();
        }
        DateFormat sdf = new SimpleDateFormat(TIME_FORMAT, Locale.ENGLISH);
        return sdf.format(time);
    }

    public static String getFormattedLeftTime(long secs) {
        String formattedTime = "";
        if (secs < 60 * 60L) {
            formattedTime = String.format(Locale.ENGLISH, "%02d:%02d", secs / 60L, secs % 60L);
        } else if (secs < 24 * 60 * 60L) {
            formattedTime = String.format(Locale.ENGLISH, "%02d:%02d", secs / (60 * 60L), (secs % (60 * 60)) / 60L);
        } else {
            //TODO : Do we need to handle the time in days
        }
        return formattedTime;
    }

    public static String getMinFormattedLeftTime(long secs) {
        String formattedTime;
        //Less than a minute -> 1 min remaining
        if (secs < 60) {
            formattedTime = String.format(Locale.ENGLISH, "%02d m", 1);
        }
        //In minutes
        else if (secs < 60 * 60L) {
            formattedTime = String.format(Locale.ENGLISH, "%02d m", secs / 60L);
        }
        //In hours
        else if (secs < 24 * 60 * 60L) {
            formattedTime = String.format(Locale.ENGLISH, "%02d h", secs / (60 * 60L));
        }
        //In days
        else {
            formattedTime = String.format(Locale.ENGLISH, "%02d d", secs / (24 * 60 * 60L));
        }
        return formattedTime;
    }

    public static String getFormattedTimeGMT() {
        DateFormat sdf = new SimpleDateFormat(TIME_FORMAT, Locale.ENGLISH);
        sdf.setTimeZone(TimeZone.getTimeZone("gmt"));
        return sdf.format(new Date());
    }

    public static String getFormattedDate(Date time) {
        if (time == null) {
            time = Calendar.getInstance().getTime();
        }
        DateFormat sdf = new SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH);
        return sdf.format(time);
    }

    public static Date getParsedTime(@NonNull String tstamp) {
        DateFormat sdf = new SimpleDateFormat(TIME_FORMAT, Locale.ENGLISH);
        try {
            return sdf.parse(tstamp);
        } catch (ParseException e) {
            Crashlytics.logException(e);
            return null;
        }
    }

    private static long getTimeStamp(@NonNull String time) {
        long timestamp = 0;
        try {
            timestamp = Long.parseLong(time);
        } catch (NumberFormatException ignored) {
            timestamp = getParsedTime(time).getTime();
        }

        return timestamp;
    }

    public static boolean isTimeoutExpired(@NonNull Context ctx, @NonNull String shared_pref_key,
                                           long timeout) {
        String lastTStampString = SPHandler.getString(ctx, shared_pref_key);
        return isTimeoutExpired(lastTStampString, timeout);
    }

    private static boolean isTimeoutExpired(String lastTStampString,
                                            long timeout) {
        if (Utility.isSet(lastTStampString)) {
            long lastTStamp = getTimeStamp(lastTStampString);
            return (getSystemTimeInMiliSeconds() - lastTStamp > timeout);
        }
        return true;
    }

    //Gets the difference between the stored timestamp and the given delay
    public static Long getTimeoutDifference(@NonNull Context ctx, @NonNull String rigid_key,
                                            String userId, long timeout) {
        String lastTStampString = RFHandler.getString(ctx,
                userId, rigid_key);
        if (Utility.isSet(lastTStampString)) {
            long lastTStamp = Long.parseLong(lastTStampString);
            return getTimeoutDifference(lastTStamp, timeout);
        }
        return null;
    }

    public static boolean isTimeoutExpired(long lastTimeStamp, long timeout) {
        Long difference = getTimeoutDifference(lastTimeStamp, timeout);
        return (Math.abs(difference) >= timeout);
    }

    public static long getSystemTimeInMiliSeconds() {
        return Calendar.getInstance().getTimeInMillis();
    }

    public static Long getTimeoutDifference(long lastTStamp, long timeout) {
        return (lastTStamp + timeout - getSystemTimeInMiliSeconds());
    }

    public static boolean isRigidTimeoutExpired(@NonNull Context ctx, @NonNull String rigid_key,
                                                long timeout) {
        String userId = Utility.getMyId(ctx);
        String lastTStampString = RFHandler.getString(ctx, userId, rigid_key);
        return isTimeoutExpired(lastTStampString, timeout);
    }

    public static boolean isDifferenceGreater(@NonNull Context ctx, @NonNull String rigid_key,
                                              long timeout) {
        String lastTStampString = RFHandler.getString(ctx,
                Utility.getMyId(ctx), rigid_key);
        return isDifferenceGreater(lastTStampString, timeout);
    }

    public static boolean isDifferenceGreater(String lastTStampString, long timeout) {
        if (Utility.isSet(lastTStampString)) {
            long lastTStamp = Long.parseLong(lastTStampString);
            long difference = lastTStamp - getSystemTimeInMiliSeconds();
            if (Math.abs(difference) < timeout)
                return false;
        }
        return true;
    }

    private static int getDifferenceInDays(Calendar cal1, Calendar cal2) {
        cal1.set(Calendar.HOUR_OF_DAY, 0);
        cal1.set(Calendar.MINUTE, 0);
        cal1.set(Calendar.SECOND, 0);
        cal1.set(Calendar.MILLISECOND, 0);
        cal2.set(Calendar.HOUR_OF_DAY, 0);
        cal2.set(Calendar.MINUTE, 0);
        cal2.set(Calendar.SECOND, 0);
        cal2.set(Calendar.MILLISECOND, 0);

        return (int) ((cal1.getTimeInMillis() - cal2.getTimeInMillis()) / (1000 * 60 * 60 * 24));
    }

    public static String getFormatteddateForChatItem(String tstamp) {
        try {
            // http://stackoverflow.com/questions/14853389/how-to-convert-utc-timestamp-to-device-local-time-in-android
            // http://stackoverflow.com/questions/2818086/android-get-current-utc-time
            // http://stackoverflow.com/questions/10725246/java-android-convert-a-gmt-time-string-to-local-time?rq=1

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
                    Locale.ENGLISH);
            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
            Date date = sdf.parse(tstamp);
            Calendar cal = Calendar.getInstance();
            TimeZone tz = cal.getTimeZone();

            Calendar now = Calendar.getInstance();
            Calendar then = Calendar.getInstance();
            then.setTime(date);
            int diff = getDifferenceInDays(now, then);
            if (diff == 0) {
                SimpleDateFormat sdf_mytime = new SimpleDateFormat(
                        "h:mm a", Locale.ENGLISH);
                sdf_mytime.setTimeZone(tz);
                tstamp = sdf_mytime.format(date);
            } else if (diff == 1) {
                tstamp = "Yesterday";
            } else if (diff > 1) {
                SimpleDateFormat sdf_mytime = new SimpleDateFormat(
                        "dd/MM", Locale.ENGLISH);
                sdf_mytime.setTimeZone(tz);
                tstamp = sdf_mytime.format(date);
            } else {
                SimpleDateFormat sdf_mytime = new SimpleDateFormat(
                        "MMM dd, h:mm a", Locale.ENGLISH);
                sdf_mytime.setTimeZone(tz);
                tstamp = sdf_mytime.format(date);
            }
            // 9:00
        } catch (Exception e) {
            Crashlytics.logException(e);

        }
        return tstamp;
    }

    public static boolean isDayChanged(long lastTimeStamp) {
        if (lastTimeStamp == -1)
            return false;

        Calendar lastCalendar = Calendar.getInstance();
        lastCalendar.setTimeInMillis(lastTimeStamp);

        Calendar currentCalendar = Calendar.getInstance();
        return lastCalendar.get(Calendar.DAY_OF_MONTH) != currentCalendar.get(Calendar.DAY_OF_MONTH) ||
                lastCalendar.get(Calendar.MONTH) != currentCalendar.get(Calendar.MONTH) ||
                lastCalendar.get(Calendar.YEAR) != currentCalendar.get(Calendar.YEAR);
    }
}
