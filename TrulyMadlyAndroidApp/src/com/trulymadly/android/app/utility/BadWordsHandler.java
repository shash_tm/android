package com.trulymadly.android.app.utility;

import android.app.Activity;
import android.content.Context;
import android.widget.EditText;

import com.crashlytics.android.Crashlytics;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.listener.AlertDialogInterface;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Locale;
import java.util.regex.Pattern;

/**
 * Created by udbhav on 10/11/15.
 */
public class BadWordsHandler {
    private static final String badWordJsonArrayStringFirstTime = "[\"arse\",\"balatkaar\",\"bastard\",\"behenchod\",\"betichod\",\"bhosadi ke\",\"bhosdi\",\"bhus land\",\"billa\",\"blow job\",\"call girl\",\"chut\",\"chutia\",\"chutiya\",\"cocaine seller\",\"drug lord\",\"drug dealer\",\"fucker\",\"gaand\",\"gaandu\",\"gigolo\",\"haramkhor\",\"harami\",\"hooker\",\"housewife\",\"husband\",\"hustler\",\"kuttey\",\"kuttiya\",\"laura\",\"lodu\",\"lund\",\"ma chod\",\"madar chod\",\"madarjaat\",\"per day\",\"per night\",\"per shot\",\"pimp\",\"porn\",\"pornstar\",\"prostitute\",\"randaap\",\"randi\",\"rupees\",\"skank\",\"smuggler\",\"tart\",\"whore\",\"anus\",\"bastard\",\"bitch\",\"boob\",\"cock\",\"cunt\",\"dick\",\"dildo\",\"fuck\",\"gangbang\",\"handjob\",\"homo\",\"penis\",\"piss\",\"pussy\",\"rape\",\"rectum\",\"semen\",\"sex\",\"slut\",\"vagina\"]";

    public static ArrayList<String> getBadWordList(Context aContext) {
        String badWordsJsonArrayString = badWordJsonArrayStringFirstTime;

        JSONArray badWordJsonArray = null;
        try {
            badWordJsonArray = new JSONArray(badWordsJsonArrayString);
        } catch (JSONException e) {
            Crashlytics.logException(e);
        }
//        CustomOkHttpResponseHandler responseHandler = new CustomOkHttpResponseHandler(aContext) {
//
//            @Override
//            public void onRequestSuccess(JSONObject responseJSON) {
//                if (responseJSON.optInt("status") == 200 && responseJSON.optBoolean("success")) {
//                    JSONArray badWordJsonArray = responseJSON.optJSONArray("result");
//                    SPHandler.setString(aContext, ConstantsSP.SHARED_KEYS_BAD_WORD_LIST, badWordJsonArray.toString());
//                    onBadWordListRefreshedInterface.onRefresh(Utility.jsonArrayToStringArrayList(badWordJsonArray));
//                }
//            }
//
//            @Override
//            public void onRequestFailure(Exception exception) {
//                // DO NOTHING
//            }
//        };
//        String url = Constants.BAD_WORD_SHEETSU_API_URL;
//        OkHttpHandler.httpGet(aContext, url, responseHandler);
        if (badWordJsonArray != null) {
            return Utility.jsonArrayToStringArrayList(badWordJsonArray);
        } else {
            return null;
        }
    }

    public static boolean hasBadWord(Activity activity, String str, ArrayList<String> badWordList) {
        return BadWordsHandler.hasBadWord(activity, str, badWordList, 0, null);
    }

    public static boolean hasBadWord(Activity activity, String str, ArrayList<String> badWordList, int badWordText,
                                     final EditText et) {
        boolean hasBadWord = false;
        str = str.trim().toLowerCase(Locale.ENGLISH);
        for (String badWord : badWordList) {
            if (str.contains(badWord.toLowerCase(Locale.ENGLISH))) {
                hasBadWord = true;
                break;
            }
        }
        if (Pattern.matches(".*" + Constants.phoneNoPattern + ".*", str)) {
            hasBadWord = true;
        }
        if (hasBadWord) {
            if (badWordText != 0) {
                AlertsHandler.showAlertDialog(activity, badWordText, new AlertDialogInterface() {
                    @Override
                    public void onButtonSelected() {
                        if (et != null) {
                            et.setText("");
                        }
                    }
                });
            }
        }
        return hasBadWord;

    }
}
