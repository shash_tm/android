package com.trulymadly.android.app.utility;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;

import com.trulymadly.android.app.BuildConfig;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.bus.CancelEvent;
import com.trulymadly.android.app.services.ImageSharingService;

import java.io.File;

/**
 * Created by avin on 03/03/16.
 */
public class ImageUploader {

    public static final int CAMERA_REQUEST_CODE = 1;
    public static final int GALLERY_REQUEST_CODE = 2;
    public static final int PICK_FROM_SYSTEM_REQUEST_CODE = 3;

    public static final String SOURCE_CAMERA = "camera";
    public static final String SOURCE_GALLERY = "gallery";
    public static final String SOURCE_RETRY = "retry";

    public static final long MAX_FILE_SIZE = 1024L * 1024L * 8;

    public static void getImage(Activity activity, IMAGE_SOURCE imageSource, String targetUri) {
        imageSource.startIntent(activity, targetUri);
    }

    public static void onImageSelected(Context context, String uri, String randomMessageId,
                                       String matchId, String source) {
        ImageSharingService.startActionCompressAndSave(context, ImageSharingService.FORMAT_JPEG, uri,
                randomMessageId, matchId, source);
    }

    public static void cancelImageUpload(Context context, String messageId, String matchId) {
        Utility.fireBusEvent(context, true, new CancelEvent(messageId, matchId));
    }

    public enum IMAGE_SOURCE {
        camera, gallery;

        public Intent startIntent(Activity activity, String targetUri) {
            Intent intent = null;
            switch (this) {
                case camera:
//                    if (PermissionsHelper.checkAndAskForPermission(activity, new String[]{Manifest.permission.CAMERA,
//                                    Manifest.permission.WRITE_EXTERNAL_STORAGE},
//                            PermissionsHelper.REQUEST_CAMERA_STORAGE_PERMISSIONS_CODE)) {
                    intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    ComponentName cameraActivity = intent.resolveActivity(activity.getPackageManager());
                    if (cameraActivity != null) {
                        // Continue only if the File was successfully created
                        if (Utility.isSet(targetUri)) {
//                            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(targetUri)));
                            Uri fileUri = FileProvider.getUriForFile(activity, BuildConfig.APPLICATION_ID + ".provider", new File(targetUri));
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                            activity.grantUriPermission(cameraActivity.getPackageName(), fileUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            activity.startActivityForResult(intent, CAMERA_REQUEST_CODE);
                        } else {
                            intent = null;
                        }
                    }
//                    }
                    break;

                case gallery:
                    intent = new Intent(Intent.ACTION_PICK);
                    intent.setType("image/*");
                    //http://stackoverflow.com/questions/9080109/android-image-picker-for-local-files-only
                    intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                    try {
                        activity.startActivityForResult(intent, GALLERY_REQUEST_CODE);
                    } catch (ActivityNotFoundException e) {
                        intent = new Intent();
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        intent.setType("image/*");
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                            String[] mimetypes = {"image/*"};
                            intent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
                        }
                        try {
                            activity.startActivityForResult(intent, PICK_FROM_SYSTEM_REQUEST_CODE);
                        } catch (ActivityNotFoundException activityNotFoundException) {
                            AlertsHandler.showMessage(activity, R.string.unable_to_load_gallery);
                        }
                    } catch (NullPointerException ignored) {
                        //gionee crash
                    }
                    break;
            }

            return intent;
        }
    }

    public static class ImageUploaderConfig {
        private String mParentDirectoryName, mFilename;
        private String mTargetUri;

        public ImageUploaderConfig(String mParentDirectoryName, String mFilename) {
            this.mParentDirectoryName = mParentDirectoryName;
            this.mFilename = mFilename;
        }

        public ImageUploaderConfig(String mParentDirectoryName, String mFilename, String mTargetUri) {
            this.mParentDirectoryName = mParentDirectoryName;
            this.mFilename = mFilename;
            this.mTargetUri = mTargetUri;
        }

        public String getmParentDirectoryName() {
            return mParentDirectoryName;
        }

        public void setmParentDirectoryName(String mParentDirectoryName) {
            this.mParentDirectoryName = mParentDirectoryName;
        }

        public String getmFilename() {
            return mFilename;
        }

        public void setmFilename(String mFilename) {
            this.mFilename = mFilename;
        }

        public String getmTargetUri() {
            return mTargetUri;
        }

        public void setmTargetUri(String mTargetUri) {
            this.mTargetUri = mTargetUri;
        }
    }
}
