/**
 *
 */
package com.trulymadly.android.app.utility;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu.OnCloseListener;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu.OnClosedListener;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu.OnOpenedListener;
import com.squareup.picasso.Picasso;
import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.TrulyMadlyApplication;
import com.trulymadly.android.app.listener.SaveZonesInterface;
import com.trulymadly.android.app.modal.DateSpotZone;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes.filter_clicked;

/**
 * @author udbhav
 */
public class DateSpotPreferenceSlidingMenuHandler implements CompoundButton.OnCheckedChangeListener {

    private static final String ZONE_DOESNT_MATTER_ID = "-1";
    private final SaveZonesInterface saveZonesInterface;
    private final LayoutInflater inflater;
    private final DateSpotZone dateSpotZoneDoesntMatter;
    private final String match_id;
    private final Activity aActivity;
    @BindView(R.id.date_spot_preferences_bg)
    ImageView date_spot_preferences_bg;
    @BindView(R.id.date_spot_preference_zone_container)
    LinearLayout date_spot_preference_zone_container;
    @BindView(R.id.date_spot_preference_city_name_tv)
    TextView date_spot_preference_city_name_tv;
    @BindView(R.id.date_spot_preference_apply)
    View date_spot_preference_apply;
    private SlidingMenu menu = null;
    private ArrayList<String> selectedZoneIds = new ArrayList<>();
    private ArrayList<DateSpotZone> zones;
    private CheckBox defaultZone;

    public DateSpotPreferenceSlidingMenuHandler(Activity act, String match_id, LocationHelper locationHelper) {
        aActivity = act;
        this.match_id = match_id;
        if (act instanceof SaveZonesInterface) {
            this.saveZonesInterface = (SaveZonesInterface) act;
        } else {
            this.saveZonesInterface = null;
        }
        inflater = (LayoutInflater) aActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        dateSpotZoneDoesntMatter = new DateSpotZone();
        dateSpotZoneDoesntMatter.setZoneId(ZONE_DOESNT_MATTER_ID);
        dateSpotZoneDoesntMatter.setName(aActivity.getResources().getString(R.string.doesnt_matter));

        try {
            menu = new SlidingMenu(aActivity);
            menu.setMode(SlidingMenu.RIGHT);
            menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
            menu.setBehindScrollScale(0);
            menu.setBehindOffsetRes(R.dimen.date_spot_preference_right_offset);
            menu.setFadeDegree(.35f);
            menu.setFadeEnabled(true);
            menu.attachToActivity(aActivity, SlidingMenu.SLIDING_CONTENT, true);
            menu.setMenu(R.layout.date_spot_list_preference_menu);
        } catch (InflateException | VerifyError | OutOfMemoryError e) {
            aActivity.finish();
            return;
        }

        ButterKnife.bind(this, menu);
        Picasso.with(aActivity)
                .load(R.drawable.map_blur_bg).noFade().config(Bitmap.Config.RGB_565).into(date_spot_preferences_bg);


        menu.setOnOpenListener(new SlidingMenu.OnOpenListener() {
            @Override
            public void onOpen() {
                TmLogger.d("SLIDING MENU", "On open");
            }
        });

        if (act instanceof OnOpenedListener) {
            menu.setOnOpenedListener((OnOpenedListener) act);
        }
        if (act instanceof OnClosedListener) {
            menu.setOnClosedListener((OnClosedListener) act);
        }
        if (act instanceof OnCloseListener) {
            menu.setOnCloseListener((OnCloseListener) act);
        }
    }


    public SlidingMenu getMenu() {
        return menu;
    }

    public int setZoneData(String cityName, ArrayList<DateSpotZone> zones) {
        this.zones = zones;
        int size = 0;
        if (date_spot_preference_city_name_tv != null)
            date_spot_preference_city_name_tv.setText(cityName);
        if (date_spot_preference_zone_container != null) {
            date_spot_preference_zone_container.removeAllViews();
            selectedZoneIds = new ArrayList<>();

            defaultZone = checkNotNull(addZone(dateSpotZoneDoesntMatter));
            boolean isAnySelected = false;
            for (DateSpotZone zone : zones) {
                addZone(zone);
                if (zone.isSelected()) {
                    if (!selectedZoneIds.contains(zone.getZoneId())) {
                        selectedZoneIds.add(zone.getZoneId());
                    }
                    isAnySelected = true;
                }
            }
            size = selectedZoneIds.size();
            if (!isAnySelected) {
                size = 0;
                checkDefault(true);
            }
        }
        if (date_spot_preference_apply != null) date_spot_preference_apply.setEnabled(false);
        return size;
    }

    private CheckBox addZone(DateSpotZone zone) {

        if (inflater != null && zone != null) {
            CheckBox v = (CheckBox) inflater.inflate(R.layout.date_spot_preference_zone_item, date_spot_preference_zone_container, false);
            v.setChecked(zone.isSelected());
            v.setText(zone.getName());
            v.setTag(zone);
            v.setOnCheckedChangeListener(this);
            date_spot_preference_zone_container.addView(v);
            return v;
        }
        return null;
    }

    /**
     * Called when the checked state of a compound button has changed.
     *
     * @param v         The compound button view whose state has changed.
     * @param isChecked The new checked state of buttonView.
     */
    @Override
    public void onCheckedChanged(CompoundButton v, boolean isChecked) {
        HashMap<String, String> eventInfo = new HashMap<>();
        eventInfo.put("nudge_shown", TrulyMadlyApplication.cdChatTutorialShownInThisSession ? "true" : "false");
        TrulyMadlyTrackEvent.trackEvent(aActivity,
                TrulyMadlyEvent.TrulyMadlyActivities.datespot_list, filter_clicked, 0, match_id
                , eventInfo, true);
        DateSpotZone zone = (DateSpotZone) v.getTag();
        String id = zone.getZoneId();
        if (isChecked) {
            if (!selectedZoneIds.contains(id)) {
                selectedZoneIds.add(id);
            }
            if (ZONE_DOESNT_MATTER_ID.equals(id)) {
                uncheckAllNonDefault();
            } else {
                checkDefault(false);
            }
        } else {
            if (selectedZoneIds.contains(id)) {
                selectedZoneIds.remove(id);
            }
        }
        if (selectedZoneIds.size() == 0) {
            checkDefault(true);
        }
//        if (zones.size() == selectedZoneIds.size()) {
//            uncheckAllNonDefault();
//        }
        date_spot_preference_apply.setEnabled(true);
    }

    private void checkDefault(boolean isChecked) {
        if (selectedZoneIds.contains(ZONE_DOESNT_MATTER_ID)) {
            selectedZoneIds.remove(ZONE_DOESNT_MATTER_ID);
        }
        if (isChecked) {
            selectedZoneIds.add(ZONE_DOESNT_MATTER_ID);
        }
        defaultZone.setChecked(isChecked);
    }

    private void uncheckAllNonDefault() {
        for (DateSpotZone zone : zones) {
            if (selectedZoneIds.contains(zone.getZoneId())) {
                selectedZoneIds.remove(zone.getZoneId());
            }
            ((CheckBox) date_spot_preference_zone_container.findViewWithTag(zone)).setChecked(false);
        }
        if (!selectedZoneIds.contains(ZONE_DOESNT_MATTER_ID)) {
            selectedZoneIds.add(ZONE_DOESNT_MATTER_ID);
            if (!defaultZone.isChecked()) {
                checkDefault(true);
            }
        }
    }

    @OnClick(R.id.date_spot_preference_apply)
    public void onApplyClicked() {
        if (saveZonesInterface != null) {
            saveZonesInterface.onSaveClicked(selectedZoneIds);
        }
    }
}
