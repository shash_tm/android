package com.trulymadly.android.app.utility;

/**
 * Created by avin on 29/10/15.
 */
public class AdsHandler {

//    private final ADS mType;
//    private final Context mContext;
//    private final String mCategory;
//    private AdContainer mAdContainer;
//    private String INMOBI_TAG = "INMOBI";
//    private String mTimeStampKey, mCounterKey;
//    private boolean mAdAlreadyShown = false;
//
//
//    private AdsHandler(Context context, ADS type, ADS_PROVIDER provider,
//                       AdListener adListener, long placementId, String category) {
//        mContext = context;
//        mType = type;
//        mCategory = category;
//
//        mTimeStampKey = ConstantsRF.RIGID_FIELD_LAST_INTERSTITIAL_AD_TIMESTAMP;
//
//        switch (mType) {
//            case conversationItemAd:
//                mTimeStampKey = ConstantsRF.RIGID_FIELD_LAST_NATIVE_AD_TIMESTAMP;
//                break;
//            case interstitialMatchesAd:
//                mCounterKey = ConstantsRF.RIGID_FIELD_INTERSTITIAL_MATCHES_COUNTER;
//                break;
//            case interstitialConversationAd:
////                mCounterKey = ConstantsRF.RIGID_FIELD_INTERSTITIAL_CONVERSATION_COUNTER;
//                break;
//        }
//
//        switch (provider) {
//            case in_mobi:
//                switch (mType) {
//                    case conversationItemAd:
//                    case fullScreenNativeAd:
//                        mAdContainer = new AdInMobiNativeContainer(adListener, placementId);
//                        break;
//                    case interstitialMatchesAd:
//                    case interstitialConversationAd:
//                        mAdContainer = new AdInMobiInterstitialContainer(adListener, placementId);
//                }
//                break;
//            case mo_pub:
//                break;
//        }
//    }
//
//    public static AdsHandler getInstance(Context context, ADS type, ADS_PROVIDER provider,
//                                         AdListener adListener, long placementId, String category) {
//        if (!isAdAllowed(context, type))
//            return null;
//
//        return new AdsHandler(context, type, provider, adListener, placementId, category);
//    }
//
//    public static boolean isAdAllowed(Context context, ADS type) {
//        boolean isAdEnabled = true;
//
//        return (isAdEnabled && isAdTimeStampSatisfied(context, type) && isAdCounterSatisfied(context, type));
//
//    }
//
//    private static boolean isAdTimeStampSatisfied(Context context, ADS type) {
//        //Initialization of keys and default values
//        String lastTimestampKey = ConstantsRF.RIGID_FIELD_LAST_INTERSTITIAL_AD_TIMESTAMP;
//        String timeoutKey = ConstantsRF.RIGID_FIELD_INTERSTITIAL_AD_TIMEINTERVAL;
//        long defaultTimeout = Constants.ADS_INTERSTITIAL_TIMEOUT;
//        String userId = Utility.getMyId(context);
//        if (type == ADS.conversationItemAd) {
//            lastTimestampKey = ConstantsRF.RIGID_FIELD_LAST_NATIVE_AD_TIMESTAMP;
//            defaultTimeout = Constants.ADS_NATIVE_TIMEOUT;
//            timeoutKey = ConstantsRF.RIGID_FIELD_NATIVE_AD_TIMEINTERVAL;
//        }
//        defaultTimeout = -1;
//
//        long timeinterval = RFHandler.getLong(context, userId, timeoutKey, defaultTimeout);
//        if (timeinterval == -1) {
//            return false;
//        } else {
//            if (isDayChanged(context, userId, lastTimestampKey)) {
//                RFHandler.insert(context, userId, ConstantsRF.RIGID_FIELD_INTERSTITIAL_COUNTER, 0);
////                RFHandler.insert(context, userId, ConstantsRF.RIGID_FIELD_INTERSTITIAL_CONVERSATION_COUNTER, 0);
//                RFHandler.insert(context, userId, ConstantsRF.RIGID_FIELD_INTERSTITIAL_MATCHES_COUNTER, 0);
//            }
//        }
//        return TimeUtils.isDifferenceGreater(context, lastTimestampKey, timeinterval);
//    }
//
//    private static boolean isDayChanged(Context context, String userId, String timeStampKey) {
//        long lastTimeStamp = RFHandler.getLong(context, userId, timeStampKey, -1);
//        if (lastTimeStamp == -1)
//            return false;
//
//        Calendar lastCalendar = Calendar.getInstance();
//        lastCalendar.setTimeInMillis(lastTimeStamp);
//
//        Calendar currentCalendar = Calendar.getInstance();
//        return lastCalendar.get(Calendar.DAY_OF_MONTH) != currentCalendar.get(Calendar.DAY_OF_MONTH) ||
//                lastCalendar.get(Calendar.MONTH) != currentCalendar.get(Calendar.MONTH) ||
//                lastCalendar.get(Calendar.YEAR) != currentCalendar.get(Calendar.YEAR);
//    }
//
//    private static boolean isAdCounterSatisfied(Context context, ADS type) {
//        if (type == ADS.conversationItemAd)
//            return true;
//
//        String globalInterstitialUpperCapKey = ConstantsRF.RIGID_FIELD_INTERSTITIAL_UPPER_CAP;
//        String currentInterstitialUpperCapKey = "";
//        String globalInterstitialCounterKey = ConstantsRF.RIGID_FIELD_INTERSTITIAL_COUNTER;
//        String currentInterstitialCounterKey = "";
//        int currentInterstitialUpperCap = -1, currentInterstitialCounter = -1,
//                globalInterstitialUpperCap = -1, globalInterstitialCounter = -1;
//        int defaultCurrentUpperCap = 0;
//        int defaultGlobalUpperCap = Constants.ADS_INTERSTITIAL_UPPER_CAP;
//        String userId = Utility.getMyId(context);
//        switch (type) {
//            case interstitialConversationAd:
////                currentInterstitialUpperCapKey = ConstantsRF.RIGID_FIELD_CONVERSATION_INTERSTITIAL_UPPER_CAP;
////                currentInterstitialCounterKey = ConstantsRF.RIGID_FIELD_INTERSTITIAL_CONVERSATION_COUNTER;
//                defaultCurrentUpperCap = Constants.ADS_CONVERSATION_INTERSTITIAL_UPPER_CAP;
//                break;
//            case interstitialMatchesAd:
//                currentInterstitialUpperCapKey = ConstantsRF.RIGID_FIELD_MATCHES_INTERSTITIAL_UPPER_CAP;
//                currentInterstitialCounterKey = ConstantsRF.RIGID_FIELD_INTERSTITIAL_MATCHES_COUNTER;
//                defaultCurrentUpperCap = Constants.ADS_MATCHES_INTERSTITIAL_UPPER_CAP;
//                break;
//        }
//
//        //default values
//        defaultGlobalUpperCap = 0;
//        defaultCurrentUpperCap = 0;
//
//        currentInterstitialCounter = RFHandler.getInt(context,
//                userId, currentInterstitialCounterKey, 0);
//        globalInterstitialCounter = RFHandler.getInt(context, userId,
//                globalInterstitialCounterKey, 0);
//
//        currentInterstitialUpperCap = RFHandler.getInt(context,
//                userId, currentInterstitialUpperCapKey, defaultCurrentUpperCap);
//        globalInterstitialUpperCap = RFHandler.getInt(context, userId,
//                globalInterstitialUpperCapKey, defaultGlobalUpperCap);
//
//        return globalInterstitialCounter < globalInterstitialUpperCap &&
//                currentInterstitialCounter < currentInterstitialUpperCap;
//
//    }
//
//    public static void resetInterstitialNativeAdsData(Context context) {
//        String userId = Utility.getMyId(context);
//
//        //Resetting all counters
////        RFHandler.insert(context, userId,
////                ConstantsRF.RIGID_FIELD_INTERSTITIAL_CONVERSATION_COUNTER, 0);
//        RFHandler.insert(context, userId,
//                ConstantsRF.RIGID_FIELD_INTERSTITIAL_MATCHES_COUNTER, 0);
//        RFHandler.insert(context, userId,
//                ConstantsRF.RIGID_FIELD_INTERSTITIAL_COUNTER, 0);
//
//        //Resetting timestamp fields
//        RFHandler.insert(context, userId,
//                ConstantsRF.RIGID_FIELD_LAST_INTERSTITIAL_AD_TIMESTAMP, 0L);
//        RFHandler.insert(context, userId,
//                ConstantsRF.RIGID_FIELD_LAST_NATIVE_AD_TIMESTAMP, 0L);
//
//        //Resetting timeinterval fields
//        RFHandler.insert(context, userId,
//                ConstantsRF.RIGID_FIELD_INTERSTITIAL_AD_TIMEINTERVAL, -1);
//        RFHandler.insert(context, userId,
//                ConstantsRF.RIGID_FIELD_NATIVE_AD_TIMEINTERVAL, -1);
//    }
//
//    public void register() {
//        if (mAdContainer != null) {
//            mAdContainer.register();
//        }
//    }
//
//    public void unregister() {
//        if (mAdContainer != null) {
//            mAdContainer.unregister();
//        }
//    }
//
//    public void loadAd() {
//        mAdContainer.loadAd();
//    }
//
//    public void checkAndLoadAd() {
//        if (isAdAllowed(mContext, mType))
//            mAdContainer.loadAd();
//    }
//
//    public void showAd() {
//        mAdContainer.showAd();
//    }
//
//    public void reportImpression(View view) {
//        mAdContainer.reportImpression(view);
//    }
//
//    public void reportImpressionOver(View view) {
//        mAdContainer.reportImpressionOver(view);
//    }
//
//    public void reportClick() {
//        mAdContainer.reportClick();
//    }
//
//    public void reportClickAndOpenUrl() {
//        mAdContainer.reportClickAndOpenUrl();
//    }
//
//    private void adShown(ADS type) {
//        String userId = Utility.getMyId(mContext);
//
//        if (type != ADS.conversationItemAd) {
//            int currentCounter = RFHandler.getInt(mContext, userId, mCounterKey, 0);
//            int globalCounter = RFHandler.getInt(mContext, userId,
//                    ConstantsRF.RIGID_FIELD_INTERSTITIAL_COUNTER, 0);
//            currentCounter++;
//            globalCounter++;
//            RFHandler.insert(mContext, userId, mCounterKey, currentCounter);
//            RFHandler.insert(mContext, userId,
//                    ConstantsRF.RIGID_FIELD_INTERSTITIAL_COUNTER, globalCounter);
//        }
//
//        RFHandler.insert(mContext, userId, mTimeStampKey, System.currentTimeMillis());
//    }
//
//    public enum ADS {
//        fullScreenNativeAd, conversationItemAd, interstitialMatchesAd, interstitialConversationAd
//    }
//
//    public enum ADS_PROVIDER {
//        in_mobi, mo_pub
//    }
//
//    private abstract class AdContainer {
//        AdListener mAdListener;
//
//        public abstract void register();
//
//        public abstract void unregister();
//
//        public abstract void loadAd();
//
//        public abstract void showAd();
//
//        public abstract void reportImpression(View view);
//
//        public abstract void reportImpressionOver(View view);
//
//        public abstract void reportClick();
//
//        public abstract void reportClickAndOpenUrl();
//    }
//
//    private class AdInMobiNativeContainer extends AdContainer {
//        private final String TAG = "AdInMobiNativeContainer";
//        private final InMobiNative mInMobiNative;
//        private InMobiNative mReceivedInMobiNative;
//        private long mPlacementId;
//        private AdNativeModal mAdNativeModal;
//        private int mListItemCounter = 0;
//
//        public AdInMobiNativeContainer(AdListener adListener, long placementId) {
//            mAdListener = adListener;
//            if (placementId == -1)
//                switch (mType) {
//                    case fullScreenNativeAd:
//                        mPlacementId = Constants.INMOBI_PLACEMENT_MATCH_ID;
//                        break;
//                    case conversationItemAd:
//                        mPlacementId = Constants.INMOBI_PLACEMENT_CONVERSATIONS_ITEM_ID;
//                }
//            else {
//                mPlacementId = placementId;
//            }
//            mInMobiNative = new InMobiNative(mPlacementId, new InMobiNative.NativeAdListener() {
//                @Override
//                public void onAdLoadSucceeded(InMobiNative inMobiNative) {
//                    mReceivedInMobiNative = inMobiNative;
//                    try {
//                        JSONObject content = new JSONObject((String) inMobiNative.getAdContent());
//                        switch (mType) {
//                            case fullScreenNativeAd:
//                                mAdNativeModal = AdParser.parseNativeFullScreenAd(content);
//                                break;
//                            case conversationItemAd:
//                                mAdNativeModal = AdParser.parseNativeListItemAd(content);
//                                break;
//                        }
//                        if(mAdNativeModal == null){
//                            mAdListener.onAdLoadFailed();
//                        }else {
//                            mAdListener.onAdLoadSucceeded(mAdNativeModal);
//                        }
//                    } catch (JSONException e) {
//                        Crashlytics.logException(e);
//                    }
//                    TmLogger.d(TAG, "Native Ad loaded successfully");
//                    TrulyMadlyTrackEvent.trackEvent(mContext, mCategory,
//                            TrulyMadlyEvent.TrulyMadlyEventTypes.ad_native, 0,
//                            TrulyMadlyEvent.TrulyMadlyEventStatus.ad_request_success, null, true);
//                }
//
//                @Override
//                public void onAdLoadFailed(InMobiNative inMobiNative, InMobiAdRequestStatus inMobiAdRequestStatus) {
//                    mAdListener.onAdLoadFailed();
//                    TmLogger.d(TAG, "Native Ad load failed : "
//                            + ((inMobiAdRequestStatus != null)?inMobiAdRequestStatus.getMessage():""));
//                    TrulyMadlyTrackEvent.trackEvent(mContext, mCategory,
//                            TrulyMadlyEvent.TrulyMadlyEventTypes.ad_native, 0,
//                            TrulyMadlyEvent.TrulyMadlyEventStatus.ad_request_failure, null, true);
//                }
//
//                @Override
//                public void onAdDismissed(InMobiNative inMobiNative) {
//
//                }
//
//                @Override
//                public void onAdDisplayed(InMobiNative inMobiNative) {
//
//                }
//
//                @Override
//                public void onUserLeftApplication(InMobiNative inMobiNative) {
//
//                }
//            });
//        }
//
//        @Override
//        public void register() {
//            TmLogger.d(TAG, "Native Resuming");
//            mInMobiNative.resume();
//        }
//
//        @Override
//        public void unregister() {
//            TmLogger.d(TAG, "Native Pausing");
//            mInMobiNative.pause();
//        }
//
//        @Override
//        public void loadAd() {
//            TmLogger.d(TAG, "Native Loading ad");
//            mInMobiNative.load();
//            TrulyMadlyTrackEvent.trackEvent(mContext, mCategory,
//                    TrulyMadlyEvent.TrulyMadlyEventTypes.ad_native, 0,
//                    TrulyMadlyEvent.TrulyMadlyEventStatus.ad_request_made, null, true);
//        }
//
//        @Override
//        public void showAd() {
//
//        }
//
//        @Override
//        public void reportImpression(View view) {
////            TmLogger.d(TAG, "Native Calling bind");
//            InMobiNative.bind(view, mReceivedInMobiNative);
//            mListItemCounter++;
//            if ((mListItemCounter == 1 && mType == ADS.conversationItemAd) ||
//                    mType == ADS.fullScreenNativeAd) {
//                adShown(mType);
//                TrulyMadlyTrackEvent.trackEvent(mContext, mCategory,
//                        TrulyMadlyEvent.TrulyMadlyEventTypes.ad_native, 0,
//                        TrulyMadlyEvent.TrulyMadlyEventStatus.ad_shown, null, true);
//            }
//        }
//
//        @Override
//        public void reportImpressionOver(View view) {
////            TmLogger.d(TAG, "Native Calling unbind");
//            InMobiNative.unbind(view);
//        }
//
//        @Override
//        public void reportClick() {
//            if(mReceivedInMobiNative == null)
//                return;
//
//            mReceivedInMobiNative.reportAdClick(null);
//            TrulyMadlyTrackEvent.trackEvent(mContext, mCategory,
//                    TrulyMadlyEvent.TrulyMadlyEventTypes.ad_native, 0,
//                    TrulyMadlyEvent.TrulyMadlyEventStatus.ad_clicked, null, true);
//        }
//
//        @Override
//        public void reportClickAndOpenUrl() {
//            if(mReceivedInMobiNative == null)
//                return;
//
//            mReceivedInMobiNative.reportAdClickAndOpenLandingPage(null);
//        }
//
//        private void openUrl() {
//            if (mAdNativeModal.getmLandingUrl() != null) {
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(mAdNativeModal.getmLandingUrl()));
//                browserIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                mContext.startActivity(browserIntent);
//            }
//        }
//    }
//
//    private class AdInMobiInterstitialContainer extends AdContainer {
//        private final String TAG = "AdInMobiInterstitialContainer";
//        private final InMobiInterstitial mInMobiInterstitial;
//        private final long mPlacementId;
//        private InMobiInterstitial mReceivedInMobiInterstitial;
//
//        public AdInMobiInterstitialContainer(AdListener adListener, long placementId) {
//            mAdListener = adListener;
//            mPlacementId = placementId;
//            mInMobiInterstitial = new InMobiInterstitial(mContext, mPlacementId, new InMobiInterstitial.InterstitialAdListener() {
//                @Override
//                public void onAdRewardActionCompleted(InMobiInterstitial inMobiInterstitial, Map<Object, Object> map) {
//
//                }
//
//                @Override
//                public void onAdDisplayed(InMobiInterstitial inMobiInterstitial) {
//                    TmLogger.d(TAG, "Interstitial Ad Displayed");
//                    adShown(mType);
//                    TrulyMadlyTrackEvent.trackEvent(mContext, mCategory,
//                            TrulyMadlyEvent.TrulyMadlyEventTypes.ad_interstitial, 0,
//                            TrulyMadlyEvent.TrulyMadlyEventStatus.ad_shown, null, true);
//                }
//
//                @Override
//                public void onAdDismissed(InMobiInterstitial inMobiInterstitial) {
//                    TmLogger.d(TAG, "Interstitial Ad Dismissed");
//                    mAdListener.onAdLoadDismissed();
//                    TrulyMadlyTrackEvent.trackEvent(mContext, mCategory,
//                            TrulyMadlyEvent.TrulyMadlyEventTypes.ad_interstitial, 0,
//                            TrulyMadlyEvent.TrulyMadlyEventStatus.ad_closed, null, true);
//                }
//
//                @Override
//                public void onAdInteraction(InMobiInterstitial inMobiInterstitial, Map<Object, Object> map) {
//                    TrulyMadlyTrackEvent.trackEvent(mContext, mCategory,
//                            TrulyMadlyEvent.TrulyMadlyEventTypes.ad_interstitial, 0,
//                            TrulyMadlyEvent.TrulyMadlyEventStatus.ad_interacted, null, true);
//                }
//
//                @Override
//                public void onAdLoadSucceeded(InMobiInterstitial inMobiInterstitial) {
//                    TmLogger.d(TAG, "Interstitial Ad Load Success");
//                    mReceivedInMobiInterstitial = inMobiInterstitial;
//                    mAdListener.onAdLoadSucceeded(null);
//
//                    TrulyMadlyTrackEvent.trackEvent(mContext, mCategory,
//                            TrulyMadlyEvent.TrulyMadlyEventTypes.ad_interstitial, 0,
//                            TrulyMadlyEvent.TrulyMadlyEventStatus.ad_request_success, null, true);
//                }
//
//                @Override
//                public void onAdLoadFailed(InMobiInterstitial inMobiInterstitial, InMobiAdRequestStatus inMobiAdRequestStatus) {
//                    mAdListener.onAdLoadFailed();
//                    TmLogger.d(TAG, "Interstitial Ad Load Failed : "
//                            + ((inMobiAdRequestStatus != null)?inMobiAdRequestStatus.getMessage():""));
//                    TrulyMadlyTrackEvent.trackEvent(mContext, mCategory,
//                            TrulyMadlyEvent.TrulyMadlyEventTypes.ad_interstitial, 0,
//                            TrulyMadlyEvent.TrulyMadlyEventStatus.ad_request_failure, null, true);
//                }
//
//                @Override
//                public void onUserLeftApplication(InMobiInterstitial inMobiInterstitial) {
//
//                }
//            });
//        }
//
//        @Override
//        public void register() {
//
//        }
//
//        @Override
//        public void unregister() {
//
//        }
//
//        @Override
//        public void loadAd() {
//            TmLogger.d(TAG, "Interstitial Requesting Ad Load");
//            mInMobiInterstitial.load();
//            TrulyMadlyTrackEvent.trackEvent(mContext, mCategory,
//                    TrulyMadlyEvent.TrulyMadlyEventTypes.ad_interstitial, 0,
//                    TrulyMadlyEvent.TrulyMadlyEventStatus.ad_request_made, null, true);
//        }
//
//        @Override
//        public void showAd() {
//            if (isAdAllowed(mContext, mType) && mReceivedInMobiInterstitial != null && mReceivedInMobiInterstitial.isReady())
//                mReceivedInMobiInterstitial.show(R.anim.ads_slide_up, R.anim.ads_slide_down);
//        }
//
//        @Override
//        public void reportImpression(View view) {
//
//        }
//
//        @Override
//        public void reportImpressionOver(View view) {
//
//        }
//
//        @Override
//        public void reportClick() {
//
//        }
//
//        @Override
//        public void reportClickAndOpenUrl() {
//
//        }
//    }

}
