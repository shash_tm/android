package com.trulymadly.android.app.utility;

import android.content.Context;

import com.moe.pushlibrary.MoEHelper;
import com.trulymadly.android.app.json.ConstantsSP;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by avin on 07/09/16.
 */
public class MoEHandler {

    public static void trackEvent(Context context, String eventName, JSONObject parameters){
        if(parameters == null){
            parameters = new JSONObject();
        }
        MoEHelper.getInstance(context).trackEvent(eventName, parameters);
    }

    public static void trackEvent(Context context, String eventName){
        JSONObject newJson = new JSONObject();
        addBasicParameters(context, newJson);
        trackEvent(context, eventName, newJson);
    }

    public static void trackEvent(Context context, String eventName, JSONObject parameters,
                                  boolean addBasicParameters){
        if(addBasicParameters) {
            if (parameters == null) {
                parameters = new JSONObject();
            }
            addBasicParameters(context, parameters);
        }
        trackEvent(context, eventName, parameters);
    }

    private static void addBasicParameters(Context context, JSONObject parameters){
        if(parameters != null){
            try {
                String gender = SPHandler.getString(context, ConstantsSP.SHARED_KEYS_USER_GENDER);
                if(Utility.isSet(gender)) {
                    parameters.put("Gender", gender);
                }

                String userId = Utility.getMyId(context);
                if(Utility.isSet(userId)) {
                    parameters.put("User ID", userId);
                }

                String name = SPHandler.getString(context, ConstantsSP.SHARED_KEYS_USER_NAME);
                if(Utility.isSet(name)) {
                    parameters.put("User Name", name);
                }

                String age = SPHandler.getString(context, ConstantsSP.SHARED_KEYS_USER_AGE);
                if(Utility.isSet(age)) {
                    parameters.put("Age", age);
                    try {
                        parameters.put("int_age", Integer.parseInt(age));
                    } catch (Exception e) {

                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public class Events {
        public static final String CONV_LIST_LAUNCHED = "Conversation List Launched";
        public static final String EDU_SCREEN_LAUNCHED = "Education Screen Launched";
        public static final String INTERESTS_SCREEN_LAUNCHED = "Interest Screen Launched";
        public static final String PROFESSION_SCREEN_LAUNCHED = "Profession Screen Launched";
        public static final String BASICS_SAVE = "Basics Save";
        public static final String HASHTAGS_SAVE = "Hashtag Save";
        public static final String USER_HASHTAGS = "user hashtags";
        public static final String LOGIN_SUCCESS = "Login Successful";
        public static final String NEW_USER = "New User";
        public static final String SPARK_ICON_CLICKED = "Spark Icon Clicked";
        public static final String SPARK_PAGE_OPENED = "Spark Page Opened";
        public static final String MATCHES_LAUNCHED = "Matches Launched";
        public static final String DEACTIVATE = "Deactivate";
        public static final String ENDORSEMENT_CLICK = "Endorsement Click";
        public static final String PHONE_VERIFICATION = "Phone Verification";
        public static final String USER_PHOTOS_LAUNCHED = "User Photos Launched";
        public static final String PURCHASE_NOT_COMPLETED = "Purchase not completed";
        public static final String SPARK_SENT_WITH_MSG_USER = "Spark sent with message user";
        public static final String SPARK_SENT_WITH_MSG_DEFAULT = "Spark sent with message default";
        public static final String CLICKED_ON_A_PACKAGE = "Clicked on a package";
        public static final String PURCHASE_COMPLETED = "Purchase completed";
        public static final String SPARK_SIDE_MENU_CLICKED = "Spark Side Menu Clicked";
        public static final String SPARK_RECEIVED = "Spark Received";
        public static final String SPARK_REJECTED = "Spark Rejected";
        public static final String SPARK_ACCEPTED = "Spark Accepted";
        public static final String BUY_PAGE_OPENED = "Buy Page Opened";
        public static final String MESSAGE_SENT = "Message Sent";
        public static final String MESSAGE_RECEIVED = "Message Received";
    }
}
