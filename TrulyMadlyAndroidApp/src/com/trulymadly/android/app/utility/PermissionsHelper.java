package com.trulymadly.android.app.utility;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;

import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.listener.ConfirmDialogInterface;

import java.util.ArrayList;

public class PermissionsHelper {
	public static final int REQUEST_GET_ACCOUNTS_PERMISSIONS_CODE = 101;
	public static final int REQUEST_CAMERA_PERMISSIONS_CODE = 102;
	public static final int REQUEST_STORAGE_PERMISSIONS_CODE = 103;
	public static final int REQUEST_CAMERA_STORAGE_PERMISSIONS_CODE = 104;
	public static final int REQUEST_FINE_LOCATION_ACCESS = 105;
	public static final int REQUEST_VIDEO_PERMISSIONS = 106;


	// Not have any permission listener associated with it
	//It is being used in MatchesPageLatest2 when we ask for permissions on the page launch.
	public static final int INITIAL_STORAGE_PERMISSION_CODE = 201;

	public static boolean hasPermission(@NonNull Activity activity, @NonNull String permission){
		return (PermissionChecker.checkSelfPermission(activity, permission)) == PermissionChecker.PERMISSION_GRANTED;
	}
	
	public static boolean checkAndAskForPermission(@NonNull Activity activity, @NonNull String permission, int requestCode){
		if(hasPermission(activity, permission))
			return true;
		ActivityCompat.requestPermissions(activity, new String[]{permission}, requestCode);
		if(requestCode == INITIAL_STORAGE_PERMISSION_CODE){
			TrulyMadlyTrackEvent.trackEvent(activity, TrulyMadlyEvent.TrulyMadlyActivities.conversation_list,
					TrulyMadlyEvent.TrulyMadlyEventTypes.permission, 0,
					TrulyMadlyEvent.TrulyMadlyEventStatus.shown, null, false);
		}
		return false;
	}
	
	public static boolean checkAndAskForPermission(@NonNull Activity activity, @NonNull String[] permissions, int requestCode){
		ArrayList<String> permissionsToAskList = new ArrayList<>();
		for(String permission : permissions){
			if(!hasPermission(activity, permission))
				permissionsToAskList.add(permission);
		}
		if(permissionsToAskList.size() > 0){
			ActivityCompat.requestPermissions(activity, permissionsToAskList.toArray(new String[0]), requestCode);
			return false;
		}
		else
			return true;
	}

    public static void handlePermissionDenied(@NonNull final Activity activity, int messageId){
		handlePermissionDeniedWithDialog(activity, messageId);
	}

	private static void handlePermissionDeniedWithDialog(@NonNull final Activity activity, int messageId) {
		AlertsHandler.showConfirmDialog(activity, messageId, R.string.go_to_settings, R.string.not_now, new ConfirmDialogInterface() {

            @Override
            public void onPositiveButtonSelected() {
                ActivityHandler.openAppSettings(activity);
            }

            @Override
            public void onNegativeButtonSelected() {

            }
        }, false);
	}
}
