package com.trulymadly.android.app.utility;

import com.trulymadly.android.app.json.ConstantsCities;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Map;

/**
 * Created by udbhav on 25/02/16.
 */
public class CityHandler {
    private final String[] allCities;

    public CityHandler() {
        this.allCities = ConstantsCities.cityNames;
    }

    public String getCities(Map<String, String> params) {
        boolean currentTextAdded = false;
        String text = params.get("text");
        if (text.charAt(0) == '#') {
            text = text.substring(1);
        }
//        text = "#" + text;

        ArrayList<String> filteredHashtags = new ArrayList<>();
        for (String string : allCities) {
            if (string.toLowerCase(Locale.ENGLISH).startsWith(text.toLowerCase(Locale.ENGLISH))) {
                if (string.equalsIgnoreCase(text)) {
                    filteredHashtags.add(0, string);
//                    currentTextAdded = true;
                } else {
                    filteredHashtags.add(string);
                }
            }
        }
//        if (!currentTextAdded) {
//            filteredHashtags.add(0, text);
//        }
        return "{responseCode: 200 , cities: " + Utility.stringArrayListToJsonArray(filteredHashtags).toString() + "}";
    }

    private String[] getFilteredCities(String filter) {
        ArrayList<String> filteredCities = new ArrayList<>();
        for (String string : allCities) {
            if (string.toLowerCase(Locale.ENGLISH).startsWith(filter.toLowerCase(Locale.ENGLISH))) {
                if (string.equalsIgnoreCase(filter)) {
                    filteredCities.add(0, string);
                } else {
                    filteredCities.add(string);
                }
            }
        }
        //TODO: CITYLIST: sort filtered cities here
        //TODO: CITYLIST: move popular cities at the top of the sorted list
        String[] citiesArr = new String[filteredCities.size()];
        citiesArr = filteredCities.toArray(citiesArr);
        return citiesArr;
    }

}

