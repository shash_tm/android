package com.trulymadly.android.app.utility;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.trulymadly.android.app.R;
import com.trulymadly.android.app.json.ConstantsSP;

/**
 * Created by udbhav on 10/11/15.
 */
public class UiUtils {
    private static final int ANIMATION_DURATION = 500; // in millisec
    private static final int ANIMATION_FROM_MENU_DURATION = 750; // in millisec

    public static void hideKeyBoard(@NonNull Context paramContext) {
        if (paramContext != null) {
            InputMethodManager localInputMethodManager = (InputMethodManager) paramContext
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            if (((Activity) paramContext).getCurrentFocus() != null)
                localInputMethodManager.hideSoftInputFromWindow(
                        ((Activity) paramContext).getCurrentFocus()
                                .getWindowToken(), 0);
        }
    }

    public static void showKeyBoard(@NonNull Activity aActivity) {
        if (aActivity != null) {
            InputMethodManager localInputMethodManager = (InputMethodManager) aActivity
                    .getSystemService(Context.INPUT_METHOD_SERVICE);

            localInputMethodManager.showSoftInput(
                    aActivity.getCurrentFocus(),
                    InputMethodManager.SHOW_FORCED);

        }

    }

    private static ProgressDialog showProgressBar(@NonNull Context aContext,
                                                  ProgressDialog mProgressDialog, boolean setCancelable,
                                                  int message_res_id) {
        // mProgressDialog = hideProgressBar(mProgressDialog);
        TextView msgView;
        if (aContext != null) {
            if (mProgressDialog == null) {
                mProgressDialog = new ProgressDialog(aContext);
            }
            try {
                if (!mProgressDialog.isShowing()
                        && !((Activity) aContext).isFinishing()) {
                    mProgressDialog.show();
                    mProgressDialog.setCancelable(setCancelable);
                    mProgressDialog.setContentView(R.layout.progressdialog);
                    mProgressDialog.getWindow().setBackgroundDrawableResource(
                            R.color.transparent);
                    msgView = ((TextView) mProgressDialog
                            .findViewById(R.id.message_text));
                    if (message_res_id != 0) {
                        msgView.setVisibility(View.VISIBLE);
                        msgView.setText(message_res_id);
                    } else {
                        msgView.setVisibility(View.GONE);
                    }
                }
            } catch (Exception | OutOfMemoryError ignored) {
            }
        }
        return mProgressDialog;

    }

    public static ProgressDialog showProgressBar(@NonNull Context aContext,
                                                 ProgressDialog mProgressDialog) {
        return showProgressBar(aContext, mProgressDialog, false, 0);
    }

    public static ProgressDialog showProgressBar(@NonNull Context aContext,
                                                 ProgressDialog mProgressDialog, boolean setCancelable) {
        return showProgressBar(aContext, mProgressDialog,
                setCancelable, 0);
    }

    public static ProgressDialog showProgressBar(@NonNull Context aContext,
                                                 ProgressDialog mProgressDialog, int message_res_id) {
        return showProgressBar(aContext, mProgressDialog, false,
                message_res_id);
    }

    public static ProgressDialog hideProgressBar(ProgressDialog mProgressDialog) {

        try {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
        } catch (final Exception ignored) {
        } finally {
            mProgressDialog = null;
        }
        return mProgressDialog;
    }

    public static Animation inFromLeftAnimation() {
        Animation inFromLeft = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF,
                0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, 0.0f);
        inFromLeft.setDuration(ANIMATION_DURATION);
        inFromLeft.setFillAfter(true);
        return inFromLeft;
    }

    public static Animation outToRightAnimation() {
        Animation outtoRight = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
                +1.0f, Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, 0.0f);
        outtoRight.setDuration(ANIMATION_DURATION);
        outtoRight.setInterpolator(new AccelerateInterpolator());
        return outtoRight;
    }

    public static Animation inFromRightAnimation() {
        Animation inFromRight = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, +1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        inFromRight.setDuration(ANIMATION_DURATION);
        inFromRight.setInterpolator(new AccelerateInterpolator());
        return inFromRight;
    }

    public static Animation outToLeftAnimation() {
        Animation outtoLeft = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, -1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        outtoLeft.setDuration(ANIMATION_DURATION);
        outtoLeft.setFillAfter(true);
        return outtoLeft;
    }

    public static Animation inFromRightAnimationFromMenu() {
        Animation inFromRight = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, +1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        inFromRight.setDuration(ANIMATION_FROM_MENU_DURATION);
        inFromRight.setInterpolator(new AccelerateInterpolator());
        return inFromRight;
    }

    public static Animation outToLeftAnimationFromMenu() {
        Animation outtoLeft = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, -1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        outtoLeft.setDuration(ANIMATION_FROM_MENU_DURATION);
        outtoLeft.setInterpolator(new AccelerateInterpolator());
        return outtoLeft;
    }

    public static Animation fadeOut() {
        Animation animation = new AlphaAnimation(1.0f, 0.0f);
        animation.setDuration(ANIMATION_DURATION);
        return animation;
    }

    public static Animation fadeIn() {
        Animation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(ANIMATION_DURATION);
        return animation;
    }

    public static double getScreenHeight(@NonNull Activity activity) {
        // int measuredWidth = 0;
        int measuredHeight = 0;
        Point size = new Point();
        WindowManager w = activity.getWindowManager();

        w.getDefaultDisplay().getSize(size);
        // measuredWidth = size.x;
        measuredHeight = size.y;
        return measuredHeight;
    }

    public static double getScreenWidth(@NonNull Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float density = Resources.getSystem().getDisplayMetrics().density;
        float dpHeight = outMetrics.heightPixels / density;
        float dpWidth = outMetrics.widthPixels / density;

        return outMetrics.widthPixels;

        // int measuredHeight = 0;
        // int measuredWidth = 0;
        // Point size = new Point();
        // WindowManager w = activity.getWindowManager();
        //
        // w.getDefaultDisplay().getSize(size);
        // measuredWidth = size.x;
        // measuredHeight = size.y;
        // return measuredWidth;
    }

    public static void setRadioButtonCheckedByValue(@NonNull RadioGroup radioGroup,
                                                    String value) {
        if (value != null) {
            int len = radioGroup.getChildCount();
            for (int i = 0; i < len; i++) {
                if (radioGroup.getChildAt(i) instanceof RadioButton) {
                    if (((RadioButton) radioGroup.getChildAt(i)).getText()
                            .toString().equalsIgnoreCase(value)) {
                        ((RadioButton) radioGroup.getChildAt(i))
                                .setChecked(true);
                        break;
                    }
                }
            }
        }
    }

    public static void setBackground(@NonNull Context aContext, View v, int drawableId) {
        if (v != null) {
            try {
                v.setBackground(getDrawable(aContext, drawableId));
            } catch (OutOfMemoryError ignore) {
            }
        }
    }

    public static void setBackgroundDrawable(View v, Drawable drawable) {
        if (v != null && drawable != null) {
            v.setBackground(drawable);
        }
    }

    @SuppressWarnings("deprecation")
    @TargetApi(Build.VERSION_CODES.LOLLIPOP_MR1)
    private static Drawable getDrawable(@NonNull Context aContext, int drawableId) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1) {
            return aContext.getResources().getDrawable(drawableId);
        } else {
            return aContext.getResources().getDrawable(drawableId, null);
        }

    }


    @SuppressWarnings("deprecation")
    @TargetApi(Build.VERSION_CODES.M)
    public static void setBackgroundColor(@NonNull Context aContext, View view, int colorId) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            view.setBackgroundColor(aContext.getResources().getColor(colorId));
        } else {
            view.setBackgroundColor(aContext.getColor(colorId));
        }

    }

    public static void setAlpha(@NonNull View v, float alpha) {
        AlphaAnimation animation = new AlphaAnimation(alpha, alpha);
        animation.setFillAfter(true);
        v.startAnimation(animation);
    }

    /**
     * return density of the device so that I can call the stickers accordingly
     *
     * @param context
     * @return
     */
    public static String getDeviceDensity(@NonNull Context context) {

        float density = context.getResources().getDisplayMetrics().density;
        if (density >= 1.5) {
            return "hdpi";
        }
        return "mdpi";
    }


    public static Boolean isXxhdpi(@NonNull Context context) {


        float density = context.getResources().getDisplayMetrics().density;
        return density >= 3.0;

    }

    public static void removeAlpha(@NonNull View v) {
        v.clearAnimation();
    }

    public static Uri resIdToUri(@NonNull Context context, int resId) {
        return Uri.parse("android.resource://" + context.getPackageName() + "/"
                + resId);
    }

    public static int dpToPx(int dp) {
        return (int) ((dp * Resources.getSystem().getDisplayMetrics().density) + 0.5f);
    }

    public static Point getLocationInView(View src, View target) {
        final int[] l0 = new int[2];
        src.getLocationOnScreen(l0);

        final int[] l1 = new int[2];
        target.getLocationOnScreen(l1);

        l1[0] = l1[0] - l0[0] + target.getWidth() / 2;
        l1[1] = l1[1] - l0[1] + target.getHeight() / 2;

        return new Point(l1[0], l1[1]);
    }

    public static int getProfileDummyImageForMatch(Context context) {
        String gender = SPHandler.getString(context, ConstantsSP.SHARED_KEYS_USER_GENDER);
        boolean isMale = Utility.stringCompare(gender, "m");
        int dummyImage = R.drawable.dummy_male_circular;
        if (isMale) {
            dummyImage = R.drawable.dummy_female_circular;
        }

        return dummyImage;
    }

    public static void slideUpAndBounceDown(final View view, final int repeatFrequency) {
        if (repeatFrequency <= 0) {
            return;
        }
        float distance = -0.5f;
        final TranslateAnimation slideDown = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0,
                Animation.RELATIVE_TO_SELF, 0,
                Animation.RELATIVE_TO_SELF, distance,
                Animation.RELATIVE_TO_SELF, 0
        );
        slideDown.setDuration(218);
        slideDown.setInterpolator(new AccelerateInterpolator());
        slideDown.setFillAfter(true);
        slideDown.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                slideUpAndBounceDown(view, repeatFrequency - 1);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        TranslateAnimation slideUp = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0,
                Animation.RELATIVE_TO_SELF, 0,
                Animation.RELATIVE_TO_SELF, 0,
                Animation.RELATIVE_TO_SELF, distance
        );
        slideUp.setDuration(218);
        slideUp.setInterpolator(new DecelerateInterpolator());
        slideUp.setFillAfter(true);
        slideUp.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.startAnimation(slideDown);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        view.startAnimation(slideUp);


    }

    public static void doRevealAnimationOnView(View view, boolean toReveal, int centerX, int centerY) {
        doRevealAnimationOnView(view, toReveal, centerX, centerY, (float) Math.hypot(centerX, centerY));
    }

    public static void doRevealAnimationOnView(final View view, boolean toReveal, int centerX, int centerY, float radius) {
        if (toReveal) {
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
                Animator anim = ViewAnimationUtils.createCircularReveal(view, centerX, centerY, 0, radius);
                view.setVisibility(View.VISIBLE);
                anim.setDuration(218);
                anim.start();
            } else {
                view.setVisibility(View.VISIBLE);
            }

        } else {
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
                Animator anim = ViewAnimationUtils.createCircularReveal(view, centerX, centerY, radius, 0);
                anim.setDuration(218);
                anim.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        view.setVisibility(View.GONE);
                    }
                });
                anim.start();
            } else {
                view.setVisibility(View.GONE);
            }
        }
    }
}
