package com.trulymadly.android.app.utility;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by avin on 13/05/16.
 */
public class SparkTimer {
    private Timer mTimer;
    private TimerTask mTimerTask;
    private int mIntervalInSeconds = 1;
    private long mStartedTime = 0;
    private long mTimeout = 0;
    private long mTimeLeftInSeconds = 0;
    private long mElapsedTimeInSeconds = 0;
    private SparkTimerInterface mSparkTimerInterface;
    private boolean isTaskRunning = false;


    public SparkTimer(int mIntervalInSeconds, long mStartedTime,
                      long mTimeout) {
        resetValues(mIntervalInSeconds, mStartedTime, mTimeout);
    }

    public void resetValues(int mIntervalInSeconds, long mStartedTime,
                             long mTimeout){
        this.mIntervalInSeconds = mIntervalInSeconds;
        this.mStartedTime = mStartedTime;
        this.mTimeout = mTimeout;
        mTimeLeftInSeconds = TimeUtils.getTimeoutDifference(mStartedTime, mTimeout)/1000;
        mElapsedTimeInSeconds = 0;
    }

    public int getElapsedTimeWrtStartTime(){
        return (int) ((mTimeout/1000) - mTimeLeftInSeconds + mElapsedTimeInSeconds);
    }

    private void initialize(){
        mTimer = new Timer();
        mTimerTask = new TimerTask() {
            @Override
            public void run() {
                doSomething();
            }
        };
        mTimeLeftInSeconds = TimeUtils.getTimeoutDifference(mStartedTime, mTimeout)/1000;
        mElapsedTimeInSeconds = 0;
    }

    private void doSomething(){
        mElapsedTimeInSeconds += mIntervalInSeconds;
        if(mSparkTimerInterface != null){
            if(mElapsedTimeInSeconds >= mTimeLeftInSeconds){
                mSparkTimerInterface.onTimeExpired();
                stop();
            }else {
                mSparkTimerInterface.onTimeUpdated((int) ((mTimeout/1000) - mTimeLeftInSeconds + mElapsedTimeInSeconds));
            }
        }
    }

    public void start(SparkTimerInterface mSparkTimerInterface){
        if(!isTaskRunning) {
            isTaskRunning = true;
            this.mSparkTimerInterface = mSparkTimerInterface;
            initialize();
            mTimer.schedule(mTimerTask, 0, mIntervalInSeconds*1000);
        }
    }

    public void forceStart(SparkTimerInterface mSparkTimerInterface){
        stop();
        start(mSparkTimerInterface);
    }

    public void stop(){
        isTaskRunning = false;
        this.mSparkTimerInterface = null;
        if(mTimer != null) {
            mTimer.cancel();
            mTimer.purge();
        }
    }

    public boolean isRunning(){
        return isTaskRunning;
    }

    public interface SparkTimerInterface{
        void onTimeUpdated(int mElapsedTimeInSeconds);
        void onTimeExpired();
    }
}
