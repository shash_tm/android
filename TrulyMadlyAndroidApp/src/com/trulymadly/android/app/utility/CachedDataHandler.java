package com.trulymadly.android.app.utility;

import android.content.Context;

import com.trulymadly.android.app.listener.CachedDataInterface;
import com.trulymadly.android.app.listener.CustomOkHttpResponseHandler;
import com.trulymadly.android.app.sqlite.CachingDBHandler;

import org.json.JSONObject;

import java.util.Map;

public class CachedDataHandler {


    private final boolean showFailure;
    private final CustomOkHttpResponseHandler okHttpResponseHandler;

    private CachedDataHandler(final String url, final CachedDataInterface callback, final Context aContext, String activity,
                              String event_type, final String event_status, Map<String, String> event_info, boolean showfailure) {

        this.showFailure = showfailure;
        this.okHttpResponseHandler = new CustomOkHttpResponseHandler(
                aContext, activity, event_type, event_status, event_info) {

            @Override
            public void onRequestSuccess(JSONObject response) {
                int responseCode = response.optInt("responseCode");
                if (responseCode != 304) {
                    //update hash
                    CachingDBHandler.insertResponse(aContext, url, response.optString("hash"), response.toString(), Utility.getMyId(aContext), response.optString("tstamp"));
                    callback.onSuccess(response);
                } else {
                    CachingDBHandler.insertTstamp(aContext, url, Utility.getMyId(aContext), response.optString("tstamp"));

                }


            }

            @Override
            public void onRequestFailure(Exception exception) {

                if (showFailure)
                    callback.onError(exception);

            }
        };
    }

    public CachedDataHandler(final String url, final CachedDataInterface callback, Context aContext, String activity,
                             String event_type, Map<String, String> event_info, boolean showFailure) {
        this(url, callback, aContext, activity, event_type, null, event_info, showFailure);

    }

    public static boolean showDataFromDb(Context aContext, String url, CachedDataInterface callback) {
        JSONObject response = CachingDBHandler.getResponse(aContext, url, Utility.getMyId(aContext));
        if (response != null) {
            callback.onSuccess(response);
            return false;
        }
        return true;
    }

    public void httpGet(Context aContext, String url) {
        OkHttpHandler.httpGet(aContext, url, this.okHttpResponseHandler);
    }

    public void httpGet(Context aContext, String url, Map<String, String> params) {
        OkHttpHandler.httpGet(aContext, url, params, this.okHttpResponseHandler);
    }


    public void httpPost(Context aContext, String url, Map<String, String> map) {
        OkHttpHandler.httpPost(aContext, url, map, this.okHttpResponseHandler);
    }


}
