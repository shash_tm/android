package com.trulymadly.android.app.utility;

import com.trulymadly.android.app.json.ConstantsHashtags;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by udbhav on 25/02/16.
 */
public class HashtagHandler {
    private final String[] allHashtags;

    public HashtagHandler() {
        this.allHashtags = ConstantsHashtags.allHashtags;
    }

    public JSONArray getHashtags(String text) {
        boolean currentTextAdded = false;
        text = text.trim();
        if (text.charAt(0) == '#') {
            text = text.substring(1);
        }
        text = "#" + text;

        ArrayList<String> filteredHashtags = new ArrayList<>();
        for (String string : allHashtags) {
            if (string.toLowerCase(Locale.ENGLISH).startsWith(text.toLowerCase(Locale.ENGLISH))) {
                if (string.equalsIgnoreCase(text)) {
                    filteredHashtags.add(0, string);
                    currentTextAdded = true;
                } else {
                    filteredHashtags.add(string);
                }
            }
        }
        if (!currentTextAdded) {
            filteredHashtags.add(0, text);
        }

        return Utility.stringArrayListToJsonArray(filteredHashtags);
    }

}

