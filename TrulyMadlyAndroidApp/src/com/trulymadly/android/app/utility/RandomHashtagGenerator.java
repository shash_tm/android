package com.trulymadly.android.app.utility;

import com.trulymadly.android.app.json.ConstantsHashtags;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by deveshbatra on 5/10/16.
 */
public class RandomHashtagGenerator {


    private static final int MAX_HASHTAGS = 12;
    public ArrayList<String> userHashtags;

    public ArrayList<String> getUserHashtags() {
        return userHashtags;
    }

    public void setUserHashtags(ArrayList<String> userHashtags) {
        this.userHashtags = userHashtags;
    }

    public ArrayList<String> getRandomHashtags() {
        ArrayList<String> randomHashtags = new ArrayList<>();
        String[] hashTags = ConstantsHashtags.allHashtags;
        int maxSize = hashTags.length;

        int i = 0, randomNumber;
        Random ran = new Random();
        while (i < MAX_HASHTAGS) {
            randomNumber = ran.nextInt(maxSize);
            if(!randomHashtags.contains(hashTags[randomNumber])) {
                if (userHashtags == null) {
                    randomHashtags.add(hashTags[randomNumber]);
                    i++;


                } else if (!userHashtags.contains(hashTags[randomNumber])) {
                    randomHashtags.add(hashTags[randomNumber]);
                    i++;
                }
            }
        }
        return randomHashtags;
    }


}
