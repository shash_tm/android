package com.trulymadly.android.app.utility;

import android.Manifest;
import android.app.Activity;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.listener.LocationFetchedInterface;

/**
 * Created by deveshbatra on 3/14/16.
 */
public class LocationHelper extends LocationCallback implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {


    static final int LOCATION_ON_REQUEST = 1;
    static final String TAG = "LOCATION";
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    private static final long INTERVAL = 1000 * 10;
    private static final long FASTEST_INTERVAL = 1000 * 5;
    private final Activity aActivity;
    private final LocationFetchedInterface locationFetchedInterface;
    private GoogleApiClient mGoogleApiClient;

    public LocationHelper(Activity activity, LocationFetchedInterface locationFetchedInterface) {
        this.aActivity = activity;
        this.locationFetchedInterface = locationFetchedInterface;
    }

    public void stopLocationService() {

        Log.d(TAG, "stopLocationService");
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {

            PendingResult<Status> removeResult = LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();

        }
    }

    private void askForLocation() {
        if (!mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }

        LocationRequest mLocationRequest = LocationRequest.create()
                .setInterval(60 * 1000) // every 10 minutes
                .setExpirationDuration(10 * 1000) // After 10 seconds
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                Log.d(TAG, "callback");
                final Status status = result.getStatus();
                final LocationSettingsStates locationSettingsStates = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.d(TAG, "LocationSettingsStatusCodes.SUCCESS");
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
//                        mGoogleApiClient.connect();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.d(TAG, "LocationSettingsStatusCodes.RESOLUTION_REQUIRED");
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    aActivity,
                                    Constants.REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.d(TAG, "LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE");
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }


    public void getLocation() {
        String[] permissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
        if (PermissionsHelper.checkAndAskForPermission(aActivity, permissions, PermissionsHelper.REQUEST_FINE_LOCATION_ACCESS)) {
            onPermissionGranted();
        }
    }

    public void onPermissionGranted() {
        Log.d(TAG, "getLocation");
        if (checkPlayServices()) {
            buildGoogleApiClient();
        }
    }

    private synchronized void buildGoogleApiClient() {
        Log.d(TAG, "buildGoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(aActivity)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
        mGoogleApiClient.connect();
    }

    private boolean checkPlayServices() {
        Log.d(TAG, "checkPlayServices");
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(aActivity);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, aActivity,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                AlertsHandler.showMessage(aActivity, "This device is not supported.");
            }
            return false;
        }
        return true;
    }

    private Location displayLocation() {
        Log.d(TAG, "displayLocation");
        if (ActivityCompat.checkSelfPermission(aActivity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(aActivity, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            Location mLastLocation = LocationServices.FusedLocationApi
                    .getLastLocation(mGoogleApiClient);
            if (mLastLocation != null) {
                double latitude = mLastLocation.getLatitude();
                double longitude = mLastLocation.getLongitude();
                Log.d(TAG, "lat:" + latitude + " long:" + longitude);
                stopLocationService();
                locationFetchedInterface.onLocationRecived(mLastLocation);
            } else {
                askForLocation();
                Log.d(TAG, "(Couldn't getBool the location. Make sure location is enabled on the device)");
            }
            return mLastLocation;
        } else
            return null;
    }


    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "onConnected");
        displayLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "onConnectionSuspended");
        mGoogleApiClient.connect();

    }


    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.d(TAG, "Connection failed: ConnectionResult.getErrorCode() = "
                + result.getErrorCode());
    }


}

