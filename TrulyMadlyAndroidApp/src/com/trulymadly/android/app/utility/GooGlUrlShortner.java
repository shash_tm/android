/**
 * 
 */
package com.trulymadly.android.app.utility;

import android.app.ProgressDialog;
import android.content.Context;

import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.listener.CustomOkHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author udbhav
 *
 */
public class GooGlUrlShortner {

	private ProgressDialog mProgressDialog;
	
	

	public GooGlUrlShortner(Context aContext,
			GooGlUrlShortnerInterface responseInterface, String longUrl) {
		this(aContext, responseInterface, longUrl, longUrl);
	}

	public GooGlUrlShortner(Context aContext,
			final GooGlUrlShortnerInterface responseInterface, String longUrl,
			final String defaultShortUrl) {
		
		
		JSONObject params = new JSONObject();
		try {
			params.put("longUrl", longUrl);
		} catch (JSONException ignored) {
		}
		CustomOkHttpResponseHandler responseHandler = new CustomOkHttpResponseHandler(
				aContext, TrulyMadlyActivities.shorten_api, TrulyMadlyEventTypes.shorten) {

			@Override
			public void onRequestSuccess(JSONObject responseJSON) {
				processResponse(responseJSON.optString("id", defaultShortUrl));
			}

			@Override
			public void onRequestFailure(Exception exception) {
				processResponse(defaultShortUrl);
			}

			private void processResponse(String url) {
				mProgressDialog = UiUtils.hideProgressBar(mProgressDialog);
				responseInterface.onComplete(url);
			}
		};
		OkHttpHandler.httpPostJson(aContext,
				Constants.goo_gl_api_shortener_url +"?alt=json&key=" + Constants.GOO_GL_API_KEY, params, responseHandler);
		mProgressDialog = UiUtils.showProgressBar(aContext, mProgressDialog);

	}

	public interface GooGlUrlShortnerInterface {
		void onComplete(String shortUrl);

	}
}
