package com.trulymadly.android.app.utility;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;
import android.webkit.MimeTypeMap;

import com.crashlytics.android.Crashlytics;
import com.google.common.io.ByteStreams;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.listener.GetFileCallback;
import com.trulymadly.android.app.listener.ImageWriteCompleteInterface;
import com.trulymadly.android.app.modal.StickerData;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.util.List;
import java.util.Random;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by udbhav on 10/11/15.
 */
public class FilesHandler {

    public static String createStickerUrl(StickerData sticker, String type) {

        if (sticker == null)
            return null;

        String extension = getExtension();
        String url = Constants.gallery_url;
        String prefix = "tm_";
        if (sticker.getType().equalsIgnoreCase("gallery")) {
            url = url + "/" + prefix + sticker.getId() + "/" + sticker.getId()
                    + extension;
        } else {
            url = url + "/" + prefix + sticker.getGalleryId() + "/" + type
                    + "/" + sticker.getId() + extension;
        }
        return url;

    }

    public static String getStickerKey(String image_url) {
        String location = null;
        if (image_url.contains(Constants.gallery_suffix)) {
            location = image_url.substring(image_url.indexOf(Constants.gallery_suffix) + Constants.gallery_suffix.length(), image_url.length() - 4);
            location = location + getExtension();
            location = location.replaceAll("_|\\.|/", "");
        } else if (image_url.contains(Constants.old_gallery_suffix)) {
            //For old gallery used the image as it is
            location = image_url.substring(image_url.indexOf(Constants.old_gallery_suffix) + Constants.old_gallery_suffix.length(), image_url.length());
            location = location.replaceAll("_|\\.|/", "");
        } else {
            Crashlytics.logException(new IOException("STICKER_KEY_INVALID"));
        }
        return location;
    }

    private static String getExtension() {
        String extension = ".png";

        if (Utility.isWebPSupported()) {
            extension = ".webp";
        }
        return extension;
    }

    /**
     * convert urls of the stickers according to the density
     *
     * @param context
     * @return
     */
    public static String getStickerUrlBasedOnDensity(Context context,
                                                     String sticker_url) {

        String mine_density = UiUtils.getDeviceDensity(context);
        String return_url = sticker_url;
        if (Utility.isSet(return_url)) {
            if (mine_density.equals("hdpi")) {
                return_url = sticker_url.replace("mdpi", "hdpi");
            } else if (mine_density.equals("mdpi")) {
                return_url = sticker_url.replace("hdpi", "mdpi");
            }
        }
        return return_url;
    }

    public static String getPath(Context aContext, Uri uri, String scheme) {
        String path = null;
        if (Utility.isSet(scheme)) {
            if (scheme.equalsIgnoreCase("content")) {
                Cursor cursor = null;
                try {
                    String[] projection = {MediaStore.MediaColumns.DATA};
                    if (uri != null && projection != null) {
                        cursor = aContext.getContentResolver().query(uri,
                                projection, null, null, null);
                    }
                    if (cursor == null) {
                        path = uri.getPath();
                    } else {
                        int column_index = cursor
                                .getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                        if (cursor.moveToFirst()) {
                            path = cursor.getString(column_index);
                        }
                        if (path == null) {
                            List<String> segments = uri.getPathSegments();
                            if (segments.size() >= 3) {
                                path = uri.getPathSegments().get(2);
                            } else if (segments.size() == 2) {
                                path = uri.getPathSegments().get(1);
                            } else if (segments.size() == 1) {
                                path = uri.getPathSegments().get(0);
                            }
                        }
                    }
                } catch (RuntimeException e) {
                    TmLogger.e("GET_PATH_EXCEPTION", e.getMessage());
                } finally {
                    if (cursor != null)
                        cursor.close();
                }
            } else if (scheme.equalsIgnoreCase("file")) {
                path = uri.getPath();
            } else {
                path = uri.getPath();
            }
        }
        return path;
    }

    public static void getFile(Context aContext, Uri uri, String scheme, GetFileCallback getFileCallback, Constants.HttpRequestType httpRequestType) {
        (new GetFileAsyncTask(aContext, uri, scheme, getFileCallback, httpRequestType)).execute();
    }

    /**
     * Deletes the given file or directory
     * In case it is a directory, it first deletes the content of the directory first
     *
     * @param filePath
     */
    public static boolean deleteFileOrDirectory(String filePath) {
        //Re-enabling deleting of files for non live release builds as well
//        if (!Utility.isLiveRelease()) {
//            return false;
//        }
        Log.d("FilesHandler", "deleteFileOrDirectory : filePath : " + filePath);
        if (!Utility.isSet(filePath) || !filePath.contains(Constants.TM_PACKAGE_BASE)) {
            return false;
        }

        File file = new File(filePath);
        if (!file.isDirectory())
            return file.delete();
        else {
            deleteContents(filePath);
            return file.delete();
        }
    }

    /**
     * Deletes the given file or directory inside IMAGE_ASSETS folder
     * In case it is a directory, it deletes the content of the directory first
     *
     * @param fileName
     */
    public static void deleteFileOrDirectoryFromImageAssets(String fileName) {
        File file = new File(Constants.disk_path + "/" + Constants.IMAGES_ASSETS_FOLDER + "/" + fileName);
        deleteFileOrDirectory(file.getAbsolutePath());
    }

    /**
     * Deletes the contents of the given directory
     *
     * @param directoryPath
     */
    public static void deleteContents(String directoryPath) {
        File directory = new File(directoryPath);
        if (directory.isDirectory()) {
            File[] files = directory.listFiles();
            if (files == null)
                return;
            for (File child : directory.listFiles()) {
                child.delete();
            }
        }
    }

    /**
     * Deletes the contents of the given directory matching the given pattern
     *
     * @param directoryPath
     * @param pattern       - Pattern of the filename
     * @param include       - Decides whether you want to include the files with the
     *                      given pattern or exclude them
     */
    public static void deleteContents(String directoryPath, final String pattern, final boolean include) {
        File directory = new File(directoryPath);
        if (!directory.isDirectory())
            return;
        File[] files = directory.listFiles(new FilenameFilter() {

            @Override
            public boolean accept(File dir, String filename) {
                boolean b = filename.matches(pattern);
                return (include) == b;
            }
        });

        if (files == null)
            return;

        for (File child : files)
            child.delete();
    }

    /**
     * Gets the filepath from the parentDirectory path and the filename
     * i.e. parentDirectory + "/" + filename
     *
     * @param parentPath - parent directory path to be used
     * @param fileName
     */
    public static String getFilePath(String parentPath, String fileName) {
        return parentPath + "/" + fileName;
    }

    public static void createImageCacheFolder() {
        boolean didExist = createFolder("/" + Constants.IMAGES_ASSETS_FOLDER);
        try {
            if (!didExist) {
                File noMediaFile = new File(getFilePath(Constants.disk_path,
                        Constants.IMAGES_ASSETS_FOLDER), ".nomedia");
                noMediaFile.createNewFile();
            }
        } catch (IOException ignored) {
        }
    }

    public static String getUniqueFileName(String uniqueId, String format) {
        return uniqueId + "_" + TimeUtils.getSystemTimeInMiliSeconds() + "." + format;
    }

    public static File createFile(String directory, String fileName) {
        createFolder("/" + Constants.IMAGES_ASSETS_FOLDER + "/" + directory);
        String parentPath = getFilePath(getFilePath(Constants.disk_path, Constants.IMAGES_ASSETS_FOLDER), directory);
        String filePath = getFilePath(parentPath, fileName);
        File file = new File(filePath);
        TmLogger.d("file", Constants.disk_path + "/" + directory + "/" + fileName);
        boolean didExist = true;
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            didExist = false;
        }
        return file;
    }

    public static String getFilePath(String directory, String fileName, Context ctx) {
        createFolder("/" + Constants.IMAGES_ASSETS_FOLDER + "/" + directory);
        String parentPath = getFilePath(getFilePath(Constants.disk_path, Constants.IMAGES_ASSETS_FOLDER), directory);
        return getFilePath(parentPath, fileName);
    }

    //TODO: FILE HANLDING: Check on all devices
    public static void renameFile(String fromPath, String toName) {
        File fromFile = new File(fromPath);
        File newFile = new File(fromFile.getParentFile(), toName);
        boolean success = fromFile.renameTo(newFile);
    }

    public static File copyFile(Context ctx, String fromPath, String directory, String fileName) throws IOException {
        createFolder("/" + Constants.IMAGES_ASSETS_FOLDER + "/" + directory);
        String parentPath = getFilePath(getFilePath(Constants.disk_path, Constants.IMAGES_ASSETS_FOLDER), directory);
        String filePath = getFilePath(parentPath, fileName);
        File src = new File(fromPath);
        File dst = new File(filePath);
        FileChannel inChannel = new FileInputStream(src).getChannel();
        FileChannel outChannel = new FileOutputStream(dst).getChannel();
        try {
            inChannel.transferTo(0, inChannel.size(), outChannel);
        } finally {
            if (inChannel != null)
                inChannel.close();
            if (outChannel != null)
                outChannel.close();
        }

        return dst;
    }

    /**
     * @param createIfNotPresent : If true - Creates the directory if it doesn't exist
     *                           return : the video_assets folder path -
     *                           currently we are storing current users video profiles in this directory
     */
    public static String getVideoDirectoryPath(boolean createIfNotPresent) {
        if (createIfNotPresent) {
            boolean didExist = createFolder("/" + Constants.VIDEO_ASSETS_FOLDER);
            try {
                if (!didExist) {
                    File noMediaFile = new File(getFilePath(Constants.disk_path,
                            Constants.VIDEO_ASSETS_FOLDER), ".nomedia");
                    noMediaFile.createNewFile();
                }
            } catch (IOException ignored) {
            }
        }

        return Constants.disk_path + "/" + Constants.VIDEO_ASSETS_FOLDER;
    }

    public static boolean createFolder(String location) {

        File folder = new File(Constants.disk_path + location);
        boolean didExist = true;
        if (!folder.exists()) {
            folder.mkdirs();
            didExist = false;
        }
        return didExist;
    }

    public static void downloadFile(Context context, String urlString, File file,
                                    ImageWriteCompleteInterface imageWriteCompleteInterface) {
        try {
            URL url = new URL(urlString);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            if (imageWriteCompleteInterface != null) {
                imageWriteCompleteInterface.onFail(e, 0);
            }

            return;
        }
        new DownloadFileAsyncTask(context, urlString, file, imageWriteCompleteInterface).execute();
    }

    public static int getFileSizeInKb(File file) {
        if (file != null && file.exists()) {
            return Integer.parseInt(String.valueOf(file.length() / 1024));
        }
        return 0;
    }

    public static int getFileSizeInKb(String filePath) {
        if (Utility.isSet(filePath)) {
            return getFileSizeInKb(new File(filePath));
        }
        return 0;
    }

    public static int getFileSizeInKb(Context context, Uri uri) {
        Cursor returnCursor = context.getContentResolver().query(uri, null, null, null, null);
        String name = null;
        long size = 0;
        if (returnCursor != null && returnCursor.moveToFirst()) {
            int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
            size = returnCursor.getLong(sizeIndex);
            size /= 1000;
        }

        if (returnCursor != null) {
            returnCursor.close();
        }

        return (int) size;
    }

    public static String getFileExtension(Context context, Uri uri) {
        String extension = null;
        if (uri != null) {
            extension = MimeTypeMap.getFileExtensionFromUrl(uri.toString());
            if (!Utility.isSet(extension)) {
                Cursor returnCursor = context.getContentResolver().query(uri, null, null, null, null);
                long size = 0;
                if (returnCursor != null && returnCursor.moveToFirst()) {
                    int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                    String name = returnCursor.getString(nameIndex);
                    extension = getFileExtension(name);
                }

                if (returnCursor != null) {
                    returnCursor.close();
                }
            }
        }

        return extension;
    }

    public static String getFileExtension(String fileNameOrPath) {
        String extension = null;
        if (Utility.isSet(fileNameOrPath)) {
            extension = fileNameOrPath.substring(fileNameOrPath.lastIndexOf(".") + 1);
        }

        return extension;
    }

    private static class GetFileAsyncTask extends AsyncTask<Void, Void, File> {

        private final Context aContext;
        private final Uri uri;
        private final String scheme;
        private final GetFileCallback getFileCallback;
        private final Constants.HttpRequestType httpRequestType;

        public GetFileAsyncTask(Context aContext, Uri uri, String scheme, GetFileCallback getFileCallback,
                                Constants.HttpRequestType httpRequestType) {
            this.aContext = aContext;
            this.uri = uri;
            this.scheme = scheme;
            this.getFileCallback = getFileCallback;
            this.httpRequestType = httpRequestType;
        }

        @Override
        protected File doInBackground(Void... aVoids) {
            Random rand = new Random();
            File file = null;
            if (Utility.isSet(scheme)) {
                if (scheme.equalsIgnoreCase("content")) {
                    try {
                        String mime = aContext.getContentResolver().getType(uri);
                        InputStream inputStream = aContext.getContentResolver()
                                .openInputStream(uri);
                        file = new File(aContext.getCacheDir(), "user_file_"
                                + rand.nextInt(10000) + "."
                                + MimeUtils.guessExtensionFromMimeType(mime));
                        OutputStream outputStream = new FileOutputStream(file);
                        ByteStreams.copy(inputStream, outputStream);
                        outputStream.close();
                    } catch (IOException | NullPointerException | SecurityException ignored) {

                    }
                } else if (scheme.equalsIgnoreCase("file")) {
                    file = new File(uri.getPath());
                }
            }
            return file;
        }

        @Override
        protected void onPostExecute(File file) {
            getFileCallback.onGetFileComplete(file, uri, httpRequestType);
        }
    }

    private static class DownloadFileAsyncTask extends AsyncTask<String, Void, Exception> {
        private final Context mContext;
        private final String url;
        private final File mFile;
        private final ImageWriteCompleteInterface mImageWriteCompleteInterface;
        private long mTimeTaken = 0L;

        public DownloadFileAsyncTask(Context mContext, String url, File mFile,
                                     ImageWriteCompleteInterface imageWriteCompleteInterface) {
            this.mContext = mContext.getApplicationContext();
            this.url = url;
            this.mFile = mFile;
            this.mImageWriteCompleteInterface = imageWriteCompleteInterface;
        }

        @Override
        protected Exception doInBackground(String... params) {
            mTimeTaken = TimeUtils.getSystemTimeInMiliSeconds();
            Exception exception = null;
            InputStream inputStream = null;
            BufferedOutputStream bufferedOutputStream = null;
            try {

                OkHttpHandler.NET_TIMEOUT timeoutType = OkHttpHandler.NET_TIMEOUT.LARGE;
                OkHttpClient.Builder builder = OkHttpHandler.getOkHttpBuilder(timeoutType.value(), timeoutType.value(), timeoutType.value());
                OkHttpClient client = builder.build();

//                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url(url).build();
                Response response = client.newCall(request).execute();
                if (OkHttpHandler.isValidResponse(response)) {
                    inputStream = response.body().byteStream();

                    bufferedOutputStream = new BufferedOutputStream(
                            new FileOutputStream(mFile));

                    int read = 0;
                    byte[] bytes = new byte[1024];

                    while ((read = inputStream.read(bytes)) != -1) {
                        bufferedOutputStream.write(bytes, 0, read);
                    }

                    bufferedOutputStream.flush();
                } else if (response.code() == 404 || response.code() == 403) {
                    exception = new FileNotFoundException(mContext.getString(R.string.download_image_not_avialable));
                } else {
                    exception = new IOException("Invalid Response " + response != null ? response.code() + "" : " No Code ");
                }
            } catch (IOException e) {
                Crashlytics.logException(e);
                exception = e;
            } catch (OutOfMemoryError e) {
                Crashlytics.logException(e);
                exception = new Exception("OutOfMemory Error : " + url);
            } finally {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (bufferedOutputStream != null) {
                    try {
                        bufferedOutputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return exception;
        }

        @Override
        protected void onPostExecute(Exception e) {
            mTimeTaken = TimeUtils.getSystemTimeInMiliSeconds() - mTimeTaken;
            super.onPostExecute(e);
            if (mImageWriteCompleteInterface != null) {
                if (e == null) {
                    mImageWriteCompleteInterface.onSuccess(mTimeTaken);
                } else {
                    mImageWriteCompleteInterface.onFail(e, mTimeTaken);
                }
            }
        }
    }

}
