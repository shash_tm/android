package com.trulymadly.android.app.utility;

import android.content.Context;

import com.trulymadly.android.app.R;

/**
 * Created by avin on 06/07/16.
 */
public enum ServerCodes {
    internet_error(-2), server_error(-1), none(0), no_sparks(1);

    private int key;

    ServerCodes(int key) {
        this.key = key;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public static ServerCodes getServerCodeFromIntValue(int key){
        switch (key){
            case -1:
                return internet_error;
            case -2:
                return server_error;
            case 1:
                return no_sparks;
        }

        return none;
    }

    public String getDesc(Context context){
        switch (this){
            case internet_error:
                return context.getString(R.string.ERROR_NETWORK_FAILURE);
            case server_error:
                return context.getString(R.string.SERVER_ERROR_LARGE);
            case no_sparks:
                return context.getString(R.string.no_sparks_left);
        }

        return null;
    }

    public int getDesc(){
        switch (this){
            case internet_error:
                return R.string.ERROR_NETWORK_FAILURE;
            case server_error:
                return R.string.SERVER_ERROR_LARGE;
            case no_sparks:
                return R.string.no_sparks_left;
        }

        return -1;
    }
}
