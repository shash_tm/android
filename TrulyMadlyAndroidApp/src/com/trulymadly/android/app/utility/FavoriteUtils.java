package com.trulymadly.android.app.utility;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.adapter.MyFavoriteGridItem;
import com.trulymadly.android.app.asynctasks.FavoriteImageLoadAsyncTask;
import com.trulymadly.android.app.modal.FavoriteDataModal;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities.favorites;

/**
 * Created by udbhav on 26/07/16.
 */
public class FavoriteUtils {
    public static final int FAVORITE_IMAGE_PREFETCH_LIMIT = 5;
    public static final String FAVORITE_KEY_MOVIES = "movies";
    public static final String FAVORITE_KEY_MUSIC = "music";
    public static final String FAVORITE_KEY_BOOKS = "books";
    public static final String FAVORITE_KEY_FOOD = "food";
    public static final String FAVORITE_KEY_OTHERS = "others";
    public static final String[] TAB_CATEGORY_KEYS = new String[]{FAVORITE_KEY_MOVIES, FAVORITE_KEY_MUSIC, FAVORITE_KEY_BOOKS, FAVORITE_KEY_FOOD, FAVORITE_KEY_OTHERS};
    public final static int PAGE_COUNT = 5;
    public static String[] tabTitles = new String[]{"Movies", "Music", "Books", "Food", "Others"};
    public static int[] imageResId = {
            R.drawable.fav_movies,
            R.drawable.fav_music,
            R.drawable.fav_books,
            R.drawable.fav_food,
            R.drawable.fav_others
    };
    public static int[] imageResIdSelected = {
            R.drawable.fav_movies_selected,
            R.drawable.fav_music_selected,
            R.drawable.fav_books_selected,
            R.drawable.fav_food_selected,
            R.drawable.fav_others_selected
    };

    public static void bindView(Context aContext, MyFavoriteGridItem holder, FavoriteDataModal favoriteData, View.OnClickListener onClickListener) {
        holder.myFavoriteName.setText(favoriteData.getName());
        RelativeLayout.LayoutParams myFavoriteNameParams = (RelativeLayout.LayoutParams) holder.myFavoriteName.getLayoutParams();
        if (favoriteData.isMutualFavorite()) {
            myFavoriteNameParams.setMargins(UiUtils.dpToPx(4), 0, UiUtils.dpToPx(4), UiUtils.dpToPx(2));
            holder.myFavoriteName.setTypeface(null, Typeface.BOLD);
        } else {
            myFavoriteNameParams.setMargins(UiUtils.dpToPx(2), 0, UiUtils.dpToPx(2), UiUtils.dpToPx(2));
            holder.myFavoriteName.setTypeface(null, Typeface.NORMAL);
        }
        holder.myFavoriteName.setLayoutParams(myFavoriteNameParams);


        // Trick to use vector drawable as a placeholder in Picasso
        // First setImageResource
        // Second user option .noPlaceHolder() while loading
        //holder.myFavoriteImage.setImageResource(placeHder);


        if (favoriteData.isSelected()) {
            holder.myFavoriteOverLayImage.setImageResource(R.drawable.select_check_mark_white_circle);
            holder.myFavoriteOverLay.setVisibility(View.VISIBLE);
        } else {
            holder.myFavoriteOverLay.setVisibility(View.GONE);
        }
        holder.setFavoriteData(favoriteData);
        holder.itemView.setTag(holder);
        if (favoriteData.isSelectable() || onClickListener != null) {
            holder.itemView.setOnClickListener(onClickListener);
        } else {
            holder.itemView.setOnClickListener(null);
        }

        holder.favoriteBorder.setVisibility(View.VISIBLE);
        if (favoriteData.isMutualFavorite()) {
            UiUtils.setBackground(aContext, holder.favoriteBorder, R.drawable.black_rounded_rect_favorite_solid);
        } else if (favoriteData.isShowMoreItem()) {
            UiUtils.setBackground(aContext, holder.favoriteBorder, R.drawable.yellow_rounded_rect_favorite_solid);
        } else if (favoriteData.isEdit()) {
            UiUtils.setBackground(aContext, holder.favoriteBorder, R.drawable.black_rounded_rect);
        } else {
            UiUtils.setBackground(aContext, holder.favoriteBorder, R.drawable.black_rounded_rect);
        }

        RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) holder.itemView.getLayoutParams();

//        int OFFSET_IN_DP = 4;
        int OFFSET_IN_PX = UiUtils.dpToPx(4);


        if (favoriteData.isSelected() || favoriteData.isSuggestion() || favoriteData.isShowMoreItem()) {
            params.width = RecyclerView.LayoutParams.MATCH_PARENT;
            params.setMargins(0, 0, 0, 0);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.itemView.setElevation(UiUtils.dpToPx(0));
            }
        } else if (favoriteData.isMutualFavorite()) {
            params.width = UiUtils.dpToPx(80);
            params.setMargins(0, 0, 0, 0);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.itemView.setElevation(UiUtils.dpToPx(5));
            }
        } else {
            params.width = UiUtils.dpToPx(80);
            params.setMargins(0, OFFSET_IN_PX, 0, 0);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.itemView.setElevation(UiUtils.dpToPx(0));
            }
        }

        holder.itemView.setLayoutParams(params);


        if (favoriteData.isShowMoreItem()) {
            Picasso.with(aContext).load(R.drawable.fav_others_yellow).noFade().into(holder.showMoreImageView);
            holder.myFavoriteImage.setVisibility(View.GONE);
            holder.myFavoriteName.setVisibility(View.GONE);
            holder.showMoreTextImage.setVisibility(View.VISIBLE);
        } else {
            holder.myFavoriteImage.setVisibility(View.VISIBLE);
            holder.myFavoriteName.setVisibility(View.VISIBLE);
            holder.showMoreTextImage.setVisibility(View.GONE);
        }

        //String imageWithFbToken = favoriteData.getImageUrl(true);
        String imageWithoutFbToken = favoriteData.getImageUrl(false);
        final int placeHolder = favoriteData.getPlaceHolderResId();
        //if (imageWithFbToken != null && imageWithoutFbToken != null) {
        if (imageWithoutFbToken != null) {
            new FavoriteImageLoadAsyncTask(aContext, holder, imageWithoutFbToken, favoriteData.isShowMoreItem(), favoriteData.isMutualFavorite(), placeHolder).execute();

        } else {
            holder.myFavoriteProgressBar.setVisibility(View.GONE);
            holder.myFavoriteImage.setImageResource(placeHolder);
            if (favoriteData.isShowMoreItem()) {
                UiUtils.setBackground(aContext, holder.favoriteBorder, R.drawable.yellow_rounded_rect_favorite);
            } else if (favoriteData.isMutualFavorite()) {
                UiUtils.setBackground(aContext, holder.favoriteBorder, R.drawable.black_rounded_rect_favorite);
            }
        }
    }

    public static FavoriteDataModal createSeeMoreItem() {
        FavoriteDataModal favModal = new FavoriteDataModal("app:SEE_MORE_ITEM", "See More", FAVORITE_KEY_MOVIES, null,
                false, false, false, false, true);
        favModal.setShowMoreItem(true);
        return favModal;
    }

    public static HashMap<String, ArrayList<FavoriteDataModal>> parseFavorites(JSONObject likesData, boolean isSelected, boolean isSelectable, boolean isEdit) {
        HashMap<String, ArrayList<FavoriteDataModal>> fDataMap = new HashMap<>();
        fDataMap = getFavoritesForCategory(fDataMap, likesData, FAVORITE_KEY_MOVIES, "movies_favorites", isSelected, isSelectable, isEdit);
        fDataMap = getFavoritesForCategory(fDataMap, likesData, FAVORITE_KEY_MUSIC, "music_favorites", isSelected, isSelectable, isEdit);
        fDataMap = getFavoritesForCategory(fDataMap, likesData, FAVORITE_KEY_BOOKS, "books_favorites", isSelected, isSelectable, isEdit);
        fDataMap = getFavoritesForCategory(fDataMap, likesData, FAVORITE_KEY_FOOD, "food_favorites", isSelected, isSelectable, isEdit);
        fDataMap = getFavoritesForCategory(fDataMap, likesData, FAVORITE_KEY_OTHERS, "other_favorites", isSelected, isSelectable, isEdit);
        return fDataMap;
    }

    private static HashMap<String, ArrayList<FavoriteDataModal>> getFavoritesForCategory(HashMap<String, ArrayList<FavoriteDataModal>> fDataMap, JSONObject likesData, String categoryKey, String jsonKey, boolean isSelected, boolean isSelectable, boolean isEdit) {
        if (fDataMap == null) {
            fDataMap = new HashMap<>();
        }

        ArrayList<FavoriteDataModal> modalList;
        if (fDataMap.containsKey(categoryKey)) {
            modalList = fDataMap.get(categoryKey);
        } else {
            modalList = new ArrayList<>();
        }

        if (likesData != null && likesData.optJSONArray(jsonKey) != null) {
            JSONArray favArray = likesData.optJSONArray(jsonKey);
            if (favArray != null && favArray.length() > 0) {
                int len = favArray.length();
                for (int i = 0; i < len; i++) {
                    JSONObject singleFavJson = favArray.optJSONObject(i);
                    if (singleFavJson != null) {
                        FavoriteDataModal favDataModal = new FavoriteDataModal(singleFavJson.optString("id"), singleFavJson.optString("name"), categoryKey, singleFavJson.optString("pic"), isSelected, isSelectable, singleFavJson.optBoolean("common"), false, isEdit);
                        if (!modalList.contains(favDataModal)) {
                            modalList.add(favDataModal);
                        }
                    }
                }
            }

        }

        fDataMap.put(categoryKey, modalList);

        return fDataMap;

    }

    public static void prefetchFavoriteImages(Context aContext, HashMap<String, ArrayList<FavoriteDataModal>> favDataMap, int pos) {
        prefetchFavoriteImages(aContext, favDataMap, pos, Integer.MAX_VALUE);

    }

    public static void prefetchFavoriteImages(Context aContext, HashMap<String, ArrayList<FavoriteDataModal>> favDataMap, int pos, int favoriteImagePrefetchLimit) {
        if (favDataMap != null && favDataMap.get(TAB_CATEGORY_KEYS[pos]) != null && favDataMap.get(TAB_CATEGORY_KEYS[pos]).size() > 0) {
            ArrayList<FavoriteDataModal> items = favDataMap.get(TAB_CATEGORY_KEYS[pos]);
            for (int i = 0; i < items.size() && i < favoriteImagePrefetchLimit; i++) {
                Utility.prefetchPhoto(aContext, items.get(i).getImageUrl(false), favorites);
            }
        }
    }
}
