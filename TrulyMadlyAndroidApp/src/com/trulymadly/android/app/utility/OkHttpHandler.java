/**
 *
 */
package com.trulymadly.android.app.utility;

import android.content.Context;
import android.net.Uri;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.listener.CustomOkHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpCookie;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nonnull;

import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.JavaNetCookieJar;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * @author udbhav
 */
public class OkHttpHandler {

    private static final String TAG = "OkHttpHandler";
    private static OkHttpClient client = null;
    private static CookieManager cookieManager = null;

    private static void createClient(Context aContext, NET_TIMEOUT timeoutType) {
        boolean reCreateClient = false;
        if (client == null
                || client.connectTimeoutMillis() != timeoutType.value * 1000
                || client.readTimeoutMillis() != timeoutType.value * 1000) {
            reCreateClient = true;
        }
//        client.setProxy(new Proxy(Proxy.Type.HTTP, new
//                InetSocketAddress("192.168.1.200", 8888)));
//        client.setConnectTimeout(timeoutType.value(), TimeUnit.SECONDS);
//        client.setReadTimeout(timeoutType.value(), TimeUnit.SECONDS);

        if (reCreateClient) {
            OkHttpClient.Builder builder = getOkHttpBuilder(timeoutType.value(), timeoutType.value(), 0);
            createCookieManager();
            builder.cookieJar(new JavaNetCookieJar(cookieManager));
            client = builder.build();
        }

        insertPhpSessIdForokhttp(aContext);
    }

    public static OkHttpClient.Builder getOkHttpBuilder(int connectTimeoutSeconds, int readTimeoutSeconds, int writeTimeoutSeconds) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        if (connectTimeoutSeconds > 0) {
            builder.connectTimeout(connectTimeoutSeconds, TimeUnit.SECONDS);
        }
        if (readTimeoutSeconds > 0) {
            builder.readTimeout(readTimeoutSeconds, TimeUnit.SECONDS);
        }
        if (writeTimeoutSeconds > 0) {
            builder.writeTimeout(writeTimeoutSeconds, TimeUnit.SECONDS);
        }
        //Adding Stetho Interceptor for non live builds.
        if (!Utility.isLiveRelease()) {
            builder.addNetworkInterceptor(new StethoInterceptor());
        }
        return builder;
    }

    private static void createCookieManager() {
        if (cookieManager == null) {
            cookieManager = new CookieManager();
            cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        }
    }

    public static void httpGet(Context aContext, String url,
                               @Nonnull CustomOkHttpResponseHandler responseHandler) {
        httpGet(aContext, url, null, responseHandler, NET_TIMEOUT.NORMAL);
    }

    public static void httpGet(Context aContext, String url,
                               Map<String, String> params,
                               @Nonnull CustomOkHttpResponseHandler responseHandler) {
        httpGet(aContext, url, params, responseHandler, NET_TIMEOUT.NORMAL);

    }

    public static void httpGet(Context aContext, String url,
                               Map<String, String> params,
                               @Nonnull CustomOkHttpResponseHandler responseHandler, NET_TIMEOUT timeoutType) {

        if (url == null) {
            responseHandler.onRequestFailure(new IOException(
                    "Unexpected responseCode "));
            return;
        }
        createClient(aContext, timeoutType);
        url = createHttpGetUrl(url, params);
        Request request = createRequestBuilder(aContext, url).build();
        Call call = client.newCall(request);
        call.enqueue(responseHandler);
    }

    public static String httpGetSynchronous(Context aContext, String url,
                                            Map<String, String> params) {
        return httpGetSynchronous(aContext, url, params, null, null, null, null);
    }

    public static String httpGetSynchronous(Context aContext, String url,
                                            Map<String, String> params, String trkActivity, String trkEventType) {
        return httpGetSynchronous(aContext, url, params, trkActivity, trkEventType, null, null);

    }

    public static String httpGetSynchronous(Context aContext, String url,
                                            Map<String, String> params, String trkActivity, String trkEventType, String trkEventStatus, Map<String, String> trkEventinfo) {

        String returnVal = null;
        long trkStartTime = (new Date()).getTime();
        Response response = null;
        if (url != null) {
            createClient(aContext, NET_TIMEOUT.NORMAL);
            url = createHttpGetUrl(url, params);
            Request request = createRequestBuilder(aContext, url).build();
            try {
                response = client.newCall(request).execute();
                if (isValidResponse(response)) {
                    returnVal = response.body().string();
                }
            } catch (IOException | OutOfMemoryError e) {
                e.printStackTrace();
            }
        }
        if (Utility.isSet(trkActivity)) {
            long trkEndTime = (new Date()).getTime();
            if (returnVal != null) {
                if (!Utility.isSet(trkEventStatus)) {
                    trkEventStatus = "success";
                }
            } else {
                trkEventStatus = "fail";
            }
            TrulyMadlyTrackEvent.trackEvent(aContext, trkActivity,
                    trkEventType, trkEndTime - trkStartTime, trkEventStatus, trkEventinfo, true);
        }
        return returnVal;
    }

    private static String createHttpGetUrl(String url,
                                           Map<String, String> params) {
        if (url.contains("?"))
            url += "&login_mobile=true";
        else
            url += "?login_mobile=true";

        if (Utility.isWebPSupported()) {
            url += "&webpSupport=yes";
        }

        if (params != null) {
            Iterator<Entry<String, String>> it = params.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry<String, String> pairs = it.next();
                url += "&" + pairs.getKey() + "=" + pairs.getValue();
                it.remove(); // avoids a ConcurrentModificationException
            }
        }

        //quick fix for encoding url
        String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";
        url = Uri.encode(url, ALLOWED_URI_CHARS);
        return url;
    }

    public static void httpPost(Context aContext, String url) {

        CustomOkHttpResponseHandler responseHandler = new CustomOkHttpResponseHandler(aContext) {

            @Override
            public void onRequestSuccess(JSONObject responseJSON) {
            }

            @Override
            public void onRequestFailure(Exception exception) {
            }
        };
        httpPost(aContext, url, null, responseHandler, NET_TIMEOUT.NORMAL);
    }

    public static void httpPost(Context aContext, String url, Map<String, String> params) {

        CustomOkHttpResponseHandler responseHandler = new CustomOkHttpResponseHandler(aContext) {

            @Override
            public void onRequestSuccess(JSONObject responseJSON) {
            }

            @Override
            public void onRequestFailure(Exception exception) {
            }
        };
        responseHandler.setForceSuccess(true);
        httpPost(aContext, url, params, responseHandler, NET_TIMEOUT.NORMAL);
    }

    public static void httpPost(Context aContext, String url,
                                CustomOkHttpResponseHandler responseHandler) {
        httpPost(aContext, url, null, responseHandler, NET_TIMEOUT.NORMAL);
    }

    public static void httpPost(Context aContext, String url,
                                Map<String, String> params,
                                CustomOkHttpResponseHandler responseHandler) {
        httpPost(aContext, url, params, responseHandler, NET_TIMEOUT.NORMAL);
    }

    public static String httpPostSynchronous(Context aContext, String url,
                                             Map<String, String> params) {
        if (url == null) {
            return null;
        }
        createClient(aContext, NET_TIMEOUT.NORMAL);
        Request request = createRequestBuilder(aContext, url).post(createHttpPostFormBody(params)).build();
        try {
            Response response = client.newCall(request).execute();
            if (isValidResponse(response)) {
                return response.body().string();
            } else {
                return null;
            }
        } catch (IOException e1) {
            e1.printStackTrace();
            return null;
        }
    }

    public static void httpPost(Context aContext, String url,
                                Map<String, String> params,
                                CustomOkHttpResponseHandler responseHandler, NET_TIMEOUT timeoutType) {
        if (url == null) {
            responseHandler.onRequestFailure(new IOException(
                    "Unexpected responseCode "));
            return;
        }
        createClient(aContext, timeoutType);
        Request request = createRequestBuilder(aContext, url).post(createHttpPostFormBody(params)).build();
        Call call = client.newCall(request);
        call.enqueue(responseHandler);
    }

    public static RequestBody createHttpPostFormBody(Map<String, String> params) {
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add("login_mobile", "true");
        if (Utility.isWebPSupported()) {
            formBuilder.add("webpSupport", "yes");
        }
        if (params != null) {
            Iterator<Entry<String, String>> it = params.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry<String, String> pairs = it.next();
                if (pairs.getValue() != null)
                    formBuilder.add(pairs.getKey(), pairs.getValue());
                //formBuilder.add(pairs.getKey().toString(), pairs.getValue().toString());

                it.remove(); // avoids a ConcurrentModificationException
            }
        }
        return formBuilder.build();
    }

    public static void httpPostJson(Context aContext, String url,
                                    JSONObject params,
                                    CustomOkHttpResponseHandler responseHandler) {
        String jsonContentType = "application/json; charset=utf-8";
        if (url == null) {
            responseHandler.onRequestFailure(new IOException(
                    "Unexpected responseCode "));
            return;
        }
        createClient(aContext, NET_TIMEOUT.NORMAL);

        if (params == null) {
            params = new JSONObject();
        }
        try {
            params.put("login_mobile", "true");
            if (Utility.isWebPSupported()) {
                params.put("webpSupport", "yes");
            }
        } catch (JSONException ignored) {
        }
        Request.Builder builder = createRequestBuilder(aContext, url);
        builder.addHeader("Content-Type", jsonContentType);
        Request request = builder.post(RequestBody.create(MediaType.parse(jsonContentType), params.toString())).build();
        Call call = client.newCall(request);
        call.enqueue(responseHandler);
    }

    private static Request.Builder createRequestBuilder(Context aContext, String url) {
        Request.Builder builder = new Request.Builder().url(url);
        builder.addHeader("source", "androidApp");
        builder.addHeader("store", Constants.RELEASE_STORE);
        builder.addHeader("app_version_code",
                Utility.getAppVersionCode(aContext));
        builder.addHeader("app_version_name",
                Utility.getAppVersionName(aContext));
        builder.addHeader("device_id",
                Utility.getDeviceId(aContext));
        builder.addHeader("login_mobile", "true");
        if (Utility.isWebPSupported()) {
            builder.addHeader("webpSupport", "yes");
        }
        return builder;
    }

    private static void insertPhpSessIdForokhttp(Context aContext) {
        if (client == null) {
            createClient(aContext, NET_TIMEOUT.NORMAL);
            return;
        }
        String phpSessId = SPHandler.getString(aContext,
                ConstantsSP.SHARED_KEYS_PHPSESSID);
        if (Utility.isSet(phpSessId)) {
            setCookieByName("PHPSESSID", phpSessId);
        }
    }

    public static void saveCookieOnLogin(Context aContext) {
        String phpSessId = getCookieByName(aContext, "PHPSESSID");
        if (Utility.isSet(phpSessId)) {
            SPHandler.setString(aContext,
                    ConstantsSP.SHARED_KEYS_PHPSESSID,
                    phpSessId);
        }
    }

    public static void clearAllCookies(Context aContext) {
        if (client == null) {
            createClient(aContext, NET_TIMEOUT.NORMAL);
        }
        //CookieManager cookieManager = (CookieManager) client.getCookieHandler();
        if (cookieManager != null) {
            cookieManager.getCookieStore().removeAll();
            client = client.newBuilder().cookieJar(new JavaNetCookieJar(cookieManager)).build();
        }

    }

    public static void clearCookie(Context aContext, String cookieName) {
        if (client == null) {
            createClient(aContext, NET_TIMEOUT.NORMAL);
        }
        //CookieManager cookieManager = (CookieManager) client.getCookieHandler();
        if (cookieManager != null) {
            for (HttpCookie cookie : cookieManager.getCookieStore().getCookies()) {
                if (Utility.stringCompare(cookieName, cookie.getName())) {
                    try {
                        cookieManager.getCookieStore().remove(new URI(ConstantsUrls.COOKIE_DOMAIN), cookie);
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                    return;
                }
            }
            client = client.newBuilder().cookieJar(new JavaNetCookieJar(cookieManager)).build();
        }


    }

    public static void setCookieByName(String name, String value) {
        //CookieManager cookieManager = (CookieManager) client.getCookieHandler();
        createCookieManager();

        //http://stackoverflow.com/questions/26004623/manually-set-a-cookie-in-default-cookiestore-and-using-it-in-okhttp-requests/26024937?noredirect=1#comment40790484_26024937
        /** SECOND WAY */
        /*
        List<String> values = new ArrayList<String>(
				Arrays.asList(name + "=" + value));
		Map<String, List<String>> cookies = new HashMap<String, List<String>>();
		cookies.put("Set-Cookie", values);
		try {
			cookieManager.put(new URI(Constants.cookieDomain), cookies);
		} catch (IOException e) {
		} catch (URISyntaxException e) {
		}
		*/

        /** FIRST WAY */
        try {
            HttpCookie cookie = new HttpCookie(name, value);
            cookie.setPath("/");
            cookie.setVersion(0);
            cookie.setDomain(ConstantsUrls.COOKIE_DOMAIN);
            cookieManager.getCookieStore().add(new URI(ConstantsUrls.COOKIE_DOMAIN),
                    cookie);
        } catch (URISyntaxException | NoSuchMethodError | ArrayIndexOutOfBoundsException | StackOverflowError | NullPointerException e) {
            e.printStackTrace();
        }

        client = client.newBuilder().cookieJar(new JavaNetCookieJar(cookieManager)).build();
    }

    public static String getCookieByName(Context aContext, String name) {

        if (client == null) {
            createClient(aContext, NET_TIMEOUT.NORMAL);
        } else {
            insertPhpSessIdForokhttp(aContext);
        }
        //CookieManager cookieManager = (CookieManager) client.getCookieHandler();
        if (cookieManager != null) {
            cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
            for (HttpCookie cookie : cookieManager.getCookieStore().getCookies()) {
                if (cookie.getName().equalsIgnoreCase(name) && cookie.getDomain().toLowerCase(Locale.ENGLISH).contains(ConstantsUrls.COOKIE_DOMAIN.toLowerCase(Locale.ENGLISH))) {
                    return cookie.getValue();
                }
            }
        }

        return null;
    }

    public static boolean isValidResponse(Response response) {
        if (response == null) {
            return false;
        }
        if (response.isSuccessful()) {
            return true;
        } else if (response.code() >= 300) {
            response.body().close();
            return false;
        }
        return false;
    }

    public enum NET_TIMEOUT {
        SMALL(10), NORMAL(30), LARGE(60);
        private final int value;

        NET_TIMEOUT(int value) {
            this.value = value;
        }

        public int value() {
            return value;
        }
    }

}
