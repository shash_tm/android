package com.trulymadly.android.app.utility;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;

import com.crashlytics.android.Crashlytics;

import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by udbhav on 10/11/15.
 */
@SuppressLint("CommitPrefEdits")
public class SPHandler {
    /**
     * Get shared preference by key
     *
     * @param aContext
     * @param key
     * @return String
     */
    public static String getString(@NonNull Context aContext, @NonNull String key) {
        try {
            return PreferenceManager.getDefaultSharedPreferences(aContext)
                    .getString(key, null);
        } catch (NullPointerException ignored) {
            return null;
        }
    }

    public static boolean getBool(@NonNull Context aContext,
                                  @NonNull String key) {
        return getBool(aContext, key, false);
    }

    public static boolean getBool(@NonNull Context aContext,
                                  @NonNull String key, boolean defaultVal) {
        return PreferenceManager.getDefaultSharedPreferences(aContext)
                .getBoolean(key, defaultVal);
    }

    public static boolean isExists(@NonNull Context aContext, @NonNull String key) {
        return PreferenceManager.getDefaultSharedPreferences(aContext)
                .contains(key);
    }

    public static void setBool(@NonNull Context aContext,
                               @NonNull String key, boolean value) {
        try {
            PreferenceManager.getDefaultSharedPreferences(aContext).edit()
                    .putBoolean(key, value).commit();
        } catch (OutOfMemoryError ignored) {

        }

    }

    public static void setString(@NonNull Context aContext, @NonNull String key,
                                 String value) {
        if (Utility.isSet(value)) {
            try {
                PreferenceManager.getDefaultSharedPreferences(aContext).edit()
                        .putString(key, value).commit();
            } catch (OutOfMemoryError ignored) {
            }
        } else {
            remove(aContext, key);
        }
    }

    /**
     * Set shared preferences by key
     *
     * @param aContext of the application
     * @param key      to setString
     * @param value    to setString
     */
    public static void setInt(@NonNull Context aContext,
                              @NonNull String key, int value) {
        try {
            PreferenceManager.getDefaultSharedPreferences(aContext).edit()
                    .putInt(key, value).commit();
        } catch (OutOfMemoryError ignored) {

        }

    }

    public static int getInt(@NonNull Context aContext, @NonNull String key) {
        return PreferenceManager.getDefaultSharedPreferences(aContext).getInt(
                key, 0);
    }

    public static int getInt(@NonNull Context aContext, @NonNull String key,
                             int defaultValue) {
        return PreferenceManager.getDefaultSharedPreferences(aContext).getInt(
                key, defaultValue);
    }

    public static void remove(@NonNull Context aContext, @NonNull String key) {
        PreferenceManager.getDefaultSharedPreferences(aContext).edit()
                .remove(key).commit();
    }

    public static void clear(@NonNull Context aContext) {

        SharedPreferences sharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(aContext);

        SharedPreferences.Editor prefEditor = sharedPrefs.edit();
        Map<String, ?> keys = sharedPrefs.getAll();

        String key = null;

        for (Map.Entry<String, ?> entry : keys.entrySet()) {
            key = checkNotNull(entry).getKey();
            if (key != null && !key.endsWith("_PERSISTANT")) {
                prefEditor.remove(key);
                // remove(aContext, key);
            }
        }
        prefEditor.apply();
        Crashlytics.setUserIdentifier(null);
    }
}
