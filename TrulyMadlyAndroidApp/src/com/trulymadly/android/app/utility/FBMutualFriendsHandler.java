package com.trulymadly.android.app.utility;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewStub;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.trulymadly.android.app.R;
import com.trulymadly.android.app.adapter.FbGridAdapter;
import com.trulymadly.android.app.modal.FbFriendModal;

import java.util.ArrayList;

/**
 * Created by avin on 13/10/16.
 */

public class FBMutualFriendsHandler implements View.OnClickListener {

    private ViewStub mMutualFriendsStub;
    private View mMutualFriendsContainer, mMutualFriendsMainView;
    private ProgressBar mProgressbar;
    private TextView mCommonConnTV;
    private RecyclerView mMutualFriendsRV;
    private FbGridAdapter mFbFriendsAdpater;
    private Context mContext;

    private ArrayList<FbFriendModal> mFbFriendsList;
    private String mCommonConnString = "";
    private View.OnClickListener mOnClickListener;
    private boolean isInflated = false;
    private View mMainFbFriendLayout;

    public FBMutualFriendsHandler(Context mContext, View mMutualFriendsContainer,
                                  View.OnClickListener mOnClickListener) {
        this.mContext = mContext;
        this.mMutualFriendsContainer = mMutualFriendsContainer;
        mMutualFriendsContainer.setOnClickListener(this);
        this.mOnClickListener = mOnClickListener;
        mMutualFriendsStub = (ViewStub) mMutualFriendsContainer.findViewById(R.id.sub_mutual_friend_layout);
    }

    public void setmCommonConnString(String mCommonConnString) {
        this.mCommonConnString = mCommonConnString;
        if (mCommonConnTV != null) {
            mCommonConnTV.setText(mCommonConnString);
        }
    }

    public void setmFbFriendsList(ArrayList<FbFriendModal> mFbFriendsList) {
        this.mFbFriendsList = mFbFriendsList;
    }

    public void toggleFbMutualFriendsList(boolean show, boolean isNewList, ArrayList<FbFriendModal> mFbFriendsList) {
        if (mFbFriendsList == null || mFbFriendsList.size() == 0) {
            mFbFriendsList = null;
            show = false;
        }
        if (show) {
            inflateView();
            toggleProgressbar(false);
            setmCommonConnString(mCommonConnString);

            if (mFbFriendsAdpater == null) {
                mFbFriendsAdpater = new FbGridAdapter(mContext, mFbFriendsList);
                mMutualFriendsRV.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
                mMutualFriendsRV.setAdapter(mFbFriendsAdpater);
            }

            if (isNewList) {
                mFbFriendsAdpater.setFriendList(mFbFriendsList);
                mFbFriendsAdpater.notifyDataSetChanged();
            }
            if (mFbFriendsList.size() == 1) {
                setmCommonConnString("1 " +
                        mContext.getResources().getString(R.string.common_connection));
            } else if (mFbFriendsList.size() > 1) {
                setmCommonConnString(mFbFriendsList.size() + " " +
                        mContext.getResources().getString(R.string.common_connections));
            }
            mMutualFriendsContainer.setVisibility(View.VISIBLE);
        } else {
            mMutualFriendsContainer.setVisibility(View.GONE);
        }
    }

    public void toggleProgressbar(boolean show) {
        inflateView();
        mProgressbar.setVisibility(show ? View.VISIBLE : View.GONE);
        mMainFbFriendLayout.setVisibility(show ? View.GONE : View.VISIBLE);
        if (show) {
            mMutualFriendsContainer.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View view) {
        if (mOnClickListener != null) {
            mOnClickListener.onClick(view);
        }
    }

    private void inflateView() {
        if (!isInflated) {
            View view = mMutualFriendsStub.inflate();
            view.setOnClickListener(this);
            mMainFbFriendLayout = mMutualFriendsContainer.findViewById(R.id.main_fb_friend_layout);
            mMutualFriendsRV = (RecyclerView) mMutualFriendsContainer.findViewById(R.id.mutual_friend_gridview);
            mCommonConnTV = (TextView) mMutualFriendsContainer.findViewById(R.id.common_connection_tv);
            mProgressbar = (ProgressBar) mMutualFriendsContainer.findViewById(R.id.fb_friend_loader);
            mMutualFriendsContainer.setVisibility(View.VISIBLE);
            isInflated = true;
        }
    }
}
