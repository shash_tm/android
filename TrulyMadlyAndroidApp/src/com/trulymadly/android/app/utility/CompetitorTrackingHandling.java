package com.trulymadly.android.app.utility;

import android.content.Context;

import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.json.ConstantsSP;

/**
 * Created by udbhav on 10/11/15.
 */
public class CompetitorTrackingHandling {
    public static void trackCompetitors(Context ctx) {
        if (!SPHandler.getBool(ctx, ConstantsSP.SHARED_KEYS_COMPS_TRACKED)) {
            trkComp(ctx, "Woo", "com.u2opia.woo");
            trkComp(ctx, "iCrushiFlush", "com.app.icrushiflushapp");
            trkComp(ctx, "Vee", "com.ongraph.vee");
            trkComp(ctx, "Shaadi", "com.shaadi.android");
            trkComp(ctx, "JeevanSathi", "com.jeevansathi.android");
            trkComp(ctx, "BharatMatrimony", "com.bharatmatrimony");
            trkComp(ctx, "okCupid", "com.okcupid.okcupid");
            trkComp(ctx, "Tinder", "com.tinder");
            trkComp(ctx, "Match.com", "com.match.android.matchmobile.asiapac");
            trkComp(ctx, "Paktor", "com.paktor");
            SPHandler.setBool(ctx, ConstantsSP.SHARED_KEYS_COMPS_TRACKED, true);
        }
        if (!SPHandler.getBool(ctx, ConstantsSP.SHARED_KEYS_COMPS_TRACKED_1)) {
            trkComp(ctx, "Hike", "com.bsb.hike");
            SPHandler.setBool(ctx, ConstantsSP.SHARED_KEYS_COMPS_TRACKED_1, true);
        }
    }

    private static void trkComp(Context ctx, String event_type, String packageName) {
        TrulyMadlyTrackEvent.trackEvent(ctx, TrulyMadlyEvent.TrulyMadlyActivities.competitors, event_type, 0, ActivityHandler.appInstalledOrNot(ctx, packageName) ? "yes" : "no", null, true);
    }
}
