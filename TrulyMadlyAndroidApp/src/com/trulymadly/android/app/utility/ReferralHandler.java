package com.trulymadly.android.app.utility;

import android.content.Context;

import com.crashlytics.android.Crashlytics;
import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.json.ConstantsSP;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/**
 * Created by avin on 11/01/17.
 */

public class ReferralHandler {
    private static final String UTM_SOURCE = "utm_source";
    private static final String UTM_MEDIUM = "utm_medium";
    private static final String UTM_TERM = "utm_term";
    private static final String UTM_CONTENT = "utm_content";
    private static final String UTM_CAMPAIGN = "utm_campaign";
    private static final String GCL_ID = "gclid";

    public static boolean checkAndLoadUrl(Context context, ACTION_POSITION actionPosition){
        if(actionPosition != null) {
            String referralString = SPHandler.getString(context, ConstantsSP.SHARED_KEYS_INSTALL_REFERRER);
            boolean isAlreadyShown = SPHandler.getBool(context, actionPosition + ConstantsSP.SHARED_KEYS_ACTION, false);
            if (Utility.isSet(referralString) && !isAlreadyShown) {
                JSONObject referralJSON = Utility.convertUrlParamsToJson(referralString);
                if (referralJSON != null) {
                    String unescUtmCampaignString = referralJSON.optString(UTM_CONTENT);
                    if (Utility.isSet(unescUtmCampaignString)) {
                        try {
                            String utmCampaignString = URLDecoder.decode(unescUtmCampaignString, "UTF-8");
                            if (Utility.isSet(utmCampaignString)) {
                                JSONObject utmCampaignObject = Utility.convertUrlParamsToJson(utmCampaignString, "@");
                                if (utmCampaignObject != null) {
                                    String url = utmCampaignObject.optString(actionPosition.name());
                                    TmLogger.d("REFERRER", url);
                                    if (Utility.isSet(url)) {
                                        TrulyMadlyTrackEvent.trackEvent(context, TrulyMadlyEvent.TrulyMadlyActivities.referrer, TrulyMadlyEvent.TrulyMadlyEventTypes.webview_open, 0, actionPosition.name(), null, true);
                                        SPHandler.setBool(context, actionPosition + ConstantsSP.SHARED_KEYS_ACTION, true);
                                        ActivityHandler.startWebViewUrlActivity(context, null, url, true, false, false);
                                    }
                                }
                            }
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                            Crashlytics.logException(e);
                        }
                    }
                }
            }
        }

        return false;
    }

    public enum ACTION_POSITION {
        on_install, on_registration_start, on_registration_end
    }
}
