package com.trulymadly.android.app.utility;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.trulymadly.android.app.R;
import com.trulymadly.android.app.adapter.BuySparkDialogViewPagerAdapter;
import com.trulymadly.android.app.adapter.SelectpackagesAdapter;
import com.trulymadly.android.app.fragments.BuySparkEventListener;
import com.trulymadly.android.app.modal.ImageAndText;
import com.trulymadly.android.app.modal.SelectPackageModal;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;

import static butterknife.ButterKnife.findById;

/**
 * Created by avin on 13/01/17.
 */

public class TMSelectPackagesUIHandler2 {
    private Context mContext;
    private View mMasterView, mMainScreen;
    private View mPaymentSuccessScreen, mOkFAB, mRestortePurchasesTV;

    private ViewPager mSelectBuyViewPager;
    private CirclePageIndicator mSelectBuyCirclePageIndicator;
    private BuySparkDialogViewPagerAdapter mBuySelectViewPagerAdapter;

    private ArrayList<String> mDescStrings;
    private ArrayList<Integer> mDescImages;
    private ArrayList<ImageAndText> mImageAndTexts;

    private Timer mTimer;
    private int mCurrentPage = 0;
    private boolean isFromAutoScroll = true;
    private ViewPager.OnPageChangeListener mOnPageChangeListener;
    private WeakReference<Context> mContextWeakReference;

    private TextView mCongratsTV, mYouAreSelectMemberTV, mEnjoyExperienceTV, mLetUsHandpickTV;

    private ArrayList<SelectPackageModal> mSelectPackageModals;
    private View.OnClickListener mOnClickListener;

    private RecyclerView mSelectPackagesRV;
    private LinearLayoutManager mLinearLayoutManager;
    private SelectpackagesAdapter mSelectpackagesAdapter;

    private View mMatchGuaranteeTncContainer, mMatchGuaranteeTncTV, mMatchGuaranteeTncOkTV;
    private BuySparkEventListener mBuySparkEventListener;
    private String mMatchId;

    public TMSelectPackagesUIHandler2(Context mContext, View mMasterView,
                                      ArrayList<SelectPackageModal> mSelectPackageModals,
                                      View.OnClickListener mOnClickListener,
                                      String mMatchId,
                                      BuySparkEventListener buySparkEventListener) {
        this.mContext = mContext;
        this.mMasterView = mMasterView;
        this.mSelectPackageModals = mSelectPackageModals;
        this.mOnClickListener = mOnClickListener;
        this.mBuySparkEventListener = buySparkEventListener;
        this.mMatchId = mMatchId;

        mMainScreen = findById(mMasterView, R.id.main_screen);

        mMatchGuaranteeTncContainer = findById(mMasterView, R.id.match_guarantee_tnc_container);
        mMatchGuaranteeTncOkTV = findById(mMasterView, R.id.match_guarantee_tnc_ok_tv);
        mMatchGuaranteeTncOkTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMatchGuaranteeTncContainer.setVisibility(View.GONE);
            }
        });
        mMatchGuaranteeTncTV = findById(mMasterView, R.id.match_guarantee_tnc_tv);
        mMatchGuaranteeTncTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMatchGuaranteeTncContainer.setVisibility(View.VISIBLE);
            }
        });
        mSelectPackagesRV = findById(mMasterView, R.id.select_packages_rv);
        mLinearLayoutManager = new LinearLayoutManager(mContext);
        mSelectpackagesAdapter = new SelectpackagesAdapter(mContext, mSelectPackageModals, mMatchId, mBuySparkEventListener);
        mSelectPackagesRV.setLayoutManager(mLinearLayoutManager);
        mSelectPackagesRV.setAdapter(mSelectpackagesAdapter);

        mMatchGuaranteeTncTV.setVisibility(View.GONE);
        for(SelectPackageModal selectPackageModal : mSelectPackageModals){
            if(selectPackageModal.isMatchGauranteed()){
                mMatchGuaranteeTncTV.setVisibility(View.VISIBLE);
                break;
            }
        }

        mSelectBuyViewPager = findById(mMasterView, R.id.select_buy_viewpager);
        mSelectBuyCirclePageIndicator = findById(mMasterView, R.id.select_buy_viewpager_indicator);

        ArrayList<String> mDescStrings = getDescStrings();
        ArrayList<Integer> mDescImages = getDescImages();

        mContextWeakReference = new WeakReference<>(mContext);

        mImageAndTexts = new ArrayList<>();
        for (int i = 0; i < mDescStrings.size(); i++) {
            ImageAndText imageAndText = new ImageAndText();
            imageAndText.setmText(mDescStrings.get(i));
            imageAndText.setmDrawable(mDescImages.get(i));
            mImageAndTexts.add(imageAndText);
        }

        mBuySelectViewPagerAdapter = new BuySparkDialogViewPagerAdapter(mImageAndTexts, mContext,
                ActivityCompat.getColor(mContext, R.color.tm_select_text_color));
        mSelectBuyViewPager.setAdapter(mBuySelectViewPagerAdapter);

        mSelectBuyCirclePageIndicator.setStrokeColor(
                ActivityCompat.getColor(mContext, R.color.gray_63));
        mSelectBuyCirclePageIndicator.setFillColor(
                ActivityCompat.getColor(mContext, R.color.gray_63));
        mSelectBuyCirclePageIndicator.setViewPager(mSelectBuyViewPager);


        if (mOnPageChangeListener == null) {
            mOnPageChangeListener = new ViewPager.SimpleOnPageChangeListener() {
                @Override
                public void onPageSelected(int position) {
                    super.onPageSelected(position);
                    if (!isFromAutoScroll) {
                        if (mTimer != null) {
                            mTimer.cancel();
                        }
                    }

                    isFromAutoScroll = false;
                }
            };
        }
        mSelectBuyViewPager.addOnPageChangeListener(mOnPageChangeListener);
        startTimer();



        mRestortePurchasesTV = findById(mMasterView, R.id.restore_purchases_tv);
        mRestortePurchasesTV.setOnClickListener(mOnClickListener);

        mPaymentSuccessScreen = findById(mMasterView, R.id.payment_success_screen);
        mPaymentSuccessScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        mCongratsTV = findById(mMasterView, R.id.congrats_tv);
        mLetUsHandpickTV = findById(mMasterView, R.id.let_us_handpick_tv);
        mYouAreSelectMemberTV = findById(mMasterView, R.id.you_are_tm_select_member_tv);
        mEnjoyExperienceTV = findById(mMasterView, R.id.enjoy_all_new_tm_tv);
        mOkFAB = findById(mMasterView, R.id.ok_fab);
        mOkFAB.setOnClickListener(mOnClickListener);

    }

    private ArrayList<String> getDescStrings() {
        if (mDescStrings == null) {
            String[] strings = mContext.getResources().getStringArray(R.array.select_desc_packages);
            mDescStrings = new ArrayList<>(Arrays.asList(strings));
        }

        return mDescStrings;
    }

    private ArrayList<Integer> getDescImages() {
        if (mDescImages == null) {
            TypedArray typedArray = mContext.getResources().obtainTypedArray(R.array.select_desc_packages_drawables);
            mDescImages = new ArrayList<>();
            for (int i = 0; i < typedArray.length(); i++) {
                mDescImages.add(i, typedArray.getResourceId(i, -1));
            }
            typedArray.recycle();
        }

        return mDescImages;
    }

    private void startTimer() {
        mTimer = new Timer();
        TimerTask mTimerTask = new TimerTask() {
            @Override
            public void run() {
                Context context = mContextWeakReference.get();
                if (mSelectBuyViewPager != null && mImageAndTexts != null && mImageAndTexts.size() > 1
                        && context != null) {
                    mCurrentPage = (mCurrentPage + 1) % mImageAndTexts.size();
                    mSelectBuyViewPager.post(new Runnable() {
                        @Override
                        public void run() {
                            if (mSelectBuyViewPager != null) {
                                isFromAutoScroll = true;
                                mSelectBuyViewPager.setCurrentItem(mCurrentPage, true);
                            }
                        }
                    });
                } else {
                    mTimer.cancel();
                }
            }
        };

        mTimer.schedule(mTimerTask, 5000, 5000);
    }

    public void toggleSuccessScreen(boolean show, boolean isFresh) {
        mCongratsTV.setText(Html.fromHtml(String.format(mContext.getString(R.string.congrats_selectmember),
                Utility.getMyName(mContext))));
        if (isFresh) {
            mYouAreSelectMemberTV.setText(Html.fromHtml(String.valueOf(mContext.getText(R.string.you_are_now_select_member))));
            mEnjoyExperienceTV.setText(R.string.all_new_tm_select);
            mEnjoyExperienceTV.setVisibility(View.VISIBLE);
        } else {
            mYouAreSelectMemberTV.setText(R.string.tm_select_extended);
            mEnjoyExperienceTV.setVisibility(View.GONE);
        }
        mPaymentSuccessScreen.setVisibility((show) ? View.VISIBLE : View.GONE);
    }

    public void toggleMainScreen(boolean show) {
        mMainScreen.setVisibility((show) ? View.VISIBLE : View.GONE);
    }

    public void toggleRestorePurchases(boolean show) {
        mRestortePurchasesTV.setVisibility((show) ? View.VISIBLE : View.GONE);
    }

    public void onDestroy() {
        if (mTimer != null) {
            mTimer.cancel();
        }
    }
}
