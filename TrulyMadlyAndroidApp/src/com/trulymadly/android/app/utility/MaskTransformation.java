package com.trulymadly.android.app.utility;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.Drawable;

import com.squareup.picasso.Transformation;

/**
 * Created by avin on 14/01/16.
 */
public class MaskTransformation implements Transformation {

    private static final Paint mMaskingPaint = new Paint();

    static {
        mMaskingPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
    }

    private final Context mContext;
    private final int mMaskId;

    /**
     * @param maskId If you change the mask file, please also rename the mask file, or Glide will getBool
     *               the cache with the old mask. Because getId() return the same values if using the
     *               same make file name. If you have a good idea please tell us, thanks.
     */
    public MaskTransformation(Context context, int maskId) {
        mContext = context.getApplicationContext();
        mMaskId = maskId;
    }

    @Override
    public Bitmap transform(Bitmap source) {
        int width = source.getWidth();
        int height = source.getHeight();

        Bitmap result = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        try {
            Canvas canvas = new Canvas(result);
            Drawable mask = Utility.getMaskDrawable(mContext, mMaskId);
            mask.setBounds(0, 0, width, height);
            mask.draw(canvas);
            canvas.drawBitmap(source, 0, 0, mMaskingPaint);
            source.recycle();
            return result;
        } catch (NullPointerException ignored) {
            return source;
        }

    }

    @Override
    public String key() {
        return "MaskTransformation(maskId=" + mContext.getResources().getResourceEntryName(mMaskId)
                + ")";
    }
}
