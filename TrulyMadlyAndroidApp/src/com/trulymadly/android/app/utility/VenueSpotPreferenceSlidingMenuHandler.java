package com.trulymadly.android.app.utility;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.squareup.picasso.Picasso;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.listener.SaveZonesInterface;
import com.trulymadly.android.app.modal.DateSpotZone;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes.filter_clicked;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes.nearby_filter_clicked;

/**
 * Created by deveshbatra on 3/31/16.
 */
public class VenueSpotPreferenceSlidingMenuHandler {
    private static final String ZONE_DOESNT_MATTER_ID = "-1";
    private static final String NEAR_BY_ID = "-2";
    private final SaveZonesInterface saveZonesInterface;
    private final LayoutInflater inflater;
    private final DateSpotZone dateSpotZoneDoesntMatter;
    private final DateSpotZone nearByZone;
    private final ArrayList<String> previousSelectedZoneIds = new ArrayList<>();
    private final Activity aActivity;
    private final LocationHelper locationHelper;
    private final String event_status;
    private final String eventActivity;
    @BindView(R.id.date_spot_preferences_bg)
    ImageView date_spot_preferences_bg;
    @BindView(R.id.date_spot_preference_zone_container)
    LinearLayout date_spot_preference_zone_container;
    @BindView(R.id.date_spot_preference_city_name_tv)
    TextView date_spot_preference_city_name_tv;
    @BindView(R.id.date_spot_preference_apply)
    View date_spot_preference_apply;
    @BindView(R.id.near_by_item)
    CheckBox nearByCheckBox;
    private SlidingMenu menu = null;
    private ArrayList<String> selectedZoneIds = new ArrayList<>();
    private ArrayList<DateSpotZone> zones;
    private CheckBox defaultZone;
    private CompoundButton.OnCheckedChangeListener onNearByCheckChangedListener, onZoneCheckChangedListener;
    private boolean isNearbyChecked = false;

    public VenueSpotPreferenceSlidingMenuHandler(Activity act, LocationHelper locationHelper, String eventActivity) {
        this(act, locationHelper, eventActivity, null);
    }

    public VenueSpotPreferenceSlidingMenuHandler(Activity act, LocationHelper locationHelper, String eventActivity, String event_status) {
        aActivity = act;
        this.locationHelper = locationHelper;
        this.eventActivity = eventActivity;
        this.event_status = event_status;

        if (act instanceof SaveZonesInterface) {
            this.saveZonesInterface = (SaveZonesInterface) act;
        } else {
            this.saveZonesInterface = null;
        }
        inflater = (LayoutInflater) aActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        dateSpotZoneDoesntMatter = new DateSpotZone();
        dateSpotZoneDoesntMatter.setZoneId(ZONE_DOESNT_MATTER_ID);
        dateSpotZoneDoesntMatter.setName(aActivity.getResources().getString(R.string.doesnt_matter));

        nearByZone = new DateSpotZone();
        nearByZone.setZoneId(NEAR_BY_ID);
        nearByZone.setName(aActivity.getResources().getString(R.string.near_by));

        try {
            menu = new SlidingMenu(aActivity);
            menu.setMode(SlidingMenu.RIGHT);
            menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
            menu.setBehindScrollScale(0);
            menu.setBehindOffsetRes(R.dimen.date_spot_preference_right_offset);
            menu.setFadeDegree(.35f);
            menu.setFadeEnabled(true);
            menu.attachToActivity(aActivity, SlidingMenu.SLIDING_CONTENT, true);
            menu.setMenu(R.layout.venue_list_preference_menu);

        } catch (InflateException | VerifyError | OutOfMemoryError e) {
            aActivity.finish();
            return;
        }
        ButterKnife.bind(this, menu);
        Picasso.with(aActivity)
                .load(R.drawable.map_blur_bg).noFade().config(Bitmap.Config.RGB_565).into(date_spot_preferences_bg);

        menu.setOnOpenListener(new SlidingMenu.OnOpenListener() {
            @Override
            public void onOpen() {
                TmLogger.d("SLIDING MENU", "On open");
            }
        });

        if (act instanceof SlidingMenu.OnOpenedListener) {
            menu.setOnOpenedListener((SlidingMenu.OnOpenedListener) act);
        }
        if (act instanceof SlidingMenu.OnClosedListener) {
            menu.setOnClosedListener((SlidingMenu.OnClosedListener) act);
        }
        if (act instanceof SlidingMenu.OnCloseListener) {
            menu.setOnCloseListener((SlidingMenu.OnCloseListener) act);
        }

        onNearByCheckChangedListener = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                onNearByCheckChanged(isChecked);
            }
        };

        onZoneCheckChangedListener = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                onZoneCheckChanged(buttonView, isChecked);
            }
        };
    }

    public ArrayList<String> getPreviousSelectedZoneIds() {
        return previousSelectedZoneIds;
    }

    private void setPreviousSelectedZoneIds(ArrayList<String> previousSelectedZoneIds) {
        this.previousSelectedZoneIds.clear();
        if (previousSelectedZoneIds != null) {
            for (int i = 0; i < previousSelectedZoneIds.size(); i++)
                this.previousSelectedZoneIds.add(previousSelectedZoneIds.get(i));
        }
    }

    public SlidingMenu getMenu() {
        return menu;
    }

    public int setZoneData(String cityName, ArrayList<DateSpotZone> zones) {
        this.zones = zones;
        date_spot_preference_city_name_tv.setText(cityName);
        date_spot_preference_zone_container.removeAllViews();
        selectedZoneIds = new ArrayList<>();

        nearByCheckBox.setTag(nearByZone);
        nearByCheckBox.setOnCheckedChangeListener(onNearByCheckChangedListener);

        defaultZone = addZone(dateSpotZoneDoesntMatter);
        boolean isAnySelected = false;
        if (zones != null) {
            for (DateSpotZone zone : zones) {
                addZone(zone);
                if (zone.isSelected()) {
                    if (!selectedZoneIds.contains(zone.getZoneId())) {
                        selectedZoneIds.add(zone.getZoneId());
                    }
                    isAnySelected = true;
                }
            }
        }
        int size = selectedZoneIds.size();
        if (!isNearbyChecked && !isAnySelected) {
            size = 0;
            checkDefault(true, true);
        }
        date_spot_preference_apply.setEnabled(false);
        return size;
    }

    private CheckBox addZone(DateSpotZone zone) {

        if (inflater != null && zone != null) {

            CheckBox v = (CheckBox) inflater.inflate(R.layout.location_preference_zone_item, date_spot_preference_zone_container, false);
            v.setChecked(zone.isSelected());
            v.setText(zone.getName());
            v.setTag(zone);
            v.setOnCheckedChangeListener(onZoneCheckChangedListener);
            date_spot_preference_zone_container.addView(v);
            return v;
        }
        return null;
    }


    private void onNearByCheckChanged(boolean isChecked) {
        if (Utility.isSet(event_status)) {
            TrulyMadlyTrackEvent.trackEvent(aActivity,
                    eventActivity, nearby_filter_clicked, 0, event_status
                    , null, true);
        }
        isNearbyChecked = isChecked;
        setPreviousSelectedZoneIds(selectedZoneIds);
        uncheckAllNonDefault(false);
        checkDefault(false, false);

        if (isChecked) {
            locationHelper.getLocation();
            selectedZoneIds.clear();
            selectedZoneIds.add(NEAR_BY_ID);
            saveZonesInterface.onSaveClicked(selectedZoneIds);

        } else {
            selectedZoneIds.clear();
            selectedZoneIds.add(ZONE_DOESNT_MATTER_ID);
            saveZonesInterface.onSaveClicked(selectedZoneIds);
        }
    }

    private void onZoneCheckChanged(CompoundButton v, boolean isChecked) {
        if (Utility.isSet(event_status)) {
            TrulyMadlyTrackEvent.trackEvent(aActivity,
                    eventActivity, filter_clicked, 0, event_status
                    , null, true);
        }
        DateSpotZone zone = (DateSpotZone) v.getTag();
        String id = zone.getZoneId();
        if (isChecked) {
            checkNearBy(false);
            if (!selectedZoneIds.contains(id)) {
                selectedZoneIds.add(id);
            }
            if (ZONE_DOESNT_MATTER_ID.equals(id)) {
                uncheckAllNonDefault(false);
            } else {
                checkDefault(false, false);
            }
        } else {
            if (selectedZoneIds.contains(id)) {
                selectedZoneIds.remove(id);
            }
        }
        if (selectedZoneIds.size() == 0) {
            checkDefault(true, false);
            checkNearBy(false);
        }

        date_spot_preference_apply.setEnabled(true);
    }

    public void checkDefault(boolean isChecked, boolean withListener) {
        if (selectedZoneIds.contains(ZONE_DOESNT_MATTER_ID)) {
            selectedZoneIds.remove(ZONE_DOESNT_MATTER_ID);
        }
        if (isChecked) {
            selectedZoneIds.add(ZONE_DOESNT_MATTER_ID);
        }
        if (!withListener) {
            defaultZone.setOnCheckedChangeListener(null);
        }
        defaultZone.setChecked(isChecked);
        if (!withListener) {
            defaultZone.setOnCheckedChangeListener(onZoneCheckChangedListener);
        }
    }

    public void checkNearBy(boolean isChecked) {
        isNearbyChecked = isChecked;
        nearByCheckBox.setOnCheckedChangeListener(null);
        nearByCheckBox.setChecked(isChecked);
        nearByCheckBox.setOnCheckedChangeListener(onNearByCheckChangedListener);
    }


    public void uncheckAllNonDefault(boolean withListener) {
        for (DateSpotZone zone : zones) {
            if (selectedZoneIds.contains(zone.getZoneId())) {
                selectedZoneIds.remove(zone.getZoneId());
            }
            CheckBox checkbox = ((CheckBox) date_spot_preference_zone_container.findViewWithTag(zone));
            if (!withListener) {
                checkbox.setOnCheckedChangeListener(null);
            }
            checkbox.setChecked(false);
            if (!withListener) {
                checkbox.setOnCheckedChangeListener(onZoneCheckChangedListener);
            }
        }
        if (!selectedZoneIds.contains(ZONE_DOESNT_MATTER_ID)) {
            selectedZoneIds.add(ZONE_DOESNT_MATTER_ID);
            if (!defaultZone.isChecked()) {
                checkDefault(true, false);
            }
        }
    }


    @OnClick(R.id.date_spot_preference_apply)
    public void onApplyClicked() {
        TmLogger.d("SLIDING MENU", selectedZoneIds.toString());
        if (saveZonesInterface != null) {
            setPreviousSelectedZoneIds(selectedZoneIds);
            saveZonesInterface.onSaveClicked(selectedZoneIds);
        }

    }


//    @Override
//    public void onClick(View view) {
//
//        switch (view.getId()) {
//            case R.id.near_by_container:
//                LocationHelper locationHelper = new LocationHelper(aActivity);
//                locationHelper.getLocation();
//                uncheckAllNonDefault();
//                break;
//            default:
//                break;
//        }
//    }
}
