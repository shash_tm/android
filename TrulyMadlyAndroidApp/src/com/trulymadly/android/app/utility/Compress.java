package com.trulymadly.android.app.utility;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;

import java.io.File;
import java.io.FileOutputStream;

public class Compress {

	//@Trace(category = MetricCategory.IMAGE)
	private static int computeScale(double maxDimension, double reqDimension) {
		int scale = 1;
		if (maxDimension > reqDimension)
			scale = (int) Math.floor(maxDimension / reqDimension);

		return scale;
	}

	//@Trace(category = MetricCategory.IMAGE)
	public static File createFile(Bitmap bmap) throws Throwable {
		File storageDir = Utility.getPictureDirectory();

		File destinationFile = File.createTempFile(Utility.getTempFileName(), /* prefix */
				".jpg", /* suffix */
				storageDir /* directory */
		);
		FileOutputStream out = new FileOutputStream(destinationFile);
		bmap.compress(Bitmap.CompressFormat.JPEG, 80, out);
		out.flush();
		out.close();

		return destinationFile;
	}


	//@Trace(category = MetricCategory.IMAGE)
	private static File returnResizeFile(String filePath, int limit,
			int height, int width) throws Throwable {

		float scaleWidth, scaleHeight;

		File myFile;
		if (height > limit || width > limit) {
			if (width > height) {
				scaleHeight = scaleWidth = ((float) limit) / width;
				scaleHeight = scaleWidth;
			} else {
				scaleHeight = ((float) limit) / height;
				scaleWidth = scaleHeight;
			}
			// create matrix for the manipulation
			Matrix matrix = new Matrix();
			// resize the bit map
			matrix.postScale(scaleWidth, scaleHeight);
			// recreate the new Bitmap
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = false;
			Bitmap bmap = BitmapFactory.decodeFile(filePath, options);
			bmap = Bitmap.createBitmap(bmap, 0, 0, width, height, matrix, true);
			myFile = createFile(bmap);
			bmap.recycle();
		} else {
			myFile = new File(filePath);
		}
		return myFile;

	}

	//@Trace(category = MetricCategory.IMAGE)
	private static File returnResampleFile(String filePath, int limit,
			int height, int width) throws Throwable {
		File myFile;

		int maxDimension = height > width ? height : width;

		int scale = computeScale(maxDimension, limit);
		if (scale > 1) {
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inSampleSize = scale;
			options.inJustDecodeBounds = false;
			Bitmap bmap = BitmapFactory.decodeFile(filePath, options);
			myFile = createFile(bmap);
			bmap.recycle();
		} else {
			myFile = new File(filePath);
		}
		return myFile;
	}

	//@Trace(category = MetricCategory.IMAGE)
	public static File returnFile(String filePath, int limit) {
		File myFile;

		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(filePath, options);

		try {
			myFile = returnResizeFile(filePath, limit, options.outHeight,
					options.outWidth);
		} catch (Throwable t) {
			try {
				myFile = returnResampleFile(filePath, limit, options.outHeight,
						options.outWidth);
			} catch (Throwable t1) {
				myFile = new File(filePath);
			}
		}

		return myFile;
	}

	//@Trace(category = MetricCategory.IMAGE)
	public static File returnFile2(String filePath, int limit) {
		File myFile;

		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(filePath, options);

	    try {
				myFile = returnResampleFile(filePath, limit, options.outHeight,
						options.outWidth);
			} catch (Throwable t1) {
				myFile = new File(filePath);
			}
		return myFile;
	}
	
	
	
	
	
	

}
