package com.trulymadly.android.app.utility;

import android.content.Context;

import com.trulymadly.android.app.bus.ImageUploadedProgress;

import java.io.File;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.internal.Util;
import okio.BufferedSink;
import okio.Okio;
import okio.Source;

/**
 * Created by avin on 15/03/16.
 */
public class CountingFileRequestBody extends RequestBody {

    private static final int SEGMENT_SIZE = 2048; // okio.Segment.SIZE
    private static final String TAG = "CountingFileRequestBody";

    private final File file;
    private final String contentType;
    private final String mIdentifier;
    private final Context mContext;
    private int mProgress = 0;
    private boolean isCancelled = false;

    public CountingFileRequestBody(Context context, File file, String contentType,
                                   String mIdentifier) {
        this.file = file;
        this.mIdentifier = mIdentifier;
        this.contentType = contentType;
        mContext = context;
    }

    @Override
    public long contentLength() {
        return file.length();
    }

    @Override
    public MediaType contentType() {
        return MediaType.parse(contentType);
    }

    @Override
    public void writeTo(BufferedSink sink) throws IOException {
        Source source = null;
        try {
            source = Okio.source(file);
            long total = 0;
            long read;

            while (!isCancelled && (read = source.read(sink.buffer(), SEGMENT_SIZE)) != -1) {
                total += read;
                sink.flush();
                int curretProgress = Long.valueOf(total * 100 / contentLength()).intValue();
                mProgress = curretProgress;
                TmLogger.d(TAG, "Progress : " + mProgress);
                Utility.fireBusEvent(mContext, true, new ImageUploadedProgress(mIdentifier, curretProgress));
            }

            if (isCancelled) {
                mProgress = 0;
                TmLogger.d(TAG, "Progress : " + mProgress);
                Utility.fireBusEvent(mContext, true, new ImageUploadedProgress(mIdentifier, 0));
            }

        } finally {
            Util.closeQuietly(source);
        }
    }

    public void cancelUpload() {
        isCancelled = true;
    }

    public int getProgress() {
        return mProgress;
    }

}