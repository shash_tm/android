package com.trulymadly.android.app.utility;

import android.app.Activity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.trulymadly.android.app.R;

/**
 * Created by avin on 30/09/16.
 */

public class PhotoSliderHandler implements View.OnClickListener {
    private Activity mActivity;
    private View mSliderView, mBackgroundView;
    private View mOptionEditPhoto, mOptionMakeProfilePhoto, mOptionDeletePhoto, mOptionPreviewVideo,
            mOptionDeleteVideo, mOptionTakePhoto, mOptionTakeVideo, mOptionTakeFromGallery,
            mOptionTakeFromFacebook;

    private boolean isVisible = false;

    public PhotoSliderHandler(Activity mActivity, View mSliderView, View mBackgroundView) {
        this.mActivity = mActivity;
        this.mSliderView = mSliderView;
        this.mBackgroundView = mBackgroundView;

        mOptionEditPhoto = mSliderView.findViewById(R.id.edit_pic_layout);
        mOptionMakeProfilePhoto = mSliderView.findViewById(R.id.make_profile_pic_layout);
        mOptionDeletePhoto = mSliderView.findViewById(R.id.remove_pic_layout);
        mOptionDeleteVideo = mSliderView.findViewById(R.id.delete_video_layout);
        mOptionPreviewVideo = mSliderView.findViewById(R.id.preview_video_layout);
        mOptionTakePhoto = mSliderView.findViewById(R.id.take_from_camera_layout);
        mOptionTakeFromGallery = mSliderView.findViewById(R.id.add_from_gallery_layout);
        mOptionTakeVideo = mSliderView.findViewById(R.id.shoot_video_layout);
        mOptionTakeFromFacebook = mSliderView.findViewById(R.id.import_from_facebook_layout);

        mBackgroundView.setOnClickListener(this);
    }

    private boolean switchOptions(SliderMode sliderMode, boolean makeProfilePic) {
        mOptionEditPhoto.setVisibility(View.GONE);
        mOptionMakeProfilePhoto.setVisibility(View.GONE);
        mOptionDeletePhoto.setVisibility(View.GONE);
        mOptionDeleteVideo.setVisibility(View.GONE);
        mOptionPreviewVideo.setVisibility(View.GONE);
        mOptionTakePhoto.setVisibility(View.GONE);
        mOptionTakeFromGallery.setVisibility(View.GONE);
        mOptionTakeVideo.setVisibility(View.GONE);
        mOptionTakeFromFacebook.setVisibility(View.GONE);

        boolean toggleSlider = true;
        switch (sliderMode) {
//            case uploading:
//                toggleSlider = false;
//                break;
//            case processing:
//                toggleSlider = false;
//                break;

            case profile_clicked:
                mOptionEditPhoto.setVisibility(View.VISIBLE);
                break;
            case profile_add_clicked:
                mOptionTakePhoto.setVisibility(View.VISIBLE);
                mOptionTakeFromGallery.setVisibility(View.VISIBLE);
                mOptionTakeFromFacebook.setVisibility(View.VISIBLE);
                break;

            case other_add_clicked:
                mOptionTakePhoto.setVisibility(View.VISIBLE);
                mOptionTakeVideo.setVisibility(View.VISIBLE);
                mOptionTakeFromGallery.setVisibility(View.VISIBLE);
                mOptionTakeFromFacebook.setVisibility(View.VISIBLE);
                break;
            case other_video_clicked:
                mOptionDeleteVideo.setVisibility(View.VISIBLE);
                mOptionPreviewVideo.setVisibility(View.VISIBLE);
                break;
            case other_picture_clicked:
                mOptionEditPhoto.setVisibility(View.VISIBLE);
                if (makeProfilePic) {
                    mOptionMakeProfilePhoto.setVisibility(View.VISIBLE);
                }
                mOptionDeletePhoto.setVisibility(View.VISIBLE);
                break;

            case rejected_photo_clicked:
                mOptionDeletePhoto.setVisibility(View.VISIBLE);
                break;
            case rejected_video_clicked:
                mOptionDeleteVideo.setVisibility(View.VISIBLE);
                break;
        }

        return toggleSlider;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void hideSlider(boolean animate) {
        mSliderView.setVisibility(View.GONE);
        mBackgroundView.setVisibility(View.GONE);
        if (animate) {
            Animation slideDownAnimation = AnimationUtils.loadAnimation(mActivity, R.anim.slide_down);
            mSliderView.startAnimation(slideDownAnimation);
        }
        isVisible = false;
    }

    public void toggleSlider(SliderMode sliderMode, boolean makeVisible, boolean animate, boolean makeProfilePic) {
        if (makeVisible) {
            if (!switchOptions(sliderMode, makeProfilePic)) {
                return;
            }
            mBackgroundView.setVisibility(View.VISIBLE);
            mSliderView.setVisibility(View.VISIBLE);
            if (animate) {
                Animation slideUpAnimation = AnimationUtils.loadAnimation(mActivity, R.anim.slide_up);
                mSliderView.startAnimation(slideUpAnimation);
            }
        } else {
            hideSlider(animate);
        }
        isVisible = makeVisible;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sliding_background:
                hideSlider(true);
                break;
        }
    }

    public enum SliderMode {
        none,
        //        uploading, processing,
        profile_clicked, profile_add_clicked,
        other_video_clicked, other_picture_clicked, other_add_clicked,
        rejected_photo_clicked, rejected_video_clicked
    }
}
