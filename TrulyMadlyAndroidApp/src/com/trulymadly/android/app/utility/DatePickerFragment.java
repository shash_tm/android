package com.trulymadly.android.app.utility;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.InflateException;
import android.widget.DatePicker;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class DatePickerFragment extends DialogFragment implements
		DatePickerDialog.OnDateSetListener {

	private DatePickerDialog datePickerDialog = null;
	private Context ctx;
	private OnDateSetInterface onDateSetInterface;

    public DatePickerFragment() {

    }

    @SuppressLint("ValidFragment")
    public DatePickerFragment(Context c, OnDateSetInterface onDateSetInterface) {
        ctx = c;
        this.onDateSetInterface = onDateSetInterface;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		Bundle data = getArguments();
		if (datePickerDialog == null) {
			int d, m, y, max_d, max_m, max_y, min_d, min_m, min_y;

			Calendar c = Calendar.getInstance();
			max_d = c.get(Calendar.DAY_OF_MONTH);
			max_m = c.get(Calendar.MONTH);
			int MIN_AGE = 18;
			max_y = c.get(Calendar.YEAR) - MIN_AGE;
			
			min_d = c.get(Calendar.DAY_OF_MONTH);
			min_m = c.get(Calendar.MONTH);
			int MAX_AGE = 70;
			min_y = c.get(Calendar.YEAR) - MAX_AGE;

			if (data != null && data.getInt("dob_d") != 0) {
				d = data.getInt("dob_d");
				m = data.getInt("dob_m") - 1;
				y = data.getInt("dob_y");
			} else {
				d = max_d;
				m = max_m;
				int DEFAULT_AGE = 21;
				y = c.get(Calendar.YEAR) - DEFAULT_AGE;
			}

			try {
				datePickerDialog = createDatePicker(y, m, d, max_d, max_m,
						max_y, min_d, min_m,
						min_y);
			} catch (InflateException e) {
				getActivity().finish();
			}

		}
		return datePickerDialog;
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private DatePickerDialog createDatePicker(int y, int m, int d,
			final int max_d, final int max_m, final int max_y, final int min_d, final int min_m, final int min_y) {

		DatePickerDialog dialog = null;
		dialog = new DatePickerDialog(ctx, this, y, m, d);
		try {
			dialog.getDatePicker().setMaxDate(
					new GregorianCalendar(max_y, max_m, max_d)
							.getTimeInMillis());
			dialog.getDatePicker().setMinDate(new GregorianCalendar(min_y, min_m, min_d)
							.getTimeInMillis());
		} catch (IllegalArgumentException ignored) {

		}

		return dialog;
	}

	public void onDateSet(DatePicker view, int year, int month, int day) {
		onDateSetInterface.onDateSet(day, month + 1, year);
	}

	public interface OnDateSetInterface {
		void onDateSet(int day, int month, int year);
	}
}