package com.trulymadly.android.app.utility;

import android.content.Context;

import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.GetOkHttpRequestParams;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.billing.PaymentFor;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.ConstantsRF;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.listener.CustomOkHttpResponseHandler;
import com.trulymadly.android.app.modal.MySelectData;
import com.trulymadly.android.app.modal.SelectModePackageModal;
import com.trulymadly.android.app.modal.SelectPackageModal;
import com.trulymadly.android.app.modal.SelectQuizCompareModal;
import com.trulymadly.android.app.modal.SelectQuizModal;
import com.trulymadly.android.app.modal.SelectQuizQuesCompareResp;
import com.trulymadly.android.app.sqlite.RFHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;

import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities.quiz;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities.select;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventStatus.buy;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventStatus.fetch_fail;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventStatus.fetch_success;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventStatus.tm_error;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes.buy_trial;

/**
 * Created by avin on 20/10/16.
 */

public class TMSelectHandler {

    /////////////////////////////////////////////
    //Utility Methods
    /////////////////////////////////////////////

    public static boolean shouldShowSelectNudge(Context context) {
        String userId = Utility.getMyId(context);
        return isSelectEnabled(context) && !isSelectMember(context) &&
                RFHandler.getBool(context, userId, ConstantsRF.RIGID_FIELD_SELECT_NUDGE_ENABLED)
                && TimeUtils.isDifferenceGreater(RFHandler.getString(context, userId, ConstantsRF.RIGID_FIELD_SELECT_NUDGE_LAST_TS),
                RFHandler.getInt(context, userId, ConstantsRF.RIGID_FIELD_SELECT_NUDGE_FREQUENCY, 0) * 60 * 60 * 1000);
    }

    public static boolean isLikeAllowedOnSelect(Context context) {
        return RFHandler.getBool(context, Utility.getMyId(context), ConstantsRF.RIGID_FIELD_SELECT_ACTION_ALLOW_LIKE);
    }

    public static boolean isLikeBackAllowedOnSelect(Context context) {
        return RFHandler.getBool(context, Utility.getMyId(context), ConstantsRF.RIGID_FIELD_SELECT_ACTION_ALLOW_LIKEBACK);
    }

    public static boolean isSparkAllowedOnSelect(Context context) {
        return RFHandler.getBool(context, Utility.getMyId(context), ConstantsRF.RIGID_FIELD_SELECT_ACTION_ALLOW_SPARK);
    }

    /**
     * @param context Returns if the given user should be blocked on matches page to directly open the select quiz
     */
    public static boolean blockUser(Context context) {
        return isPaymentDone(context) && !isSelectQuizPlayed(context);
    }

    /**
     * @param context Returns if the given user has made the payment or not
     */
    public static boolean isPaymentDone(Context context) {
        return RFHandler.getBool(context, Utility.getMyId(context), ConstantsRF.RIGID_FIELD_SELECT_PAYMENT_DONE);
    }

    public static void setPaymentDone(Context context, boolean isPaymentDone) {
        RFHandler.insert(context, Utility.getMyId(context), ConstantsRF.RIGID_FIELD_SELECT_PAYMENT_DONE, isPaymentDone);
    }

    /**
     * @param context Returns if the given user is a select member or not
     */
    public static boolean isSelectMember(Context context) {
        return RFHandler.getBool(context, Utility.getMyId(context), ConstantsRF.RIGID_FIELD_IS_SELECT_MEMBER);
    }

//    /**
//     * @param context
//     */
//    public static void setSelectMember(Context context, boolean isSelectMember) {
//        RFHandler.insert(context, Utility.getMyId(context), ConstantsRF.RIGID_FIELD_IS_SELECT_MEMBER, isSelectMember);
//    }

    /**
     * @param context Returns the number of days left in the TMSelect membership
     */
    public static int getSelectDaysLeft(Context context) {
        return RFHandler.getInt(context, Utility.getMyId(context), ConstantsRF.RIGID_FIELD_SELECT_DAYS_LEFT, 0);
    }

    /**
     * @param context Returns the CTA text to be shown to the user in the side menu
     */
    public static String getSelectSideCta(Context context) {
        return RFHandler.getString(context, Utility.getMyId(context), ConstantsRF.RIGID_FIELD_SELECT_SIDE_CTA_TEXT);
    }

    /**
     * @param context Returns the CTA text to be shown to the user in the profile page
     */
    public static String getSelectProfileCta(Context context) {
        return RFHandler.getString(context, Utility.getMyId(context), ConstantsRF.RIGID_FIELD_SELECT_PROFILE_CTA);
    }

    /**
     * @param context
     * Returns the text to be shown to the user below the CTA in the profile page
     */
    public static String getSelectProfileCtaSubText(Context context) {
        return RFHandler.getString(context, Utility.getMyId(context), ConstantsRF.RIGID_FIELD_SELECT_PROFILE_CTA_SUBTEXT);
    }

    /**
     * @param context Returns the compatibility text default value
     */
    public static String getSelectCompatibilityTextDef(Context context) {
        return RFHandler.getString(context, Utility.getMyId(context),
                ConstantsRF.RIGID_FIELD_SELECT_COMP_TEXT_DEF);
    }

    /**
     * @param context Returns the compatibility image default value
     */
    public static String getSelectCompatibilityImageDef(Context context) {
        return RFHandler.getString(context, Utility.getMyId(context),
                ConstantsRF.RIGID_FIELD_SELECT_COMP_IMAGE_DEF);
    }

    /**
     * @param context Returns if the given user has played the select quiz or not
     */
    public static boolean isSelectQuizPlayed(Context context) {
        return RFHandler.getBool(context, Utility.getMyId(context), ConstantsRF.RIGID_FIELD_IS_SELECT_QUIZ_PLAYED);
    }

    public static void setSelectQuizPlayed(Context context, boolean isQuizPlayed) {
        RFHandler.insert(context, Utility.getMyId(context), ConstantsRF.RIGID_FIELD_IS_SELECT_QUIZ_PLAYED, isQuizPlayed);
    }

    /**
     * Checks if "Select" is enabled
     *
     * @param mContext
     */
    public static boolean isSelectEnabled(Context mContext) {
        return RFHandler.getBool(mContext, Utility.getMyId(mContext), ConstantsRF.RIGID_FIELD_SELECT_ENABLED);
    }


    /////////////////////////////////////////////
    //Utility Classes
    /////////////////////////////////////////////

    public static class PackagesHandler {
        private Context mContext;
        private PackagesHandler.PackagesHandlerListener mPackagesHandlerListener;

        public PackagesHandler(Context mContext, PackagesHandlerListener mPackagesHandlerListener) {
            this.mContext = mContext;
            this.mPackagesHandlerListener = mPackagesHandlerListener;
        }

        public void unregister() {
            mPackagesHandlerListener = null;
        }

//        private SelectModePackageModal getDummySelectModePackageModal() {
//            boolean isFreeTrial = true;
//            boolean isQuizAbOn = false;
//            ArrayList<SelectPackageModal> selectPackageModals = new ArrayList<>();
//            SelectPackageModal firstPackageModal = new SelectPackageModal("SKU_1", "SAVE 20%", false, 999, 1);
//            SelectPackageModal secondPackageModal = new SelectPackageModal("SKU_2", "POPULAR", true, 2199, 3);
//            SelectPackageModal thirdPackageModal = new SelectPackageModal("SKU_3", "SAVE 40%", false, 5199, 6);
//            selectPackageModals.add(firstPackageModal);
//            selectPackageModals.add(secondPackageModal);
//            selectPackageModals.add(thirdPackageModal);
//            return new SelectModePackageModal(isFreeTrial,
//                    isQuizAbOn, selectPackageModals);
//        }

        private SelectModePackageModal parsePackages(JSONArray jsonArray) {
            SelectModePackageModal selectModePackageModal = null;

            if (jsonArray != null && jsonArray.length() > 0) {
                selectModePackageModal = new SelectModePackageModal(false, false, null);
                ArrayList<SelectPackageModal> selectPackageModals = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    SelectPackageModal selectPackageModal = parseSelectPackage(jsonArray.optJSONObject(i));
                    if (selectPackageModal != null) {
                        selectPackageModals.add(selectPackageModal);
                    }
                }

                if (selectPackageModals.size() == 1) {
                    selectModePackageModal.setFreeTrial(selectPackageModals.get(0).getmPrice() == 0);
                    selectModePackageModal.setmFreeTrialCTASubText(selectPackageModals.get(0).getmFreeTrialExpiryText());
                    selectModePackageModal.setmFreeTrialCtaText(selectPackageModals.get(0).getmFreeTrialCtaText());
                } else {
                    selectModePackageModal.setmSelectPackageModals(selectPackageModals);
                }
            }

            return selectModePackageModal;
        }

        private SelectPackageModal parseSelectPackage(JSONObject jsonObject) {
            SelectPackageModal selectPackageModal = null;
            if (jsonObject != null) {
                JSONObject metaData = jsonObject.optJSONObject("metadata");
                String tag = null, perUnit = null, subText = null, freeTrialExpiryText = null;
                String freeTrialCtaText = mContext.getResources().getString(R.string.start_free_trial);
                int pricePerUnit = 0, discount = 0;
                boolean mostPopular = false, matchGaurantee = false;
                if (metaData != null) {
                    tag = metaData.optString("tag");
                    mostPopular = metaData.optBoolean("most_popular");
                    perUnit = metaData.optString("per_unit");
                    subText = metaData.optString("sub_text");
                    pricePerUnit = metaData.optInt("price_per_unit");
                    freeTrialExpiryText = metaData.optString("free_trial_expiry_text");
                    freeTrialCtaText = metaData.optString("free_trial_cta_text");

                    //3rd iteration
                    matchGaurantee = metaData.optBoolean("match_guarantee");
                    discount = metaData.optInt("discount", 0);
                }

                //Remove: Debug Code
//                matchGaurantee = new Random().nextBoolean();
//                if(matchGaurantee) {
//                    discount = new Random().nextInt(99);
//                }
//                try {
//                    jsonObject.put("title", new Random().nextInt(30) + " Days");
//                    if(new Random().nextBoolean()) {
//                        jsonObject.put("description", "Some random int : " + new Random().nextInt(500));
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }

                selectPackageModal = new SelectPackageModal(
                        jsonObject.optString("google_sku"),
                        tag,
                        mostPopular,
                        jsonObject.optInt("price"), pricePerUnit,
                        jsonObject.optInt("expiry_days"),
                        perUnit,
                        subText,
                        freeTrialCtaText,
                        freeTrialExpiryText,
                        jsonObject.optString("title"),
                        matchGaurantee,
                        discount,
                        jsonObject.optString("description"));
            }
            return selectPackageModal;
        }

        public void getPackages(final String mMatchId) {
            CustomOkHttpResponseHandler customOkHttpResponseHandler =
                    new CustomOkHttpResponseHandler(mContext) {
                        @Override
                        public void onRequestSuccess(JSONObject responseJSON) {
                            int responseCode = responseJSON.optInt("responseCode", 403);
                            boolean isSuccessfull = true;
                            String message = null;
                            SelectModePackageModal selectModePackageModal = null;
                            switch (responseCode) {
                                case 200:
                                    if (responseJSON.has("error")) {
                                        isSuccessfull = false;
                                        message = mContext.getString(R.string.SERVER_ERROR_SMALL);
                                        HashMap<String, String> eventInfo = new HashMap<>();
                                        String error = responseJSON.optString("error");
                                        eventInfo.put("error", error);
                                        if (Utility.isSet(mMatchId)) {
                                            eventInfo.put("match_id", mMatchId);
                                        } else {
                                            eventInfo.put("match_id", TrulyMadlyEvent.TrulyMadlyActivities.select);
                                        }
                                        TrulyMadlyTrackEvent.trackEvent(mContext, select, buy, 0, tm_error,
                                                eventInfo, true, true);
                                    } else {
                                        JSONArray packagesJsonArray = responseJSON.optJSONArray("spark_packages");
                                        selectModePackageModal = parsePackages(packagesJsonArray);
                                    }

                                    break;
                                default:
                                    HashMap<String, String> eventInfo = new HashMap<>();
                                    String error = responseJSON.optString("error");
                                    eventInfo.put("error", (Utility.isSet(error)) ? error : "Response : " + responseCode);
                                    if (Utility.isSet(mMatchId)) {
                                        eventInfo.put("match_id", mMatchId);
                                    } else {
                                        eventInfo.put("match_id", TrulyMadlyEvent.TrulyMadlyActivities.select);
                                    }
                                    TrulyMadlyTrackEvent.trackEvent(mContext, select, buy, 0, tm_error,
                                            eventInfo, true, true);
                                    isSuccessfull = false;
                                    message = mContext.getString(R.string.SERVER_ERROR_SMALL);
                                    break;
                            }

                            if (mPackagesHandlerListener != null) {
                                mPackagesHandlerListener.onGetPackages(isSuccessfull, selectModePackageModal, message);
                            }
                        }

                        @Override
                        public void onRequestFailure(Exception exception) {
                            HashMap<String, String> eventInfo = new HashMap<>();
                            eventInfo.put("error", exception.getMessage());
                            if (Utility.isSet(mMatchId)) {
                                eventInfo.put("match_id", mMatchId);
                            } else {
                                eventInfo.put("match_id", TrulyMadlyEvent.TrulyMadlyActivities.select);
                            }
                            TrulyMadlyTrackEvent.trackEvent(mContext, select, buy, 0, tm_error,
                                    eventInfo, true, true);

                            if (mPackagesHandlerListener != null) {
                                int messageId = R.string.whoops_no_internet;
                                if (!Utility.isNetworkFailed(mContext, exception)) {
                                    messageId = R.string.SERVER_ERROR_SMALL;
                                }
                                mPackagesHandlerListener.onGetPackages(false, null, mContext.getString(messageId));
                            }
                        }
                    };
            OkHttpHandler
                    .httpPost(mContext, ConstantsUrls.getSparksApiUrl(), GetOkHttpRequestParams
                            .getHttpRequestParams(
                                    Constants.HttpRequestType.GET_PACKAGES,
                                    PaymentFor.select.name()), customOkHttpResponseHandler);
        }

        public interface PackagesHandlerListener {
            void onGetPackages(boolean success, SelectModePackageModal selectModePackageModal, String errorMessage);
        }
    }

    /**
     * Class that handles everything related to Select Quiz:
     * 1. Get Quiz
     * 2. Submit Quiz
     * 3. Get compare report
     */
    public static class QuizHandler {
        private Context mContext;
        private QuizHandlerListener mQuizHandlerListener;

        public QuizHandler(Context mContext, QuizHandlerListener mQuizHandlerListener) {
            this.mContext = mContext;
            this.mQuizHandlerListener = mQuizHandlerListener;
        }

        private void clearQuizData() {
            RFHandler.insert(mContext, Utility.getMyId(mContext), ConstantsRF.RIGID_FIELD_SELECT_QUIZ_ID, "");
            RFHandler.insert(mContext, Utility.getMyId(mContext), ConstantsRF.RIGID_FIELD_SELECT_QUIZ_DATA, "");
        }

        public boolean isQuizAlreadyPlayed() {
            String quizId = RFHandler.getString(mContext, Utility.getMyId(mContext),
                    ConstantsRF.RIGID_FIELD_SELECT_QUIZ_ID);
            return Utility.isSet(quizId);
        }

        public void saveQuizData(SelectQuizModal selectQuizModal) {
            if (selectQuizModal != null) {
                RFHandler.insert(mContext, Utility.getMyId(mContext), ConstantsRF.RIGID_FIELD_SELECT_QUIZ_ID,
                        selectQuizModal.getmQuizId());
                JSONObject jsonObject = new JSONObject();
                for (SelectQuizModal.SelectQuizQuestionModal selectQuizQuestionModal :
                        selectQuizModal.getSelectQuizQuestionModals()) {
                    try {
                        jsonObject.put(selectQuizQuestionModal.getmQuesId(), selectQuizQuestionModal.getmSelectedOptionId());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                RFHandler.insert(mContext, Utility.getMyId(mContext), ConstantsRF.RIGID_FIELD_SELECT_QUIZ_DATA, jsonObject.toString());
            }
        }

        private SelectQuizModal checkAndSetSavedResponses(SelectQuizModal selectQuizModal) {
            SelectQuizModal savedSelectQuizModal = retrieveQuizData();
            if (savedSelectQuizModal != null) {
                if (Utility.stringCompare(savedSelectQuizModal.getmQuizId(), selectQuizModal.getmQuizId())) {
                    int totalCompleted = 0;
                    for (SelectQuizModal.SelectQuizQuestionModal selectQuizQuestionModal :
                            savedSelectQuizModal.getSelectQuizQuestionModals()) {
                        int index = selectQuizModal.getSelectQuizQuestionModals().indexOf(selectQuizQuestionModal);
                        if (index != -1) {
                            String response = selectQuizQuestionModal.getmSelectedOptionId();
                            selectQuizModal.getSelectQuizQuestionModals().get(index).setmSelectedOptionId(response);
                            totalCompleted += ((Utility.isSet(response)) ? 1 : 0);
                        }
                    }
                    selectQuizModal.setmTotalCompletedQuestions(totalCompleted);
                } else {
                    clearQuizData();
                }
            }

            return selectQuizModal;
        }

        private SelectQuizModal retrieveQuizData() {
            SelectQuizModal selectQuizModal = null;
            String quizId = RFHandler.getString(mContext, Utility.getMyId(mContext),
                    ConstantsRF.RIGID_FIELD_SELECT_QUIZ_ID);
            if (Utility.isSet(quizId)) {
                ArrayList<SelectQuizModal.SelectQuizQuestionModal> selectQuizQuestionModals = null;
                try {
                    JSONObject jsonObject = new JSONObject(RFHandler.getString(mContext, Utility.getMyId(mContext), ConstantsRF.RIGID_FIELD_SELECT_QUIZ_DATA));
                    selectQuizQuestionModals = new ArrayList<>();
                    Iterator<String> iter = jsonObject.keys();
                    while (iter.hasNext()) {
                        String key = iter.next();
                        String selectedOption = jsonObject.optString(key);
                        selectQuizQuestionModals.add(new SelectQuizModal.SelectQuizQuestionModal(key, selectedOption));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (selectQuizQuestionModals != null && selectQuizQuestionModals.size() > 0) {
                    selectQuizModal = new SelectQuizModal(quizId, selectQuizQuestionModals);
                }
            }

            return selectQuizModal;
        }

        public void fetchQuizData() {
            CustomOkHttpResponseHandler customOkHttpResponseHandler =
                    new CustomOkHttpResponseHandler(mContext) {
                        @Override
                        public void onRequestSuccess(JSONObject responseJSON) {
                            int responseCode = responseJSON.optInt("responseCode");
                            SelectQuizModal selectQuizModal = null;
                            boolean isSuccessfull = true;
                            String message = null;
                            HashMap<String, String> eventInfo = null;
                            String error = null;
                            switch (responseCode) {
                                case 200:
                                    selectQuizModal = parseGetQuizResponse(mContext, responseJSON);
                                    if (selectQuizModal != null) {
                                        checkAndSetSavedResponses(selectQuizModal);
                                    } else {
                                        isSuccessfull = false;
                                        message = mContext.getString(R.string.SERVER_ERROR_SMALL);
                                    }
                                    break;

                                default:
                                    eventInfo = new HashMap<>();
                                    eventInfo.put("error", "ResponseCode : " + responseCode);
                                    isSuccessfull = false;
                                    message = mContext.getString(R.string.SERVER_ERROR_SMALL);
                                    break;
                            }

                            TrulyMadlyTrackEvent.trackEvent(mContext, select, quiz, 0, (isSuccessfull)
                                            ? fetch_success : fetch_fail,
                                    eventInfo, true, true);

                            if (mQuizHandlerListener != null) {
                                mQuizHandlerListener.onQuizFetch(isSuccessfull, selectQuizModal, message);
                            }
                        }

                        @Override
                        public void onRequestFailure(Exception exception) {
                            HashMap<String, String> eventInfo = new HashMap<>();
                            eventInfo.put("error", exception.getMessage());
                            TrulyMadlyTrackEvent.trackEvent(mContext, select, quiz, 0, fetch_fail,
                                    eventInfo, true, true);
                            if (mQuizHandlerListener != null) {
                                int messageId = R.string.whoops_no_internet;
                                if (!Utility.isNetworkFailed(mContext, exception)) {
                                    messageId = R.string.SERVER_ERROR_SMALL;
                                }
                                mQuizHandlerListener.onQuizFetch(false, null, mContext.getString(messageId));
                            }
                        }
                    };
            OkHttpHandler
                    .httpPost(mContext, ConstantsUrls.getSelectApiUrl(), GetOkHttpRequestParams
                            .getHttpRequestParams(
                                    Constants.HttpRequestType.GET_QUIZ,
                                    null), customOkHttpResponseHandler);
        }

        public void submitQuizData(SelectQuizModal selectQuizModal) {
            CustomOkHttpResponseHandler customOkHttpResponseHandler =
                    new CustomOkHttpResponseHandler(mContext) {
                        @Override
                        public void onRequestSuccess(JSONObject responseJSON) {
                            int responseCode = responseJSON.optInt("responseCode");
                            boolean isSuccessfull = true;
                            String message = null;
                            switch (responseCode) {
                                case 200:
                                    break;

                                default:
                                    isSuccessfull = false;
                                    message = mContext.getString(R.string.SERVER_ERROR_SMALL);
                                    break;
                            }

                            if (mQuizHandlerListener != null) {
                                mQuizHandlerListener.onQuizSubmit(isSuccessfull, message);
                            }
                        }

                        @Override
                        public void onRequestFailure(Exception exception) {
                            if (mQuizHandlerListener != null) {
                                int messageId = R.string.whoops_no_internet;
                                if (!Utility.isNetworkFailed(mContext, exception)) {
                                    messageId = R.string.SERVER_ERROR_SMALL;
                                }
                                mQuizHandlerListener.onQuizSubmit(false, mContext.getString(messageId));
                            }
                        }
                    };
            OkHttpHandler
                    .httpPost(mContext, ConstantsUrls.getSelectApiUrl(), GetOkHttpRequestParams
                            .getHttpRequestParams(
                                    Constants.HttpRequestType.SUBMIT_QUIZ,
                                    selectQuizModal), customOkHttpResponseHandler);
        }

        private SelectQuizModal parseGetQuizResponse(Context mContext, JSONObject responseJSON) {
            SelectQuizModal selectQuizModal = null;

            if (responseJSON != null) {
                ArrayList<SelectQuizModal.SelectQuizQuestionModal> selectQuizQuestionModals =
                        parseQuestions(responseJSON.optJSONArray("questions"));
                if (selectQuizQuestionModals != null && selectQuizQuestionModals.size() > 0) {
                    selectQuizModal = new SelectQuizModal(responseJSON.optString("quiz_id"), selectQuizQuestionModals);
                }
            }

            return selectQuizModal;
        }

        private ArrayList<SelectQuizModal.SelectQuizQuestionModal> parseQuestions(JSONArray questionsJSON) {
            ArrayList<SelectQuizModal.SelectQuizQuestionModal> selectQuizQuestionModals = null;

            if (questionsJSON != null && questionsJSON.length() > 0) {
                selectQuizQuestionModals = new ArrayList<>();
                for (int i = 0; i < questionsJSON.length(); i++) {
                    JSONObject jsonObject = questionsJSON.optJSONObject(i);
                    ArrayList<SelectQuizModal.SelectQuizOptionModal> selectQuizOptionModals = new ArrayList<>();
                    JSONArray options = jsonObject.optJSONArray("options");
                    for (int j = 0; j < options.length(); j++) {
                        JSONObject option = options.optJSONObject(j);
                        String optionId = option.optString("option_id");
                        String optionText = option.optString("option_text");
                        selectQuizOptionModals.add(new SelectQuizModal.SelectQuizOptionModal(optionId, optionText));
                    }
                    SelectQuizModal.SelectQuizQuestionModal selectQuizQuestionModal =
                            new SelectQuizModal.SelectQuizQuestionModal(
                                    jsonObject.optString("question_id"),
                                    jsonObject.optString("question_image"),
                                    jsonObject.optString("question_text"),
                                    selectQuizOptionModals);
                    selectQuizQuestionModals.add(selectQuizQuestionModal);
                }
            }

            return selectQuizQuestionModals;
        }

        public void fetchQuizResultData(String matchId) {
            CustomOkHttpResponseHandler customOkHttpResponseHandler =
                    new CustomOkHttpResponseHandler(mContext) {
                        @Override
                        public void onRequestSuccess(JSONObject responseJSON) {
                            int responseCode = responseJSON.optInt("responseCode");
                            SelectQuizCompareModal selectQuizCompareModal = null;
                            boolean isSuccessfull = true;
                            String message = null;
                            switch (responseCode) {
                                case 200:
                                    selectQuizCompareModal
                                            = parseCompareQuizResponse(responseJSON);
                                    if (selectQuizCompareModal == null) {
                                        isSuccessfull = false;
                                        message = mContext.getString(R.string.SERVER_ERROR_SMALL);
                                    }
                                    break;

                                default:
                                    isSuccessfull = false;
                                    message = mContext.getString(R.string.SERVER_ERROR_SMALL);
                                    break;
                            }

                            if (mQuizHandlerListener != null) {
                                mQuizHandlerListener.onQuizResult(isSuccessfull, selectQuizCompareModal, message);
                            }
                        }

                        @Override
                        public void onRequestFailure(Exception exception) {
                            if (mQuizHandlerListener != null) {
                                int messageId = R.string.whoops_no_internet;
                                if (!Utility.isNetworkFailed(mContext, exception)) {
                                    messageId = R.string.SERVER_ERROR_SMALL;
                                }
                                mQuizHandlerListener.onQuizResult(false, null, mContext.getString(messageId));
                            }
                        }
                    };
            OkHttpHandler
                    .httpPost(mContext, ConstantsUrls.getSelectApiUrl(), GetOkHttpRequestParams
                            .getHttpRequestParams(
                                    Constants.HttpRequestType.COMPARE_QUIZ,
                                    matchId), customOkHttpResponseHandler);
        }

        private SelectQuizCompareModal parseCompareQuizResponse(JSONObject responseJSON) {
            SelectQuizCompareModal selectQuizCompareModal = null;
            ArrayList<SelectQuizQuesCompareResp> selectQuizQuesCompareResps = null;

            if (responseJSON != null) {
                JSONObject questions = responseJSON.optJSONObject("questions");
                if (questions != null) {
                    Iterator<String> iter = questions.keys();
                    selectQuizQuesCompareResps = new ArrayList<>();
                    selectQuizCompareModal = new SelectQuizCompareModal(responseJSON.optInt("match_percent"));
                    selectQuizCompareModal.setmMatchId(responseJSON.optString("match_id"));
                    boolean sortResponse = true;
                    while (iter.hasNext()) {
                        String key = iter.next();
                        JSONObject question = questions.optJSONObject(key);
                        SelectQuizQuesCompareResp selectQuizQuesCompareResp =
                                new SelectQuizQuesCompareResp(key,
                                        question.optString("question_text"),
                                        question.optString("my_option_id"),
                                        question.optString("match_option_id"),
                                        question.optInt("rank", -1));
                        if (sortResponse) {
                            sortResponse = selectQuizQuesCompareResp.getmRank() >= 0;
                        }
                        selectQuizQuesCompareResps.add(selectQuizQuesCompareResp);
                    }
                    if (sortResponse) {
                        Collections.sort(selectQuizQuesCompareResps, new Comparator<SelectQuizQuesCompareResp>() {
                            @Override
                            public int compare(SelectQuizQuesCompareResp selectQuizQuesCompareResp,
                                               SelectQuizQuesCompareResp t1) {
                                return Integer.valueOf(selectQuizQuesCompareResp.getmRank()).compareTo(t1.getmRank());
                            }
                        });
                    }

                    selectQuizCompareModal.setmSelectQuizQuesCompareResps(selectQuizQuesCompareResps);
                }
            }

            return selectQuizCompareModal;
        }


        public void unregister() {
            mQuizHandlerListener = null;
        }

        public interface QuizHandlerListener {
            void onQuizFetch(boolean success, SelectQuizModal selectQuizModal, String errorMessage);
            void onQuizSubmit(boolean success, String error);

            void onQuizResult(boolean success, SelectQuizCompareModal selectQuizCompareModal, String errorMessage);
        }
    }

    public static class SubscriptionHandler {
        private Context mContext;
        private SelectSubscriptionListener mSelectSubscriptionListener;

        public SubscriptionHandler(Context mContext, SelectSubscriptionListener mSelectSubscriptionListener) {
            this.mContext = mContext;
            this.mSelectSubscriptionListener = mSelectSubscriptionListener;
        }

        public void unregister() {
            this.mSelectSubscriptionListener = null;
        }

        public void startFreeTrial(final String mMatchId) {
            CustomOkHttpResponseHandler customOkHttpResponseHandler =
                    new CustomOkHttpResponseHandler(mContext) {
                        @Override
                        public void onRequestSuccess(JSONObject responseJSON) {
                            int responseCode = responseJSON.optInt("responseCode");
                            boolean isSuccessfull = true;
                            String message = null;
                            switch (responseCode) {
                                case 200:
                                    JSONObject jsonObject = responseJSON.optJSONObject("my_select");
                                    if (jsonObject != null) {
                                        new MySelectData(jsonObject).saveMySelectData(mContext);
                                    } else {
                                        isSuccessfull = false;
                                        message = mContext.getString(R.string.SERVER_ERROR_SMALL);
                                    }
                                    break;

                                default:
                                    HashMap<String, String> eventInfo = new HashMap<>();
                                    String error = responseJSON.optString("error");
                                    eventInfo.put("error", (Utility.isSet(error)) ? error : "Response : " + responseCode);
                                    if (Utility.isSet(mMatchId)) {
                                        eventInfo.put("match_id", mMatchId);
                                    } else {
                                        eventInfo.put("match_id", TrulyMadlyEvent.TrulyMadlyActivities.select);
                                    }
                                    TrulyMadlyTrackEvent.trackEvent(mContext, select, buy_trial, 0, tm_error,
                                            eventInfo, true, true);
                                    isSuccessfull = false;
                                    message = mContext.getString(R.string.SERVER_ERROR_SMALL);
                                    break;
                            }

                            if (mSelectSubscriptionListener != null) {
                                mSelectSubscriptionListener.onSubscription(isSuccessfull, message);
                            }
                        }

                        @Override
                        public void onRequestFailure(Exception exception) {
                            HashMap<String, String> eventInfo = new HashMap<>();
                            eventInfo.put("error", exception.getMessage());
                            if (Utility.isSet(mMatchId)) {
                                eventInfo.put("match_id", mMatchId);
                            } else {
                                eventInfo.put("match_id", TrulyMadlyEvent.TrulyMadlyActivities.select);
                            }
                            TrulyMadlyTrackEvent.trackEvent(mContext, select, buy_trial, 0, tm_error,
                                    eventInfo, true, true);

                            if (mSelectSubscriptionListener != null) {
                                int messageId = R.string.whoops_no_internet;
                                if (!Utility.isNetworkFailed(mContext, exception)) {
                                    messageId = R.string.SERVER_ERROR_SMALL;
                                }
                                mSelectSubscriptionListener.onSubscription(false, mContext.getString(messageId));
                            }
                        }
                    };
            OkHttpHandler
                    .httpPost(mContext, ConstantsUrls.getSelectApiUrl(), GetOkHttpRequestParams
                            .getHttpRequestParams(
                                    Constants.HttpRequestType.SELECT_FREE_TRIAL,
                                    null), customOkHttpResponseHandler);
        }

        public interface SelectSubscriptionListener {
            void onSubscription(boolean success, String errorMessage);
        }
    }
}
