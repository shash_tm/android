
package com.trulymadly.android.app.utility;


import com.crashlytics.android.Crashlytics;
import com.trulymadly.android.app.IABUtil.Base64;
import com.trulymadly.android.app.IABUtil.Base64DecoderException;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by udbhav on 16/09/16.
 * based on https://github.com/stevenholder/PHP-Java-AES-Encrypt/blob/master/security.java
 */
public class AESUtils {


    //AESUtils.encrypt(data, key)
    public static String encrypt(String unencyptedString, String key) {
        byte[] crypted = null;
        try {
            SecretKeySpec skey = new SecretKeySpec(key.getBytes(), "AES");
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, skey);
            crypted = cipher.doFinal(unencyptedString.getBytes());
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
            e.printStackTrace();
        }
        return Base64.encode(crypted);
    }

    //AESUtils.decrypt(AESUtils.encrypt(data, key), key)
    public static String decrypt(String encryptedString, String key) {
        byte[] output = null;
        if (!Utility.isSet(encryptedString)) {
            return null;
        }
        try {
            SecretKeySpec skey = new SecretKeySpec(key.getBytes(), "AES");
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            //"AES/CFB8/NoPadding"
            cipher.init(Cipher.DECRYPT_MODE, skey);
            output = cipher.doFinal(Base64.decode(encryptedString));
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException | Base64DecoderException e) {
            Crashlytics.logException(e);
        }
        return new String(output);
    }

}
