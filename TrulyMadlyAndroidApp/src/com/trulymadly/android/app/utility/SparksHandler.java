package com.trulymadly.android.app.utility;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.facebook.appevents.AppEventsConstants;
import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.GetOkHttpRequestParams;
import com.trulymadly.android.app.MessageModal;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.asynctasks.TrackEventToFbAsyncTask;
import com.trulymadly.android.app.billing.PaymentFor;
import com.trulymadly.android.app.fragments.BuySparkDialogFragment;
import com.trulymadly.android.app.fragments.SendSparkFragment;
import com.trulymadly.android.app.fragments.SimpleDialogFragment;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.ConstantsRF;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.listener.ActivityEventsListener;
import com.trulymadly.android.app.listener.CustomOkHttpResponseHandler;
import com.trulymadly.android.app.modal.MatchMessageMetaData;
import com.trulymadly.android.app.modal.SparkBlooperModal;
import com.trulymadly.android.app.modal.SparkModal;
import com.trulymadly.android.app.modal.SparkPackageModal;
import com.trulymadly.android.app.modal.SparkSendModal;
import com.trulymadly.android.app.sqlite.MessageDBHandler;
import com.trulymadly.android.app.sqlite.RFHandler;
import com.trulymadly.android.app.sqlite.SparksDbHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities.sparks;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventStatus.buy;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventStatus.error;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventStatus.interests_error;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventStatus.seen;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventStatus.send;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventStatus.send_error;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventStatus.tm_error;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventStatus.view;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes.intro;

/**
 * Created by avin on 17/05/16.
 */
public class SparksHandler {

    //////////////////////////////////////////////////////
    // Utility Methods
    //////////////////////////////////////////////////////

    /**
     * Checks if the sparks are present in the users's account or not
     *
     * @param mContext
     */
    public static boolean isSparksPresent(Context mContext) {
        int counter = RFHandler.getInt(mContext, Utility.getMyId(mContext), ConstantsRF.RIGID_FIELD_SPARK_COUNTER, 0);
        return counter > 0;
    }

    /**
     * Check the sparks left for the user in his account
     *
     * @param mContext
     */
    public static int getSparksLeft(Context mContext) {
        return RFHandler.getInt(mContext, Utility.getMyId(mContext), ConstantsRF.RIGID_FIELD_SPARK_COUNTER, 0);
    }

    /**
     * Checks if "Spark" is enabled for the given user or not
     *
     * @param mContext
     */
    public static boolean isSparksEnabled(Context mContext) {
        //Disabling the flag check for now
        //Undo this
//        return false;
        return RFHandler.getBool(mContext, Utility.getMyId(mContext), ConstantsRF.RIGID_FIELD_SPARK_ENABLED);
    }

    /**
     * Inserts sparks for the given user in his account
     *
     * @param mContext
     * @param totalSparks
     */
    public static void insertSparks(Context mContext, int totalSparks) {
        if (totalSparks < 0) {
            totalSparks = 0;
        }
        RFHandler.insert(mContext, Utility.getMyId(mContext), ConstantsRF.RIGID_FIELD_SPARK_COUNTER, totalSparks);
    }

    /**
     * Checks if the given spark is expired or not
     *
     * @param sparkModal
     */
    public static boolean isExpired(SparkModal sparkModal) {
        if (sparkModal != null) {
            if ((sparkModal.getmStartTime() != 0 && TimeUtils.isTimeoutExpired(sparkModal.getmStartTime(),
                    sparkModal.getmExpiredTimeInSeconds() * 1000))) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks if the given spark is accepted or expired -> If either of these conditions are true, that means its not a valid spark
     *
     * @param sparkModal
     * @param sparkMessageMetaData
     */
    public static boolean isValidSpark(SparkModal sparkModal, MatchMessageMetaData sparkMessageMetaData) {
        if (sparkModal != null && sparkMessageMetaData != null) {
            if (sparkMessageMetaData.isMutualSpark() || (sparkModal.getmStartTime() != 0 && TimeUtils.isTimeoutExpired(sparkModal.getmStartTime(),
                    sparkModal.getmExpiredTimeInSeconds() * 1000))) {
                return false;
            }
        }

        return true;
    }

    /**
     * Checks if the given spark exists in the db or not
     *
     * @param context
     * @param sparkModal
     */
    public static boolean isSparkExists(Context context, SparkModal sparkModal) {
        return sparkModal != null && SparksDbHandler.getSpark(context, Utility.getMyId(context), sparkModal.getmMatchId()) != null;

    }

    /**
     * @param context
     */
    public static int getSparkWarningShownCounter(Context context) {
        return SPHandler.getInt(context, ConstantsSP.SHARED_PREF_SPARK_RESPOND_WARNING_SHOWN_COUNTER, 0);
    }

    /**
     * @param context
     */
    public static void incSparkWarningShownCounter(Context context) {
        SPHandler.setInt(context, ConstantsSP.SHARED_PREF_SPARK_RESPOND_WARNING_SHOWN_COUNTER, getSparkWarningShownCounter(context) + 1);
    }

    /**
     * @param context
     */
    public static void resetSparkWarningShownCounter(Context context) {
        SPHandler.setInt(context, ConstantsSP.SHARED_PREF_SPARK_RESPOND_WARNING_SHOWN_COUNTER, 0);
    }

    /**
     * @param context
     */
    public static boolean shouldShowSparkConvAnim(Context context) {
        return SPHandler.getInt(context, ConstantsSP.SHARED_KEY_SPARK_CONV_TUTORIAL_ANIM_COUNT, 0) <
                Constants.SPARK_RECEIVER_CONV_ANIM_MAX_COUNT;
    }

    /**
     * @param context
     */
    public static int getSparkConvAnimCounter(Context context) {
        return SPHandler.getInt(context, ConstantsSP.SHARED_KEY_SPARK_CONV_TUTORIAL_ANIM_COUNT, 0);
    }

    /**
     * @param context
     */
    public static void incSparkConvAnimCounter(Context context) {
        SPHandler.setInt(context, ConstantsSP.SHARED_KEY_SPARK_CONV_TUTORIAL_ANIM_COUNT, getSparkConvAnimCounter(context) + 1);
    }

    /**
     * @param context
     */
    public static boolean shouldShowSparkMsgOneononeAnim(Context context) {
        return SPHandler.getInt(context, ConstantsSP.SHARED_KEY_SPARK_MSG_ONEONONE_TUTORIAL_ANIM_COUNT, 0) <
                Constants.SPARK_RECEIVER_MSG_ONEONONE_ANIM_MAX_COUNT;
    }

    /**
     * @param context
     */
    public static int getSparkMsgOneononeAnimCounter(Context context) {
        return SPHandler.getInt(context, ConstantsSP.SHARED_KEY_SPARK_MSG_ONEONONE_TUTORIAL_ANIM_COUNT, 0);
    }

    /**
     * @param context
     */
    public static void incSparkMsgOneononeAnimCounter(Context context) {
        SPHandler.setInt(context, ConstantsSP.SHARED_KEY_SPARK_MSG_ONEONONE_TUTORIAL_ANIM_COUNT, getSparkMsgOneononeAnimCounter(context) + 1);
    }

    /**
     * @param context
     */
    public static String getWhyNotSparkInsteadContent(Context context) {
        return SPHandler.getString(context, ConstantsSP.SHARED_KEY_WHY_NOT_SPARK_INSTEAD_MESSAGE);
    }

    /**
     * Checks if there is any new spark
     *
     * @param context
     */
    public static boolean isThereAnyNewSpark(Context context) {
        if (isSparksEnabled(context)) {
            ArrayList<SparkModal> sparkModals = SparksDbHandler.getTopTwoSparks(context, Utility.getMyId(context));
            if (sparkModals != null && sparkModals.size() > 0) {
                SparkModal firstSparkModal = sparkModals.get(0);
                if (firstSparkModal.getmStartTime() == 0) {
                    return true;
                }

                if (isExpired(firstSparkModal) && sparkModals.size() == 2) {
                    SparkModal secondSparkModal = sparkModals.get(1);
                    if (secondSparkModal.getmStartTime() == 0) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Checks if there is any new spark and returns its matchId
     *
     * @param context
     */
    public static String getCurrentSparkMatchId(Context context) {
        if (isSparksEnabled(context)) {
            ArrayList<SparkModal> sparkModals = SparksDbHandler.getTopTwoSparks(context, Utility.getMyId(context));
            if (sparkModals != null && sparkModals.size() > 0) {
                SparkModal firstSparkModal = sparkModals.get(0);
                if (firstSparkModal.getmStartTime() == 0) {
                    return firstSparkModal.getmMatchId();
                }

                if (isExpired(firstSparkModal) && sparkModals.size() == 2) {
                    SparkModal secondSparkModal = sparkModals.get(1);
                    if (secondSparkModal.getmStartTime() == 0) {
                        return secondSparkModal.getmMatchId();
                    }
                }
            }
        }

        return null;
    }

    /**
     * Checks if there is any new spark and returns its matchId
     *
     * @param context
     */
    public static SparkBlooperModal checkForSparkBlooper(Context context) {
        SparkBlooperModal sparkBlooperModal = new SparkBlooperModal();
        if (isSparksEnabled(context)) {
            ArrayList<SparkModal> sparkModals = SparksDbHandler.getTopTwoSparks(context, Utility.getMyId(context));
            if (sparkModals != null && sparkModals.size() > 0) {
                SparkModal firstSparkModal = sparkModals.get(0);
                if (firstSparkModal.getmStartTime() <= 0) {
                    sparkBlooperModal.setmNewSparkMatchId(firstSparkModal.getmMatchId());
                } else if (isExpired(firstSparkModal)) {
                    SparksDbHandler.sparkRejected(context, Utility.getMyId(context), firstSparkModal.getmMatchId());
                    if (sparkModals.size() == 2) {
                        SparkModal secondSparkModal = sparkModals.get(1);
                        if (secondSparkModal.getmStartTime() <= 0) {
                            sparkBlooperModal.setmNewSparkMatchId(firstSparkModal.getmMatchId());
                        }
                    }
                } else {
                    sparkBlooperModal.setBlooperRequired(true);
                }
            }
        }

        return sparkBlooperModal;
    }

    public static Fragment toggleFragment(AppCompatActivity activity,
                                          Fragment mCurrentFragment,
                                          Object listener,
                                          String matchId, String sparkHash,
                                          boolean showFragment, SparksDialogType dialogType,
                                          int newSparksAdded, int totalSparks, boolean fromIntro,
                                          boolean fromScenes, String scenesId, int likesDone, int hidesDone, int sparksDone,
                                          boolean favorites_viewed, boolean isRelationshipExpertAdded,
                                          boolean mHasLikedBefore,
                                          boolean isSelectMember) {
        FragmentManager mFragmentManager = activity.getSupportFragmentManager();
        HashMap<String, String> eventInfo = null;
        if (showFragment) {
            switch (dialogType) {
                case introducing_sparks:
                    //Tracking : Intro sparks - viewed
                    eventInfo = new HashMap<>();
                    eventInfo.put("match_id", matchId);
                    eventInfo.put("favorites_viewed", String.valueOf(favorites_viewed));
                    eventInfo.put("from_scenes", String.valueOf(fromScenes));
                    if (fromScenes) {
                        eventInfo.put("event_id", scenesId);
                    }
                    TrulyMadlyTrackEvent.trackEvent(activity.getApplicationContext(), sparks, dialogType.getEvent(), 0, view,
                            eventInfo, true, true);

                    if (mCurrentFragment != null) {
                        mFragmentManager.beginTransaction().remove(mCurrentFragment).commit();
                    }
                    mCurrentFragment = SimpleDialogFragment.newInstance(
                            SimpleDialogFragment.getSimpleDialogModal(activity,
                                    SimpleDialogFragment.SIMPLE_DIALOG_INTRODUCING_SPARK));
                    mFragmentManager.beginTransaction().replace(R.id.spark_dialog_container, mCurrentFragment).commit();
                    ((ActivityEventsListener) mCurrentFragment).registerListener(listener);
                    break;
                case one_spark_left:
                    if (mCurrentFragment != null) {
                        mFragmentManager.beginTransaction().remove(mCurrentFragment).commit();
                    }
                    mCurrentFragment = SimpleDialogFragment.newInstance(
                            SimpleDialogFragment.getSimpleDialogModal(activity,
                                    SimpleDialogFragment.SIMPLE_DIALOG_ONE_LEFT));
                    mFragmentManager.beginTransaction().replace(R.id.spark_dialog_container, mCurrentFragment).commit();
                    ((ActivityEventsListener) mCurrentFragment).registerListener(listener);
                    break;
                case buy_spark:
                    eventInfo = new HashMap<>();
                    eventInfo.put("match_id", matchId);
                    eventInfo.put("sparks_left", String.valueOf(getSparksLeft(activity.getApplicationContext())));
                    eventInfo.put("from_intro", String.valueOf(fromIntro));
                    eventInfo.put("from_scenes", String.valueOf(fromScenes));
                    if (fromScenes) {
                        eventInfo.put("event_id", scenesId);
                    }
                    eventInfo.put("likes_done", String.valueOf(likesDone));
                    eventInfo.put("hides_done", String.valueOf(hidesDone));
                    eventInfo.put("sparks_done", String.valueOf(sparksDone));
                    eventInfo.put("favorites_viewed", String.valueOf(favorites_viewed));
                    TrulyMadlyTrackEvent.trackEvent(activity.getApplicationContext(), sparks, dialogType.getEvent(), 0, view,
                            eventInfo, true, true);


//                    BuySparkDialogFragmentNew.BuySparkFragmentDataModal buySparkFragmentDataModal =
//                            new BuySparkDialogFragmentNew.BuySparkFragmentDataModal(null, false, 0, 0, 0);
//                    mCurrentFragment = BuySparkDialogFragmentNew.newInstance(buySparkFragmentDataModal);
//                    ((BuySparkDialogFragmentNew)mCurrentFragment).show(activity.getSupportFragmentManager(),
//                            ((BuySparkDialogFragmentNew)mCurrentFragment).getTag());
//                    ((ActivityEventsListener) mCurrentFragment).registerListener(listener);

                    BuySparkDialogFragment.BuySparkFragmentDataModal buySparkFragmentDataModal =
                            new BuySparkDialogFragment.BuySparkFragmentDataModal(matchId, fromIntro,
                                    likesDone, hidesDone, sparksDone, favorites_viewed, fromScenes, scenesId);
                    if (mCurrentFragment == null || !(mCurrentFragment instanceof BuySparkDialogFragment)) {
                        if (mCurrentFragment != null) {
                            mFragmentManager.beginTransaction().remove(mCurrentFragment).commit();
                        }

                        mCurrentFragment = BuySparkDialogFragment.newInstance(buySparkFragmentDataModal);
                        mFragmentManager.beginTransaction().replace(R.id.spark_dialog_container, mCurrentFragment).commit();
                        ((ActivityEventsListener) mCurrentFragment).registerListener(listener);
                    } else {
                        ((ActivityEventsListener) mCurrentFragment).reInitialize(buySparkFragmentDataModal);
                    }
                    break;
                case send_spark:
                    eventInfo = new HashMap<>();
                    eventInfo.put("match_id", matchId);
                    eventInfo.put("sparks_left", String.valueOf(getSparksLeft(activity.getApplicationContext())));
                    eventInfo.put("from_intro", String.valueOf(fromIntro));
                    eventInfo.put("from_scenes", String.valueOf(fromScenes));
                    if (fromScenes) {
                        eventInfo.put("event_id", scenesId);
                    }
                    eventInfo.put("likes_done", String.valueOf(likesDone));
                    eventInfo.put("hides_done", String.valueOf(hidesDone));
                    eventInfo.put("sparks_done", String.valueOf(sparksDone));
                    eventInfo.put("favorites_viewed", String.valueOf(favorites_viewed));

                    eventInfo.put("is_select_member", String.valueOf(TMSelectHandler.isSelectMember(activity)));
                    eventInfo.put("is_profile_select", String.valueOf(isSelectMember));

                    TrulyMadlyTrackEvent.trackEvent(activity.getApplicationContext(), sparks, dialogType.getEvent(), 0, view,
                            eventInfo, true, true);
                    if (mCurrentFragment == null || !(mCurrentFragment instanceof SendSparkFragment)) {
                        if (mCurrentFragment != null) {
                            mFragmentManager.beginTransaction().remove(mCurrentFragment).commit();
                        }

                        mCurrentFragment = SendSparkFragment.newInstance(
                                new SendSparkFragment.SendSparkFragmentModal(
                                        matchId, sparkHash, fromIntro,
                                        sparksDone, likesDone, hidesDone, fromScenes, scenesId, mHasLikedBefore));
                        mFragmentManager.beginTransaction().replace(R.id.spark_dialog_container, mCurrentFragment).commit();
                        ((ActivityEventsListener) mCurrentFragment).registerListener(listener);
                    } else {
                        ((ActivityEventsListener) mCurrentFragment).reInitialize(
                                new SendSparkFragment.SendSparkFragmentModal(
                                        matchId, sparkHash, fromIntro,
                                        sparksDone, likesDone, hidesDone, fromScenes, scenesId, mHasLikedBefore));
                    }
                    break;
                case spark_purchased:
//                    if(mCurrentFragment != null && mCurrentFragment instanceof BuySparkDialogFragmentNew){
//                        ((BuySparkDialogFragmentNew)mCurrentFragment).dismiss();
//                    }
                    if (mCurrentFragment != null) {
                        mFragmentManager.beginTransaction().remove(mCurrentFragment).commit();
                    }
                    mCurrentFragment = SimpleDialogFragment.newInstance(
                            SimpleDialogFragment.getSimpleDialogModal(activity,
                                    SimpleDialogFragment.SIMPLE_DIALOG_PURCHASE_COMPLETE, newSparksAdded, totalSparks, isRelationshipExpertAdded));
                    mFragmentManager.beginTransaction().replace(R.id.spark_dialog_container, mCurrentFragment).commit();
                    ((ActivityEventsListener) mCurrentFragment).registerListener(listener);
                    break;
            }
        }

        return mCurrentFragment;
    }

    //////////////////////////////////////////////////////
    // UI Utility Methods
    //////////////////////////////////////////////////////
    public enum SparksDialogType {
        introducing_sparks, one_spark_left, buy_spark, send_spark, spark_purchased;

        //Solely for tracking
        public String getEvent() {
            switch (this) {
                case introducing_sparks:
                    return intro;
                case one_spark_left:
                    return null;
                case buy_spark:
                    return buy;
                case send_spark:
                    return send;
                case spark_purchased:
                    return null;
            }

            return null;
        }
    }

    //////////////////////////////////////////////////////
    // Handles sparks receiving - fetching, updating status etc
    //////////////////////////////////////////////////////
    public static class SparksReceiver {

        private Context mContext;
        private SparksReceiverListener mSparksReceiverListener;

        public SparksReceiver(Context mContext, SparksReceiverListener mSparksReceiverListener) {
            this.mContext = mContext;
            this.mSparksReceiverListener = mSparksReceiverListener;
        }

        public static void resetSparks(Context mContext) {
            CustomOkHttpResponseHandler customOkHttpResponseHandler =
                    new CustomOkHttpResponseHandler(mContext) {
                        @Override
                        public void onRequestSuccess(JSONObject responseJSON) {

                        }

                        @Override
                        public void onRequestFailure(Exception exception) {

                        }
                    };
            OkHttpHandler
                    .httpPost(mContext, ConstantsUrls.getSparksApiUrl(), GetOkHttpRequestParams
                            .getHttpRequestParams(
                                    Constants.HttpRequestType.SPARKS_RESET,
                                    null), customOkHttpResponseHandler);
        }

        public void register(SparksReceiverListener mSparksReceiverListener) {
            this.mSparksReceiverListener = mSparksReceiverListener;
        }

        private SparkModal getValidSparkFromDb() {
            SparkModal sparkModal = SparksDbHandler.getNextSpark(mContext, Utility.getMyId(mContext));
            MatchMessageMetaData sparkMessageMetaData = null;
            if (sparkModal != null) {
                sparkMessageMetaData = MessageDBHandler.getMatchMessageMetaData(sparkModal.getmUserId(),
                        sparkModal.getmMatchId(), mContext);
                if (sparkMessageMetaData.isMutualSpark()) {
                    SparksDbHandler.sparkAccepted(mContext, Utility.getMyId(mContext), sparkModal.getmMatchId(),
                            MessageDBHandler.getLastMessage(Utility.getMyId(mContext), sparkModal.getmMatchId(), mContext));
                    sparkModal = getValidSparkFromDb();
                } else if (sparkModal.getmStartTime() != 0 && TimeUtils.isTimeoutExpired(sparkModal.getmStartTime(),
                        sparkModal.getmExpiredTimeInSeconds() * 1000)) {
                    SparksDbHandler.sparkRejected(mContext, Utility.getMyId(mContext), sparkModal.getmMatchId());
                    sparkModal = getValidSparkFromDb();
                }
            }

            return sparkModal;
        }

        /**
         * Gets a valid spark from db, if available and fetches sparks from the server
         *
         * @param fetchForcefully
         */
        public void getAndFetchSparks(boolean fetchForcefully) {

            final SparkModal sparkModal = getValidSparkFromDb();//SparksDbHandler.getTopSpark(mContext, Utility.getMyId(mContext));
            final boolean isSparkAlreadyPassed = sparkModal != null && mSparksReceiverListener != null;

            if (mSparksReceiverListener != null) {
                mSparksReceiverListener.loadSpark(sparkModal);
            }

            //1. If no sparks present right now
            //2. fetch forcefully -> Once when ConversationListActivity is created
            if (fetchForcefully || sparkModal == null) {
//                    || SparksDbHandler.getSparksCount(mContext, Utility.getMyId(mContext)) == 1){
                fetchSparks(!isSparkAlreadyPassed);
            }
        }

        public void fetchSparks(final boolean passTheSpark) {
            CustomOkHttpResponseHandler customOkHttpResponseHandler =
                    new CustomOkHttpResponseHandler(mContext) {
                        @Override
                        public void onRequestSuccess(JSONObject responseJSON) {
                            ArrayList<SparkModal> sparkModals = parseResponses(mContext, responseJSON);
                            if (sparkModals != null && sparkModals.size() > 0) {
                                SparksDbHandler.removePendingSparks(mContext, Utility.getMyId(mContext));
                                SparksDbHandler.insertSparks(mContext, Utility.getMyId(mContext), sparkModals);
                                if (mSparksReceiverListener != null) {
                                    if (passTheSpark) {
                                        mSparksReceiverListener.loadSpark(sparkModals.get(0));
                                    }
                                    mSparksReceiverListener.onSparkfetchSuccess(sparkModals.size());
                                }
                            } else {
                                if (mSparksReceiverListener != null) {
                                    if (passTheSpark) {
                                        mSparksReceiverListener.loadSpark(null);
                                    }
                                    mSparksReceiverListener.onSparkfetchSuccess(0);
                                }
                            }
                        }

                        @Override
                        public void onRequestFailure(Exception exception) {
                            if (mSparksReceiverListener != null) {
                                mSparksReceiverListener.onSparkfetchFailure();
                            }
                        }
                    };
            OkHttpHandler
                    .httpPost(mContext, ConstantsUrls.getSparksApiUrl(), GetOkHttpRequestParams
                            .getHttpRequestParams(
                                    Constants.HttpRequestType.SPARKS_GET,
                                    null), customOkHttpResponseHandler);
        }

        // Parsers
        private ArrayList<SparkModal> parseResponses(Context context, JSONObject jsonObject) {
            ArrayList<SparkModal> sparkModals = null;
            JSONArray sparks = jsonObject.optJSONArray("sparks");
            if (sparks != null && sparks.length() > 0) {
                sparkModals = new ArrayList<>();
                for (int i = 0; i < sparks.length(); i++) {
                    SparkModal sparkModal = parseResponse(context, sparks.optJSONObject(i));
                    if (sparkModal != null) {
                        sparkModals.add(sparkModal);
                    }
                }
            }
            return sparkModals;
        }

        private SparkModal parseResponse(Context context, JSONObject jsonObject) {
            SparkModal sparkModal = null;
            if (jsonObject != null) {
                sparkModal = SparkModal.parseSpark(context, jsonObject);
            }
            return sparkModal;
        }

        // Spark Status related
        public void sparkStatusChanged(SparkModal sparkModal, SparkModal.SPARK_STATUS sparkStatus, MessageModal messageObj) {
            if (sparkModal != null) {
                //Extra check to fix:
                //https://fabric.io/trulymadly/android/apps/com.trulymadly.android.app.t1/issues/5770d824ffcdc04250e06b8d
                String matchId = sparkModal.getmMatchId();
                if (!Utility.isSet(matchId)) {
                    return;
                }

                switch (sparkStatus) {
                    case seen:
                        SparksDbHandler.startSparkTimer(mContext, Utility.getMyId(mContext),
                                matchId, Calendar.getInstance().getTimeInMillis());

                        //match_id, sparks_left
                        int sparksLeft = SparksDbHandler.getPendingSparksCount(mContext, Utility.getMyId(mContext));
                        HashMap<String, String> eventInfo = new HashMap<>();
                        eventInfo.put("match_id", matchId);
                        eventInfo.put("sparks_left", String.valueOf(sparksLeft));
                        TrulyMadlyTrackEvent.trackEvent(mContext, sparks, TrulyMadlyEvent.TrulyMadlyEventTypes.conv_list, 0, seen,
                                eventInfo, true, true);

                        MoEHandler.trackEvent(mContext, MoEHandler.Events.SPARK_RECEIVED);

                        //fetch more sparks in case this is the last spark updated
                        if (sparksLeft <= 0 /*!SparksDbHandler.areThereAnyPendingSparks(mContext, Utility.getMyId(mContext))*/) {
                            fetchSparks(false);
                        }
                        break;
                    case rejected:
                        MoEHandler.trackEvent(mContext, MoEHandler.Events.SPARK_REJECTED);
                        SparksDbHandler.sparkRejected(mContext, Utility.getMyId(mContext), matchId);
                        break;
                    case accepted:
                        MoEHandler.trackEvent(mContext, MoEHandler.Events.SPARK_ACCEPTED);
//                    SparksDbHandler.sparkAccepted(mContext, Utility.getMyId(mContext), matchId, messageObj);
                        break;
                }
                if (mSparksReceiverListener != null) {
                    mSparksReceiverListener.onSparkUpdateSuccess(Utility.getMyId(mContext), matchId, sparkStatus);
                }
            }
        }

        public void updateSparkStatus(final SparkModal sparkModal, final SparkModal.SPARK_STATUS sparkStatus) {
            sparkModal.setmSparkAction(sparkStatus);
            CustomOkHttpResponseHandler customOkHttpResponseHandler =
                    new CustomOkHttpResponseHandler(mContext) {
                        @Override
                        public void onRequestSuccess(JSONObject responseJSON) {
                            if (responseJSON == null || responseJSON.has("error")) {

                                //match_id, sparks_left
                                HashMap<String, String> eventInfo = new HashMap<>();
                                if (sparkModal != null) {
                                    eventInfo.put("match_id", sparkModal.getmMatchId());
                                }
                                eventInfo.put("error", (responseJSON != null) ? responseJSON.optString("error") : "Unknown Error");
                                eventInfo.put("action", sparkStatus.name());
                                TrulyMadlyTrackEvent.trackEvent(mContext, sparks, TrulyMadlyEvent.TrulyMadlyEventTypes.conv_list, 0, error,
                                        eventInfo, true, true);

                                onRequestFailure(new Exception("Some error"));
                            } else {
                                sparkStatusChanged(sparkModal, sparkStatus, null);
                            }
                        }

                        @Override
                        public void onRequestFailure(Exception exception) {
                            //match_id, sparks_left
                            HashMap<String, String> eventInfo = new HashMap<>();
                            if (sparkModal != null) {
                                eventInfo.put("match_id", sparkModal.getmMatchId());
                            }
                            eventInfo.put("error", (exception != null) ? exception.getMessage() : "Unknown Error");
                            eventInfo.put("action", sparkStatus.name());
                            TrulyMadlyTrackEvent.trackEvent(mContext, sparks, TrulyMadlyEvent.TrulyMadlyEventTypes.conv_list, 0, error,
                                    eventInfo, true, true);

                            if (mSparksReceiverListener != null) {
                                mSparksReceiverListener.onSparkUpdateFailed(sparkModal.getmUserId(), sparkModal.getmMatchId(), sparkStatus);
                            }
                        }
                    };
            OkHttpHandler
                    .httpPost(mContext, ConstantsUrls.getSparksApiUrl(), GetOkHttpRequestParams
                            .getHttpRequestParams(
                                    Constants.HttpRequestType.SPARKS_UPDATE,
                                    sparkModal), customOkHttpResponseHandler);
        }

        // Listener
        public interface SparksReceiverListener {
            void loadSpark(SparkModal sparkModal);

            void onSparkfetchSuccess(int sparksFetched);

            void onSparkfetchFailure();

            void onSparkUpdateFailed(String userId, String matchId, SparkModal.SPARK_STATUS sparkStatus);

            void onSparkUpdateSuccess(String userId, String matchId, SparkModal.SPARK_STATUS sparkStatus);
        }

        public static class SimpleSparkReceiverListener implements SparksReceiverListener {

            @Override
            public void loadSpark(SparkModal sparkModal) {

            }

            @Override
            public void onSparkfetchSuccess(int sparksFetched) {

            }

            @Override
            public void onSparkfetchFailure() {

            }

            @Override
            public void onSparkUpdateFailed(String userId, String matchId, SparkModal.SPARK_STATUS sparkStatus) {

            }

            @Override
            public void onSparkUpdateSuccess(String userId, String matchId, SparkModal.SPARK_STATUS sparkStatus) {

            }
        }
    }

    //////////////////////////////////////////////////////
    // Handles sparks sending - fetching commonalities, sending spark etc
    //////////////////////////////////////////////////////
    public static class Sender {
        private Context mContext;
        private SparksSenderListener mSparksSenderListener;
        private boolean isSparkBeingSent = false;
        private long mCurrentTime = 0L;

        public Sender(Context mContext, SparksSenderListener mSparksSenderListener) {
            this.mContext = mContext;
            this.mSparksSenderListener = mSparksSenderListener;
        }

        public void registerListener(SparksSenderListener mSparksSenderListener) {
            this.mSparksSenderListener = mSparksSenderListener;
        }

        private ArrayList<String> parseCommonalities(JSONArray jsonArray) {
            ArrayList<String> mMessages = null;

            if (jsonArray != null && jsonArray.length() > 0) {
                mMessages = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    String message = jsonArray.optString(i);
                    if (Utility.isSet(message)) {
                        mMessages.add(message);
                    }
                }
            }

            return mMessages;
        }

        /*
        * Sample response
        *"message": {
            "time": "2016-06-24 07:47:23",
            "sender_id": "2136",
            "receiver_id": "130014",
            "unique_id": "130014_1466754420124_eiolrowspgtizuwomsnw",
            "quiz_id": null,
            "msg": "high-class",
            "message_type": "SPARK",
            "msg_id": "1765541"
        },
        * */
        private MessageModal parseForMessageModal(JSONObject jsonObject) {
            MessageModal messageModal = null;
            if (jsonObject != null) {
                String messageId = jsonObject.optString("msg_id");
                if (Utility.isSet(messageId)) {
                    messageModal = new MessageModal(Utility.getMyId(mContext));
                    messageModal.parseMessage(messageId, jsonObject.optString("msg"),
                            jsonObject.optString("time"), MessageModal.MessageType.TEXT.name(),
                            jsonObject.optString("receiver_id"), Constants.MessageState.OUTGOING_SENT, 0,
                            null, null);
                }
            }

            return messageModal;
        }

        public boolean isSending() {
            return isSparkBeingSent;
        }

        public void sendSpark(final SparkSendModal sparkSendModal) {
            if (!isSparkBeingSent) {
                isSparkBeingSent = true;
                CustomOkHttpResponseHandler customOkHttpResponseHandler =
                        new CustomOkHttpResponseHandler(mContext) {
                            @Override
                            public void onRequestSuccess(JSONObject responseJSON) {
                                int responseCode = responseJSON.optInt("responseCode", 403);
                                boolean isSuccessfull = true;
                                String message = null;
                                ServerCodes serverCodes = ServerCodes.none;
                                long newTime = Calendar.getInstance().getTimeInMillis();
                                long timeTaken = (newTime - mCurrentTime) / 1000;
                                HashMap<String, String> eventInfo = null;
                                switch (responseCode) {
                                    case 200:
                                        if (responseJSON.has("error")) {
                                            eventInfo = new HashMap<>();
                                            eventInfo.put("match_id", sparkSendModal.getmReceiverId());
                                            eventInfo.put("error", responseJSON.optString("error"));
                                            eventInfo.put("time_taken", String.valueOf(timeTaken));
                                            TrulyMadlyTrackEvent.trackEvent(mContext, sparks, send, 0, send_error,
                                                    eventInfo, true, true);
                                            isSuccessfull = false;
                                            message = mContext.getString(R.string.SERVER_ERROR_LARGE);
                                        } else {
                                            if (responseJSON.has("sparks_left")) {
                                                int sparks_left = responseJSON.optInt("sparks_left");
                                                SparksHandler.insertSparks(mContext, sparks_left);
                                                MessageModal messageModal = parseForMessageModal(responseJSON.optJSONObject("message"));
                                                if (messageModal != null) {
                                                    MessageDBHandler.insertAMessage(mContext, Utility.getMyId(mContext),
                                                            messageModal, true, Constants.FetchSource.LOCAL, false);
                                                }
                                                Bundle params = new Bundle();
                                                params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, "Sparks");
                                                TrackEventToFbAsyncTask.trackEvent(mContext, AppEventsConstants.EVENT_NAME_SPENT_CREDITS, params);
                                            }
                                        }
                                        break;
                                    case 403:
                                        eventInfo = new HashMap<>();
                                        eventInfo.put("match_id", sparkSendModal.getmReceiverId());
                                        eventInfo.put("error", responseJSON.optString("error"));
                                        eventInfo.put("time_taken", String.valueOf(timeTaken));
                                        TrulyMadlyTrackEvent.trackEvent(mContext, sparks, send, 0, send_error,
                                                eventInfo, true, true);
                                        isSuccessfull = false;
                                        int errorCode = responseJSON.optInt("error_code", ServerCodes.server_error.getKey());
                                        serverCodes = ServerCodes.getServerCodeFromIntValue(errorCode);
                                        if (serverCodes == ServerCodes.no_sparks) {
                                            insertSparks(mContext, 0);
                                        }
//                                        if(errorCode == ServerCodes.no_sparks.getKey()){
//                                            message = mContext.getString(R.string.no_sparks_left);
//                                            serverCodes = ServerCodes.no_sparks;
//                                        }else {
//                                            message = mContext.getString(R.string.SERVER_ERROR_LARGE);
//                                        }
                                        break;
                                }
                                isSparkBeingSent = false;
                                if (mSparksSenderListener != null) {
                                    mSparksSenderListener.onSparkSent(isSuccessfull, serverCodes, timeTaken);
                                }
                            }

                            @Override
                            public void onRequestFailure(Exception exception) {
                                long newTime = Calendar.getInstance().getTimeInMillis();
                                long timeTaken = (newTime - mCurrentTime) / 1000;
                                HashMap<String, String> eventInfo = new HashMap<>();
                                eventInfo.put("match_id", sparkSendModal.getmReceiverId());
                                eventInfo.put("error", (exception != null) ? exception.getMessage() : "Unknown Error");
                                eventInfo.put("time_taken", String.valueOf(timeTaken));
                                TrulyMadlyTrackEvent.trackEvent(mContext, sparks, send, 0, send_error,
                                        eventInfo, true, true);
                                isSparkBeingSent = false;
                                if (mSparksSenderListener != null) {
                                    mSparksSenderListener.onSparkSent(false,
                                            Utility.getNetworkErrorCode(mContext, exception), timeTaken);
                                }
                            }
                        };

                mCurrentTime = Calendar.getInstance().getTimeInMillis();

                OkHttpHandler
                        .httpPost(mContext, ConstantsUrls.getSparksApiUrl(), GetOkHttpRequestParams
                                .getHttpRequestParams(
                                        Constants.HttpRequestType.SPARKS_SENT,
                                        sparkSendModal), customOkHttpResponseHandler);
            }
        }

        public void getCommonalities(final String matchId, final String scenesId) {
            CustomOkHttpResponseHandler customOkHttpResponseHandler =
                    new CustomOkHttpResponseHandler(mContext) {
                        @Override
                        public void onRequestSuccess(JSONObject responseJSON) {
                            int responseCode = responseJSON.optInt("responseCode", 403);
                            boolean isSuccessfull = true;
                            String message = null;
                            ArrayList<String> messages = null;
                            switch (responseCode) {
                                case 200:
                                    if (responseJSON.has("error")) {
                                        isSuccessfull = false;
                                        message = responseJSON.optString("error");
                                    } else {
                                        JSONArray jsonArray = responseJSON.optJSONArray("commonalities_string");
                                        messages = parseCommonalities(jsonArray);
                                    }
                                    break;
                                case 403:
                                    HashMap<String, String> eventInfo = new HashMap<>();
                                    eventInfo.put("match_id", matchId);
                                    boolean fromScenes = Utility.isSet(scenesId);
                                    eventInfo.put("from_scenes", String.valueOf(fromScenes));
                                    if (fromScenes) {
                                        eventInfo.put("event_id", scenesId);
                                    }
                                    eventInfo.put("error", responseJSON.optString("error"));
                                    TrulyMadlyTrackEvent.trackEvent(mContext, sparks, send, 0, interests_error,
                                            eventInfo, true, true);
                                    isSuccessfull = false;
                                    message = responseJSON.optString("error");
                                    break;
                            }
                            if (mSparksSenderListener != null) {
                                mSparksSenderListener.onCommonalitiesReceived(isSuccessfull, messages, message);
                            }
                        }

                        @Override
                        public void onRequestFailure(Exception exception) {
                            HashMap<String, String> eventInfo = new HashMap<>();
                            eventInfo.put("match_id", matchId);
                            boolean fromScenes = Utility.isSet(scenesId);
                            eventInfo.put("from_scenes", String.valueOf(fromScenes));
                            if (fromScenes) {
                                eventInfo.put("event_id", scenesId);
                            }
                            eventInfo.put("error", (exception != null) ? exception.getMessage() : "Unknown Error");
                            TrulyMadlyTrackEvent.trackEvent(mContext, sparks, send, 0, interests_error,
                                    eventInfo, true, true);
                            if (mSparksSenderListener != null) {
                                mSparksSenderListener.onCommonalitiesReceived(false, null, exception.getMessage());
                            }
                        }
                    };
            OkHttpHandler
                    .httpPost(mContext, ConstantsUrls.getSparksApiUrl(), GetOkHttpRequestParams
                            .getHttpRequestParams(
                                    Constants.HttpRequestType.GET_COMMONALITIES_SPARK,
                                    new CommonalitiesModal(matchId, scenesId)), customOkHttpResponseHandler);
        }

        public interface SparksSenderListener {
            void onSparkSent(boolean success, ServerCodes serverCodes, long timeTaken);

            void onCommonalitiesReceived(boolean success, ArrayList<String> messages, String message);
        }

        public class CommonalitiesModal {
            private String mMatchId;
            private String scenesId;

            public CommonalitiesModal(String mMatchId, String scenesId) {
                this.mMatchId = mMatchId;
                this.scenesId = scenesId;
            }

            public String getmMatchId() {
                return mMatchId;
            }

            public void setmMatchId(String mMatchId) {
                this.mMatchId = mMatchId;
            }

            public String getScenesId() {
                return scenesId;
            }

            public void setScenesId(String scenesId) {
                this.scenesId = scenesId;
            }
        }
    }

    //////////////////////////////////////////////////////
    // Handles sparks packages - fetching packages
    //////////////////////////////////////////////////////
    public static class PackagesHandler {
        private Context mContext;
        private PackageshandlerListener mPackageshandlerListener;
        private String mMatchId;

        public PackagesHandler(Context mContext, PackageshandlerListener mPackageshandlerListener) {
            this.mContext = mContext;
            this.mPackageshandlerListener = mPackageshandlerListener;
        }

        public void register(PackageshandlerListener mPackageshandlerListener) {
            this.mPackageshandlerListener = mPackageshandlerListener;
        }

        public String getmMatchId() {
            return mMatchId;
        }

        public void setmMatchId(String mMatchId) {
            this.mMatchId = mMatchId;
        }

        private ArrayList<String> parsePackagesDescStrings(JSONArray jsonArray) {
            ArrayList<String> descStrings = null;

            if (jsonArray != null && jsonArray.length() > 0) {
                descStrings = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    String string = jsonArray.optString(i);
                    if (Utility.isSet(string)) {
                        descStrings.add(string);
                    }
                }
            }

            return descStrings;
        }

        private ArrayList<SparkPackageModal> parsePackages(JSONArray jsonArray) {
            ArrayList<SparkPackageModal> sparkPackageModals = null;

            if (jsonArray != null && jsonArray.length() > 0) {
                sparkPackageModals = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    SparkPackageModal sparkPackageModal = parseSparkPackage(jsonArray.optJSONObject(i));
                    if (sparkPackageModal != null) {
                        sparkPackageModals.add(sparkPackageModal);
                    }
                }
            }

            return sparkPackageModals;
        }

        private SparkPackageModal parseSparkPackage(JSONObject jsonObject) {
            SparkPackageModal sparkPackageModal = null;
            if (jsonObject != null) {
                sparkPackageModal = new SparkPackageModal();
                sparkPackageModal.setmPackageSku(jsonObject.optString("google_sku"));
                String price = jsonObject.optString("price");
                sparkPackageModal.setmPrice(price);
                sparkPackageModal.setmSparkCount(jsonObject.optInt("spark_count"));
                sparkPackageModal.setmExpiryDays(jsonObject.optInt("expiry_days"));
                sparkPackageModal.setmTitle(jsonObject.optString("title"));
                sparkPackageModal.setmSubtitle(jsonObject.optString("description"));

                JSONObject metadata = jsonObject.optJSONObject("metadata");
                if (metadata != null) {
                    sparkPackageModal.setMostPopular(metadata.optBoolean("most_popular"));
                    sparkPackageModal.setChatSessionsAvailable(metadata.optBoolean("chat_assist"));
                    sparkPackageModal.setmDiscount(metadata.optInt("discount", 0));
                    sparkPackageModal.setmTag(metadata.optString("tag"));
                    sparkPackageModal.setMatchGuarantee(metadata.optBoolean("match_guarantee"));
                }
            }

            return sparkPackageModal;
        }

        public void getPackages() {
            CustomOkHttpResponseHandler customOkHttpResponseHandler =
                    new CustomOkHttpResponseHandler(mContext) {
                        @Override
                        public void onRequestSuccess(JSONObject responseJSON) {
                            int responseCode = responseJSON.optInt("responseCode", 403);
                            boolean isSuccessfull = true;
                            String message = null;
                            ArrayList<SparkPackageModal> sparkPackageModals = null;
                            switch (responseCode) {
                                case 200:
                                    if (responseJSON.has("error")) {
                                        isSuccessfull = false;
                                        message = mContext.getString(R.string.SERVER_ERROR_SMALL);
                                        HashMap<String, String> eventInfo = new HashMap<>();
                                        eventInfo.put("error", responseJSON.optString("error"));
                                        if (Utility.isSet("match_id")) {
                                            eventInfo.put("match_id", mMatchId);
                                        } else {
                                            eventInfo.put("match_id", TrulyMadlyEvent.TrulyMadlyActivities.sparks);
                                        }
                                        TrulyMadlyTrackEvent.trackEvent(mContext, sparks, buy, 0, tm_error,
                                                eventInfo, true, true);
                                    } else {
                                        JSONArray packagesJsonArray = responseJSON.optJSONArray("spark_packages");
                                        sparkPackageModals = parsePackages(packagesJsonArray);
                                        JSONArray descJsonArray = responseJSON.optJSONArray("desc_strings");

                                        String matchGuarantee = responseJSON.optString("match_guarantee");
                                        if (Utility.stringCompare("ON", matchGuarantee)) {
                                            SPHandler.setBool(mContext, ConstantsSP.SHARED_KEY_AB_SPARKS_MATCH_GUARANTEE_TAG_OVERRIDE, true);
                                        } else if (Utility.stringCompare("OFF", matchGuarantee)) {
                                            SPHandler.setBool(mContext, ConstantsSP.SHARED_KEY_AB_SPARKS_MATCH_GUARANTEE_TAG_OVERRIDE, false);
                                        } else if (Utility.stringCompare("AB", matchGuarantee)) {
                                            SPHandler.remove(mContext, ConstantsSP.SHARED_KEY_AB_SPARKS_MATCH_GUARANTEE_TAG_OVERRIDE);
                                        }
                                    }

                                    break;
                                case 403:
                                    HashMap<String, String> eventInfo = new HashMap<>();
                                    eventInfo.put("error", responseJSON.optString("error"));
                                    if (Utility.isSet("match_id")) {
                                        eventInfo.put("match_id", mMatchId);
                                    } else {
                                        eventInfo.put("match_id", TrulyMadlyEvent.TrulyMadlyActivities.sparks);
                                    }
                                    TrulyMadlyTrackEvent.trackEvent(mContext, sparks, buy, 0, tm_error,
                                            eventInfo, true, true);
                                    isSuccessfull = false;
                                    message = mContext.getString(R.string.SERVER_ERROR_SMALL);
                                    break;
                            }

                            if (mPackageshandlerListener != null) {
                                mPackageshandlerListener.onPackagesReceived(isSuccessfull, sparkPackageModals,
                                        null, message);
                            }
                        }

                        @Override
                        public void onRequestFailure(Exception exception) {
                            HashMap<String, String> eventInfo = new HashMap<>();
                            eventInfo.put("error", exception.getMessage());
                            if (Utility.isSet("match_id")) {
                                eventInfo.put("match_id", mMatchId);
                            } else {
                                eventInfo.put("match_id", TrulyMadlyEvent.TrulyMadlyActivities.sparks);
                            }
                            TrulyMadlyTrackEvent.trackEvent(mContext, sparks, buy, 0, tm_error,
                                    eventInfo, true, true);
                            if (mPackageshandlerListener != null) {
                                int messageId = R.string.whoops_no_internet;
                                if (!Utility.isNetworkFailed(mContext, exception)) {
                                    messageId = R.string.SERVER_ERROR_SMALL;
                                }
                                mPackageshandlerListener.onPackagesReceived(false, null, null, mContext.getString(messageId));
                            }
                        }
                    };
            OkHttpHandler
                    .httpPost(mContext, ConstantsUrls.getSparksApiUrl(), GetOkHttpRequestParams
                            .getHttpRequestParams(
                                    Constants.HttpRequestType.GET_PACKAGES,
                                    PaymentFor.spark.name()), customOkHttpResponseHandler);
        }

        public interface PackageshandlerListener {
            void onPackagesReceived(boolean success, ArrayList<SparkPackageModal> sparkPackageModals,
                                    ArrayList<String> mDescStrings,
                                    String message);
        }
    }

}