package com.trulymadly.android.app.utility;

import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.text.Html;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.trulymadly.android.app.R;
import com.trulymadly.android.app.modal.SelectPackageModal;

import java.util.ArrayList;

import static butterknife.ButterKnife.findById;

/**
 * Created by avin on 27/10/16.
 */

public class TMSelectPackagesUIHandler {
    private Context mContext;
    private View mMasterView, mMainScreen;
    private View mFirstPkgContainer, mSecondPkgContainer, mThirdPkgContainer, mPaymentSuccessScreen, mOkFAB, mRestortePurchasesTV;

    private TextView mFirstPkgPriceTV, mSecondPkgPriceTV, mThirdPkgPriceTV;
    private TextView mFirstPkgExpiryTV, mSecondPkgExpiryTV, mThirdPkgExpiryTV;
    private TextView mFirstPkgTagTV, mSecondPkgTagTV, mThirdPkgTagTV;
    private TextView mFirstPerUnitTV, mSecondPerUnitTV, mThirdPerUnitTV;
    private TextView mCongratsTV, mYouAreSelectMemberTV, mEnjoyExperienceTV, mLetUsHandpickTV;

    private ArrayList<SelectPackageModal> mSelectPackageModals;
    private View.OnClickListener mOnClickListener;
    private int mSelectedHeight, mUnSelectedHeight;
    private int mSelectedTagTextColor, mUnSelectedTagTextColor;

    private TMSelectMainContentUIHandler mTMSelectMainContentUIHandler;

    public TMSelectPackagesUIHandler(Context mContext, View mMasterView,
                                     ArrayList<SelectPackageModal> mSelectPackageModals,
                                     View.OnClickListener mOnClickListener) {
        this.mContext = mContext;
        this.mMasterView = mMasterView;
        this.mSelectPackageModals = mSelectPackageModals;
        this.mOnClickListener = mOnClickListener;

        mMainScreen = findById(mMasterView, R.id.main_screen);
        mTMSelectMainContentUIHandler = new TMSelectMainContentUIHandler(mContext, mMainScreen);

        mSelectedHeight = mContext.getResources().getDimensionPixelSize(R.dimen.select_package_selected_height);
        mUnSelectedHeight = mContext.getResources().getDimensionPixelSize(R.dimen.select_package_unselected_height);

        mSelectedTagTextColor = ActivityCompat.getColor(mContext, R.color.white);
        mUnSelectedTagTextColor = ActivityCompat.getColor(mContext, R.color.white);

        mRestortePurchasesTV = findById(mMasterView, R.id.restore_purchases_tv);
        mRestortePurchasesTV.setOnClickListener(mOnClickListener);

        mPaymentSuccessScreen = findById(mMasterView, R.id.payment_success_screen);
        mPaymentSuccessScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        mCongratsTV = findById(mMasterView, R.id.congrats_tv);
        mLetUsHandpickTV = findById(mMasterView, R.id.let_us_handpick_tv);
//        mLetUsHandpickTV.setText(String.format(mContext.getString(R.string.let_us_handpick),
//                (Utility.isMale(mContext)) ? mContext.getString(R.string.women) : mContext.getString(R.string.men)));
        mYouAreSelectMemberTV = findById(mMasterView, R.id.you_are_tm_select_member_tv);
        mEnjoyExperienceTV = findById(mMasterView, R.id.enjoy_all_new_tm_tv);
        mOkFAB = findById(mMasterView, R.id.ok_fab);
        mOkFAB.setOnClickListener(mOnClickListener);

        mFirstPkgContainer = findById(mMasterView, R.id.first_package_container);
        mSecondPkgContainer = findById(mMasterView, R.id.second_package_container);
        mThirdPkgContainer = findById(mMasterView, R.id.third_package_container);

        mFirstPkgPriceTV = findById(mFirstPkgContainer, R.id.first_package_price_tv);
        mSecondPkgPriceTV = findById(mSecondPkgContainer, R.id.second_package_price_tv);
        mThirdPkgPriceTV = findById(mThirdPkgContainer, R.id.third_package_price_tv);

        mFirstPkgExpiryTV = findById(mFirstPkgContainer, R.id.first_package_dur_tv);
        mSecondPkgExpiryTV = findById(mSecondPkgContainer, R.id.second_package_dur_tv);
        mThirdPkgExpiryTV = findById(mThirdPkgContainer, R.id.third_package_dur_tv);

        mFirstPerUnitTV = findById(mFirstPkgContainer, R.id.first_per_unit_tv);
        mSecondPerUnitTV = findById(mSecondPkgContainer, R.id.second_per_unit_tv);
        mThirdPerUnitTV = findById(mThirdPkgContainer, R.id.third_per_unit_tv);

        mFirstPkgTagTV = findById(mFirstPkgContainer, R.id.first_package_top_tag);
        mSecondPkgTagTV = findById(mSecondPkgContainer, R.id.second_package_top_tag);
        mThirdPkgTagTV = findById(mThirdPkgContainer, R.id.third_package_top_tag);

        mFirstPkgPriceTV.setText(String.format(mContext.getString(R.string.ruppee_value),
                String.valueOf(mSelectPackageModals.get(0).getmPricePerUnit())));
        mSecondPkgPriceTV.setText(String.format(mContext.getString(R.string.ruppee_value),
                String.valueOf(mSelectPackageModals.get(1).getmPricePerUnit())));
        mThirdPkgPriceTV.setText(String.format(mContext.getString(R.string.ruppee_value),
                String.valueOf(mSelectPackageModals.get(2).getmPricePerUnit())));

//        mFirstPkgExpiryTV.setText(getPriceForDurationString(mSelectPackageModals.get(0).getmPrice(),
//                mSelectPackageModals.get(0).getmDurationInMonths()));
//        mSecondPkgExpiryTV.setText(getPriceForDurationString(mSelectPackageModals.get(1).getmPrice(),
//                mSelectPackageModals.get(1).getmDurationInMonths()));
//        mThirdPkgExpiryTV.setText(getPriceForDurationString(mSelectPackageModals.get(2).getmPrice(),
//                mSelectPackageModals.get(2).getmDurationInMonths()));

        mFirstPkgExpiryTV.setText(mSelectPackageModals.get(0).getmSubText());
        mSecondPkgExpiryTV.setText(mSelectPackageModals.get(1).getmSubText());
        mThirdPkgExpiryTV.setText(mSelectPackageModals.get(2).getmSubText());

        mFirstPerUnitTV.setText(mSelectPackageModals.get(0).getmPerUnit());
        mSecondPerUnitTV.setText(mSelectPackageModals.get(1).getmPerUnit());
        mThirdPerUnitTV.setText(mSelectPackageModals.get(2).getmPerUnit());

        mFirstPkgTagTV.setText(mSelectPackageModals.get(0).getmTag());
        mSecondPkgTagTV.setText(mSelectPackageModals.get(1).getmTag());
        mThirdPkgTagTV.setText(mSelectPackageModals.get(2).getmTag());

        mFirstPkgContainer.setOnClickListener(mOnClickListener);
        mSecondPkgContainer.setOnClickListener(mOnClickListener);
        mThirdPkgContainer.setOnClickListener(mOnClickListener);

        if (mSelectPackageModals.get(0).isPopular()) {
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) mFirstPkgContainer.getLayoutParams();
            layoutParams.height = mSelectedHeight;
            mFirstPkgContainer.setLayoutParams(layoutParams);

            mFirstPkgTagTV.setTextColor(mSelectedTagTextColor);
            mFirstPkgTagTV.setBackgroundResource(R.drawable.rect_rounded_top_green);
        } else if (mSelectPackageModals.get(1).isPopular()) {
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) mSecondPkgContainer.getLayoutParams();
            layoutParams.height = mSelectedHeight;
            mSecondPkgContainer.setLayoutParams(layoutParams);

            mSecondPkgTagTV.setTextColor(mSelectedTagTextColor);
            mSecondPkgTagTV.setBackgroundResource(R.drawable.rect_rounded_top_green);
        } else if (mSelectPackageModals.get(2).isPopular()) {
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) mThirdPkgContainer.getLayoutParams();
            layoutParams.height = mSelectedHeight;
            mThirdPkgContainer.setLayoutParams(layoutParams);

            mThirdPkgTagTV.setTextColor(mSelectedTagTextColor);
            mThirdPkgTagTV.setBackgroundResource(R.drawable.rect_rounded_top_green);
        }
    }

    private String getDurationString(int months) {
        String durationString;
        if (months <= 1) {
            durationString = String.format(mContext.getString(R.string.month), String.valueOf(months));
        } else {
            durationString = String.format(mContext.getString(R.string.months), String.valueOf(months));
        }

        return durationString;
    }

    private String getPriceForDurationString(int price, int months) {
        String durationString;
        if (months <= 1) {
            durationString = String.format(mContext.getString(R.string.ruppees_for_month), String.valueOf(price),
                    months);
        } else {
            durationString = String.format(mContext.getString(R.string.ruppees_for_months), String.valueOf(price),
                    months);
        }

        return durationString;
    }

    public void toggleSuccessScreen(boolean show, boolean isFresh) {
        mCongratsTV.setText(Html.fromHtml(String.format(mContext.getString(R.string.congrats_selectmember),
                Utility.getMyName(mContext))));
        if (isFresh) {
            mYouAreSelectMemberTV.setText(Html.fromHtml(String.valueOf(mContext.getText(R.string.you_are_now_select_member))));
            mEnjoyExperienceTV.setText(R.string.all_new_tm_select);
            mEnjoyExperienceTV.setVisibility(View.VISIBLE);
        } else {
            mYouAreSelectMemberTV.setText(R.string.tm_select_extended);
            mEnjoyExperienceTV.setVisibility(View.GONE);
        }
        mPaymentSuccessScreen.setVisibility((show) ? View.VISIBLE : View.GONE);
    }

    public void toggleMainScreen(boolean show) {
        mMainScreen.setVisibility((show) ? View.VISIBLE : View.GONE);
    }

    public void toggleRestorePurchases(boolean show) {
        mRestortePurchasesTV.setVisibility((show) ? View.VISIBLE : View.GONE);
    }

    public void onDestroy() {
        if (mTMSelectMainContentUIHandler != null) {
            mTMSelectMainContentUIHandler.onDestroy();
        }
    }
}
