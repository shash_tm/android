package com.trulymadly.android.app.utility;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.AlbumFullViewPager;
import com.trulymadly.android.app.ConversationListActivity;
import com.trulymadly.android.app.DateSpotProfile;
import com.trulymadly.android.app.EditPartnerPreferencesNew;
import com.trulymadly.android.app.EditProfileActivity;
import com.trulymadly.android.app.MatchesPageLatest2;
import com.trulymadly.android.app.MenuFullViewPager;
import com.trulymadly.android.app.MessageOneonOneConversionActivity;
import com.trulymadly.android.app.ProfileNew;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.Registration;
import com.trulymadly.android.app.TrulyMadlyApplication;
import com.trulymadly.android.app.TrustBuilderNative;
import com.trulymadly.android.app.UserPhotos;
import com.trulymadly.android.app.WebViewUrlActivity;
import com.trulymadly.android.app.activities.BuyPackagesActivity;
import com.trulymadly.android.app.activities.BuyPackagesActivityNew;
import com.trulymadly.android.app.activities.CategoriesActivity;
import com.trulymadly.android.app.activities.DateSpotList;
import com.trulymadly.android.app.activities.DeleteAccountActivity;
import com.trulymadly.android.app.activities.EditFavoritesActivity;
import com.trulymadly.android.app.activities.EventProfileActivity;
import com.trulymadly.android.app.activities.FAQActivity;
import com.trulymadly.android.app.activities.FullImageViewerActivity;
import com.trulymadly.android.app.activities.HelpActivity;
import com.trulymadly.android.app.activities.MultipleLocationsFragment;
import com.trulymadly.android.app.activities.PlayVideoActivity;
import com.trulymadly.android.app.activities.SettingsActivity;
import com.trulymadly.android.app.activities.SingleMatchActivity;
import com.trulymadly.android.app.activities.TMExtrasActivity;
import com.trulymadly.android.app.activities.TMSelectActivity;
import com.trulymadly.android.app.activities.TrimmerActivity;
import com.trulymadly.android.app.activities.VideoCaptureActivity;
import com.trulymadly.android.app.activities.VoucherActivity;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.listener.ConfirmDialogInterface;
import com.trulymadly.android.app.listener.CustomOkHttpResponseHandler;
import com.trulymadly.android.app.modal.CuratedDealsChatsModal;
import com.trulymadly.android.app.modal.EventChatModal;
import com.trulymadly.android.app.modal.Photos;
import com.trulymadly.android.app.modal.UserInfoModal;
import com.trulymadly.android.app.modal.VideoModal;

import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by udbhav on 10/11/15.
 */
public class ActivityHandler {

    public static boolean appInstalledOrNot(Context aContext, String uri) {
        PackageManager pm = aContext.getPackageManager();
        boolean app_installed = false;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException | RuntimeException ignored) {
            app_installed = false;
        }
        return app_installed;
    }


    public static void startVideoCaptureActivity(Context aContext) {
        Intent intent = new Intent(aContext, VideoCaptureActivity.class);
        try {
            aContext.startActivity(intent);
        } catch (NullPointerException ignored) {
            // gionee
        }
    }

    public static void startPlayVideoActivity(Context aContext, String thumbnailUrl, String url) {
        Intent intent = new Intent(aContext, PlayVideoActivity.class);
        intent.putExtra("url", url);
        intent.putExtra("thumbnailUrl", thumbnailUrl);
        try {
            aContext.startActivity(intent);
        } catch (NullPointerException ignored) {
            // gionee
        }
    }

    public static void startPlayVideoActivity(Context aContext, Photos photo) {
        Intent intent = new Intent(aContext, PlayVideoActivity.class);
        intent.putExtra("photo", photo);
        try {
            aContext.startActivity(intent);
        } catch (NullPointerException ignored) {
            // gionee
        }
    }

    public static void startVideoCaptureActivityForResult(Activity act, int requestCode) {
        Intent intent = new Intent(act, VideoCaptureActivity.class);
        try {
            act.startActivityForResult(intent, requestCode);
        } catch (NullPointerException ignored) {
            // gionee
        }
    }


    public static void startMatchesActivity(Context aContext) {
        startMatchesActivity(aContext, false, false);
    }

    public static void startMatchesActivity(Context aContext, boolean startSparksOnLaunch, boolean startSelectOnLaunch) {
        Intent intent = new Intent(aContext, MatchesPageLatest2.class);
        intent.putExtra("startSparksOnLaunch", startSparksOnLaunch);
        intent.putExtra("startSelectOnLaunch", startSelectOnLaunch);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        try {
            aContext.startActivity(intent);
        } catch (NullPointerException ignored) {
            // gionee
        }
    }

    public static void startRegistrationFlowActivity(Context aContext,
                                                     boolean isCitySet, boolean isFbConnected, String fbDesignation) {
        Intent intent = new Intent(aContext, Registration.class);
        intent.putExtra("isCitySet", isCitySet);
        intent.putExtra("isFbConnected", isFbConnected);
        intent.putExtra("fbDesignation", fbDesignation);
        // intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        try {
            aContext.startActivity(intent);
        } catch (NullPointerException ignored) {
            // gionee
        }
    }

    public static void startWebViewUrlActivity(Context aContext, String title,
                                                String url) {
        startWebViewUrlActivity(aContext, title, url, false, true, false);
    }

    public static void startWebViewUrlActivity(Context aContext, String title,
                                               String url, boolean isJSEnabled,
                                               boolean isTMHeadersRequired, boolean isCrossEnabled) {
        Intent intent = new Intent(aContext, WebViewUrlActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.putExtra(WebViewUrlActivity.PARAM_TITLE, title);
        intent.putExtra(WebViewUrlActivity.PARAM_URL, url);
        intent.putExtra(WebViewUrlActivity.PARAM_HEADERS_REQUIRED, isTMHeadersRequired);
        intent.putExtra(WebViewUrlActivity.PARAM_JS_ENABLED, isJSEnabled);
        intent.putExtra(WebViewUrlActivity.PARAM_CROSS_ENABLED, isCrossEnabled);
        aContext.startActivity(intent);
    }


    public static void startTermsConditionsActivity(Context aContext) {
        startWebViewUrlActivity(aContext, aContext.getResources().getString(R.string.terms_of_use), ConstantsUrls.get_terms_url());
    }

    public static void startPrivacyActivity(Context aContext) {
        startWebViewUrlActivity(aContext, aContext.getResources().getString(R.string.privacy_policy),
                ConstantsUrls.get_privacy_url());
    }

    public static void startSafetyActivity(Context aContext) {
        startWebViewUrlActivity(aContext, aContext.getResources().getString(R.string.safety_guidelines),
                ConstantsUrls.get_safety_url());
    }

    public static void startTrustSecurityActivity(Context aContext) {
        startWebViewUrlActivity(aContext, aContext.getResources().getString(R.string.trust_security),
                ConstantsUrls.get_trust_security_url());
    }

    public static void startTrueCompatibilityActivity(Context aContext) {
        startWebViewUrlActivity(aContext, aContext.getResources().getString(R.string.true_compatibility),
                ConstantsUrls.get_true_compatibility_url());
    }

    public static void startConversationListActivity(Context aContext) {
        startConversationListActivity(aContext, null);
    }

    public static void startConversationListActivity(Context aContext,
                                                     Bundle bundle) {
        Intent intent = new Intent(aContext, ConversationListActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        try {
            aContext.startActivity(intent);
        } catch (NullPointerException ignored) {
            // gionee
        }
    }


    public static void startEditPartnerPreferencesActivity(Context aContext) {
        Intent intent = new Intent(aContext, EditPartnerPreferencesNew.class);
        // intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        try {
            aContext.startActivity(intent);
        } catch (NullPointerException ignored) {

        }
        if (getTopActivityName().equalsIgnoreCase("EditPartnerPreferencesNew")) {
            ((Activity) aContext).finish();
        }
    }

    public static void startVoucherActivity(Context aContext, Boolean isPush) {
        Intent intent = new Intent(aContext, VoucherActivity.class);
        intent.putExtra("isPush", isPush);
        aContext.startActivity(intent);
    }

    public static void startActivityFromString(Context aContext,
                                               Boolean isPush, String className, boolean isDeepLink) {
        Class<?> c = null;
        if (className != null) {
            try {
                c = Class.forName(className);
                Intent intent = new Intent(aContext, c);
                intent.putExtra("isPush", isPush);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                if (isDeepLink) {
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                }
                aContext.startActivity(intent);
            } catch (ClassNotFoundException ignored) {
            }
        }
    }

    public static void startTrustBuilder(Context aContext, Boolean isPush,
                                         Boolean isRegistration) {
        Intent intent = new Intent(aContext, TrustBuilderNative.class);
        intent.putExtra("isPush", isPush);
        intent.putExtra("isRegistration", isRegistration);
        // intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        try {
            aContext.startActivity(intent);
        } catch (NullPointerException ignored) {
            // gionee
        }
    }

    public static void startTrustBuilderForResult(Activity aActivity) {
        Intent intent = new Intent(aActivity, TrustBuilderNative.class);
        intent.putExtra("isActivityForResult", true);
        try {
            aActivity.startActivityForResult(intent,
                    Constants.GET_TRUST_FROM_ACTIVITY);
        } catch (NullPointerException ignored) {
            // gionee
        }
    }

    public static void startSingleMatchActivityForResult(Activity aActivity, UserInfoModal user, String eventId, int noOfUsers) {
        Intent intent = new Intent(aActivity, SingleMatchActivity.class);
        intent.putExtra("userInfoModal", user);
        intent.putExtra("eventId", eventId);
        intent.putExtra("noOfUsers", noOfUsers);
        try {
            aActivity.startActivityForResult(intent,
                    Constants.LIKE_OR_HIDE_OF_SINGLE_MATCH);
        } catch (NullPointerException ignored) {
            // gionee
        }
    }

    public static void startMessageOneonOneActivity(Context aContext,
                                                    String match_id, String message_url, Boolean isPush,
                                                    Boolean isFeedbackForm, Boolean isUnread, boolean isFromSpark) {
        Intent intent = new Intent(aContext,
                MessageOneonOneConversionActivity.class);
        if (Utility.isSet(match_id)) {
            intent.putExtra("match_id", match_id);
        }
        if (Utility.isSet(message_url)) {
            intent.putExtra("message_one_one_conversion_url", message_url);
        }
        intent.putExtra("isPush", isPush);
        intent.putExtra("isFeedbackForm", isFeedbackForm);
        intent.putExtra("isUnread", isUnread);
        intent.putExtra("isFromSpark", isFromSpark);

        try {
            if (getTopActivityName().equalsIgnoreCase(
                    "MessageOneonOneConversionActivity")) {
                aContext.startActivity(intent);
                ((Activity) aContext).finish();
            } else {
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                aContext.startActivity(intent);
            }
        } catch (NullPointerException ignored) {
            // gionee
        }
    }

    public static void startEditProfileActivityForResult(Activity aActivity,
                                                         Constants.EditProfilePageType launchType) {
        try {
            Intent intent = new Intent(aActivity, EditProfileActivity.class);
            if (launchType != null) {
                intent.putExtra("launchType", launchType.toString());
            }
            aActivity.startActivityForResult(intent,
                    Constants.EDIT_PROFILE_FROM_PROFILE);
        } catch (NullPointerException ignored) {
            // gionee
        }
    }

    public static void startEditFavoritesActivity(Context aContext, int position, boolean fromMyProfile) {
        try {
            Intent intent = new Intent(aContext, EditFavoritesActivity.class);
            intent.putExtra("launchPosition", position);
            if (fromMyProfile) {
                intent.putExtra("fromMyProfile", true);
            }
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            aContext.startActivity(intent);
        } catch (NullPointerException ignored) {
            // gionee
        }
    }

    public static void startEditFavoritesActivityForResult(Activity aActivity, int requestCode, int position, boolean fromMatches) {
        try {
            Intent intent = new Intent(aActivity, EditFavoritesActivity.class);
            if (position >= 0 && position < FavoriteUtils.PAGE_COUNT) {
                intent.putExtra("launchPosition", position);
                intent.putExtra("isActivityForResult", true);
                if (fromMatches) {
                    intent.putExtra("fromMatches", true);

                }
            }
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            aActivity.startActivityForResult(intent, requestCode);
        } catch (NullPointerException ignored) {
            // gionee
        }
    }

    public static void startProfileActivity(Context aContext,
                                            String profileUrl, boolean isPush,
                                            boolean isMyProfile, boolean isFromRegistration) {
        Intent intent = new Intent(aContext, ProfileNew.class);
        // intent.putExtra("profileUrl", Constants.my_profile_url);
        if (profileUrl != null) {
            intent.putExtra("profileUrl", profileUrl);
        }

        if (isMyProfile)
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);

        intent.putExtra("isPush", isPush);
        intent.putExtra("isMyProfile", isMyProfile);
        intent.putExtra("isFromRegistration", isFromRegistration);
        try {
            aContext.startActivity(intent);
        } catch (NullPointerException ignored) {
            // gionee
        }

    }

    public static void startDeleteAccountActivity(Context aContext) {
        try {
            Intent intent = new Intent(aContext, DeleteAccountActivity.class);
            aContext.startActivity(intent);
        } catch (NullPointerException e) {
            // gionee
        }
    }

    public static void startEventProfileActivityForResult(Activity activity, String eventId) {
        try {
            Intent intent = new Intent(activity, EventProfileActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("eventId", eventId);
            activity.startActivityForResult(intent, Constants.FOLLOW_EVENT);
        } catch (NullPointerException e) {
            // gionee
        }
    }

    public static void startEventProfileActivity(Context ctx, String eventId, String from, boolean isDeepLink) {
        try {
            Intent intent = new Intent(ctx, EventProfileActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            intent.putExtra("eventId", eventId);
            if (Utility.isSet(from)) {
                intent.putExtra("from", from);
            }
            if (isDeepLink) {
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            }
            ctx.startActivity(intent);
        } catch (NullPointerException e) {
            // gionee
        }
    }

    public static void startPhotosForResult(Activity aActivity,
                                            boolean showUploadSlider) {
        startPhotosForResult(aActivity, showUploadSlider, null);
    }

    public static void startPhotosForResult(Activity aActivity,
                                            boolean showUploadSlider, Bundle extras) {
        Intent intent = new Intent(aActivity, UserPhotos.class);
        intent.putExtra("showUploadSlider", showUploadSlider);
        if (extras != null)
            intent.putExtras(extras);
        try {
            aActivity.startActivityForResult(intent,
                    Constants.GET_PHOTOS_FROM_ACTIVITY);
        } catch (NullPointerException ignored) {
            // gionee
        }
    }

    public static void startPhotos(Context aContext,
                                   boolean showUploadSlider, Bundle extras) {
        Intent intent = new Intent(aContext, UserPhotos.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.putExtra("showUploadSlider", showUploadSlider);
        if (extras != null)
            intent.putExtras(extras);
        try {
            aContext.startActivity(intent);
        } catch (NullPointerException ignored) {
            // gionee
        }
    }

    private static String getTopActivityName() {
        return TrulyMadlyApplication.getVisibleActivityName();
    }

    public static void openAppSettings(Context context) {
        Intent intent = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.setData(Uri.parse("package:" + context.getPackageName()));
        try {
            context.startActivity(intent);
        } catch (NullPointerException ignored) {
            // gionee
        }
    }

    public static void startSettingsActivity(Context aContext) {
        Intent intent = new Intent(aContext, SettingsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        try {
            aContext.startActivity(intent);
        } catch (NullPointerException ignored) {
            //gionee
        }
    }

    public static void startCategoriesActivity(Context aContext) {
        Intent intent = new Intent(aContext, CategoriesActivity.class);
        try {
            aContext.startActivity(intent);
        } catch (NullPointerException ignored) {
            //gionee
        }
    }

    public static void startCategoriesActivity(Context aContext, String push_category_id, String push_category_name) {
        Intent intent = new Intent(aContext, CategoriesActivity.class);
        intent.putExtra("push_category_id", push_category_id);
        intent.putExtra("push_category_name", push_category_name);
        try {
            aContext.startActivity(intent);
        } catch (NullPointerException ignored) {
            //gionee
        }
    }


    public static void startDateSpotActivity(@NonNull Context aContext, String dateSpotId, String dealId, @NonNull String matchId, @NonNull String messageOneOnOneUrl) {
        Intent intent = new Intent(aContext, DateSpotProfile.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.putExtra("dateSpotId", dateSpotId);
        intent.putExtra("dealId", dealId);
        intent.putExtra("matchId", matchId);
        intent.putExtra("messageOneOnOneUrl", messageOneOnOneUrl);
        try {
            aContext.startActivity(intent);
        } catch (NullPointerException ignored) {
            //gionee
        }
    }

    public static void startDateSpotActivity(@NonNull Context aContext, String dateSpotId, String dealId, @NonNull String matchId, @NonNull String messageOneOnOneUrl,
                                             boolean isSpecial, boolean isNew, String locationScore, String popularityScore, String totalScore, String pricing, String zone_id,
                                             String geo_location, String rank) {
        Intent intent = new Intent(aContext, DateSpotProfile.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.putExtra("dateSpotId", dateSpotId);
        intent.putExtra("dealId", dealId);
        intent.putExtra("matchId", matchId);
        intent.putExtra("messageOneOnOneUrl", messageOneOnOneUrl);
        intent.putExtra("isSpecial", isSpecial);
        intent.putExtra("isNew", isNew);
        intent.putExtra("locationScore", locationScore);
        intent.putExtra("popularityScore", popularityScore);
        intent.putExtra("totalScore", totalScore);
        intent.putExtra("pricing", pricing);
        intent.putExtra("zone_id", zone_id);
        intent.putExtra("geo_location", geo_location);
        intent.putExtra("rank", rank);
        try {
            aContext.startActivity(intent);
        } catch (NullPointerException ignored) {
            //gionee
        }
    }

    public static void startDateSpotListActivity(Context aContext,
                                                 String match_id, String message_url, boolean isPush) {
        Intent intent = new Intent(aContext, DateSpotList.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        if (Utility.isSet(match_id)) {
            intent.putExtra("match_id", match_id);
        }
        if (Utility.isSet(message_url)) {
            intent.putExtra("message_one_one_conversion_url", message_url);
        }
        intent.putExtra("isPush", isPush);
        try {
            aContext.startActivity(intent);
        } catch (NullPointerException ignored) {
            //gionee
        }
    }

    public static void startFaqsActivity(Context aContext) {
        Intent intent = new Intent(aContext, FAQActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        try {
            aContext.startActivity(intent);
        } catch (NullPointerException ignored) {
            //gionee
        }
    }

    public static void startHelpActivity(Context aContext) {
        Intent intent = new Intent(aContext, HelpActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        try {
            aContext.startActivity(intent);
        } catch (NullPointerException ignored) {
            //gionee
        }

    }

    public static void startMenuFullViewPagerForResult(Activity aActivity, int pos, String[] pics, String title) {
        if (pics == null) {
            return;
        }

        MenuFullViewPager.pics = pics;
        Intent albumIntent = new Intent(aActivity, MenuFullViewPager.class);
        albumIntent.putExtra("title", title);
        albumIntent.putExtra("POS", pos);
        try {
            aActivity.startActivityForResult(albumIntent, Constants.FROM_FULL_ALBUM_PAGE);
        } catch (NullPointerException e) {
            // gionee
        }
    }

    public static void startAlbumFullViewPagerForResult(Activity aActivity, int pos, String[] pics,
                                                        VideoModal[] videoModals, String title,
                                                        String trkActivity, String userId) {
        if (pics == null) {
            return;
        }

        AlbumFullViewPager.pics = pics;
        AlbumFullViewPager.videoModals = videoModals;
        Intent albumIntent = new Intent(aActivity, AlbumFullViewPager.class);
        albumIntent.putExtra("title", title);
        albumIntent.putExtra("POS", pos);
        albumIntent.putExtra("trkActivity", trkActivity);
        albumIntent.putExtra("user_id", userId);
        try {
            aActivity.startActivityForResult(albumIntent, Constants.FROM_FULL_ALBUM_PAGE);
        } catch (NullPointerException e) {
            // gionee
        }
    }

    public static void startAlbumFullViewPager(Context context, int pos, String[] pics, String title) {
        if (pics == null) {
            return;
        }

        AlbumFullViewPager.pics = pics;
        Intent albumIntent = new Intent(context, AlbumFullViewPager.class);
        albumIntent.putExtra("title", title);
        albumIntent.putExtra("POS", pos);
        try {
            context.startActivity(albumIntent);
        } catch (NullPointerException e) {
            // gionee
        }
    }

    public static void startTrimActivity(Activity activity, @NonNull Uri uri, int orientationDegree, String source, String camera) {
        Intent intent = new Intent(activity, TrimmerActivity.class);
        intent.putExtra("path", FilesHandler.getPath(activity, uri, uri.getScheme()));
        intent.putExtra("rotate", orientationDegree);
        intent.putExtra("source", source);
        intent.putExtra("camera", camera);

        try {
            activity.startActivityForResult(intent, Constants.TRIM_VIDEO_REQUEST_CODE);
        } catch (NullPointerException e) {
            // gionee
        }
    }

    public static void startTrimActivity(Activity activity, @NonNull String path, int orientationDegree, String source, String camera) {
        Intent intent = new Intent(activity, TrimmerActivity.class);
        intent.putExtra("path", path);
        intent.putExtra("rotate", orientationDegree);
        intent.putExtra("source", source);
        intent.putExtra("camera", camera);

        try {
            activity.startActivityForResult(intent, Constants.TRIM_VIDEO_REQUEST_CODE);
        } catch (NullPointerException e) {
            // gionee
        }
    }

    /**
     * Starts the FullImageViewerActivity with the given parameters
     *
     * @param context
     * @param pics       - Array of the images to be loaded into the FullImageViewer
     * @param folderName - FolderName to be used in case caching is enabled using flag isCachingEnabled
     */
    public static void startFullImageViewerActivity(Context context, String[] pics, boolean isZoomable,
                                                    boolean isCachingEnabled, String folderName) {
        if (pics == null || pics.length == 0) {
            return;
        }

        Intent albumIntent = new Intent(context, FullImageViewerActivity.class);
        albumIntent.putExtra(FullImageViewerActivity.IMAGES, pics);
        albumIntent.putExtra(FullImageViewerActivity.FOLDER_NAME, folderName);
        albumIntent.putExtra(FullImageViewerActivity.IS_ZOOMABLE, isZoomable);
        albumIntent.putExtra(FullImageViewerActivity.IS_CACHING_ENABLED, isCachingEnabled);
        try {
            context.startActivity(albumIntent);
        } catch (NullPointerException e) {
            // gionee
        }
    }


    public static void shareViaTextIntent(final Activity aActivity) {
        // getBool custom share url
        String userId = Utility.getMyId(aActivity);
        String gender = SPHandler.getString(aActivity,
                ConstantsSP.SHARED_KEYS_USER_GENDER);
        String longUrl = Constants.default_share_long_url;
        if (Utility.isSet(userId)) {
            longUrl += "_" + userId;
        }
        if (Utility.isSet(gender)) {
            longUrl += "_" + gender;
        }

        GooGlUrlShortner.GooGlUrlShortnerInterface responseInterface = new GooGlUrlShortner.GooGlUrlShortnerInterface() {

            @Override
            public void onComplete(String shortUrl) {
                createTextIntent(aActivity, aActivity.getResources().getString(R.string.default_share_text) + " "
                        + shortUrl, false);

            }
        };
        new GooGlUrlShortner(aActivity, responseInterface, longUrl,
                Constants.default_share_short_url);
        // REMOVING GOO.GL call to save net traffic.
        //createTextIntent(aContext, Constants.default_share_text + " " + Constants.default_share_short_url);

    }

    public static void createTextIntent(Activity aActivity, String shareText, boolean isStartingForResult) {
        createTextIntentForApp(aActivity, shareText, null, null, isStartingForResult);
    }

    public static void createTextIntent(Activity aActivity, String shareText, String subject, boolean isStartingForResult) {
        createTextIntentForApp(aActivity, shareText, null, subject, isStartingForResult);
    }

    public static void createTextIntentForFacebook(Activity aActivity, String shareText,
                                                   boolean isStartingForResult) {
        if (appInstalledOrNot(aActivity,
                Constants.packageFacebookmessenger)) {
            createTextIntentForApp(aActivity, shareText,
                    Constants.packageFacebookmessenger, null, isStartingForResult);
        } else {
            AlertsHandler.showMessage(
                    aActivity,
                    R.string.error_connecting_facebook_messenger);
        }
    }

    public static void createTextIntentForApp(Activity aActivity,
                                              String shareText, String appPackage, String subject, boolean isStartingForResult) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, shareText);
        if (Utility.isSet(subject)) {
            sendIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        }
        sendIntent.setType("text/plain");
        if (appPackage != null) {
            // sendIntent.setPackage("com.facebook.orca");
            // sendIntent.setPackage("com.whatsapp");
            sendIntent.setPackage(appPackage);
            try {
                aActivity.startActivity(sendIntent);
            } catch (ActivityNotFoundException ignored) {
                AlertsHandler.showMessage(
                        aActivity,
                        R.string.error_connecting_app);
            }
        } else {
            try {
                if (isStartingForResult) {
                    aActivity.startActivityForResult(Intent.createChooser(sendIntent, "Share using "), Constants.SHARE_VIA_TEXT_INTENT);
                } else {
                    aActivity.startActivity(Intent.createChooser(sendIntent, "Share using "));
                }
            } catch (ActivityNotFoundException ignored) {

            }
        }
        // aContext.startActivity(sendIntent);
    }

    public static void openInstagramProfile(Context aContext, String profileName) {
        Uri uri = Uri.parse("http://instagram.com/_u/" + profileName);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);

        intent.setPackage(Constants.packageInstagram);

        try {
            aContext.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            aContext.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://www.instagram.com/" + profileName)));
        }
    }

    public static void openFacebookProfileTrulymadly(Context aContext) {
        /*
        //OLD WAY
        String tmFBId = "642669439102365";
        String tmFbURL = "https://www.facebook.com/n/?trulymadly";

        try {
            Uri uri;
            PackageManager packageManager = aContext.getPackageManager();
            int versionCode = packageManager.getPackageInfo(Constants.packageFacebook, 0).versionCode;
            if (versionCode >= 3002850) { //newer versions of fb app
                uri = Uri.parse("fb://facewebmodal/f?href=" + tmFbURL);
//                uri = Uri.parse("fb://profile/" + tmFBId);
            } else { //older versions of fb app
                uri = Uri.parse("fb://page/" + tmFBId);
            }
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            intent.setPackage(Constants.packageFacebook);
            aContext.startActivity(intent);
        } catch (Exception e) {
            aContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(tmFbURL)));
        }
        */

        try {
            aContext.getPackageManager()
                    .getPackageInfo("com.facebook.katana", 0); //Checks if FB is even installed.
            aContext.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("fb://page/642669439102365"))); //Trys to make intent with FB's URI
        } catch (Exception e) {
            aContext.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://www.facebook.com/trulymadly"))); //catches and opens a url to the desired page
        }
    }

    public static void onShareClicked(final WeakReference<Activity> weakActivity, String matchId, final String trackingActivity) {
        Map<String, String> params = new HashMap<>();
        params.put("shorten_url_conversationlist", "1");
        params.put("match_id", matchId);
        params.put("source", "converstion_list");
        Activity activity = weakActivity.get();
        if (activity != null) {
            final ProgressDialog progressDialog = UiUtils.showProgressBar(activity, null);
            CustomOkHttpResponseHandler responseHandler = new CustomOkHttpResponseHandler(activity.getApplicationContext(),
                    trackingActivity, TrulyMadlyEvent.TrulyMadlyEventTypes.ask_friend_click) {

                @Override
                public void onRequestSuccess(JSONObject responseJSON) {
                    UiUtils.hideProgressBar(progressDialog);

                    switch (responseJSON.optInt("responseCode")) {
                        case 200:
                            final JSONObject data = responseJSON.optJSONObject("response");
                            ConfirmDialogInterface confirmDialogInterface = new ConfirmDialogInterface() {

                                @Override
                                public void onPositiveButtonSelected() {
                                    Activity activityRef = weakActivity.get();
                                    if (activityRef != null) {
                                        TrulyMadlyTrackEvent.trackEvent(activityRef.getApplicationContext(), trackingActivity,
                                                TrulyMadlyEvent.TrulyMadlyEventTypes.ask_friend_confirm, 0, TrulyMadlyEvent.TrulyMadlyEventStatus.matched, null, true);

                                        createTextIntent(activityRef,
                                                data.optString("share_message") + " " + data.optString("link"), false);
                                    }

                                }

                                @Override
                                public void onNegativeButtonSelected() {
                                    Activity activityRef = weakActivity.get();
                                    if (activityRef != null) {
                                        TrulyMadlyTrackEvent.trackEvent(activityRef.getApplicationContext(), trackingActivity,
                                                TrulyMadlyEvent.TrulyMadlyEventTypes.ask_friend_cancel, 0, TrulyMadlyEvent.TrulyMadlyEventStatus.matched, null, true);
                                    }

                                }
                            };
                            Activity activityRef = weakActivity.get();
                            if (activityRef != null) {
                                AlertsHandler.showConfirmDialog(activityRef, data.optString("alert_message"), R.string.share,
                                        R.string.cancel, confirmDialogInterface, false);
                            }
                            break;

                        case 403:
                            Activity activityRef1 = weakActivity.get();
                            if (activityRef1 != null) {
                                AlertsHandler.showAlertDialog(activityRef1, responseJSON.optString("error"), null);
                            }
                            break;
                        default:
                            break;

                    }
                }

                @Override
                public void onRequestFailure(Exception exception) {
                    UiUtils.hideProgressBar(progressDialog);
                    Activity activityRef = weakActivity.get();
                    if (activityRef != null) {
                        AlertsHandler.showNetworkError(activityRef, exception);
                    }
                }

            };
            OkHttpHandler.httpGet(activity, ConstantsUrls.get_getAskFriendsUrl(), params, responseHandler);
        }
    }

    /**
     * @param context
     * @param match_id
     * @param messageConversationUrl
     * @param curatedDealsChatsModal Starts messageOneOnOneActivity
     *                               (With Bring to front setString)
     */
    public static void startMessageOneOnOneActivity(@NonNull Context context, @NonNull String match_id,
                                                    @NonNull String messageConversationUrl,
                                                    CuratedDealsChatsModal curatedDealsChatsModal) {
        Intent intent = new Intent(context, MessageOneonOneConversionActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        Bundle bundle = new Bundle();
        bundle.putString("match_id", match_id);
        bundle.putString("message_one_one_conversion_url", messageConversationUrl);
        if (curatedDealsChatsModal != null)
            bundle.putParcelable(Constants.KEY_CD_MODAL, curatedDealsChatsModal);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    /**
     * @param context
     * @param match_id
     * @param messageConversationUrl
     * @param eventChatModal         Starts messageOneOnOneActivity
     *                               (With Bring to front setString)
     */
    public static void startMessageOneOnOneActivity(@NonNull Context context, @NonNull String match_id,
                                                    @NonNull String messageConversationUrl,
                                                    EventChatModal eventChatModal) {
        Intent intent = new Intent(context, MessageOneonOneConversionActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        Bundle bundle = new Bundle();
        bundle.putString("match_id", match_id);
        bundle.putString("message_one_one_conversion_url", messageConversationUrl);
        if (eventChatModal != null)
            bundle.putParcelable(Constants.KEY_EVENT_MODAL, eventChatModal);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    /**
     * @param context
     * @param match_id
     * @param messageConversationUrl
     * @param message                Starts messageOneOnOneActivity
     *                               (With Bring to front set)
     */
    public static void startMessageOneOnOneActivity(@NonNull Context context, @NonNull String match_id,
                                                    @NonNull String messageConversationUrl,
                                                    String message) {
        Intent intent = new Intent(context, MessageOneonOneConversionActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        Bundle bundle = new Bundle();
        bundle.putString("match_id", match_id);
        bundle.putString("message_one_one_conversion_url", messageConversationUrl);
        if (Utility.isSet(message)) {
            bundle.putString(Constants.KEY_SPARK_ACCEPT_MESSAGE, message);
        }
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    public static void startMultipleLocationsActivity(@NonNull Context ctx) {
        Intent intent = new Intent(ctx, MultipleLocationsFragment.class);

        ctx.startActivity(intent);
    }

    public static void startTMExtrasActivity(@NonNull Context ctx) {
        Intent intent = new Intent(ctx, TMExtrasActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        ctx.startActivity(intent);
    }

    /**
     * Starts TMSelectActivity
     */
    public static void startTMSelectActivity(@NonNull Context ctx, String source, boolean startQuiz) {
        if (!TMSelectHandler.isSelectEnabled(ctx)) {
            return;
        }

        Intent intent = new Intent(ctx, TMSelectActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("source", source);
        bundle.putBoolean("start_quiz", startQuiz);
        intent.putExtras(bundle);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        ctx.startActivity(intent);
    }

    /**
     * Starts TMSelectActivity
     */
    public static void startTMSelectActivity(@NonNull Context ctx, String source,
                                             boolean startQuiz, int likesDone, int hidesDone,
                                             int sparksDone, boolean favViewed, String matchId) {
        Intent intent = new Intent(ctx, TMSelectActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("source", source);
        bundle.putBoolean("start_quiz", startQuiz);
        bundle.putInt("likes_done", likesDone);
        bundle.putInt("hides_done", hidesDone);
        bundle.putInt("sparks_done", sparksDone);
        bundle.putBoolean("fav_viewed", favViewed);
        bundle.putString("match_id", matchId);

        intent.putExtras(bundle);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        ctx.startActivity(intent);
    }

    public static void startBuyPackagesActivity(@NonNull Activity activity) {
        Intent intent = new Intent(activity, BuyPackagesActivity.class);
        activity.startActivityForResult(intent, Constants.BUY_PACKAGES_REQUEST_CODE);
        activity.overridePendingTransition(0, 0);
    }

    public static void startBuyPackagesActivityNew(@NonNull Activity activity) {
        Intent intent = new Intent(activity, BuyPackagesActivityNew.class);
        activity.startActivityForResult(intent, Constants.BUY_PACKAGES_REQUEST_CODE);
        activity.overridePendingTransition(0, 0);
    }

    public static boolean checkIfYoutubeApiCanbeUsed(Context aContext) {
        PackageInfo pInfo = null;
        try {
            pInfo = aContext.getPackageManager().getPackageInfo(Constants.YOUTUBE_PACKAGE_NAME, PackageManager.GET_ACTIVITIES);
            String version = pInfo.versionName;
            int verCode = pInfo.versionCode;
            version = version.replace(".", "");
            int versionInt = Integer.parseInt(version);
            Log.d("Youtube Package", "Version : " + version + " and code : " + verCode);
            return versionInt >= 4216;
        } catch (PackageManager.NameNotFoundException | NumberFormatException ignored) {

        }

        return false;
    }

    public static void launchPlayStoreTrulymadly(Activity aActivity) {
        final String appPackageName = "com.trulymadly.android.app";
        try {
            aActivity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            aActivity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }
}
