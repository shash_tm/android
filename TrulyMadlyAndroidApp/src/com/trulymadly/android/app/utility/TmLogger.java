package com.trulymadly.android.app.utility;

import android.util.Log;

import com.crashlytics.android.Crashlytics;

/**
 * Created by udbhav on 28/01/16.
 */
public class TmLogger {
    public static void log(int priorityLevel, String tag, String msg) {
        if (Utility.isSet(msg)) {
            Crashlytics.log(priorityLevel, tag, msg);
        }
    }

    public static void d(String tag, String msg) {
        log(Log.DEBUG, tag, msg);
    }

    public static void e(String tag, String msg, Throwable tr) {
        log(Log.ERROR, tag, msg + '\n' + Log.getStackTraceString(tr));
    }

    public static void e(String tag, String msg) {
        log(Log.ERROR, tag, msg);
    }

    public static void w(String tag, String msg) {
        log(Log.WARN, tag, msg);
    }

    public static void w(String tag, String msg, Throwable tr) {
        log(Log.WARN, tag, msg + '\n' + Log.getStackTraceString(tr));
    }

    public static void v(String tag, String msg) {
        log(Log.VERBOSE, tag, msg);
    }

    public static void i(String tag, String msg) {
        log(Log.INFO, tag, msg);
    }
}
