package com.trulymadly.android.app.utility;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Process;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;
import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.CircleTransformation;
import com.trulymadly.android.app.LoginActivity;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.ConstantsRF;
import com.trulymadly.android.app.modal.NotificationModal;
import com.trulymadly.android.app.modal.NotificationModal.NotificationType;
import com.trulymadly.android.app.receivers.NotificationClickReceiver;
import com.trulymadly.android.app.sqlite.RFHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by udbhav on 17/02/16.
 */
public class NotificationHandler {
    public static final String COLLAPSE_KEY_PREFIX = "TM_PUSH_NOTIFICATION_";
    private static final int NOTIFICATION_ID = 1;

    private static Bitmap getScaledBitmap(Bitmap myBitmap, Context ctx) {
        if (myBitmap != null) {
            int height = (int) ctx.getResources().getDimension(
                    android.R.dimen.notification_large_icon_height);
            int width = (int) ctx.getResources().getDimension(
                    android.R.dimen.notification_large_icon_width);
            try {
                myBitmap = Bitmap.createScaledBitmap(myBitmap, width, height,
                        false);
            } catch (OutOfMemoryError | NullPointerException ignored) {
            }
        }
        return myBitmap;
    }

    /**
     * create a local notification
     */
    public static void createLocalNotification(Context ctx,
                                               NotificationModal notificationModal) {
        sendNotification(getLocalNotifcationBundle(notificationModal), ctx, false, false, true);
    }

    private static Bundle getLocalNotifcationBundle(NotificationModal notificationModal) {
        Bundle notificationBundle = new Bundle();
        // mandatory fields
        notificationBundle.putString("push_type", notificationModal.getPushType().toString());
        notificationBundle.putString("title_text", notificationModal.getTitle_text());
        notificationBundle.putString("pic_url", notificationModal.getPic_url());
        notificationBundle.putString("content_text", notificationModal.getContent_text());
        notificationBundle.putString("ticker_text", notificationModal.getTicker_text());
        notificationBundle.putString("collapse_key", notificationModal.getCollapse_key());

        // optional fields
        notificationBundle.putString("event_status", notificationModal.getEvent_status());
        // for message
        notificationBundle.putString("match_id", notificationModal.getMatch_id());
        notificationBundle.putString("message_url", notificationModal.getMessage_url());
        notificationBundle.putString("is_admin_set", notificationModal.isAdmin() ? "1" : "0");

        // for match
        // push_profile_url = b.getString("match_profile_url");

        //for class
        if (notificationModal.getClass_name() != null)
            notificationBundle.putString("class_name", notificationModal.getClass_name());

        // for web url
        // pushWebUrl = b.getString("web_url");
        return notificationBundle;
    }

    public static void sendNotification(Bundle extras, Context c, boolean forceTotalSilence, boolean showActionButtons, boolean toTrack) {

        Context ctx = c.getApplicationContext();
        String push_type = extras.getString("push_type");
        // do nothing when silent notification
        if (!Utility.isSet(push_type)
                || NotificationType.SILENT.toString().equalsIgnoreCase(
                push_type)
                || !Utility.isSet(extras.getString("title_text"))) {
            return;
        }
        extras = stripExtrasForHideConversations(extras, c);
        if (toTrack) {
            trackEvent(extras, ctx, push_type);
        }


        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                ctx).setSmallIcon(getSmallIcon())
                .setSubText(getSubText(extras))
                .setTicker(getTickerText(extras)).setContentTitle(getContentTitle(extras))
                .setAutoCancel(true);
        mBuilder.setDefaults(getNotificationPrefs(ctx, forceTotalSilence));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBuilder.setColor(ctx.getResources().getColor(R.color.final_pink_2));
        }
        mBuilder.setContentText(getText(extras));

        Bitmap bitmap = null;
        if (NotificationType.MATCHES_IMAGE_MULTI.toString().equals(push_type)) {
            bitmap = getBitmap(extras, ctx, false);
            if (bitmap != null) {
                mBuilder.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(bitmap));
                //mBuilder.setContentText(getText(extras));
            } else {
                mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(getText(extras)));
            }
            if (showActionButtons) {
                mBuilder.addAction(R.drawable.arrow_left_grey, "BACK", NotificationClickReceiver.getNotificationClickReceiverIntent(ctx, NotificationModal.NotificationClickType.MATCHES_IMAGE_MULTI_ACTION_PREV, extras));
                mBuilder.addAction(R.drawable.arrow_right_grey, "NEXT", NotificationClickReceiver.getNotificationClickReceiverIntent(ctx, NotificationModal.NotificationClickType.MATCHES_IMAGE_MULTI_ACTION_NEXT, extras));
            }
            //mBuilder.setContentIntent(NotificationClickReceiver.getNotificationClickReceiverIntent(ctx, NotificationModal.NotificationClickType.MATCHES_IMAGE_MULTI_LAUNCH_MATCH, extras));
            mBuilder.setContentIntent(getPendingIntent(extras, ctx));
        } else {
            mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(getText(extras)));
            //mBuilder.setContentText(getText(extras));
            bitmap = getBitmap(extras, ctx, true);
            if (bitmap != null) {
                mBuilder.setLargeIcon(bitmap);
            }
            mBuilder.setContentIntent(getPendingIntent(extras, ctx));
        }


        try {
            NotificationManager mNotificationManager = (NotificationManager) ctx
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.notify(getCollapseKey(extras, push_type), NOTIFICATION_ID,
                    mBuilder.build());
        } catch (RuntimeException e) {
            e.printStackTrace();
        }

    }

    private static String getCollapseKey(Bundle extras, String push_type) {
        String collapseKey = null;
        // Setting collapse key for the case of messages.
        if (NotificationType.MESSAGE.toString().equalsIgnoreCase(push_type)) {
            collapseKey = extras.getString("collapse_key");
        }
        return collapseKey;
    }

    private static PendingIntent getPendingIntent(Bundle extras, Context ctx) {
        Intent i = new Intent(ctx, LoginActivity.class);
        i.putExtra("isPush", true);
        i.putExtras(extras);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return PendingIntent.getActivity(ctx,
                getRequestCode(extras), i, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private static void trackEvent(Bundle extras, Context ctx, String push_type) {
        String eventStatus = extras.getString("event_status");
        if (!Utility.isSet(eventStatus)) {
            eventStatus = push_type;
        }
        TrulyMadlyTrackEvent.trackEvent(ctx, TrulyMadlyEvent.TrulyMadlyActivities.push,
                TrulyMadlyEvent.TrulyMadlyEventTypes.push_received, 0, eventStatus, null, true);
    }

    private static Bitmap getBitmap(Bundle extras, Context ctx, boolean scaleToIcon) {
        Bitmap bitmap = null;
        //String profilePicUrl = null;
        RequestCreator picassoRequest;
        int profilePicFallbackResId = R.drawable.tm_logo_icon_notify;
        if (Utility.isSet(extras.getString("pic_url"))) {
            picassoRequest = Picasso.with(ctx).load(extras.getString("pic_url"));
        } else {
            picassoRequest = Picasso.with(ctx).load(profilePicFallbackResId);
        }
        try {
            // getBool bitmap using picasso
            if (scaleToIcon) {
                bitmap = getScaledBitmap(picassoRequest.transform(new CircleTransformation())
                        .get(), ctx);
            } else {
                bitmap = picassoRequest.get();
            }
        } catch (IOException | OutOfMemoryError | IllegalStateException | SecurityException | NumberFormatException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    private static String getText(Bundle extras) {
        String text = "";
        if (Utility.isSet(extras.getString("content_text"))) {
            text = Utility.stripHtml(Utility.decodeURI(extras
                    .getString("content_text")));
        }
        return text;
    }

    private static String getSubText(Bundle extras) {
        String text = "";
        if (Utility.isSet(extras.getString("sub_text"))) {
            text = Utility.stripHtml(Utility.decodeURI(extras
                    .getString("sub_text")));
        }
        return text;
    }

    private static String getContentTitle(Bundle extras) {
        String title = "New message";
        if (Utility.isSet(extras.getString("title_text"))) {
            title = Utility.decodeURI(extras.getString("title_text"));
        }
        return title;
    }

    private static String getTickerText(Bundle extras) {
        String tickerText = "You have a new message";
        if (Utility.isSet(extras.getString("ticker_text"))) {
            tickerText = Utility.decodeURI(extras.getString("ticker_text"));
        }
        return tickerText;
    }

    private static int getSmallIcon() {
        int smallIcon = R.drawable.tm_logo_icon;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            smallIcon = R.drawable.tm_icon_lollipop;
        }
        return smallIcon;
    }

    private static Bundle stripExtrasForHideConversations(Bundle extras, Context c) {
        String userId = Utility.getMyId(c);
        //Hide the notification content if
        //1. Passcode protection feature is available and
        //2. Chats are currently in hidden state and
        //3. If PUSH_TYPE is MESSAGE or PUSH_TYPE is MESSAGE
        if (RFHandler.getBool(c, userId,
                ConstantsRF.RIGID_FIELD_IS_PASSCODE_PROTECTION_ENABLED) &&
                RFHandler.getBool(c, userId,
                        ConstantsRF.RIGID_FIELD_CONVERSATIONS_HIDDEN) &&
                (Utility.stringCompare(extras.getString("push_type"),
                        NotificationType.MESSAGE.toString()) || Utility.stringCompare(extras.getString("push_type"),
                        NotificationType.MATCH.toString()))) {

            //Hide the conversation details if chats are in hidden mode right now
            extras.putString("title_text", c.getResources().getString(R.string.app_name));
            extras.putString("content_text", c.getResources().getString(R.string.new_notification));
            extras.putString("ticker_text", c.getResources().getString(R.string.new_notification));
            extras.remove("pic_url");
        }
        return extras;
    }

    private static int getNotificationPrefs(Context ctx, boolean forceTotalSilence) {
        int notificationPref = Notification.DEFAULT_ALL;
        if (forceTotalSilence || !SPHandler.getBool(ctx,
                Constants.SOUND_OPTION, true)) {
            notificationPref = notificationPref ^ Notification.DEFAULT_SOUND;
        }
        if (forceTotalSilence || !SPHandler.getBool(ctx,
                Constants.VIBRATION_OPTION, true)) {
            notificationPref = notificationPref ^ Notification.DEFAULT_VIBRATE;
        }
        return notificationPref;
    }

    public static int getRequestCode(Bundle extras) {
        int requestCode = 0;
        try {
            String matchId = extras.getString("match_id", "0");
            requestCode = matchId.equalsIgnoreCase("admin") ? 1 : Integer
                    .parseInt(matchId);
        } catch (NumberFormatException e) {
            // number format for admin
            requestCode = 1;
        }
        return requestCode;
    }

    public static void createMultiNotification(Context aContext, int pos, final ArrayList<NotificationModal> notificationListArray, boolean forceTotalSilence, boolean toTrack) {
        Bundle notificationBundle = getLocalNotifcationBundle(notificationListArray.get(pos));
        notificationBundle.putInt("NOTIFICATION_LIST_POSITION", pos);
        notificationBundle.putParcelableArrayList("NOTIFICATION_LIST_ARRAY", notificationListArray);
        sendNotification(notificationBundle, aContext, forceTotalSilence, notificationListArray.size() > 1, toTrack);
    }

    public static void startMatchesMultiLocation(Context aContext, Bundle extras) {
        String push_type = extras.getString("push_type");
        String collapseKey = NotificationHandler.COLLAPSE_KEY_PREFIX + NotificationType.MATCHES_IMAGE_MULTI.toString();
        ArrayList<NotificationModal> matchesImageMultiNotificationList = new ArrayList<>();
        JSONObject nJson;
        NotificationModal nModal = null;
        if (NotificationType.MATCHES_IMAGE_MULTI.toString().equals(push_type)) {
            int startIndex = 1;
            while (Utility.isSet(extras.getString(startIndex + ""))) {
                String jsonString = extras.getString(startIndex + "");
                try {
                    nJson = new JSONObject(jsonString);
                    nModal = new NotificationModal(NotificationType.MATCHES_IMAGE_MULTI, nJson.optString("title_text"), nJson.optString("content_text"), nJson.optString("ticker_text"), collapseKey, nJson.optString("match_id"), false, nJson.optString("pic_url"), nJson.optString("sub_text"));
                    Crashlytics.log(Log.DEBUG, "GCM", nModal.getPic_url());
                    try {
                        if (startIndex == 1) {
                            Picasso.with(aContext).load(nModal.getPic_url()).get();
                        }
                        Picasso.with(aContext).load(nModal.getPic_url()).fetch(new Callback() {
                            @Override
                            public void onSuccess() {
                                Crashlytics.log(Log.DEBUG, "GCM", "fetch success");
                            }

                            @Override
                            public void onError() {
                                Crashlytics.log(Log.DEBUG, "GCM", "fetch fail");

                            }
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                        Crashlytics.log(Log.ERROR, "GCM", nModal != null ? nModal.getPic_url() : "null nModal");
                        Crashlytics.logException(e);
                    }
                    matchesImageMultiNotificationList.add(nModal);
                    Crashlytics.log(Log.DEBUG, "GCM", nJson.toString());
                } catch (JSONException e) {
                    Crashlytics.log(Log.ERROR, "GCM", jsonString);
                    Crashlytics.logException(e);
                    break;
                }
                startIndex++;
            }
            if (matchesImageMultiNotificationList.size() >= 1) {
                createMultiNotification(aContext, 0, matchesImageMultiNotificationList, false, true);
            }
        }
    }

    //Dummy method for testing carousel notification
    public static void startMatchesMultiLocation(final Context aContext) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
                final String u1 = "https://djy1s2eqovnmt.cloudfront.net/files/images/profiles/1411126830661.4_680465618614107392.jpg";
                final String u2 = "https://djy1s2eqovnmt.cloudfront.net/files/images/profiles/1452918900497.4_911394751165062144.jpg";
                final String u3 = "https://djy1s2eqovnmt.cloudfront.net/files/images/profiles/1447125759452.8_793681344948708992.jpg";
                final ArrayList<NotificationModal> matchesImageMultiNotificationList = new ArrayList<>();
                matchesImageMultiNotificationList.add(new NotificationModal(NotificationType.MATCHES_IMAGE_MULTI, "Title1", "Content1", "Ticker1", "CollapseKey", "admin", true, u1, "SubText1"));
                matchesImageMultiNotificationList.add(new NotificationModal(NotificationType.MATCHES_IMAGE_MULTI, "Title2", "Content2", "Ticker2", "CollapseKey", "admin", true, u2, "SubText2"));
                matchesImageMultiNotificationList.add(new NotificationModal(NotificationType.MATCHES_IMAGE_MULTI, "Title3", "Content3", "Ticker3", "CollapseKey", "admin", true, u3, "SubText3"));
                for (NotificationModal nModal :
                        matchesImageMultiNotificationList) {
                    try {
                        Picasso.with(aContext).load(nModal.getPic_url()).get();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                createMultiNotification(aContext, 0, matchesImageMultiNotificationList, false, true);
            }
        }).start();
    }

    public static void clearAllNotifications(Context aContext) {
        NotificationManager mNotificationManager = (NotificationManager) aContext
                .getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancelAll();
    }

    public static void clearAllNonMessageNotifications(Context aContext) {
        NotificationManager mNotificationManager = (NotificationManager) aContext
                .getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancel(NOTIFICATION_ID);
    }

    public static void clearAllNotificationByCollapseKey(Context aContext,
                                                         String collapseKeySuffix) {
        String collapseKey = NotificationHandler.COLLAPSE_KEY_PREFIX + collapseKeySuffix;
        NotificationManager mNotificationManager = (NotificationManager) aContext
                .getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancel(collapseKey,
                NOTIFICATION_ID);
    }

    public static void vibrateOnChatMessageReceived(Context aContext) {
        Vibrator vibrator;
        vibrator = (Vibrator) aContext.getSystemService(Context.VIBRATOR_SERVICE);
        //vibrator.vibrate(300);
        long pattern[] = {0, 100, 50, 75};
        vibrator.vibrate(pattern, -1);
    }

    public static void soundOnChatMessageReceived(Context aContext) {
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(aContext.getApplicationContext(), notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
