package com.trulymadly.android.app.utility;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.ViewConfiguration;
import android.view.WindowManager;

import com.crashlytics.android.Crashlytics;
import com.facebook.login.LoginManager;
import com.google.common.base.CaseFormat;
import com.moe.pushlibrary.MoEHelper;
import com.moengage.push.PushManager;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.trulymadly.android.Exception.DbIsNullException;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventStatus;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.BuildConfig;
import com.trulymadly.android.app.GetOkHttpRequestParams;
import com.trulymadly.android.app.HttpTasks;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.TrulyMadlyApplication;
import com.trulymadly.android.app.TrulyMadlyService;
import com.trulymadly.android.app.asynctasks.ParseConversationListFromServerAsyncTask;
import com.trulymadly.android.app.bus.BusProvider;
import com.trulymadly.android.app.custom.ResponseFailureException;
import com.trulymadly.android.app.fragments.PasscodeFragment;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.Constants.AlarmRequestStatus;
import com.trulymadly.android.app.json.Constants.HttpRequestType;
import com.trulymadly.android.app.json.ConstantsRF;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.json.QuizParser;
import com.trulymadly.android.app.json.StickerParser;
import com.trulymadly.android.app.listener.ConnectFBviaTrustBuilderCallbackInterface;
import com.trulymadly.android.app.listener.CustomOkHttpResponseHandler;
import com.trulymadly.android.app.listener.OnConversationListFetchedInterface;
import com.trulymadly.android.app.listener.ShowGalleryInterface;
import com.trulymadly.android.app.listener.onQuizApiCompleteInterface;
import com.trulymadly.android.app.modal.StickerData;
import com.trulymadly.android.app.modal.TrustBuilderParamsFacebook;
import com.trulymadly.android.app.receivers.PasscodeAlarmReceiver;
import com.trulymadly.android.app.receivers.ServiceAlarmReceiver;
import com.trulymadly.android.app.receivers.SparkExpiredAlarmReceiver;
import com.trulymadly.android.app.sqlite.QuizDBHandler;
import com.trulymadly.android.app.sqlite.RFHandler;
import com.trulymadly.android.app.sqlite.StickerDBHandler;
import com.trulymadly.android.app.sqlite.TrulyMadlySQLiteHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.net.SocketTimeoutException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utility {

    public static String packageVersionCode, packageVersionName, androidId;
    // private static int DOUBLE_BACK_PRESSED_EXIT_DURATION = 2000; // in
    // millisec
    private static String myId = null;

    public static void disableScreenShot(Activity aActivity) {
        if (Constants.isLive && !BuildConfig.DEBUG) {
            aActivity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                    WindowManager.LayoutParams.FLAG_SECURE);
        }
    }

    public static void enableScreenShot(Activity aActivity) {
        if (Constants.isLive) {
            aActivity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_SECURE);
        }
    }

    public static boolean checkEmail(String email) {
        if (email == null)
            return false;
        String expression = "^[\\w\\.\\+-]+@([\\w\\-]+\\.)+[a-zA-Z]{2,4}$";
        // Make the comparison case-insensitive.
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    /**
     * Check if network is available.
     *
     * @return Boolean whether network is available.
     */
    public static boolean isNetworkAvailable(Context aContext) {
        ConnectivityManager connectivity = (ConnectivityManager) aContext
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        try {
            if (connectivity != null && connectivity.getActiveNetworkInfo() != null)
                return true;
        } catch (SecurityException | NullPointerException ignored) {
        }
        return false;
    }

    public static void logoutSession(Context aContext) {
        MoEHelper.getInstance(aContext).logoutUser();
        ProgressDialog mProgressDialog = null;
        mProgressDialog = UiUtils.showProgressBar(aContext, mProgressDialog);
        LoginManager.getInstance().logOut();
        HttpTasks.logout(aContext, mProgressDialog);

    }

    /**
     * For JSON Object check
     *
     * @param val which is to be checked
     * @return
     */
    public static boolean isSet(String val) {
        return !(val == null || val.trim().equals("") || val.trim().equals("null"));
    }

    public static String toCamelCase(String input) {
        return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, input.replaceAll(" ", " _"));
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    public static String getAppVersionCode(Context context) {
        if (isSet(packageVersionCode)) {
            return packageVersionCode;
        }
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            packageVersionCode = packageInfo.versionCode + "";
            return packageVersionCode;
        } catch (NameNotFoundException | RuntimeException ignored) {
            return "";
        }
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    public static String getAppVersionName(Context context) {
        if (isSet(packageVersionName)) {
            return packageVersionName;
        }
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            packageVersionName = packageInfo.versionName + "";
            return packageVersionName;
        } catch (NameNotFoundException | RuntimeException ignored) {
            return "";
        }
    }

    @SuppressLint("PackageManagerGetSignatures")
    public static void logHashKeyForSystem(Context context) {
        try {
            PackageInfo info = context.getPackageManager()
                    .getPackageInfo("com.trulymadly.android.app",
                            PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Crashlytics.log(
                        Log.DEBUG,
                        "Hash Key:",
                        "Hash Key "
                                + Base64.encodeToString(md.digest(),
                                Base64.DEFAULT));

            }
        } catch (NameNotFoundException | NoSuchAlgorithmException e) {
            Crashlytics.logException(e);
        }
    }

    public static String encodeURI(String uri) {
        if (!Utility.isSet(uri)) {
            return "";
        }
        String encodedURI = null;
        try {
            encodedURI = URLEncoder.encode(uri, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            Crashlytics.logException(e);
        }
        return encodedURI;
    }

    public static String decodeURI(String uri) {
        String decodedURI = null;
        if (isSet(uri)) {
            try {
                uri = uri.replaceAll("%(?![0-9a-fA-F]{2})", "%25").replaceAll(
                        "\\+", "%2B");
                decodedURI = URLDecoder.decode(uri, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                Crashlytics.logException(e);
            }
        }
        return decodedURI;
    }

    public static void connectFBviaTrustBuilder(
            final String access_token,
            final ConnectFBviaTrustBuilderCallbackInterface connectFBviaTrustBuilderCallbackInterface,
            String connectedFrom, Boolean fbReimport,
            final String eventActivity, final Context aContext, final boolean isFBPhotoDeclined) {
        CustomOkHttpResponseHandler responseHandler = new CustomOkHttpResponseHandler(
                aContext) {

            @Override
            public void onRequestSuccess(JSONObject jsonFBTrustBuilderResponse) {
                String status = jsonFBTrustBuilderResponse.optString("status");
                String error = jsonFBTrustBuilderResponse.optString("error");
                JSONObject diff = jsonFBTrustBuilderResponse
                        .optJSONObject("diff");

                if (status.equalsIgnoreCase("SUCCESS")) {
                    TrulyMadlyTrackEvent.trackEvent(aContext, eventActivity,
                            TrulyMadlyEventTypes.fb_server_call, 0,
                            TrulyMadlyEventStatus.success, null, true);
                    connectFBviaTrustBuilderCallbackInterface
                            .onSuccess(access_token, jsonFBTrustBuilderResponse
                                    .optString("connections"), isFBPhotoDeclined);
                    SPHandler.setString(aContext,
                            ConstantsSP.SHARED_KEYS_FB_CONNECTED, "Y");

                } else if (status.equalsIgnoreCase("FAIL")) {
                    TrulyMadlyTrackEvent.trackEvent(aContext, eventActivity,
                            TrulyMadlyEventTypes.fb_server_call, 0, "fail : "
                                    + error, null, true);
                    connectFBviaTrustBuilderCallbackInterface.onFail(error);
                } else if (status.equalsIgnoreCase("mismatch")) {
                    TrulyMadlyTrackEvent.trackEvent(aContext, eventActivity,
                            TrulyMadlyEventTypes.fb_server_call, 0,
                            "mismatch : " + error, null, true);
                    connectFBviaTrustBuilderCallbackInterface.onMismatch(error,
                            diff, isFBPhotoDeclined);

                } else {
                    TrulyMadlyTrackEvent
                            .trackEvent(
                                    aContext,
                                    eventActivity,
                                    TrulyMadlyEventTypes.fb_server_call,
                                    0,
                                    "fail : "
                                            + "Cannot connect to facebook with status - "
                                            + status, null, true);
                    connectFBviaTrustBuilderCallbackInterface
                            .onFail(R.string.cannot_connect_to_facebook);
                }

            }

            @Override
            public void onRequestFailure(Exception exception) {
                connectFBviaTrustBuilderCallbackInterface
                        .onFail(R.string.ERROR_NETWORK_FAILURE);

            }

        };

        OkHttpHandler.httpPost(aContext, ConstantsUrls.get_trust_builder_api_url(),
                GetOkHttpRequestParams.getHttpRequestParams(
                        HttpRequestType.FB_TRUSTBUILDER,
                        new TrustBuilderParamsFacebook(connectedFrom,
                                access_token, fbReimport)), responseHandler);

    }

    public static String stripHtml(String text) {
        String return_msg = text;
        if (isSet(return_msg)) {
            return_msg = return_msg.replace("<br />", "");
            return_msg = return_msg.replace("\n", "<br>");
            return_msg = Html.fromHtml(return_msg).toString();
        }
        return return_msg;
    }

    public static void minimizeApp(Activity aActivity) {
        try {
            aActivity.moveTaskToBack(true);
        } catch (NullPointerException ignored) {
        }
    }




	/* Basic Data section */

    public static void showOptionsMenuByForce(Context context) {
        /*
         * HACK TO SHOW MENU ON DEVICES WHICH HAVE PHYSICAL MENU BUTTON
		 */
        try {
            ViewConfiguration config = ViewConfiguration.get(context);
            Field menuKeyField = ViewConfiguration.class
                    .getDeclaredField("sHasPermanentMenuKey");

            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception ignored) {
        }

    }

    public static void sendRegistrationIdToBackend(final String regid,
                                                   CustomOkHttpResponseHandler responseHandler,
                                                   final Context aContext, String status) {

        if (responseHandler == null) {
            responseHandler = new CustomOkHttpResponseHandler(aContext) {

                @Override
                public void onRequestSuccess(JSONObject response) {
                    Utility.storeRegistrationId(regid, aContext);
                }

                @Override
                public void onRequestFailure(Exception exception) {
                }
            };
        }

        Map<String, String> params = GetOkHttpRequestParams
                .getHttpRequestParams(HttpRequestType.SEND_REG_ID, regid);
        params.put("device_id", getDeviceId(aContext));
        params.put("status", status);
        String referrer = SPHandler.getString(aContext,
                ConstantsSP.SHARED_KEYS_INSTALL_REFERRER);
        if (Utility.isSet(referrer)) {
            params.put("referrer", referrer);
            params.put("source", referrer);
        }
        responseHandler.setForceSuccess(true);
        OkHttpHandler.httpGet(aContext, ConstantsUrls.get_gcm_url(), params,
                responseHandler);

        (new Thread(new Runnable() {

            @Override
            public void run() {
                PushManager.getInstance().refreshToken(aContext.getApplicationContext(), regid);
            }
        })).start();

    }

    public static String getDeviceId(Context context) {
        if (isSet(androidId)) {
            return androidId;
        }
        androidId = android.provider.Settings.Secure.getString(
                context.getContentResolver(),
                android.provider.Settings.Secure.ANDROID_ID);
        return androidId;

    }

    @SuppressWarnings("SameReturnValue")
    public static String getOsVersion() {
        return android.os.Build.VERSION.RELEASE;
    }

    /**
     * Stores the registration ID and app versionCode in the application's
     * {@code SharedPreferences}.
     *
     * @param aContext application's context.
     * @param regId    registration ID
     */
    public static void storeRegistrationId(String regId, Context aContext) {
        String appVersion = Utility.getAppVersionCode(aContext);
        SPHandler.setString(aContext,
                ConstantsSP.SHARED_KEYS_APP_VERSION, appVersion);
        SPHandler.setString(aContext, ConstantsSP.SHARED_KEYS_REG_ID,
                regId);
        SPHandler.setString(aContext,
                ConstantsSP.SHARED_KEYS_REG_ID_TIMESTAMP, (new Date()).getTime()
                        + "");
    }

    @SuppressWarnings("SameReturnValue")
    private static Integer getOsVersionCode() {
        return android.os.Build.VERSION.SDK_INT;
    }

    public static boolean isWebPSupported() {
        return android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR2;
    }

    @SuppressWarnings("SameReturnValue")
    public static String getDeviceName() {
        return android.os.Build.MODEL;
    }

    public static JSONObject getDeviceDetails(Context aContext) {
        JSONObject deviceDetails = new JSONObject();
        try {
            deviceDetails.put("device_id", getDeviceId(aContext));
            deviceDetails.put("device_name", getDeviceName());
            deviceDetails.put("app_version_code", getAppVersionCode(aContext));
            deviceDetails.put("app_version_name", getAppVersionName(aContext));
            deviceDetails.put("android_version_name", getOsVersion());
            deviceDetails.put("android_version_code", getOsVersionCode()
                    .toString());
        } catch (JSONException ignored) {
        }
        return deviceDetails;
    }

    public static boolean isUserLoggedIn(Context aContext) {
        return isSet(OkHttpHandler.getCookieByName(aContext, "PHPSESSID"));
    }

    public static boolean isRootActivity(Activity a) {
        return a.isTaskRoot();
    }

    public static boolean launchDeepLinkInWebview(Context aContext, String url) {
        try {
            if (containsDeepLink(url)) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse(url));
                browserIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                aContext.startActivity(browserIntent);
                return true;
            } else if (containsMarketLink(url) && (ActivityHandler.appInstalledOrNot(aContext, Constants.packageGoogleMarket)
                    || ActivityHandler.appInstalledOrNot(aContext, Constants.packageGooglePlay))) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse(url));
                browserIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                aContext.startActivity(browserIntent);
                return true;
            }
        } catch (ActivityNotFoundException activityNotFoundException) {
            return false;
        }
        return false;
    }

    private static boolean containsMarketLink(String url) {
        return Uri.parse(url).getScheme().equals("market");
    }

    private static boolean containsDeepLink(String url) {
        String scheme = Uri.parse(url).getScheme();
        return !(scheme.equals("http") || scheme.equals("https") || containsMarketLink(url));
    }

    public static void openWebsite(Context aContext, String url) {
        if (!url.startsWith("http://") && !url.startsWith("https://")
                && !url.startsWith("market://") && !url.startsWith("trulymadly://")) {
            url = "http://" + url;
        }
        try {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse(url));
            ((Activity) aContext).startActivityForResult(browserIntent,
                    Constants.OPEN_WEB_LINK);
        } catch (ActivityNotFoundException | SecurityException e) {
            Crashlytics.logException(e);
        }
    }

    public static boolean isValidJsonObject(String response) {
        boolean isValid = false;
        try {
            new JSONObject(response);
            isValid = true;
        } catch (JSONException ignored) {
            isValid = false;
        }
        return isValid;
    }

    public static boolean isValidJsonArray(String response) {
        boolean isValid = false;
        try {
            new JSONArray(response);
            isValid = true;
        } catch (JSONException ignored) {
            isValid = false;
        }
        return isValid;
    }

    public static boolean stringCompare(String a, String b) {
        return isSet(a) && isSet(b) && a.equalsIgnoreCase(b);
    }

    public static String getNetworkClass(Context aContext) {
        String networkClassString = getNetworkClassString(aContext);
        String lastNetworkClass = SPHandler.getString(aContext,
                ConstantsSP.SHARED_KEYS_LAST_NETWORK_CLASS);
        SPHandler.setString(aContext,
                ConstantsSP.SHARED_KEYS_LAST_NETWORK_CLASS, networkClassString);
        if (isSet(lastNetworkClass) && !lastNetworkClass.equals("NONE")
                && !lastNetworkClass.equals(networkClassString)) {
            // SocketHandler.getInstance(aContext).connectionEnd(SOCKET_END.network_class_change);
            SPHandler.remove(aContext,
                    ConstantsSP.SHARED_KEYS_LAST_TSTAMP_SOCKET_SWITCHED_TO_PHP_POLLING);
        }
        return networkClassString;
    }

    private static String getNetworkClassString(Context aContext) {
        if (!isNetworkAvailable(aContext)) {
            return "NONE";
        }

        ConnectivityManager connectivity = (ConnectivityManager) aContext
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = null;
        if (connectivity != null) {
            networkInfo = connectivity.getActiveNetworkInfo();
        }
        if (networkInfo != null) {
            switch (networkInfo.getType()) {
                case ConnectivityManager.TYPE_WIFI:
                case ConnectivityManager.TYPE_WIMAX:
                case ConnectivityManager.TYPE_ETHERNET:
                case ConnectivityManager.TYPE_DUMMY:
                case ConnectivityManager.TYPE_BLUETOOTH:
                case ConnectivityManager.TYPE_VPN:
                    return "WIFI";
                case ConnectivityManager.TYPE_MOBILE:
                case ConnectivityManager.TYPE_MOBILE_DUN:
                case ConnectivityManager.TYPE_MOBILE_HIPRI:
                case ConnectivityManager.TYPE_MOBILE_MMS:
                case ConnectivityManager.TYPE_MOBILE_SUPL:
                    break;
                default:
                    return "UNKNOWN";
            }
        }

        TelephonyManager mTelephonyManager = (TelephonyManager) aContext
                .getSystemService(Context.TELEPHONY_SERVICE);
        try {
            int networkType = mTelephonyManager.getNetworkType();
            switch (networkType) {
                case TelephonyManager.NETWORK_TYPE_GPRS:
                case TelephonyManager.NETWORK_TYPE_EDGE:
                case TelephonyManager.NETWORK_TYPE_CDMA:
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                case TelephonyManager.NETWORK_TYPE_IDEN:
                    return "2G";
                case TelephonyManager.NETWORK_TYPE_UMTS:
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                case TelephonyManager.NETWORK_TYPE_HSPA:
                case TelephonyManager.NETWORK_TYPE_EVDO_B:
                case TelephonyManager.NETWORK_TYPE_EHRPD:
                case TelephonyManager.NETWORK_TYPE_HSPAP:
                    return "3G";
                case TelephonyManager.NETWORK_TYPE_LTE:
                    return "4G";
                default:
                    return "UNKNOWN";
            }
        } catch (OutOfMemoryError ignored) {
            return "UNKNOWN";
        }

    }

    public static String generateUniqueRandomId() {
        return generateUniqueRandomId(null);
    }

    public static String generateUniqueRandomId(String matchId) {
        Calendar lCDateTime = Calendar.getInstance();
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 20; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        String returnVal = "";
        if (matchId != null) {
            returnVal = matchId + "_";
        }
        return returnVal + lCDateTime.getTimeInMillis() + "_" + sb.toString();
    }

    public static String generateRandomFileName(String userId, String matchId) {
        Calendar lCDateTime = Calendar.getInstance();
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        String returnVal = "";
        if (matchId != null) {
            returnVal = userId + "_" + matchId + "_";
        }
        return returnVal + lCDateTime.getTimeInMillis() + "_" + sb.toString();
    }

    /**
     * Start the service and fire the event
     */
    public static void fireServiceBusEvent(Context aContext, Object event) {
        fireServiceBusEvent(aContext, event, 0);
    }

    public static void fireServiceBusEvent(Context aContext, Object event, int delayMilliSeconds) {
        if (!TrulyMadlyService.isInstanceCreated()) {
            TrulyMadlyApplication.createService(aContext);
        }
        fireBusEventWithDelay(aContext, true, event, delayMilliSeconds);
    }

    public static void fireBusEvent(Context aContext, boolean toFire,
                                    Object event) {
        fireBusEventWithDelay(aContext, toFire, event, 0);
    }

    private static void fireBusEventWithDelay(Context aContext, boolean toFire,
                                              final Object event, int delayMilliSeconds) {
        if (toFire) {
            new Handler(aContext.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (BusProvider.getInstance() != null) {
                        try {
                            BusProvider.getInstance().post(event);
                        } catch (RuntimeException ignored) {
                        }
                    }
                }
            }, delayMilliSeconds);
        }

    }

    public static void sendInstallReferrerRequestToServer(String referrer,
                                                          Context ctx) {
        if (!isSet(SPHandler.getString(ctx,
                ConstantsSP.SHARED_KEYS_INSTALL_REFERRER_REQUEST_SENT))
                && isSet(referrer)) {
            SPHandler.setString(ctx,
                    ConstantsSP.SHARED_KEYS_INSTALL_REFERRER, referrer);
            Map<String, String> event_info = null;
            event_info = new HashMap<>();
            event_info.put("referrer", referrer);
            TrulyMadlyTrackEvent.trackEvent(ctx, TrulyMadlyActivities.install,
                    TrulyMadlyEventTypes.install_referrer, 0, null, event_info, true);
            SPHandler.setString(ctx,
                    ConstantsSP.SHARED_KEYS_INSTALL_REFERRER_REQUEST_SENT, "yes");
        }
    }

//    public static void sendReferrerPostBackToCrobo(final Context ctx) {
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                String referrer = SPHandler.getString(ctx,
//                        ConstantsSP.SHARED_KEYS_INSTALL_REFERRER);
//                if (Utility.isSet(referrer)) {
//                    sendReferrerPostBackToCrobo(referrer, ctx);
//                }
//            }
//        }).start();
//    }

//    private static void sendReferrerPostBackToCrobo(String referrer, Context ctx) {
//        if (!isSet(SPHandler.getString(ctx,
//                ConstantsSP.SHARED_KEYS_INSTALL_REFERRER_REQUEST_SENT_TO_CROBO))
//                && isSet(referrer)) {
//            JSONObject referrerParams = convertUrlParamsToJson(referrer);
//            if (referrerParams != null && Utility.stringCompare("crobo", referrerParams.optString("utm_source"))) {
//                //utm_source%3Dcrobo%26utm_medium%3D{transaction_id}%26utm_term%3D{affiliate_id}
//                String transactionId = referrerParams.optString("utm_medium", "");
//                String affiliateId = referrerParams.optString("utm_term", "");
//                if (Utility.isSet(transactionId)) {
//                    try {
//                        //http://tracking.crobo.com/aff_lsr?transaction_id=TRANSACTION_ID_MACRO_FROM_ADVERTISER
//                        String url = Constants.crobo_postback_url + "?transaction_id=" + transactionId;
//                        Request request = new Request.Builder().url(url).get().build();
//                        Response response = new OkHttpClient().newCall(request).execute();
//                        if (OkHttpHandler.isValidResponse(response)) {
//                            SPHandler.setString(ctx, ConstantsSP.SHARED_KEYS_INSTALL_REFERRER_REQUEST_SENT_TO_CROBO, "yes");
//                            TrulyMadlyTrackEvent.trackEvent(ctx, TrulyMadlyActivities.install,
//                                    TrulyMadlyEventTypes.crobo_postback, 0, null, null, true);
//                        }
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//        }
//    }

    public static JSONObject convertUrlParamsToJson(String referrer) {
        return convertUrlParamsToJson(referrer, "=");
    }

    public static JSONObject convertUrlParamsToJson(String referrer, String keyValueDelimeter) {
        JSONObject referrerParams = new JSONObject();
        if (Utility.isSet(referrer)) {
            try {
                String[] referrerParamsStrings = URLDecoder.decode(referrer, "UTF-8").split("&");
                if (referrerParamsStrings != null && referrerParamsStrings.length > 0) {
                    for (String referrerParamsString : referrerParamsStrings) {
                        String[] referrerParamString = referrerParamsString.split(keyValueDelimeter);
                        if (referrerParamString != null && referrerParamString.length == 2) {
                            referrerParams.put(referrerParamString[0], referrerParamString[1]);
                        }
                    }
                }
            } catch (UnsupportedEncodingException | JSONException e) {
                e.printStackTrace();
            }
        }
        //TODO: Convert this to referrer modal in future instead of JSONObject to handle the utm_* values
        return referrerParams;

    }

    public static boolean isMale(Context context) {
        return Utility.stringCompare("m", SPHandler.getString(context, ConstantsSP.SHARED_KEYS_USER_GENDER));
    }

    private static void setNewGalleryFlag(Context aContext, JSONArray galleries, HashMap<Integer, Integer> existingGalleries) {


        String oppositeGender = "m";
        if (Utility.stringCompare(oppositeGender, SPHandler.getString(aContext, ConstantsSP.SHARED_KEYS_USER_GENDER)))
            oppositeGender = "f";

        for (int i = 0; i < galleries.length(); i++) {
            JSONObject gallery = galleries.optJSONObject(i);
            // new gallery

            if (gallery != null && gallery.optString("gender").equalsIgnoreCase(oppositeGender))
                continue;

            if (gallery != null && (existingGalleries.get(gallery.optInt("id")) == null || existingGalleries.get(gallery.optInt("id")) == 0)) {
                SPHandler.setBool(aContext, ConstantsSP.SHARED_KEYS_NEW_GALLERY_ICON, true);
                return;
            }
        }

        SPHandler.setBool(aContext, ConstantsSP.SHARED_KEYS_NEW_GALLERY_ICON, false);
    }

    public static void callStickerApi(final Context aContext, final ShowGalleryInterface showGalleryInterface) {

        CustomOkHttpResponseHandler stickerHttpResponseHandler = new CustomOkHttpResponseHandler(
                aContext, TrulyMadlyActivities.sticker,
                TrulyMadlyEventTypes.api_call) {

            @Override
            public void onRequestSuccess(JSONObject responseJSON) {


                if (isValidStickerResponse(responseJSON)) {

                    HashMap<Integer, Integer> existingGalleries = StickerDBHandler.returnExistingGalleries(aContext);


                    if (existingGalleries != null) {
                        JSONArray galleries = responseJSON.optJSONArray("gallery");

                        if (galleries != null) {
                            setNewGalleryFlag(aContext, galleries, existingGalleries);
                        }
                    }

                    try {

                        ArrayList<StickerData> recentStickers = StickerDBHandler.createRecentUsedStickers(aContext);
                        StickerDBHandler.deleteTable(TrulyMadlySQLiteHandler.getDatabase(aContext), "stickers");
                        StickerParser sParser = new StickerParser(aContext);
                        sParser.insert(responseJSON);
                        StickerDBHandler.downloadAll(aContext);
                        HashMap<Integer, Integer> newStickers = StickerDBHandler.newStickerMap(aContext);
                        ArrayList<StickerData> newRecentStickers = new ArrayList<>();


                        //  we have new stickers n recentstickers before deleting the table
                        // remove the timestamp of old stickers

                        if (recentStickers != null && recentStickers.size() > 0) {
                            for (StickerData s : recentStickers) {
                                if (newStickers.get(s.getId()) == null || newStickers.get(s.getId()) == 0) {
                                } else
                                    newRecentStickers.add(s);
                            }
                            // update the timestamp of existing stickers
                            StickerDBHandler.updateLatestTimeStamp(newRecentStickers, aContext);
                        }
                        showGalleryInterface.showBlooper();
                    } catch (DbIsNullException e) {
                        Crashlytics.logException(e);
                    }

                }
            }

            @Override
            public void onRequestFailure(Exception exception) {

            }

        };
        OkHttpHandler.httpGet(aContext, ConstantsUrls.get_sticker_url(),
                stickerHttpResponseHandler);
    }

    public static void callQuizApi(final Context aContext) {
        callQuizApi(aContext, null);
    }

    private static boolean isValidStickerResponse(JSONObject response) {
        JSONArray galleries = response.optJSONArray("gallery");
        int noOfGalleries = 0;
        if (galleries != null) {
            noOfGalleries = galleries.length();
            if (noOfGalleries <= 2)
                return false;
            for (int i = 0; i < noOfGalleries; i++) {
                JSONObject gallery = galleries.optJSONObject(i);
                if (gallery != null) {
                    JSONArray stickers = gallery.optJSONArray("stickers");
                    if (stickers == null || stickers.length() < 5)
                        return false;
                } else
                    return false;
            }
            return true;
        }
        return false;
    }

    private static void callQuizApi(final Context aContext,
                                    final onQuizApiCompleteInterface onComplete) {
        CustomOkHttpResponseHandler quizCustomOkHttpResponseHandler = new CustomOkHttpResponseHandler(
                aContext, TrulyMadlyActivities.quiz,
                TrulyMadlyEventTypes.api_call) {

            @Override
            public void onRequestSuccess(JSONObject responseJSON) {
                int responseCode = responseJSON.optInt("responseCode");
                String currentVerison = responseJSON
                        .optString("current_version");
                String appQuizVersion = ""
                        + SPHandler.getInt(aContext,
                        ConstantsSP.SHARED_KEYS_QUIZ_VERSION);

                if (responseCode == 200
                        && (!appQuizVersion.equalsIgnoreCase(currentVerison))) {
                    SPHandler.setBool(aContext, ConstantsSP.SHARED_KEYS_NEW_QUIZ_ICON, true);
                    SPHandler.setInt(aContext, ConstantsSP.SHARED_KEYS_MAX_QUIZ_ID, QuizDBHandler.returnMaxId(aContext));
                    QuizParser qParser = new QuizParser(aContext);
                    qParser.insert(responseJSON);
                    if (onComplete != null) {
                        onComplete.onComplete();
                    }
                }
            }

            @Override
            public void onRequestFailure(Exception exception) {
            }
        };

        HashMap<String, String> params = new HashMap<>();
        params.put("action", "get_quiz");
        params.put(
                "version",
                SPHandler.getInt(aContext,
                        ConstantsSP.SHARED_KEYS_QUIZ_VERSION) + "");


        OkHttpHandler.httpGet(aContext, ConstantsUrls.get_quiz_url(), params,
                quizCustomOkHttpResponseHandler);

    }

    public static ArrayList<String> jsonArrayToStringArrayList(JSONArray jsonArray) {
        ArrayList<String> returnList = new ArrayList<>();
        if (jsonArray != null) {
            for (int i = 0; i < jsonArray.length(); i++) {
                if (isSet(jsonArray.optString(i))) {
                    returnList.add(jsonArray.optString(i));
                }
            }
        }
        return returnList;
    }

    public static JSONArray stringArrayListToJsonArray(ArrayList<String> arrayList) {
        JSONArray returnArray = new JSONArray();
        if (arrayList != null) {
            for (String str : arrayList) {
                if (isSet(str)) {
                    returnArray.put(str);
                }
            }
        }
        return returnArray;
    }

    //Checks if any passcode alarm is required. If yes, creates an alarm to send the notification
    //Called when the app launches from Matches activity
    public static void checkForPendingPasscodeAlarms(Context ctx, long timeout) {
        String userId = getMyId(ctx);
        String passcode = RFHandler.getString(ctx,
                userId,
                ConstantsRF.RIGID_FIELD_HIDE_CONVERSATIONS_PASSCODE);
        if (passcode == null)
            return;

        String rigid_key = ConstantsRF.RIGID_FIELD_CONVERSATIONS_PASSCODE_REQUESTED_TIMESTAMP;
        Long difference = TimeUtils.getTimeoutDifference(ctx, rigid_key, userId, timeout);
        if (difference == null) {

        } else if (difference != 0) {
            generatePasscodeAlarm(ctx, passcode, difference);
        }
    }

    //Generates an alarm for passcode (when user forgets his/her conversation passcode)
    public static void generatePasscodeAlarm(Context ctx, String passcode, long delay) {
        RFHandler.insert(ctx, getMyId(ctx), ConstantsRF.RIGID_FIELD_CONVERSATIONS_PASSCODE_REQUESTED_TIMESTAMP,
                String.valueOf(System.currentTimeMillis()));
        PendingIntent alarmIntent;
        Intent intent = new Intent(ctx, PasscodeAlarmReceiver.class);
        intent.putExtra(PasscodeFragment.PASSCODE_PARAM, passcode);
        alarmIntent = PendingIntent.getBroadcast(ctx, 0, intent, 0);
        generateAlarm(ctx, alarmIntent, delay);
    }

    public static void generateSparkAlarm(Context ctx, String matchId, long delay) {
        PendingIntent alarmIntent;
        Intent intent = new Intent(ctx, SparkExpiredAlarmReceiver.class);
        intent.putExtra(SparkExpiredAlarmReceiver.PARAM_USER_ID, getMyId(ctx));
        intent.putExtra(SparkExpiredAlarmReceiver.PARAM_MATCH_ID, matchId);
        alarmIntent = PendingIntent.getBroadcast(ctx, 0, intent, 0);
        generateAlarm(ctx, alarmIntent, delay);
    }

    //Generates an alarm to kill the service
    public static void generateServiceAlarm(Context ctx, long delay) {
        PendingIntent alarmIntent;
        Intent intent = new Intent(ctx, ServiceAlarmReceiver.class);
        alarmIntent = PendingIntent.getBroadcast(ctx, 0, intent, 0);
        generateAlarm(ctx, alarmIntent, delay);
    }

    //Generates an alarm with the given delay and given pendingIntent
    public static void generateAlarm(Context ctx, PendingIntent pendingIntent, long delay) {
        AlarmManager alarmMgr = (AlarmManager) ctx.getSystemService(Context.ALARM_SERVICE);
        try {
            alarmMgr.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() +
                    delay, pendingIntent);
        } catch (SecurityException e) {
            //meizu mx5 issue
            //https://fabric.io/trulymadly/android/apps/com.trulymadly.android.app/issues/5671743df5d3a7f76b6797cc
        }
    }

    public static void sendDiscoveryOptions(final boolean isChecked, Context aContext,
                                            CustomOkHttpResponseHandler okHttpResponseHandler) {
        String is_discovery_on = (isChecked ? "0" : "1");
        if (Utility.isNetworkAvailable(aContext)) {
            // server_call
            Map<String, String> params = new HashMap<>();
            params.put("is_discovery_on", is_discovery_on);
            OkHttpHandler.httpGet(aContext, ConstantsUrls.get_matches_utils_url(),
                    params, okHttpResponseHandler);
        } else {
            okHttpResponseHandler.onRequestFailure(null);
        }
    }

    //Permissions
    //GET_ACCOUNTS
    /* fetch device's email */
    // http://stackoverflow.com/questions/2112965/how-to-get-the-android-devices-primary-e-mail-address
    public static String getUserEmail(Activity activity, boolean askForPermission) {
        if (askForPermission && !PermissionsHelper.checkAndAskForPermission(activity, Manifest.permission.GET_ACCOUNTS,
                PermissionsHelper.REQUEST_GET_ACCOUNTS_PERMISSIONS_CODE))
            return null;

        if (!askForPermission && !PermissionsHelper.hasPermission(activity, Manifest.permission.GET_ACCOUNTS))
            return null;

        Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
        Account[] accounts = AccountManager.get(activity).getAccounts();
        String possibleEmail = null;
        for (Account account : accounts) {
            if (emailPattern.matcher(account.name).matches()) {
                possibleEmail = account.name;
                break;
            }
        }

        return possibleEmail;
    }

    public static AlarmRequestStatus getAlarmRequestStatus(Context aContext,
                                                           String myId, String rigidFieldKey, int delayMiliSeconds) {
        long currentTime = System.currentTimeMillis();
        String alreadyRequestedTS = RFHandler
                .getString(aContext, myId, rigidFieldKey);
        if (!Utility.isSet(alreadyRequestedTS)) {
            return AlarmRequestStatus.NOT_SET;
        }

        long alreadyRequestedTSValue = Long.parseLong(alreadyRequestedTS);
        if (currentTime <= alreadyRequestedTSValue + delayMiliSeconds)
            return AlarmRequestStatus.IN_PROGRESS;
        else {

            return AlarmRequestStatus.EXPIRED;
        }
    }

    public static void prefetchPhoto(Context aContext, String url, final String trkActivity) {
        //final Context applicationContext = aContext.getApplicationContext();
        final WeakReference<Context> weakReferenceApplicationContext = new WeakReference<>(aContext.getApplicationContext());
        final long startTime = TimeUtils.getSystemTimeInMiliSeconds();

        Picasso.with(aContext).load(url).config(Bitmap.Config.RGB_565)
                .fetch(new Callback() {

                    @Override
                    public void onError() {
                        if (weakReferenceApplicationContext.get() != null) {
                            long timeTaken = TimeUtils.getSystemTimeInMiliSeconds() - startTime;
                            TrulyMadlyTrackEvent.trackEvent(weakReferenceApplicationContext.get(), trkActivity,
                                    TrulyMadlyEventTypes.fetching_images, timeTaken, null, null, true);
                        }

                    }

                    @Override
                    public void onSuccess() {
                        if (weakReferenceApplicationContext.get() != null) {
                            long timeTaken = TimeUtils.getSystemTimeInMiliSeconds() - startTime;
                            TrulyMadlyTrackEvent.trackEvent(weakReferenceApplicationContext.get(), trkActivity,
                                    TrulyMadlyEventTypes.fetching_images, timeTaken, null, null, true);
                        }
                    }

                });
    }

    public static void prefetchPhotos(final Context context, ArrayList<String> urls, String trkActivity) {
        for (String url : urls) {
            prefetchPhoto(context, url, trkActivity);
        }
    }

    public static void prefetchPhotos(final Context context, String[] urls, int startIndex, String trkActivity) {
        if (urls == null || urls.length < (startIndex + 1))
            return;

        for (int i = startIndex; i < urls.length; i++) {
            prefetchPhoto(context, urls[i], trkActivity);
        }
    }

    public static ServerCodes getNetworkErrorCode(Context aActivity, Exception e) {
        if (isNetworkFailed(aActivity, e)) {
            return ServerCodes.internet_error;
        } else {
            return ServerCodes.server_error;
        }
    }

    public static int getNetworkErrorStringResid(Context aContext, Exception e) {
        if (isNetworkFailed(aContext, e)) {
            return (R.string.ERROR_NETWORK_FAILURE);
        } else {
            return (R.string.SERVER_ERROR_LARGE);
        }
    }

    public static int getNetworkErrorUiStringResId(Context aActivity, Exception e) {
        if (isNetworkFailed(aActivity, e)) {
            return (R.string.network_problem);
        } else {
            return (R.string.SERVER_ERROR_SMALL);
        }
    }

    public static boolean isNetworkFailed(Context aContext, Exception e) {
        if (!Utility.isNetworkAvailable(aContext)) {
            return true;
        }
        // find out the timeout exception
        else if (e == null || e instanceof SocketTimeoutException) {
            return true;
        } else if (e instanceof JSONException) {
            return false;
        } else if (e instanceof ResponseFailureException) {
            return false;
        } else {
            return false;
        }
    }

    public static String getMyId(Context ctx) {
        if (myId == null) {
            myId = SPHandler.getString(ctx,
                    ConstantsSP.SHARED_KEYS_USER_ID);
        }
        return myId;
    }

    public static String getMyProfilePic(Context ctx) {
        return SPHandler.getString(ctx,
                ConstantsSP.SHARED_KEYS_USER_PROFILE_FULL_URL);
    }

    public static String getMyName(Context ctx) {
        return SPHandler.getString(ctx,
                ConstantsSP.SHARED_KEYS_USER_NAME);
    }

    public static void clearMyId(Context ctx) {
        myId = null;
        SPHandler.remove(ctx,
                ConstantsSP.SHARED_KEYS_USER_ID);
    }

    /**
     * @param context : Activity context
     * @param matchId : matchid for which CuratedDeals ID needs to be generated
     *                <p/>
     *                Generates a unique curated deal id by concatinating the following:
     *                UserId + MatchId + TimeStamp + RandomNumber
     *                "RandomNumber" used here is a 4 digit random number
     */
    public static String generateCuratedDealId(Context context, String matchId) {
        String userId = getMyId(context);
        String timestamp = String.valueOf(TimeUtils.getSystemTimeInMiliSeconds());
        String randomNumber = String.valueOf((new Random()).nextInt(4));
        return userId + matchId + timestamp + randomNumber;
    }

    public static boolean isValidPhoneNo(String phoneNo) {
        String pattern = "[1-9]{1}\\d{9}";
        return phoneNo.matches(Constants.phoneNoPattern);
    }

    public static Drawable getMaskDrawable(Context context, int maskId) {
        Drawable drawable;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            drawable = context.getDrawable(maskId);
        } else {
            drawable = context.getResources().getDrawable(maskId);
        }

        if (drawable == null) {
            throw new IllegalArgumentException("maskId is invalid");
        }

        return drawable;
    }

    public static int validateHashtagString(String input) {
        if (input == null || !isSet(input.trim())) {
            return 0;
        }
        input = input.trim();
        if (input.length() > 20) {
            return R.string.write_shorter_hashtag;
        }
        for (int i = 0; i < input.length(); i++) {
            if (i == 0 && input.charAt(0) == '#') {
                if (input.length() > 1) {
                    continue;
                } else {
                    return R.string.cant_use_special_chars;
                }
            }
            if (input.charAt(i) >= 'a' && input.charAt(i) <= 'z') {
            } else if (input.charAt(i) >= 'A' && input.charAt(i) <= 'Z') {
            } else {
                return R.string.cant_use_special_chars;
            }
        }
        return 0;
    }


    public static void getConversationListFromServer(final Context aContext, final OnConversationListFetchedInterface onConversationListFetchedInterface) {
        CustomOkHttpResponseHandler messageListOkHttpResponseHandler = new CustomOkHttpResponseHandler(aContext,
                TrulyMadlyActivities.conversation_list, TrulyMadlyEventTypes.page_load) {

            @Override
            public void onRequestSuccess(JSONObject response) {
                new ParseConversationListFromServerAsyncTask(aContext, onConversationListFetchedInterface).execute(response);
            }

            @Override
            public void onRequestFailure(Exception exception) {
                onConversationListFetchedInterface.onFail(exception);
            }

        };

        if (isNetworkAvailable(aContext)) {
            HashMap<String, String> params = new HashMap<>();
            params.put("isMessageMatchMergedList", "true");
            String lastRequestSentTstamp = SPHandler.getString(aContext,
                    ConstantsSP.SHARED_KEYS_CONVERSATION_LIST_API_CALL_TIME);
            if (isSet(lastRequestSentTstamp)) {
                params.put("tstamp", lastRequestSentTstamp);
            }

            if (!RFHandler.getBool(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_IS_MISS_TM_CREATED)) {
                params.put("create_miss_tm", "true");
            }

            // making post as tstamp will contain a space.
            OkHttpHandler.httpPost(aContext, ConstantsUrls.get_message_url(), params, messageListOkHttpResponseHandler);
        } else {
            onConversationListFetchedInterface.onFail(null);
            //failedNetworkRequest(showLoader, null);
        }
    }

    public static boolean handleDeepLinks(Activity aActivity, String dataString) {
        //HANDLING DEEP LINKS HERE
        if (Utility.isSet(dataString)
                && Utility.isSet(dataString.substring(dataString.lastIndexOf("/launch?") + 8))) {
            JSONObject launchParams = Utility.convertUrlParamsToJson(dataString.substring(dataString.lastIndexOf("/launch?") + 8));
            if (launchParams != null &&
                    launchParams.optString("action").equalsIgnoreCase("share_event")
                    && Utility.isSet(launchParams.optString("event_id"))
                    && Utility.isUserLoggedIn(aActivity)) {
                ActivityHandler.startEventProfileActivity(aActivity, launchParams.optString("event_id"), null, true);
                return true;
            }
            if (launchParams != null &&
                    launchParams.optString("action").equalsIgnoreCase("buy_sparks")
                    && Utility.isUserLoggedIn(aActivity)) {
                ActivityHandler.startMatchesActivity(aActivity, true, false);
                return true;
            }
            if (launchParams != null &&
                    launchParams.optString("action").equalsIgnoreCase("buy_select")
                    && Utility.isUserLoggedIn(aActivity)) {
                ActivityHandler.startTMSelectActivity(aActivity, "deep_link", false);
                return true;
            }
            if (launchParams != null &&
                    launchParams.optString("action").equalsIgnoreCase("launch_activity")
                    && Utility.isUserLoggedIn(aActivity)) {
                ActivityHandler.startActivityFromString(aActivity, false, launchParams.optString("class_name"), true);
                return true;
            }
        }
        return false;
    }

    public static File getPictureDirectory() {
        File storageDir = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

        if (!storageDir.exists() || !storageDir.isDirectory() || !storageDir.canWrite()) {
            storageDir = Environment
                    .getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        }
        if (!storageDir.exists() || !storageDir.isDirectory() || !storageDir.canWrite()) {
            storageDir = Environment
                    .getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        }
        return storageDir;
    }

    public static String getTempFileName() {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH)
                .format(Calendar.getInstance().getTime());
        return "JPEG_" + timeStamp + "_" + "file";
    }

    public static boolean isLiveRelease() {
        return Constants.isLive && !BuildConfig.DEBUG;
    }

    public static String getCountryNameFromId(int countryId) {
        switch (countryId) {
            case Constants.COUNTRY_ID_INDIA:
                return Constants.COUNTRY_NAME_INDIA;

            case Constants.COUNTRY_ID_US:
                return Constants.COUNTRY_NAME_US;
        }

        return Constants.COUNTRY_NAME_INDIA;
    }
}
