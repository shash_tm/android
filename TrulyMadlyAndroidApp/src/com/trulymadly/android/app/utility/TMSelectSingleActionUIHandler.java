package com.trulymadly.android.app.utility;

import android.content.Context;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.trulymadly.android.app.R;

import static butterknife.ButterKnife.findById;

/**
 * Created by avin on 03/11/16.
 */

public class TMSelectSingleActionUIHandler {
    private Context mContext;
    private View.OnClickListener mOnClickListener;
    private View mMasterView;

    private String mActionButtonText;
    private Button mActionButton;
    private View mSuccessScreen, mOkFAB, mProgressContainer, mMainScreen;
    private TextView mCongratsTV, mYouAreSelectMemberTV, mEnjoyExperienceTV;
    private TextView mLetUsHandpickTV, mCTASubtextTV;
    private TMSelectMainContentUIHandler mTMSelectMainContentUIHandler;

    public TMSelectSingleActionUIHandler(Context mContext, View mMasterView,
                                         String mActionButtonText,
                                         View.OnClickListener mOnClickListener, String actionCTASubText) {
        this.mContext = mContext;
        this.mOnClickListener = mOnClickListener;
        this.mActionButtonText = mActionButtonText;
        this.mMasterView = mMasterView;

        mMainScreen = findById(mMasterView, R.id.main_screen);
        mTMSelectMainContentUIHandler = new TMSelectMainContentUIHandler(mContext, mMainScreen);

        mSuccessScreen = findById(mMasterView, R.id.payment_success_screen);
        mCongratsTV = findById(mMasterView, R.id.congrats_tv);
        mLetUsHandpickTV = findById(mMasterView, R.id.let_us_handpick_tv);
//        mLetUsHandpickTV.setText(String.format(mContext.getString(R.string.let_us_handpick),
//                (Utility.isMale(mContext)) ? mContext.getString(R.string.women) : mContext.getString(R.string.men)));
        mYouAreSelectMemberTV = findById(mMasterView, R.id.you_are_tm_select_member_tv);
        mEnjoyExperienceTV = findById(mMasterView, R.id.enjoy_all_new_tm_tv);
        mOkFAB = findById(mMasterView, R.id.ok_fab);
        mOkFAB.setOnClickListener(mOnClickListener);

        mProgressContainer = findById(mMasterView, R.id.select_progress_container);
        mProgressContainer.setOnClickListener(mOnClickListener);

        mActionButton = findById(mMasterView, R.id.select_action_button);
        mActionButton.setText(mActionButtonText);
        mActionButton.setOnClickListener(mOnClickListener);

        mCTASubtextTV = findById(mMasterView, R.id.cta_subtext_tv);
        String ctaSubText = TMSelectHandler.getSelectProfileCtaSubText(mContext);
//        if (Utility.isSet(ctaSubText)) {
//            mCTASubtextTV.setText(ctaSubText);
        if (Utility.isSet(actionCTASubText)) {
            mCTASubtextTV.setText(actionCTASubText);
            mCTASubtextTV.setVisibility(View.VISIBLE);
        } else {
            mCTASubtextTV.setVisibility(View.GONE);
        }

        toggleMainScreen(true);
        mMasterView.setVisibility(View.VISIBLE);
    }

    public void toggleProgressbar(boolean show) {
        mProgressContainer.setVisibility((show) ? View.VISIBLE : View.GONE);
    }

    public void toggleMainScreen(boolean show) {
        mMainScreen.setVisibility((show) ? View.VISIBLE : View.GONE);
    }

    public void toggleSuccessScreen(boolean show, boolean isFresh) {
        toggleMainScreen(false);
        toggleProgressbar(false);
        mCongratsTV.setText(Html.fromHtml(String.format(mContext.getString(R.string.congrats_selectmember),
                Utility.getMyName(mContext))));
        if (isFresh) {
            mYouAreSelectMemberTV.setText(Html.fromHtml(String.valueOf(mContext.getText(R.string.you_are_now_select_member))));
            mEnjoyExperienceTV.setText(R.string.all_new_tm_select);
            mEnjoyExperienceTV.setVisibility(View.VISIBLE);
        } else {
            mYouAreSelectMemberTV.setText(R.string.tm_select_extended);
            mEnjoyExperienceTV.setVisibility(View.GONE);
        }
        mSuccessScreen.setVisibility((show) ? View.VISIBLE : View.GONE);
    }

    public void onDestroy() {
        if (mTMSelectMainContentUIHandler != null) {
            mTMSelectMainContentUIHandler.onDestroy();
        }
    }


}
