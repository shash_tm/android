/**
 *
 */
package com.trulymadly.android.app.utility;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.trulymadly.android.app.CircleTransformation;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.listener.OnActionBarClickedInterface;
import com.trulymadly.android.app.listener.OnActionBarMenuItemClickedInterface;
import com.trulymadly.android.app.listener.OnActionBarMenutItemProfileDeactivateClickedInterface;
import com.trulymadly.android.app.modal.MatchMessageMetaData;

/**
 * @author udbhav
 */
public class ActionBarHandler {

    private final OnClickListener onActionBarClickListener;
    private final OnLongClickListener onActionBarLongClickListener;
    private final AppCompatActivity aActionBarActivity;
    private final TextView action_bar_title;
    private final TextView action_bar_user_name;
    private final ImageView action_bar_share;
    private final ImageView action_bar_share_tutorial;
    private final ImageView action_bar_curated_deals_icon;
    private final View action_bar_curated_deals_container;
    private final View action_bar_curated_deals_badge;
    private final SearchView mSearchView;
    private final SearchView.OnQueryTextListener mOnQueryTextListener;
    private final ImageView location_blooper;
    private final ImageView scenes_blooper;
    private Toolbar toolbar = null;
    private SlidingMenuHandler menuHandler = null;
    private OnActionBarClickedInterface onActionBarClickedInterface = null;
    private OnActionBarMenuItemClickedInterface onActionBarMenuItemClickedInterface = null;
    private OnActionBarMenutItemProfileDeactivateClickedInterface onActionBarMenutItemProfileDeactivateClickedInterface = null;
    private TextView titleTv = null, conversationsBadge = null;
    private ImageView profilePicImageView = null;
    private View profilePicContainer = null, titleContainer = null, notificationCenter = null, actionBarBack = null,
            actionBarMenu = null, actionBarAskFriends = null, action_bar_overlay = null, actionBarLocation = null, actionBarCategories = null,
            actionBarMenuBlooper = null;
    private String title = null;
    private boolean isOptionsMenuVisible = false;
    private boolean isSearchViewExpanded = false;
    private boolean isSearchViewVisible = false;
    private boolean isClearChatEnabled = true;
    private boolean isTimerVisible = false;
    //    private AnalogTimerView mAnalogTimerView;
    private ImageView mSparkIcon;
    private Animation mSlideUpAnimation, mSlideDownAnimation;


    public ActionBarHandler(AppCompatActivity aActionBarActivity, String title, SlidingMenuHandler menuHandler,
                            OnActionBarClickedInterface onActionBarClickedInterface, Boolean showProfilePicInsteadOfTitle,
                            Boolean isUserProfileClickActive, boolean isOptionsMenuVisible) {

        this(aActionBarActivity, title, menuHandler, onActionBarClickedInterface,
                showProfilePicInsteadOfTitle, isUserProfileClickActive, isOptionsMenuVisible, false,
                false, null, false);
    }

    public ActionBarHandler(AppCompatActivity aActionBarActivity, String title, SlidingMenuHandler menuHandler,
                            OnActionBarClickedInterface onActionBarClickedInterface, Boolean showProfilePicInsteadOfTitle,
                            Boolean isUserProfileClickActive, boolean isOptionsMenuVisible, boolean isTitleClickActive, boolean isTimerVisible) {

        this(aActionBarActivity, title, menuHandler, onActionBarClickedInterface,
                showProfilePicInsteadOfTitle, isUserProfileClickActive, isOptionsMenuVisible, isTitleClickActive,
                false, null, isTimerVisible);
    }

    public ActionBarHandler(final AppCompatActivity aActionBarActivity, String title, SlidingMenuHandler menuHandler,
                            final OnActionBarClickedInterface onActionBarClickedInterface, boolean showProfilePicInsteadOfTitle,
                            boolean isUserProfileClickActive, boolean isOptionsMenuVisible, boolean isTitleClickActive,
                            boolean isSearchViewVisible, SearchView.OnQueryTextListener onQueryTextListener, boolean isTimerVisible) {
        this.isOptionsMenuVisible = isOptionsMenuVisible;
        this.isTimerVisible = isTimerVisible;
        this.aActionBarActivity = aActionBarActivity;
        View v;
        ActionBar actionBar;
        toolbar = (Toolbar) aActionBarActivity.findViewById(R.id.toolbar_main);
        mOnQueryTextListener = onQueryTextListener;
        mSearchView = (SearchView) toolbar.findViewById(R.id.action_bar_searchview);
        if (isSearchViewVisible) {
            mSearchView.setVisibility(View.VISIBLE);
        } else {
            mSearchView.setVisibility(View.GONE);
        }
        toolbar.setVisibility(View.VISIBLE);
        v = toolbar;
        aActionBarActivity.setSupportActionBar(toolbar);
        actionBar = aActionBarActivity.getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);

        profilePicContainer = v.findViewById(R.id.action_bar_profile_view_container);
        profilePicImageView = (ImageView) v.findViewById(R.id.action_bar_user_pic);
        titleContainer = v.findViewById(R.id.action_bar_title_container);

        action_bar_title = (TextView) v.findViewById(R.id.action_bar_title);
        action_bar_user_name = (TextView) v.findViewById(R.id.action_bar_user_name);
        this.title = title;
        showProfilePicInsteadOfTitle(showProfilePicInsteadOfTitle);

        conversationsBadge = (TextView) v.findViewById(R.id.action_bar_conversations_badge);
        conversationsBadge.setVisibility(View.GONE);

        action_bar_share = (ImageView) v.findViewById(R.id.action_bar_share);
        action_bar_share_tutorial = (ImageView) v.findViewById(R.id.action_bar_share_tutorial);
        action_bar_curated_deals_icon = (ImageView) v.findViewById(R.id.action_bar_curated_deals_icon);
        action_bar_curated_deals_container = v.findViewById(R.id.action_bar_curated_deals_container);
        action_bar_curated_deals_badge = v.findViewById(R.id.action_bar_curated_deals_badge);
        location_blooper = (ImageView) v.findViewById(R.id.location_blooper);
        scenes_blooper = (ImageView) v.findViewById(R.id.scenes_blooper);
        action_bar_share.setVisibility(View.GONE);
        action_bar_share_tutorial.setVisibility(View.GONE);
        action_bar_curated_deals_container.setVisibility(View.GONE);

        onActionBarClickListener = new OnClickListener() {
            @Override
            public void onClick(View v) {
                onActionBarClicked(v);
            }
        };

        onActionBarLongClickListener = new OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                onActionBarLongClicked(v);
                return true;
            }
        };

        if (isTitleClickActive) {
            titleContainer.setOnClickListener(onActionBarClickListener);
            titleContainer.setOnLongClickListener(onActionBarLongClickListener);
        }

        actionBarBack = v.findViewById(R.id.action_bar_back);
        actionBarLocation = v.findViewById(R.id.action_bar_location);
        actionBarMenu = v.findViewById(R.id.action_bar_menu);
        actionBarMenuBlooper = v.findViewById(R.id.action_bar_menu_blooper);
        action_bar_overlay = v.findViewById(R.id.action_bar_overlay);

        // actionBarCross.setOnClickListener(onActionBarClickListener);
        action_bar_overlay.setOnClickListener(onActionBarClickListener);
        actionBarLocation.setOnClickListener(onActionBarClickListener);
        action_bar_share.setOnClickListener(onActionBarClickListener);
        action_bar_curated_deals_container.setOnClickListener(onActionBarClickListener);

        if (menuHandler != null) {
            this.menuHandler = menuHandler;
            toggleMenuVisibility(true);
        } else {
            toggleBackButton(true);
        }
        notificationCenter = v.findViewById(R.id.action_bar_conversations);
        notificationCenter.setOnClickListener(onActionBarClickListener);
        actionBarCategories = v.findViewById(R.id.action_bar_categories);
        actionBarCategories.setOnClickListener(onActionBarClickListener);
        actionBarCategories.setVisibility(View.GONE);

        toggleNotificationCenter(false);
        setUserProfileClick(isUserProfileClickActive);

        if (onActionBarClickedInterface != null) {
            this.onActionBarClickedInterface = onActionBarClickedInterface;
        }

        mSparkIcon = (ImageView) v.findViewById(R.id.sparks_indicator_iv_titlebar);
//        mAnalogTimerView = (AnalogTimerView) v.findViewById(R.id.analog_timer_titlebar);
//        mAnalogTimerView.setVisibility((isTimerVisible)?View.VISIBLE:View.GONE);
        mSparkIcon.setVisibility((isTimerVisible) ? View.VISIBLE : View.GONE);
    }

    public void reconfigureTimer(int totalValue, int currentValue) {
//        mAnalogTimerView.reconfigure(totalValue, currentValue);
    }

    public void updateTimer(int currentValue) {
//        mAnalogTimerView.setCurrentValue(currentValue);
    }

    public void toggleTimer(boolean showTimer) {
        if (isTimerVisible && !showTimer) {
//            mAnalogTimerView.setVisibility(View.GONE);
            mSparkIcon.setVisibility(View.GONE);
        }

        if (!isTimerVisible && showTimer) {
//            mAnalogTimerView.setVisibility(View.VISIBLE);
            mSparkIcon.setVisibility(View.VISIBLE);
        }
        isTimerVisible = showTimer;
    }

    public void toggleTimer(boolean showTimer, int totalValue, int currentValue) {
        toggleTimer(showTimer);
        if (showTimer) {
            reconfigureTimer(totalValue, currentValue);
        }
    }

    public void setOnActionBarClickedInterface(OnActionBarClickedInterface onActionBarClickedInterface) {
        if (onActionBarClickedInterface != null) {
            this.onActionBarClickedInterface = onActionBarClickedInterface;
        }
    }

    public void setUserProfileClick(Boolean isUserProfileClickActive) {
        if (isUserProfileClickActive && onActionBarClickListener != null) {
            profilePicContainer.setOnClickListener(onActionBarClickListener);
        } else {
            profilePicContainer.setOnClickListener(null);
        }
    }

    public void setLocationBlooperVisibility(boolean visibility) {
        location_blooper.setVisibility(visibility ? View.VISIBLE : View.GONE);
    }

    public void setScenesBlooperVisibility(boolean visibility) {
        scenes_blooper.setVisibility(visibility ? View.VISIBLE : View.GONE);
    }

    public void showProfilePicInsteadOfTitle(Boolean showProfilePicInsteadOfTitle) {
        if (!showProfilePicInsteadOfTitle) {
            titleTv = action_bar_title;
            android.widget.LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams) titleTv
                    .getLayoutParams();
            if (isOptionsMenuVisible) {
                params.setMargins(UiUtils.dpToPx(15), 0, 0, 0);
            } else {
                params.setMargins(0, 0, 0, 0);
            }
            profilePicContainer.setVisibility(View.GONE);
            titleContainer.setVisibility(View.VISIBLE);
        } else {
            titleTv = action_bar_user_name;
            profilePicContainer.setVisibility(View.VISIBLE);
            titleContainer.setVisibility(View.GONE);
        }
        setTitle(title);

    }

    private void onActionBarClicked(View v) {
        switch (v.getId()) {
            case R.id.action_bar_back:
                if (onActionBarClickedInterface != null) {
                    onActionBarClickedInterface.onBackClicked();
                }
                break;
            case R.id.action_bar_location:
                if (onActionBarClickedInterface != null) {
                    onActionBarClickedInterface.onLocationClicked();
                }
                break;
            case R.id.action_bar_menu:
                toggleMenu();
                break;
            case R.id.action_bar_conversations:
                if (onActionBarClickedInterface != null) {
                    onActionBarClickedInterface.onConversationsClicked();
                }
                break;
            case R.id.action_bar_categories:
                if (onActionBarClickedInterface != null) {
                    onActionBarClickedInterface.onCategoriesClicked();
                }
                break;
            case R.id.action_bar_profile_view_container:
                if (onActionBarClickedInterface != null) {
                    onActionBarClickedInterface.onUserProfileClicked();
                }
                break;
            case R.id.action_bar_title:
            case R.id.action_bar_title_container:
                if (onActionBarClickedInterface != null) {
                    onActionBarClickedInterface.onTitleClicked();
                }
                break;
            case R.id.action_bar_share:
                if (onActionBarMenuItemClickedInterface != null) {
                    onActionBarMenuItemClickedInterface.onShareProfileClicked();
                }
                break;
            case R.id.action_bar_curated_deals_container:
                if (onActionBarClickedInterface != null) {
                    onActionBarClickedInterface.onCuratedDealsClicked();
                }
                break;
            default:
                break;
        }
    }

    private void onActionBarLongClicked(View v) {
        switch (v.getId()) {
            case R.id.action_bar_title_container:
                if (onActionBarClickedInterface != null) {
                    onActionBarClickedInterface.onTitleLongClicked();
                }
                break;
            case R.id.action_bar_title:
                if (onActionBarClickedInterface != null) {
                    onActionBarClickedInterface.onTitleLongClicked();
                }
                break;
            default:
                break;
        }
    }

    private void toggleMenu() {
        if (menuHandler != null) {
            menuHandler.getMenu().toggle(true);
        }
    }

    public void makeTitleBarLongClickable(boolean clickable) {
        titleContainer.setLongClickable(clickable);
        if (clickable)
            titleContainer.setOnLongClickListener(onActionBarLongClickListener);
        else
            titleContainer.setOnLongClickListener(null);
    }

    public void setTitle(String title) {
        if (titleTv != null) {
            if (title != null) {
                titleTv.setText(title);
            }
        }
    }

    public void setProfilePic(String profilePicUrl, String matchId) {
        if (profilePicImageView != null && profilePicUrl != null) {
            ImageCacheHelper.with(aActionBarActivity).loadWithKey(profilePicUrl, matchId)
                    .transform(new CircleTransformation())
                    .into(profilePicImageView);
        }
    }

    public void showConversationBadge(boolean sparksBlooper, boolean animate) {
        showConversationBadge(getConversationsCount(), sparksBlooper, animate);
    }

    public void slideUpAndBounceDown() {
        if (mSlideDownAnimation == null) {
            mSlideDownAnimation = new TranslateAnimation(
                    Animation.RELATIVE_TO_SELF, 0,
                    Animation.RELATIVE_TO_SELF, 0,
                    Animation.RELATIVE_TO_SELF, -0.5f,
                    Animation.RELATIVE_TO_SELF, 0
            );
            mSlideDownAnimation.setDuration(218);
            mSlideDownAnimation.setInterpolator(new AccelerateInterpolator());
            mSlideDownAnimation.setFillAfter(true);
            mSlideDownAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    int animCount = (int) conversationsBadge.getTag();
                    if (animCount > 0) {
                        conversationsBadge.setTag(animCount - 1);
                        slideUpAndBounceDown();
                    }
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        }

        if (mSlideUpAnimation == null) {
            mSlideUpAnimation = new TranslateAnimation(
                    Animation.RELATIVE_TO_SELF, 0,
                    Animation.RELATIVE_TO_SELF, 0,
                    Animation.RELATIVE_TO_SELF, 0,
                    Animation.RELATIVE_TO_SELF, -0.5f
            );
            mSlideUpAnimation.setDuration(218);
            mSlideUpAnimation.setInterpolator(new DecelerateInterpolator());
            mSlideUpAnimation.setFillAfter(true);
            mSlideUpAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    conversationsBadge.startAnimation(mSlideDownAnimation);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        }

        conversationsBadge.startAnimation(mSlideUpAnimation);


    }

    private void showConversationBadge(int count, boolean sparksBlooper, boolean animate) {
//        boolean sparksPresent = SparksHandler.isSparksEnabled(aActionBarActivity) && SparksDbHandler.getSparksCount(
//                    aActionBarActivity, Utility.getMyId(aActionBarActivity)) > 0;
        if (count > 0) {
            conversationsBadge.setVisibility(View.VISIBLE);
            conversationsBadge.setText(count + "");
            if (animate) {
                conversationsBadge.setTag(3);
                slideUpAndBounceDown();
            }
        } else if (sparksBlooper) {
            conversationsBadge.setText("");
            conversationsBadge.setVisibility(View.VISIBLE);
            if (animate) {
                conversationsBadge.setTag(3);
                slideUpAndBounceDown();
            }
        } else {
            conversationsBadge.setTag(0);
            conversationsBadge.clearAnimation();
            conversationsBadge.setVisibility(View.GONE);
        }
    }

    private int getConversationsCount() {
        return SPHandler.getInt(aActionBarActivity, ConstantsSP.SHARED_KEYS_CONVERSATION_COUNT);
    }

    public void setConversationsCount(int count) {
        SPHandler.setInt(aActionBarActivity, ConstantsSP.SHARED_KEYS_CONVERSATION_COUNT, count);
    }

    public void incConversationsCount() {
        setConversationsCount(getConversationsCount() + 1);
    }

    public void onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
//            case R.id.menu_item_share_profile:
//                if (onActionBarMenuItemClickedInterface != null) {
//                    onActionBarMenuItemClickedInterface.onShareProfileClicked();
//                }
//
//                break;
            case R.id.menu_item_unmatch_user:
                if (onActionBarMenuItemClickedInterface != null) {
                    onActionBarMenuItemClickedInterface.onUmatchUserClicked();
                }
                break;
            case R.id.menu_item_clear_chat:
                if (onActionBarMenuItemClickedInterface != null && isClearChatEnabled) {
                    onActionBarMenuItemClickedInterface.onClearChat();
                }
                break;

            case R.id.menu_item_block_user:
                if (onActionBarMenuItemClickedInterface != null) {
                    onActionBarMenuItemClickedInterface.onBlockUserClicked();
                }
                break;
            case R.id.menu_item_deactivate_profile:
                if (onActionBarMenutItemProfileDeactivateClickedInterface != null)
                    onActionBarMenutItemProfileDeactivateClickedInterface.onProfileDeactivateClicked();
                break;
            case R.id.menu_item_delete_spark:
                if (onActionBarMenuItemClickedInterface != null) {
                    onActionBarMenuItemClickedInterface.onDeleteSparkClicked();
                }
                break;


            default:
                break;
        }
    }

    public void setOptionsMenuHandler(OnActionBarMenuItemClickedInterface onClickedOptionsMenu) {
        onActionBarMenuItemClickedInterface = onClickedOptionsMenu;
    }

    public void toggleNotificationCenter(Boolean showNotificationCenter) {
        if (notificationCenter != null) {
            notificationCenter.setVisibility(showNotificationCenter ? View.VISIBLE : View.GONE);
        }
    }

    public void toggleCategories(Boolean showCategories) {
        if (actionBarCategories != null) {
            actionBarCategories.setVisibility(showCategories ? View.VISIBLE : View.GONE);
        }
    }

    public void toggleTitleBar(Boolean showTitleBar) {
        if (titleContainer != null) {
            titleContainer.setVisibility(showTitleBar ? View.VISIBLE : View.GONE);
        }
    }

    public void toggleMenuBlooperVisibility(boolean showBlooper) {
        actionBarMenuBlooper.setVisibility(showBlooper ? View.VISIBLE : View.GONE);
    }

    public void toggleMenuVisibility(boolean showMenu) {
        actionBarMenu.setVisibility(showMenu ? View.VISIBLE : View.GONE);
        toggleMenuHandler(showMenu);
        // HIDE THE BACK BUTTON AND TOGGLE LISTENERS
        if (showMenu) {
            toggleMenuBlooperVisibility(false);
            actionBarMenu.setOnClickListener(onActionBarClickListener);
            actionBarBack.setVisibility(View.GONE);
            actionBarBack.setOnClickListener(null);
        }
    }

    public void toggleLocationIcon(boolean showLocation) {
        actionBarLocation.setVisibility(showLocation ? View.VISIBLE : View.GONE);
    }

    public boolean isSearchViewVisible() {
        return isSearchViewVisible;
    }

    public void toggleSearchView(boolean showSearchView) {
        if (mSearchView == null) {
            return;
        }
        isSearchViewVisible = showSearchView;
        if (showSearchView) {
            mSearchView.setVisibility(View.VISIBLE);
            boolean isSearchViewInitialized = false;
            if (!isSearchViewInitialized) {
                int searchPlateId = mSearchView.getContext().getResources().getIdentifier(
                        "android:id/search_src_text", null, null);
                final EditText searchPlate = (EditText) mSearchView.findViewById(searchPlateId);
                searchPlate.setTextColor(Color.WHITE);
                searchPlate.setHint(aActionBarActivity.getString(R.string.search_hint));
                searchPlate.setHintTextColor(Color.WHITE);
                mSearchView.setOnQueryTextListener(mOnQueryTextListener);
                mSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
                    @Override
                    public boolean onClose() {
                        isSearchViewExpanded = false;
                        if (onActionBarClickedInterface != null) {
                            onActionBarClickedInterface.onSearchViewClosed();
                        }
                        return false;
                    }
                });
                mSearchView.setOnSearchClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        isSearchViewExpanded = true;
                        if (onActionBarClickedInterface != null) {
                            onActionBarClickedInterface.onSearchViewOpened();
                        }
                    }
                });
            }
        } else {
            mSearchView.setVisibility(View.GONE);
            mSearchView.setOnQueryTextListener(null);
        }
    }

    public void expandCollapseSearchView(boolean showSearchView) {
        if (mSearchView == null) {
            return;
        }
        isSearchViewExpanded = showSearchView;
        if (showSearchView) {
            mSearchView.onActionViewExpanded();
        } else {
            mSearchView.onActionViewCollapsed();
        }
    }

    public boolean isSearchViewExpanded() {
        return isSearchViewExpanded;
    }

    public void toggleBackButton(boolean showBackButton) {
        actionBarBack.setVisibility(showBackButton ? View.VISIBLE : View.GONE);
        // HIDE THE MENU BUTTON AND TOGGLE LISTENERS
        if (showBackButton) {
            actionBarBack.setOnClickListener(onActionBarClickListener);
            actionBarMenu.setVisibility(View.GONE);
            actionBarMenu.setOnClickListener(null);
        }
    }

    private void toggleMenuHandler(boolean showMenu) {
        if (menuHandler != null) {
            menuHandler.toggleVisibility(showMenu);
        }
    }

    public void toggleActionBar(boolean showActionBar) {
        toggleMenuHandler(showActionBar);
        if (showActionBar) {
            toolbar.setVisibility(View.VISIBLE);
        } else {
            toolbar.setVisibility(View.GONE);
        }
    }

    public void toggleToolbarShadow(boolean isToolbarShadowRequired){
        if(toolbar != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if(isToolbarShadowRequired) {
                toolbar.setElevation(UiUtils.dpToPx(4));
            }else{
                toolbar.setElevation(0f);
            }
        }
    }

    public void setOptionsMenuProfileDeativateHandler(
            OnActionBarMenutItemProfileDeactivateClickedInterface onClickedOptionsMenu) {
        onActionBarMenutItemProfileDeactivateClickedInterface = onClickedOptionsMenu;
    }

    public void toggleOverlay(boolean showOverlay) {
        if (action_bar_overlay != null) {
            action_bar_overlay.setVisibility(showOverlay ? View.VISIBLE : View.GONE);
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void setToolbarTransparent(boolean setTransparent, String title) {
        if (setTransparent) {
            UiUtils.setBackground(aActionBarActivity, toolbar, R.drawable.scrim_gradient_light);
        } else {
            toolbar.setBackgroundColor(aActionBarActivity.getResources().getColor(R.color.colorPrimary));
        }
        setTitle(title);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            aActionBarActivity.getWindow().setStatusBarColor(aActionBarActivity.getResources()
                    .getColor(setTransparent ? R.color.black : R.color.colorPrimaryDark));
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void setToolbarColor(int color, String title) {
        toolbar.setBackgroundColor(color);
        setTitle(title);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            aActionBarActivity.getWindow().setStatusBarColor(color);
        }
    }

    public void toogleToolbarOpacity(Context context, final boolean showToolbar) {
        int anim = R.anim.fadeout;
        if (showToolbar)
            anim = R.anim.fade_in;

        Animation animation = AnimationUtils.loadAnimation(context, anim);
        animation.setFillAfter(true);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                toggleActionBar(true);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (showToolbar)
                    toggleActionBar(true);
                else
                    toggleActionBar(false);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        toolbar.startAnimation(animation);
    }

    public void toggleClearChat(boolean isClearChatEnabled) {
        this.isClearChatEnabled = isClearChatEnabled;

    }

    public void toggleCuratedDealsView(boolean showCuratedDealsView, MatchMessageMetaData.CDClickableState mCDCdClickableState) {
        int icon = R.drawable.cd_icon;
        if (mCDCdClickableState != null) {
            icon = mCDCdClickableState.getImage();
        }
        if (showCuratedDealsView) {
            Picasso.with(aActionBarActivity).load(icon).noFade().into(action_bar_curated_deals_icon);
        }
        action_bar_curated_deals_container.setVisibility(showCuratedDealsView ? View.VISIBLE : View.GONE);
    }

    public void toggleCuratedDealsBlooper(int newDatesCount) {
        if (newDatesCount > 0) {
            action_bar_curated_deals_badge.setVisibility(View.VISIBLE);
        } else {
            action_bar_curated_deals_badge.setVisibility(View.GONE);
        }
    }

    public void toggleShareView(boolean showShareView) {
        if (showShareView) {
            Picasso.with(aActionBarActivity).load(R.drawable.share_icon_white).noFade().into(action_bar_share);
        }
        action_bar_share.setVisibility(showShareView ? View.VISIBLE : View.GONE);
    }

    public void showShareTutorial(boolean showShareTutorial) {
        if (showShareTutorial) {
            Picasso.with(aActionBarActivity).load(R.drawable.share_icon_white_blue_bg).noFade().into(action_bar_share_tutorial);
        }
        action_bar_share_tutorial.setVisibility(showShareTutorial ? View.VISIBLE : View.GONE);
        action_bar_share.setVisibility(!showShareTutorial ? View.VISIBLE : View.GONE);
    }
}
