package com.trulymadly.android.app.utility;

import android.annotation.SuppressLint;
import android.os.Build;
import android.view.View;
import android.webkit.WebView;
import android.widget.ProgressBar;

import com.trulymadly.android.app.R;
import com.trulymadly.android.app.json.Constants;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by avin on 24/02/16.
 */
public class WebviewHandler {
    private final View mWebViewOverlay;
    private final WebView mWebView;
    private final WebviewActionsListener mWebviewActionsListener;
    private boolean isVisible = false;
    private boolean isJSEnabled = false;
    private boolean isCrossEnabled = true;

    @SuppressLint("SetJavaScriptEnabled")
    public WebviewHandler(View mIncludeView, WebviewActionsListener webviewActionsListener, boolean isJSEnabled, boolean isCrossEnabled) {
        mWebViewOverlay = mIncludeView;
        this.isJSEnabled = isJSEnabled;
        this.isCrossEnabled = isCrossEnabled;
        mWebView = (WebView) mIncludeView.findViewById(R.id.webview);
        ProgressBar mProgressBar = (ProgressBar) mIncludeView.findViewById(R.id.progress_bar);
        View mCloseIV = mIncludeView.findViewById(R.id.close_webview);
        mCloseIV.setVisibility((isCrossEnabled)?View.VISIBLE:View.GONE);
        this.mWebviewActionsListener = webviewActionsListener;
        mWebView.getSettings().setJavaScriptEnabled(isJSEnabled);
        mWebView.setWebViewClient(new WebViewClientWithProgressBar(mProgressBar){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                url = url.trim();
                boolean deepLinkLaunched = Utility.launchDeepLinkInWebview(view.getContext().getApplicationContext(), url);
                if (!deepLinkLaunched) {
                    return mWebviewActionsListener.shouldOverrideUrlLoading(url);
                } else {
                    hide();
                    mWebviewActionsListener.webViewHiddenOnUrlLoad();
                    return true;
                }
            }
        });
        View.OnClickListener onCloseClicked = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mWebviewActionsListener.onWebViewCloseClicked();
                hide();
            }
        };
        mCloseIV.setOnClickListener(onCloseClicked);
    }

    @SuppressLint("SetJavaScriptEnabled")
    public void loadUrl(String url) {
        url = url.trim();
        boolean deepLinkLaunched = Utility.launchDeepLinkInWebview(mWebView.getContext().getApplicationContext(), url);
        if (!deepLinkLaunched) {
            Map<String, String> headers = new HashMap<>();
            headers.put("app_version_code", Utility.getAppVersionCode(mWebView.getContext()));
            headers.put("source", "androidApp");
            headers.put("store", Constants.RELEASE_STORE);
            headers.put("app_version_name",
                    Utility.getAppVersionName(mWebView.getContext()));
            headers.put("device_id",
                    Utility.getDeviceId(mWebView.getContext()));
            mWebView.getSettings().setJavaScriptEnabled(isJSEnabled);
            mWebView.loadUrl(url, headers);
            mWebViewOverlay.setVisibility(View.VISIBLE);
            isVisible = true;
        } else {
            hide();
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    public void loadUrl(String url, Map<String, String> headers) {
        url = url.trim();
        boolean deepLinkLaunched = Utility.launchDeepLinkInWebview(mWebView.getContext().getApplicationContext(), url);
        if (!deepLinkLaunched) {
            mWebView.getSettings().setJavaScriptEnabled(isJSEnabled);
            mWebView.loadUrl(url, headers);
            mWebViewOverlay.setVisibility(View.VISIBLE);
            isVisible = true;
        } else {
            hide();
        }
    }

    public void hide() {
        mWebViewOverlay.setVisibility(View.GONE);
        mWebView.getSettings().setJavaScriptEnabled(false);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) {
            mWebView.clearView();
        } else {
            mWebView.loadUrl("about:blank");
        }
        isVisible = false;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public interface WebviewActionsListener {
        boolean shouldOverrideUrlLoading(String url);

        void webViewHiddenOnUrlLoad();

        void onWebViewCloseClicked();
    }
}
