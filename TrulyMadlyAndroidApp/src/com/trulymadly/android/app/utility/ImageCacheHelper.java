package com.trulymadly.android.app.utility;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.crashlytics.android.Crashlytics;
import com.squareup.picasso.Callback.EmptyCallback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;
import com.squareup.picasso.Target;
import com.squareup.picasso.Transformation;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.listener.ImageWriteCompleteInterface;
import com.trulymadly.android.app.listener.OnDataLoadedInterface;

import java.io.File;

public class ImageCacheHelper {

	private static final String FALLBACK_FILE_PREFIX = "fallback_";

	private Context mContext;
	private String mParentDirectoryPath;

	public static String getPathFromUrl(String url, String key, boolean isFolderKey) {
		String mParentDirectoryPath = FilesHandler.getFilePath(Constants.disk_path, Constants.IMAGES_ASSETS_FOLDER);
		String mFileName = null;
		if (isFolderKey) {
			mParentDirectoryPath = FilesHandler.getFilePath(mParentDirectoryPath, key);
			mFileName = url.substring(url.lastIndexOf("/") + 1);
		} else {
			mFileName = key + "_" + url.substring(url.lastIndexOf("/") + 1);
		}

		return FilesHandler.getFilePath(mParentDirectoryPath, mFileName);
	}

	public static ImageCacheHelper with(Context context) {
		ImageCacheHelperContainer.INSTANCE.setContext(context.getApplicationContext());
		return ImageCacheHelperContainer.INSTANCE;
	}

	/**
	 * Clears the files with the given unique key from the cache folder
	 *
	 * @param uniqueKey
	 */
	public static void removeImageWithUniqueKey(String uniqueKey) {
		clearCache(uniqueKey + "_.*");
	}

	/**
	 * Clears the files matching the given pattern from the cache folder
	 *
	 * @param pattern
	 */
	private static void clearCache(String pattern) {
		FilesHandler.deleteContents(FilesHandler.getFilePath(Constants.disk_path, Constants.IMAGES_ASSETS_FOLDER),
				pattern, true);
	}

	/**
	 * Clears the cache folder with the exception of the ".nomedia" file
	 */
	public static void clearCache() {
		FilesHandler.deleteContents(FilesHandler.getFilePath(Constants.disk_path, Constants.IMAGES_ASSETS_FOLDER),
				".nomedia", false);
	}

	private void setContext(Context context) {
		mContext = context;
		mParentDirectoryPath = FilesHandler.getFilePath(Constants.disk_path, Constants.IMAGES_ASSETS_FOLDER);
	}

	/**
	 * Tells ImageCacheHelper which url to use for downloading the image along with the
	 * unique key which is appended to the filename at the beginning
	 * @param serverUrl - Server url to be used for downloading the image
	 * @param key - Unique key to be used
	 */
	public ImageCacheLoader loadWithKey(String serverUrl, String key) {
		return new ImageCacheLoader(serverUrl, key);
	}

	/**
	 * Tells ImageCacheHelper which url to use for downloading the image along with the
	 * folder name wherethe file is saved
	 *
	 * @param folder    - Folder in which the file is to be saved
	 * @param serverUrl - Server url to be used for downloading the image
	 */
	public ImageCacheLoader loadInFolder(String serverUrl, String folder) {
		mParentDirectoryPath = FilesHandler.getFilePath(mParentDirectoryPath, folder);
		return new ImageCacheLoader(serverUrl);
	}

//	/**
//	 * Tells ImageCacheHelper which url to use for downloading the image
//	 * @param serverUrl - Server url to be used for downloading the image
//	 */
	public ImageCacheLoader load(String serverUrl){
		return new ImageCacheLoader(serverUrl);
	}

	private static class ImageCacheHelperContainer {
		public static final ImageCacheHelper INSTANCE = new ImageCacheHelper();
	}

//	/**
//	 * Tells ImageCacheHelper which url to use for downloading the image
//	 *
//	 * @param serverUrl - Server url to be used for downloading the image
//	 */
//	public ImageCacheLoader load(String serverUrl){
//		return new ImageCacheLoader(serverUrl);
//	}

	//Takes care of the downloading and displaying the image into the imageview
	//Should not be used without ImageCacheHelper as it internally uses the context
	//used to initialize the ImageCacheHelper
	public class ImageCacheLoader{
		private final String mServerUrl;
		private final String mFileName;
		private final String mUniqueKey;
		private ImageView mImageView;
		private OnDataLoadedInterface mOnDataLoadedInterface, mOnImageDownloadedInterface,
				mOnFallbackImageDownloadedListener;
		private File mImageFile;
		private boolean isFileExists;
		private RequestCreator mRequestCreatorLocal, mRequestCreatorServer;

		private String mFallbackUrl, mFallbackFileName;
		private File mFallbackFile;
		private boolean isFallbackUrlExists;
		private RequestCreator mFallbackRequestCreatorLocal;
		private RequestCreator mFallbackRequestCreatorServer;

		public ImageCacheLoader(String serverUrl, String key){
			mServerUrl = serverUrl;
			mUniqueKey = key;
			mFileName = mUniqueKey + "_" + mServerUrl.substring(mServerUrl.lastIndexOf("/") + 1);
			createImageFile();
		}

		public ImageCacheLoader(String serverUrl) {
			mServerUrl = serverUrl;
			mUniqueKey = null;
			mFileName = mServerUrl.substring(mServerUrl.lastIndexOf("/") + 1);
			createImageFile();
		}

//		public ImageCacheLoader(String serverUrl){
//			mServerUrl = serverUrl;
//			mFileName = mServerUrl.substring(mServerUrl.lastIndexOf("/") + 1);
//			createImageFile();
//		}

		//Creates the imagefile from the given path and filename
		private void createImageFile(){
			mImageFile = new File(FilesHandler.getFilePath(mParentDirectoryPath, mFileName));
			File parentFile = mImageFile.getParentFile();
			if (!parentFile.exists()) {
				parentFile.mkdirs();
			}
			isFileExists = mImageFile.exists();
		}

		/**
		 * Sets a placeholder to the given imageview using the given resource id
		 *
		 * @param resourceId
		 * */
		public ImageCacheLoader placeholder(int resourceId) {
			if (Utility.isSet(mFallbackUrl) && !isFileExists) {
				getFallbackRequestCreator(true).placeholder(resourceId);
				getFallbackRequestCreator(false).placeholder(resourceId);
			} else {
				getRequestCreator(true).placeholder(resourceId);
				getRequestCreator(false).placeholder(resourceId);
			}
			return this;
		}

		/**
		 * Sets a placeholder to the given imageview using the given resource
		 *
		 * @param resource
		 * */
		public ImageCacheLoader placeholder(Drawable resource) {
			if (Utility.isSet(mFallbackUrl) && !isFileExists) {
				getFallbackRequestCreator(true).placeholder(resource);
				getFallbackRequestCreator(false).placeholder(resource);
			} else {
				getRequestCreator(true).placeholder(resource);
				getRequestCreator(false).placeholder(resource);
			}
			return this;
		}

//		/**
//		 * Sets a placeholder to the given imageview using the given uri
//		 *
//		 * @param uri
//		 * */
//		public ImageCacheLoader placeholder(String uri){
//			if(Utility.isSet(mFallbackUrl) && !isFileExists){
//				getFallbackRequestCreator(true).placeholder(resource);
//				getFallbackRequestCreator(false).placeholder(resource);
//			}else {
//				getRequestCreator(true).placeholder(resource);
//				getRequestCreator(false).placeholder(resource);
//			}
//			return this;
//		}

		/**
		 * Resizes the image using the given dimensions
		 *
		 * @param width
		 * @param height
		 * */
		public ImageCacheLoader resize(int width, int height) {
			if (Utility.isSet(mFallbackUrl) && !isFileExists) {
				getFallbackRequestCreator(true).resize(width, height);
				getFallbackRequestCreator(false).resize(width, height);
			} else {
				getRequestCreator(true).resize(width, height);
				getRequestCreator(false).resize(width, height);
			}
			return this;
		}

		/**
		 * Applies the given transformation to the image
		 *
		 * @param transformation
		 * */
		public ImageCacheLoader transform(Transformation transformation) {
			if (Utility.isSet(mFallbackUrl) && !isFileExists) {
				getFallbackRequestCreator(true).transform(transformation);
				getFallbackRequestCreator(false).transform(transformation);
			} else {
				getRequestCreator(true).transform(transformation);
				getRequestCreator(false).transform(transformation);
			}
			return this;
		}

		/**
		 * Applies centerCrop to the given image
		 * */
		public ImageCacheLoader centerCrop() {
			if (Utility.isSet(mFallbackUrl) && !isFileExists) {
				getFallbackRequestCreator(true).centerCrop();
				getFallbackRequestCreator(false).centerCrop();
			} else {
				getRequestCreator(true).centerCrop();
				getRequestCreator(false).centerCrop();
			}
			return this;
		}

		/**
		 * Applies fit to the given image
		 */
		public ImageCacheLoader fit() {
			if (Utility.isSet(mFallbackUrl) && !isFileExists) {
				getFallbackRequestCreator(true).fit();
				getFallbackRequestCreator(false).fit();
			} else {
				getRequestCreator(true).fit();
				getRequestCreator(false).fit();
			}
			return this;
		}

		/**
		 * Applies config to the given image
		 */
		public ImageCacheLoader config(Bitmap.Config config) {
			if (Utility.isSet(mFallbackUrl) && !isFileExists) {
				getFallbackRequestCreator(true).config(config);
				getFallbackRequestCreator(false).config(config);
			} else {
				getRequestCreator(true).config(config);
				getRequestCreator(false).config(config);
			}
			return this;
		}

		/**
		 * Applies tag to the given Picasso request
		 * */
		public ImageCacheLoader tag(Object tag){
			if(Utility.isSet(mFallbackUrl) && !isFileExists){
				getFallbackRequestCreator(true).tag(tag);
				getFallbackRequestCreator(false).tag(tag);
			}else {
				getRequestCreator(true).tag(tag);
				getRequestCreator(false).tag(tag);
			}
			return this;
		}

		/**
		 * Applies centerInside to the given image
		 */
		public ImageCacheLoader centerInside() {
			if (Utility.isSet(mFallbackUrl) && !isFileExists) {
				getFallbackRequestCreator(true).centerInside();
				getFallbackRequestCreator(false).centerInside();
			} else {
				getRequestCreator(true).centerInside();
				getRequestCreator(false).centerInside();
			}
			return this;
		}

		/**
		 * Fetchs the image and saves it.
		 */
		public void fetch() {
			downloadImage(1, 1, mServerUrl, mImageFile, null);
		}

		public ImageCacheLoader fallback(String fallbackUrl, OnDataLoadedInterface onFallbackImageDownloadedListener) {
			if (!Utility.isSet(fallbackUrl)) {
				return this;
			}
			mFallbackUrl = fallbackUrl;
			mFallbackFileName = FALLBACK_FILE_PREFIX + mFallbackUrl.substring(mFallbackUrl.lastIndexOf("/") + 1);
			mFallbackFile = new File(mParentDirectoryPath, mFallbackFileName);
			isFallbackUrlExists = mFallbackFile.exists();
			mOnFallbackImageDownloadedListener = onFallbackImageDownloadedListener;

			return this;
		}

		/**
		 * Loads image into the given imageview
		 * @param imageView - ImageView into which the image is required to be loaded
		 *
		 * returns true -> If original image was loaded
		 * returns false -> If fallback image was loaded
		 */
		public boolean into(ImageView imageView) {
			mImageView = imageView;
			return displayImage();
		}

		/**
		 * Loads image into the given imageview with the given dataLoadListener
		 * @param imageView - ImageView into which the image is required to be loaded
		 * @param onDataLoadedInterface - OnDataLoadedInterface to be used for tracking purpose
		 *
		 * returns true -> If original image was loaded
		 * returns false -> If fallback image was loaded
		 */
		@SuppressWarnings("UnusedReturnValue")
		public boolean into(ImageView imageView, OnDataLoadedInterface onDataLoadedInterface) {
			mImageView = imageView;
			mOnDataLoadedInterface = onDataLoadedInterface;
			return displayImage();
		}

		/**
		 * Loads image into the given imageview with the given dataLoadListener
		 *
		 * @param imageView             - ImageView into which the image is required to be loaded
		 * @param onDataLoadedInterface - OnDataLoadedInterface to be used for tracking purpose
		 * @param onDownloadedInterface - OnDataLoadedInterface to be used for tracking downloading
		 *                              <p/>
		 *                              returns true -> If original image was loaded
		 *                              returns false -> If fallback image was loaded
		 */
		public boolean into(ImageView imageView, OnDataLoadedInterface onDataLoadedInterface,
							OnDataLoadedInterface onDownloadedInterface) {
			mImageView = imageView;
			mOnDataLoadedInterface = onDataLoadedInterface;
			mOnImageDownloadedInterface = onDownloadedInterface;
			return displayImage();
		}

		/**
		 * Loads image into the given imageview with the given dataLoadListener
		 *
		 * @param target - target into which the bitmap is to be loaded
		 *               <p/>
		 *               returns true -> If original image was loaded
		 *               returns false -> If fallback image was loaded
		 */
		@SuppressWarnings("UnusedReturnValue")
		public boolean into(Target target) {
			return loadIntoTarget(target);
		}

		/**
		 * Loads image into the given target
		 *
		 * @param target - target into which the bitmap is to be loaded
		 *               <p/>
		 *               returns true -> If original image was loaded
		 *               returns false -> If fallback image was loaded
		 */
		private boolean loadIntoTarget(Target target) {
			if (isFileExists) {
				RequestCreator i = getRequestCreator(true);
				i.into(target);
				return true;
			} else if (Utility.isSet(mFallbackUrl)) {
				if (isFallbackUrlExists) {
					RequestCreator i = getFallbackRequestCreator(true);
					i.into(target);
				} else {
					downloadAndLoadFallbackImage(target);
				}
				return false;
			} else {
				downloadAndLoadImage(target);
				return true;
			}
		}

		public void fetch(OnDataLoadedInterface onDataLoadedInterface) {
			downloadImage(1, 1, mServerUrl, mImageFile, onDataLoadedInterface);
		}

//		private RequestCreator getRequestCreator(){
//			if(mRequestCreator == null){
//				if(isFileExists)
//					mRequestCreator = Picasso.with(mContext).load(mImageFile);
//				else
//					mRequestCreator = Picasso.with(mContext).load(mServerUrl);
//			}
//			return mRequestCreator;
//		}

		private RequestCreator getRequestCreator(boolean isLocal) {
			if (mRequestCreatorLocal == null) {
				mRequestCreatorLocal = Picasso.with(mContext).load(mImageFile);//.config(Bitmap.Config.RGB_565);
			}
			if (mRequestCreatorServer == null) {
				mRequestCreatorServer = Picasso.with(mContext).load(mServerUrl);//.config(Bitmap.Config.RGB_565);
			}

			if (isLocal) {
				return mRequestCreatorLocal;
			} else {
				return mRequestCreatorServer;
			}
		}

		private RequestCreator getFallbackRequestCreator(boolean isLocal) {
			if (mFallbackRequestCreatorLocal == null) {
				mFallbackRequestCreatorLocal = Picasso.with(mContext).load(mFallbackFile);//.config(Bitmap.Config.RGB_565);
			}
			if (mFallbackRequestCreatorServer == null) {
				mFallbackRequestCreatorServer = Picasso.with(mContext).load(mFallbackUrl);//.config(Bitmap.Config.RGB_565);
			}

			if(isLocal){
				return mFallbackRequestCreatorLocal;
			} else {
				return mFallbackRequestCreatorServer;
			}
		}

		/**
		 * Displays the image and calls download if image is not already present in the cache folder
		 */
		private boolean displayImage() {
			if (isFileExists) {
				try {
					RequestCreator i = getRequestCreator(true);
					i.noFade().into(mImageView, new EmptyCallback() {

						@Override
						public void onSuccess() {
							if (mOnDataLoadedInterface != null)
								mOnDataLoadedInterface.onLoadSuccess(0);
						}

						@Override
						public void onError() {
							downloadAndDisplayImage();
						}
					});
				}catch(OutOfMemoryError oome){
					Crashlytics.log("OutOfMemory while loading " + mServerUrl);
				}
				return true;
			} else if (Utility.isSet(mFallbackUrl)) {
				try {
					if (isFallbackUrlExists) {
						RequestCreator i = getFallbackRequestCreator(true);
						i.noFade().into(mImageView, new EmptyCallback() {

							@Override
							public void onSuccess() {
								if (mOnDataLoadedInterface != null)
									mOnDataLoadedInterface.onLoadSuccess(0);
							}

							@Override
							public void onError() {
								downloadAndDisplayFallbackImage();
							}
						});
					} else {
						downloadAndDisplayFallbackImage();
					}
				}catch(OutOfMemoryError oome){
					Crashlytics.log("OutOfMemory while loading fallback " + mFallbackUrl);
				}
				return false;
			} else {
				try {
					downloadAndDisplayImage();
				}catch(OutOfMemoryError oome){
					Crashlytics.log("OutOfMemory while loading " + mServerUrl);
				}
				return true;
			}
		}

		/**
		 * Downloads and loads the fallback image
		 */
		private void downloadAndLoadFallbackImage(Target target) {
			RequestCreator i = getFallbackRequestCreator(false);
			i.noFade().into(target);
			downloadImage(1, 1, mFallbackUrl, mFallbackFile, null);
		}

		/**
		 * Downloads and displays the image
		 */
		private void downloadAndDisplayImage() {
			RequestCreator i = getRequestCreator(false);
			i.noFade().into(mImageView, new EmptyCallback() {
				@Override
				public void onSuccess() {
					if (mOnDataLoadedInterface != null)
						mOnDataLoadedInterface.onLoadSuccess(0);
				}

				@Override
				public void onError() {
					if (mOnDataLoadedInterface != null)
						mOnDataLoadedInterface.onLoadFailure(null, 0);
				}
			});

			downloadImage(1, 1, mServerUrl, mImageFile, mOnImageDownloadedInterface);
		}

		/**
		 * Downloads and loads the image
		 */
		private void downloadAndLoadImage(Target target) {
			RequestCreator i = getRequestCreator(false);
			i.noFade().into(target);
			downloadImage(1, 1, mServerUrl, mImageFile, null);
		}

		/**
		 * Downloads and displays the fallback image
		 */
		private void downloadAndDisplayFallbackImage() {
			RequestCreator i = getFallbackRequestCreator(false);
			i.noFade().into(mImageView, new EmptyCallback() {
				@Override
				public void onSuccess() {
					if(mOnDataLoadedInterface != null)
						mOnDataLoadedInterface.onLoadSuccess(0);
				}

				@Override
				public void onError() {
					if (mOnDataLoadedInterface != null)
						mOnDataLoadedInterface.onLoadFailure(null, 0);
				}
			});

			downloadImage(1, 1, mFallbackUrl, mFallbackFile, mOnFallbackImageDownloadedListener);
		}

		/**
		 * Downloads the image
		 * @param count - number of times it has been tried so far
		 * @param maxCount - maximum number of times we should try to download the image
		 */
		private void downloadImage(final int count, final int maxCount, final String serverUrl,
								   final File imageFile, final OnDataLoadedInterface onDataLoadedInterface) {
			if (count > maxCount)
				return;

			FilesHandler.downloadFile(mContext, serverUrl, imageFile, new ImageWriteCompleteInterface() {
				@Override
				public void onSuccess(long mTimeTaken) {
					if (onDataLoadedInterface != null) {
						onDataLoadedInterface.onLoadSuccess(mTimeTaken);
					}
				}

				@Override
				public void onFail(Exception e, long mTimeTaken) {
					downloadImage(count + 1, maxCount, serverUrl, imageFile, onDataLoadedInterface);
					Crashlytics.logException(e);
					if(count == maxCount && onDataLoadedInterface != null){
						onDataLoadedInterface.onLoadFailure(e, mTimeTaken);
					}
				}
			});

		}
	}
	
	
}
