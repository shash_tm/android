package com.trulymadly.android.app;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;

import com.squareup.picasso.Transformation;

//http://stackoverflow.com/questions/26112150/android-create-circular-image-with-picasso

public class CircleTransformation implements Transformation {
    @Override
    public Bitmap transform(Bitmap source) {
        int size = Math.min(source.getWidth(), source.getHeight());

        int x = (source.getWidth() - size) / 2;
        int y = (source.getHeight() - size) / 2;

        Bitmap squaredBitmap;
        try {
            squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
            if (squaredBitmap != source) {
                source.recycle();
            }
        } catch (NullPointerException e) {
            return source;
        }
        Bitmap bitmap;
        Canvas canvas;
        try {
            bitmap = Bitmap.createBitmap(size, size, source.getConfig());
            canvas = new Canvas(bitmap);
        } catch (NullPointerException e) {
            return squaredBitmap;
        } catch (IllegalArgumentException e) {
            return squaredBitmap;
        }

        Paint paint = new Paint();
        BitmapShader shader = new BitmapShader(squaredBitmap,
                BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
        paint.setShader(shader);
        paint.setAntiAlias(true);

        float r = size / 2f;
        canvas.drawCircle(r, r, r, paint);

        squaredBitmap.recycle();
        return bitmap;
    }

    @Override
    public String key() {
        return "circle";
    }
}