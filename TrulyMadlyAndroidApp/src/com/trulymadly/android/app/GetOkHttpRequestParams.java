package com.trulymadly.android.app;

import com.trulymadly.android.app.json.Constants.HttpRequestType;
import com.trulymadly.android.app.modal.EditPrefSaveModal;
import com.trulymadly.android.app.modal.LoginDetail;
import com.trulymadly.android.app.modal.RegisterDetail;
import com.trulymadly.android.app.modal.SelectQuizModal;
import com.trulymadly.android.app.modal.SparkModal;
import com.trulymadly.android.app.modal.SparkSendModal;
import com.trulymadly.android.app.modal.TrustBuilderParamsFacebook;
import com.trulymadly.android.app.utility.SparksHandler;
import com.trulymadly.android.app.utility.Utility;

import java.util.HashMap;
import java.util.Map;

public class GetOkHttpRequestParams {
    public static Map<String, String> getHttpRequestParams(
            HttpRequestType httpRequestType) {
        return getHttpRequestParams(httpRequestType, null);
    }

    public static Map<String, String> getHttpRequestParams(
            HttpRequestType httpRequestType, Object object) {
        LoginDetail aLoginDetail = null;
        Map<String, String> params = new HashMap<>();

        //params.put("login_mobile", "true");

        switch (httpRequestType) {
            case GET_USER_DATA:
                params.put("getuserdata", "true");
                break;
            case LOGIN_EMAIL:
                aLoginDetail = (LoginDetail) object;
                params.put("login", "true");
                params.put("email", aLoginDetail.getEmail());
                params.put("password", aLoginDetail.getPassword());
                break;
            case LOGIN_FB:
                aLoginDetail = (LoginDetail) object;
                params.put("from_fb", "true");
                params.put("token", aLoginDetail.getFBToken());
                params.put("referrer", aLoginDetail.getReferrer());
                break;
            case LOGOUT:
                params.put("login", "true");
                params.put("registration_id", (String) object);
                break;
            case REGISTER_EMAIL:
                RegisterDetail aRegisterDetail = new RegisterDetail();
                aRegisterDetail = (RegisterDetail) object;
                params.put("from_fb", "0");
                params.put("fname", aRegisterDetail.getFirstName());
                params.put("lname", aRegisterDetail.getLastName());
                params.put("gender", aRegisterDetail.getGender());
                params.put("email", aRegisterDetail.getEmail());
                params.put("password", aRegisterDetail.getPassword());
                params.put("TM_S1_WhenBornDay", aRegisterDetail.getDob_d() + "");
                params.put("TM_S1_WhenBornMonth", aRegisterDetail.getDob_m() + "");
                params.put("TM_S1_WhenBornYear", aRegisterDetail.getDob_y() + "");
                params.put("registerViaEmail", "1");
                params.put("termAgree", "accepted");
                break;
            case SAVE_DATA_BASIC:
                params.put("save", "basics");
                break;
            case SAVE_DATA_PHOTO:
                params.put("save", "photo_upload_nudge_skipped_view");
                break;
            case SAVE_DATA_PSYCHO1:
                params.put("save", "psycho1");
                break;
            case SAVE_DATA_PSYCHO2:
                params.put("save", "psycho2");
                break;
            case FB_TRUSTBUILDER:
                TrustBuilderParamsFacebook tbParams = (TrustBuilderParamsFacebook) object;
                params.put("checkFacebook", "check");
                params.put("token", tbParams.getAccessToken());
                params.put("connected_from", tbParams.getConnectedFrom());
                if (tbParams.getFbReimport()) {
                    params.put("reimport", "reimport");
                }
                break;
            case FB_PHOTO_SELECT:
                params.put("data[]", (String) object);
                params.put("action", "saveFb");
                break;
            case PHOTO_DELETE:
                params.put("id", (String) object);
                params.put("action", "delete");
                break;
            case PHOTO_PROFILE_SET:
                params.put("id", (String) object);
                params.put("action", "set_as_profile");
                break;
            case FETCH_PSYCHO1_DATA:
                params.put("step", "psycho1");
                break;
            case FETCH_PSYCHO2_DATA:
                params.put("step", "psycho2");
                break;
            case FETCH_HOBBY_DATA:
                params.put("step", "hobby");
                break;
            case SAVE_DATA_HOBBY:
                params.put("data", object.toString());
                break;
            case MATCH:
                //params.put("login_mobile", "true");
                break;

            case MESSAGE_ONE_ONE_CONVERSATION:
                if (object != null) {
                    params.put("msg_id", (String) object);
                }
                break;
            case EDIT_PREF_SAVE:

                EditPrefSaveModal aEditPrefSaveModal = (EditPrefSaveModal) object;

                //params.put("login_mobile", "true");
                params.put("update", "true");
                params.put("lookingAgeStart", aEditPrefSaveModal.lookingAgeStart);
                params.put("lookingAgeEnd", aEditPrefSaveModal.lookingAgeEnd);
                params.put("marital_status_spouse",
                        aEditPrefSaveModal.marital_status_spouse);
                params.put("spouseCountry", aEditPrefSaveModal.spouseCountry);
                params.put("spouseState", aEditPrefSaveModal.spouseState);
                params.put("spouseCity", aEditPrefSaveModal.spouseCity);
                params.put("start_height", aEditPrefSaveModal.start_height);
                params.put("end_height", aEditPrefSaveModal.end_height);
                params.put("income", aEditPrefSaveModal.income);
                params.put("education_spouse", aEditPrefSaveModal.education_spouse);
                params.put("show_us_profiles", String.valueOf(aEditPrefSaveModal.show_us_profiles));


                break;

		case DISPLAY_PICS:
			params.put("action", "display_pics");
            params.put("video_profile_enabled", "true");
            params.put("device_name", Utility.getDeviceName());
            break;
		case SEND_REG_ID:
			if (object != null) {
				params.put("registration_id", (String) object);
			}
			break;
		case SUSPEND_PROFILE:
			params.put("suspend", "true");
			break;
		case VERIFY_PHOTOID_PASSWORD:
			params.put("action", "sendpassword");
			params.put("password", (String) object);
			params.put("type", "1");
			break;
		case VERIFY_PHOTOID_MISMATCH_SYNC:
			params.put("action", "syncNamewithID");
			params.put("update", (String) object);
			params.put("type", "1");
			break;
		case VERIFY_PHONE:
			params.put("action", "numberVerify");
			params.put("number", (String) object);
			break;
		case VERIFY_EMPLOYMENT_PASSWORD:
			params.put("action", "sendpassword");
			params.put("password", (String) object);
			params.put("type", "3");
			break;
		case POLL_PHONE:
			params.put("action", "checkNumber");
			break;
		case PROFILE_DEACTIVATE:
			params.put("reason", (String) object);
		//	params.put("login_mobile", "true");
			break;
		case ACTIVATE:
			params.put("activate", "true");
			break;
		case CONTACT_US:
			params.put("msg", (String)object);
			break;
		case GET_REG_IDS:
			params.put("get_registration_id", "true");
			break;
		case SPARKS_GET:
			params.put("action", "get_sparks");
			break;
		case SPARKS_UPDATE:
			SparkModal sparkModal = (SparkModal) object;
			params.put("action", "update_spark");
			params.put("sender_id", sparkModal.getmMatchId());
			params.put("hash", sparkModal.getmHashKey());
			params.put("spark_action", sparkModal.getmSparkAction().name());
			break;
		case SPARKS_SENT:
			SparkSendModal sparkSendModal = (SparkSendModal) object;
			params.put("action", "send_spark");
			params.put("receiver_id", sparkSendModal.getmReceiverId());
			params.put("unique_id", sparkSendModal.getmUniqueId());
			params.put("message", sparkSendModal.getmMessage());
			params.put("spark_hash", sparkSendModal.getmSparkHash());
            params.put("has_liked_before", String.valueOf(sparkSendModal.ismHasLikedBefore()));
            break;
		case SPARKS_RESET:
			params.put("action", "update_spark");
			params.put("spark_action", "reset_abra_ka_dabra");
			break;
		case GET_PACKAGES:
			params.put("action", "get_packages");
            if (object != null) {
                params.put("type", (String) object);
            }
            break;
		case GET_COMMONALITIES_SPARK:
			params.put("action", "get_commonalities");
            SparksHandler.Sender.CommonalitiesModal cModal = (SparksHandler.Sender.CommonalitiesModal) object;
            params.put("match_id", cModal.getmMatchId());
            if (Utility.isSet(cModal.getScenesId())) {
                params.put("datespot_id", cModal.getScenesId());
            }
            break;
		case PASSCODE:
			params.put("action", (String) object);
			break;
        case GET_PAYTM_PURORDER:
			params.put("action", "get_purorder");
			break;
            //Select
            case GET_QUIZ:
                params.put("action", "get_quiz");
                break;
            case SUBMIT_QUIZ:
                SelectQuizModal selectQuizModal = (SelectQuizModal) object;
                params.put("action", "submit_quiz");
                params.put("quiz_id", selectQuizModal.getmQuizId());
                params.put("answers", selectQuizModal.getAnswersAsJSONString());
                break;
            case COMPARE_QUIZ:
                params.put("action", "compare_quiz");
                params.put("match_id", (String) object);
                break;
            case SELECT_FREE_TRIAL:
                params.put("action", "start_free_trial");
                break;
            default:
                break;
        }
		return params;
	}
}
