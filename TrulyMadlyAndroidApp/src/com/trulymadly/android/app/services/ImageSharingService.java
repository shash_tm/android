package com.trulymadly.android.app.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;

import com.crashlytics.android.Crashlytics;
import com.squareup.otto.Subscribe;
import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.GetHttpRequestParams;
import com.trulymadly.android.app.MessageModal;
import com.trulymadly.android.app.MessageOneonOneConversionActivity;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.bus.BusProvider;
import com.trulymadly.android.app.bus.CancelEvent;
import com.trulymadly.android.app.bus.ImageUploadedEvent;
import com.trulymadly.android.app.bus.ImageUploadedProgress;
import com.trulymadly.android.app.bus.QueryEvent;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.sqlite.MessageDBHandler;
import com.trulymadly.android.app.utility.CountingFileRequestBody;
import com.trulymadly.android.app.utility.FilesHandler;
import com.trulymadly.android.app.utility.OkHttpHandler;
import com.trulymadly.android.app.utility.TimeUtils;
import com.trulymadly.android.app.utility.Utility;
import com.trulymadly.android.socket.SocketHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.HashMap;

import okhttp3.Call;
import okhttp3.Headers;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * helper methods.
 */
public class ImageSharingService extends IntentService {

    public static final int FORMAT_JPEG = 2;
    //Compression formats
    private static final int FORMAT_WEBP = 1;
    private static final int FORMAT_PNG = 3;

    //Actions to be performed
    private static final String ACTION_COMPRESS_AND_SAVE = "com.trulymadly.android.app.services.action.COMPRESS_AND_SAVE";

    //Parameters keys
    private static final String EXTRA_FORMAT = "com.trulymadly.android.app.services.extra.FORMAT";
    private static final String EXTRA_URI = "com.trulymadly.android.app.services.extra.URI";
    private static final String EXTRA_MESSAGE_ID = "com.trulymadly.android.app.services.extra.MESSAGE_ID";
    private static final String EXTRA_MATCH_ID = "com.trulymadly.android.app.services.extra.MATCH_ID";
    private static final String EXTRA_SOURCE = "com.trulymadly.android.app.services.extra.SOURCE";
    private static boolean isRunning = false;
    private boolean isTaskCompleted = false;
    private String mIdentifier, mMatchId;
    private CountingFileRequestBody mCountingFileRequestBody;
    private boolean isCancelled = false;
    private boolean isCompressed;
    private long mFileSize;
    private String mSource;
    private int mToRotate = 0;
    private long mTimeTaken;

    public ImageSharingService() {
        super("ImageSharingService");
    }

    /**
     * Starts this service to perform action COMPRESS_AND_SAVE with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @param context
     * @param format  - Target compression format
     * @param uri     - Source file uri
     * @see IntentService
     */
    public static void startActionCompressAndSave(Context context, int format, String uri,
                                                  String messageId, String matchId, String source) {
        Intent intent = new Intent(context, ImageSharingService.class);
        intent.setAction(ACTION_COMPRESS_AND_SAVE);
        intent.putExtra(EXTRA_FORMAT, format);
        intent.putExtra(EXTRA_URI, uri);
        intent.putExtra(EXTRA_MESSAGE_ID, messageId);
        intent.putExtra(EXTRA_MATCH_ID, matchId);
        intent.putExtra(EXTRA_SOURCE, source);
        context.startService(intent);
        isRunning = true;
    }

    public static boolean isRunning() {
        return isRunning;
    }

    public static int exifToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }

    private static int degreeToExif(int degrees) {
        if (degrees == 90) {
            return ExifInterface.ORIENTATION_ROTATE_90;
        } else if (degrees == 180) {
            return ExifInterface.ORIENTATION_ROTATE_180;
        } else if (degrees == 270) {
            return ExifInterface.ORIENTATION_ROTATE_270;
        }
        return ExifInterface.ORIENTATION_NORMAL;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        BusProvider.getInstance().register(this);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_COMPRESS_AND_SAVE.equals(action)) {
                final int format = intent.getIntExtra(EXTRA_FORMAT, -1);
                final String uri = intent.getStringExtra(EXTRA_URI);
                final String messageId = intent.getStringExtra(EXTRA_MESSAGE_ID);
                final String matchId = intent.getStringExtra(EXTRA_MATCH_ID);
                final String source = intent.getStringExtra(EXTRA_SOURCE);
                handleActionCompressAndSave(getCompressionFormatFromInt(format), uri, messageId, matchId, source);
            }
        }
    }

    private Bitmap.CompressFormat getCompressionFormatFromInt(int format) {
        switch (format) {
            case FORMAT_JPEG:
                return Bitmap.CompressFormat.JPEG;

            case FORMAT_WEBP:
                return Bitmap.CompressFormat.WEBP;

            case FORMAT_PNG:
                return Bitmap.CompressFormat.PNG;
        }

        return Bitmap.CompressFormat.WEBP;
    }

    private int getCompressionValueFromSize(long fileSize) {
        int compression = 50;
        if (fileSize > 500 * 1024L) {
            compression = 30;
        }

        return compression;
    }

    /**
     * Handle action COMPRESS_AND_SAVE in the provided background thread with the provided
     * parameters.
     */
    private void handleActionCompressAndSave(Bitmap.CompressFormat format, String uri,
                                             String messageId, String matchId, String source) {
        mIdentifier = messageId;
        mMatchId = matchId;
        File file = new File(uri);
        mFileSize = file.length();
        mSource = source;
        int outputCompression = 30;
        boolean isCompressionRequired = true;

        try {
            ExifInterface exifInterface = new ExifInterface(uri);
            int rotation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            mToRotate = exifToDegrees(rotation);
        } catch (IOException e) {
            Crashlytics.logException(e);
        }

        //Deciding the outputCompression value based on the file size (Calculated using file.length() -> Bytes)
        //This link can help
        //http://stackoverflow.com/questions/7131930/get-the-file-size-in-android-sdk
        if (file.exists()) {
            if (mFileSize <= 200 * 1024L) {
                isCompressionRequired = false;
            } else {
                isCompressionRequired = true;
                outputCompression = getCompressionValueFromSize(mFileSize);
            }
        }
        boolean compressed = false;
        if (isCompressionRequired) {
            File copy = FilesHandler.createFile(file.getParentFile().getName(), "temp_" + file.getName()
            );
            try {
                Bitmap bitmap = BitmapFactory.decodeFile(uri);
                OutputStream outputStream = null;
                outputStream = new FileOutputStream(copy.getPath());
                if (bitmap != null) {
                    compressed = bitmap.compress(format, outputCompression, outputStream);
                }
                //when image compression fails - loading the uncompressed image
                if (outputStream != null) {
                    outputStream.close();
                }
            } catch (IOException | OutOfMemoryError e) {
                Crashlytics.logException(e);
            }

            HashMap<String, String> eventInfo = new HashMap<>();
            eventInfo.put("original_size", String.valueOf(file.length()));
            eventInfo.put("match_id", mMatchId);
            if (Utility.isSet(mSource)) {
                eventInfo.put("source", mSource);
            }
            if (compressed) {
                try {
                    ExifInterface exifInterface = new ExifInterface(copy.getAbsolutePath());
                    exifInterface.setAttribute(ExifInterface.TAG_ORIENTATION, String.valueOf(degreeToExif(mToRotate)));
                    exifInterface.saveAttributes();
                } catch (IOException e) {
                    Crashlytics.logException(e);
                }
                eventInfo.put("compressed_size", String.valueOf(copy.length()));
                boolean deleted = file.delete();
                if (deleted) {
                    boolean renamed = copy.renameTo(file);
                    mFileSize = file.length();
                }
                //Trk
                TrulyMadlyTrackEvent.trackEvent(getApplicationContext(),
                        TrulyMadlyEvent.TrulyMadlyActivities.photo_share,
                        TrulyMadlyEvent.TrulyMadlyEventTypes.compress, 0,
                        TrulyMadlyEvent.TrulyMadlyEventStatus.success,
                        eventInfo, true, false);
                //GA
                TrulyMadlyTrackEvent.trackEvent(getApplicationContext(),
                        TrulyMadlyEvent.TrulyMadlyActivities.photo_share,
                        TrulyMadlyEvent.TrulyMadlyEventTypes.compress_success, 0,
                        source,
                        null, false, true, true);
            } else {
                eventInfo.put("model", android.os.Build.MODEL);
                copy.delete();
                //Trk
                TrulyMadlyTrackEvent.trackEvent(getApplicationContext(),
                        TrulyMadlyEvent.TrulyMadlyActivities.photo_share,
                        TrulyMadlyEvent.TrulyMadlyEventTypes.compress, 0,
                        TrulyMadlyEvent.TrulyMadlyEventStatus.fail,
                        eventInfo, true, false);
                //GA
                TrulyMadlyTrackEvent.trackEvent(getApplicationContext(),
                        TrulyMadlyEvent.TrulyMadlyActivities.photo_share,
                        TrulyMadlyEvent.TrulyMadlyEventTypes.compress_fail, 0,
                        source,
                        null, false, true, true);
            }
        }

        isCompressed = compressed;
        handleActionUpload(uri, messageId, matchId);
    }

    /**
     * Handle action UPLOAD in the provided background thread with the provided
     * parameters.
     */
    private void handleActionUpload(String uri, String messageId, String matchId) {
        File file = new File(uri);
        uploadImageNew(file, messageId, matchId);
    }

    private void renameToserverName(String localUri, String serverUrl) {
        //TODO : Rename this when backend is done with its changes
        String newName = serverUrl.substring(serverUrl.lastIndexOf("/") + 1);
        FilesHandler.renameFile(localUri, newName);
    }

    private void uploadImageNew(File file, String messageId, String matchId) {
        long currentTimeStamp = TimeUtils.getSystemTimeInMiliSeconds();
        ImageUploadedEvent imageUploadedEvent = new ImageUploadedEvent();

        if (isCancelled()) {
            return;
        }
        MultipartBody.Builder multipartBuilder = new MultipartBody.Builder().setType(MultipartBody.FORM);

        String url = ConstantsUrls.get_photos_share_url();
        multipartBuilder.addFormDataPart("action", Constants.photos_share_action);
        multipartBuilder.addFormDataPart("match_id", matchId);
        multipartBuilder.addFormDataPart("rotate", String.valueOf(mToRotate));
        multipartBuilder.addFormDataPart("file_name", file.getName());

        mCountingFileRequestBody = new CountingFileRequestBody(getApplicationContext(), file, "image/jpg", messageId);
        multipartBuilder.addFormDataPart("file", file.getName(),
                mCountingFileRequestBody);

        RequestBody requestBody = multipartBuilder.build();

        Headers headers = Headers.of(GetHttpRequestParams.getHeaders(getApplicationContext()));
        Request request = new Request.Builder()
                .url(url)
                .headers(headers)
                .post(requestBody)
                .build();


        OkHttpHandler.NET_TIMEOUT timeoutType = OkHttpHandler.NET_TIMEOUT.NORMAL;

        OkHttpClient.Builder builder = OkHttpHandler.getOkHttpBuilder(timeoutType.value(), timeoutType.value(), timeoutType.value());
        OkHttpClient client = builder.build();

        Call mUploadCall = client.newCall(request);
        String uploadFailReason = null;
        int htmlCode = 0;
        try {
            if (isCancelled()) {
                return;
            }
            Response response = mUploadCall.execute();
            long timeStampAfterComplete = TimeUtils.getSystemTimeInMiliSeconds();
            mTimeTaken = timeStampAfterComplete - currentTimeStamp;

            String str = response.body().string();
            htmlCode = response.code();
            JSONObject responseJSON = new JSONObject(str);
            if (isCancelled()) {
                return;
            }
            if (responseJSON != null) {
                int responseCode = responseJSON.optInt("responseCode", -1);
                switch (responseCode) {
                    case 200:
                        JSONObject urls = responseJSON.optJSONObject("urls");
                        if (urls != null) {
                            imageUploadedEvent.setmStatus(true);
                            responseJSON.remove("response_code");
                            String image_url = (Utility.isWebPSupported() && urls.has(MessageModal.WEBP_URL_KEY)) ?
                                    urls.optString(MessageModal.WEBP_URL_KEY)
                                    : urls.optString(MessageModal.JPEG_URL_KEY);
                            renameToserverName(file.getAbsolutePath(), image_url);
                            imageUploadedEvent.setMessageJson(MessageModal.prepareImageMetadataJsonString(
                                    urls.optString(MessageModal.JPEG_URL_KEY),
                                    urls.optString(MessageModal.JPEG_SIZE_KEY),
                                    urls.optString(MessageModal.WEBP_URL_KEY),
                                    urls.optString(MessageModal.WEBP_SIZE_KEY),
                                    file.getAbsolutePath(),
                                    urls.optString(MessageModal.THUMBNAIL_JPEG_KEY),
                                    urls.optString(MessageModal.THUMBNAIL_WEBP_KEY)
                            ));
                        } else {
                            imageUploadedEvent.setmStatus(false);
                            imageUploadedEvent.setmMessage(getApplicationContext()
                                    .getString(R.string.image_upload_failed));
                        }
                        break;

                    default:
                        uploadFailReason = "ResponseCode : " + responseCode;
                        imageUploadedEvent.setmStatus(false);
                        imageUploadedEvent.setmMessage(getApplicationContext()
                                .getString(R.string.image_upload_failed));
                        break;
                }
            } else {
                uploadFailReason = "HTMLResponseCode : " + htmlCode;
                imageUploadedEvent.setmStatus(false);
                imageUploadedEvent.setmMessage(getApplicationContext()
                        .getString(R.string.image_upload_failed));
            }
        } catch (JSONException | OutOfMemoryError e) {
            uploadFailReason = "HTMLResponseCode : " + htmlCode + " :: ";
            uploadFailReason += e.getMessage();
            Crashlytics.logException(e);
            imageUploadedEvent.setmStatus(false);
            imageUploadedEvent.setmMessage(getApplicationContext()
                    .getString(R.string.image_upload_failed));
        } catch (IOException e) {
            uploadFailReason = "HTMLResponseCode : " + htmlCode + " :: ";
            uploadFailReason += e.getMessage();
            Crashlytics.logException(e);
            imageUploadedEvent.setmStatus(false);
            if (e instanceof SocketTimeoutException || e instanceof ConnectException) {
                imageUploadedEvent.setmMessage(getApplicationContext().getString(R.string.ERROR_NETWORK_FAILURE));
            } else {
                imageUploadedEvent.setmMessage(getApplicationContext()
                        .getString(R.string.image_upload_failed));
            }
        }

        if (isCancelled()) {
            return;
        }

        imageUploadedEvent.setmMessageId(messageId);
        HashMap<String, String> eventInfo = new HashMap<>();
        eventInfo.put("is_compressed", String.valueOf(isCompressed));
        eventInfo.put("size", String.valueOf(mFileSize));
        eventInfo.put("match_id", mMatchId);
        if (Utility.isSet(mSource)) {
            eventInfo.put("source", mSource);
        }
        if (Utility.isSet(uploadFailReason)) {
            eventInfo.put("reason", uploadFailReason);
        }
        String status = imageUploadedEvent.ismStatus() ? TrulyMadlyEvent.TrulyMadlyEventStatus.success :
                TrulyMadlyEvent.TrulyMadlyEventStatus.fail;
        String type_GA = imageUploadedEvent.ismStatus() ? TrulyMadlyEvent.TrulyMadlyEventTypes.upload_success :
                TrulyMadlyEvent.TrulyMadlyEventTypes.upload_fail;
        //TRK:
        TrulyMadlyTrackEvent.trackEvent(getApplicationContext(),
                TrulyMadlyEvent.TrulyMadlyActivities.photo_share,
                TrulyMadlyEvent.TrulyMadlyEventTypes.upload, mTimeTaken,
                status,
                eventInfo, true, false);
        //GA:
        TrulyMadlyTrackEvent.trackEvent(getApplicationContext(),
                TrulyMadlyEvent.TrulyMadlyActivities.photo_share,
                type_GA, mTimeTaken,
                String.valueOf(isCompressed),
                null, false, true, true);

        updateMessage(imageUploadedEvent);
        isTaskCompleted = true;
    }

    @Subscribe
    public void onQueryReceived(QueryEvent queryEvent) {
        if (queryEvent == null ||
                (Utility.isSet(mIdentifier) && !mIdentifier.equals(queryEvent.getmIdentifier()))
                || mCountingFileRequestBody == null) {
            return;
        }

        Utility.fireBusEvent(getApplicationContext(), true,
                new ImageUploadedProgress(mIdentifier, mCountingFileRequestBody.getProgress()));
    }

    @Subscribe
    public void onCancelEventReceived(CancelEvent cancelEvent) {
        if (mIdentifier.equals(cancelEvent.getmIdentifier()) && mMatchId.equals(cancelEvent.getmMatchId())) {
            isCancelled = true;
            if (mCountingFileRequestBody != null) {
                mCountingFileRequestBody.cancelUpload();
            }

            ImageUploadedEvent imageUploadedEvent = new ImageUploadedEvent();
            imageUploadedEvent.setmStatus(false);
            imageUploadedEvent.setmMessageId(mIdentifier);
            HashMap<String, String> eventInfo = new HashMap<>();
            eventInfo.put("is_compressed", String.valueOf(isCompressed));
            eventInfo.put("size", String.valueOf(mFileSize));
            eventInfo.put("match_id", mMatchId);
            if (Utility.isSet(mSource)) {
                eventInfo.put("source", mSource);
            }
            String status = TrulyMadlyEvent.TrulyMadlyEventStatus.cancel;

            //Trk
            TrulyMadlyTrackEvent.trackEvent(getApplicationContext(),
                    TrulyMadlyEvent.TrulyMadlyActivities.photo_share,
                    TrulyMadlyEvent.TrulyMadlyEventTypes.upload, 0,
                    status,
                    eventInfo, true, false);

            //Ga
            TrulyMadlyTrackEvent.trackEvent(getApplicationContext(),
                    TrulyMadlyEvent.TrulyMadlyActivities.photo_share,
                    TrulyMadlyEvent.TrulyMadlyEventTypes.upload_cancel, 0,
                    String.valueOf(isCompressed),
                    null, false, true, true);

            updateMessage(imageUploadedEvent);
            isRunning = false;
            stopSelf();
        }
    }

    private boolean isCancelled() {
        return isCancelled;
    }

    private void updateMessage(ImageUploadedEvent imageUploadedEvent) {
        Context context = getApplicationContext();
        String userId = Utility.getMyId(getApplicationContext());
        if (!imageUploadedEvent.ismStatus()) {
            MessageDBHandler.changeMessageState(userId, mMatchId, context,
                    mIdentifier, Constants.MessageState.OUTGOING_FAILED);
            Utility.fireBusEvent(getApplicationContext(), true, imageUploadedEvent);
        } else {
            MessageModal messageModal = MessageDBHandler.getAMessage(userId,
                    imageUploadedEvent.getmMessageId(), context);
            if (messageModal == null) {
                return;
            }
            messageModal.setMsgJsonString(imageUploadedEvent.getMessageJson());
            MessageDBHandler.updateAMessage(context, userId, messageModal,
                    messageModal.getMsg_id(), Constants.FetchSource.LOCAL);
            //Chat active
            if (MessageOneonOneConversionActivity.isChatActiveForId(context, mMatchId)) {
                Utility.fireBusEvent(getApplicationContext(), true, imageUploadedEvent);
            }
            //Chat not active
            else if (SocketHandler.isSocketEnabled(context)) {
                //TODO: SOCKET: Decide what to do here
                MessageDBHandler.changeMessageState(userId, mMatchId, context,
                        mIdentifier, Constants.MessageState.OUTGOING_FAILED);
//                Utility.fireServiceBusEvent(context,
//                        new ServiceEvent(ServiceEvent.SERVICE_EVENT_TYPE.EMIT,
//                                ConstantsSocket.SOCKET_EMITS.chat_sent, paramsSocket, ack, true,
//                                1));
            }
            //Socket not enabled
            else {
                MessageDBHandler.changeMessageState(userId, mMatchId, context,
                        mIdentifier, Constants.MessageState.OUTGOING_FAILED);
            }
        }
    }

    @Override
    public void onDestroy() {
        BusProvider.getInstance().unregister(this);
        super.onDestroy();
        Context context = getApplicationContext();
        //Uploading failed
        if (!isTaskCompleted) {
            String userId = Utility.getMyId(getApplicationContext());
            MessageDBHandler.changeMessageState(userId, mMatchId, context,
                    mIdentifier, Constants.MessageState.OUTGOING_FAILED);
        }
        isRunning = false;
    }
}
