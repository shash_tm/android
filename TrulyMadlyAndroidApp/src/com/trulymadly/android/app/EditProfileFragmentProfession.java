/**
 * 
 */
package com.trulymadly.android.app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.InflateException;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.crashlytics.android.Crashlytics;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.adapter.CustomSpinnerArrayAdapter;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.Constants.EditProfilePageType;
import com.trulymadly.android.app.listener.ConfirmDialogInterface;
import com.trulymadly.android.app.modal.EditPrefBasicDataModal;
import com.trulymadly.android.app.modal.Income;
import com.trulymadly.android.app.modal.UserData;
import com.trulymadly.android.app.sqlite.EditPrefDBHandler;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.BadWordsHandler;
import com.trulymadly.android.app.utility.MoEHandler;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;

import org.apmem.tools.layouts.FlowLayout;

import java.util.ArrayList;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author udbhav
 * 
 */
public class EditProfileFragmentProfession extends Fragment
		implements OnItemSelectedListener, OnItemClickListener, OnFocusChangeListener {

	private static final EditProfilePageType currentPageType = EditProfilePageType.PROFESSION;
	private final ArrayList<String> linkedinColleges = null;
	private final String companyTextviewTag = "company_textview";
	private OnClickListener onClickListener = null;
	private EditProfileActionInterface editProfileActionInterface;
	private Button continueButton;
	private EditText jobTitleAutoCompleteTextView;
	private EditText companyName;
	private FlowLayout companiesContainer;
	private String currentTitle = null;
	private ArrayList<String> companies = null;
	private View view, addCompanyButton;
	private ArrayList<TextView> allCompaniesTextViews = null;
	private long startTime;
	private Spinner incomeSpinner;
	private String income = null;
	private ArrayList<EditPrefBasicDataModal> incomeList;
	private ProgressDialog mProgressDialog;
	private Context aContext;
	private Activity aActivity;
	private UserData userData;
	private ArrayList<String> badWordList;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			editProfileActionInterface = (EditProfileActivity) activity;
		} catch (ClassCastException e) {
			Crashlytics.logException(e);
		}
	}

	private void setStartTime() {
		startTime = (new java.util.Date()).getTime();
	}

	private void setEndTime() {
		long endTime = (new java.util.Date()).getTime();
		long time_diff = endTime - startTime;
		TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyActivities.edit_profile, TrulyMadlyEventTypes.register_work,
				time_diff, null, null, true);

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle data = getArguments();
		currentTitle = data.getString("current_title");
		companies = data.getStringArrayList("companies");
		income = data.getString("income");
		userData = (UserData) data.getSerializable("userData");
		aContext = getActivity();
		aActivity = getActivity();

//		OnBadWordListRefreshedInterface onBadWordListUpdate = new OnBadWordListRefreshedInterface() {
//			@Override
//			public void onRefresh(ArrayList<String> badWordArrayList) {
//				badWordList = badWordArrayList;
//			}
//		};
		badWordList = BadWordsHandler.getBadWordList(aContext);

		incomeList = EditPrefDBHandler.getBasicDataFromDB(Constants.EDIT_PREF_INCOME, aContext, 0);
		MoEHandler.trackEvent(aContext, MoEHandler.Events.PROFESSION_SCREEN_LAUNCHED);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		Resources aResources = getResources();
		editProfileActionInterface.setHeaderText(aResources.getString(R.string.header_profession));

		try {
			view = inflater.inflate(R.layout.edit_profile_profession, container, false);
		} catch (NotFoundException e) {
			aActivity.finish();
			return view;
		} catch (InflateException e) {
			aActivity.finish();
			return view;
		}

		continueButton = (Button) view.findViewById(R.id.continue_profession);
		continueButton.setText(R.string.save);

		addCompanyButton = view.findViewById(R.id.edit_profession_add_company);

		jobTitleAutoCompleteTextView = (EditText) view.findViewById(R.id.autocomplete_job_title);
		TextWatcher jobTitleTextWatcher = new TextWatcher() {
			private String lastString = "";

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (s.toString().trim().equals(lastString)) {
					return;
				}
				lastString = s.toString().trim();
				if (BadWordsHandler.hasBadWord(aActivity, s.toString(), badWordList, R.string.bad_word_text_occupation, jobTitleAutoCompleteTextView)) {

				} else if (s.toString().trim().length() > 30) {
					UiUtils.hideKeyBoard(aActivity);
					AlertsHandler.showMessage(aActivity, R.string.max_char_exceeded, true);
					jobTitleAutoCompleteTextView.setText(s.subSequence(0, 30));
					validateProfessionData(false);
				} else {
					validateProfessionData(false);
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {

			}
		};
		setCurrentTitle(currentTitle);

		companyName = (EditText) view.findViewById(R.id.company_textview);
		TextWatcher companyNameTextWatcher = new TextWatcher() {
			private String lastString = "";

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (s.toString().trim().equals(lastString)) {
					return;
				}
				lastString = s.toString().trim();
				if (s.toString().trim().length() == 0) {
					addCompanyButton.setVisibility(View.INVISIBLE);
				} else if (BadWordsHandler.hasBadWord(aActivity, s.toString(), badWordList, R.string.bad_word_text_occupation, companyName)) {

				} else {
					addCompanyButton.setVisibility(View.VISIBLE);
					addCompanyButton.invalidate();
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {

			}
		};

		onClickListener = new OnClickListener() {

			@Override
			public void onClick(View v) {
				onClicked(v);

			}
		};

		companiesContainer = (FlowLayout) view.findViewById(R.id.companies_container);
		allCompaniesTextViews = new ArrayList<>();
		if (companies == null) {
			companies = new ArrayList<>();
		} else {
			for (int i = 0; i < companies.size(); i++) {
				displayNewCompany(companies.get(i));
			}
		}

		incomeSpinner = (Spinner) view.findViewById(R.id.income_spinner);

		createIncomeSpinner(Income.getIncomes(incomeList, aResources));

		continueButton.setOnClickListener(onClickListener);
		addCompanyButton.setOnClickListener(onClickListener);

		// jobTitleAutoCompleteTextView.setOnItemClickListener(this);
		incomeSpinner.setOnItemSelectedListener(this);

		companyName.addTextChangedListener(companyNameTextWatcher);
		companyName.setOnFocusChangeListener(this);
		jobTitleAutoCompleteTextView.setOnFocusChangeListener(this);
		jobTitleAutoCompleteTextView.addTextChangedListener(jobTitleTextWatcher);

		((EditText) view.findViewById(R.id.company_textview)).setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))
						|| (actionId == EditorInfo.IME_ACTION_DONE)) {
					addNewCompany(companyName.getText().toString(), true);
					validateProfessionData(false);
				}
				return false;
			}
		});

		jobTitleAutoCompleteTextView.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))
						|| (actionId == EditorInfo.IME_ACTION_NEXT)) {
					setCurrentTitleUI();
				}
				return false;
			}
		});

		setWorkingStatus();

		validateProfessionData(false);

		setStartTime();

		return view;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		editProfileActionInterface.onViewCreated();
	}

	private void onClicked(final View v) {
		int id = v.getId();
		Object tag = v.getTag();

		switch (id) {
		case R.id.continue_profession:
			addNewCompany(companyName.getText().toString(), true);
			if (validateProfessionData(false)) {
				Bundle responseData = new Bundle();
				responseData.putString("current_title", currentTitle);
				responseData.putStringArrayList("companies", companies);
				responseData.putStringArrayList("linkedinColleges", linkedinColleges);
				responseData.putString("income", income);
				responseData.putSerializable("userData", userData);

				setEndTime();
				editProfileActionInterface.processFragmentSaved(currentPageType, responseData);
			}
			break;
		case R.id.edit_profession_add_company:
			addNewCompany(companyName.getText().toString(), true);
			validateProfessionData(false);
			break;
		default:
			break;
		}
		if (tag != null && ((String) tag).equalsIgnoreCase(companyTextviewTag)) {
			final String companyName = ((TextView) v).getText().toString();
			//i18n
			AlertsHandler.showConfirmDialog(aContext, "Are you sure you want to remove " + companyName + " from the list?",
                    R.string.remove, R.string.cancel, new ConfirmDialogInterface() {

                        @Override
                        public void onPositiveButtonSelected() {
                            removeCompany(companyName, v);
                        }

                        @Override
                        public void onNegativeButtonSelected() {
                        }
                    }, false);

		}

	}

	private void removeCompany(String companyName, View companyTv) {
		if (companies != null) {
			for (int i = 0; i < companies.size(); i++) {
				if (companies.get(i).equalsIgnoreCase(companyName)) {
					companies.remove(i);
					break;
				}
			}
		}
		if (companyTv != null && companyTv.getParent() != null) {
			((ViewGroup) companyTv.getParent()).removeView(companyTv);
		}
		//noinspection SuspiciousMethodCalls
		allCompaniesTextViews.remove(companyTv);
		validateProfessionData(false);
	}

	private void addNewCompany(String newCompanyName, Boolean showError) {
		if (newCompanyName == null) {
			return;
		}
		newCompanyName = newCompanyName.trim();
		if (newCompanyName.length() > 0) {
			hideKeyboard();
			if(BadWordsHandler.hasBadWord(aActivity, newCompanyName, badWordList, 0, null)) {
				return;
			}
			if (isCompanyAdded(newCompanyName)) {
				if (showError) {
					AlertsHandler.showMessage(aActivity, R.string.company_already_added);
				}
			} else {
				companies.add(newCompanyName);
				displayNewCompany(newCompanyName);
				addCompanyButton.setVisibility(View.GONE);
				validateProfessionData(false);
			}
		}
	}

	private void hideKeyboard() {

		InputMethodManager imm = (InputMethodManager) aActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(continueButton.getWindowToken(), 0);

	}

	private void displayNewCompany(String newCompanyName) {

		TextView tv = (TextView) LayoutInflater.from(aContext).inflate(R.layout.custom_item_1, companiesContainer,
				false);

		tv.setTag(companyTextviewTag);
		tv.setText(newCompanyName);
		tv.setOnClickListener(onClickListener);

		allCompaniesTextViews.add(tv);

		companiesContainer.addView(tv);
		companyName.setText("");
	}

	private boolean isCompanyAdded(String newCompanyName) {
		for (String companyName : companies) {
			if (Utility.stringCompare(checkNotNull(companyName), newCompanyName)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View v, int pos, long Id) {
		switch (parent.getId()) {
		case R.id.income_spinner:
			income = ((Income) incomeSpinner.getSelectedItem()).getId();
			break;
		default:
			break;
		}
		validateProfessionData(false);
	}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		switch (v.getId()) {
		case R.id.autocomplete_job_title:

			if (!hasFocus) {
				hideKeyboard();
			}
			break;
		case R.id.company_textview:
			if (!hasFocus) {
				hideKeyboard();

			}
			break;

		default:
			break;
		}

	}

	private void setWorkingStatus() {
		jobTitleAutoCompleteTextView.setEnabled(true);
		jobTitleAutoCompleteTextView.setHint(R.string.enter_designation);
	}

	private Boolean validateProfessionData(boolean showError) {
		Boolean isValid = true;
		int errorMessage = 0;
		currentTitle = jobTitleAutoCompleteTextView.getText().toString().trim();
		if (currentTitle == null || currentTitle.isEmpty()) {
			isValid = false;
			errorMessage = R.string.please_select_current_title;
		}
		if (!isValid && showError) {
			AlertsHandler.showMessage(aActivity, errorMessage);
		}
		continueButton.setEnabled(isValid);
		if (companies.size() > 0) {
			companyName.setHint(R.string.add_more_companies);
			companiesContainer.setVisibility(View.VISIBLE);
		} else {
			companyName.setHint(R.string.enter_most_recent_companies_first);
			companiesContainer.setVisibility(View.GONE);
		}
		if (Utility.isSet(income) && income.equalsIgnoreCase("0")) {
			income = null;
		}
		return isValid;
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View v, int pos, long arg3) {
		setCurrentTitleUI();
	}

	private void createIncomeSpinner(Income[] incomes) {
		CustomSpinnerArrayAdapter spinnerIncomeAdapter = new CustomSpinnerArrayAdapter(getActivity(),
				R.layout.custom_spinner_2, incomes);
		spinnerIncomeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		incomeSpinner.setAdapter(spinnerIncomeAdapter);

		if (income != null) {
			for (int i = 0; i < incomes.length; i++) {
				if (incomes[i].getId().equalsIgnoreCase(income)) {
					if (spinnerIncomeAdapter.getCount() > i)
						incomeSpinner.setSelection(i);
					break;
				}
			}
		}
	}

	private void setCurrentTitleUI() {
		setCurrentTitle(jobTitleAutoCompleteTextView.getText().toString());
		validateProfessionData(false);
		// hideKeyboard();
		companyName.requestFocus();
	}

	private void setCurrentTitle(String title) {
		if (title != null && !title.isEmpty()) {
			currentTitle = title;
			jobTitleAutoCompleteTextView.setText(currentTitle);
		} else {
			currentTitle = null;
			jobTitleAutoCompleteTextView.setText("");
		}
	}

	public void hideLoaders() {
		mProgressDialog = UiUtils.hideProgressBar(mProgressDialog);
	}

}
