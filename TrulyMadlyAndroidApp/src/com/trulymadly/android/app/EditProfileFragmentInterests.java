/**
 *
 */
package com.trulymadly.android.app;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.moe.pushlibrary.MoEHelper;
import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.adapter.HashtagAutoCompleteAdapter;
import com.trulymadly.android.app.json.Constants.EditProfilePageType;
import com.trulymadly.android.app.listener.OnHashtagSelected;
import com.trulymadly.android.app.modal.HashtagModal;
import com.trulymadly.android.app.modal.UserData;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.MoEHandler;
import com.trulymadly.android.app.utility.RandomHashtagGenerator;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;

import org.apmem.tools.layouts.FlowLayout;

import java.util.ArrayList;

/**
 * @author udbhav
 */
public class EditProfileFragmentInterests extends Fragment implements OnClickListener {

    private static final EditProfilePageType currentPageType = EditProfilePageType.INTERESTS;
    private static final long ANIMATION_DURATION_DOWNSCALE = 500, ANIMATION_DURATION_UPSCALE = 500;
    private final int MAX_INTERESTS = 5;
    private final int MIN_INTERESTS = 3;
    protected String TAG = "Edit Profile Fragment Interests";
    private EditProfileActionInterface editProfileActionInterface;
    private Button continueButton;
    private ArrayList<String> interests;
    private Context aContext;
    private Activity aActivity;
    private FlowLayout interestsContainer;
    private OnClickListener deletehashatagListener;
    private long startTime;
    private UserData userData;
    private AutoCompleteTextView hashtagAutoCompleteTextView;
    private OnHashtagSelected hashtagSelectedListener;
    private View hashtag_container;
    private MoEHelper mhelper = null;
    private View hashtag_sub_container;
    private ValueAnimator upAnimator, downAnimator;
    private ArrayList<HashtagModal> randomHashTags;
    private ImageView helpIcon;
    private boolean helpTrayVisible = false;
    private View random_hashtag_layout;
    private CompoundButton.OnCheckedChangeListener onRandomHashtagCheckChanged;
    private FlowLayout randomInterestContainer;
    private HashtagAutoCompleteAdapter hashtagAutoCompleteAdapter;

    public boolean isHelpTrayVisible() {
        return helpTrayVisible;
    }

    public void setHelpTrayVisible(boolean helpTrayVisible) {
        this.helpTrayVisible = helpTrayVisible;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            editProfileActionInterface = (EditProfileActivity) activity;
        } catch (ClassCastException e) {
            Crashlytics.logException(e);
        }
    }

    public void hideHelpTray() {
        if (helpTrayVisible) {
            helpTrayVisible = false;
            random_hashtag_layout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle data = getArguments();
        if (interests == null) {
            interests = data.getStringArrayList("interests");
        }
        userData = (UserData) data.getSerializable("userData");


        RandomHashtagGenerator randomHashtagGenerator = new RandomHashtagGenerator();
        randomHashtagGenerator.setUserHashtags(interests);

        randomHashTags = new ArrayList<>();
        ArrayList<String> ranHashtags = randomHashtagGenerator.getRandomHashtags();
        for (int i = 0; i < ranHashtags.size(); i++) {
            HashtagModal hashtagModal = new HashtagModal();
            hashtagModal.setSelected(false);
            hashtagModal.setName(ranHashtags.get(i));
            randomHashTags.add(hashtagModal);
        }

        aContext = getActivity();
        aActivity = getActivity();
        LayoutInflater inflater = (LayoutInflater) aActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        MoEHandler.trackEvent(aContext, MoEHandler.Events.INTERESTS_SCREEN_LAUNCHED);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Resources aResources = getResources();
        editProfileActionInterface.setHeaderText(aResources.getString(R.string.header_interests_edit));
        View view = inflater.inflate(R.layout.edit_profile_interests_new, container, false);

        continueButton = (Button) view.findViewById(R.id.continue_interests_new);


        interestsContainer = (FlowLayout) view.findViewById(R.id.interests_container);


        hashtag_sub_container = view.findViewById(R.id.hashtag_sub_container);
        hashtag_container = view.findViewById(R.id.hashtag_container);
        randomInterestContainer = (FlowLayout) view.findViewById(R.id.random_hashtag_container);
        random_hashtag_layout = view.findViewById(R.id.random_hashtag_layout);
        hashtag_sub_container.setOnClickListener(null);
        random_hashtag_layout.setVisibility(View.GONE);

        final RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) hashtag_sub_container.getLayoutParams();
        upAnimator = ValueAnimator.ofInt(UiUtils.dpToPx(40), UiUtils.dpToPx(0));
        upAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                params.topMargin = (Integer) valueAnimator.getAnimatedValue();
                hashtag_sub_container.requestLayout();

            }
        });
        downAnimator = ValueAnimator.ofInt(UiUtils.dpToPx(0), UiUtils.dpToPx(40));
        downAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                params.topMargin = (Integer) valueAnimator.getAnimatedValue();
                hashtag_sub_container.requestLayout();

            }
        });

        upAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                hashtag_container.setVisibility(View.INVISIBLE);

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                AlertsHandler.showMessage(aActivity, getActivity().getResources().getString(R.string.five_hashtags), false);
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });


        downAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                hashtag_container.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationCancel(Animator animator) {
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
            }
        });
        upAnimator.setDuration(ANIMATION_DURATION_UPSCALE);
        downAnimator.setDuration(ANIMATION_DURATION_UPSCALE);


        deletehashatagListener = new OnClickListener() {
            @Override
            public void onClick(View view) {
                interests.remove((int) view.getTag());
                deleteInterest();

            }
        }
        ;


        hashtagAutoCompleteTextView = (AutoCompleteTextView) view
                .findViewById(R.id.custom_hashtag);

        hashtagAutoCompleteTextView.setDropDownHorizontalOffset(UiUtils
                .dpToPx(-1));
        hashtagAutoCompleteTextView.setDropDownVerticalOffset(UiUtils
                .dpToPx(11));
        hashtagAutoCompleteTextView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                hideHelpTray();
            }
        });

        helpIcon = (ImageView) view
                .findViewById(R.id.hashtag_help_icon);
        helpIcon.setOnClickListener(this);

        hashtagSelectedListener = new OnHashtagSelected() {

            @Override
            public void onHashtagSelected(String s) {

                if (s == null || s.trim().length() == 0) {
                    hashtagAutoCompleteTextView.setText("");
                    hashtagAutoCompleteTextView.dismissDropDown();
                } else {
                    String output;
                    if (s.charAt(0) != '#') {
                        output = "#" + s.trim();
                    } else {
                        output = s.trim();
                    }
                    if (!interests.contains(output)) {
                        if (interests.size() < MAX_INTERESTS) {
                            interests.add(output);
                            TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyEvent.TrulyMadlyActivities.edit_profile,
                                    TrulyMadlyEventTypes.hashtag_selected, 0, output, null, true);
                            displayNewInterest(output, interests.size() - 1);
                            hashtagAutoCompleteTextView.setText("");
                            validateInterestData(true);
                            userData.setInterests(interests);


                            if (interests.size() >= MIN_INTERESTS) {
                                UiUtils.hideKeyBoard(aContext);
                            }
                            if (interests.size() == MAX_INTERESTS) {
                                hideHelpTray();
                                upAnimator.start();
                            }


                        } else {
                            hideHelpTray();
                            //i18n
                            UiUtils.hideKeyBoard(aContext);
                            AlertsHandler.showMessage(aActivity,
                                    "Sorry. You can only select "
                                            + MAX_INTERESTS + " hastags.");
                        }
                    } else {
                        hideHelpTray();
                        hashtagAutoCompleteTextView.setText("");
                        UiUtils.hideKeyBoard(aContext);
                        AlertsHandler.showMessage(aActivity,
                                "Already selected");
                    }

                }

            }
        };

        hashtagAutoCompleteTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                helpIcon.setVisibility(charSequence.length() == 0 ? View.VISIBLE : View.GONE);
                hideHelpTray();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        hashtagAutoCompleteAdapter = new HashtagAutoCompleteAdapter(
                aActivity, R.layout.hashtag_autocomplete_item,
                R.id.hashtag_autocomplete_item, hashtagSelectedListener);
        hashtagAutoCompleteTextView.setThreshold(1);
        hashtagAutoCompleteTextView.setAdapter(hashtagAutoCompleteAdapter);
        hashtagAutoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                hashtagSelectedListener.onHashtagSelected(parent.getAdapter().getItem(position).toString());
            }
        });


        hashtagAutoCompleteTextView.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_ENTER || keyCode == KeyEvent.KEYCODE_SPACE) {
                        if (Utility.isSet(hashtagAutoCompleteTextView.getText().toString()) && hashtagAutoCompleteAdapter.getCount() > 0) {
                            hashtagSelectedListener.onHashtagSelected(hashtagAutoCompleteAdapter.getItem(0));
                        } else {
                            hashtagSelectedListener.onHashtagSelected(null);
                        }
                        return true;
                    }
                }
                return false;
            }
        });

        onRandomHashtagCheckChanged = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                HashtagModal hashtagModal = (HashtagModal) compoundButton.getTag();
                if (isChecked) {
                    hashtagSelectedListener.onHashtagSelected(hashtagModal.getName());

                } else {
                    int index = interests.indexOf(hashtagModal.getName());
                    if (index != -1) {
                        interests.remove(index);
                        deleteInterest();
                    }
                }
                hideHelpTray();
            }
        };


        parseInterests();
        parseRandomHastags();

        if (interests == null) {
            interests = new ArrayList<>();
        }

        continueButton.setOnClickListener(this);

        if (interests.size() == MAX_INTERESTS) {
            hashtag_container.setVisibility(View.GONE);
            params.topMargin = 0;
            hashtag_sub_container.requestLayout();
            AlertsHandler.showMessage(aActivity, getActivity().getResources().getString(R.string.five_hashtags));

        }
        setStartTime();
        validateInterestData(false);

        return view;

    }

    private void deleteInterest() {
        interestsContainer.removeAllViews();
        parseInterests();
        validateInterestData(true);
        if (interests.size() == MAX_INTERESTS - 1) {
            downAnimator.start();

        }

        for (HashtagModal hashtagModal : randomHashTags) {
            if (!interests.contains(hashtagModal.getName()))
                hashtagModal.setSelected(false);
            else
                hashtagModal.setSelected(true);
        }
        parseRandomHastags();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        editProfileActionInterface.onViewCreated();
    }

    private void setStartTime() {
        startTime = (new java.util.Date()).getTime();
    }

    private void setEndTime() {
        long endTime = (new java.util.Date()).getTime();
        int time_diff = (int) (endTime - startTime) / 1000;
        TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyActivities.edit_profile,
                TrulyMadlyEventTypes.register_interest, time_diff, null, null, true);

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.continue_interests_new:
                if (validateInterestData(false)) {
                    Bundle responseData = new Bundle();
                    responseData.putStringArrayList("interests", interests);
                    responseData.putSerializable("userData", userData);
                    setEndTime();
                    editProfileActionInterface.processFragmentSaved(currentPageType, responseData);
                }
                break;
            case R.id.hashtag_help_icon:
                random_hashtag_layout.setVisibility(helpTrayVisible ? View.GONE : View.VISIBLE);

//                hashtag_sub_container.setVisibility(helpTrayVisible?View.VISIBLE:View.GONE);
                helpTrayVisible = !helpTrayVisible;
                if (helpTrayVisible)
                    UiUtils.hideKeyBoard(aContext);
            default:
                break;
        }
    }

    private Boolean validateInterestData(boolean showError) {
        Boolean isValid = true;
        String errorMessage = "";
        if (interests != null && interests.size() < MIN_INTERESTS) {
            isValid = false;
            //i18n
            errorMessage = "Please enter at least " + MIN_INTERESTS + " #tags";
        }
        if (!isValid && showError) {
            AlertsHandler.showMessage(getActivity(), errorMessage);
        }
        continueButton.setEnabled(isValid);
        if (interests.size() >= 1 && interests.size() < MIN_INTERESTS) {
            continueButton.setText(aActivity.getResources().getString(R.string.hashtag_button_state_1));
        } else {
            continueButton.setText(aActivity.getResources().getString(R.string.save));
        }
        return isValid;
    }

    private void parseInterests() {
        if (interests == null) {
            return;
        }
        int tag = 0;
        for (String interest : interests) {
            displayNewInterest(interest, tag);
            tag++;
        }
    }

    private void parseRandomHastags() {
        randomInterestContainer.removeAllViews();
        for (HashtagModal hashtagModal : randomHashTags)
            addNewRandomInterest(hashtagModal);
    }

    private void displayNewInterest(String interest, int tag) {
        if (interest != null) {
            LinearLayout interestItemContainer = (LinearLayout) LayoutInflater.from(aContext).inflate(R.layout.interest_item,
                    interestsContainer, false);
            TextView interestTextView = (TextView) interestItemContainer.findViewById(R.id.interest_item_id);
            interestTextView.setText(interest);
            interestItemContainer.setTag(tag);
            interestItemContainer.setOnClickListener(deletehashatagListener);
            interestsContainer.addView(interestItemContainer);
        }
    }


    private void addNewRandomInterest(HashtagModal hashtagModal) {
        if (hashtagModal != null) {
            CheckBox v = (CheckBox) LayoutInflater.from(aContext).inflate(R.layout.random_hashtag_item,
                    randomInterestContainer, false);
            v.setChecked(hashtagModal.isSelected());
            v.setText(hashtagModal.getName());
            v.setTag(hashtagModal);
            v.setOnCheckedChangeListener(onRandomHashtagCheckChanged);
            randomInterestContainer.addView(v);
        }
    }

}