package com.trulymadly.android.app;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;

import com.trulymadly.android.app.utility.UiUtils;

// enables hardware accelerated rounded corners
// original idea here : http://www.curious-creature.org/2012/12/11/android-recipe-1-image-with-rounded-corners/
public class RoundedTransformation implements com.squareup.picasso.Transformation {
	private final int radiusPx;
	private final int marginDp;  // dp
	private final int borderRadiusDp;
	private final int borderColor;
	private final ROUND_CORNERS roundCorners;

	// radius is corner radii in dp
	// margin is the board in dp
	public RoundedTransformation(int radiusPx, int marginDp) {
		this.radiusPx = radiusPx;
		this.marginDp = marginDp;
		this.borderRadiusDp = 0;
		this.borderColor = 0;
		this.roundCorners = ROUND_CORNERS.ALL;

	}

	public RoundedTransformation(int radiusPx, int marginDp, ROUND_CORNERS rc) {
		this.radiusPx = radiusPx;
		this.marginDp = marginDp;
		this.borderRadiusDp = 0;
		this.borderColor = 0;
		this.roundCorners = rc;

	}
	public RoundedTransformation(int radiusPx, int marginDp, int borderRadiusDp, int borderColor) {
		this.radiusPx = radiusPx;
		this.marginDp = marginDp;
		this.borderRadiusDp = borderRadiusDp;
		this.borderColor = borderColor;
		this.roundCorners = ROUND_CORNERS.ALL;
	}

	@Override
	public Bitmap transform(final Bitmap source) {
		final Paint paint = new Paint();
		paint.setAntiAlias(true);
		paint.setShader(new BitmapShader(source, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP));

		try {
			Bitmap output = Bitmap.createBitmap(source.getWidth(), source.getHeight(), Config.ARGB_8888);
			Canvas canvas = new Canvas(output);
			RectF rectF = new RectF(marginDp, marginDp, source.getWidth() - marginDp, source.getHeight() - marginDp);
			int rx=0, ry=0;
			switch (roundCorners) {
			case TOP:
				ry = radiusPx;
				break;
			case BOTTOM:
				rx = radiusPx;
				break;
			case ALL:
			default:
				rx = radiusPx;
				ry = radiusPx;
				break;
			}
			canvas.drawRoundRect(rectF , rx, ry, paint);

			if(borderRadiusDp>0){
				Paint p1 = new Paint();
				p1.setColor(borderColor);
				p1.setStyle(Paint.Style.STROKE);
		        p1.setStrokeWidth(UiUtils.dpToPx(borderRadiusDp));
		        canvas.drawCircle(radiusPx/2.5f, radiusPx/2.5f, (radiusPx - UiUtils.dpToPx(borderRadiusDp)) / 2.5f, p1);
			}

			if (source != output) {
				source.recycle();
			}

			return output;
		} catch (NullPointerException e) {
			return source;
		}
	}

	@Override
	public String key() {
		return "rounded";
	}

	public enum ROUND_CORNERS {
		TOP, BOTTOM, ALL
	}
}