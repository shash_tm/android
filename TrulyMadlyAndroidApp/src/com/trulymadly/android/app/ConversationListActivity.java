package com.trulymadly.android.app;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.SearchView;
import android.widget.TextView;

import com.fortysevendeg.swipelistview.BaseSwipeListViewListener;
import com.fortysevendeg.swipelistview.SwipeListView;
import com.moe.pushlibrary.MoEHelper;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;
import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventStatus;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.adapter.ConversationListAdapter;
import com.trulymadly.android.app.ads.AdsHelper;
import com.trulymadly.android.app.ads.AdsSource;
import com.trulymadly.android.app.ads.AdsType;
import com.trulymadly.android.app.ads.BasicNativeAdEventsListener;
import com.trulymadly.android.app.ads.SeventyNineNativeHandler;
import com.trulymadly.android.app.ads.SimpleAdsListener;
import com.trulymadly.android.app.asynctasks.CleanupCacheAsyncTask;
import com.trulymadly.android.app.bus.AdEvent;
import com.trulymadly.android.app.bus.BusProvider;
import com.trulymadly.android.app.bus.SimpleAction;
import com.trulymadly.android.app.bus.TriggerConversationListUpdateEvent;
import com.trulymadly.android.app.custom.CircularRevealView;
import com.trulymadly.android.app.custom.SwipeListener;
import com.trulymadly.android.app.fragments.PasscodeFragment;
import com.trulymadly.android.app.fragments.PasscodeFragment.OnPasscodeEnteredInterface;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.ConstantsRF;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.listener.ConfirmDialogInterface;
import com.trulymadly.android.app.listener.CustomOkHttpResponseHandler;
import com.trulymadly.android.app.listener.OnActionBarClickedInterface;
import com.trulymadly.android.app.listener.OnConversationListFetchedInterface;
import com.trulymadly.android.app.listener.TapToRetryListener;
import com.trulymadly.android.app.modal.ConversationModal;
import com.trulymadly.android.app.modal.MatchMessageMetaData;
import com.trulymadly.android.app.modal.SparkModal;
import com.trulymadly.android.app.sqlite.MessageDBHandler;
import com.trulymadly.android.app.sqlite.RFHandler;
import com.trulymadly.android.app.sqlite.SparksDbHandler;
import com.trulymadly.android.app.utility.ActionBarHandler;
import com.trulymadly.android.app.utility.ActivityHandler;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.FilesHandler;
import com.trulymadly.android.app.utility.ImageCacheHelper;
import com.trulymadly.android.app.utility.MoEHandler;
import com.trulymadly.android.app.utility.OkHttpHandler;
import com.trulymadly.android.app.utility.PermissionsHelper;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.SparkTimer;
import com.trulymadly.android.app.utility.SparksHandler;
import com.trulymadly.android.app.utility.TMSelectHandler;
import com.trulymadly.android.app.utility.TapToRetryHandler;
import com.trulymadly.android.app.utility.TimeUtils;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;
import com.trulymadly.android.app.utility.WebviewHandler;
import com.trulymadly.android.chat.ReportAbuseButtonOnClickListner;

import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.RejectedExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities.sparks;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventStatus.clicked;

public class ConversationListActivity extends AppCompatActivity implements OnClickListener, OnPasscodeEnteredInterface {

    private static final int MAX_SWIPE_LEFT_OVERLAY_SHOW_COUNT = 1;
    private static final int SPARK_TAKE_ANIMATION_VISIBLE_DURATION = 3000;
    public static boolean isResumed = false;
    private final String PERMISSION_KEY = "isPermissionAsked";
    private final int mMinNumberOfChatsToEnableChats = 10;
    //Sparks
    @BindView(R.id.spark_name_tv)
    TextView mSparkNameTV;
    @BindView(R.id.spark_select_label)
    View mSparkSelectLabel;
    @BindView(R.id.spark_designation_tv)
    TextView mSparkDesignationTV;
    @BindView(R.id.spark_message_tv)
    TextView mSparkMessageTV;
    @BindView(R.id.sparks_container)
    View mSparkContainer;
    @BindView(R.id.spark_info_card)
    View mSparkInfoCard;
    @BindView(R.id.middle_card)
    View mMiddleCard;
    @BindView(R.id.blurred_spark_background_iv)
    ImageView mBlurredSparkIV;
    @BindView(R.id.spark_profile_pic_iv)
    ImageView mSparkProfilePic;
    @BindView(R.id.bottom_card)
    View mBottomCard;
    //    @BindView(R.id.analog_timer)
//    AnalogTimerView mAnalogTimerView;
    @BindView(R.id.digital_timer_container)
    View mDigitalTimerContainer;
    @BindView(R.id.digital_timer_tv)
    TextView mDigitalTimerTV;
    @BindView(R.id.digital_timer_left_tv)
    TextView mDigitalTimerLeftTV;
    @BindView(R.id.spark_info_innerview)
    View spark_info_innerview;
    @BindView(R.id.sparks_indicator_iv)
    ImageView sparks_indicator_iv;
    @BindView(R.id.spark_stack_separator)
    View spark_stack_separator;
    @BindView(R.id.mutual_matches_header)
    View mutual_matches_header;
    @BindView(R.id.mutual_matches_header_counter_tv)
    TextView mutual_matches_header_counter_tv;
    @BindView(R.id.spark_revealview)
    CircularRevealView mSparkRevealview;
    @BindView(R.id.spark_take_action_container)
    View mSparkTakeActionContainer;
    @BindView(R.id.timer_tv)
    TextView mTimerTV;
    @BindView(R.id.spark_expiry_warning_container)
    View mSparkExpiryWarningContainer;
    private String TAG = "Conversation List";
    private Context aContext;
    //private Activity aActivity;
    private WeakReference<Activity> weakActivity;
    private SwipeListView conversation_swipelist_view;
    private View emptyText;
    private ConversationListAdapter conversationListAdapter = null;
    private MoEHelper mhelper;
    private String myId;
    private TapToRetryHandler tapToRetryHandler = null;
    private OnClickListener unmatchClickListener, shareClickListener;
    private ProgressDialog mProgressDialog;
    // Passcode Conversations Feature
    private PasscodeFragment mPasscodeFragment;
    private View mPasscodeFragmentContainer;
    private FragmentManager mFragmentManager;
    private boolean isChatsHidden = false, isHideChatsAvailable = false;
    private String mPasscode = null;
    private View tutorial_overlay, mPasscodeTutorialTV;
    private ImageView mPasscodeTutorialIV;
    private ActionBarHandler mActionBarHandler;
    private boolean isPasscodeFragmentVisible = false;
    private Handler mHandler;
    private WebviewHandler mWebviewHandler;
    private View mTransparencyOverlay;
    private boolean loadAd = false;
    private boolean isPermissionsAskedOnce = false;
    private String mSearchString;
    private View mEmptySearchText, rating_container;
    private OnActionBarClickedInterface mActionBarClickedInterface;
    private ImageView mCenterCircleIV, mHandIV;
    private View mPasscodeTutorialNew, mCircleRevealView, mHandContainer;
    private TextView mTapToHide;
    private ScaleAnimation mScaleFromCenter;
    private AlphaAnimation mShowAlphaAnimation, mShowTapToHideAnimation;
    private boolean isNewPasscodeTutorialShown = false, isPasscodeTutorialStopped = false,
            isShowTutorialCalledOnce = false, isAnyTutorialVisible = false;
    private int mPasscodeTutorialCurrentCounter = 0;
    private int mHidePasscodeTutorialTimeout = 4500;
    private View ratingPopUp, tell_us_tv, inform_tv;
    private TextView may_be_later_tv, rate_us_tv;
    private RatingBar rating_stars;
    private EditText feedback_edit_text;
    private int rating;
    private boolean submitEnabled;
    private boolean feedbackEditTextVisibility;
    private boolean ratingPopUpVisible;
    private int mConversationCount = 0;
    private boolean isConversationListVisible = false;
    private boolean isSparkTutorialRunning = false;
    private boolean skipFetchAndLoadSpark = false;
    private SparkModal mSparkModal;
    private MatchMessageMetaData mSparkMessageMetaData;
    private SparkTimer mSparkTimer;
    private SparkTimer.SparkTimerInterface mSparkTimerInterface;
    private SparksHandler.SparksReceiver mSparksReceiverHandler;
    private SparksHandler.SparksReceiver.SparksReceiverListener mSparksReceiverListener;
    private SwipeListener mSwipeListener;
    private String mSparkToBeRemoved;
    private boolean isAPICallRequired = true;
    private boolean areSparksFetchedOnce = false;
    private AlphaAnimation mShowSparkMsgAnimation;
    private AlphaAnimation mHideSparkMsgAnimation;
    private int mRevealColor;
    private Runnable mExpiryRunnable, mRevealSparkRunnable,
            mHideDigitalTimerRunnable, mRemoveSparkRunnable,
            mAcceptSparkRunnable, mShowFullScreenAdRunnable,
            mHidePasscodeTutorialRunnable;
    private boolean isSparkrevealShownOnce = false;
    //    private boolean isLoadedOnce = false;
    private AdsHelper mAdsHelper;
    private SeventyNineNativeHandler.SeventyNinenativeAdUnit mNativeAdConvItemUnit;
    private boolean mFetchNewAd = false;
    private boolean isCreated = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isCreated = true;


        if (savedInstanceState != null) {
            isPermissionsAskedOnce = savedInstanceState.getBoolean(PERMISSION_KEY, false);
        }
        try {
            setContentView(R.layout.conversation_list_layout);
        } catch (OutOfMemoryError e) {
            System.gc();
            finish();
            return;
        }

        //http://www.curious-creature.com/2012/12/01/android-performance-case-study/
        getWindow().setBackgroundDrawable(null);
        aContext = this;
        weakActivity = new WeakReference<>((Activity) this);
        //aActivity = this;
        mhelper = new MoEHelper(this);

        ButterKnife.bind(this);
        mRevealColor = ActivityCompat.getColor(aContext, R.color.spark_timer_warning_color);

        //Creating Image Cache folder
        FilesHandler.createImageCacheFolder();

        // Passcode Conversations Feature
        mFragmentManager = getSupportFragmentManager();

        isHideChatsAvailable = isHideChatsEnabled();
        mPasscodeFragmentContainer = findViewById(R.id.passcode_fragment_container);
        isChatsHidden = RFHandler.getBool(aContext, Utility.getMyId(aContext),
                ConstantsRF.RIGID_FIELD_CONVERSATIONS_HIDDEN);
        // rating popup
        ratingPopUp = findViewById(R.id.rating_pop_up_layout);
        tell_us_tv = findViewById(R.id.tell_us_tv);
        rating_stars = (RatingBar) findViewById(R.id.rating_stars);
        inform_tv = findViewById(R.id.inform_tv);
        may_be_later_tv = (TextView) findViewById(R.id.may_be_later_tv);
        may_be_later_tv.setOnClickListener(this);
        feedback_edit_text = (EditText) findViewById(R.id.feedback_edit_text);
        rating_container = findViewById(R.id.rating_container);
        rating_container.setOnClickListener(this);
        rate_us_tv = (TextView) findViewById(R.id.rate_us_tv);

        rating_stars.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                rating = (int) v;
                submitEnabled = true;
                Activity activity = weakActivity.get();
                if (activity != null) {
                    may_be_later_tv.setText(activity.getResources().getString(R.string.submit_2));
                }
                UiUtils.setBackgroundColor(aContext, may_be_later_tv, R.color.colorSecondary);

            }
        });

        isChatsHidden = RFHandler.getBool(aContext, Utility.getMyId(aContext),
                ConstantsRF.RIGID_FIELD_CONVERSATIONS_HIDDEN);
        mActionBarClickedInterface = new OnActionBarClickedInterface() {
            @Override
            public void onConversationsClicked() {
            }

            @Override
            public void onLocationClicked() {
            }


            @Override
            public void onCuratedDealsClicked() {

            }

            @Override
            public void onBackClicked() {
                onBackPressed();
            }

            @Override
            public void onUserProfileClicked() {
            }

            @Override
            public void onTitleClicked() {
                ConversationListActivity.this.onTitleClicked();
            }

            @Override
            public void onTitleLongClicked() {

            }

            @Override
            public void onSearchViewOpened() {

                if (isNewPasscodeTutorialShown && !isPasscodeTutorialStopped) {
                    hidePasscodeTutorial();
                }

//                if(isSparkTutorialRunning){
//                    toggleSparkTutorial(false);
//                }

                conversation_swipelist_view.closeOpenedItems();
                mActionBarHandler.toggleTitleBar(false);
            }

            @Override
            public void onSearchViewClosed() {
                mSearchString = "";
                mActionBarHandler.toggleTitleBar(true);
            }

            @Override
            public void onCategoriesClicked() {

            }
        };

        SearchView.OnQueryTextListener onQueryTextListener = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                conversationListAdapter.toggleAds(!Utility.isSet(newText));
                mSearchString = newText;
                Cursor cursor = conversationListAdapter.getFilterQueryProvider().runQuery(newText);
                showConversationList(cursor);
                return true;
            }
        };

        mActionBarHandler = new ActionBarHandler(this, getResources().getString(R.string.conversations), null,
                mActionBarClickedInterface, false, false, false, isHideChatsAvailable, false,
                onQueryTextListener, false);

        emptyText = findViewById(R.id.empty);
        mEmptySearchText = findViewById(R.id.search_empty);

        // New tutorial screen
        mPasscodeTutorialCurrentCounter = SPHandler.getInt(aContext,
                ConstantsSP.SHARED_PREF_NEW_PASSCODE_TUTORIAL_SHOWN_COUNTER);
        tutorial_overlay = findViewById(R.id.tutorial_overlay);
        mPasscodeTutorialTV = findViewById(R.id.passcode_tutorial_tv);
        mPasscodeTutorialIV = (ImageView) findViewById(R.id.passcode_tutorial_iv);

        mPasscodeTutorialNew = findViewById(R.id.passcode_tutorial_new);
        mCenterCircleIV = (ImageView) findViewById(R.id.center_circle_iv);
        mHandIV = (ImageView) findViewById(R.id.tutorial_hand_iv);
        Picasso.with(aContext).load(R.drawable.tap_hand_filled).noFade().into(mHandIV);
        mTapToHide = (TextView) findViewById(R.id.tap_to_hide_tv);
        UiUtils.setBackground(aContext, mTapToHide, R.drawable.tutorial_box);
        //mTapToHide.setBackground(ActivityCompat.getDrawable(aContext, R.drawable.tutorial_box));
        mTapToHide.setPadding(0, UiUtils.dpToPx(4), 0, 0);
        mCircleRevealView = findViewById(R.id.circle_reveal_view);
        mHandContainer = findViewById(R.id.hand_container);

        mScaleFromCenter = (ScaleAnimation) AnimationUtils.loadAnimation(aContext, R.anim.scale_from_point);
        long mRevealDuration = 1200;
        mScaleFromCenter.setDuration(mRevealDuration);
        mScaleFromCenter.setFillBefore(true);
        mScaleFromCenter.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                mCircleRevealView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (isPasscodeTutorialStopped) {
                    return;
                }
                startPasscodeTutorialReveal();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        mShowAlphaAnimation = new AlphaAnimation(0, 1);
        long mShowAlphaDuration = 1000;
        mShowAlphaAnimation.setDuration(mShowAlphaDuration);
        mShowAlphaAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                mHandIV.setVisibility(View.VISIBLE);
                mCenterCircleIV.setVisibility(View.VISIBLE);
                mHandContainer.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mCircleRevealView.startAnimation(mScaleFromCenter);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        mShowTapToHideAnimation = new AlphaAnimation(0, 1);
        mShowTapToHideAnimation.setDuration(mShowAlphaDuration);
        mShowTapToHideAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                mTapToHide.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        Picasso.with(aContext).load(R.drawable.one_click).into(mPasscodeTutorialIV);
        Picasso.with(aContext).load(R.drawable.left_swipe).into((ImageView) findViewById(R.id.conversation_swipe_tutorial_iv));
        tutorial_overlay.setOnClickListener(this);
        if (!Utility.isMale(aContext)) {
            ((TextView) findViewById(R.id.conversation_swipe_tutorial_tv))
                    .setText(getResources().getString(R.string.conversation_swipe_tutorial_female_string));
        }
        // End

        conversation_swipelist_view = (SwipeListView) findViewById(R.id.conversation_list);
        conversation_swipelist_view.setChoiceMode(ListView.CHOICE_MODE_NONE);
        int widthInDp = (int) (getResources().getDimension(R.dimen.conv_item_height) / getResources().getDisplayMetrics().density); // item width

        if (!Utility.isMale(aContext)) {
            widthInDp *= 2;
        }
        conversation_swipelist_view
                .setOffsetLeft((float) (UiUtils.getScreenWidth(this) - UiUtils.dpToPx(widthInDp)));
        conversation_swipelist_view.setSwipeListViewListener(new BaseSwipeListViewListener() {
            @Override
            public void onOpened(int position, boolean toRight) {
                try {
//					ConversationModal c = conversationListAdapter.getConversation(position);
//					if (c.getLastMessageState() == MessageState.BLOCKED_UNSHOWN) {
//						conversation_swipelist_view.closeOpenedItems();
//					}
                    conversationListAdapter.setFrontClickable(position, false);
                } catch (CursorIndexOutOfBoundsException ignored) {

                }
            }

            @Override
            public void onClosed(int position, boolean fromRight) {
                conversationListAdapter.setFrontClickable(position, true);
            }

            @Override
            public void onListChanged() {
            }

            @Override
            public void onMove(int position, float x) {
            }

            @Override
            public void onStartOpen(int position, int action, boolean right) {
            }

            @Override
            public void onStartClose(int position, boolean right) {
            }

            @Override
            public void onClickFrontView(int position) {
                onItemClick(position);
            }

            @Override
            public void onClickBackView(int position) {
            }

            @Override
            public void onDismiss(int[] reverseSortedPositions) {
            }

            @Override
            public int onChangeSwipeMode(int position) {
                if (!conversationListAdapter.isCurrentItemSwipable(position))
                    return SwipeListView.SWIPE_MODE_NONE;

                return super.onChangeSwipeMode(position);
            }
        });
        conversation_swipelist_view.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                UiUtils.hideKeyBoard(aContext);
                conversation_swipelist_view.closeOpenedItems();
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });

        tapToRetryHandler = new TapToRetryHandler(this, findViewById(android.R.id.content), new TapToRetryListener() {
            @Override
            public void reInitialize() {
                getConversationListFromServer(true);
            }
        }, null, false);

        shareClickListener = new OnClickListener() {

            @Override
            public void onClick(View v) {
                ActivityHandler.onShareClicked(weakActivity, ((ConversationModal) v.getTag()).getMatch_id(), TrulyMadlyActivities.conversation_list);
//				onShareClicked(v);
            }
        };

        unmatchClickListener = new OnClickListener() {

            @Override
            public void onClick(View v) {
                onUnmatchClicked(v);
            }
        };

        // re-attempt migrate of conversation list table
        MessageDBHandler.migrateToConversationListTable(aContext);

        ArrayList<String> matchesIdArray = MessageDBHandler.getMatchIdsWithNullValue(Utility.getMyId(aContext), aContext);
        if (matchesIdArray != null && matchesIdArray.size() > 0) {
            SPHandler.remove(aContext, ConstantsSP.SHARED_KEYS_CONVERSATION_LIST_API_CALL_TIME);
            MessageDBHandler.deleteMatchIdsWithNullValue(Utility.getMyId(aContext), aContext);
            getConversationListFromServer(false);
        }

        mHandler = new Handler();
        mHidePasscodeTutorialRunnable = new Runnable() {
            @Override
            public void run() {
                if (isNewPasscodeTutorialShown && !isPasscodeTutorialStopped) {
                    hidePasscodeTutorial();
                }
            }
        };
        mAcceptSparkRunnable = new Runnable() {
            @Override
            public void run() {
                mSwipeListener.slideRight(mSparkInfoCard, new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        checkAndLoadSpark();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }
        };

        initializeAd();
    }

    private void showRatingPopUp() {
        //&& MessageDBHandler.getRealMutualMatchesCount(Utility.getMyId(aContext), aContext) > 0
        if (!RFHandler.getBool(aContext, ConstantsRF.RIGID_FIELD_RATING_POPUP_SHOWN)) {
            RFHandler.insert(aContext, ConstantsRF.RIGID_FIELD_RATING_POPUP_SHOWN, true);
            rating_container.setVisibility(View.VISIBLE);
            ratingPopUp.setVisibility(View.VISIBLE);
            ratingPopUpVisible = true;
        } else {
            ratingPopUpVisible = false;
            rating_container.setVisibility(View.GONE);
            ratingPopUp.setVisibility(View.GONE);
        }

    }


    private void initializeAd() {

        //Webview related
        WebviewHandler.WebviewActionsListener mWebviewActionsListener = new WebviewHandler.WebviewActionsListener() {
            @Override
            public boolean shouldOverrideUrlLoading(String url) {
                mWebviewHandler.loadUrl(url);
                mActionBarHandler.toggleActionBar(false);
                return true;
            }

            @Override
            public void webViewHiddenOnUrlLoad() {
                mActionBarHandler.toggleActionBar(true);
            }

            @Override
            public void onWebViewCloseClicked() {
                mActionBarHandler.toggleActionBar(true);
//                mWebViewOverlay.setVisibility(View.GONE);
//                TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyActivities.conversation_list,
//                        TrulyMadlyEventTypes.ad_native, 0,
//                        TrulyMadlyEventStatus.webview_closed, null, true);
            }
        };
        View mWebviewIncludeView = findViewById(R.id.webview_include_view);
        mWebviewHandler = new WebviewHandler(mWebviewIncludeView, mWebviewActionsListener, true, true);
        mTransparencyOverlay = findViewById(R.id.transparency_overlay);

        mAdsHelper = new AdsHelper(aContext, new SimpleAdsListener() {
            @Override
            public void onAdLoadSuccess(AdsType adsType, Object object) {
                mNativeAdConvItemUnit = (SeventyNineNativeHandler.SeventyNinenativeAdUnit) object;
                if (mNativeAdConvItemUnit != null && mNativeAdConvItemUnit.getmNativeAdConvItemUIUnit() != null) {
                    conversationListAdapter.loadAd(mNativeAdConvItemUnit.getmNativeAdConvItemUIUnit());
                }
                if (mNativeAdConvItemUnit != null && mNativeAdConvItemUnit.getmAdContainer() != null) {
                    conversationListAdapter.loadAd(mNativeAdConvItemUnit.getmAdContainer());
                }
            }

            @Override
            public void onAdLoadfailed(AdsType adsType) {
                conversationListAdapter.removeAd();
                mNativeAdConvItemUnit = null;
                super.onAdLoadfailed(adsType);
            }
        }, AdsSource.SEVENTY_NINE);
//        mAdsHelper.initialize(AdsType.CONV_LIST_NATIVE, TrulyMadlyActivities.conversation_list);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);

        fetchConversationData(true);
    }

    private void processIntent() {
        Bundle extras = getIntent().getExtras();
        String toast_to_shown = null;
        String removeSpark = null;
        isAPICallRequired = true;
        if (extras != null) {
            toast_to_shown = extras.getString("blocked_text");
            removeSpark = extras.getString("remove_spark");
            isAPICallRequired = extras.getBoolean("call_api", true);
            getIntent().removeExtra("remove_spark");
            getIntent().removeExtra("blocked_text");
            getIntent().removeExtra("call_api");
        }

        if (toast_to_shown != null) {
            AlertsHandler.showMessage(this, toast_to_shown);
        }

        if (Utility.isSet(removeSpark)) {
            mSparkToBeRemoved = removeSpark;
            if (mRemoveSparkRunnable == null) {
                mRemoveSparkRunnable = new Runnable() {
                    @Override
                    public void run() {
                        mProgressDialog = UiUtils.showProgressBar(aContext, mProgressDialog);
                        updateSpark(SparkModal.SPARK_STATUS.rejected, isAPICallRequired);
                        isAPICallRequired = true;
                    }
                };
            }
//            if(mSparkModal != null && Utility.stringCompare(removeSpark, mSparkModal.getmMatchId()))
//            {
            skipFetchAndLoadSpark = true;
//            }
        }
    }

    private void fetchConversationData(boolean attemptServerConnection) {
        loadAd = true;
        if (!RFHandler.getBool(aContext, ConstantsRF.RIGID_FIELD_CONVERSATION_VIEW_FIRST_TIME)) {
            getConversationListFromServer(true);
        } else {
            (new GetConversationsAsyncTask(attemptServerConnection, true)).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null, null, null);
        }
    }

    private void getConversationListFromServerOnUiThread(final boolean showLoader) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                getConversationListFromServer(showLoader);
            }
        });
    }

    private void getConversationListFromServer(final Boolean showLoader) {

        if (showLoader) {
            tapToRetryHandler.showLoader();
        }

        OnConversationListFetchedInterface onConversationListFetchedInterface = new OnConversationListFetchedInterface() {
            @Override
            public void onSuccess(int unreadConversationsCount) {
                showConversationList(showLoader);
            }

            @Override
            public void onFail(Exception exception) {
                failedNetworkRequest(showLoader, exception);

            }
        };
        Utility.getConversationListFromServer(aContext, onConversationListFetchedInterface);
    }


    @SuppressWarnings("UnusedParameters")
    @Subscribe
    public void triggerUpdateConversationList(TriggerConversationListUpdateEvent triggerConversationListUpdateEvent) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                fetchConversationData(false);
            }
        });
    }

    public void triggerCloseConversationListSwipes() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                conversation_swipelist_view.closeOpenedItems();
            }
        });
    }

    /**
     * Hide the loader and show the conversation list
     *
     * @param showLoader flag to check is loader was shown
     */
    private void showConversationList(boolean showLoader) {
        if (showLoader) {
            toggleConversationList(true);
            tapToRetryHandler.onSuccessFull();
        }
        (new GetConversationsAsyncTask(false, false)).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null, null, null);
    }

    /**
     * @param cursor of the database for full conversation list
     */
    private void showConversationList(Cursor cursor) {
        if (cursor != null && cursor.getCount() > 0 && (!isHideChatsAvailable || !isChatsHidden)) {
            int mMinNumberOfChatsToEnableChats = 10;
            mConversationCount = cursor.getCount();
            if (cursor.getCount() > mMinNumberOfChatsToEnableChats) {
                mActionBarHandler.toggleSearchView(true);
            }
            toggleConversationList(true);
            if (cursor.getCount() > 1) {
                MoEHandler.trackEvent(aContext, MoEHandler.Events.CONV_LIST_LAUNCHED);
            }
            emptyText.setVisibility(View.GONE);
            mEmptySearchText.setVisibility(View.GONE);

            if (conversationListAdapter == null) {
                conversationListAdapter = new ConversationListAdapter(aContext, cursor, unmatchClickListener,
                        shareClickListener, new BasicNativeAdEventsListener() {
                    @Override
                    public void reportImpression() {
                        if (mAdsHelper != null) {
                            mAdsHelper.reportImpression(AdsType.CONV_LIST_NATIVE, mNativeAdConvItemUnit);
                        }
                    }

                    @Override
                    public void onClicked() {

                    }
                },
                        (mNativeAdConvItemUnit != null) ? mNativeAdConvItemUnit.getmNativeAdConvItemUIUnit() : null);
                conversation_swipelist_view.setAdapter(conversationListAdapter);
            } else {
                conversationListAdapter.changeCursor(cursor);
            }

            if (mFetchNewAd && mAdsHelper.areConstraintsSatisfied(AdsType.CONV_LIST_NATIVE,
                    TrulyMadlyActivities.conversation_list)) {
                mFetchNewAd = false;
                mAdsHelper.loadAd(null, AdsType.CONV_LIST_NATIVE, null, mAdsHelper.shouldShowAd(AdsType.CONV_LIST_NATIVE, false));
            }

            loadAd = false;
            showTutorialOverlay(cursor.getCount());

        } else {
            if (cursor != null) {
                if (cursor.getCount() == 0) {
                    toogleHideChatsFeature(false);
                }
                cursor.close();
            }
            hideConversationList();
        }
    }

    private void hideConversationList() {
        toggleConversationList(false);
        mEmptySearchText.setVisibility(View.VISIBLE);

        if (!Utility.isSet(mSearchString)) {
            mActionBarHandler.toggleSearchView(false);
        }
    }

    private void failedNetworkRequest(Boolean showLoader, Exception exception) {
        if (showLoader) {
            toggleConversationList(false);
            tapToRetryHandler.onNetWorkFailed(exception);
        }
    }

    private void toggleMutualMatchesHeader(boolean showHeader) {
        if (isConversationListVisible && showHeader && mConversationCount > 0) {
            mutual_matches_header.setVisibility(View.VISIBLE);
            mutual_matches_header_counter_tv.setText(
                    String.format(aContext.getString(R.string.mutual_matches),
                            String.valueOf(mConversationCount)));
        } else {
            mutual_matches_header.setVisibility(View.GONE);
        }
    }

    private void toggleConversationList(boolean makeVisible) {
        isConversationListVisible = makeVisible;
        if (conversation_swipelist_view != null) {
            conversation_swipelist_view.setVisibility((makeVisible) ? View.VISIBLE : View.GONE);
            mutual_matches_header_counter_tv.setVisibility((makeVisible) ? View.VISIBLE : View.GONE);
        }

        toggleMutualMatchesHeader(isSparkTutorialRunning || mSparkModal != null);

    }

    private void toogleHideChatsFeature(boolean enable) {
        isHideChatsAvailable = enable;
        mActionBarHandler.makeTitleBarLongClickable(enable);
    }

    //Always checks the rigid fields to check if the passcode protection feature is enabled or not
    private boolean isHideChatsEnabled() {
        return RFHandler.getBool(aContext, Utility.getMyId(aContext),
                ConstantsRF.RIGID_FIELD_IS_PASSCODE_PROTECTION_ENABLED);
    }

    // Called when the title in the toolbar is clicked to handle the passcode
    private void onTitleClicked() {
        //This is to take care of the edge case when user is not having any matches
        if (!isHideChatsAvailable)
            return;

        if (isNewPasscodeTutorialShown && !isPasscodeTutorialStopped) {
            hidePasscodeTutorial();
        }

//        if(isSparkTutorialRunning){
//            toggleSparkTutorial(false);
//        }

        if (getPasscode() == null || isChatsHidden)
            showPasscodeFragment();
        else {
            OkHttpHandler.httpGet(aContext, ConstantsUrls.getPasscodeTrackUrl(),
                    GetOkHttpRequestParams.getHttpRequestParams(
                            Constants.HttpRequestType.PASSCODE, "hide"),
                    new CustomOkHttpResponseHandler(aContext) {
                        @Override
                        public void onRequestSuccess(JSONObject responseJSON) {

                        }

                        @Override
                        public void onRequestFailure(Exception exception) {

                        }
                    });
            hideConversationList();
            isChatsHidden = !isChatsHidden;
            AlertsHandler.showMessage(this, R.string.chats_hidden_message, false);
            RFHandler.insert(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_CONVERSATIONS_HIDDEN,
                    isChatsHidden);
        }
    }

    private String getPasscode() {
        if (mPasscode == null)
            mPasscode = RFHandler.getString(aContext,
                    Utility.getMyId(aContext),
                    ConstantsRF.RIGID_FIELD_HIDE_CONVERSATIONS_PASSCODE);

        return mPasscode;
    }

    private void showPasscodeFragment() {
        try {
            mPasscodeFragmentContainer.setVisibility(View.VISIBLE);
            isPasscodeFragmentVisible = true;
            mPasscodeFragment = PasscodeFragment.getInstance(ConstantsRF.RIGID_FIELD_HIDE_CONVERSATIONS_PASSCODE);

            mFragmentManager.beginTransaction().replace(R.id.passcode_fragment_container, mPasscodeFragment,
                    PasscodeFragment.class.getSimpleName()).commitAllowingStateLoss();

        } catch (IllegalStateException ignored) {
        }
    }

    private void hidePassCodeFragment() {
        UiUtils.hideKeyBoard(this);
        mPasscodeFragmentContainer.setVisibility(View.GONE);
        isPasscodeFragmentVisible = false;
        if (mPasscodeFragment != null) {
            mFragmentManager.beginTransaction().remove(mPasscodeFragment).commitAllowingStateLoss();
        }
    }

    @Override
    public void onPasscodeSuccess() {
        // Check if conversations are currently hidden
        if (!isChatsHidden) {
            isChatsHidden = !isChatsHidden;
            if (!SPHandler.getBool(aContext,
                    ConstantsSP.SHARED_PREF_NEW_PASSCODE_TUTORIAL_UNHIDE_SHOWN, false)) {
                isAnyTutorialVisible = true;
                isPasscodeTutorialStopped = false;
                startPasscodeTutorial(false);
            }
            hideConversationList();
            OkHttpHandler.httpGet(aContext, ConstantsUrls.getPasscodeTrackUrl(),
                    GetOkHttpRequestParams.getHttpRequestParams(
                            Constants.HttpRequestType.PASSCODE, "hide"),
                    new CustomOkHttpResponseHandler(aContext) {
                        @Override
                        public void onRequestSuccess(JSONObject responseJSON) {

                        }

                        @Override
                        public void onRequestFailure(Exception exception) {

                        }
                    });
            AlertsHandler.showMessage(this, R.string.chats_hidden_message, false);
        } else {
            isChatsHidden = !isChatsHidden;
            TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyActivities.conversation_list,
                    TrulyMadlyEventTypes.conversation_passcode, 0, TrulyMadlyEventStatus.unlock, null, true);
            OkHttpHandler.httpGet(aContext, ConstantsUrls.getPasscodeTrackUrl(),
                    GetOkHttpRequestParams.getHttpRequestParams(
                            Constants.HttpRequestType.PASSCODE, "unhide"),
                    new CustomOkHttpResponseHandler(aContext) {
                        @Override
                        public void onRequestSuccess(JSONObject responseJSON) {

                        }

                        @Override
                        public void onRequestFailure(Exception exception) {

                        }
                    });
            fetchConversationData(false);
        }

        RFHandler.insert(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_CONVERSATIONS_HIDDEN,
                isChatsHidden);

        hidePassCodeFragment();
    }

    @Override
    public void onPasscodeFailure() {
    }

    @Override
    public void onPasscodeForgot() {
        UiUtils.hideKeyBoard(aContext);
        if (!isPasscodeAlreadyRequested()) {
            AlertsHandler.showMessage(this, R.string.resend_password_message, false);
            Utility.generatePasscodeAlarm(aContext, getPasscode(), Constants.PASSCODE_DELAY);
        } else {
            AlertsHandler.showMessage(this, R.string.password_already_requested_message, false);
        }
    }

    private boolean isPasscodeAlreadyRequested() {
        long currentTime = System.currentTimeMillis();
        String alreadyRequestedTS = RFHandler.getString(aContext, Utility.getMyId(aContext),
                ConstantsRF.RIGID_FIELD_CONVERSATIONS_PASSCODE_REQUESTED_TIMESTAMP);
        if (!Utility.isSet(alreadyRequestedTS)) {
            RFHandler.insert(aContext, Utility.getMyId(aContext),
                    ConstantsRF.RIGID_FIELD_CONVERSATIONS_PASSCODE_REQUESTED_TIMESTAMP, String.valueOf(currentTime));
            return false;
        }

        long alreadyRequestedTSValue = Long.parseLong(alreadyRequestedTS);
        if (currentTime <= alreadyRequestedTSValue + Constants.PASSCODE_DELAY)
            return true;
        else {
            RFHandler.insert(aContext, Utility.getMyId(aContext),
                    ConstantsRF.RIGID_FIELD_CONVERSATIONS_PASSCODE_REQUESTED_TIMESTAMP, String.valueOf(currentTime));
            return false;
        }
    }

    private void onItemClick(int position) {
        try {
            if (conversationListAdapter.isCurrentItemAd(position)) {
                mAdsHelper.onClicked(AdsType.CONV_LIST_NATIVE, mNativeAdConvItemUnit);
//                if (Uri.parse(conversationListAdapter.getLandingUrl()).getScheme().equals("market") && (ActivityHandler.appInstalledOrNot(this, Constants.packageGoogleMarket)
//                        || ActivityHandler.appInstalledOrNot(this, Constants.packageGooglePlay))) {
//                    Utility.openWebsite(aContext, conversationListAdapter.getLandingUrl());
//                    mActionBarHandler.toggleActionBar(true);
//                    mWebviewHandler.hide();
//                } else {
//                    mActionBarHandler.toggleActionBar(false);
//                    mWebviewHandler.loadUrl(conversationListAdapter.getLandingUrl());
//                }
                return;
            }

            ConversationModal conversation = conversationListAdapter.getConversation(position);
            boolean isUnread = false;
            switch (conversation.getLastMessageState()) {
                case BLOCKED_SHOWN:
                case BLOCKED_UNSHOWN:
                    new AlertDialog.Builder(this).setMessage(R.string.declined_txt)
                            .setPositiveButton(android.R.string.yes, new BlockedShownListener(position))
                            .setIcon(android.R.drawable.ic_dialog_alert).setCancelable(false).show();
                    break;
                case CONVERSATION_FETCHED_NEW:
                case INCOMING_DELIVERED:
                case MATCH_ONLY_NEW:
                    isUnread = true;
                default:
                    ActivityHandler.startMessageOneonOneActivity(aContext, conversation.getMatch_id(),
                            conversation.getMessage_link(), false, false, isUnread, false);
                    break;
            }
        } catch (CursorIndexOutOfBoundsException ignored) {

        }

    }

    @Override
    public void onStart() {
        super.onStart();
        BusProvider.getInstance().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        BusProvider.getInstance().unregister(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(PERMISSION_KEY, isPermissionsAskedOnce);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mFetchNewAd = true;

        toogleHideChatsFeature(isHideChatsEnabled());
        fetchConversationData(true);

        if (TimeUtils.isTimeoutExpired(aContext, ConstantsSP.SHARED_KEYS_LAST_CLEANUP_STARTED_TIMESTAMP,
                Constants.IMAGE_CACHE_CLEANUP_TIMEOUT)) {

            CleanupCacheAsyncTask task = new CleanupCacheAsyncTask();
            try {
                task.execute(Constants.IMAGES_ASSETS_FOLDER);
                SPHandler.setString(aContext, ConstantsSP.SHARED_KEYS_LAST_CLEANUP_STARTED_TIMESTAMP, "" + TimeUtils.getSystemTimeInMiliSeconds());
            } catch (RejectedExecutionException ignored) {

            }
        }

        boolean isCreated = true;
        isResumed = true;

        processIntent();
        if (skipFetchAndLoadSpark && mSparkModal == null) {
            checkAndLoadSpark();
        }

        startSparkTimer();
        if (!skipFetchAndLoadSpark) {
            checkAndLoadSpark();
        } else {
            if (mSparkModal != null && mSwipeListener != null && Utility.stringCompare(mSparkToBeRemoved, mSparkModal.getmMatchId())) {
                mHandler.postDelayed(mRemoveSparkRunnable, 418);
            }
            mSparkToBeRemoved = null;
            skipFetchAndLoadSpark = false;
        }
    }

    @Override
    protected void onDestroy() {
        isCreated = false;
        weakActivity.clear();
        if (mHandler != null) {
            mHandler.removeCallbacks(mHidePasscodeTutorialRunnable);
            mHandler.removeCallbacks(mRemoveSparkRunnable);
            mHandler.removeCallbacks(mAcceptSparkRunnable);
        }

        if (mSparksReceiverHandler != null) {
            mSparksReceiverHandler.register(null);
        }

        if (conversationListAdapter != null) {
            conversationListAdapter.changeCursor(null);
        }

        super.onDestroy();
    }


    @Override
    protected void onPause() {
        if (mHandler != null) {
            mHandler.removeCallbacks(mRevealSparkRunnable);
            mHandler.removeCallbacks(mExpiryRunnable);
            mHandler.removeCallbacks(mHideDigitalTimerRunnable);
            mDigitalTimerContainer.setVisibility(View.GONE);
        }

        conversation_swipelist_view.closeOpenedItems();

        if (isNewPasscodeTutorialShown && !isPasscodeTutorialStopped) {
            hidePasscodeTutorial();
        }

        if (mSwipeListener != null) {
            mSwipeListener.cancelTutorialAnimation(mSparkInfoCard);
        }

//        if(isSparkTutorialRunning){
//            toggleSparkTutorial(false);
//        }

//        if(mSparkTimer != null) {
//            mSparkTimer.stop();
//        }

        super.onPause();
        isResumed = false;
    }

    @Override
    public void onBackPressed() {

        if (ratingPopUpVisible) {
            ratingPopUpVisible = false;
            hideRatingPopUp();
            return;
        }

        if (mActionBarHandler.isSearchViewExpanded()) {
            mActionBarHandler.expandCollapseSearchView(false);
            mActionBarClickedInterface.onSearchViewClosed();
            return;
        }
        if (Utility.isRootActivity(this)) {
            ActivityHandler.startMatchesActivity(aContext);
            finish();
        }
        try {
            if (isHideChatsAvailable && isPasscodeFragmentVisible) {
                hidePassCodeFragment();
            } else if (mWebviewHandler.isVisible()) {
                mActionBarHandler.toggleActionBar(true);
                mWebviewHandler.hide();
//                TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyActivities.conversation_list,
//                        TrulyMadlyEvent.TrulyMadlyEventTypes.ad_native, 0,
//                        TrulyMadlyEventStatus.webview_closed, null, true);
            } else {
                super.onBackPressed();
            }
        } catch (IllegalStateException ignored) {
        }
    }

    private void hideRatingPopUp() {

        ratingPopUpVisible = false;
        rating_container.setVisibility(View.GONE);
        ratingPopUp.setVisibility(View.GONE);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tutorial_overlay:
                hideTutorialOverlay();
                break;

            case R.id.may_be_later_tv:
                if (!submitEnabled) {
                    submitEnabled = false;
                    hideRatingPopUp();

                } else if (feedbackEditTextVisibility) {
                    UiUtils.hideKeyBoard(aContext);
                    hideRatingPopUp();
                    HashMap<String, String> eventInfo = new HashMap<>();
                    eventInfo.put("reason", feedback_edit_text.getText().toString());
                    TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyActivities.conversation_list, TrulyMadlyEventTypes.rating, 0, "" + rating, eventInfo, true);
                } else {
                    rate_us_tv.setText(this.getResources().getString(R.string.thank_you));
                    if (rating <= 3) {
                        rating_stars.setVisibility(View.GONE);
                        inform_tv.setVisibility(View.VISIBLE);
                        tell_us_tv.setVisibility(View.GONE);
                        feedback_edit_text.setVisibility(View.VISIBLE);
                        feedbackEditTextVisibility = true;
                    } else if (rating >= 4) {
                        hideRatingPopUp();
                        TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyActivities.conversation_list, TrulyMadlyEventTypes.rating, 0, "" + rating, null, true);
                        ActivityHandler.launchPlayStoreTrulymadly(this);
                    } else {
                        hideRatingPopUp();
                    }
                }
                break;
            default:
                break;
        }
    }

    private void hideTutorialOverlay() {
        tutorial_overlay.setVisibility(View.GONE);
        int count = SPHandler.getInt(aContext, ConstantsSP.SHARED_KEYS_SWIPE_LEFT_OVERLAY_SHOWN_COUNT);
        SPHandler.setInt(aContext, ConstantsSP.SHARED_KEYS_SWIPE_LEFT_OVERLAY_SHOWN_COUNT, count + 1);
        if (isHideChatsAvailable &&
                mPasscodeTutorialCurrentCounter < Constants.NEW_PASSCODE_TUTORIAL_MAX_COUNT
                && !isNewPasscodeTutorialShown) {
            startPasscodeTutorial(true);
        } else {
            isAnyTutorialVisible = false;
        }
    }

    private void showTutorialOverlay(int convCount) {
        int count = SPHandler.getInt(aContext, ConstantsSP.SHARED_KEYS_SWIPE_LEFT_OVERLAY_SHOWN_COUNT);
        //Conditions:
        //1. If any tutorial is already visible or not
        //2. Is already shown or not
        if (!isAnyTutorialVisible && count < MAX_SWIPE_LEFT_OVERLAY_SHOW_COUNT) {
//            if (isHideChatsAvailable) {
//                mPasscodeTutorialIV.setVisibility(View.VISIBLE);
//                mPasscodeTutorialTV.setVisibility(View.VISIBLE);
//            } else {
//                mPasscodeTutorialIV.setVisibility(View.GONE);
//                mPasscodeTutorialTV.setVisibility(View.GONE);
//            }
            mPasscodeTutorialIV.setVisibility(View.GONE);
            mPasscodeTutorialTV.setVisibility(View.GONE);
            isAnyTutorialVisible = true;
            tutorial_overlay.setVisibility(View.VISIBLE);
        }
        //Conditions:
        //1. If any tutorial is already visible or not
        //2. Is hide chats available or not
        //3. If already shown maximum number of times
        //4. Is already shown in this activity session
        else if (!isAnyTutorialVisible && isHideChatsAvailable &&
                mPasscodeTutorialCurrentCounter < Constants.NEW_PASSCODE_TUTORIAL_MAX_COUNT
                && !isNewPasscodeTutorialShown) {
            isAnyTutorialVisible = true;
            startPasscodeTutorial(true);
        }
        //Conditions:
        //1. If any tutorial is already visible or not
        //2. Is conv count is > 5 or not
        //3. If already asked once in this activity session
        else if (!isAnyTutorialVisible && convCount > 5 && !isPermissionsAskedOnce) {
            askForPermission();
        } else if (!isAnyTutorialVisible && convCount > 1) {
            int lastConvCount = RFHandler.getInt(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_LAST_CONV_COUNT, 0);

            int realMutualMatchCount = MessageDBHandler.getRealMutualMatchesCount(Utility.getMyId(aContext), aContext);
            if (realMutualMatchCount > lastConvCount && lastConvCount > 0) {
                RFHandler.insert(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_LAST_CONV_COUNT, realMutualMatchCount);
                showRatingPopUp();
            }
        }
    }

    private void startPasscodeTutorial(boolean hideTutorial) {
        isNewPasscodeTutorialShown = true;
        mPasscodeTutorialCurrentCounter++;
        if (hideTutorial) {
            SPHandler.setInt(aContext,
                    ConstantsSP.SHARED_PREF_NEW_PASSCODE_TUTORIAL_SHOWN_COUNTER, mPasscodeTutorialCurrentCounter);
            mTapToHide.setText(R.string.tap_to_hide_your_chats);
        } else {
            SPHandler.setBool(aContext,
                    ConstantsSP.SHARED_PREF_NEW_PASSCODE_TUTORIAL_UNHIDE_SHOWN, true);
            mTapToHide.setText(R.string.tap_to_unhide_your_chats);
        }
        mPasscodeTutorialNew.setVisibility(View.VISIBLE);
        mHandContainer.startAnimation(mShowAlphaAnimation);
        mTapToHide.startAnimation(mShowTapToHideAnimation);
        int mHidePasscodeTutorialTimeout = 4500;
        mHandler.postDelayed(mHidePasscodeTutorialRunnable, mHidePasscodeTutorialTimeout);
    }

    private void startPasscodeTutorialReveal() {
        long mRevealDelay = 700;
        mScaleFromCenter.setStartOffset(mRevealDelay);
        mCircleRevealView.startAnimation(mScaleFromCenter);
    }

    private void hidePasscodeTutorial() {
        isPasscodeTutorialStopped = true;
        isAnyTutorialVisible = false;
        mPasscodeTutorialNew.setVisibility(View.GONE);
        mHandIV.setVisibility(View.INVISIBLE);
        mCenterCircleIV.setVisibility(View.INVISIBLE);
        mCircleRevealView.setVisibility(View.INVISIBLE);
        mTapToHide.setVisibility(View.INVISIBLE);
    }

    private void askForPermission() {
        isPermissionsAskedOnce = true;
        PermissionsHelper.checkAndAskForPermission(this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                PermissionsHelper.INITIAL_STORAGE_PERMISSION_CODE);
    }

    @SuppressLint({"Override", "NewApi"})
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (grantResults == null || grantResults.length == 0) {
            return;
        }
        switch (requestCode) {
            case PermissionsHelper.INITIAL_STORAGE_PERMISSION_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyEvent.TrulyMadlyActivities.conversation_list,
                            TrulyMadlyEvent.TrulyMadlyEventTypes.permission, 0,
                            TrulyMadlyEventStatus.granted, null, false);
                } else {
                    boolean neverAskAgain = !ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[0]);
                    if (!neverAskAgain) {
                        AlertsHandler.showMessageWithAction(this, R.string.we_need_permissions,
                                R.string.permission_rejection_action, new OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyEvent.TrulyMadlyActivities.conversation_list,
                                                TrulyMadlyEvent.TrulyMadlyEventTypes.permission, 0,
                                                TrulyMadlyEventStatus.try_again, null, false);
                                        askForPermission();
                                    }
                                }, false, 3);
                        TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyEvent.TrulyMadlyActivities.conversation_list,
                                TrulyMadlyEvent.TrulyMadlyEventTypes.permission, 0,
                                TrulyMadlyEventStatus.denied, null, false);
                    }
                }
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    private void getAndSetNextSpark() {
        mSparkModal = SparksDbHandler.getTopSpark(aContext, Utility.getMyId(aContext));
        if (mSparkModal != null) {
            mSparkMessageMetaData = MessageDBHandler.getMatchMessageMetaData(mSparkModal.getmUserId(), mSparkModal.getmMatchId(), aContext);
        }
    }

    private void startSparkTimer() {
        if (mSparkModal != null) {
            long timeLeft = mSparkModal.getmExpiredTimeInSeconds();
            if (mSparkModal.getmStartTime() > 0) {
//            if (mSparkTimer == null) {
//                mSparkTimerInterface = new SparkTimer.SparkTimerInterface() {
//                    @Override
//                    public void onTimeUpdated(final int mElapsedTimeInSeconds) {
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                if(mSparkModal != null) {
//                                    int timeLeft = (int) mSparkModal.getmExpiredTimeInSeconds() - mElapsedTimeInSeconds;
////                                    mAnalogTimerView.setCurrentValue(timeLeft);
//                                    if (mElapsedTimeInSeconds <= 5) {
//                                        mDigitalTimerTV.setText(TimeUtils.getFormattedLeftTime(timeLeft));
//                                        int textId = R.string.mins_left;
//                                        if(timeLeft == 60*60){
//                                            textId = R.string.hour_left;
//                                        }else if(timeLeft >= 2*60*60){
//                                            textId = R.string.hours_left;
//                                        }
//                                        mDigitalTimerLeftTV.setText(textId);
//                                    } else {
//                                        mDigitalTimerTV.setText("");
//                                        mDigitalTimerContainer.setVisibility(View.GONE);
//                                    }
//                                }
//                            }
//                        });
//                    }
//
//                    @Override
//                    public void onTimeExpired() {
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                if (mSparkModal != null) {
//                                    updateSpark(SparkModal.SPARK_STATUS.rejected, false);
//                                }
//                            }
//                        });
//                    }
//                };
//                mSparkTimer = new SparkTimer(1, mSparkModal.getmStartTime(), mSparkModal.getmExpiredTimeInSeconds() * 1000);
//            }
//            if(mSparkModal.getmStartTime() > 0) {
//                mSparkTimer.resetValues(1, mSparkModal.getmStartTime(), mSparkModal.getmExpiredTimeInSeconds() * 1000);
////                mAnalogTimerView.reconfigure((int)mSparkModal.getmExpiredTimeInSeconds(), mSparkTimer.getElapsedTimeWrtStartTime());
//                mSparkTimer.start(mSparkTimerInterface);
//            }


                timeLeft = TimeUtils.getTimeoutDifference(
                        mSparkModal.getmStartTime(), mSparkModal.getmExpiredTimeInSeconds() * 1000);
                timeLeft = timeLeft / 1000;

                if (mExpiryRunnable == null) {
                    mExpiryRunnable = new Runnable() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    updateSpark(SparkModal.SPARK_STATUS.rejected, false);
                                }
                            });
                        }
                    };
                } else {
                    mHandler.removeCallbacks(mExpiryRunnable);
                }
                mHandler.postDelayed(mExpiryRunnable, timeLeft * 1000);
                mSparkExpiryWarningContainer.setVisibility(View.VISIBLE);
            } else {
                mSparkExpiryWarningContainer.setVisibility(View.GONE);
            }
            mTimerTV.setText(TimeUtils.getMinFormattedLeftTime(timeLeft));
        }
    }

    @Subscribe
    public void onAdEventFired(AdEvent adEvent) {
        if (adEvent != null) {
            switch (adEvent.getmAction()) {
                case AdEvent.AD_LOAD_FAILURE:
                    mAdsHelper.adLoaded(AdsType.CONV_LIST_NATIVE, false);
                    break;
                case AdEvent.AD_LOAD_SUCCESS:
                    mAdsHelper.adLoaded(AdsType.CONV_LIST_NATIVE, true);
                    break;
            }
        }
    }

    @Subscribe
    public void triggerSimpleAction(SimpleAction simpleAction) {
        switch (simpleAction.getmAction()) {
            case SimpleAction.ACTION_SPARK_ACCEPTED:
                String matchId = simpleAction.getmStringValue();
                triggerUpdateConversationList(new TriggerConversationListUpdateEvent());
                if (mSparkModal != null && Utility.stringCompare(mSparkModal.getmMatchId(), matchId)) {
                    if (mAcceptSparkRunnable != null) {
                        mHandler.removeCallbacks(mAcceptSparkRunnable);
                    }
                    mHandler.postDelayed(mAcceptSparkRunnable, 418);
                }
                break;
        }
    }

    private void updateSpark(SparkModal.SPARK_STATUS sparkStatus, boolean callServer) {
        if (!callServer) {
            mSparksReceiverHandler.sparkStatusChanged(mSparkModal, sparkStatus, null);
        } else {
            mSparksReceiverHandler.updateSparkStatus(mSparkModal, sparkStatus);
        }
    }

    private void setAndProcessNextSpark(SparkModal sparkModal) {
        mSparkModal = sparkModal;
        if (mSparkModal != null) {
            mSparkMessageMetaData = MessageDBHandler.getMatchMessageMetaData(mSparkModal.getmUserId(), mSparkModal.getmMatchId(), aContext);
        }

        if (mSparkModal != null) {
            SPHandler.setInt(aContext, ConstantsSP.SHARED_PREF_SPARK_TUTORIAL_SHOWN, Constants.SPARK_RECEIVER_TUTORIAL_MAX_COUNT);
            toggleMutualMatchesHeader(true);
            if (mSparkModal.getmStartTime() == 0L) {
//                mAnalogTimerView.reconfigure(100, 100);
                updateSpark(SparkModal.SPARK_STATUS.seen, true);
            }

            startSparkTimer();
            if (mSparkModal.getmStartTime() > 0) {
                revealSparkMessage(418);
            }
            if (mSwipeListener == null) {
                mSwipeListener = new SwipeListener((ViewGroup) mSparkInfoCard.getParent()) {

                    @Override
                    public void onClicked() {
                        onSparkClicked();
                    }

                    @Override
                    public void onLeftClicked() {
                        //match_id, time_left, sparks_left
                        HashMap<String, String> eventInfo = new HashMap<>();
                        if (mSparkModal != null) {
                            eventInfo.put("match_id", (mSparkModal != null) ? mSparkModal.getmMatchId() : "");
                            eventInfo.put("time_left", String.valueOf(TimeUtils.getTimeoutDifference(mSparkModal.getmStartTime(),
                                    mSparkModal.getmExpiredTimeInSeconds() * 1000) / 1000));
                        }
                        eventInfo.put("sparks_left", String.valueOf(SparksDbHandler.getPendingSparksCount(aContext,
                                Utility.getMyId(aContext), mSparkModal.getmMatchId())));
                        TrulyMadlyTrackEvent.trackEvent(aContext, sparks, TrulyMadlyEvent.TrulyMadlyEventTypes.conv_list, 0, TrulyMadlyEventStatus.say_hi,
                                eventInfo, true, true);
                        ActivityHandler.startMessageOneOnOneActivity(aContext, mSparkModal.getmMatchId(),
                                mSparkMessageMetaData.getMessage_link(), aContext.getString(R.string.hi));
                    }

                    @Override
                    public void onRightClicked() {
                        //match_id, time_left, sparks_left
                        final HashMap<String, String> eventInfo = new HashMap<>();
                        if (mSparkModal != null) {
                            eventInfo.put("match_id", (mSparkModal != null) ? mSparkModal.getmMatchId() : "");
                            eventInfo.put("time_left", String.valueOf(TimeUtils.getTimeoutDifference(mSparkModal.getmStartTime(),
                                    mSparkModal.getmExpiredTimeInSeconds() * 1000) / 1000));
                        }
                        eventInfo.put("sparks_left", String.valueOf(SparksDbHandler.getPendingSparksCount(aContext,
                                Utility.getMyId(aContext), mSparkModal.getmMatchId())));
                        TrulyMadlyTrackEvent.trackEvent(aContext, sparks, TrulyMadlyEvent.TrulyMadlyEventTypes.conv_list, 0,
                                TrulyMadlyEventStatus.delete_clicked, eventInfo, true, true);
                        AlertsHandler.showConfirmDialog(aContext, R.string.are_you_sure_delete_spark,
                                R.string.yes, R.string.no, new ConfirmDialogInterface() {
                                    @Override
                                    public void onPositiveButtonSelected() {
                                        TrulyMadlyTrackEvent.trackEvent(aContext, sparks, TrulyMadlyEvent.TrulyMadlyEventTypes.conv_list, 0,
                                                TrulyMadlyEventStatus.delete_confirmed,
                                                eventInfo, true, true);
                                        mProgressDialog = UiUtils.showProgressBar(aContext, mProgressDialog);
                                        updateSpark(SparkModal.SPARK_STATUS.rejected, true);
                                    }

                                    @Override
                                    public void onNegativeButtonSelected() {

                                    }
                                });
                    }
                };
            }
            mSparkInfoCard.setOnTouchListener(mSwipeListener);
            mSwipeListener.hardReset(mSparkInfoCard);
            mSparkContainer.setVisibility(View.VISIBLE);
            mSparkNameTV.setVisibility(View.VISIBLE);
            mSparkDesignationTV.setVisibility(View.VISIBLE);
//            mAnalogTimerView.setVisibility(View.VISIBLE);
            sparks_indicator_iv.setVisibility(View.VISIBLE);
            spark_stack_separator.setVisibility(View.VISIBLE);
            mSparkProfilePic.setBackground(null);
            spark_info_innerview.setBackground(ActivityCompat.getDrawable(aContext, R.drawable.spark_card_background));
            String nameAgeText = mSparkMessageMetaData.getFName();
            if (Utility.isSet(mSparkMessageMetaData.getAge())) {
                nameAgeText += ", " + mSparkMessageMetaData.getAge();
            }
            mSparkNameTV.setText(nameAgeText);

            String designation = mSparkModal.getmDesignation();
            if (Utility.isSet(designation)) {
                mSparkDesignationTV.setVisibility(View.VISIBLE);
                mSparkDesignationTV.setText(designation);
            } else {
                mSparkDesignationTV.setVisibility(View.GONE);
            }

            String message = mSparkModal.getmMessage();
            if (Utility.isSet(message)) {
                mSparkMessageTV.setVisibility(View.VISIBLE);
                mSparkMessageTV.setText(message);
            } else {
                mSparkMessageTV.setVisibility(View.GONE);
            }

            Picasso.with(aContext).load(mSparkMessageMetaData.getProfilePic())
                    .placeholder(UiUtils.getProfileDummyImageForMatch(aContext))
                    .transform(new CircleTransformation())
                    .into(mSparkProfilePic);

            boolean isSelect = TMSelectHandler.isSelectEnabled(aContext) && mSparkMessageMetaData.isSelectMatch();
            //mSparkSelectLabel.setVisibility((isSelect) ? View.VISIBLE : View.GONE);
            mSparkNameTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tm_select_label_full, 0);

            int remainingSparksCount = SparksDbHandler.getPendingSparksCount(aContext,
                    Utility.getMyId(aContext), mSparkModal.getmMatchId());
            switch (remainingSparksCount) {
                case 0:
                    mMiddleCard.setVisibility(View.GONE);
                    mBottomCard.setVisibility(View.GONE);
                    mBlurredSparkIV.setVisibility(View.GONE);
                    break;
                case 1:
                    mMiddleCard.setVisibility(View.VISIBLE);
                    mBottomCard.setVisibility(View.GONE);
                    mBlurredSparkIV.setVisibility(View.VISIBLE);
                    Picasso.with(aContext).load(R.drawable.sparks_conv_blur).into(mBlurredSparkIV);
                    break;
                default:
                    mMiddleCard.setVisibility(View.VISIBLE);
                    mBottomCard.setVisibility(View.VISIBLE);
                    mBlurredSparkIV.setVisibility(View.VISIBLE);
                    Picasso.with(aContext).load(R.drawable.sparks_conv_blur).into(mBlurredSparkIV);
                    break;
            }

//            if (SparksHandler.shouldShowSparkConvAnim(aContext)) {
//                mSparkInfoCard.post(new Runnable() {
//                    @Override
//                    public void run() {
//                        mSwipeListener.startTutorialAnimation(mSparkInfoCard, null);
//                        SparksHandler.incSparkConvAnimCounter(aContext);
//                    }
//                });
//            }

        } else {
            toggleSparkTutorial(true);
        }
    }

    private void showSparkCardOptionsAnimation() {
        if (SparksHandler.shouldShowSparkConvAnim(aContext)) {
            mSparkInfoCard.post(new Runnable() {
                @Override
                public void run() {
                    mSwipeListener.startTutorialAnimation(mSparkInfoCard, null);
                    SparksHandler.incSparkConvAnimCounter(aContext);
                }
            });
        }
    }

    private void checkAndLoadSpark() {
        if (!SparksHandler.isSparksEnabled(aContext)) {
            return;
        }
        if (mSparksReceiverListener == null) {
            mSparksReceiverListener = new SparksHandler.SparksReceiver.SparksReceiverListener() {

                @Override
                public void loadSpark(SparkModal sparkModal) {
                    if (!isSparkTutorialRunning) {
                        setAndProcessNextSpark(sparkModal);
                    }

                    if (sparkModal != null) {
                        SPHandler.setInt(aContext, ConstantsSP.SHARED_PREF_SPARK_TUTORIAL_SHOWN, 2);
                    }
                }

                @Override
                public void onSparkfetchSuccess(int sparksFetched) {
                    areSparksFetchedOnce = true;
                }

                @Override
                public void onSparkfetchFailure() {
                    if (!isSparkTutorialRunning && mSparkModal != null) {
                        setAndProcessNextSpark(null);
                    }
                }

                @Override
                public void onSparkUpdateFailed(String userId, String matchId, SparkModal.SPARK_STATUS sparkStatus) {
//                    AlertsHandler.showMessage(aActivity, "Spark update failed");
                    Activity activity = weakActivity.get();
                    if (activity != null) {
                        AlertsHandler.showMessage(activity, aContext.getString(Utility.getNetworkErrorStringResid(aContext, null)));
                    }
                    UiUtils.hideProgressBar(mProgressDialog);
                }

                @Override
                public void onSparkUpdateSuccess(String userId, String matchId, SparkModal.SPARK_STATUS sparkStatus) {
                    if (mSparkModal != null && userId.equalsIgnoreCase(mSparkModal.getmUserId()) && matchId.equalsIgnoreCase(mSparkModal.getmMatchId())) {
                        switch (sparkStatus) {
                            case seen:
                                long currentTime = Calendar.getInstance().getTimeInMillis();
                                mSparkModal.setmStartTime(currentTime);
                                SparksHandler.resetSparkWarningShownCounter(aContext);
                                startSparkTimer();
                                revealSparkMessage(418);
                                //Hiding digital timer on top of spark image
                                //showDigitalTimer(TimeUtils.getTimeoutDifference(mSparkModal.getmStartTime(), mSparkModal.getmExpiredTimeInSeconds()*1000)/1000);
                                break;
                            case rejected:
                                if (mHandler != null) {
                                    mHandler.removeCallbacks(mExpiryRunnable);
                                }
                                mProgressDialog = UiUtils.hideProgressBar(mProgressDialog);
                                mSwipeListener.slideLeft(mSparkInfoCard, new Animation.AnimationListener() {
                                    @Override
                                    public void onAnimationStart(Animation animation) {

                                    }

                                    @Override
                                    public void onAnimationEnd(Animation animation) {
                                        checkAndLoadSpark();
                                        mSwipeListener.hardReset(mSparkInfoCard);
                                    }

                                    @Override
                                    public void onAnimationRepeat(Animation animation) {

                                    }
                                });
                                break;
                        }
                    }
                }

            };
        }

        if (mSparksReceiverHandler == null) {
            mSparksReceiverHandler = new SparksHandler.SparksReceiver(aContext, mSparksReceiverListener);
        }

        //Checking if anyone of the following conditions are satisfied before fetching the next spark:
        //1. No spark is loaded right now
        //2. Spark loaded is not valid - i.e. already accepted or expired
        //3. Spark loaded doesn't exists anymore - in case the spark got deleted
        if (mSparkModal == null || !SparksHandler.isValidSpark(mSparkModal, mSparkMessageMetaData)
                || !SparksHandler.isSparkExists(aContext, mSparkModal)) {
            mSparksReceiverHandler.getAndFetchSparks(!areSparksFetchedOnce);
        } else {
//            revealSparkMessage();
        }
    }

    private void showDigitalTimer(long timeLeft) {
        if (mHideDigitalTimerRunnable == null) {
            mHideDigitalTimerRunnable = new Runnable() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mDigitalTimerContainer.setVisibility(View.GONE);
                        }
                    });
                }
            };
            mDigitalTimerTV.setText(TimeUtils.getFormattedLeftTime(timeLeft));
            int textId = R.string.mins_left;
            if (timeLeft == 60 * 60) {
                textId = R.string.hour_left;
            } else if (timeLeft >= 2 * 60 * 60) {
                textId = R.string.hours_left;
            }
            mDigitalTimerLeftTV.setText(textId);
            mDigitalTimerContainer.setVisibility(View.VISIBLE);
            mHandler.postDelayed(mHideDigitalTimerRunnable, 5000);
        }
    }

    private void revealSparkMessage(long delay) {
        if (delay == 0) {
            mSparkRevealview.reveal(UiUtils.dpToPx(5), (int) (-mSparkRevealview.getHeight() * 0.2f),
                    mRevealColor, 0, 618, 818, new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mSparkRevealview.hide(UiUtils.dpToPx(5), (int) (-mSparkRevealview.getHeight() * 0.2f),
                                    Color.TRANSPARENT, 0, 418, SPARK_TAKE_ANIMATION_VISIBLE_DURATION, null);
                            mSparkTakeActionContainer.startAnimation(mHideSparkMsgAnimation);
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    });
            if (mShowSparkMsgAnimation == null) {
                mShowSparkMsgAnimation = new AlphaAnimation(0, 1);
                mShowSparkMsgAnimation.setDuration(318);
                mShowSparkMsgAnimation.setStartOffset(818);
                mShowSparkMsgAnimation.setFillAfter(true);
                mShowSparkMsgAnimation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        mSparkTakeActionContainer.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }

            if (mHideSparkMsgAnimation == null) {
                mHideSparkMsgAnimation = new AlphaAnimation(1, 0);
                mHideSparkMsgAnimation.setDuration(318);
                mHideSparkMsgAnimation.setStartOffset(SPARK_TAKE_ANIMATION_VISIBLE_DURATION);
                mHideSparkMsgAnimation.setFillAfter(true);
                mHideSparkMsgAnimation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        mSparkTakeActionContainer.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        mSparkTakeActionContainer.setVisibility(View.INVISIBLE);
                        if (weakActivity.get() != null && isResumed) {
                            showSparkCardOptionsAnimation();
                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }

            mSparkTakeActionContainer.startAnimation(mShowSparkMsgAnimation);
        } else {
            if (mRevealSparkRunnable == null) {
                mRevealSparkRunnable = new Runnable() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                revealSparkMessage(0);
                            }
                        });
                    }
                };
            }

            if (!isSparkrevealShownOnce && SparksHandler.getSparkWarningShownCounter(aContext) < 2) {
                isSparkrevealShownOnce = true;
                mHandler.postDelayed(mRevealSparkRunnable, delay);
                SparksHandler.incSparkWarningShownCounter(aContext);
            } else {
                if (weakActivity.get() != null && isResumed) {
                    showSparkCardOptionsAnimation();
                }
            }
        }
    }

    @OnClick(R.id.spark_info_card)
    public void onSparkClicked() {

        //Spark exists check is required to prevent user from opening the spark
        //while we are removing it from the UI
        if (mSparkModal != null && SparksHandler.isSparkExists(aContext, mSparkModal)) {

            //match_id, time_left, sparks_left
            HashMap<String, String> eventInfo = new HashMap<>();
            eventInfo.put("match_id", mSparkModal.getmMatchId());
            eventInfo.put("sparks_left", String.valueOf(SparksDbHandler.getPendingSparksCount(aContext,
                    Utility.getMyId(aContext))));
            eventInfo.put("time_left", String.valueOf(TimeUtils.getTimeoutDifference(mSparkModal.getmStartTime(),
                    mSparkModal.getmExpiredTimeInSeconds() * 1000) / 1000));
            TrulyMadlyTrackEvent.trackEvent(aContext, sparks, TrulyMadlyEvent.TrulyMadlyEventTypes.conv_list, 0, clicked,
                    eventInfo, true, true);

            ActivityHandler.startMessageOneonOneActivity(aContext, mSparkModal.getmMatchId(),
                    mSparkMessageMetaData.getMessage_link(), false, false, true, true);
        }
//        else if(isSparkTutorialRunning){
//            toggleSparkTutorial(false);
//        }
    }

    private void toggleSparkTutorial(boolean showSparkTutorial) {
        if (showSparkTutorial) {
            int currentSparkTutorialCounter = SPHandler.getInt(aContext, ConstantsSP.SHARED_PREF_SPARK_TUTORIAL_SHOWN, 0);
            if (currentSparkTutorialCounter < 2) {
                isSparkTutorialRunning = true;
                SPHandler.setInt(aContext, ConstantsSP.SHARED_PREF_SPARK_TUTORIAL_SHOWN, currentSparkTutorialCounter + 1);
                mSparkContainer.setVisibility(View.VISIBLE);
//                mAnalogTimerView.setVisibility(View.GONE);
                mSparkNameTV.setVisibility(View.GONE);
                mSparkDesignationTV.setVisibility(View.GONE);
                mSparkMessageTV.setText(aContext.getString(R.string.sparks_receiver_tutorial));
                mMiddleCard.setVisibility(View.GONE);
                mBottomCard.setVisibility(View.GONE);
                mBlurredSparkIV.setVisibility(View.GONE);
                sparks_indicator_iv.setVisibility(View.GONE);
                spark_stack_separator.setVisibility(View.GONE);
                mSparkProfilePic.setBackground(ActivityCompat.getDrawable(aContext, R.drawable.circle_dash_border_blue));
//                int px = UiUtils.dpToPx(2);
//                mSparkProfilePic.setPadding(px, px, px, px);
                Picasso.with(aContext).load(R.drawable.spark_dummy_image)
                        .transform(new CircleTransformation()).noFade().into(mSparkProfilePic);
                spark_info_innerview.setBackground(ActivityCompat.getDrawable(aContext, R.drawable.border_dashed_blue_compact));
                toggleMutualMatchesHeader(true);
            } else {
//                int px = UiUtils.dpToPx(10);
//                mSparkProfilePic.setPadding(px, px, px, px);
                isSparkTutorialRunning = false;
                mSparkContainer.setVisibility(View.GONE);
                toggleMutualMatchesHeader(false);
            }
        } else {
//            int px = UiUtils.dpToPx(10);
//            mSparkProfilePic.setPadding(px, px, px, px);
            isSparkTutorialRunning = false;
            mSparkContainer.setVisibility(View.GONE);
            toggleMutualMatchesHeader(false);
            checkAndLoadSpark();
        }
    }

    protected void onShareClicked(View v) {
        final ConversationModal c = (ConversationModal) v.getTag();

        Map<String, String> params = new HashMap<>();
        params.put("shorten_url_conversationlist", "1");
        params.put("match_id", c.getMatch_id());
        params.put("source", "converstion_list");
        CustomOkHttpResponseHandler responseHandler = new CustomOkHttpResponseHandler(aContext,
                TrulyMadlyActivities.conversation_list, TrulyMadlyEventTypes.ask_friend_click) {

            @Override
            public void onRequestSuccess(JSONObject responseJSON) {
                mProgressDialog = UiUtils.hideProgressBar(mProgressDialog);

                switch (responseJSON.optInt("responseCode")) {
                    case 200:
                        final JSONObject data = responseJSON.optJSONObject("response");
                        ConfirmDialogInterface confirmDialogInterface = new ConfirmDialogInterface() {

                            @Override
                            public void onPositiveButtonSelected() {
                                TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyActivities.conversation_list,
                                        TrulyMadlyEventTypes.ask_friend_confirm, 0, TrulyMadlyEventStatus.matched, null, true);

                                Activity activity = weakActivity.get();
                                if (activity != null) {
                                    ActivityHandler.createTextIntent(activity,
                                            data.optString("share_message") + " " + data.optString("link"), false);
                                }

                            }

                            @Override
                            public void onNegativeButtonSelected() {
                                TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyActivities.conversation_list,
                                        TrulyMadlyEventTypes.ask_friend_cancel, 0, TrulyMadlyEventStatus.matched, null, true);

                            }
                        };
                        AlertsHandler.showConfirmDialog(aContext, data.optString("alert_message"), R.string.share,
                                R.string.cancel, confirmDialogInterface, false);
                        break;

                    case 403:
                        Activity activity = weakActivity.get();
                        if (activity != null) {
                            AlertsHandler.showAlertDialog(activity, responseJSON.optString("error"), null);
                        }
                        break;
                    default:
                        break;

                }
            }

            @Override
            public void onRequestFailure(Exception exception) {
                mProgressDialog = UiUtils.hideProgressBar(mProgressDialog);
                Activity activity = weakActivity.get();
                if (activity != null) {
                    AlertsHandler.showNetworkError(activity, exception);
                }

            }

        };
        OkHttpHandler.httpGet(aContext, ConstantsUrls.get_getAskFriendsUrl(), params, responseHandler);
        mProgressDialog = UiUtils.showProgressBar(aContext, mProgressDialog);
    }

    private void onUnmatchClicked(View v) {
        final ConversationModal c = (ConversationModal) v.getTag();
        ConfirmDialogInterface confirmDialogInterface = new ConfirmDialogInterface() {

            @Override
            public void onPositiveButtonSelected() {
                CustomOkHttpResponseHandler okHttpResponseHandler = new CustomOkHttpResponseHandler(aContext) {
                    @Override
                    public void onRequestSuccess(JSONObject responseJSON) {
                        UiUtils.hideProgressBar(mProgressDialog);
                        MessageDBHandler.setUserBlocked(aContext, c.getMatch_id());
                        Activity activity = weakActivity.get();
                        if (activity != null) {
                            AlertsHandler.showMessage(activity, R.string.unmatch_successfull, false);
                        }
                        conversation_swipelist_view.closeOpenedItems();
                        triggerUpdateConversationList(new TriggerConversationListUpdateEvent());

                        //Removing image from local cache after unmatching that match
                        ImageCacheHelper.removeImageWithUniqueKey(c.getMatch_id());
                    }

                    @Override
                    public void onRequestFailure(Exception exception) {
                        UiUtils.hideProgressBar(mProgressDialog);
                        Activity activity = weakActivity.get();
                        if (activity != null) {
                            AlertsHandler.showNetworkError(activity, exception);
                        }
                    }
                };
                mProgressDialog = UiUtils.showProgressBar(aContext, mProgressDialog);
                ReportAbuseButtonOnClickListner.sendReportAbuseReasonToServer(aContext,
                        getResources().getString(R.string.report_reason_not_interested), "", c.getMessage_link(),
                        okHttpResponseHandler);
            }

            @Override
            public void onNegativeButtonSelected() {

            }
        };
        String gender;
        if (Utility.isMale(aContext)) {
            gender = "her";
        } else {
            gender = "him";
        }
        AlertsHandler.showConfirmDialog(aContext, getResources().getString(R.string.unmatch) + " " + c.getFname()
                        + "?\n\n" + "This will clear all chat history and remove " + gender + " permanently from your matches.",
                R.string.unmatch, R.string.nope, confirmDialogInterface, true);
    }

    private void toggleTransparency(final boolean show) {
        int anim = R.anim.fadeout;
        if (show) {
            anim = R.anim.fade_in;
        }

        Animation animation = AnimationUtils.loadAnimation(aContext, anim);
        animation.setFillAfter(true);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                mTransparencyOverlay.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (show)
                    mTransparencyOverlay.setVisibility(View.VISIBLE);
                else
                    mTransparencyOverlay.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        mActionBarHandler.toogleToolbarOpacity(aContext, !show);
        mTransparencyOverlay.startAnimation(animation);
    }

    class BlockedShownListener implements DialogInterface.OnClickListener {

        final int index;

        public BlockedShownListener(int index) {
            this.index = index;
        }

        public void onClick(DialogInterface dialog, int which) {
            try {
                ConversationModal c = conversationListAdapter.getConversation(index);
                MessageDBHandler.setUserBlocked(aContext, c.getMatch_id());
                CustomOkHttpResponseHandler okHttpResponseHandler = new CustomOkHttpResponseHandler(aContext) {
                    @Override
                    public void onRequestSuccess(JSONObject responseJSON) {
                        fetchConversationData(false);
                    }

                    @Override
                    public void onRequestFailure(Exception exception) {
                    }
                };
                Map<String, String> params = new HashMap<>();
                params.put("block_shown", "block_shown");
                params.put("block_shown_id", c.getMatch_id());

                OkHttpHandler.httpPost(aContext, ConstantsUrls.get_message_url(), params, okHttpResponseHandler);

            } catch (CursorIndexOutOfBoundsException ignored) {
            }
        }
    }

    private class GetConversationsAsyncTask extends AsyncTask<Void, Void, Cursor> {

        private boolean isServerConnectionAttemptRequired = false, isFetchingRequired = false;

        public GetConversationsAsyncTask(boolean isServerConnectionAttemptRequired, boolean isFetchingRequired) {
            this.isServerConnectionAttemptRequired = isServerConnectionAttemptRequired;
            this.isFetchingRequired = isFetchingRequired;
        }

        @Override
        protected Cursor doInBackground(Void... voids) {
            return MessageDBHandler.getConversationDetails(Utility.getMyId(aContext), aContext,
                    Utility.isSet(mSearchString) ? mSearchString : null);
        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            Activity activity = weakActivity.get();
            Log.d("GetConvTask", "isCreated : " + isCreated + " :: activity : " + (activity != null));
            if (activity != null) {
                if (isFetchingRequired && !Utility.isSet(mSearchString)) {
                    if (cursor == null || (isServerConnectionAttemptRequired || cursor.getCount() > 0)) {
                        getConversationListFromServerOnUiThread(cursor != null && cursor.getCount() == 0);
                    }
                }

                showConversationList(cursor);
            } else if (cursor != null) {
                cursor.close();
            }
        }
    }
}
