package com.trulymadly.android.app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.IntentCompat;

import com.moe.pushlibrary.MoEHelper;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.Constants.EditProfilePageType;
import com.trulymadly.android.app.json.Constants.HttpRequestType;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.json.ConstantsSocket.SOCKET_END;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.json.ParseUserData;
import com.trulymadly.android.app.listener.CustomOkHttpResponseHandler;
import com.trulymadly.android.app.listener.OnDataFetchedInterface;
import com.trulymadly.android.app.modal.EditPrefBasicDataModal;
import com.trulymadly.android.app.modal.LoginDetail;
import com.trulymadly.android.app.modal.UserData;
import com.trulymadly.android.app.sqlite.CachingDBHandler;
import com.trulymadly.android.app.sqlite.EditPrefDBHandler;
import com.trulymadly.android.app.sqlite.MatchesDbHandler;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.MoEHandler;
import com.trulymadly.android.app.utility.NotificationHandler;
import com.trulymadly.android.app.utility.OkHttpHandler;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class HttpTasks {

    public static void login(final LoginDetail aLoginDetail,
                             final Context aContext,
                             CustomOkHttpResponseHandler loginHandlerResponse) {

        OkHttpHandler.NET_TIMEOUT timeoutType = OkHttpHandler.NET_TIMEOUT.NORMAL;

        Map<String, String> params = new HashMap<>();
        if (aLoginDetail.getIsFromFB()) {
            params = GetOkHttpRequestParams.getHttpRequestParams(
                    HttpRequestType.LOGIN_FB, aLoginDetail);
            loginHandlerResponse
                    .setEventType(TrulyMadlyEventTypes.fb_server_call);
            timeoutType = OkHttpHandler.NET_TIMEOUT.LARGE;
        } else {
            loginHandlerResponse
                    .setEventType(TrulyMadlyEventTypes.login_via_email);
            params = GetOkHttpRequestParams.getHttpRequestParams(
                    HttpRequestType.LOGIN_EMAIL, aLoginDetail);
        }
        params.put("device_id", Utility.getDeviceId(aContext));

        loginHandlerResponse.setStartTime();
        OkHttpHandler.httpPost(aContext, ConstantsUrls.get_login_url(), params,
                loginHandlerResponse, timeoutType);
    }

    public static void logout(final Context aContext,
                              final ProgressDialog mProgressDialog) {

        final String reg_id = SPHandler.getString(aContext,
                ConstantsSP.SHARED_KEYS_REG_ID);
        TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyActivities.GCM,
                TrulyMadlyEventTypes.logout, 0, reg_id, null, true);
        final Map<String, String> params = GetOkHttpRequestParams
                .getHttpRequestParams(HttpRequestType.LOGOUT, reg_id);

        final CustomOkHttpResponseHandler responseHandler = new CustomOkHttpResponseHandler(
                aContext, TrulyMadlyActivities.logout,
                TrulyMadlyEventTypes.logout_server_call) {

            @Override
            public void onRequestSuccess(JSONObject responseJSON) {
                MoEHelper.getInstance(aContext).logoutUser();
                MatchesDbHandler.clearMatchesCache(aContext);
                Utility.clearMyId(aContext);
                SPHandler.clear(aContext);
                NotificationHandler.clearAllNotifications(aContext);
                OkHttpHandler.clearAllCookies(aContext);
                UiUtils.hideProgressBar(mProgressDialog);
                TrulyMadlyApplication.stopService(aContext, SOCKET_END.logout);
                Intent intent = new Intent(aContext, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                        | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);

                try {
                    aContext.startActivity(intent);
                } catch (NullPointerException e) {
                    // gionee
                }
                try {
                    ((Activity) aContext).finish();
                } catch (ClassCastException ignored) {
                }
            }

            @Override
            public void onRequestFailure(Exception exception) {
                UiUtils.hideProgressBar(mProgressDialog);
                AlertsHandler.showNetworkError((Activity) aContext, exception);

            }
        };
        CustomOkHttpResponseHandler regIdResponseHandler = new CustomOkHttpResponseHandler(
                aContext) {

            @Override
            public void onRequestSuccess(JSONObject responseJSON) {
                Utility.storeRegistrationId(reg_id, aContext);
                OkHttpHandler.httpPost(aContext, ConstantsUrls.get_logout_url(), params,
                        responseHandler);

            }

            @Override
            public void onRequestFailure(Exception exception) {
                UiUtils.hideProgressBar(mProgressDialog);
                AlertsHandler.showNetworkError((Activity) aContext, exception);

            }

        };

        Utility.sendRegistrationIdToBackend(reg_id, regIdResponseHandler,
                aContext, TrulyMadlyEventTypes.logout);

    }

    public static void saveBasicDataOnServer(UserData userData,
                                             Context aContext, CustomOkHttpResponseHandler responseHandler) {

        MoEHandler.trackEvent(aContext, MoEHandler.Events.BASICS_SAVE);

        OkHttpHandler.setCookieByName("TM_S3_StayCountry",
                userData.getCountryId());
        if (Utility.isSet(userData.getStateId())) {
            OkHttpHandler.setCookieByName("TM_S3_StayState",
                    userData.getCountryId() + "-" + userData.getStateId());
        }
        if (Utility.isSet(userData.getCityId())) {
            OkHttpHandler.setCookieByName("TM_S3_StayCity",
                    userData.getCountryId() + "-" + userData.getStateId() + "-"
                            + userData.getCityId());

            ArrayList<EditPrefBasicDataModal> citiesList = EditPrefDBHandler.getBasicDataFromDB(Constants.EDIT_PREF_CITY,
                    aContext, 2);
            if (citiesList != null) {
                for (EditPrefBasicDataModal city : citiesList) {
                    if (city.getCityId().equalsIgnoreCase(userData.getCityId())) {
                        SPHandler.setString(aContext, ConstantsSP.SHARED_KEYS_USER_CITY, city.getName());
                        break;
                    }
                }
            }
        }
        OkHttpHandler.setCookieByName("TM_S1_HeightFoot",
                userData.getHeight_feet());
        OkHttpHandler.setCookieByName("TM_S1_HeightInch",
                userData.getHeight_inches());
        OkHttpHandler.setCookieByName("TM_Designation_0",
                Utility.encodeURI(userData.getCurrentTitle()));
        OkHttpHandler.setCookieByName("TM_interest",
                userData.getInterestsForSaving(true));

        sendingHashtagsToMoengage(userData, aContext);

        OkHttpHandler.setCookieByName("TM_Education_0",
                userData.getHighestDegree());

        OkHttpHandler.setCookieByName("pageInd", "9");

        OkHttpHandler.httpGet(aContext, ConstantsUrls.get_user_data_url(),
                GetOkHttpRequestParams
                        .getHttpRequestParams(HttpRequestType.SAVE_DATA_BASIC),
                responseHandler, OkHttpHandler.NET_TIMEOUT.LARGE);
    }

    private static void sendingHashtagsToMoengage(UserData userData,
                                                  Context aContext) {

        MoEHandler.trackEvent(aContext, MoEHandler.Events.HASHTAGS_SAVE);

        if (userData.getInterests() != null) {

            ArrayList<String> interests = userData.getInterests();
            for (String s : interests) {
                if (Utility.isSet(s)) {
                    JSONObject json = new JSONObject();
                    try {
                        json.put("hashtag", s);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    MoEHandler.trackEvent(aContext, MoEHandler.Events.USER_HASHTAGS, json, true);


                }
            }

        }
    }

    public static void fetchUserData(final Context aContext,
                                     final EditProfilePageType type,
                                     final OnDataFetchedInterface onDataFetched) {

        CustomOkHttpResponseHandler okHttpResponseHandler = new CustomOkHttpResponseHandler(
                aContext, TrulyMadlyActivities.register_basics,
                TrulyMadlyEventTypes.page_load) {

            @Override
            public void onRequestSuccess(JSONObject response) {
                if (response != null) {
                    if (response.optJSONObject("data") != null) {

                        if (response.optJSONObject("data").optJSONObject(
                                "location") != null) {
                            String city = response.optJSONObject("data")
                                    .optJSONObject("location")
                                    .optJSONObject("city").optString("name");
                            if (Utility.isSet(city)) {
                                try {

                                    MoEHelper.getInstance(aContext)
                                            .setUserAttribute("City", city);
                                } catch (NumberFormatException ignored) {
                                }

                            }

                            String state = response.optJSONObject("data")
                                    .optJSONObject("location")
                                    .optJSONObject("state").optString("name");
                            if (Utility.isSet(state)) {
                                try {

                                    MoEHelper.getInstance(aContext)
                                            .setUserAttribute("State", state);
                                } catch (NumberFormatException ignored) {
                                }

                            }
                        }

                    }
                }

                UserData userData = (new ParseUserData())
                        .parseRegisterData(response);
                HttpTasks.fetchStaticData(aContext, userData, type,
                        onDataFetched, TrulyMadlyActivities.register_basics);
            }

            @Override
            public void onRequestFailure(Exception exception) {
                onDataFetched.onFailure(exception);
            }
        };
        OkHttpHandler.httpPost(aContext, ConstantsUrls.get_user_data_url(),
                GetOkHttpRequestParams
                        .getHttpRequestParams(HttpRequestType.FETCH_USER_DATA),
                okHttpResponseHandler);

    }

    public static void fetchStaticData(final Context aContext,
                                       final UserData userData, final EditProfilePageType type,
                                       final OnDataFetchedInterface onDataFetched, String trkActivity) {
        final String url = EditPrefDBHandler.checkBasicDataExists(userData.getDataUrl(),
                aContext);
        if (!Utility.isSet(url)) {
            onDataFetched.onSuccess(type, userData);
        } else {
            CustomOkHttpResponseHandler okHttpResponseHandler = new CustomOkHttpResponseHandler(
                    aContext, trkActivity,
                    TrulyMadlyEventTypes.fetch_static_data) {

                @Override
                public void onRequestSuccess(JSONObject response) {
                    EditPrefDBHandler.insertBasicDataInDB(response, url, aContext);
                    onDataFetched.onSuccess(type, userData);
                }

                @Override
                public void onRequestFailure(Exception exception) {
                    onDataFetched.onFailure(exception);
                }
            };
            OkHttpHandler.httpGet(aContext, userData.getDataUrl(),
                    okHttpResponseHandler);
        }
    }

    public void updateBasicDataOnServer(UserData userData,
                                        EditProfilePageType type, Context aContext,
                                        CustomOkHttpResponseHandler responseHandler) {
        if (type == null) {
            responseHandler.onRequestFailure(null);
            return;
        }

        Map<String, String> params = new HashMap<>();

        MoEHandler.trackEvent(aContext, MoEHandler.Events.BASICS_SAVE, null, true);
        switch (type) {
            case AGE:
                params.put("param", "basics");
                params.put("fname", userData.getFname());
                params.put("lname", userData.getLname());
                params.put("TM_S1_WhenBornDay", userData.getDob_d() + "");
                params.put("TM_S1_WhenBornMonth", userData.getDob_m() + "");
                params.put("TM_S1_WhenBornYear", userData.getDob_y() + "");
                params.put("TM_S3_StayCountry", userData.getCountryId());
                params.put("TM_S3_StayState", userData.getCountryId() + "-"
                        + userData.getStateId());
                params.put("TM_S3_StayCity", userData.getCountryId() + "-"
                        + userData.getStateId() + "-" + userData.getCityId());
                params.put("TM_S1_HeightFoot", userData.getHeight_feet());
                params.put("TM_S1_HeightInch", userData.getHeight_inches());
                break;
            case INTERESTS:
                if (userData.getInterests() != null) {
                    params.put("param", "interest");
                    params.put("TM_interest", userData.getInterestsForSaving(false));
                    sendingHashtagsToMoengage(userData, aContext);
                }
                break;
            case PROFESSION:
                params.put("param", "worknew");
                params.put("TM_Designation_0",
                        Utility.encodeURI(userData.getCurrentTitle()));
                if (userData.getCompanies() != null) {
                    params.put("TM_Company_name_0",
                            Utility.encodeURI(userData.getCompaniesForSaving()));
                }
                if (userData.getIsNotWorking()) {
                    params.put("TM_Working_area", "no");
                }
                if (userData.getIncome() != null) {
                    String[] incomeStrings = userData.getIncome().split("-");
                    if (incomeStrings.length == 2) {
                        params.put("TM_Income_start", incomeStrings[0]);
                        params.put("TM_Income_end", incomeStrings[1]);
                    }
                }
                break;
            case EDUCATION:
                params.put("param", "education");
                params.put("TM_Education_0", userData.getHighestDegree());
                if (userData.getColleges() != null) {
                    params.put("TM_Institute_0",
                            Utility.encodeURI(userData.getCollegesForSaving()));
                }
                break;
            default:
                break;
        }
//		deleting my profile data n editprofile data


        CachingDBHandler.deleteURL(aContext, ConstantsUrls.get_user_edit_url(), Utility.getMyId(aContext));
        CachingDBHandler.deleteURL(aContext, ConstantsUrls.get_my_profile_url(), Utility.getMyId(aContext));
        MatchesDbHandler.clearMatchesCache(aContext);

        OkHttpHandler.httpPost(aContext, ConstantsUrls.get_user_update_url(), params,
                responseHandler);
    }

}
