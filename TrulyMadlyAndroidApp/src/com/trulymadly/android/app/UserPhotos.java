package com.trulymadly.android.app;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.MediaCodecInfo;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferType;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.crashlytics.android.Crashlytics;
import com.facebook.CallbackManager;
import com.moe.pushlibrary.MoEHelper;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;
import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.adapter.FacebookLoginCallback;
import com.trulymadly.android.app.adapter.FacebookLoginCallback.FacebookLoginCallbackInterface;
import com.trulymadly.android.app.asynctasks.PhotoUploader;
import com.trulymadly.android.app.asynctasks.PhotoUploader.PhotoUploaderRequestInterface;
import com.trulymadly.android.app.bus.BusProvider;
import com.trulymadly.android.app.bus.ImageUploadedProgress;
import com.trulymadly.android.app.facebookalbum.FacebookAlbumActivity;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.Constants.HttpRequestType;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.json.PhotoResponseParser;
import com.trulymadly.android.app.listener.CachedDataInterface;
import com.trulymadly.android.app.listener.ConfirmDialogInterface;
import com.trulymadly.android.app.listener.ConnectFBviaTrustBuilderCallbackInterface;
import com.trulymadly.android.app.listener.CustomOkHttpResponseHandler;
import com.trulymadly.android.app.listener.GetFileCallback;
import com.trulymadly.android.app.listener.OnActionBarClickedInterface;
import com.trulymadly.android.app.listener.TapToRetryListener;
import com.trulymadly.android.app.modal.Photos;
import com.trulymadly.android.app.sqlite.CachingDBHandler;
import com.trulymadly.android.app.utility.AESUtils;
import com.trulymadly.android.app.utility.ActionBarHandler;
import com.trulymadly.android.app.utility.ActivityHandler;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.CachedDataHandler;
import com.trulymadly.android.app.utility.FilesHandler;
import com.trulymadly.android.app.utility.ImageCacheHelper;
import com.trulymadly.android.app.utility.MimeUtils;
import com.trulymadly.android.app.utility.MoEHandler;
import com.trulymadly.android.app.utility.OkHttpHandler;
import com.trulymadly.android.app.utility.PermissionsHelper;
import com.trulymadly.android.app.utility.PhotoSliderHandler;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.TapToRetryHandler;
import com.trulymadly.android.app.utility.TmLogger;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;
import com.trulymadly.android.app.utility.VideoUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.m4m.IProgressListener;
import org.m4m.MediaComposer;
import org.m4m.MediaFile;
import org.m4m.android.AndroidMediaObjectFactory;
import org.m4m.android.AudioFormatAndroid;
import org.m4m.android.MediaExtractorPlugin;
import org.m4m.android.VideoFormatAndroid;
import org.m4m.domain.MediaFormat;
import org.m4m.effects.RotateEffect;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.SSLException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities.photo;
import static com.trulymadly.android.app.utility.Utility.isSet;
import static com.trulymadly.android.app.utility.VideoUtils.VIDEO_DIMENSION_MIN_SIDE;

//import com.trulymadly.android.app.Compression.AndroidMediaObjectFactory;
//import com.trulymadly.android.app.Compression.MediaComposer;


public class UserPhotos extends AppCompatActivity implements OnClickListener {

    private static final int USER_PHOTOS_EDIT_PERMISSION_REQUEST = 1;
    private static final int MY_PHOTOS_FULL = 1;
    private static final int PICK_FROM_FACEBOOK = 5;
    private static final int VIDEO_CAPTURING_CODE = 2;
    private static final String TAG = "UserPhotos";
    public static Photos profilePhoto = null;
    private final ImageView[] user_pic = new ImageView[5];
    private final ImageView[] video_cam_icon = new ImageView[5];
    private final TextView[] user_pic_under_moderation = new TextView[5];
    private final View[] user_pic_upload = new View[5];
    private final RelativeLayout[] user_pic_layout_detail = new RelativeLayout[5];
    private final RelativeLayout[] user_pic_add_more = new RelativeLayout[5];
    public IProgressListener transcodingProgressListener;
    protected org.m4m.AudioFormat audioFormat = null;
    @BindView(R.id.profile_pic_under_moderation_id)
    TextView profile_pic_under_moderation_id;
    @BindView(R.id.profile_pic_logo)
    ImageView profile_pic_logo;
    @BindView(R.id.user_pic_add_more_profile)
    RelativeLayout user_pic_add_more_profile;
    @BindView(R.id.profile_pic_layout_detail)
    RelativeLayout profile_pic_layout_detail;
    @BindView(R.id.secondary_photo_layout_container)
    LinearLayout secondary_photo_layout_container;
    @BindView(R.id.continue_user_photos)
    Button continue_button;
    @BindView(R.id.photo_text_container)
    View photoGuidelinesContainer;
    @BindView(R.id.sliding_background)
    View slidingBackground;
    @BindView(R.id.delete_video_layout)
    View delete_video_layout;
    @BindView(R.id.preview_video_layout)
    View preview_video_layout;
    @BindView(R.id.edit_pic_layout)
    View edit_pic_layout;
    @BindView(R.id.remove_pic_layout)
    View delete_photo_layout;
    //    @BindView(R.id.upload_video_from_gallery_layout)
//    View upload_video_from_gallery_layout;
    @BindView(R.id.make_profile_pic_layout)
    View set_profile_pic_layout;
    @BindView(R.id.take_from_camera_layout)
    View take_from_camera_layout;
    @BindView(R.id.add_from_gallery_layout)
    View add_from_gallery_layout;
    @BindView(R.id.shoot_video_layout)
    View shoot_video_layout;
    @BindView(R.id.import_from_facebook_layout)
    View import_from_facebook_layout;
    @BindView(R.id.show_photo_slider_layout_photos_new)
    RelativeLayout show_photo_slider_layout;
    @BindView(R.id.main_user_photos_layout)
    LinearLayout main_layout;
    @BindView(R.id.crop_layout)
    RelativeLayout crop_layout;
    @BindView(R.id.profile_pic_id)
    ImageView profile_pic_id;
    @BindView(R.id.user_pic_plus_button_profile)
    ImageView user_pic_plus_button_profile;
    @BindView(R.id.user_pic_plus_button_1)
    ImageView user_pic_plus_button_1;
    @BindView(R.id.user_pic_plus_button_2)
    ImageView user_pic_plus_button_2;
    @BindView(R.id.user_pic_plus_button_3)
    ImageView user_pic_plus_button_3;
    @BindView(R.id.user_pic_plus_button_4)
    ImageView user_pic_plus_button_4;
    @BindView(R.id.user_pic_plus_button_5)
    ImageView user_pic_plus_button_5;
    @BindView(R.id.profile_pic_upload)
    View profile_pic_upload;
    private ImageView currentUserPic;
    private HashMap<Integer, ProgressBar> mIdToProgressbarMap;
    private MoEHelper mHelper = null;
    private ProgressDialog mProgressDialog = null;
    private int photosLen = 0;
    private ArrayList<Photos> completePhotosList = null, photosList = null;
    private View cur_pic_upload;
    private Boolean isPhotoUploadSliderShowing = false;
    private Boolean isPhotoGuideLinesVisible = false;
    private Context aContext;
    private Activity aActivity;
    private ConnectFBviaTrustBuilderCallbackInterface connectFBviaTrustBuilderCallbackInterface;
    private TapToRetryHandler tapToRetryHandler;
    private CallbackManager callbackManager;
    private FacebookLoginCallback facebookLoginCallBack;
    private PhotoUploader photoUploader = null;
    private PhotoUploaderRequestInterface photoUploaderRequestInterface;
    private boolean isDisplayPicsCall = false;
    private int lastClickedImage = -1, uploadAtPos = -1;
    private CachedDataInterface cachedDataInterface;
    private boolean isFromPiNudge = false;
    private CognitoCachingCredentialsProvider credentialsProvider;
    private ProgressBar currentProgressBar;//, progressBar1, progressBar2, progressBar3, progressBar4, progressBar5;
    private View uploadView, currentAddMoreView;
    private android.os.Handler handler;
    private boolean isMuted, isVideoUploading, isProfilePictureClicked;
    private int orientationDegree = VideoUtils.VIDEO_ROTATION_DEFAULT;
    private int currentPosition;
    private String outFileDestination, sourceFilePath, uploadedFilePath;
    private boolean isVideoPending = false, isVideoPendingRequestScheduled = false;
    private PhotoSliderHandler mPhotoSliderHandler;
    private HashMap<Integer, Integer> mUserPicLayoutToPositionMap;
    private int mPercentage = 0;
    private Runnable videoPendingRequestRunnable = null;
    private Handler mHandler;
    private AndroidMediaObjectFactory androidMediaObjectFactory = null;
    private MediaComposer mediaComposer = null;
    private MediaFile mediaFile = null;
    private TransferUtility transferUtility = null;
    private long mStartVideoUploadTime = 0, videotimeInMillisec = 0;
    private int videoSizeBeforeCompression = 0, videoSizeAfterCompression = 0;
    private String cameraUsed = null, videoSource = null;
    private boolean video_upload_enabled = true, video_compression_enabled = true, rotateVideoOnApp = false, keepOriginalAudioDuringAudio = true;
    private boolean isTranscodingInProgress = false;

    private void updateProgress(int percentage) {
        if (percentage == 0 || mPercentage < percentage) {
            mPercentage = percentage;
            Log.d(TAG, "progress change " + percentage);
            currentProgressBar.setProgress(percentage);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.photosnew);
        } catch (OutOfMemoryError e) {
            System.gc();
            finish();
            return;
        }

        //http://www.curious-creature.com/2012/12/01/android-performance-case-study/
        //For reducing overdraws : Suggested by Romain guy
        getWindow().setBackgroundDrawable(null);

        ButterKnife.bind(this);
        aContext = this;
        aActivity = this;

        mIdToProgressbarMap = new HashMap<>();
        mHelper = new MoEHelper(this);
        MoEHandler.trackEvent(aContext, MoEHandler.Events.USER_PHOTOS_LAUNCHED);
        uninitialize();


        user_pic_under_moderation[0] = (TextView) findViewById(R.id.user_pic_1_under_moderation);
        user_pic_under_moderation[1] = (TextView) findViewById(R.id.user_pic_2_under_moderation);
        user_pic_under_moderation[2] = (TextView) findViewById(R.id.user_pic_3_under_moderation);
        user_pic_under_moderation[3] = (TextView) findViewById(R.id.user_pic_4_under_moderation);
        user_pic_under_moderation[4] = (TextView) findViewById(R.id.user_pic_5_under_moderation);

        user_pic_add_more[0] = (RelativeLayout) findViewById(R.id.user_pic_add_more_1);
        user_pic_add_more[1] = (RelativeLayout) findViewById(R.id.user_pic_add_more_2);
        user_pic_add_more[2] = (RelativeLayout) findViewById(R.id.user_pic_add_more_3);
        user_pic_add_more[3] = (RelativeLayout) findViewById(R.id.user_pic_add_more_4);
        user_pic_add_more[4] = (RelativeLayout) findViewById(R.id.user_pic_add_more_5);

        user_pic_add_more_profile.setVisibility(VISIBLE);
        for (RelativeLayout upam : user_pic_add_more) {
            upam.setVisibility(VISIBLE);
        }

        user_pic_layout_detail[0] = (RelativeLayout) findViewById(R.id.user_pic_1_layout_detail);
        user_pic_layout_detail[1] = (RelativeLayout) findViewById(R.id.user_pic_2_layout_detail);
        user_pic_layout_detail[2] = (RelativeLayout) findViewById(R.id.user_pic_3_layout_detail);
        user_pic_layout_detail[3] = (RelativeLayout) findViewById(R.id.user_pic_4_layout_detail);
        user_pic_layout_detail[4] = (RelativeLayout) findViewById(R.id.user_pic_5_layout_detail);

        mUserPicLayoutToPositionMap = new HashMap<>();
        mUserPicLayoutToPositionMap.put(R.id.profile_pic_layout_detail, 0);
        mUserPicLayoutToPositionMap.put(R.id.user_pic_1_layout_detail, 1);
        mUserPicLayoutToPositionMap.put(R.id.user_pic_2_layout_detail, 2);
        mUserPicLayoutToPositionMap.put(R.id.user_pic_3_layout_detail, 3);
        mUserPicLayoutToPositionMap.put(R.id.user_pic_4_layout_detail, 4);
        mUserPicLayoutToPositionMap.put(R.id.user_pic_5_layout_detail, 5);


        show_photo_slider_layout.setVisibility(GONE);
        mPhotoSliderHandler = new PhotoSliderHandler(this, show_photo_slider_layout, slidingBackground);

        user_pic[0] = (ImageView) findViewById(R.id.user_pic_1);
        user_pic[1] = (ImageView) findViewById(R.id.user_pic_2);
        user_pic[2] = (ImageView) findViewById(R.id.user_pic_3);
        user_pic[3] = (ImageView) findViewById(R.id.user_pic_4);
        user_pic[4] = (ImageView) findViewById(R.id.user_pic_5);

        video_cam_icon[0] = (ImageView) findViewById(R.id.vd_icon_1);
        video_cam_icon[1] = (ImageView) findViewById(R.id.vd_icon_2);
        video_cam_icon[2] = (ImageView) findViewById(R.id.vd_icon_3);
        video_cam_icon[3] = (ImageView) findViewById(R.id.vd_icon_4);
        video_cam_icon[4] = (ImageView) findViewById(R.id.vd_icon_5);

        user_pic_upload[0] = findViewById(R.id.user_pic_upload_1);
        user_pic_upload[1] = findViewById(R.id.user_pic_upload_2);
        user_pic_upload[2] = findViewById(R.id.user_pic_upload_3);
        user_pic_upload[3] = findViewById(R.id.user_pic_upload_4);
        user_pic_upload[4] = findViewById(R.id.user_pic_upload_5);

        mIdToProgressbarMap.put(0, (ProgressBar) profile_pic_upload.findViewWithTag("cur_pic_upload_progress_bar"));
        mIdToProgressbarMap.put(1, (ProgressBar) user_pic_upload[0].findViewWithTag("cur_pic_upload_progress_bar"));
        mIdToProgressbarMap.put(2, (ProgressBar) user_pic_upload[1].findViewWithTag("cur_pic_upload_progress_bar"));
        mIdToProgressbarMap.put(3, (ProgressBar) user_pic_upload[2].findViewWithTag("cur_pic_upload_progress_bar"));
        mIdToProgressbarMap.put(4, (ProgressBar) user_pic_upload[3].findViewWithTag("cur_pic_upload_progress_bar"));
        mIdToProgressbarMap.put(5, (ProgressBar) user_pic_upload[4].findViewWithTag("cur_pic_upload_progress_bar"));


//      custom_prog_bar_id = (ProgressBar) findViewById(R.id.custom_prog_bar_id);

        Picasso.with(aContext).load(R.drawable.profilepic).into(profile_pic_logo);
        Picasso.with(aContext).load(R.drawable.addphoto).into(user_pic_plus_button_profile);
        Picasso.with(aContext).load(R.drawable.addphoto).into(user_pic_plus_button_1);
        Picasso.with(aContext).load(R.drawable.addphoto).into(user_pic_plus_button_2);
        Picasso.with(aContext).load(R.drawable.addphoto).into(user_pic_plus_button_3);
        Picasso.with(aContext).load(R.drawable.addphoto).into(user_pic_plus_button_4);
        Picasso.with(aContext).load(R.drawable.addphoto).into(user_pic_plus_button_5);

        OnActionBarClickedInterface onActionBarClickedListener = new OnActionBarClickedInterface() {

            @Override
            public void onUserProfileClicked() {

            }


            @Override
            public void onCuratedDealsClicked() {

            }

            @Override
            public void onConversationsClicked() {
            }

            @Override
            public void onLocationClicked() {
            }

            @Override
            public void onBackClicked() {
                onBackPressed();
            }

            @Override
            public void onTitleClicked() {
            }

            @Override
            public void onTitleLongClicked() {
            }

            @Override
            public void onSearchViewOpened() {

            }

            @Override
            public void onSearchViewClosed() {

            }

            @Override
            public void onCategoriesClicked() {

            }
        };
        ActionBarHandler actionBarHandler = new ActionBarHandler(this, getResources().getString(R.string.my_photos), null,
                onActionBarClickedListener, false, false, false);

        Intent i = getIntent();
        Bundle b = i.getExtras();
        if (b != null) {
            Boolean showUploadSlider = b.getBoolean("showUploadSlider");
            isFromPiNudge = b.getBoolean(Constants.PI_NUDGE_USER_PHOTOS_KEY);
        }
        actionBarHandler.toggleBackButton(true);

        connectFBviaTrustBuilderCallbackInterface = new ConnectFBviaTrustBuilderCallbackInterface() {

            @Override
            public void onSuccess(String access_token, String fbConnections, boolean isFbPhotoDeclined) {
                hideLoaders();
                Intent i = new Intent(UserPhotos.this, FacebookAlbumActivity.class);
                i.putExtra("ACCESS_TOKEN", access_token);
                i.putExtra("photo", photo);
                startActivityForResult(i, PICK_FROM_FACEBOOK);
            }

            @Override
            public void onFail(String errorMessage) {
                hideLoaders();
                AlertsHandler.showMessage(aActivity, errorMessage);
            }

            @Override
            public void onFail(int error_msg_res_id) {
                hideLoaders();
                AlertsHandler.showMessage(aActivity, error_msg_res_id);

            }

            @Override
            public void onMismatch(String mismatch, JSONObject diff, boolean isFBPhotoDeclined) {
                hideLoaders();
                AlertsHandler.showMessage(aActivity, mismatch);
            }
        };


        photoGuidelinesContainer.setOnClickListener(this);
        continue_button.setOnClickListener(this);

        profile_pic_upload.setOnClickListener(this);
        for (View upu : user_pic_upload) {
            upu.setOnClickListener(this);
        }

        findViewById(R.id.photo_text_link).setOnClickListener(this);

        tapToRetryHandler = new TapToRetryHandler(this, findViewById(android.R.id.content), new TapToRetryListener() {
            @Override
            public void reInitialize() {
                issueRequest(HttpRequestType.DISPLAY_PICS, "", false);
            }
        }, null);

        photoUploaderRequestInterface = new PhotoUploaderRequestInterface() {

            @Override
            public void onSuccess(String jsonResponseString) {
                if (!isDisplayPicsCall) {
                    //AlertsHandler.showMessage(aActivity, R.string.hoping_pics_meet_guidelines);
                    deleteCache();

                }

                isDisplayPicsCall = false;
                try {
                    processPhotoResponse(new JSONObject(jsonResponseString));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFail(String failMessage) {
                onComplete();
                AlertsHandler.showMessage(aActivity, failMessage);
                displayAllPhotos();
            }

            @Override
            public void onComplete() {
                hideLoaders();
                displayAllPhotos();


            }

            @Override
            public void onActivityResultSuccess(HttpRequestType type, String filePath) {
                issueRequest(type, filePath, false);

            }

            @Override
            public void onActivityResultSuccessFile(HttpRequestType httpRequestType, Uri selectedImage, String scheme) {

            }
        };
        photoUploader = new PhotoUploader(aActivity, photoUploaderRequestInterface, main_layout, crop_layout);
        photoUploader.setUserPhotos(true);
        cachedDataInterface = new CachedDataInterface() {
            @Override
            public void onSuccess(JSONObject response) {
                tapToRetryHandler.onSuccessFull();
                photoUploader.photoRequestUploadResponse(aActivity, true, response.toString(),
                        photoUploaderRequestInterface);
            }

            @Override
            public void onError(Exception e) {
                tapToRetryHandler.onNetWorkFailed(e);
                photoUploader.photoRequestUploadResponse(aActivity, false, null, photoUploaderRequestInterface);

            }
        };
        issueRequest(HttpRequestType.DISPLAY_PICS, "", false);

        callbackManager = CallbackManager.Factory.create();
        FacebookLoginCallbackInterface callbackInterface = new FacebookLoginCallbackInterface() {
            @Override
            public void onFail() {
                hideLoaders();
            }

            @Override
            public void onLogin(String accessToken, boolean isBirthdayDeclined, boolean isPhotoDeclined) {
                showLoaders();
                Utility.connectFBviaTrustBuilder(accessToken, connectFBviaTrustBuilderCallbackInterface,
                        "photoRegister", false, photo, aContext, isPhotoDeclined);

            }

            @Override
            public void onRetry() {
                mProgressDialog = UiUtils.showProgressBar(aContext, mProgressDialog);
            }
        };

        facebookLoginCallBack = new FacebookLoginCallback(this, callbackManager, callbackInterface, true, true, photo);

        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("lastClickedPosition")) {
                lastClickedImage = savedInstanceState.getInt("lastClickedPosition");
                uploadAtPos = lastClickedImage;
            }
            if (savedInstanceState.containsKey("isProfilePicClicked")) {
                isProfilePictureClicked = savedInstanceState.getBoolean("isProfilePicClicked");
            }
            if (savedInstanceState.containsKey("progressBarIndex")) {
                currentProgressBar = mIdToProgressbarMap.get(savedInstanceState.getInt("progressBarIndex"));
            }
            mStartVideoUploadTime = savedInstanceState.getLong("mStartVideoUploadTime", 0);
            videotimeInMillisec = savedInstanceState.getLong("videotimeInMillisec", 0);
            videoSizeBeforeCompression = savedInstanceState.getInt("videoSizeBeforeCompression", 0);
            videoSizeAfterCompression = savedInstanceState.getInt("videoSizeAfterCompression", 0);
            cameraUsed = savedInstanceState.getString("cameraUsed", null);
            videoSource = savedInstanceState.getString("videoSource", null);

            video_upload_enabled = savedInstanceState.getBoolean("video_upload_enabled", video_upload_enabled);
            video_compression_enabled = savedInstanceState.getBoolean("video_compression_enabled", video_compression_enabled);
            rotateVideoOnApp = savedInstanceState.getBoolean("rotateVideoOnApp", rotateVideoOnApp);
            keepOriginalAudioDuringAudio = savedInstanceState.getBoolean("keepOriginalAudioDuringAudio", keepOriginalAudioDuringAudio);
        }

        SPHandler.setBool(aActivity, ConstantsSP.SHARED_KEY_IS_PHOTOS_LAUNCHED_ONCE, true);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        LayoutParams lp = profile_pic_layout_detail.getLayoutParams(),
                lpSec = secondary_photo_layout_container.getLayoutParams();
        int width = profile_pic_layout_detail.getMeasuredWidth();
        lp.height = width;
        profile_pic_layout_detail.setLayoutParams(lp);
        width = user_pic_layout_detail[2].getMeasuredWidth();
        lpSec.height = width;
        secondary_photo_layout_container.setLayoutParams(lpSec);

        super.onWindowFocusChanged(hasFocus);

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt("lastClickedPosition", lastClickedImage);
        outState.putBoolean("isProfilePicClicked", isProfilePictureClicked);
        if (currentProgressBar != null) {
            outState.putInt("progressBarIndex", currentPosition + 1);
        }

        outState.putLong("mStartVideoUploadTime", mStartVideoUploadTime);
        outState.putLong("videotimeInMillisec", videotimeInMillisec);
        outState.putInt("videoSizeBeforeCompression", videoSizeBeforeCompression);
        outState.putInt("videoSizeAfterCompression", videoSizeAfterCompression);
        if (isSet(cameraUsed)) {
            outState.putString("cameraUsed", cameraUsed);
        }
        if (isSet(videoSource)) {
            outState.putString("videoSource", videoSource);
        }
        outState.putBoolean("video_upload_enabled", video_upload_enabled);
        outState.putBoolean("video_compression_enabled", video_compression_enabled);
        outState.putBoolean("rotateVideoOnApp", rotateVideoOnApp);
        outState.putBoolean("keepOriginalAudioDuringAudio", keepOriginalAudioDuringAudio);

        super.onSaveInstanceState(outState);
    }

    private void uninitialize() {
        profilePhoto = null;
        completePhotosList = null;
    }

    private void issueRequest(final HttpRequestType type, String filePath, boolean isSilent) {
        if (type == HttpRequestType.DISPLAY_PICS) {
            if (!isSilent) {
                tapToRetryHandler.showLoader();
            }

            isDisplayPicsCall = true;


            String url = ConstantsUrls.get_photos_url();
            boolean showFailure = CachedDataHandler.showDataFromDb(aContext, url, cachedDataInterface);

            CachedDataHandler cachedDataHandler = new CachedDataHandler(url, cachedDataInterface, this, photo, TrulyMadlyEventTypes.page_load, null, showFailure);
            Map<String, String> map = GetOkHttpRequestParams.getHttpRequestParams(type);
            map.put("hash", CachingDBHandler.getHashValue(aContext, url, Utility.getMyId(aContext)));
            cachedDataHandler.httpGet(this, url, map);


        } else if (type == HttpRequestType.UPLOAD_PIC_GALLERY || type == HttpRequestType.UPLOAD_PIC_CAMERA) {
            if (photoUploader.isInProgress()) {
                AlertsHandler.showMessage(aActivity, R.string.cant_upload_photo_yet);
                return;
            }
            int positionToUpload = -1;
            if (uploadAtPos != -1) {
                positionToUpload = uploadAtPos;

                if (uploadAtPos == 0) {
                    clearSingleImageView(profile_pic_id, -1);
                    cur_pic_upload = profile_pic_upload;
                } else {
                    clearSingleImageView(user_pic[uploadAtPos - 1], uploadAtPos - 1);
                    cur_pic_upload = user_pic_upload[uploadAtPos - 1];
                }


            } else {
                if (profilePhoto == null || photosLen == 0) {
                    positionToUpload = 0;
                    cur_pic_upload = profile_pic_upload;
                } else if (photosLen <= 5) {
                    positionToUpload = photosLen;
                    cur_pic_upload = user_pic_upload[photosLen - 1];
                }
                if (cur_pic_upload == null) {
                    positionToUpload = 0;
                    cur_pic_upload = profile_pic_upload;
                }
            }
            //photoUploader.startPhotoUpload(filePath, selectedImage, type, cur_pic_upload, photo);
            photoUploader.startPhotoUpload(filePath, type, cur_pic_upload, photo,
                    PhotoUploader.UPLOAD_TYPE.PICTURE, null, String.valueOf(positionToUpload));
        }
    }

    @Subscribe
    public void onImageProgressEventReceived(ImageUploadedProgress imageUploadedProgress) {
        if (imageUploadedProgress == null) {
            return;
        }

        if (isSet(imageUploadedProgress.getmMesageId())) {
            ProgressBar progressBar = mIdToProgressbarMap.get(
                    Integer.parseInt(imageUploadedProgress.getmMesageId()));
            if (progressBar != null) {
                progressBar.setProgress(imageUploadedProgress.getmProgress());
            }
        }
    }

    private void issueRequestForDeleteOrProFilePic(String id, HttpRequestType type) {

        mProgressDialog = UiUtils.showProgressBar(aContext, mProgressDialog);
        CustomOkHttpResponseHandler responseHandler = new CustomOkHttpResponseHandler(
                aContext) {

            @Override
            public void onRequestSuccess(JSONObject responseJSON) {
                deleteCache();

                mProgressDialog = UiUtils.hideProgressBar(mProgressDialog);
                processPhotoResponse(responseJSON);

            }

            @Override
            public void onRequestFailure(Exception exeption) {
                mProgressDialog = UiUtils.hideProgressBar(mProgressDialog);
                AlertsHandler.showMessage(aActivity, R.string.ERROR_NETWORK_FAILURE);
            }

        };

        OkHttpHandler.httpPost(aContext, ConstantsUrls.get_photos_url(),
                GetOkHttpRequestParams.getHttpRequestParams(type, id),
                responseHandler);

    }


    private void makeVideoDeleteCall(Photos photo) {

        mProgressDialog = UiUtils.showProgressBar(aContext, mProgressDialog);
        Map<String, String> eventInfo = new HashMap<>();
        eventInfo.put("video_length_ms", String.valueOf(photo.getDuration() * 1000));
        eventInfo.put("muted", String.valueOf(photo.isMuted()));
        eventInfo.put("video_id", String.valueOf(photo.getVideo_id()));
        eventInfo.put("photos_count", getUploadedPhotosCount());
        CustomOkHttpResponseHandler responseHandler = new CustomOkHttpResponseHandler(
                aContext, TrulyMadlyEvent.TrulyMadlyActivities.photo, TrulyMadlyEventTypes.delete_video, eventInfo) {

            @Override
            public void onRequestSuccess(JSONObject responseJSON) {
                deleteCache();
                mProgressDialog = UiUtils.hideProgressBar(mProgressDialog);
                processPhotoResponse(responseJSON);

            }

            @Override
            public void onRequestFailure(Exception exeption) {
                mProgressDialog = UiUtils.hideProgressBar(mProgressDialog);
                AlertsHandler.showMessage(aActivity, R.string.ERROR_NETWORK_FAILURE);
            }

        };

        HashMap<String, String> params = new HashMap<>();
        params.put("type", "delete");
        params.put("old_video_id", photo.getVideo_id());
        OkHttpHandler.httpPost(aContext, ConstantsUrls.get_post_video_url(),
                params,
                responseHandler);

    }

    private void deleteCache() {
        CachingDBHandler.deleteURL(aContext, ConstantsUrls.get_photos_url(), Utility.getMyId(aContext));
        CachingDBHandler.deleteURL(aContext, ConstantsUrls.get_my_profile_url(), Utility.getMyId(aContext));
    }

    private void showPopUp(final String photoID, final HttpRequestType type) {
        int msgID;
        if (type == HttpRequestType.PHOTO_DELETE)
            msgID = R.string.delete_pic;
        else
            msgID = R.string.profile_pic_set;

        AlertsHandler.showConfirmDialog(this, msgID, R.string.yes, R.string.no, new ConfirmDialogInterface() {

            @Override
            public void onPositiveButtonSelected() {
                issueRequestForDeleteOrProFilePic(photoID, type);
            }

            @Override
            public void onNegativeButtonSelected() {

            }
        });
    }


    @Override
    public void onClick(View arg0) {
        int id = arg0.getId();

        int clickedPos = -1;
        uploadAtPos = -1;
        PhotoSliderHandler.SliderMode sliderMode = null;
        if (isVideoUploading || (photoUploader != null && photoUploader.isInProgress())) {
            sliderMode = PhotoSliderHandler.SliderMode.none;
        }

        switch (id) {

            case R.id.photo_text_container:
                hidePhotoGuideLines();
                break;


            case R.id.photo_text_link:
                showPhotoGuideLines();
                break;

            case R.id.continue_user_photos:
                if (isVideoUploading) {
                    AlertsHandler.showConfirmDialog(aContext, R.string.pressing_continue_video_cancel, R.string.continue_upload, R.string.cancel,
                            new ConfirmDialogInterface() {
                                @Override
                                public void onPositiveButtonSelected() {

                                }

                                @Override
                                public void onNegativeButtonSelected() {
                                    if (transferUtility != null) {
                                        transferUtility.cancelAllWithType(TransferType.ANY);
                                    }
                                    continueUserPhotos();
                                }
                            });
                    return;
                } else {
                    continueUserPhotos();
                }
                break;

            case R.id.profile_pic_upload:
            case R.id.user_pic_upload_1:
            case R.id.user_pic_upload_2:
            case R.id.user_pic_upload_3:
            case R.id.user_pic_upload_4:
            case R.id.user_pic_upload_5:
                //Removing the cancelling with the new OkHttp Multipart upload
                //photoUploader.cancelPhotoUpload();
                break;

            default:
                break;
        }
    }

    private void continueUserPhotos() {
        HashMap<String, String> params = new HashMap<>();
        if (photosLen == 0) {
            params.put("continue_type", "skip");
        } else {
            params.put("continue_type", "continue");
        }
        finishUserPhotos(isFromPiNudge);
    }

    private int setLastClickedPosition(int clickedPosition) {
        isProfilePictureClicked = clickedPosition == 0;
        int lastClicked = -1;
        if (profilePhoto == null) {
            clickedPosition--;
        }
        if (photosLen > clickedPosition) {
            lastClicked = clickedPosition;
        }

        lastClickedImage = lastClicked;

        return lastClicked;
    }

    @OnClick({R.id.profile_pic_layout_detail, R.id.user_pic_1_layout_detail,
            R.id.user_pic_2_layout_detail, R.id.user_pic_3_layout_detail,
            R.id.user_pic_4_layout_detail, R.id.user_pic_5_layout_detail})
    public void onPicClicked(View view) {
        if (isVideoUploading || (photoUploader != null && photoUploader.isInProgress())) {
            AlertsHandler.showMessage(this, R.string.cant_upload_file_yet, false);
            return;
        }

        PhotoSliderHandler.SliderMode sliderMode = PhotoSliderHandler.SliderMode.none;
        Integer id = view.getId();
        boolean makeProfilePic = false;
        currentProgressBar = mIdToProgressbarMap.get(mUserPicLayoutToPositionMap.get(id));
        currentPosition = mUserPicLayoutToPositionMap.get(id) - 1;
        int newClickedPos = mUserPicLayoutToPositionMap.get(id);
        int pos = setLastClickedPosition(newClickedPos);

        switch (id) {
            case R.id.profile_pic_layout_detail:
                if (profilePhoto == null) {
                    sliderMode = PhotoSliderHandler.SliderMode.profile_add_clicked;
//                }else if(aPhotos.getCan_profile() && (!aPhotos.getStatus().equalsIgnoreCase("Rejected"))){
//                    sliderMode = PhotoSliderHandler.SliderMode.rejected_photo_clicked;
                } else {
                    sliderMode = PhotoSliderHandler.SliderMode.profile_clicked;
                }
                break;
            case R.id.user_pic_1_layout_detail:
            case R.id.user_pic_2_layout_detail:
            case R.id.user_pic_3_layout_detail:
            case R.id.user_pic_4_layout_detail:
            case R.id.user_pic_5_layout_detail:
                sliderMode = PhotoSliderHandler.SliderMode.other_add_clicked;
                if (pos >= 0 && completePhotosList != null && completePhotosList.size() > 0 && completePhotosList.size() > pos) {
                    Photos aPhotos = completePhotosList.get(pos);

                    if (aPhotos != null) {
                        if (aPhotos.isVideo()) {
                            if (Utility.stringCompare("pending", aPhotos.getEncodingStatus())) {
                                AlertsHandler.showMessage(aActivity, "Please wait while the video is being processed");
                                return;
                            } else {
                                Map<String, String> eventInfo = new HashMap<>();
                                eventInfo.put("video_length_ms", String.valueOf(aPhotos.getDuration() * 1000));
                                eventInfo.put("muted", String.valueOf(aPhotos.isMuted()));
                                eventInfo.put("video_id", String.valueOf(aPhotos.getVideo_id()));
                                eventInfo.put("photos_count", getUploadedPhotosCount());
                                TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyEvent.TrulyMadlyActivities.photo, TrulyMadlyEventTypes.video_clicked, 0,
                                        null, eventInfo, true);
                                sliderMode = PhotoSliderHandler.SliderMode.other_video_clicked;

                            }
                        } else if (aPhotos.getStatus().equalsIgnoreCase("Rejected")) {
                            sliderMode = PhotoSliderHandler.SliderMode.rejected_photo_clicked;
                        } else {
                            makeProfilePic = aPhotos.getCan_profile();
                            sliderMode = PhotoSliderHandler.SliderMode.other_picture_clicked;
                        }
                    }
                }

                break;
        }

        if (!video_upload_enabled && sliderMode == PhotoSliderHandler.SliderMode.other_add_clicked) {
            sliderMode = PhotoSliderHandler.SliderMode.profile_add_clicked;
        }

        mPhotoSliderHandler.toggleSlider(sliderMode, true, true, makeProfilePic);
    }

    private String getUploadedPhotosCount() {
        return completePhotosList != null ? String.valueOf(completePhotosList.size()) : "0";
    }

    @OnClick({R.id.remove_pic_layout, R.id.delete_video_layout, R.id.make_profile_pic_layout,
            R.id.edit_pic_layout, R.id.preview_video_layout, R.id.take_from_camera_layout,
            R.id.add_from_gallery_layout, R.id.shoot_video_layout, R.id.import_from_facebook_layout})
    public void onSliderOptionClicked(View view) {
        boolean animateSlider = true;
        int clickedPos = -1;
        switch (view.getId()) {
            case R.id.remove_pic_layout:
                clickedPos = lastClickedImage;

                if (clickedPos != -1) {
                    String photo_id = completePhotosList.get(clickedPos)
                            .getPhotoID();
                    showPopUp(photo_id, HttpRequestType.PHOTO_DELETE);
                }
                break;
            case R.id.make_profile_pic_layout:
                clickedPos = lastClickedImage;
                if (clickedPos != -1) {
                    if (completePhotosList.get(clickedPos).getCan_profile()) {
                        String photo_id = completePhotosList.get(clickedPos)
                                .getPhotoID();
                        showPopUp(photo_id, HttpRequestType.PHOTO_PROFILE_SET);
                    } else {
                        AlertsHandler.showAlertDialog(aActivity,
                                R.string.cannot_set_photo);
                    }
                }
                break;
            case R.id.take_from_camera_layout:
                if (!isVideoUploading) {
                    TrulyMadlyTrackEvent.trackEvent(aContext, photo, TrulyMadlyEventTypes.take_from_camera_clicked, 0,
                            null, null, true);
                    photoUploader.takeFromCamera(PhotoUploader.UPLOAD_TYPE.PICTURE);
                } else {
                    AlertsHandler.showMessage(aActivity, R.string.cant_upload_document_yet);
                }
                break;

            case R.id.add_from_gallery_layout:
                TrulyMadlyTrackEvent.trackEvent(aContext, photo, TrulyMadlyEventTypes.add_from_computer_clicked, 0,
                        null, null, true);
                photoUploader.fetchFromGallery(PhotoUploader.UPLOAD_TYPE.PICTURE, isProfilePictureClicked);
                break;
            case R.id.shoot_video_layout:
                //Doing this as the videocaptureactivity was taking time in starting because of the animation
                animateSlider = false;
                if (!isVideoUploading) {
                    Map<String, String> eventInfo = new HashMap<>();
                    eventInfo.put("photos_count", getUploadedPhotosCount());
                    TrulyMadlyTrackEvent.trackEvent(aContext, photo, TrulyMadlyEventTypes.video_create, 0,
                            null, eventInfo, true);
                    if (PermissionsHelper.checkAndAskForPermission(aActivity, new String[]{android.Manifest.permission.CAMERA,
                                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO},
                            PermissionsHelper.REQUEST_VIDEO_PERMISSIONS)) {
                        ActivityHandler.startVideoCaptureActivityForResult(aActivity, VIDEO_CAPTURING_CODE);
                    }
                } else {
                    AlertsHandler.showMessage(aActivity, R.string.cant_upload_document_yet);
                }

                break;
            case R.id.preview_video_layout:
                Map<String, String> eventInfo = new HashMap<>();
                clickedPos = lastClickedImage;
                Photos photo = null;
                if (clickedPos >= 0) {
                    photo = completePhotosList.get(lastClickedImage);
                }
                if (photo != null) {
                    eventInfo.put("video_length_ms", String.valueOf(photo.getDuration() * 1000));
                    eventInfo.put("muted", String.valueOf(photo.isMuted()));
                    eventInfo.put("video_id", String.valueOf(photo.getVideo_id()));
                    eventInfo.put("photos_count", getUploadedPhotosCount());
                    TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyEvent.TrulyMadlyActivities.photo, TrulyMadlyEventTypes.preview_video, 0,
                            null, eventInfo, true);
                    ActivityHandler.startPlayVideoActivity(aContext, photo);
                }
                break;

            case R.id.delete_video_layout:
                clickedPos = lastClickedImage;
                Photos photoToDelete = null;
                if (clickedPos >= 0) {
                    photoToDelete = completePhotosList.get(lastClickedImage);
                }
                if (photoToDelete != null) {
                    makeVideoDeleteCall(photoToDelete);
                }
                break;


            case R.id.edit_pic_layout:
                clickedPos = lastClickedImage;
                uploadAtPos = lastClickedImage;
                if (clickedPos != -1) {
                    if (completePhotosList.get(clickedPos).getName() != null) {
                        String url = completePhotosList.get(clickedPos)
                                .getName();
                        String photoId = completePhotosList.get(clickedPos)
                                .getPhotoID();
                        if (PermissionsHelper.checkAndAskForPermission(aActivity, android.Manifest.permission.READ_EXTERNAL_STORAGE,
                                USER_PHOTOS_EDIT_PERMISSION_REQUEST)) {
                            photoUploader.fetchImageFromUrl(url, PhotoUploader.UPLOAD_TYPE.PICTURE, photoId);
                        }
                    } else {
                        AlertsHandler.showAlertDialog(aActivity,
                                R.string.cannot_set_photo);
                    }
                }
                break;


            case R.id.import_from_facebook_layout:
                TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyEvent.TrulyMadlyActivities.photo, TrulyMadlyEventTypes.fb_click, 0, null, null, true);
                facebookLoginCallBack.login();
                break;
        }
        mPhotoSliderHandler.hideSlider(animateSlider);
    }

    private void showPhotoGuideLines() {
        photoGuidelinesContainer.setVisibility(VISIBLE);
        isPhotoGuideLinesVisible = true;
    }

    private void hidePhotoGuideLines() {
        photoGuidelinesContainer.setVisibility(GONE);
        isPhotoGuideLinesVisible = false;
    }

    private void finishUserPhotos(boolean isFromPiNudge) {
        Intent intent = new Intent();
        intent.putExtra("photosLen", photosLen);
        if (isFromPiNudge)
            intent.putExtra(Constants.PI_NUDGE_USER_PHOTOS_KEY, true);
        setResult(RESULT_OK, intent);
        finish();
    }


    private void deleteFiles(boolean source, boolean dest, boolean uploadedFile) {
        if (source && isSet(sourceFilePath)) {
            FilesHandler.deleteFileOrDirectory(sourceFilePath);
            sourceFilePath = null;
        }
        if (dest && isSet(outFileDestination)) {
            FilesHandler.deleteFileOrDirectory(outFileDestination);
            outFileDestination = null;
        }
        if (uploadedFile && isSet(uploadedFilePath)) {
            FilesHandler.deleteFileOrDirectory(uploadedFilePath);
            uploadedFilePath = null;
        }
    }

    private void makeVideoPostCall(final String filename, final boolean isCompressed) {
        Map<String, String> event_info = new HashMap<>();
        event_info.put("filename", filename);
        event_info.put("isCompressed", String.valueOf(isCompressed));
        CustomOkHttpResponseHandler customOkHttpResponseHandler = new CustomOkHttpResponseHandler(aContext, photo, TrulyMadlyEventTypes.post_video_server_call, event_info) {
            @Override
            public void onRequestSuccess(JSONObject responseJSON) {
                isVideoUploading = false;
                int responseCode = responseJSON.optInt("responseCode");
                if (responseCode == 200) {
                    isVideoUploading = false;
                    uploadView.setVisibility(GONE);
                    deleteCache();
                    onVideoUploadSuccess(isCompressed, responseJSON.optString("presetUsed"), responseJSON.optString("isTrimmingUsed"), responseJSON.optString("video_id"));
                    processPhotoResponse(responseJSON);
                } else if (responseCode == 403) {
                    onVideoUploadFailed(null, isCompressed, responseJSON.optString("error_code"), responseJSON.optString("error"));
                    currentUserPic.setVisibility(GONE);
                    uploadView.setVisibility(GONE);
                    currentAddMoreView.setVisibility(VISIBLE);
                }

                deleteFiles(true, true, true);
            }

            @Override
            public void onRequestFailure(Exception exception) {
                deleteFiles(true, true, true);
                isVideoUploading = false;
                onVideoUploadFailed(null, isCompressed, exception.toString(), getResources().getString(Utility.getNetworkErrorStringResid(aContext, exception)));
            }
        };
        HashMap<String, String> params = new HashMap<>();
        params.put("type", "new");
        params.put("filename", filename);
        params.put("ismuted", "" + isMuted);
        params.put("rotate", String.valueOf(orientationDegree));
        params.put("crop_square", "true");
        isVideoUploading = true;
        OkHttpHandler.httpPost(aContext, ConstantsUrls.get_post_video_url(), params, customOkHttpResponseHandler);
    }

    private void startVideoUpload(String path) {
        currentUserPic = user_pic[currentPosition];
        uploadView = user_pic_upload[currentPosition];
        currentAddMoreView = user_pic_add_more[currentPosition];
        currentAddMoreView = user_pic_add_more[currentPosition];
        uploadView.setVisibility(VISIBLE);

        File file = new File(path);
        //Reset it to null when uploading done/fails
        sourceFilePath = path;
        updateProgress(0);
        videoSizeBeforeCompression = FilesHandler.getFileSizeInKb(file);
        videoSizeAfterCompression = 0;
        mStartVideoUploadTime = Calendar.getInstance().getTimeInMillis();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2 && video_compression_enabled) {
            startCompression(path);
        } else {
            uploadToS3(path, false);
        }
    }

    private String startCompression(String path) {

//        outFileDestination = path;
//        outFileDestination = path.substring(0, path.length() - 4);
//        outFileDestination = outFileDestination + "_compressed.mp4";
        String fileName = path.substring(path.lastIndexOf("/") + 1, path.lastIndexOf("."));
        String extension = path.substring(path.lastIndexOf("."));
        outFileDestination = FilesHandler.getFilePath(FilesHandler.getVideoDirectoryPath(true),
                fileName + "_compressed" + extension);
//        outFileDestination = FilesHandler.getVideoDirectoryPath() +
//                path.substring(path.lastIndexOf("/"), path.length() - 4) + "_compressed" + path.substring(path.length() - 4);
        transcode(path, outFileDestination);
        return outFileDestination;
    }

    private boolean configureAudioVideoEncoder(String path) {

        MediaExtractorPlugin mex = new MediaExtractorPlugin();
        boolean isAudioConfigured = false, isVideoConfigured = false;
        try {
            mex.setDataSource(path);
            for (int position = 0; position < mex.getTrackCount(); position++) {
                MediaFormat mf = checkNotNull(mex.getTrackFormat(position));
                String mime = mf.getMimeType();
                if (!isAudioConfigured && isSet(mime) && mime.startsWith("audio")) {

                    AudioFormatAndroid aFormat = new AudioFormatAndroid(VideoUtils.AUDIO_MIME_TYPE_DEFAULT, VideoUtils.AUDIO_SAMPLE_RATE_DEFAULT, VideoUtils.AUDIO_CHANNEL_COUNT_DEFAULT);
                    aFormat.setAudioBitrateInBytes(VideoUtils.AUDIO_BIT_RATE_DEFAULT);
                    aFormat.setAudioProfile(MediaCodecInfo.CodecProfileLevel.AACObjectLC);

                    if (keepOriginalAudioDuringAudio) {
                        AudioFormatAndroid existingAudioFormat = (AudioFormatAndroid) mf;
                        int existingBitRate = 0;
                        try {
                            existingBitRate = existingAudioFormat.getInteger(android.media.MediaFormat.KEY_BIT_RATE);
                        } catch (RuntimeException ignored) {
                            existingBitRate = 0;
                        }
                        if (existingBitRate == 0) {
                            existingAudioFormat.setInteger(android.media.MediaFormat.KEY_BIT_RATE, VideoUtils.AUDIO_BIT_RATE_DEFAULT);
                        }
                        getMediaComposer().setTargetAudioFormat(existingAudioFormat);
                    } else {
                        getMediaComposer().setTargetAudioFormat(aFormat);
                    }
                    isAudioConfigured = true;
                }
                if (!isVideoConfigured && isSet(mime) && mime.startsWith("video")) {
                    //parse width from metadata else from bmp
                    VideoUtils.VideoDimension videoDimension = VideoUtils.getVideoDimensions(path);
                    if (videoDimension.mWidth < VIDEO_DIMENSION_MIN_SIDE || videoDimension.mHeight < VIDEO_DIMENSION_MIN_SIDE) {
                        //VIDEO lower than 240p, disabling compression/
                        //TODO: add tracking
                        TmLogger.e(TAG, "Video lower than min dimension," + VIDEO_DIMENSION_MIN_SIDE + " : " + videoDimension.mWidth + "x" + videoDimension.mHeight);
                        continue;

                    }
                    VideoFormatAndroid videoFormat = new VideoFormatAndroid(VideoUtils.VIDEO_MIME_TYPE_DEFAULT,
                            videoDimension.mWidth, videoDimension.mHeight);
                    videoFormat.setVideoBitRateInKBytes(VideoUtils.VIDEO_BITRATE_DEFAULT);
                    videoFormat.setVideoFrameRate(VideoUtils.VIDEO_FRAME_RATE_DEFAULT);
                    videoFormat.setVideoIFrameInterval(VideoUtils.VIDEO_IFRAME_INTERVAL_DEFAULT);
                    getMediaComposer().setTargetVideoFormat(videoFormat);
                    isVideoConfigured = true;
                }
            }
        } catch (IOException e) {
            Crashlytics.logException(e);
        }
        return !(!isAudioConfigured || !isVideoConfigured);


    }

    private void configureVideoEncoder(int width, int height) {
        VideoFormatAndroid videoFormat = new VideoFormatAndroid(VideoUtils.VIDEO_MIME_TYPE_DEFAULT, width, height);
        videoFormat.setVideoBitRateInKBytes(VideoUtils.VIDEO_BITRATE_DEFAULT);
        videoFormat.setVideoFrameRate(VideoUtils.VIDEO_FRAME_RATE_DEFAULT);
        videoFormat.setVideoIFrameInterval(VideoUtils.VIDEO_IFRAME_INTERVAL_DEFAULT);
        getMediaComposer().setTargetVideoFormat(videoFormat);
    }

    protected void configureAudioEncoder(MediaComposer mediaComposer, String path) {


        MediaExtractorPlugin mex = new MediaExtractorPlugin();
        //MediaExtractor mex = new MediaExtractor();


        AudioFormatAndroid aFormat = new AudioFormatAndroid(VideoUtils.AUDIO_MIME_TYPE_DEFAULT, VideoUtils.AUDIO_SAMPLE_RATE_DEFAULT, VideoUtils.AUDIO_CHANNEL_COUNT_DEFAULT);
        aFormat.setAudioBitrateInBytes(VideoUtils.AUDIO_BIT_RATE_DEFAULT);
        aFormat.setAudioProfile(MediaCodecInfo.CodecProfileLevel.AACObjectLC);
        try {
            mex.setDataSource(path);// the adresss location of the sound on sdcard.
            for (int position = 0; position < mex.getTrackCount(); position++) {
                //MediaFormat mf = mex.getTrackFormat(position);
                //String mime = mf.getString(MediaFormat.KEY_MIME);
                org.m4m.domain.MediaFormat mf = checkNotNull(mex.getTrackFormat(position));
                String mime = checkNotNull(mf.getMimeType());
                if (isSet(mime) && mime.contains("audio")) {
                    aFormat = (AudioFormatAndroid) mf;
                    /*
                    if (mf.containsKey(MediaFormat.KEY_SAMPLE_RATE)) {
                        aFormat.setAudioSampleRateInHz(mf.getInteger(MediaFormat.KEY_SAMPLE_RATE));
                    }
                    if (mf.containsKey(MediaFormat.KEY_CHANNEL_COUNT)) {
                        aFormat.setAudioChannelCount(mf.getInteger(MediaFormat.KEY_CHANNEL_COUNT));
                    }
                    if (mf.containsKey(MediaFormat.KEY_BIT_RATE)) {
                        aFormat.setAudioBitrateInBytes(mf.getInteger(MediaFormat.KEY_BIT_RATE));
                    }
                    */
                    break;
                }
            }
        } catch (IOException e) {
            Crashlytics.logException(e);
        }
        getMediaComposer().setTargetAudioFormat(aFormat);
    }

    protected void transcode(String path, String outFilePath) {
        try {
            mediaFile = getMediaComposer().addSourceFile(path);
            getMediaComposer().setTargetFile(outFilePath);
        } catch (IOException e) {
            mediaFile = null;
            Crashlytics.logException(e);
        }

        boolean isConfiguredSuccessfully = configureAudioVideoEncoder(path);

        if (!isConfiguredSuccessfully) {
            mediaFile = null;
            uploadToS3(path, false);
            return;
        }
        //configureVideoEncoder(mediaComposer, width, height);

        //https://software.intel.com/en-us/forums/intel-media-sdk/topic/561046
        if (rotateVideoOnApp) {
            getMediaComposer().addVideoEffect(new RotateEffect(orientationDegree, androidMediaObjectFactory.getEglUtil()));
        }

        //configureAudioEncoder(mediaComposer, path);
        isTranscodingInProgress = true;
        isVideoUploading = true;
        getMediaComposer().start();
    }


    private String makeThumbnailOnDisk(String filePath, String fileName) {

        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(filePath);
        Bitmap bmpCover = retriever.getFrameAtTime(0, MediaMetadataRetriever.OPTION_CLOSEST_SYNC);
        fileName = fileName.substring(0, fileName.length() - 4);
        String bmapLocation = Constants.disk_path + "/" + Constants.IMAGES_ASSETS_FOLDER + "/" + fileName + "_bmapLocation.jpg";

        File destinationFile = new File(bmapLocation);
        try {
            FileOutputStream outStream = new FileOutputStream(destinationFile);
            bmpCover.compress(Bitmap.CompressFormat.JPEG, 80, outStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            retriever.release();
        }
        return bmapLocation;

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case Constants.PICK_FROM_CAMERA_OWN:
                photoUploader.onActivityResult(requestCode, resultCode, data);
                break;
            case Constants.PICK_IMAGE_FROM_SYSTEM:
                photoUploader.onActivityResult(requestCode, resultCode, data);
                break;
            case Constants.PICK_FROM_GALLERY:
                //TODO : move this code to photo uploader
                if (resultCode == Activity.RESULT_OK && data != null) {
                    Uri selectedImage = data.getData();
                    if (data.getScheme() != null && selectedImage != null) {
                        String mime = MimeUtils.getMimeFromUri(selectedImage, aActivity);
                        if (isSet(mime)) {
                            if (mime.contains("image")) {
                                photoUploader.onActivityResult(requestCode, resultCode, data);
                            } else if (video_upload_enabled && mime.contains("video")) {
                                //                                String extension = MimeTypeMap.getFileExtensionFromUrl(selectedImage.toString());
                                String extension = FilesHandler.getFileExtension(aContext, selectedImage);
                                if (isProfilePictureClicked) {
                                    AlertsHandler.showMessage(aActivity, R.string.main_video_not_supported, false);
                                    return;
                                } else if (isSet(extension) && !VideoUtils.isVideoFormatSupported(
                                        extension)) {
                                    AlertsHandler.showMessage(aActivity,
                                            String.format(getString(R.string.file_ext_not_supported), extension), false);
                                    return;
                                }

                                videotimeInMillisec = VideoUtils.getVideoDurationMs(aContext, selectedImage);
                                //Prechecking video duration before making a file copy.
                                if (videotimeInMillisec != -1L) {
                                    int durationInSeconds = Long.valueOf(videotimeInMillisec / 1000).intValue();
                                    if (durationInSeconds < VideoUtils.VIDEO_DURATION_MIN_SECS_DEFAULT) {
                                        AlertsHandler.showMessage(aActivity,
                                                aActivity.getString(R.string.video_duration_message), false);
                                        return;
                                    } else if (durationInSeconds > 5 * 60) {
                                        AlertsHandler.showMessage(aActivity,
                                                aActivity.getString(R.string.video_5_min), false);
                                        return;
                                    }
                                }

                                GetFileCallback onFileComplete = new GetFileCallback() {
                                    @Override
                                    public void onGetFileComplete(File file, Uri selectedImage, HttpRequestType requestType) {
                                        hideLoaders();
                                        if (file == null) {
                                            photoUploaderRequestInterface.onFail(aActivity
                                                    .getString(R.string.video_parse_fail));
                                        } else {
                                            String extension = file.getName().substring(file.getName().lastIndexOf(".") + 1);
                                            if (isSet(extension) &&
                                                    !VideoUtils.isVideoFormatSupported(extension)) {
                                                AlertsHandler.showMessage(aActivity,
                                                        String.format(getString(R.string.file_ext_not_supported), extension), false);
                                                return;
                                            }

                                            videotimeInMillisec = VideoUtils.getVideoDurationMs(aContext, Uri.fromFile(file));
                                            if (videotimeInMillisec < VideoUtils.VIDEO_DURATION_MIN_SECS_DEFAULT * 1000) {
                                                FilesHandler.deleteFileOrDirectory(file.getPath());
                                                AlertsHandler.showMessage(aActivity,
                                                        aActivity.getString(R.string.video_duration_message), false);
                                            } else if (videotimeInMillisec > 5 * 60 * 1000) {
                                                FilesHandler.deleteFileOrDirectory(file.getPath());
                                                AlertsHandler.showMessage(aActivity,
                                                        aActivity.getString(R.string.video_5_min), false);
                                            } else {
                                                orientationDegree = VideoUtils.getRotation(file.getPath());
                                                videoSource = VideoUtils.VIDEO_SOURCE_GALLERY;
                                                cameraUsed = null;
                                                ActivityHandler.startTrimActivity(aActivity, file.getPath(), orientationDegree, VideoUtils.VIDEO_SOURCE_GALLERY, null);
                                            }
                                        }
                                    }
                                };
                                showLoaders();
                                FilesHandler.getFile(aActivity, selectedImage, data.getScheme(), onFileComplete,
                                        HttpRequestType.UPLOAD_PIC_GALLERY);
                            } else {
                                AlertsHandler.showMessage(aActivity, R.string.file_not_supported, false);
                            }
                        } else {
                            //TODO : Some message?
                        }
                    }
                }
                break;
            case Constants.TRIM_VIDEO_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    String trimStatus = data.getExtras().getString("trim_status");
                    if (Utility.stringCompare(trimStatus, "success")) {
                        String path = data.getExtras().getString("path");
                        int fileSizeInKb = FilesHandler.getFileSizeInKb(path);
                        if (fileSizeInKb <= 50) {
                            AlertsHandler.showMessage(aActivity, R.string.video_parse_fail);
                            break;
                        }
                        isMuted = data.getExtras().getBoolean("isMuted");
                        orientationDegree = data.getExtras().getInt("rotate", VideoUtils.VIDEO_ROTATION_DEFAULT);
                        startVideoUpload(path);
                    } else if (Utility.stringCompare(trimStatus, "error")) {
                        AlertsHandler.showMessage(aActivity, R.string.video_parse_fail);
                    }
                }
                break;
            case MY_PHOTOS_FULL:
            case PICK_FROM_FACEBOOK:
                if (data != null && data.getExtras() != null && data.getExtras().getString("response") != null) {
                    String photoResponse = data.getExtras().getString("response");
                    try {
                        processPhotoResponse(new JSONObject(photoResponse));
                    } catch (JSONException ignored) {
                    }
                }
                break;
            case VIDEO_CAPTURING_CODE:
                if (data != null && data.getExtras() != null) {
                    if (data.getExtras().containsKey("error")) {
                        AlertsHandler.showMessage(aActivity, data.getExtras().getString("error"), false);
                        return;
                    }
                    String filePath = data.getExtras().getString("filepath");
                    videotimeInMillisec = data.getExtras().getInt("video_time_ms");
                    videoSource = VideoUtils.VIDEO_SOURCE_CAMERA;
                    cameraUsed = data.getExtras().getString("camera");
                    int orientationDegree = data.getExtras().getInt("rotate", VideoUtils.VIDEO_ROTATION_DEFAULT);
                    if (isSet(filePath)) {
                        ActivityHandler.startTrimActivity(aActivity, filePath, orientationDegree, VideoUtils.VIDEO_SOURCE_CAMERA, cameraUsed);
                    }
                }
                break;
            default:
                break;
        }
    }

    private void uploadToS3(final String filePath, final boolean isCompressed) {
        String videoHash = SPHandler.getString(aContext, ConstantsSP.SHARED_KEY_VIDEO_KEY_VIDEOHASH);
        String awsPoolIdEncrypted = SPHandler.getString(aContext, ConstantsSP.SHARED_KEY_VIDEO_KEY_AWSVIDEOPOOLID);
        String awsVideoBucket = SPHandler.getString(aContext, ConstantsSP.SHARED_KEY_VIDEO_KEY_AWSVIDEOBUCKET);
        String awsVideoFolder = SPHandler.getString(aContext, ConstantsSP.SHARED_KEY_VIDEO_KEY_AWSVIDEOFOLDER);
        if (!isSet(filePath) || !isSet(awsPoolIdEncrypted)
                || !isSet(videoHash)
                || !isSet(awsVideoBucket)
                || !isSet(awsVideoFolder)) {
            onVideoUploadFailed(filePath, isCompressed,
                    "Value not set " + filePath + "," + awsPoolIdEncrypted + "," + videoHash + "," + awsVideoBucket + "," + awsVideoFolder,
                    null);
            return;
        }

        credentialsProvider = new CognitoCachingCredentialsProvider(
                getApplicationContext(),
                AESUtils.decrypt(awsPoolIdEncrypted, new StringBuilder(videoHash).reverse().toString()),
                VideoUtils.AWS_REGION_DEFAULT
        );

        //Timeout for the amazon s3 upload
        //Connection timeout : Timeout for the initial connection
        //Socket timeout : The amount of time to wait (in milliseconds) for
        //  data to be transfered over an established, open connection
        //  before the connection is times out and is closed.
        ClientConfiguration s3ClientConfig = new ClientConfiguration();
        s3ClientConfig.setConnectionTimeout(VideoUtils.AWS_CONNECTION_TIMEOUT_DEFAULT);
        s3ClientConfig.setSocketTimeout(VideoUtils.AWS_SOCKET_TIMEOUT_DEFAULT);

        AmazonS3 s3 = new AmazonS3Client(credentialsProvider, s3ClientConfig);
//        AmazonS3 s3 = new AmazonS3Client(credentialsProvider);
        File source = new File(filePath);
        String name = source.getName();
        name = videoHash + "_" + Utility.getMyId(aContext) + "_" + System.currentTimeMillis() + "_" + name;
        FilesHandler.renameFile(filePath, name);
        File file = new File(source.getParentFile(), name);
        uploadedFilePath = file.getPath();

        // uploading to s3

        if (transferUtility == null) {
            transferUtility = new TransferUtility(s3, aContext);
        }
        TransferObserver observer = transferUtility.upload(
                awsVideoBucket,
                awsVideoFolder + file.getName(),
                file    /* The file where the data to upload exists */
        );
        final String finalName = name;

        observer.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {

                Log.d(TAG, "statechange : " + state.toString());
                isVideoUploading = state == TransferState.IN_PROGRESS;

                switch (state) {
                    case COMPLETED:
                        makeVideoPostCall(finalName, isCompressed);
                        break;
                }
            }

            @Override
            public void onProgressChanged(int id, final long bytesCurrent, final long bytesTotal) {
                double current = (double) bytesCurrent;
                double total = (double) bytesTotal;
                int percentage = (int) (VideoUtils.VIDEO_UPLOAD_TRANSCODE_PERCENT_LIMIT +
                        (current / total) * (VideoUtils.VIDEO_UPLOAD_AWS_UPLOAD_PERCENT_LIMIT - VideoUtils.VIDEO_UPLOAD_TRANSCODE_PERCENT_LIMIT));
                updateProgress(percentage);
            }

            @Override
            public void onError(int id, Exception ex) {
                Crashlytics.logException(ex);
                String displayErrorMessage = null;
                if (ex.getCause() instanceof SSLException) {
                    displayErrorMessage = "Upload failed. Please check your phone's date and time.";
                }
                onVideoUploadFailed(filePath, isCompressed, ex.toString(), displayErrorMessage);
            }
        });
    }

    private void onVideoUploadSuccess(boolean isCompressed, String presetUsed, String trimmingUsed, String videoId) {
        long diff = Calendar.getInstance().getTimeInMillis() - mStartVideoUploadTime;

        TmLogger.d(TAG, "videoUploadSuccess");
        TmLogger.d(TAG, "Time Taken : " + diff);
        TmLogger.d(TAG, "isCompressed : " + isCompressed);
        TmLogger.d(TAG, "presetUsed : " + presetUsed);
        TmLogger.d(TAG, "trimmingUsed : " + trimmingUsed);
        TmLogger.d(TAG, "videoId : " + videoId);

//        Map<String, String> eventInfo = new HashMap<>();
        Map<String, String> eventInfo = getEventInfoVideoUpload();
        eventInfo.put("video_id", videoId);
        eventInfo.put("isCompressed", String.valueOf(isCompressed));
        eventInfo.put("presetUsed", presetUsed);
        eventInfo.put("trimmingUsed", trimmingUsed);

        TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyEvent.TrulyMadlyActivities.photo, TrulyMadlyEventTypes.video_upload, diff,
                "success", eventInfo, true);

    }

    private Map<String, String> getEventInfoVideoUpload() {
        Map<String, String> eventInfo = new HashMap<>();
        eventInfo.put("photos_count", getUploadedPhotosCount());
        eventInfo.put("video_position", String.valueOf(currentPosition));
        eventInfo.put("video_size_before_compression", String.valueOf(videoSizeBeforeCompression));
        eventInfo.put("video_size_after_compression", String.valueOf(videoSizeAfterCompression));
        eventInfo.put("muted", String.valueOf(isMuted));
        eventInfo.put("source", videoSource);
        eventInfo.put("device_name", Utility.getDeviceName());
        if (isSet(cameraUsed) && Utility.stringCompare(videoSource, VideoUtils.VIDEO_SOURCE_CAMERA)) {
            eventInfo.put("camera", cameraUsed);
        }
        eventInfo.put("video_length_ms", String.valueOf(videotimeInMillisec));
        return eventInfo;
    }

    private void onVideoUploadFailed(String filePath, boolean isCompressed, String trackingErrorMessage, String displayErrorMessage) {
        long diff = Calendar.getInstance().getTimeInMillis() - mStartVideoUploadTime;

        if (isSet(trackingErrorMessage)) {
            TmLogger.e(TAG, "errorMessage : " + trackingErrorMessage);
        }
        if (isSet(filePath)) {
            FilesHandler.deleteFileOrDirectory(filePath);
        }
        deleteFiles(true, true, true);
        uploadView.setVisibility(GONE);
        isVideoUploading = false;
        if (isSet(displayErrorMessage)) {
            AlertsHandler.showMessage(aActivity, displayErrorMessage);
        } else {
            AlertsHandler.showMessage(aActivity, R.string.video_upload_failed);
        }
        TmLogger.e(TAG, "Time Taken : " + diff);
        TmLogger.e(TAG, "isCompressed : " + isCompressed);

        Map<String, String> eventInfo = getEventInfoVideoUpload();
        eventInfo.put("isCompressed", String.valueOf(isCompressed));
        eventInfo.put("error_message", trackingErrorMessage);
        eventInfo.put("error_message_user_displayed", displayErrorMessage);
        TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyEvent.TrulyMadlyActivities.photo, TrulyMadlyEventTypes.video_upload, diff,
                "fail", eventInfo, true);
    }

    @Override
    public void onBackPressed() {
        if (mPhotoSliderHandler.isVisible()) {
            mPhotoSliderHandler.hideSlider(true);
        } else if (isPhotoGuideLinesVisible) {
            hidePhotoGuideLines();
        } else if (crop_layout.getVisibility() == VISIBLE) {
            crop_layout.setVisibility(GONE);
            main_layout.setVisibility(VISIBLE);
        } else if (isVideoUploading) {
            AlertsHandler.showConfirmDialog(aContext, R.string.pressing_back_video_cancel, R.string.continue_upload, R.string.cancel,
                    new ConfirmDialogInterface() {
                        @Override
                        public void onPositiveButtonSelected() {

                        }

                        @Override
                        public void onNegativeButtonSelected() {
                            if (transferUtility != null) {
                                transferUtility.cancelAllWithType(TransferType.ANY);
                            }
                            finishUserPhotos(false);
                        }
                    });
        } else {
            finishUserPhotos(false);
        }
    }

    void clearAllPhotos() {
        if (photosList != null)
            photosList.clear();
        photosList = null;

        if (completePhotosList != null)
            completePhotosList.clear();
        completePhotosList = null;
        profilePhoto = null;
        hideLoaders();
        clearPhotoViews();

    }

    private void clearSingleImageView(ImageView imageview, int pos) {


        imageview.setImageResource(android.R.color.transparent);

        // not  profile pic
        if (pos != -1) {
            TextView upum = user_pic_under_moderation[pos];
            UiUtils.removeAlpha(upum);
            upum.setVisibility(GONE);
            RelativeLayout upam = user_pic_add_more[pos];
            upam.setVisibility(VISIBLE);

        } else {
            UiUtils.removeAlpha(profile_pic_under_moderation_id);
            profile_pic_under_moderation_id.setVisibility(GONE);
            user_pic_add_more_profile.setVisibility(VISIBLE);
            profile_pic_logo.setVisibility(GONE);
        }
    }

    private void clearPhotoViews() {
        profile_pic_id.setImageResource(android.R.color.transparent);
        for (ImageView up : user_pic) {
            up.setImageResource(android.R.color.transparent);
        }
        UiUtils.removeAlpha(profile_pic_under_moderation_id);
        profile_pic_under_moderation_id.setVisibility(GONE);
        for (TextView upum : user_pic_under_moderation) {
            UiUtils.removeAlpha(upum);
            upum.setVisibility(GONE);
        }

        user_pic_add_more_profile.setVisibility(VISIBLE);
        for (RelativeLayout upam : user_pic_add_more) {
            upam.setVisibility(VISIBLE);
        }

        for (View vcm : video_cam_icon) {
            vcm.setVisibility(GONE);
        }

        profile_pic_logo.setVisibility(GONE);
    }

    private void processPhotoResponse(JSONObject responseJSON) {

        // flags
        String videoHash = responseJSON.optString("video_hash");
        if (isSet(videoHash)) {
            SPHandler.setString(aContext, ConstantsSP.SHARED_KEY_VIDEO_KEY_VIDEOHASH, videoHash);
        }


        JSONObject video_params = responseJSON.optJSONObject("video_params");
        if (video_params != null) {

            video_upload_enabled = video_params.optBoolean("video_upload_enabled", video_upload_enabled);
            video_compression_enabled = video_params.optBoolean("video_compression_enabled", video_compression_enabled);
            rotateVideoOnApp = video_params.optBoolean("rotate_video_on_app", rotateVideoOnApp);
            keepOriginalAudioDuringAudio = video_params.optBoolean("keep_original_audio_during_transcode", keepOriginalAudioDuringAudio);

            String awsVideoBucket = video_params.optString("bucket");
            if (isSet(awsVideoBucket)) {
                SPHandler.setString(aContext, ConstantsSP.SHARED_KEY_VIDEO_KEY_AWSVIDEOBUCKET, awsVideoBucket);
            }
            String awsVideoFolder = video_params.optString("prefix");
            if (isSet(awsVideoFolder)) {
                SPHandler.setString(aContext, ConstantsSP.SHARED_KEY_VIDEO_KEY_AWSVIDEOFOLDER, awsVideoFolder);
            }
            String awsPoolIdEncrypted = video_params.optString("poolid");
            if (isSet(awsPoolIdEncrypted)) {
                SPHandler.setString(aContext, ConstantsSP.SHARED_KEY_VIDEO_KEY_AWSVIDEOPOOLID, awsPoolIdEncrypted);
            }
        }

        String data = responseJSON.optJSONArray("data").toString();
        photosList = PhotoResponseParser.parsePhotoResponse(data, aContext);

        for (int i = 0; photosList != null && i < photosList.size(); i++) {
            photosList.get(i).setVideo(false);
        }

        JSONArray videos = responseJSON.optJSONArray("videos");

        if (videos != null && videos.length() > 0) {
            for (int i = 0; i < videos.length(); i++) {
                try {
                    JSONObject video = videos.getJSONObject(i);
                    Photos photo = new Photos();
                    photo.setVideo(true);
                    if (isSet(video.optString("thumbnail"))) {
                        photo.setThumbnailOnDisk(false);
                        photo.setThumbnail(video.optString("thumbnail"));
                    } else {
                        String name = video.optString("filename");
                        int len = 0;
                        if (isSet(name)) {
                            len = name.length();
                            name = name.substring(0, len - 4);
                            String filepath = Constants.disk_path + "/" + Constants.IMAGES_ASSETS_FOLDER + "/" + name + "_bmapLocation.jpg";
                            photo.setThumbnail(filepath);
                            photo.setThumbnailOnDisk(true);
                        }
                    }
                    photo.setName(photo.getThumbnail());
                    photo.setAdmin_approved(true);
                    photo.setIsProfile(false);
                    photo.setMuted(video.optBoolean("mute"));
                    photo.setDuration(video.optInt("duration"));
                    photo.setPhotoID(video.optString("video_id"));
                    photo.setVideo_id(video.optString("video_id"));
                    photo.setStatus(video.optString("status"));
                    photo.setEncodedUrl(video.optString("EncodedURL"));
                    photo.setEncodingStatus(video.optString("encoding_status"));
                    if (photosList == null) {
                        photosList = new ArrayList<>();
                    }
                    photosList.add(photo);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }


        //responseJSON.optJSONArray("videos")
        //parse videos
        //if(exists)
//        photosList.add()

        completePhotosList = new ArrayList<>();

        photosLen = 0;
        if (profilePhoto != null) {
            photosLen = 1;
            completePhotosList.add(profilePhoto);
        }
        if (photosList != null) {
            for (Photos aPhoto : photosList) {
                completePhotosList.add(aPhoto);
            }
            photosLen += photosList.size();
        }

        displayAllPhotos();
    }

    private void displayAllPhotos() {
        clearPhotoViews();

        if (profilePhoto != null) {


            ImageCacheHelper.with(aContext).loadWithKey(profilePhoto.getName(), Utility.getMyId(aContext)).into(profile_pic_id);

//            Picasso.with(aContext).load(profilePhoto.getName()).config(Config.RGB_565).into(profile_pic_id);


            user_pic_add_more_profile.setVisibility(GONE);
            profile_pic_logo.setVisibility(VISIBLE);
            if (profilePhoto.getStatus() != null) {
                if (!profilePhoto.getAdmin_approved()) {
                    profile_pic_under_moderation_id.setVisibility(VISIBLE);
                    profile_pic_under_moderation_id.setText(R.string.under_moderation);
                } else if (profilePhoto.getStatus().equalsIgnoreCase("Rejected")) {
                    profile_pic_under_moderation_id.setVisibility(VISIBLE);
                    profile_pic_under_moderation_id.setText(R.string.photo_rejected);
                }
            }
        }

        isVideoPending = false;
        if (photosList != null) {
            int size = photosList.size();

            for (int i = 0; i < 5; i++) {
                if (i < size) {

                    if (photosList.get(i).isVideo() && Utility.stringCompare("pending", photosList.get(i).getEncodingStatus())) {
                        Picasso.with(aContext).load(Utility.isMale(aContext) ? R.drawable.dummy_male : R.drawable.dummy_female).into(user_pic[i]);
                    } else if (!photosList.get(i).isThumbnailOnDisk())
                        ImageCacheHelper.with(aContext).loadWithKey(photosList.get(i).getName(), Utility.getMyId(aContext)).into(user_pic[i]);
                    else {
                        File file = new File(photosList.get(i).getName());
                        Picasso.with(aContext).load(file).into(user_pic[i]);
                    }
                    user_pic_add_more[i].setVisibility(GONE);
                    if (photosList.get(i).getStatus() != null) {
                        if (!photosList.get(i).getAdmin_approved()) {
                            user_pic_under_moderation[i].setVisibility(VISIBLE);
                            user_pic_under_moderation[i].setText(R.string.under_moderation);
                        } else if (photosList.get(i).getStatus().equalsIgnoreCase("Rejected")) {
                            user_pic_under_moderation[i].setVisibility(VISIBLE);
                            user_pic_under_moderation[i].setText(R.string.photo_rejected);
                        } else if (photosList.get(i).isVideo() && Utility.stringCompare("pending", photosList.get(i).getEncodingStatus())) {
                            user_pic_under_moderation[i].setVisibility(VISIBLE);
                            user_pic_under_moderation[i].setText(R.string.video_processing);
                            isVideoPending = true;
                        }
                    }
                    video_cam_icon[i].setVisibility(photosList.get(i).isVideo() ? VISIBLE : GONE);
                } else {
                    video_cam_icon[i].setVisibility(GONE);
                }

            }
        }

        if (photosLen > 0) {
            continue_button.setText(R.string.continue_string);
        } else {
            continue_button.setText(R.string.skip);
        }

        hideLoaders();

        if (isVideoPending && !isVideoPendingRequestScheduled) {
            isVideoPendingRequestScheduled = true;
            if (mHandler == null) {
                mHandler = new Handler();
            }
            if (videoPendingRequestRunnable != null) {
                mHandler.removeCallbacks(videoPendingRequestRunnable);
                videoPendingRequestRunnable = null;
            }

            videoPendingRequestRunnable = new Runnable() {
                @Override
                public void run() {
                    videoPendingRequestRunnable = null;
                    isVideoPendingRequestScheduled = false;
                    issueRequest(HttpRequestType.DISPLAY_PICS, "", true);

                }
            };
            mHandler.postDelayed(videoPendingRequestRunnable, 2000);
        }
    }

    @Override
    protected void onDestroy() {
        if (mHandler != null && videoPendingRequestRunnable != null) {
            mHandler.removeCallbacks(videoPendingRequestRunnable);
        }
        super.onDestroy();
    }

    private void hideLoaders() {
        // custom_prog_bar_id.setVisibility(View.GONE);
        mProgressDialog = UiUtils.hideProgressBar(mProgressDialog);
    }

    private void showLoaders() {
        // custom_prog_bar_id.setVisibility(View.VISIBLE);
        mProgressDialog = UiUtils.showProgressBar(aContext, mProgressDialog);
    }

    @Override
    public void onStart() {
        super.onStart();
        BusProvider.getInstance().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        BusProvider.getInstance().unregister(this);
    }

    @SuppressLint({"Override", "NewApi"})
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (grantResults == null || grantResults.length == 0) {
            return;
        }
        switch (requestCode) {
            case PermissionsHelper.REQUEST_VIDEO_PERMISSIONS:

                if (grantResults != null) {
                    boolean allGranted = true;
                    for (int result : grantResults) {
                        if (result != PackageManager.PERMISSION_GRANTED) {
                            allGranted = false;
                            break;
                        }
                    }
                    if (allGranted)
                        ActivityHandler.startVideoCaptureActivityForResult(aActivity, VIDEO_CAPTURING_CODE);
                    else
                        PermissionsHelper.handlePermissionDenied(aActivity, R.string.default_permission_message);


                }
                break;

            case PermissionsHelper.REQUEST_CAMERA_STORAGE_PERMISSIONS_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && (grantResults.length == 1 || grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
                    //TODO: FILE HANDLING: Modify this line to have a better implementation
                    FilesHandler.createImageCacheFolder();
                    photoUploader.onPermissionGranted(requestCode);
                } else {
                    photoUploader.onPermissionRejected();
                }
                break;

            case PermissionsHelper.REQUEST_STORAGE_PERMISSIONS_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //TODO: FILE HANDLING: Modify this line to have a better implementation
                    FilesHandler.createImageCacheFolder();
                    photoUploader.onPermissionGranted(requestCode);
                } else {
                    photoUploader.onPermissionRejected();
                }
                break;
            case USER_PHOTOS_EDIT_PERMISSION_REQUEST:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //TODO: FILE HANDLING: Modify this line to have a better implementation
                    FilesHandler.createImageCacheFolder();
                    String url = completePhotosList.get(uploadAtPos)
                            .getName();
                    String photoId = completePhotosList.get(uploadAtPos)
                            .getPhotoID();
                    photoUploader.fetchImageFromUrl(url, PhotoUploader.UPLOAD_TYPE.PICTURE, photoId);
                } else {
                    photoUploader.onPermissionRejected();
                }
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    public IProgressListener getTranscodingProgressListener() {
        if (transcodingProgressListener == null) {
            transcodingProgressListener = new IProgressListener() {
                @Override
                public void onMediaStart() {
                    isTranscodingInProgress = true;
                    isVideoUploading = true;
                }

                @Override
                public void onMediaProgress(final float progress) {
                    isTranscodingInProgress = true;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            int percentage = (int) (VideoUtils.VIDEO_UPLOAD_TRANSCODE_PERCENT_LIMIT * progress);
                            updateProgress(percentage);
                        }
                    });
                }

                @Override
                public void onMediaDone() {
                    Log.d(TAG, "Transcoding done");
                    //Resetting orientationDegree on successfull transcoding
                    if (rotateVideoOnApp) {
                        orientationDegree = 0;
                    }
                    isTranscodingInProgress = false;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (mediaFile != null) {
                                getMediaComposer().removeSourceFile(mediaFile);
                            }

                            deleteFiles(true, false, false);
                            if (isSet(outFileDestination)) {
                                videoSizeAfterCompression = FilesHandler.getFileSizeInKb(outFileDestination);
                                uploadToS3(outFileDestination, true);
                            }
                            mediaComposer = null;
                        }
                    });

                }

                @Override
                public void onMediaPause() {
                    TmLogger.d(TAG, "Transcoding pause");
                }

                @Override
                public void onMediaStop() {
                    TmLogger.d(TAG, "Transcoding stop");
                }

                @Override
                public void onError(Exception exception) {

                    TmLogger.e(TAG, "Transcoding error : " + exception.toString());
                    Crashlytics.logException(exception);
                    if (!isTranscodingInProgress) {
                        return;
                    }
                    isTranscodingInProgress = false;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (mediaFile != null) {
                                getMediaComposer().removeSourceFile(mediaFile);
                            }
                            deleteFiles(false, true, false);
                            if (isSet(sourceFilePath)) {
                                uploadToS3(sourceFilePath, false);
                            }
                            mediaComposer = null;
                        }
                    });
                }
            };
        }
        return transcodingProgressListener;
    }

    public MediaComposer getMediaComposer() {
        if (mediaComposer == null) {
            androidMediaObjectFactory = new AndroidMediaObjectFactory(getApplicationContext());
            mediaComposer = new MediaComposer(androidMediaObjectFactory, getTranscodingProgressListener());
        }
        return mediaComposer;
    }

}
