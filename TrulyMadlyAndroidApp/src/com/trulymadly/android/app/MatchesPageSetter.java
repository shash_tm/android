package com.trulymadly.android.app;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewStub;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.picasso.Picasso;
import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.adapter.FavCustomRecyclerAdapter;
import com.trulymadly.android.app.adapter.MatchesPagesViewPagerAdapter2;
import com.trulymadly.android.app.ads.AdsHelper;
import com.trulymadly.android.app.ads.AdsType;
import com.trulymadly.android.app.bus.EventInfoUpdateEvent;
import com.trulymadly.android.app.bus.NetworkChangeEvent;
import com.trulymadly.android.app.custom.CircularRevealView;
import com.trulymadly.android.app.json.Constants.EditProfilePageType;
import com.trulymadly.android.app.json.ConstantsRF;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.listener.ActivityLifecycleListener;
import com.trulymadly.android.app.listener.NetworkChangedListener;
import com.trulymadly.android.app.modal.EducationModal;
import com.trulymadly.android.app.modal.FavoriteDataModal;
import com.trulymadly.android.app.modal.FbFriendModal;
import com.trulymadly.android.app.modal.MatchesSysMesgModal;
import com.trulymadly.android.app.modal.MyDetailModal;
import com.trulymadly.android.app.modal.ProfileNewModal;
import com.trulymadly.android.app.modal.ProfileViewPagerModal;
import com.trulymadly.android.app.modal.TrustBuilderModal;
import com.trulymadly.android.app.modal.UserModal;
import com.trulymadly.android.app.modal.VideoModal;
import com.trulymadly.android.app.modal.WorkModal;
import com.trulymadly.android.app.sqlite.RFHandler;
import com.trulymadly.android.app.utility.ActivityHandler;
import com.trulymadly.android.app.utility.AdsTrackingHandler;
import com.trulymadly.android.app.utility.CirclePageIndicator2;
import com.trulymadly.android.app.utility.FBMutualFriendsHandler;
import com.trulymadly.android.app.utility.FavoriteUtils;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.TMSelectHandler;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;

import org.apmem.tools.layouts.FlowLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static butterknife.ButterKnife.findById;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.trulymadly.android.app.utility.FavoriteUtils.FAVORITE_IMAGE_PREFETCH_LIMIT;
import static com.trulymadly.android.app.utility.FavoriteUtils.FAVORITE_KEY_BOOKS;
import static com.trulymadly.android.app.utility.FavoriteUtils.FAVORITE_KEY_FOOD;
import static com.trulymadly.android.app.utility.FavoriteUtils.FAVORITE_KEY_MOVIES;
import static com.trulymadly.android.app.utility.FavoriteUtils.FAVORITE_KEY_MUSIC;
import static com.trulymadly.android.app.utility.FavoriteUtils.FAVORITE_KEY_OTHERS;
import static com.trulymadly.android.app.utility.FavoriteUtils.TAB_CATEGORY_KEYS;

/* This layout is used for both MatchesPageLatest2 and ProfileNew
 * Layout contains - >
 * 5 cardviews cardView 1 - 4  ,  cardView-Inter( lies b/w cardView 1 & 2 for ProfileNew Activity ) and Edit Buttons
 * Each cardView & Edit Buttons is initialized in initializecardView() and setCardView()
 */

public class MatchesPageSetter implements ActivityLifecycleListener,
        NetworkChangedListener {

    private static final ButterKnife.Action<View> VISIBLE = new ButterKnife.Action<View>() {
        @Override
        public void apply(View view, int index) {
            view.setVisibility(View.VISIBLE);
        }
    };
    private static final ButterKnife.Action<View> GONE = new ButterKnife.Action<View>() {
        @Override
        public void apply(View view, int index) {
            view.setVisibility(View.GONE);
        }
    };
    private final int mRevealDuration = 218;
    private final OnClickListener rootLayClickListener;
    private final long mTSTutorialUnrevealDuration = 218;
    private final View rootLay;
    private final boolean isMyProfile;
    private final LayoutInflater inflater;
    @BindViews({R.id.edit_hashtag_title, R.id.edit_word_edu_title, R.id.edit_favorites_title,
            R.id.pics_change_button_container, R.id.hashtags_change_button, R.id.work_info_change_button,
            R.id.edu_info_change_button, R.id.fav_change_button,
            R.id.profile_detail_change_button
    })
    List<View> changeViews;
    @BindView(R.id.pics_change_button_blooper)
    View pics_change_button_blooper;
    @BindView(R.id.matches_page_match_name_age)
    TextView matches_page_match_name_age;
    @BindView(R.id.matches_page_match_loc_height)
    TextView matches_page_match_loc_height;
    /* Edit / Change Buttons ; Used in ProfileNew Page */
    @BindView(R.id.matches_page_match_celeb_status)
    View matches_page_match_celeb_status;
    @BindView(R.id.matches_page_match_badge_image)
    ImageView matches_page_match_badge_image;
    @BindView(R.id.matches_page_match_last_login_time)
    TextView matches_page_match_last_login_time;
    @BindView(R.id.last_login_icon)
    View last_login_icon;
    @BindView(R.id.last_login_container)
    View last_login_container;
    @BindView(R.id.fb_friend_container)
    View fb_friend_container;
    @BindView(R.id.mutual_friend_count)
    TextView mutual_friend_count;
    @BindView(R.id.mutual_events_description_tv)
    TextView mutual_events_description_tv;
    @BindView(R.id.heading_container)
    LinearLayout heading_container;
    @BindView(R.id.alert_work_edu)
    View alert_work_edu;
    @BindView(R.id.alert_favorites)
    View alert_favorites;
    @BindView(R.id.sponsored_tv)
    View sponsored_tv;
    @BindView(R.id.sponsored_event_detail_stub)
    ViewStub sponsored_event_detail_stub;
    @BindView(R.id.profile_activity_dashboard_stub)
    ViewStub profile_activity_dashboard_stub;
    @BindView(R.id.profile_activity_dashboard_card_bottom_border)
    View profile_activity_dashboard_card_bottom_border;
    /* CardView - 2 */
    @BindView(R.id.matches_page_setter_work_edu_card)
    View cardView2;
    @BindView(R.id.matches_page_setter_work_edu_card_bottom_border)
    View matches_page_setter_work_edu_card_bottom_border;
    @BindView(R.id.profile_work_designation_salary_tv)
    TextView profile_work_designation_salary_tv;
    @BindView(R.id.profile_work_companies_lay)
    TextView profile_work_companies_lay;
    @BindView(R.id.profile_edu_highest_tv)
    TextView profile_edu_highest_tv;
    @BindView(R.id.profile_edu_places_lay)
    TextView profile_edu_places_lay;
    /* CardView - 3 */
    @BindView(R.id.matches_page_setter_favorites_card)
    View mFavoritesCard;
    @BindView(R.id.favorites_recyclerView)
    RecyclerView mFavoritesRecyclerView;
    @BindView(R.id.fav_movies_selected_lay)
    View fav_movies_selected_lay;
    @BindView(R.id.fav_movies_no_data_lay)
    View fav_movies_no_data_lay;
    @BindView(R.id.fav_movies_selected_icon)
    ImageView fav_movies_selected_icon;
    @BindView(R.id.fav_movies_selected_line)
    View fav_movies_selected_line;
    @BindView(R.id.fav_music_selected_lay)
    View fav_music_selected_lay;
    @BindView(R.id.fav_music_no_data_lay)
    View fav_music_no_data_lay;
    @BindView(R.id.fav_music_selected_icon)
    ImageView fav_music_selected_icon;
    @BindView(R.id.fav_music_selected_line)
    View fav_music_selected_line;
    @BindView(R.id.fav_books_selected_lay)
    View fav_books_selected_lay;
    @BindView(R.id.fav_books_no_data_lay)
    View fav_books_no_data_lay;
    @BindView(R.id.fav_books_selected_icon)
    ImageView fav_books_selected_icon;
    @BindView(R.id.fav_books_selected_line)
    View fav_books_selected_line;
    @BindView(R.id.fav_food_selected_lay)
    View fav_food_selected_lay;
    @BindView(R.id.fav_food_no_data_lay)
    View fav_food_no_data_lay;
    @BindView(R.id.fav_food_selected_icon)
    ImageView fav_food_selected_icon;
    @BindView(R.id.fav_food_selected_line)
    View fav_food_selected_line;
    @BindView(R.id.fav_misc_selected_lay)
    View fav_misc_selected_lay;
    @BindView(R.id.fav_misc_no_data_lay)
    View fav_misc_no_data_lay;
    @BindView(R.id.fav_misc_selected_icon)
    ImageView fav_misc_selected_icon;
    @BindView(R.id.fav_misc_selected_line)
    View fav_misc_selected_line;
    //New Trustscore
    @BindView(R.id.others_trustscore_container)
    View mOthersTrustScoreContainer;
    @BindView(R.id.matches_page_setter_my_trustscore_card_stub)
    ViewStub mMyTrustScoreCardStub;
    @BindView(R.id.my_trustscore_card_bottom_border)
    View my_trustscore_card_bottom_border;
    //    @BindView(R.id.indicator)
//    CirclePageIndicator indicator;
    @BindView(R.id.indicator2)
    CirclePageIndicator2 indicator2;
    @BindView(R.id.matches_page_setter_card_view1)
    View matches_page_setter_card_view1;
    @BindView(R.id.matches_page_scrim_lay)
    View matches_page_scrim_lay;
    @BindView(R.id.matches_page_setter_hashtags_container)
    View matches_page_setter_hashtags_container;
    @BindView(R.id.profile_work_lay_id)
    View profile_work_lay_id;
    @BindView(R.id.profile_edu_lay_id)
    View profile_edu_lay_id;
    @BindView(R.id.category_item_scrollview)
    NestedScrollView category_item_scrollview;
    @BindView(R.id.category_image_container)
    View category_image_container;
    //    @BindView(R.id.one_one)
//    ImageView one_one;
//    @BindView(R.id.two_one)
//    ImageView two_one;
//    @BindView(R.id.two_two)
//    ImageView two_two;
//    @BindView(R.id.three_one)
//    ImageView three_one;
//    @BindView(R.id.three_two)
//    ImageView three_two;
//    @BindView(R.id.three_three)
//    ImageView three_three;
//    @BindView(R.id.one_image_container)
//    View one_image_container;
//    @BindView(R.id.two_image_container)
//    View two_image_container;
//    @BindView(R.id.three_image_container)
//    View three_image_container;
    @BindView(R.id.height_layout_new)
    View height_layout;
    @BindView(R.id.height_layout_2_new)
    View height_layout_2;
    @BindView(R.id.category_nudge_layout)
    View parent_category_nudge;
    @BindView(R.id.photo_visibility_layout)
    View photo_visibility_layout;
    @BindView(R.id.matches_page_setter_container)
    LinearLayout matches_page_setter_container;
    //Profile Ads - from Ad Network
    @BindView(R.id.ad_container)
    ViewGroup ad_container;
    @BindView(R.id.ad_container_view)
    ViewGroup ad_container_view;
    @BindView(R.id.mutual_friend_layout)
    View mutual_friend_layout;
    //    @BindView(R.id.profile_ad_name)
//    TextView profile_ad_name;
    @BindView(R.id.matches_page_view_pager_lay)
    ViewGroup matches_page_view_pager_lay;
    @BindView(R.id.first_iv)
    ImageView first_iv;
    @BindView(R.id.second_iv)
    ImageView second_iv;
    @BindView(R.id.third_iv)
    ImageView third_iv;
    @BindView(R.id.fourth_iv)
    ImageView fourth_iv;
    @BindView(R.id.category_nudge_container)
    LinearLayout category_nudge_container;
    @BindView(R.id.category_count_tv)
    TextView category_count_tv;
    /*SELECT Related Views - START*/
    @BindView(R.id.select_details_stub)
    ViewStub mSelectDetailsStub;
    @BindView(R.id.select_details_bottom_border)
    View mSelectDetailsBottomBorder;
    @BindView(R.id.select_tag)
    View mSelectTag;
    private View alert_trust_score;
    //New TS Changes related to My Profile
    private View mMyTrustScoreCard;
    private View mMyTrustScoreContainer;
    /* CardView - Inter */
    private TextView activity_dashboard_tooltip_box_text;
    private View count_views_container;
    private TextView count_views;
    private TextView count_views_text;
    private View popularity_bar_container;
    private RelativeLayout blue_bar;
    private TextView popularity_suggestion;
    private View activity_dashboard_tooltip_box;
    private View profile_activity_dashboard;
    private View sponsored_event_detail;
    private TextView sponsored_event_details_description_tv;
    private int CURRENT_FAVORITE_POS = 0;
    /* Variables constant throughout object creation */
    private Activity aActivity;
    private OnClickListener mActivityOnClickListener;
    private ProgressBar mOthersTrustscoreProgressbar;
    private ImageView mTSFacebookIV;
    private ImageView mTSLinkedinIV;
    private ImageView mTSPhoneIV;
    private ImageView mTSPhotoIV;
    private ImageView mTSreferencesIV;
    private TextView mTSFacebookTV;
    private TextView mTSLinkedinTV;
    private TextView mTSreferencesTV;
    private boolean isOthersTrustScoreRevealed = false,
            isOthersTrustScoreContainerInitialised = false, isBeingRevealed = false;
    private CircularRevealView mCircularRevealView;
    private OnClickListener editProfileClickListener;
    private ObjectAnimator mProgressAnimator;
    private Handler mhandler;
    private Runnable mDelayedProgressRunnable;
    private Animator.AnimatorListener mRevealAnimationListener, mUnrevealAnimationListener;
    private ArrayList<View> mBounceViews;
    private int mProgressAnimationDuration = 418;
    private ArrayList<TranslateAnimation> mBounceAndShowUpTranslateAnimations;
    private ArrayList<TranslateAnimation> mBounceAndHideUpTranslateAnimations;
    private boolean isNewTSTutorialStarted;
    private Runnable mTSTutorialHideRunnable;
    private Runnable mTSTutorialShowRunnable;
    //New TS Tutorial
    private View mTSNewTutorialContainer;
    private ViewStub mTSNewTutorialContainerStub;
    private ImageView mTSNewTutorialIV;
    private Runnable mHideNewTSCoachRunnable;
    private ProfileNewModal aProfileNewModal;
    private MatchesSysMesgModal aMatchesSysMesgModal;
    /* CardView - 1 */
    private ViewPager matches_page_view_pager;
    private MatchesPagesViewPagerAdapter2 mAdapter;
    private OnClickListener matchesPageViewPagerClickListener;
    //	private boolean isMutualFriendsShowing = false;
    private FlowLayout matches_page_hashtags_lay;
    private String popularityToolTip = "";
    private FavCustomRecyclerAdapter rAdapter;
    private int cur_selected_lay_id;
    private boolean isPopularityTootipVisible = false;
    private boolean isMutualMatch = false;
    private boolean isSingleMatch = false;
    private OnClickListener mSponsoredProfileClickListener;

    //Prefetch
    private boolean isFetched = false;
    private Boolean isLastActiveShown = null;
    private RecyclerView.OnItemTouchListener onFavRecyclerViewTouchListener;
    private boolean isTSTutorialAllowed = false;
    private boolean isAdClicked = false;
    private int widthPx = 0;
    private FBMutualFriendsHandler mFBMutualFriendsHandler;
    private OnClickListener mFBOnClickListener;
    //Fav list views
    private View[] mDefaultFavViews, mDisabledFavViews, mFavSelectedLineViews;
    private ImageView[] mDefaultFavIVs;
    private int[] mFavSelectedIcons, mfavUnselectedIcons, mFavMutualIcons;
    private HashMap<String, ArrayList<FavoriteDataModal>> favDataMap;
    private boolean isFavoritesViewed = false;
    private View mSelectDetailsCard;
    private ImageView mSelectCompatibilityIV;
    private TextView mSelectCompatibilityTV;
    private View mSelectExpandArrow;
    private View mSelectPofileCtaContainer;
    private TextView mSelectPofileCtaTV;
    private TextView mSelectPofileCtaSubTextTV;
    /*SELECT Related Views - END*/


    /*
     * initializes all cardviews and cardview inter , edit buttons for
     * ProfileNew Page
     */


    public MatchesPageSetter(Activity aActivity, View rootLay, boolean isMyProfile,
                             boolean isMutualMatch, boolean isSingleMatch,
                             int bottomPaddingForFooterDp, OnClickListener mFBOnClickListener) {
        this.aActivity = aActivity;
        if (aActivity instanceof OnClickListener) {
            mActivityOnClickListener = (OnClickListener) aActivity;
        }
        this.rootLay = rootLay;
        ButterKnife.bind(this, rootLay);
        this.isMyProfile = isMyProfile;
        this.isMutualMatch = isMutualMatch;
        this.isSingleMatch = isSingleMatch;
        this.mFBOnClickListener = mFBOnClickListener;
        isTSTutorialAllowed = isMyProfile || isMutualMatch;
        inflater = (LayoutInflater) aActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        rootLayClickListener = new OnClickListener() {

            @Override
            public void onClick(View v) {
                hidePopularityTooltip();
            }
        };


        if (bottomPaddingForFooterDp > 0) {
            matches_page_setter_container.setPadding(0, 0, 0, UiUtils.dpToPx(bottomPaddingForFooterDp));
        }

        initializeCardView1();
        if (isMyProfile) {
            ButterKnife.apply(changeViews, VISIBLE);
            showLastActive(null);
            if (SPHandler.getBool(aActivity, ConstantsSP.SHARED_KEY_IS_PHOTOS_LAUNCHED_ONCE, false)) {
                pics_change_button_blooper.setVisibility(View.GONE);
            } else {
                pics_change_button_blooper.setVisibility(View.VISIBLE);
            }
        } else {
            ButterKnife.apply(changeViews, GONE);
        }

        setEditProfileClickListeners(isMyProfile);

        OnTouchListener disableScrollListener = new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        };
    }
////
//    public void createDummyData() {
//        String[] eventNames = new String[]{"eventname1", "eventname2", "eventname3", "eventname4", "eventname5"};
//        String[] eventImages = new String[]{"https://b.zmtcdn.com/images/logo/zomato.jpg",
//                "https://b.zmtcdn.com/images/logo/zomato.jpg",
//                "https://b.zmtcdn.com/images/logo/zomato.jpg",
//                "https://b.zmtcdn.com/images/logo/zomato.jpg",
//                "https://b.zmtcdn.com/images/logo/zomato.jpg"};
//
//
//        aProfileNewModal.setEventNames(eventNames);
//        aProfileNewModal.setEventsImages(eventImages);
//
//    }

    /*
     * instantiates the layout with new data aProfileNewModal is needed for data
     * of profile lis(OnClickListener) is used for going to AlbumFullViewPager
     * activity after click on profile pic
     */
    public void instantiateItem(ProfileNewModal aProfileNewModal, OnClickListener matchesPageViewPagerClickListener,
                                MatchesSysMesgModal aMatchesSysMesgModal, MyDetailModal aMyDetailModal,
                                AdsHelper adsHelper) {
        isFetched = false;
        isFavoritesViewed = false;
        this.aProfileNewModal = aProfileNewModal;
        this.aMatchesSysMesgModal = aMatchesSysMesgModal;
        this.matchesPageViewPagerClickListener = matchesPageViewPagerClickListener;
        setCardView1(adsHelper);
        setCardView2();
        setFavoritesCard();
        if (isMyProfile) {
            setInterCardView();
        }
        setSelectDetailsCard();

        if (aProfileNewModal.isSponsoredProfile()) {
            HashMap<String, String> eventInfo = new HashMap<>();
            eventInfo.put("source", isMutualMatch ? TrulyMadlyEvent.TrulyMadlyActivities.message_full_conversation :
                    TrulyMadlyEvent.TrulyMadlyActivities.matches);
            TrulyMadlyTrackEvent.trackEvent(aActivity, TrulyMadlyEvent.TrulyMadlyActivities.campaigns,
                    TrulyMadlyEvent.TrulyMadlyEventTypes.impression, 0, aProfileNewModal.getUserId(),
                    eventInfo, true);
            AdsTrackingHandler.callUrls(aProfileNewModal.getmImpressionsTrackingUrls());
        }
        //Hack to scroll down on new matches
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!isMyProfile) {
                    rootLay.scrollTo(0, 0);
                }
            }
        }, 50);
    }

    private boolean isLastActiveShown() {
        if (isLastActiveShown == null) {
            isLastActiveShown = RFHandler.getBool(aActivity,
                    Utility.getMyId(aActivity), ConstantsRF.RIGID_FIELD_LAST_ACTIVE);
        }

        return isLastActiveShown;
    }

    public void setIsLastActiveShown(Boolean isLastActiveShown) {
        this.isLastActiveShown = isLastActiveShown;
    }

    private void setOthersTrustScore() {
        if (!isOthersTrustScoreContainerInitialised) {
            mOthersTrustScoreContainer.setVisibility(View.VISIBLE);
            mTSFacebookIV = findById(mOthersTrustScoreContainer, R.id.fb);
            mTSFacebookTV = findById(mOthersTrustScoreContainer, R.id.fb_count);
            mTSLinkedinIV = findById(mOthersTrustScoreContainer, R.id.linkedin);
            mTSLinkedinTV = findById(mOthersTrustScoreContainer, R.id.linedin_text);
            mTSPhoneIV = findById(mOthersTrustScoreContainer, R.id.phone);
            mTSPhotoIV = findById(mOthersTrustScoreContainer, R.id.photo);
            mTSreferencesIV = findById(mOthersTrustScoreContainer, R.id.endorsement);
            mTSreferencesTV = findById(mOthersTrustScoreContainer, R.id.endorsement_text);
            ImageView mTSProgressCenterIV = findById(mOthersTrustScoreContainer, R.id.ts_progress_center_iv);
            Picasso.with(aActivity).load(R.drawable.verification).into(mTSProgressCenterIV);
            mOthersTrustscoreProgressbar = findById(mOthersTrustScoreContainer, R.id.trust_score_progressbar);
            mOthersTrustscoreProgressbar.setBackground(aActivity.getResources().getDrawable(R.drawable.progressbar_background));
            mOthersTrustscoreProgressbar.setProgressDrawable(aActivity.getResources().getDrawable(R.drawable.circular_progressbar_white));
            View mTSWhiteCenter = findById(mOthersTrustScoreContainer, R.id.ts_black_center);
            mTSWhiteCenter.setVisibility(View.GONE);
            mCircularRevealView = findById(mOthersTrustScoreContainer, R.id.ts_revealview);
            mhandler = new Handler();
            mDelayedProgressRunnable = new Runnable() {
                @Override
                public void run() {
                    mProgressAnimator.cancel();
                    mProgressAnimator.start();
                }
            };
            OnClickListener mOthersTrustscoreProgressbarClickListener = new OnClickListener() {
                @Override
                public void onClick(View v) {
                    toggleNewTS();
                }
            };
            mOthersTrustscoreProgressbar.setOnClickListener(mOthersTrustscoreProgressbarClickListener);
            isOthersTrustScoreContainerInitialised = true;

            mBounceViews = new ArrayList<>();
            mBounceViews.add(findById(mOthersTrustScoreContainer, R.id.fb_container));
            mBounceViews.add(findById(mOthersTrustScoreContainer, R.id.phone_container));
            mBounceViews.add(findById(mOthersTrustScoreContainer, R.id.linkedin_container));
            mBounceViews.add(findById(mOthersTrustScoreContainer, R.id.photo_container));
            mBounceViews.add(findById(mOthersTrustScoreContainer, R.id.endorsement_container));

            LinearInterpolator mLinearInterpolator = new LinearInterpolator();
            mBounceAndShowUpTranslateAnimations = new ArrayList<>();
            mBounceAndHideUpTranslateAnimations = new ArrayList<>();

            for (int i = 0; i < mBounceViews.size(); i++) {
                final int finalI = i;
                TranslateAnimation bounceShowUpTranslateAnimation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0,
                        Animation.RELATIVE_TO_SELF, 0,
                        Animation.RELATIVE_TO_SELF, 2,
                        Animation.RELATIVE_TO_SELF, -0.25f);
                int mBounceDuration = 218;
                bounceShowUpTranslateAnimation.setDuration(mBounceDuration);
                bounceShowUpTranslateAnimation.setInterpolator(mLinearInterpolator);
                bounceShowUpTranslateAnimation.setStartOffset(i * 100);
                bounceShowUpTranslateAnimation.setFillAfter(true);

                final TranslateAnimation bounceShowDownTranslateAnimation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0,
                        Animation.RELATIVE_TO_SELF, 0,
                        Animation.RELATIVE_TO_SELF, -0.25f,
                        Animation.RELATIVE_TO_SELF, 0);
                bounceShowDownTranslateAnimation.setDuration(mBounceDuration);
                bounceShowDownTranslateAnimation.setInterpolator(mLinearInterpolator);
                bounceShowDownTranslateAnimation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        if (finalI == mBounceViews.size() - 1) {
                            isBeingRevealed = false;
                            if (isNewTSTutorialStarted) {
                                isNewTSTutorialStarted = false;
                                mhandler.postDelayed(mTSTutorialHideRunnable, mTSTutorialUnrevealDuration);
                            }
                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

                bounceShowUpTranslateAnimation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        mBounceViews.get(finalI).setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        mBounceViews.get(finalI).startAnimation(bounceShowDownTranslateAnimation);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

                mBounceAndShowUpTranslateAnimations.add(bounceShowUpTranslateAnimation);

                TranslateAnimation bounceHideUpTranslateAnimation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0,
                        Animation.RELATIVE_TO_SELF, 0,
                        Animation.RELATIVE_TO_SELF, 0,
                        Animation.RELATIVE_TO_SELF, -0.25f);
                bounceHideUpTranslateAnimation.setDuration(mBounceDuration);
                bounceHideUpTranslateAnimation.setInterpolator(mLinearInterpolator);
                bounceHideUpTranslateAnimation.setStartOffset(i * 100);
                bounceHideUpTranslateAnimation.setFillAfter(true);

                final TranslateAnimation bounceHideDownTranslateAnimation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0,
                        Animation.RELATIVE_TO_SELF, 0,
                        Animation.RELATIVE_TO_SELF, -0.25f,
                        Animation.RELATIVE_TO_SELF, 2);
                bounceHideDownTranslateAnimation.setDuration(mBounceDuration);
                bounceHideDownTranslateAnimation.setInterpolator(mLinearInterpolator);

                bounceHideDownTranslateAnimation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        if (finalI == mBounceViews.size() - 3) {
                            unrevealView();
                        }
                        mBounceViews.get(finalI).setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

                bounceHideUpTranslateAnimation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        mBounceViews.get(finalI).setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        mBounceViews.get(finalI).startAnimation(bounceHideDownTranslateAnimation);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

                mBounceAndHideUpTranslateAnimations.add(bounceHideUpTranslateAnimation);
            }

            mRevealAnimationListener = new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    bounceAndShowImages();
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            };

            mUnrevealAnimationListener = new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    isBeingRevealed = false;
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            };
        }
        initiliseTSUi();
        mhandler.postDelayed(mDelayedProgressRunnable, mProgressAnimationDuration);

        isTSTutorialAllowed = isTSTutorialAllowed || isMyProfile || isMutualMatch;
        startTSTutorial(isTSTutorialAllowed, null);
    }

    public void startTSTutorial(final Animation.AnimationListener animationListener) {
        isTSTutorialAllowed = true;
        startTSTutorial(isTSTutorialAllowed, animationListener);
    }

    private void startTSTutorial(boolean isTSTutorialAllowed, final Animation.AnimationListener animationListener) {
        boolean tutorialShown = SPHandler.getBool(aActivity,
                ConstantsSP.SHARED_PREF_NEW_TS_TUTORIAL);

        isNewTSTutorialStarted = false;
        if (mTSTutorialHideRunnable != null) {
            mhandler.removeCallbacks(mTSTutorialShowRunnable);
            mhandler.removeCallbacks(mTSTutorialHideRunnable);
            mhandler.removeCallbacks(mHideNewTSCoachRunnable);
            resetTSViews();
        }

        if (isTSTutorialAllowed && !tutorialShown) {
            SPHandler.setBool(aActivity,
                    ConstantsSP.SHARED_PREF_NEW_TS_TUTORIAL, true);
            mTSTutorialHideRunnable = new Runnable() {
                @Override
                public void run() {
                    toggleNewTS();
                }
            };
            mTSTutorialShowRunnable = new Runnable() {
                @Override
                public void run() {
                    startNewTSTutorial();
                }
            };
            isNewTSTutorialStarted = true;

            mTSNewTutorialContainerStub = findById(rootLay, R.id.tutorial_new_ts_coachmark_container_stub);
            mTSNewTutorialContainer = mTSNewTutorialContainerStub.inflate();
            mTSNewTutorialIV = (ImageView) mTSNewTutorialContainer.findViewById(R.id.tutorial_new_ts_coachmark_iv);
            boolean isMyGenderMale = Utility.isMale(aActivity);
            int tutorialDrawable = isMyGenderMale ? R.drawable.tutorial_new_trustscore_her :
                    R.drawable.tutorial_new_trustscore_his;
            Picasso.with(aActivity).load(tutorialDrawable).into(mTSNewTutorialIV);
            mHideNewTSCoachRunnable = new Runnable() {
                @Override
                public void run() {
                    Animation animation = AnimationUtils.loadAnimation(aActivity, R.anim.slide_up_tutorial_first_like_hide);
                    animation.setAnimationListener(animationListener);
                    mTSNewTutorialContainer.startAnimation(animation);
                    mTSNewTutorialContainer.setVisibility(View.GONE);
                }
            };

            mhandler.postDelayed(mTSTutorialShowRunnable, mTSTutorialUnrevealDuration);
        }
    }

    private void startNewTSTutorial() {
        mTSNewTutorialContainer.setVisibility(View.VISIBLE);
        mTSNewTutorialContainer.startAnimation(
                AnimationUtils.loadAnimation(aActivity, R.anim.slide_down_tutorial_first_like_hide));
        long mHideNewTSCoachDuration = 2000;
        mhandler.postDelayed(mHideNewTSCoachRunnable, mHideNewTSCoachDuration);

        toggleNewTS();
    }

    private void toggleNewTS() {
        if (isBeingRevealed) {
            return;
        }
        isBeingRevealed = true;
        if (isOthersTrustScoreRevealed) {
            bounceAndHideImages();
            mCircularRevealView.setClickable(false);
        } else {
            revealView();
            mCircularRevealView.setClickable(true);
        }

        TrulyMadlyTrackEvent.trackEvent(aActivity,
                isMutualMatch ? TrulyMadlyEvent.TrulyMadlyActivities.profile :
                        TrulyMadlyEvent.TrulyMadlyActivities.matches,
                TrulyMadlyEvent.TrulyMadlyEventTypes.trustscore, 0,
                isOthersTrustScoreRevealed ? TrulyMadlyEvent.TrulyMadlyEventStatus.close :
                        TrulyMadlyEvent.TrulyMadlyEventStatus.open, null, true);

        isOthersTrustScoreRevealed = !isOthersTrustScoreRevealed;

        Utility.fireBusEvent(aActivity, true, new EventInfoUpdateEvent(
                EventInfoUpdateEvent.KEY_TRUST_SCORE_VIEWED, "true"));
    }

    private void resetTSViews() {
        isOthersTrustScoreRevealed = false;
        final int color = Color.TRANSPARENT;
        int[] positions = new int[2];
        mOthersTrustscoreProgressbar.getLocationOnScreen(positions);
        final Point p = getLocationInView(mOthersTrustScoreContainer, mOthersTrustscoreProgressbar);
        mCircularRevealView.hide(p.x, p.y, color, 0, 0, null);
        for (View view : mBounceViews) {
            checkNotNull(view).setVisibility(View.INVISIBLE);
        }

        if (mTSNewTutorialContainer != null) {
            mTSNewTutorialContainer.setVisibility(View.GONE);
        }
    }

    private void initiliseTSUi() {
        mOthersTrustScoreContainer.setVisibility(View.VISIBLE);
        TrustBuilderModal trustBuilderModal = aProfileNewModal != null ? aProfileNewModal.getTrustBuilder() : null;
        resetTSViews();
        int mTrustScore = (trustBuilderModal == null) ? 0 : trustBuilderModal.getTrustScore();
        mOthersTrustscoreProgressbar.setProgress(1);
        mOthersTrustscoreProgressbar.setProgress(0);
        if (mTrustScore < 50) {
            mProgressAnimationDuration = 318;
        }
        mProgressAnimator = ObjectAnimator.ofInt(mOthersTrustscoreProgressbar, "progress", 0, mTrustScore);
        mProgressAnimator.setDuration(mProgressAnimationDuration);
        mProgressAnimator.setInterpolator(new LinearInterpolator());

        Utility.fireBusEvent(aActivity, true, new EventInfoUpdateEvent(
                EventInfoUpdateEvent.KEY_TRUST_SCORE, String.valueOf(mTrustScore)));

        //Facebook
        if (trustBuilderModal != null && Utility.isSet(trustBuilderModal.getFBConn())) {
            try {
                Integer.parseInt(trustBuilderModal.getFBConn());
                mTSFacebookTV.setVisibility(View.VISIBLE);
                mTSFacebookTV.setText(trustBuilderModal.getFBConn() + " ");
            } catch (NumberFormatException e) {
                mTSFacebookTV.setVisibility(View.INVISIBLE);
            }
            Picasso.with(aActivity).load(R.drawable.ts_fb_enabled_light).into(mTSFacebookIV);
        } else {
            mTSFacebookTV.setVisibility(View.INVISIBLE);
            Picasso.with(aActivity).load(R.drawable.ts_fb_disabled_light).into(mTSFacebookIV);
        }

        //Phone
        if (trustBuilderModal != null && Utility.isSet(trustBuilderModal.getPhnNum())) {
            Picasso.with(aActivity).load(R.drawable.ts_phone_enabled_light).into(mTSPhoneIV);
        } else {
            Picasso.with(aActivity).load(R.drawable.ts_phone_disabled_light).into(mTSPhoneIV);
        }

        //LinkedIn
        int linkedInConnInt = 0;
        if (trustBuilderModal != null && (linkedInConnInt = trustBuilderModal.getLinkedInConn()) > 0) {
            mTSLinkedinTV.setVisibility(View.VISIBLE);
            mTSLinkedinTV.setText(String.valueOf(linkedInConnInt));
            Picasso.with(aActivity).load(R.drawable.ts_ln_enabled_light).into(mTSLinkedinIV);
        } else {
            mTSLinkedinTV.setVisibility(View.INVISIBLE);
            Picasso.with(aActivity).load(R.drawable.ts_ln_disabled_light).into(mTSLinkedinIV);
        }

        //Photo
        if (trustBuilderModal != null && (Utility.isSet(trustBuilderModal.getIDProof()) ||
                Utility.isSet(trustBuilderModal.getAddressProof()))) {
            Picasso.with(aActivity).load(R.drawable.ts_photoid_enabled_light).into(mTSPhotoIV);
        } else {
            Picasso.with(aActivity).load(R.drawable.ts_photoid_disabled_light).into(mTSPhotoIV);
        }

        //References
        if (trustBuilderModal != null && trustBuilderModal.getEndorsers() != null && trustBuilderModal.getEndorsers().size() > 0) {
            mTSreferencesTV.setVisibility(View.INVISIBLE);
            Picasso.with(aActivity).load(R.drawable.ts_ref_enabled_light).into(mTSreferencesIV);
        } else {
            mTSreferencesTV.setVisibility(View.INVISIBLE);
            Picasso.with(aActivity).load(R.drawable.ts_ref_disabled_light).into(mTSreferencesIV);
        }
    }

    private void revealView() {
        final int color = ActivityCompat.getColor(aActivity, R.color.black_alpha_90);
        int[] positions = new int[2];
        mOthersTrustscoreProgressbar.getLocationOnScreen(positions);
        final Point p = getLocationInView(mOthersTrustScoreContainer, mOthersTrustscoreProgressbar);
        mCircularRevealView.reveal(p.x, p.y, color, 0, mRevealDuration, mRevealAnimationListener);
    }

    private void unrevealView() {
        final int color = Color.TRANSPARENT;
        int[] positions = new int[2];
        mOthersTrustscoreProgressbar.getLocationOnScreen(positions);
        final Point p = getLocationInView(mOthersTrustScoreContainer, mOthersTrustscoreProgressbar);
        mCircularRevealView.hide(p.x, p.y, color, 0, mRevealDuration, mUnrevealAnimationListener);
    }

    private void bounceAndShowImages() {
        for (int i = 0; i < mBounceViews.size(); i++) {
            mBounceViews.get(i).startAnimation(mBounceAndShowUpTranslateAnimations.get(i));
        }
    }

    private void bounceAndHideImages() {
        for (int i = 0; i < mBounceViews.size(); i++) {
            mBounceViews.get(i).startAnimation(mBounceAndHideUpTranslateAnimations.get(i));
        }
    }

    private Point getLocationInView(View src, View target) {
        final int[] l0 = new int[2];
        src.getLocationOnScreen(l0);

        final int[] l1 = new int[2];
        target.getLocationOnScreen(l1);

        l1[0] = l1[0] - l0[0] + target.getWidth() / 2;
        l1[1] = l1[1] - l0[1] + target.getHeight() / 2;

        return new Point(l1[0], l1[1]);
    }

    private void setSelectDetailsCard() {
        if (TMSelectHandler.isSelectEnabled(aActivity) && aProfileNewModal.isTMSelectMember()) {
            if (mSelectDetailsCard == null) {
                mSelectDetailsCard = mSelectDetailsStub.inflate();
                mSelectCompatibilityIV = findById(mSelectDetailsCard, R.id.select_compatibility_image);
                mSelectCompatibilityTV = findById(mSelectDetailsCard, R.id.select_compatibility_text);
                mSelectExpandArrow = findById(mSelectDetailsCard, R.id.select_expand_arrow);
                mSelectPofileCtaContainer = findById(mSelectDetailsCard, R.id.select_pofile_cta_container);
                mSelectPofileCtaTV = findById(mSelectDetailsCard, R.id.select_profile_cta);
                mSelectPofileCtaSubTextTV = findById(mSelectDetailsCard, R.id.select_profile_cta_subtext);
            }
            if (TMSelectHandler.isSelectMember(aActivity) && TMSelectHandler.isSelectQuizPlayed(aActivity)) {
                String selectCompatibilityImage = aProfileNewModal.getmSelectCommonImage();
                if (!Utility.isSet(selectCompatibilityImage)) {
                    selectCompatibilityImage = TMSelectHandler.getSelectCompatibilityImageDef(aActivity);
                }
                if (Utility.isSet(selectCompatibilityImage)) {
                    Ion.with(aActivity).load(selectCompatibilityImage)
                            .intoImageView(mSelectCompatibilityIV).setCallback(new FutureCallback<ImageView>() {
                        @Override
                        public void onCompleted(Exception e, ImageView result) {
                            if (e != null) {
                                Picasso.with(aActivity).load(R.drawable.compatibility_image_default).into(mSelectCompatibilityIV);
                            }
                        }
                    });
                    mSelectCompatibilityTV.setText(Html.fromHtml(aProfileNewModal.getmSelectCommonString()));
                } else {
                    Crashlytics.logException(new NullPointerException("Select compatible image is null : "
                            + " User Member : " + TMSelectHandler.isSelectMember(aActivity)
                            + " Match Member : " + aProfileNewModal.isTMSelectMember()));
                    Picasso.with(aActivity).load(R.drawable.compatibility_image_default).into(mSelectCompatibilityIV);
                    String selectCompTextDef = TMSelectHandler.getSelectCompatibilityTextDef(aActivity);
                    if (Utility.isSet(selectCompTextDef)) {
                        mSelectCompatibilityTV.setText(selectCompTextDef);
                    } else {
                        mSelectCompatibilityTV.setText(R.string.compatibility_text_fallback);
                    }
                }
                mSelectExpandArrow.setVisibility(View.VISIBLE);
                mSelectPofileCtaContainer.setVisibility(View.GONE);
            } else {
                Picasso.with(aActivity).load(R.drawable.locked_info).into(mSelectCompatibilityIV);
                mSelectCompatibilityTV.setText(R.string.join_select_to_see_compatibility);
                mSelectExpandArrow.setVisibility(View.GONE);
                mSelectPofileCtaTV.setText(TMSelectHandler.getSelectProfileCta(aActivity));
//                String ctaText = TMSelectHandler.getSelectProfileCtaSubText(aActivity);
//                if(Utility.isSet(ctaText)) {
//                    mSelectPofileCtaSubTextTV.setText(ctaText);
//                    mSelectPofileCtaSubTextTV.setVisibility(View.VISIBLE);
//                }else{
                mSelectPofileCtaSubTextTV.setVisibility(View.GONE);
//                }
                mSelectPofileCtaContainer.setVisibility(View.VISIBLE);
            }
            mSelectDetailsCard.setOnClickListener(mActivityOnClickListener);
            mSelectDetailsCard.setVisibility(View.VISIBLE);
            mSelectDetailsBottomBorder.setVisibility(View.VISIBLE);
        } else {
            if (mSelectDetailsCard != null) {
                mSelectDetailsCard.setVisibility(View.GONE);
                mSelectDetailsBottomBorder.setVisibility(View.GONE);
            }
        }
    }

    private void setEditProfileClickListeners(boolean isMyProfile) {

        if (isMyProfile) {
            editProfileClickListener = new OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch (v.getId()) {
                        //edit photo
                        case R.id.matches_page_setter_card_view1:
//                        case R.id.indicator:
                        case R.id.indicator2:
                            onClickpics_change_button();
                            break;

                        //edit basics
                        case R.id.matches_page_scrim_lay:
                            onClickprofile_detail_change_button();
                            break;

                        //edit trust
                        case R.id.matches_page_setter_my_trustscore_card:
                            onClicktrustscore_change_button();
                            break;

                        //edit hashags
                        case R.id.matches_page_setter_hashtags_container:
                            onClickhashtags_change_button();
                            break;

                        //edit work
                        case R.id.profile_work_lay_id:
                            onClickwork_info_change_button();
                            break;

                        //edit education
                        case R.id.profile_edu_lay_id:
                            onClickedu_info_change_button();
                            break;

                        //edit favorites
                        case R.id.matches_page_setter_favorites_card:
                        case R.id.favorite_item:
                            onClickfav_change_button();
                            break;

                        default:
                            break;
                    }
                }
            };
            onFavRecyclerViewTouchListener = new RecyclerView.OnItemTouchListener() {
                @Override
                public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_UP &&
                            mFavoritesRecyclerView.findChildViewUnder(event.getX(), event.getY()) == null) {
                        onClickfav_change_button();
                    }
                    return false;
                }

                @Override
                public void onTouchEvent(RecyclerView rv, MotionEvent e) {

                }

                @Override
                public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

                }
            };
            mFavoritesRecyclerView.addOnItemTouchListener(onFavRecyclerViewTouchListener);
        } else {
            editProfileClickListener = null;
            if (onFavRecyclerViewTouchListener != null) {
                mFavoritesRecyclerView.removeOnItemTouchListener(onFavRecyclerViewTouchListener);
                onFavRecyclerViewTouchListener = null;
            }
        }
        matches_page_setter_card_view1.setOnClickListener(editProfileClickListener);
//        indicator.setOnClickListener(editProfileClickListener);
        indicator2.setOnClickListener(editProfileClickListener);
        matches_page_scrim_lay.setOnClickListener(editProfileClickListener);
        matches_page_setter_hashtags_container.setOnClickListener(editProfileClickListener);
        profile_work_lay_id.setOnClickListener(editProfileClickListener);
        profile_edu_lay_id.setOnClickListener(editProfileClickListener);
        mFavoritesCard.setOnClickListener(editProfileClickListener);
    }

    public void onClickpics_change_button() {
        hidePopularityTooltip();
        ActivityHandler.startPhotosForResult(aActivity, false);
    }

    @OnClick(R.id.hashtags_change_button)
    public void onClickhashtags_change_button() {
        hidePopularityTooltip();
        ActivityHandler.startEditProfileActivityForResult(aActivity, EditProfilePageType.INTERESTS);
    }

    @OnClick(R.id.work_info_change_button)
    public void onClickwork_info_change_button() {
        hidePopularityTooltip();
        ActivityHandler.startEditProfileActivityForResult(aActivity, EditProfilePageType.PROFESSION);
    }

    @OnClick(R.id.edu_info_change_button)
    public void onClickedu_info_change_button() {
        hidePopularityTooltip();
        ActivityHandler.startEditProfileActivityForResult(aActivity, EditProfilePageType.EDUCATION);
    }

    @OnClick(R.id.fav_change_button)
    public void onClickfav_change_button() {
        hidePopularityTooltip();
        //ActivityHandler.startEditProfileActivityForResult(aActivity, EditProfilePageType.HOBBY);
        ActivityHandler.startEditFavoritesActivity(aActivity, CURRENT_FAVORITE_POS, true);
    }

    public void onClicktrustscore_change_button() {
        hidePopularityTooltip();
        ActivityHandler.startTrustBuilderForResult(aActivity);
    }

    @OnClick(R.id.profile_detail_change_button)
    public void onClickprofile_detail_change_button() {
        hidePopularityTooltip();
        ActivityHandler.startEditProfileActivityForResult(aActivity, EditProfilePageType.AGE);
    }

    /* Setup CARD VIEW - 1 */
    private void initializeCardView1() {
        /* View Pager and Adapter */

		/* making the view pager square as screen width varies with device */
        if (widthPx == 0) {
            DisplayMetrics displayMetrics = aActivity.getResources().getDisplayMetrics();
            widthPx = displayMetrics.widthPixels;
        }
        View view_pager_lay = rootLay.findViewById(R.id.view_pager_lay);
        LayoutParams params = view_pager_lay.getLayoutParams();
        params.height = widthPx;
        view_pager_lay.setLayoutParams(params);




		/* setString view pager adapter */
        matches_page_view_pager = (ViewPager) rootLay.findViewById(R.id.matches_page_view_pager);
        if (mAdapter != null) {
            mAdapter.onDestroy();
        }
        mAdapter = new MatchesPagesViewPagerAdapter2(aActivity, isMyProfile, isMutualMatch);

        /* FlowLayout of HashTags */
        matches_page_hashtags_lay = (FlowLayout) rootLay.findViewById(R.id.matches_page_hashtags_lay);
//        createDummyData();
    }

    private void setPhotoVisibiltyToast(boolean showToast) {

        if (showToast) {
            photo_visibility_layout.setVisibility(View.VISIBLE);
            photo_visibility_layout.startAnimation(AnimationUtils.loadAnimation(aActivity, R.anim.scale_left_in));
        } else {
            photo_visibility_layout.setVisibility(View.GONE);
        }

    }


    private void setCategoryNudge(ProfileNewModal aProfileNewModal) {
        UserModal aUser = aProfileNewModal.getUser();

        //Hard setting category nudge to gone
        //noinspection ConstantIfStatement
        if (true) {
            parent_category_nudge.setVisibility(View.GONE);
            return;
        }
        if (aUser != null && aUser.getVideoUrls() != null && aUser.getVideoUrls().length > 0) {
            parent_category_nudge.setVisibility(View.GONE);
            return;
        }

        String[] images = aProfileNewModal.getEventsImages();
        if (images != null && images.length > 0) {

            if (images.length > 3) {
                LayoutParams params = category_item_scrollview.getLayoutParams();
                params.height = UiUtils.dpToPx(120);
                category_item_scrollview.setLayoutParams(params);
            }


            if (images.length == 1) {
                Picasso.with(aActivity).load(images[0]).transform(new CircleTransformation()).into(first_iv);
                first_iv.setVisibility(View.VISIBLE);
                second_iv.setVisibility(View.GONE);
                third_iv.setVisibility(View.GONE);
//                fourth_iv.setVisibility(View.GONE);
                category_count_tv.setVisibility(View.GONE);

            } else if (images.length == 2) {
                Picasso.with(aActivity).load(images[0]).transform(new CircleTransformation()).into(first_iv);
                Picasso.with(aActivity).load(images[1]).transform(new CircleTransformation()).into(second_iv);
                first_iv.setVisibility(View.VISIBLE);
                second_iv.setVisibility(View.VISIBLE);
                third_iv.setVisibility(View.GONE);
//                fourth_iv.setVisibility(View.GONE);
                category_count_tv.setVisibility(View.GONE);

            } else if (images.length == 3) {
                Picasso.with(aActivity).load(images[0]).transform(new CircleTransformation()).into(first_iv);
                Picasso.with(aActivity).load(images[1]).transform(new CircleTransformation()).into(second_iv);
                Picasso.with(aActivity).load(images[2]).transform(new CircleTransformation()).into(third_iv);
                first_iv.setVisibility(View.VISIBLE);
                second_iv.setVisibility(View.VISIBLE);
                third_iv.setVisibility(View.VISIBLE);
//                fourth_iv.setVisibility(View.GONE);
                category_count_tv.setVisibility(View.GONE);
            } else {
                Picasso.with(aActivity).load(images[0]).transform(new CircleTransformation()).into(first_iv);
                Picasso.with(aActivity).load(images[1]).transform(new CircleTransformation()).into(second_iv);
                Picasso.with(aActivity).load(images[2]).transform(new CircleTransformation()).into(third_iv);
                Picasso.with(aActivity).load(images[3]).transform(new CircleTransformation()).into(fourth_iv);


                category_count_tv.setText("+" + (images.length - 3));
                first_iv.setVisibility(View.VISIBLE);
                second_iv.setVisibility(View.VISIBLE);
                third_iv.setVisibility(View.VISIBLE);
//                fourth_iv.setVisibility(View.VISIBLE);
                category_count_tv.setVisibility(View.VISIBLE);
            }
            height_layout.setVisibility(isMutualMatch ? View.GONE : View.VISIBLE);
            height_layout_2.setVisibility(isMutualMatch ? View.VISIBLE : View.GONE);
            String[] eventNames = aProfileNewModal.getEventNames();
            String[] eventImages = aProfileNewModal.getEventsImages();

            if (eventNames != null && eventImages.length > 0) {
                category_nudge_container.setVisibility(View.VISIBLE);
                View item;
                TextView name;
                ImageView image;
                category_nudge_container.removeAllViews();
                for (int i = 0; i < eventImages.length; i++) {

                    item = inflater.inflate(R.layout.category_nudge_item, null);
                    name = (TextView) item.findViewById(R.id.category_name_tv);
                    image = (ImageView) item.findViewById(R.id.category_iv);
                    name.setText(eventNames[i]);
                    Picasso.with(aActivity).load(eventImages[i]).transform(new CircleTransformation()).into(image);
                    category_nudge_container.addView(item);
                }
            }

            parent_category_nudge.setVisibility(View.VISIBLE);
            parent_category_nudge.startAnimation(AnimationUtils.loadAnimation(aActivity, R.anim.scale_left_in));

//            parent_category_nudge.startAnimation(AnimationUtils.loadAnimation(aActivity, R.anim.scale_left_in));
        } else {
            parent_category_nudge.setVisibility(View.GONE);
        }


    }

    public void toggleCategoryNudgeVisibility(boolean show) {

        category_image_container.setVisibility(show ? View.GONE : View.VISIBLE);
//        category_item_scrollview.setVisibility(show?View.VISIBLE : View.GONE);

        if (show) {
            mutual_events_description_tv.setGravity(Gravity.CENTER);
            category_item_scrollview.setVisibility(View.VISIBLE);
            category_item_scrollview.startAnimation(AnimationUtils.loadAnimation(aActivity, R.anim.scale_down));
            UiUtils.setBackground(aActivity, heading_container, R.drawable.transparent_rectangle_top_radius);

        } else {
            mutual_events_description_tv.setGravity(Gravity.LEFT);
            Animation scaleUp = AnimationUtils.loadAnimation(aActivity, R.anim.scale_up);
            scaleUp.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    UiUtils.setBackground(aActivity, heading_container, R.drawable.transparent_rectangle);
                    category_item_scrollview.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            category_item_scrollview.startAnimation(scaleUp);

        }


    }

    public void reloadProfileAd(AdsHelper adsHelper) {
        if (aProfileNewModal.isAdProfile() && adsHelper != null) {
            isAdClicked = false;
            matches_page_view_pager_lay.setVisibility(View.GONE);
            ad_container.setVisibility(View.VISIBLE);
            adsHelper.loadAd(aActivity, AdsType.MATCHES_MID_PROFILE_VIDEO, ad_container_view, false);
            setHashTags();
        }
    }

    public void loadHashTagsForAds(String[] interests) {
        if (aProfileNewModal.isAdProfile() && interests != null && interests.length > 0) {
            aProfileNewModal.setInterest(interests);
            setHashTags();
        }
    }

    public void setIsAdClicked(boolean isAdClicked) {
        this.isAdClicked = isAdClicked;
    }

    public boolean isCurrentProfileAd() {
        return (aProfileNewModal != null) && aProfileNewModal.isAdProfile();
    }

    public void stopProfileAd() {
        ad_container.setVisibility(View.GONE);
        ad_container_view.removeAllViews();
        ad_container_view.invalidate();
    }

    public boolean shouldReloadAdOnResume() {
        return isAdClicked && isCurrentProfileAd();
    }

    public FBMutualFriendsHandler getmFBMutualFriendsHandler() {
        if (mFBMutualFriendsHandler == null) {
            mFBMutualFriendsHandler = new FBMutualFriendsHandler(aActivity,
                    mutual_friend_layout, mFBOnClickListener);
        }
        return mFBMutualFriendsHandler;
    }

    public void toggleMutualFriendsLayout(boolean show, boolean isNewFriendList, ArrayList<FbFriendModal> list) {
        indicator2.setVisibility((list == null || list.size() == 0 || !show) ? View.VISIBLE : View.GONE);
        getmFBMutualFriendsHandler().toggleFbMutualFriendsList(show, isNewFriendList, list);
    }

    private void setCardView1(AdsHelper adsHelper) {

        if (aProfileNewModal == null || aProfileNewModal.getUser() == null) {
            return;
        }

        if (aProfileNewModal.isAdProfile()) {
            isAdClicked = false;
            matches_page_view_pager_lay.setVisibility(View.GONE);
            ad_container.setVisibility(View.VISIBLE);
            adsHelper.loadAd(aActivity, AdsType.MATCHES_MID_PROFILE_VIDEO, ad_container_view, false);
            setHashTags();
            toggleSponsoredEventDetailCard(false);
            mSelectTag.setVisibility(View.GONE);
            return;
        }
        matches_page_view_pager_lay.setVisibility(View.VISIBLE);
        ad_container.setVisibility(View.GONE);
        ad_container_view.removeAllViews();
        ad_container_view.invalidate();

        if (!isMyProfile && !isSingleMatch) {

            if (isMutualMatch) {
                setCategoryNudge(aProfileNewModal);
            } else {

                if (aProfileNewModal.isPhotoVisibilityToast()) {
                    setPhotoVisibiltyToast(true);
                } else {
                    setPhotoVisibiltyToast(false);
                }
                setCategoryNudge(aProfileNewModal);

            }
        }

        mutual_friend_layout.setVisibility(View.GONE);

        OnClickListener firstPicListener = null;
        // Create dummy listner when no photo_upload_nudge_skipped_view set for user
        if (aProfileNewModal.isSponsoredProfile() && Utility.isSet(aProfileNewModal.getmLandingUrl())) {
            firstPicListener = matchesPageViewPagerClickListener;
        } else if (aProfileNewModal.getUser().isDummySet() || isMyProfile) {
            firstPicListener = new OnClickListener() {

                @Override
                public void onClick(View v) {

                    ActivityHandler.startPhotosForResult(aActivity, false);
                }
            };
            matchesPageViewPagerClickListener = firstPicListener;
        } else {
            firstPicListener = matchesPageViewPagerClickListener;
        }

        matches_page_view_pager.setVisibility(View.VISIBLE);

        mAdapter.setListener(matchesPageViewPagerClickListener, firstPicListener);
//        mAdapter.setMatchId(aProfileNewModal.getUser().getUserId());

        ProfileViewPagerModal[] profileViewPagerModals = null;

        VideoModal[] videoModals = null;
        ArrayList<VideoModal> videoModalsList = aProfileNewModal.getUser().getVideoArray();
        if (videoModalsList != null && videoModalsList.size() > 0) {
            videoModals = videoModalsList.toArray(new VideoModal[videoModalsList.size()]);
        }

        profileViewPagerModals = ProfileViewPagerModal.createImagesArrayFromImagesAndVideos(
                aProfileNewModal.getUser().getOtherPics(), videoModals);
        if (mAdapter != null) {
            mAdapter.onDestroy();
        }
        mAdapter.changeList(profileViewPagerModals, aProfileNewModal.getUser().getUserId(), aProfileNewModal.isTMSelectMember());

        Utility.fireBusEvent(aActivity, true, new EventInfoUpdateEvent(
                EventInfoUpdateEvent.KEY_VIDEOS_COUNT,
                String.valueOf((videoModals != null) ? videoModals.length : 0)));
        Utility.fireBusEvent(aActivity, true, new EventInfoUpdateEvent(
                EventInfoUpdateEvent.KEY_PHOTOS_COUNT,
                String.valueOf((aProfileNewModal.getUser().getOtherPics() != null) ?
                        aProfileNewModal.getUser().getOtherPics().length : 0)));

        matches_page_view_pager.setAdapter(mAdapter);

        matches_page_view_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (mAdapter != null) {
                    mAdapter.onPageSelected(position);
                }

                if (position >= 1) {
                    if (isMutualMatch) {
                        ProfileNew.setFbMutualFriendsVisibilty();
                    } else {
                        MatchesPageLatest2.setFbMutualFriendsVisibilty();
                    }
//                    indicator.setVisibility(View.VISIBLE);
                    indicator2.setVisibility(View.VISIBLE);
                    mutual_friend_layout.setVisibility(View.GONE);

                }


                if (!isMyProfile) {
                    if (isFetched)
                        return;

                    String urls[] = aProfileNewModal.getUser().getOtherPics();
                    if (urls != null && urls.length <= 2)
                        return;

                    Utility.prefetchPhotos(aActivity, urls, 2, TrulyMadlyEvent.TrulyMadlyActivities.matches);
                    isFetched = true;
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

		/* Used for dots on top of photo_upload_nudge_skipped_view indicating no of photos */
//        indicator.setStrokeColor(
//                aActivity.getResources().getColor(R.color.default_circle_indicator_stroke_color_alternate));
//        indicator
//                .setFillColor(aActivity.getResources().getColor(R.color.default_circle_indicator_fill_color_alternate));
//        indicator.setViewPager(matches_page_view_pager);
        HashMap<CirclePageIndicator2.ITEM_TYPE, CirclePageIndicator2.ItemTypeModal> map = new HashMap<>();
        map.put(CirclePageIndicator2.ITEM_TYPE.CIRCLE,
                new CirclePageIndicator2.ItemTypeModal(R.layout.pager_indicator_image, R.drawable.pager_circle_selected,
                        R.drawable.pager_circle_unselected));
        map.put(CirclePageIndicator2.ITEM_TYPE.CAMERA,
                new CirclePageIndicator2.ItemTypeModal(R.layout.pager_indicator_image, R.drawable.pager_camera_selected,
                        R.drawable.pager_camera_unselected));
        ArrayList<CirclePageIndicator2.ITEM_TYPE> items = new ArrayList<>();

        if (profileViewPagerModals != null) {
            for (ProfileViewPagerModal profileViewPagerModal : profileViewPagerModals) {
                if (profileViewPagerModal.isVideo()) {
                    items.add(CirclePageIndicator2.ITEM_TYPE.CAMERA);
                } else {
                    items.add(CirclePageIndicator2.ITEM_TYPE.CIRCLE);
                }
            }
        }

        indicator2.setViewPager(matches_page_view_pager, map, items);

//        if (mAdapter.getCount() > 1) {
//            indicator.setVisibility(View.VISIBLE);
//        } else {
//            indicator.setVisibility(View.GONE);
//        }

        if (mAdapter.getCount() > 1) {
            indicator2.setVisibility(View.VISIBLE);
        } else {
            indicator2.setVisibility(View.GONE);
        }

		/* Name:Age , Location:height , LastLogin */
        UserModal aUserModal = aProfileNewModal.getUser();
        matches_page_match_name_age.setText("");
        matches_page_match_loc_height.setText("");
        showLastActive(null);

		/* Name */
        if (Utility.isSet(aUserModal.getName())) {
            matches_page_match_name_age.setText(aUserModal.getName());
        }

        sponsored_tv.setVisibility(View.GONE);
        toggleSponsoredEventDetailCard(false);
        if (aProfileNewModal.isSponsoredProfile()) {
            fb_friend_container.setVisibility(View.GONE);
            matches_page_match_celeb_status.setVisibility(View.GONE);
            if (mMyTrustScoreContainer != null) {
                mMyTrustScoreContainer.setVisibility(View.GONE);
            }
            mOthersTrustScoreContainer.setVisibility(View.GONE);
            if (aProfileNewModal.isSponsoredEventProfile()) {
                sponsored_tv.setVisibility(View.GONE);
                if (Utility.isSet(aProfileNewModal.getmSponsoredEventDetails())) {
                    toggleSponsoredEventDetailCard(true);
                    sponsored_event_details_description_tv.setText(aProfileNewModal.getmSponsoredEventDetails());
                } else {
                    toggleSponsoredEventDetailCard(false);
                }
            } else {
                sponsored_tv.setVisibility(View.VISIBLE);
                toggleSponsoredEventDetailCard(false);
            }
        } else {
            /* Age */
            if (Utility.isSet(aUserModal.getAge())) {
                matches_page_match_name_age.setText(matches_page_match_name_age.getText() + ", " + aUserModal.getAge());
            }

            if (aUserModal.isCelebStatus()) {
                matches_page_match_celeb_status.setVisibility(View.VISIBLE);
            } else {
                matches_page_match_celeb_status.setVisibility(View.GONE);
            }

            if (Utility.isSet(aUserModal.getBadgeUrl())) {
                matches_page_match_badge_image.setVisibility(View.VISIBLE);
                Picasso.with(aActivity).load(aUserModal.getBadgeUrl()).into(matches_page_match_badge_image);
            } else {
                matches_page_match_badge_image.setVisibility(View.GONE);
            }

		/* Location */
            String locationText = "";
            if (Utility.isSet(aUserModal.getCity())) {
                locationText = aUserModal.getCity();
            }

		/* Height */
            if (Utility.isSet(aUserModal.getHeight())) {
                int h = Integer.parseInt(aUserModal.getHeight());
                locationText += (Utility.isSet(aUserModal.getCity()) ? " | " : "") + h / 12 + "\'" + h % 12 + "\"";
                matches_page_match_loc_height.setText(locationText);
            }

//
//        /* FB mututal count */
//            if(aProfileNewModal.getFbMutualFriend()>-1)
//                locationText+=" | "+ aProfileNewModal.getFbMutualFriend();
//
//            matches_page_match_loc_height.setText(locationText);

		/* LastLogin ( Shown only for matches) */
            if (isLastActiveShown() && !isMutualMatch && !isMyProfile) {
                showLastActive(aUserModal.getLastActive());
            } else {
                showLastActive(null);
            }

		/* Mutual Friends */

//            aProfileNewModal.setFbMutualFriendCount(12);

            if ((!isMyProfile) && (aProfileNewModal.getFbMutualFriendCount() > 0)) {
                //Removing the separator as we have moved the icon to the right
//                matches_page_match_loc_height.append(" | ");
                fb_friend_container.setVisibility(View.VISIBLE);
                mutual_friend_count.setText("" + aProfileNewModal.getFbMutualFriendCount());
            } else {
                fb_friend_container.setVisibility(View.GONE);
            }

			/* Trust Score */
            if (isMyProfile) {
                mOthersTrustScoreContainer.setVisibility(View.GONE);
                setMyTrustScoreCard();
            } else {
                if (mMyTrustScoreContainer != null) {
                    mMyTrustScoreContainer.setVisibility(View.GONE);
                }
                setOthersTrustScore();
            }
        }

        if (TMSelectHandler.isSelectEnabled(aActivity) && aProfileNewModal.isTMSelectMember()
                && !isMyProfile) {
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mSelectTag.getLayoutParams();
            layoutParams.topMargin = ((!isMutualMatch) ?
                    aActivity.getResources().getDimensionPixelSize(R.dimen.toolbar_height) : 0)
                    + UiUtils.dpToPx(8);
            mSelectTag.setLayoutParams(layoutParams);
            mSelectTag.setVisibility(View.VISIBLE);
        } else {
            mSelectTag.setVisibility(View.GONE);
        }

        setHashTags();
    }

    private void toggleSponsoredEventDetailCard(boolean toShow) {
        if (toShow) {
            if (sponsored_event_detail == null) {
                sponsored_event_detail = sponsored_event_detail_stub.inflate();
                sponsored_event_details_description_tv = (TextView) sponsored_event_detail.findViewById(R.id.event_details_description_tv);
            }
            sponsored_event_detail.setVisibility(View.VISIBLE);
        } else {
            sponsored_event_detail_stub.setVisibility(View.GONE);
            if (sponsored_event_detail != null) {
                sponsored_event_detail.setVisibility(View.GONE);
            }
        }
    }

    private void setHashTags() {
        /* Set List of Hashtags */
        String[] interest = null;
        String[] commonInterests = null;
        interest = aProfileNewModal.getInterest();
        commonInterests = aProfileNewModal.getCommonInterestModal();

        if (interest != null) {
            matches_page_hashtags_lay.removeAllViews();
            for (String anInterest : interest)
                if (Utility.isSet(anInterest)) {
                    matches_page_hashtags_lay.setVisibility(View.VISIBLE);
                    View v = inflater.inflate(R.layout.hashtags_or_fav_list_lay, matches_page_hashtags_lay, false);
                    TextView text = (TextView) v.findViewById(R.id.hashtags_or_fav_list_tv);
                    text.setText(anInterest);

					/*
                     * Check if this Hashtag of match is common with user If yes
					 * , setString blue background Done only for Matches
					 */
                    if (!isMyProfile) {
                        for (int j = 0; commonInterests != null && j < commonInterests.length; j++)
                            if (Utility.isSet(commonInterests[j])
                                    && commonInterests[j].length() + 1 == anInterest.length()
                                    && anInterest.indexOf(commonInterests[j]) == 1) {
                                UiUtils.setBackground(aActivity, v, R.drawable.hashtag_background_common);
                                text.setTextColor(aActivity.getResources().getColor(R.color.black));
                                text.setTypeface(Typeface.create("sans-serif-medium", Typeface.NORMAL));
//                                text.setTypeface(null, Typeface.BOLD);
                            }
                    }

                    matches_page_hashtags_lay.addView(v);
                }

        } else {
            /* No hashtags available */
            matches_page_hashtags_lay.setVisibility(View.GONE);
        }
    }

    private void showLastActive(String lastActive) {
        if (Utility.isSet(lastActive)) {
            last_login_icon.setVisibility(View.VISIBLE);
            last_login_container.setVisibility(View.VISIBLE);
            matches_page_match_last_login_time.setText(lastActive);
        } else {
            last_login_icon.setVisibility(View.GONE);
            last_login_container.setVisibility(View.GONE);
        }
    }

    private void onClickActivityDashboardToolTipIcon() {
        if (!isPopularityTootipVisible) {
            isPopularityTootipVisible = true;
            activity_dashboard_tooltip_box.setVisibility(View.VISIBLE);
            rootLay.setOnClickListener(rootLayClickListener);
        } else {
            rootLay.setOnClickListener(null);
            hidePopularityTooltip();
        }

    }

    public void onClickActivityDashboard() {
        if (Utility.stringCompare(aProfileNewModal.getActivityDashBoardClickAction(), "PHOTO")) {
            ActivityHandler.startPhotosForResult(aActivity, false);
        }
    }

    private void hidePopularityTooltip() {
        if (isPopularityTootipVisible) {
            activity_dashboard_tooltip_box.setVisibility(View.GONE);
            isPopularityTootipVisible = false;
            rootLay.setOnClickListener(null);
        }
    }

    private void setInterCardView() {

        /* populate activity dashboard for my profile */
        if (!aProfileNewModal.isSponsoredProfile() && aProfileNewModal.isActivityDashBoardAvailable()) {
            if (profile_activity_dashboard == null) {
                profile_activity_dashboard = profile_activity_dashboard_stub.inflate();
                activity_dashboard_tooltip_box_text = (TextView) profile_activity_dashboard.findViewById(R.id.activity_dashboard_tooltip_box_text);
                activity_dashboard_tooltip_box = profile_activity_dashboard.findViewById(R.id.activity_dashboard_tooltip_box);
                popularity_suggestion = (TextView) profile_activity_dashboard.findViewById(R.id.popularity_suggestion);
                count_views_text = (TextView) profile_activity_dashboard.findViewById(R.id.count_views_text);
                count_views = (TextView) profile_activity_dashboard.findViewById(R.id.count_views);
                count_views_container = profile_activity_dashboard.findViewById(R.id.count_views_container);
                blue_bar = (RelativeLayout) profile_activity_dashboard.findViewById(R.id.blue_bar);
                popularity_bar_container = profile_activity_dashboard.findViewById(R.id.popularity_bar_container);
                View activity_dashboard_tooltip_icon = profile_activity_dashboard.findViewById(R.id.activity_dashboard_tooltip_icon);
                activity_dashboard_tooltip_icon.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onClickActivityDashboardToolTipIcon();
                    }
                });
                profile_activity_dashboard.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onClickActivityDashboard();
                    }
                });
            }
            if (aMatchesSysMesgModal != null) {
                popularityToolTip = aMatchesSysMesgModal.getPopularityToolTipText();
            }
            activity_dashboard_tooltip_box_text.setText(popularityToolTip);

            if (Utility.isSet(aProfileNewModal.getActivityDashBoardProfileViews())) {
                profile_activity_dashboard.setVisibility(View.VISIBLE);
                profile_activity_dashboard_card_bottom_border.setVisibility(View.VISIBLE);
                count_views_container.setVisibility(View.VISIBLE);
                count_views.setText(aProfileNewModal.getActivityDashBoardProfileViews());
                count_views_text.setText(aProfileNewModal.getActivityDashBoardProfileViewsText());
            } else {
                count_views_container.setVisibility(View.GONE);
            }

            if (aProfileNewModal.getActivityDashBoardPopularity() >= 0
                    && aProfileNewModal.getActivityDashBoardPopularity() <= 5) {
                profile_activity_dashboard.setVisibility(View.VISIBLE);
                profile_activity_dashboard_card_bottom_border.setVisibility(View.VISIBLE);
                popularity_bar_container.setVisibility(View.VISIBLE);
                blue_bar.setLayoutParams(new LinearLayout.LayoutParams(UiUtils.dpToPx(10), LayoutParams.MATCH_PARENT,
                        aProfileNewModal.getActivityDashBoardPopularity()));
            } else {
                popularity_bar_container.setVisibility(View.INVISIBLE);
            }

            if (Utility.isSet(aProfileNewModal.getActivityDashBoardPopularityTip())) {
                profile_activity_dashboard.setVisibility(View.VISIBLE);
                profile_activity_dashboard_card_bottom_border.setVisibility(View.VISIBLE);
                popularity_suggestion.setVisibility(View.VISIBLE);
                popularity_suggestion.setText(aProfileNewModal.getActivityDashBoardPopularityTip());
            } else {
                popularity_suggestion.setVisibility(View.GONE);
            }

        } else {
            /* If information not available , hide view */
            if (profile_activity_dashboard != null) {
                profile_activity_dashboard.setVisibility(View.GONE);
            } else {
                profile_activity_dashboard_stub.setVisibility(View.GONE);
            }
            profile_activity_dashboard_card_bottom_border.setVisibility(View.GONE);
        }

    }

    private void setCardView2() {

        if (aProfileNewModal != null && (aProfileNewModal.isSponsoredProfile() || aProfileNewModal.isAdProfile())) {
            cardView2.setVisibility(View.GONE);
            matches_page_setter_work_edu_card_bottom_border.setVisibility(View.GONE);
            return;
        } else {
            cardView2.setVisibility(View.VISIBLE);
            matches_page_setter_work_edu_card_bottom_border.setVisibility(View.VISIBLE);
        }

		/* Get Work , Designation , Income and CompaniesList */
        boolean incomeShown = false, companiesShown = false, collegesShown = false;
        if (aProfileNewModal != null && aProfileNewModal.getWork() != null) {
            WorkModal aWorkModal = aProfileNewModal.getWork();
            UserModal aUserModal = aProfileNewModal.getUser();

			/* setString Designation , Income */
            String desigSalText = "";
            boolean desigAvailable = false;

            if (aWorkModal != null && Utility.isSet(aWorkModal.getDesignation())) {
                desigSalText += aWorkModal.getDesignation();
                desigAvailable = true;
            } else if (aWorkModal != null && Utility.isSet(aWorkModal.getWorkStatus())) {
                if (aWorkModal.getWorkStatus().equalsIgnoreCase("NO")) {
                    desigSalText += aActivity.getResources().getString(R.string.not_working);
                }
            }

            /* Hiding Income */
//            boolean isIncome = aUserModal != null && Utility.isSet(aUserModal.getIncomeStart());
//            if (desigAvailable && (isMyProfile || isIncome)) {
//                desigSalText += ", ";
//            }
//            if (isIncome) {
//                desigSalText += aUserModal.getIncomeStart();
//                incomeShown = true;
//                profile_work_designation_salary_tv.setText(desigSalText);
//            } else if (isMyProfile) {
//                profile_work_designation_salary_tv
//                        .setText(Html.fromHtml(desigSalText + "<font color='#D49F00'>" + aActivity.getResources().getString(R.string.add_income) + "</font>"));
//            } else {
//                profile_work_designation_salary_tv.setText(desigSalText);
//            }
            profile_work_designation_salary_tv.setText(desigSalText);

			/* setString list of companies */

            profile_work_companies_lay.setText("");
            profile_work_companies_lay.setVisibility(View.GONE);
            if (aWorkModal != null && aWorkModal.getCompanies() != null && aWorkModal.getCompanies().length > 0) {
                String[] companies = aWorkModal.getCompanies();
                for (int i = 0; i < companies.length; i++)
                    if (Utility.isSet(companies[i])) {
                        companiesShown = true;
//                        profile_work_companies_lay.setTextColor(aActivity.getResources().getColor(R.color.colorSubHeading));
                        profile_work_companies_lay
                                .setText(profile_work_companies_lay.getText() + (i > 0 ? ", " : "") + companies[i]);
                        profile_work_companies_lay.setVisibility(View.VISIBLE);
                    }
            }
            if (isMyProfile && !companiesShown) {
                profile_work_companies_lay.setTextColor(aActivity.getResources().getColor(R.color.orange));
                profile_work_companies_lay.setText("Add Workplace");
                profile_work_companies_lay.setVisibility(View.VISIBLE);
            }
        }

		/* Get Highest Education and SchoolsList */
        if (aProfileNewModal != null && aProfileNewModal.getEducation() != null) {
            EducationModal aEduModal = aProfileNewModal.getEducation();

            if (aEduModal != null && Utility.isSet(aEduModal.getHighestEdu())) {
                profile_edu_highest_tv.setText(aEduModal.getHighestEdu());
            }

			/* setString list of schools Attended */
            profile_edu_places_lay.setText("");
            profile_edu_places_lay.setVisibility(View.GONE);
            if (aEduModal != null && aEduModal.getInstitutes() != null && aEduModal.getInstitutes().length > 0) {

                String[] institutes = aEduModal.getInstitutes();
                for (int i = 0; i < institutes.length; i++)
                    if (Utility.isSet(institutes[i])) {
                        collegesShown = true;
                        //profile_edu_places_lay.setTextColor(aActivity.getResources().getColor(R.color.colorSubHeading));
                        profile_edu_places_lay
                                .setText(profile_edu_places_lay.getText() + (i > 0 ? ", " : "") + institutes[i]);
                        profile_edu_places_lay.setVisibility(View.VISIBLE);
                    }
            }
            if (isMyProfile && !collegesShown) {
                profile_edu_places_lay.setTextColor(aActivity.getResources().getColor(R.color.orange));
                profile_edu_places_lay.setText("Add Institute");
                profile_edu_places_lay.setVisibility(View.VISIBLE);
            }
        }

//        if (isMyProfile && (!companiesShown || !collegesShown || !incomeShown)) {
        if (isMyProfile && (!companiesShown || !collegesShown)) {
            alert_work_edu.setVisibility(View.VISIBLE);
        } else {
            alert_work_edu.setVisibility(View.GONE);
        }
    }

    /*
     * Select the first favorite layout whose data is available Else , Hide
     * recycler view
     */
    private void setFirstExistingFavLay(View[] cur_selected_lay, View[] cur_selected_lay_line, View[] cur_no_data_lay) {
        int pos;
        for (pos = 0; pos < 5; pos++)
            if (favDataMap != null && favDataMap.get(TAB_CATEGORY_KEYS[pos]) != null && favDataMap.get(TAB_CATEGORY_KEYS[pos]).size() > 0)
                break;

        if (favDataMap == null || pos < 0 || pos > 4) {
            mFavoritesRecyclerView.setVisibility(View.GONE);
            return;
        } else {
            mFavoritesRecyclerView.setVisibility(View.VISIBLE);
        }
        selectfavLay(pos);
        cur_selected_lay[cur_selected_lay_id].setVisibility(View.VISIBLE);
        cur_selected_lay_line[cur_selected_lay_id].setVisibility(View.VISIBLE);
        cur_no_data_lay[cur_selected_lay_id].setVisibility(View.GONE);
        FavoriteUtils.prefetchFavoriteImages(aActivity, favDataMap, pos, FAVORITE_IMAGE_PREFETCH_LIMIT);
    }

    private void selectfavLay(int pos) {
        if (cur_selected_lay_id != pos) {
            if (cur_selected_lay_id >= 0 && cur_selected_lay_id <= 4) {
                int unselectedIcon = getMfavUnselectedIcons()[cur_selected_lay_id];
                //hard removing mutual favorite icon
                //noinspection PointlessBooleanExpression
                if (false && isMutualFavPresent()) {
                    unselectedIcon = getmFavMutualIcons()[cur_selected_lay_id];
                }
                Picasso.with(aActivity.getApplicationContext()).load(unselectedIcon).noFade().into(getmDefaultFavIVs()[cur_selected_lay_id]);
                getmFavSelectedLineViews()[cur_selected_lay_id].setVisibility(View.GONE);
            }

            cur_selected_lay_id = pos;
            getmFavSelectedLineViews()[cur_selected_lay_id].setVisibility(View.VISIBLE);
            int selectedIcon = getmFavSelectedIcons()[cur_selected_lay_id];
            Picasso.with(aActivity.getApplicationContext()).load(selectedIcon).noFade().into(getmDefaultFavIVs()[cur_selected_lay_id]);
            //Using the following 2 statements as a hack to ensure scroll always happens
            mFavoritesRecyclerView.scrollToPosition(0);
            mFavoritesRecyclerView.getLayoutManager().scrollToPosition(0);
            rAdapter.changeData(favDataMap.get(TAB_CATEGORY_KEYS[pos]));
            CURRENT_FAVORITE_POS = pos;
        }
    }

    public int[] getMfavUnselectedIcons() {
        if (mfavUnselectedIcons == null) {
            mfavUnselectedIcons = new int[]{R.drawable.fav_movies, R.drawable.fav_music,
                    R.drawable.fav_books, R.drawable.fav_food, R.drawable.fav_others};
        }
        return mfavUnselectedIcons;
    }

    public int[] getmFavSelectedIcons() {
        if (mFavSelectedIcons == null) {
            mFavSelectedIcons = new int[]{R.drawable.fav_movies_selected, R.drawable.fav_music_selected,
                    R.drawable.fav_books_selected, R.drawable.fav_food_selected, R.drawable.fav_others_selected};
        }
        return mFavSelectedIcons;
    }

    public int[] getmFavMutualIcons() {
        if (mFavMutualIcons == null) {
            mFavMutualIcons = new int[]{R.drawable.fav_movies_mutual, R.drawable.fav_music_mutual,
                    R.drawable.fav_books_mutual, R.drawable.fav_food_mutual, R.drawable.fav_others_mutual};
        }
        return mFavMutualIcons;
    }

    public ImageView[] getmDefaultFavIVs() {
        if (mDefaultFavIVs == null) {
            mDefaultFavIVs = new ImageView[]{fav_movies_selected_icon, fav_music_selected_icon,
                    fav_books_selected_icon, fav_food_selected_icon, fav_misc_selected_icon};
        }
        return mDefaultFavIVs;
    }

    public View[] getmFavSelectedLineViews() {
        if (mFavSelectedLineViews == null) {
            mFavSelectedLineViews = new View[]{fav_movies_selected_line, fav_music_selected_line,
                    fav_books_selected_line, fav_food_selected_line, fav_misc_selected_line};
        }
        return mFavSelectedLineViews;
    }

    public View[] getmDisabledFavViews() {
        if (mDisabledFavViews == null) {
            mDisabledFavViews = new View[]{fav_movies_no_data_lay, fav_music_no_data_lay,
                    fav_books_no_data_lay, fav_food_no_data_lay, fav_misc_no_data_lay};
        }
        return mDisabledFavViews;
    }

    public View[] getmDefaultFavViews() {
        if (mDefaultFavViews == null) {
            mDefaultFavViews = new View[]{fav_movies_selected_lay, fav_music_selected_lay,
                    fav_books_selected_lay, fav_food_selected_lay, fav_misc_selected_lay};
        }
        return mDefaultFavViews;
    }

    private boolean isMutualFavPresent() {
        if (favDataMap != null) {
            switch (cur_selected_lay_id) {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                    return isMutualFavPresent(checkNotNull(favDataMap.get(TAB_CATEGORY_KEYS[cur_selected_lay_id])));
                default:
                    return false;
            }
        }

        return false;
    }

    private boolean isMutualFavPresent(ArrayList<FavoriteDataModal> favoriteDataModals) {
        for (FavoriteDataModal f : favoriteDataModals) {
            if (checkNotNull(f).isMutualFavorite()) {
                return true;
            }
        }
        return false;
    }

    private void setFavoritesCard() {

        if (aProfileNewModal != null && (aProfileNewModal.isSponsoredProfile() || aProfileNewModal.isAdProfile())) {
            mFavoritesCard.setVisibility(View.GONE);
            return;
        } else {
            mFavoritesCard.setVisibility(View.VISIBLE);
        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(aActivity, LinearLayout.HORIZONTAL, false);
        rAdapter = new FavCustomRecyclerAdapter(aActivity, editProfileClickListener);

        mFavoritesRecyclerView.setLayoutManager(layoutManager);
        mFavoritesRecyclerView.setAdapter(rAdapter);

        mFavoritesRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (!isFavoritesViewed) {
                    Utility.fireBusEvent(aActivity, true, new EventInfoUpdateEvent(
                            EventInfoUpdateEvent.KEY_FAVORITES_VIEWED, "true"));
                    isFavoritesViewed = true;
                }
            }
        });


        int favoritesAvailable = 0;

        favDataMap = aProfileNewModal.getFavoritesDataMap();
        if (favDataMap != null) {
            favoritesAvailable += addFavorites(favDataMap.get(FAVORITE_KEY_MOVIES), fav_movies_selected_icon, R.drawable.fav_movies);
            favoritesAvailable += addFavorites(favDataMap.get(FAVORITE_KEY_MUSIC), fav_music_selected_icon, R.drawable.fav_music);
            favoritesAvailable += addFavorites(favDataMap.get(FAVORITE_KEY_BOOKS), fav_books_selected_icon, R.drawable.fav_books);
            favoritesAvailable += addFavorites(favDataMap.get(FAVORITE_KEY_FOOD), fav_food_selected_icon, R.drawable.fav_food);
            favoritesAvailable += addFavorites(favDataMap.get(FAVORITE_KEY_OTHERS), fav_misc_selected_icon, R.drawable.fav_others);
        }

        cur_selected_lay_id = -1;

        View[] cur_selected_lay = {fav_movies_selected_lay, fav_music_selected_lay, fav_books_selected_lay,
                fav_food_selected_lay, fav_misc_selected_lay};
        View[] cur_selected_lay_line = {fav_movies_selected_line, fav_music_selected_line, fav_books_selected_line,
                fav_food_selected_line, fav_misc_selected_line};
        View[] cur_no_data_lay = {fav_movies_no_data_lay, fav_music_no_data_lay, fav_books_no_data_lay,
                fav_food_no_data_lay, fav_misc_no_data_lay};

        for (int id = 0; id < 5; id++) {
            cur_selected_lay_line[id].setVisibility(View.INVISIBLE);
            if (favDataMap == null || favDataMap.get(TAB_CATEGORY_KEYS[id]) == null || favDataMap.get(TAB_CATEGORY_KEYS[id]).size() == 0) {
                cur_selected_lay[id].setVisibility(View.GONE);
                cur_no_data_lay[id].setVisibility(View.VISIBLE);
            } else {
                cur_selected_lay[id].setVisibility(View.VISIBLE);
                cur_no_data_lay[id].setVisibility(View.GONE);
            }
        }

        setFirstExistingFavLay(cur_selected_lay, cur_selected_lay_line, cur_no_data_lay);

        if (isMyProfile && favoritesAvailable < 5) {
            alert_favorites.setVisibility(View.VISIBLE);
        } else {
            alert_favorites.setVisibility(View.GONE);
        }

    }

    private int addFavorites(ArrayList<FavoriteDataModal> favData, ImageView fav_selected_icon,
                             int normalIcon) {
        if (favData != null) {
            if (favData.size() > 0) {
                Picasso.with(aActivity).load(normalIcon).into(fav_selected_icon);
            }
        }

        return favData.size() > 0 ? 1 : 0;

    }

    @OnClick({R.id.matches_page_fav_movies_lay, R.id.matches_page_fav_music_lay, R.id.matches_page_fav_books_lay,
            R.id.matches_page_fav_food_lay, R.id.matches_page_fav_misc_lay})
    public void onClickFavLay(View v) {
//        View[] cur_selected_lay_line = {fav_movies_selected_line, fav_music_selected_line, fav_books_selected_line,
//                fav_food_selected_line, fav_misc_selected_line};

        int idx = Integer.parseInt((String) v.getTag());
        hidePopularityTooltip();
        if (favDataMap == null || favDataMap.get(TAB_CATEGORY_KEYS[idx]) == null || favDataMap.get(TAB_CATEGORY_KEYS[idx]).size() == 0)
            return;

//        if (cur_selected_lay_id >= 0 && cur_selected_lay_id <= 4) {
//            cur_selected_lay_line[cur_selected_lay_id].setVisibility(View.INVISIBLE);
//        }

        Map<String, String> map = new HashMap<>();
        String activityName;
        if (isMyProfile) {
            activityName = TrulyMadlyEvent.TrulyMadlyActivities.my_profile_fav_click;
        } else if (isMutualMatch) {
            map.put("match_id", aProfileNewModal.getUser().getUserId());
            activityName = TrulyMadlyEvent.TrulyMadlyActivities.mutual_match_fav_click;
        } else {
            map.put("match_id", aProfileNewModal.getUser().getUserId());
            activityName = TrulyMadlyEvent.TrulyMadlyActivities.match_fav_click;
        }

        map.put("fav_count", favDataMap.get(TAB_CATEGORY_KEYS[idx]).size() + "");


        if (idx == 0) {
            TrulyMadlyTrackEvent.trackEvent(aActivity, activityName, TrulyMadlyEvent.TrulyMadlyEventTypes.movies_click, 0, null, map, true);
        } else if (idx == 1) {
            TrulyMadlyTrackEvent.trackEvent(aActivity, activityName, TrulyMadlyEvent.TrulyMadlyEventTypes.music_click, 0, null, map, true);
        } else if (idx == 2) {
            TrulyMadlyTrackEvent.trackEvent(aActivity, activityName, TrulyMadlyEvent.TrulyMadlyEventTypes.books_click, 0, null, map, true);
        } else if (idx == 3) {
            TrulyMadlyTrackEvent.trackEvent(aActivity, activityName, TrulyMadlyEvent.TrulyMadlyEventTypes.food_click, 0, null, map, true);
        } else if (idx == 4) {
            TrulyMadlyTrackEvent.trackEvent(aActivity, activityName, TrulyMadlyEvent.TrulyMadlyEventTypes.others_click, 0, null, map, true);
        }

        selectfavLay(idx);
        if (!isFavoritesViewed) {
            Utility.fireBusEvent(aActivity, true, new EventInfoUpdateEvent(
                    EventInfoUpdateEvent.KEY_FAVORITES_VIEWED, "true"));
            isFavoritesViewed = true;
        }

        for (int i = 0; i < 5; i++) {
            FavoriteUtils.prefetchFavoriteImages(aActivity, favDataMap, i, FAVORITE_IMAGE_PREFETCH_LIMIT);
        }
    }

    private void setMyTrustScoreCard() {
        if (mMyTrustScoreCard == null) {
            mMyTrustScoreCard = mMyTrustScoreCardStub.inflate();
            mMyTrustScoreContainer = mMyTrustScoreCard.findViewById(R.id.my_trustscore_container);
            alert_trust_score = mMyTrustScoreCard.findViewById(R.id.alert_trust_score);
            View trustscore_change_button = mMyTrustScoreCard.findViewById(R.id.trustscore_change_button);
            trustscore_change_button.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    onClicktrustscore_change_button();
                }
            });
            mMyTrustScoreCard.setOnClickListener(editProfileClickListener);
        }
        mMyTrustScoreCard.setVisibility(View.VISIBLE);
        my_trustscore_card_bottom_border.setVisibility(View.VISIBLE);
        TrustBuilderModal trustBuilderModal = aProfileNewModal != null ? aProfileNewModal.getTrustBuilder() : null;

        int color = Color.BLACK;

        mMyTrustScoreContainer.setVisibility(View.VISIBLE);
        ImageView mMyTSFacebookIV = findById(mMyTrustScoreContainer, R.id.fb);
        TextView mMyTSFacebookTV = findById(mMyTrustScoreContainer, R.id.fb_count);
        ImageView mMyTSLinkedinIV = findById(mMyTrustScoreContainer, R.id.linkedin);
        TextView mMyTSLinkedinTV = findById(mMyTrustScoreContainer, R.id.linedin_text);
        ImageView mMyTSPhoneIV = findById(mMyTrustScoreContainer, R.id.phone);
        ImageView mMyTSPhotoIV = findById(mMyTrustScoreContainer, R.id.photo);
        ImageView mMyTSreferencesIV = findById(mMyTrustScoreContainer, R.id.endorsement);
        TextView mMyTSreferencesTV = findById(mMyTrustScoreContainer, R.id.endorsement_text);
        ImageView mMyTSProgressCenterIV = findById(mMyTrustScoreContainer, R.id.ts_progress_center_iv);
        Picasso.with(aActivity).load(R.drawable.verification_black).into(mMyTSProgressCenterIV);
        ProgressBar mMyTrustscoreProgressbar = findById(mMyTrustScoreContainer, R.id.trust_score_progressbar);
        mMyTrustscoreProgressbar.setBackground(ActivityCompat.getDrawable(aActivity, R.drawable.progressbar_background_black));
        mMyTrustscoreProgressbar.setProgressDrawable(aActivity.getResources().getDrawable(R.drawable.circular_progressbar_black));
        View mTSWhiteCenter = findById(mMyTrustScoreContainer, R.id.ts_black_center);
        mTSWhiteCenter.setVisibility(View.GONE);
        CircularRevealView mMyCircularRevealView = findById(mMyTrustScoreContainer, R.id.ts_revealview);
        mMyCircularRevealView.setVisibility(View.GONE);

        mMyTSFacebookTV.setTextColor(color);
        mMyTSFacebookTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.fb_mutual_friends_dark, 0);
        mMyTSLinkedinTV.setTextColor(color);
        mMyTSLinkedinTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ln_conn_dark, 0);
        mMyTSreferencesTV.setTextColor(color);
        mMyTSreferencesTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.fb_mutual_friends_dark, 0);


        mMyTrustscoreProgressbar.setProgress(1);
        if (trustBuilderModal != null) {
            mMyTrustscoreProgressbar.setProgress(trustBuilderModal.getTrustScore());
            if (trustBuilderModal.getTrustScore() < 100) {
                alert_trust_score.setVisibility(View.VISIBLE);
            } else {
                alert_trust_score.setVisibility(View.GONE);
            }

        } else {
            mMyTrustscoreProgressbar.setProgress(0);
            alert_trust_score.setVisibility(View.VISIBLE);
        }

        findById(mMyTrustScoreContainer, R.id.fb_container).setVisibility(View.VISIBLE);
        findById(mMyTrustScoreContainer, R.id.phone_container).setVisibility(View.VISIBLE);
        findById(mMyTrustScoreContainer, R.id.linkedin_container).setVisibility(View.VISIBLE);
        findById(mMyTrustScoreContainer, R.id.photo_container).setVisibility(View.VISIBLE);
        findById(mMyTrustScoreContainer, R.id.endorsement_container).setVisibility(View.VISIBLE);

        //Facebook
        if (trustBuilderModal != null && Utility.isSet(trustBuilderModal.getFBConn())) {
            try {
                Integer.parseInt(trustBuilderModal.getFBConn());
                mMyTSFacebookTV.setVisibility(View.VISIBLE);
                mMyTSFacebookTV.setText(trustBuilderModal.getFBConn() + " ");
            } catch (NumberFormatException e) {
                mMyTSFacebookTV.setVisibility(View.INVISIBLE);
            }
            Picasso.with(aActivity).load(R.drawable.ts_fb_enabled_dark).into(mMyTSFacebookIV);
        } else {
            mMyTSFacebookTV.setVisibility(View.INVISIBLE);
            Picasso.with(aActivity).load(R.drawable.ts_fb_disabled_dark).into(mMyTSFacebookIV);
        }

        //Phone
        if (trustBuilderModal != null && Utility.isSet(trustBuilderModal.getPhnNum())) {
            Picasso.with(aActivity).load(R.drawable.ts_phone_enabled_dark).into(mMyTSPhoneIV);
        } else {
            Picasso.with(aActivity).load(R.drawable.ts_phone_disabled_dark).into(mMyTSPhoneIV);
        }

        //LinkedIn
        int linkedInConnInt = 0;
        if (trustBuilderModal != null && (linkedInConnInt = trustBuilderModal.getLinkedInConn()) > 0) {
            mMyTSLinkedinTV.setVisibility(View.VISIBLE);
            mMyTSLinkedinTV.setText(String.valueOf(linkedInConnInt));
            Picasso.with(aActivity).load(R.drawable.ts_ln_enabled_dark).into(mMyTSLinkedinIV);
        } else {
            mMyTSLinkedinTV.setVisibility(View.INVISIBLE);
            Picasso.with(aActivity).load(R.drawable.ts_ln_disabled_dark).into(mMyTSLinkedinIV);
        }

        //Photo
        if (trustBuilderModal != null && (Utility.isSet(trustBuilderModal.getIDProof()) ||
                Utility.isSet(trustBuilderModal.getAddressProof()))) {
            Picasso.with(aActivity).load(R.drawable.ts_photoid_enabled_dark).into(mMyTSPhotoIV);
        } else {
            Picasso.with(aActivity).load(R.drawable.ts_photoid_disabled_dark).into(mMyTSPhotoIV);
        }

        //References
        if (trustBuilderModal != null && trustBuilderModal.getEndorsers() != null && trustBuilderModal.getEndorsers().size() > 0) {
//				mMyTSreferencesTV.setVisibility(View.VISIBLE);
//				mMyTSreferencesTV.setText(String.valueOf(trustBuilderModal.getEndorsers().size()) + " ");
            mMyTSreferencesTV.setVisibility(View.INVISIBLE);
            Picasso.with(aActivity).load(R.drawable.ts_ref_enabled_dark).into(mMyTSreferencesIV);
        } else {
            mMyTSreferencesTV.setVisibility(View.INVISIBLE);
            Picasso.with(aActivity).load(R.drawable.ts_ref_disabled_dark).into(mMyTSreferencesIV);
        }
    }

    @Override
    public void onStart() {
        if (mAdapter != null) {
            mAdapter.onStart();
        }
    }

    @Override
    public void onStop() {
        if (mAdapter != null) {
            mAdapter.onStop();
        }
    }

    @Override
    public void onPause() {
        if (mAdapter != null) {
            mAdapter.onPause();
        }
    }

    @Override
    public void onResume() {
        if (mAdapter != null) {
            mAdapter.onResume();
        }
    }

    @Override
    public void onDestroy() {
        if (mAdapter != null) {
            mAdapter.onDestroy();
        }
    }

    @Override
    public void onCreate() {
        if (mAdapter != null) {
            mAdapter.onCreate();
        }
    }

    @Override
    public void onNetworkChanged(NetworkChangeEvent networkChangeEvent) {
        if (mAdapter != null) {
            mAdapter.onNetworkChanged(networkChangeEvent);
        }
    }
}
