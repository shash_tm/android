package com.trulymadly.android.app;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.adapter.HashtagAutoCompleteAdapter;
import com.trulymadly.android.app.bus.BackPressedRegistrationFlow;
import com.trulymadly.android.app.bus.NextRegistrationFragment;
import com.trulymadly.android.app.listener.GetStartTimeInterface;
import com.trulymadly.android.app.listener.OnHashtagSelected;
import com.trulymadly.android.app.modal.HashtagModal;
import com.trulymadly.android.app.modal.UserData;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.RandomHashtagGenerator;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;

import org.apmem.tools.layouts.FlowLayout;

import java.util.ArrayList;

public class MaterialDesignEditHashtags extends Fragment implements OnClickListener {

    private static final long ANIMATION_DURATION_DOWNSCALE = 500, ANIMATION_DURATION_UPSCALE = 500;
    private final int MAX_INTERESTS = 5;
    private final int MIN_INTERESTS = 3;
    private GetStartTimeInterface returnStartTimeOfEditHashtagFragment;
    private Activity aActivity;
    private NextRegistrationFragment nextFragment;
    private FlowLayout interestsContainer;
    private ArrayList<String> interests;
    private UserData userData;
    private TextView tipTextView;
    private OnClickListener deletehashatagListener;
    private AutoCompleteTextView hashtagAutoCompleteTextView;
    private OnHashtagSelected hashtagSelectedListener;
    private boolean helpTrayVisible = false;
    private View hashtag_sub_container, hashtag_container;
    private Context aContext;
    private ValueAnimator upAnimator, downAnimator;
    private View random_hashtag_layout;
    private ArrayList<HashtagModal> randomHashTags;
    private CompoundButton.OnCheckedChangeListener onRandomHashtagCheckChanged;
    private ImageView helpIcon;
    private boolean actiontaken;
    private Animation mShakeLeftToRightAnimation;
    private FlowLayout randomInterestContainer;
    private Handler startAnimationHandler, stopAnimationHandler;
    private Runnable startAnimationRunnable, stopAnimationRunnable;
    private HashtagAutoCompleteAdapter hashtagAutoCompleteAdapter;


    public void hideHelpTray() {
        if (helpTrayVisible) {
            helpTrayVisible = false;
            random_hashtag_layout.setVisibility(View.GONE);
        }
    }

    public boolean isHelpTrayVisible() {
        return helpTrayVisible;
    }

    public void setHelpTrayVisible(boolean helpTrayVisible) {
        this.helpTrayVisible = helpTrayVisible;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            returnStartTimeOfEditHashtagFragment = (GetStartTimeInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement ReturnStartTimeOfEditHashtagFragment ");
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userData = new UserData();
        nextFragment = new NextRegistrationFragment();
        Bundle data = getArguments();
        if (data.getSerializable("userData") != null) {
            userData = (UserData) data.getSerializable("userData");
            interests = userData.getInterests();

        }
        if (interests == null) {
            interests = new ArrayList<>();
        }
        RandomHashtagGenerator randomHashtagGenerator = new RandomHashtagGenerator();
        randomHashtagGenerator.setUserHashtags(interests);

        randomHashTags = new ArrayList<>();
        ArrayList<String> ranHashtags = randomHashtagGenerator.getRandomHashtags();
        for (int i = 0; i < ranHashtags.size(); i++) {
            HashtagModal hashtagModal = new HashtagModal();
            hashtagModal.setSelected(false);
            hashtagModal.setName(ranHashtags.get(i));
            randomHashTags.add(hashtagModal);
        }
        aActivity = getActivity();
        aContext = getActivity();

    }

    private void parseRandomHastags() {
        randomInterestContainer.removeAllViews();
        for (HashtagModal hashtagModal : randomHashTags)
            addNewRandomInterest(hashtagModal);
    }

    private void addNewRandomInterest(HashtagModal hashtagModal) {
        if (hashtagModal != null) {
            CheckBox v = (CheckBox) LayoutInflater.from(aContext).inflate(R.layout.random_hashtag_item,
                    randomInterestContainer, false);
            v.setChecked(hashtagModal.isSelected());
            v.setText(hashtagModal.getName());
            v.setTag(hashtagModal);
            v.setOnCheckedChangeListener(onRandomHashtagCheckChanged);
            randomInterestContainer.addView(v);
        }
    }

    private void deleteInterest() {
        interestsContainer.removeAllViews();
        addInterestToContainer();
        validateInterestData(true);
        if (interests.size() == MAX_INTERESTS - 1) {
            downAnimator.start();

        }

        for (HashtagModal hashtagModal : randomHashTags) {
            if (!interests.contains(hashtagModal.getName()))
                hashtagModal.setSelected(false);
            else
                hashtagModal.setSelected(true);
        }
        parseRandomHastags();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle paramBundle) {
        View editHashtagsView = inflater.inflate(
                R.layout.edit_profile_interests_new, container, false);
        Button continueButton = (Button) editHashtagsView.findViewById(R.id.continue_interests_new);
        continueButton.setVisibility(View.INVISIBLE);

        mShakeLeftToRightAnimation = AnimationUtils.loadAnimation(aContext, R.anim.rotate_left_to_right);

        interestsContainer = (FlowLayout) editHashtagsView.findViewById(R.id.interests_container);
        hashtag_sub_container = editHashtagsView.findViewById(R.id.hashtag_sub_container);
        hashtag_sub_container.setOnClickListener(null);
        hashtag_container = editHashtagsView.findViewById(R.id.hashtag_container);
        randomInterestContainer = (FlowLayout) editHashtagsView.findViewById(R.id.random_hashtag_container);
        random_hashtag_layout = editHashtagsView.findViewById(R.id.random_hashtag_layout);

        final RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) hashtag_sub_container.getLayoutParams();
        upAnimator = ValueAnimator.ofInt(UiUtils.dpToPx(40), UiUtils.dpToPx(0));
        upAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                params.topMargin = (Integer) valueAnimator.getAnimatedValue();
                hashtag_sub_container.requestLayout();

            }
        });
        downAnimator = ValueAnimator.ofInt(UiUtils.dpToPx(0), UiUtils.dpToPx(40));
        downAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                params.topMargin = (Integer) valueAnimator.getAnimatedValue();
                hashtag_sub_container.requestLayout();

            }
        });

        upAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                hashtag_container.setVisibility(View.INVISIBLE);

            }

            @Override
            public void onAnimationEnd(Animator animator) {

                AlertsHandler.showMessage(aActivity, getActivity().getResources().getString(R.string.five_hashtags), false);

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        downAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                hashtag_container.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationCancel(Animator animator) {
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
            }
        });
        upAnimator.setDuration(ANIMATION_DURATION_UPSCALE);
        downAnimator.setDuration(ANIMATION_DURATION_UPSCALE);
        helpIcon = (ImageView) editHashtagsView
                .findViewById(R.id.hashtag_help_icon);
        helpIcon.setOnClickListener(this);

        deletehashatagListener = new OnClickListener() {
            @Override
            public void onClick(View view) {
                interests.remove((int) view.getTag());
                deleteInterest();


            }
        }
        ;
        hashtagAutoCompleteTextView = (AutoCompleteTextView) editHashtagsView
                .findViewById(R.id.custom_hashtag);
        hashtagAutoCompleteTextView.setDropDownHorizontalOffset(UiUtils
                .dpToPx(-1));
        hashtagAutoCompleteTextView.setDropDownVerticalOffset(UiUtils
                .dpToPx(11));


        hashtagSelectedListener = new OnHashtagSelected() {

            @Override
            public void onHashtagSelected(String s) {

                if (s == null || s.trim().length() == 0) {
                    hashtagAutoCompleteTextView.setText("");
                    hashtagAutoCompleteTextView.dismissDropDown();
                } else {

                    String output;
                    if (s.charAt(0) != '#') {
                        output = "#" + s.trim();
                    } else {
                        output = s.trim();
                    }
                    if (!interests.contains(output)) {
                        if (interests.size() < MAX_INTERESTS) {
                            interests.add(output);
                            displayNewInterest(output, interests.size() - 1);
                            TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyEvent.TrulyMadlyActivities.register_basics,
                                    TrulyMadlyEvent.TrulyMadlyEventTypes.hashtag_selected, 0, output, null, true);
                            hashtagAutoCompleteTextView.setText("");
                            validateInterestData(true);
                            if (interests.size() >= MIN_INTERESTS) {
                                UiUtils.hideKeyBoard(aActivity);
                            }
                            if (interests.size() == MAX_INTERESTS) {
                                hideHelpTray();
                                upAnimator.start();
                            }
                        } else {
                            hideHelpTray();
                            //i18n
                            UiUtils.hideKeyBoard(aActivity);
                            AlertsHandler.showMessage(aActivity,
                                    "Sorry. You can only select "
                                            + MAX_INTERESTS + " hastags.");
                        }
                    } else {
                        hideHelpTray();
                        hashtagAutoCompleteTextView.setText("");
                        UiUtils.hideKeyBoard(aActivity);
                        AlertsHandler.showMessage(aActivity,
                                "Already selected");
                    }

                }

            }
        };

        hashtagAutoCompleteTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                actiontaken = true;
                helpIcon.setVisibility(charSequence.length() == 0 ? View.VISIBLE : View.GONE);
                hideHelpTray();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        hashtagAutoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                hashtagSelectedListener.onHashtagSelected(parent.getAdapter().getItem(position).toString());
            }
        });

        hashtagAutoCompleteAdapter = new HashtagAutoCompleteAdapter(
                aActivity, R.layout.hashtag_autocomplete_item,
                R.id.hashtag_autocomplete_item, hashtagSelectedListener);

        hashtagAutoCompleteTextView.setThreshold(1);
        hashtagAutoCompleteTextView.setAdapter(hashtagAutoCompleteAdapter);
        hashtagAutoCompleteTextView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                hideHelpTray();
            }
        });


        hashtagAutoCompleteTextView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                actiontaken = true;
            }
        });


        hashtagAutoCompleteTextView.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_ENTER || keyCode == KeyEvent.KEYCODE_SPACE) {
                        if (Utility.isSet(hashtagAutoCompleteTextView.getText().toString()) && hashtagAutoCompleteAdapter.getCount() > 0) {
                            hashtagSelectedListener.onHashtagSelected(hashtagAutoCompleteAdapter.getItem(0));
                        } else {
                            hashtagSelectedListener.onHashtagSelected(null);
                        }
                        return true;
                    } else {
                        return false;
                    }
                }
                return false;
            }
        });


        hashtagAutoCompleteTextView.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_ENTER || keyCode == KeyEvent.KEYCODE_SPACE) {
                        if (Utility.isSet(hashtagAutoCompleteTextView.getText().toString()) && hashtagAutoCompleteAdapter.getCount() > 0) {
                            hashtagSelectedListener.onHashtagSelected(hashtagAutoCompleteAdapter.getItem(0));
                        } else {
                            hashtagSelectedListener.onHashtagSelected(null);
                        }
                        return true;
                    } else if (keyCode == KeyEvent.KEYCODE_BACK) {
                        userData.setInterests(interests);
                        BackPressedRegistrationFlow backPressed = new BackPressedRegistrationFlow();
                        backPressed.setUserData(userData);
                        Utility.fireBusEvent(aActivity, true, backPressed);
                        return true;
                    }
                }
                return false;

            }
        });

        onRandomHashtagCheckChanged = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                HashtagModal hashtagModal = (HashtagModal) compoundButton.getTag();
                if (isChecked) {
                    hashtagSelectedListener.onHashtagSelected(hashtagModal.getName());

                } else {
                    int index = interests.indexOf(hashtagModal.getName());
                    if (index != -1) {
                        interests.remove(index);
                        deleteInterest();
                    }
                }
                hideHelpTray();
            }
        };

        addInterestToContainer();
        parseRandomHastags();
        returnStartTimeOfEditHashtagFragment.setStartTime((new java.util.Date()).getTime());

        if (interests.size() == MAX_INTERESTS) {
            hashtag_container.setVisibility(View.GONE);
            params.topMargin = 0;
            hashtag_sub_container.requestLayout();
            AlertsHandler.showMessage(aActivity, getActivity().getResources().getString(R.string.five_hashtags));

        }
        validateInterestData(true);
        actiontaken = false;


        startAnimationHandler = new Handler();
        stopAnimationHandler = new Handler();
        startAnimationRunnable = new Runnable() {
            @Override
            public void run() {
                if (!actiontaken) {
                    startShaking();
                }
            }
        };
        stopAnimationRunnable = new Runnable() {
            public void run() {
                if (mShakeLeftToRightAnimation != null) {
                    helpIcon.clearAnimation();
                }
            }
        };
        startAnimationHandler.postDelayed(startAnimationRunnable, 3000);
        stopAnimationHandler.postDelayed(stopAnimationRunnable, 6000);
        return editHashtagsView;

    }

    @Override
    public void onDestroyView() {
        startAnimationHandler.removeCallbacks(startAnimationRunnable);
        stopAnimationHandler.removeCallbacks(stopAnimationRunnable);
        super.onDestroyView();
    }

    private void startShaking() {
        helpIcon.startAnimation(mShakeLeftToRightAnimation);
    }

    private void addInterestToContainer() {
        if (interests == null)
            return;

        int tag = 0;
        for (String interest : interests) {
            displayNewInterest(interest, tag);
            tag++;
        }

    }

    private void displayNewInterest(String interest, int tag) {
        if (interest != null) {
            LinearLayout interestItemContainer = (LinearLayout) LayoutInflater.from(aActivity).inflate(R.layout.interest_item,
                    interestsContainer, false);
            TextView interestTextView = (TextView) interestItemContainer.findViewById(R.id.interest_item_id);
            interestTextView.setText(interest);
            interestItemContainer.setTag(tag);
            interestItemContainer.setOnClickListener(deletehashatagListener);
            interestsContainer.addView(interestItemContainer);
        }
    }

    protected void onCheckChanged(CompoundButton buttonView, boolean isChecked) {
        String newInterest = buttonView.getText().toString();

        if (isChecked) {
            if (!interests.contains(newInterest)) {
                if (interests.size() == MAX_INTERESTS) {
                    //i18n
                    AlertsHandler.showMessage(aActivity, "You've picked "
                            + MAX_INTERESTS
                            + " already! Deselect to add others."

                    );
                    buttonView.setChecked(false);
                } else {
                    interests.add(newInterest);
                }
            }
        } else {
            interests.remove(newInterest);
        }

        validateInterestData(true);
    }

    private Boolean validateInterestData(boolean showError) {
        Boolean isValid = true;
        String errorMessage = "";
        nextFragment.setIsValid(interests != null
                && interests.size() <= MAX_INTERESTS
                && interests.size() >= MIN_INTERESTS);
        userData.setInterests(interests);
        nextFragment.setUserData(userData);
        if (interests.size() >= 1 && interests.size() < MIN_INTERESTS) {
            isValid = false;
            //i18n
            errorMessage = "Please enter at least " + MIN_INTERESTS + " #tags";
            nextFragment.setNextButtonText(aActivity.getResources().getString(R.string.hashtag_button_state_1));
        } else {
            nextFragment.setNextButtonText(aActivity.getResources().getString(R.string.next));
        }
        Utility.fireBusEvent(aActivity, true, nextFragment);
        if (!isValid && showError) {
            AlertsHandler.showMessage(getActivity(), errorMessage);
        }
        return isValid;
    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.hashtag_help_icon:
                actiontaken = true;
                if (mShakeLeftToRightAnimation != null)
                    helpIcon.clearAnimation();
                random_hashtag_layout.setVisibility(helpTrayVisible ? View.GONE : View.VISIBLE);
                helpTrayVisible = !helpTrayVisible;
                if (helpTrayVisible)
                    UiUtils.hideKeyBoard(aContext);
                break;


        }

    }


}
