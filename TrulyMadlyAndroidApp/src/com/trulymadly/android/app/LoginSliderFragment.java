/**
 *
 */
package com.trulymadly.android.app;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources.NotFoundException;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.Html;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.bus.BusProvider;
import com.trulymadly.android.app.bus.ToggleView;
import com.trulymadly.android.app.bus.ToggleViewOnBack;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.listener.AlertDialogInterface;
import com.trulymadly.android.app.utility.ABHandler;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * @author udbhav
 */
public class LoginSliderFragment extends android.support.v4.app.Fragment
        implements OnClickListener {

    private static final int NUM_PAGES = 4;
    private final View[] headerBulletFullView = new View[NUM_PAGES];
    @BindView(R.id.login_viewpager)
    ViewPager mPager;
    @BindView(R.id.home_login_link)
    TextView home_login_link;
    @BindView(R.id.home_new_user_link)
    TextView home_new_user_link;
    @BindView(R.id.information_view_background)
    View information_view_background;
    @BindView(R.id.information_layout)
    View informationView;
    @BindView(R.id.trust_us_text)
    TextView trust_us_text;
    @BindView(R.id.header_progress_bar)
    View header_progress_bar;
    @BindView(R.id.email_icon)
    ImageView email_icon;
    OnLoginSliderFragmentClick mCallback;
    @BindView(R.id.login_slider_container_child)
    RelativeLayout login_slider_container_child;
    private LoginSliderPage mPagerAdapter;
    private boolean isInformationViewVisible;
    private Unbinder unbinder;
    private EditText suffix_dev_edittext;
    private Context aContext;
    private Activity aActivity;

    public LoginSliderFragment() {
    }

    @Subscribe
    public void onBackButtonPressed(ToggleViewOnBack toggleView) {
        toggleInformationView(toggleView.isToShow(), toggleView.isToTrack());
    }

    @OnClick(R.id.information_view_background)
    public void closeInformationView() {
        toggleInformationView(false, true);
    }

    private void toggleInformationView(boolean toShow, boolean toTrack) {
        if (toTrack) {
            TrulyMadlyTrackEvent.trackEvent(getActivity(),
                    TrulyMadlyActivities.login,
                    TrulyMadlyEventTypes.login_privacy_view, 0, toShow ? "shown" : "hidden", null,
                    true);
        }
        informationView.setVisibility(toShow ? View.VISIBLE : View.GONE);
        information_view_background.setVisibility(toShow ? View.VISIBLE : View.GONE);
        trust_us_text.setVisibility(toShow ? View.INVISIBLE : View.VISIBLE);
        header_progress_bar.setVisibility(toShow ? View.INVISIBLE : View.VISIBLE);
        //mainView.setVisibility(toShow ? View.GONE : View.VISIBLE);
        Utility.fireBusEvent(getActivity(), true, new ToggleView(toShow));
        //mPagerAdapter.loadBackgroundImage((ImageView) mPager.findViewWithTag("position_" + mPager.getCurrentItem()), mPager.getCurrentItem());
        showEmailLink(toShow);
    }

    private void showEmailLink(boolean isInformationViewShown) {
        int imageResId;
        if (ABHandler.getABValue(getActivity(), ABHandler.ABTYPE.AB_EMAIL_SIGNUP_DISABLED)) {
            imageResId = isInformationViewShown ? R.drawable.login_via_email : R.drawable.login_via_email;
        } else {
            imageResId = isInformationViewShown ? R.drawable.email_login_new : R.drawable.email_login_new;
        }
        Picasso.with(getActivity()).load(imageResId).noFade().config(Bitmap.Config.RGB_565).into(email_icon);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = null;
        try {
            v = inflater.inflate(R.layout.login_slider_container, container,
                    false);
            unbinder = ButterKnife.bind(this, v);

        } catch (InflateException e) {
            getActivity().finish();
            return v;
        } catch (NotFoundException e) {
            getActivity().finish();
            return v;
        }
        aActivity = getActivity();
        aContext = getContext();
        boolean isAccountDeleted = SPHandler.getBool(getActivity(), ConstantsSP.SHARED_KEYS_ACCOUNT_DELETED);

        // home_login_link.setText(Html.fromHtml("<u>"+getActivity().getResources().getString(R.string.member_login)+"</u>"));

        home_new_user_link.setText(Html.fromHtml("<u>"
                + getActivity().getResources().getString(R.string.new_user)
                + "</u>"));

        // Instantiate a ViewPager and a PagerAdapter.
        LoginSliderPage mPagerAdapter = new LoginSliderPage(getActivity()
                .getApplicationContext());
        mPager.setAdapter(mPagerAdapter);
        mPager.addOnPageChangeListener(new OnPageChangeListener() {

            @Override
            public void onPageSelected(int pos) {
                showHeaderBullet(pos);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int arg0) {

            }
        });
        mPager.setCurrentItem(0);

        headerBulletFullView[0] = ButterKnife.findById(v, R.id.header_progress_bar_circle_full_1);
        headerBulletFullView[1] = ButterKnife.findById(v, R.id.header_progress_bar_circle_full_2);
        headerBulletFullView[2] = ButterKnife.findById(v, R.id.header_progress_bar_circle_full_3);
        headerBulletFullView[3] = ButterKnife.findById(v, R.id.header_progress_bar_circle_full_4);
        //headerBulletFullView[4] = findById(v, R.id.header_progress_bar_circle_full_5);

        showHeaderBullet(0);
        if (isAccountDeleted) {
            AlertsHandler.showAlertDialog(getActivity(), R.string.homescreen_message_after_delete,
                    new AlertDialogInterface() {
                        @Override
                        public void onButtonSelected() {
                            SPHandler.remove(getActivity(),
                                    ConstantsSP.SHARED_KEYS_ACCOUNT_DELETED);
                        }
                    });
        }
        showEmailLink(false);

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showDevSuffix();
    }

    private void showDevSuffix() {
        LayoutInflater inflater = (LayoutInflater) aContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (Constants.isDev) {
            View v = inflater.inflate(R.layout.suffix_dev_input_container, login_slider_container_child,
                    false);
            suffix_dev_edittext = ButterKnife.findById(v, R.id.suffix_dev_edittext);
            suffix_dev_edittext.setText(Constants.isDev ? ConstantsUrls.setSUFFIX_DEV(aContext, null) : ConstantsUrls.setSUFFIX_T1(aContext, null));
            OnClickListener onSuffixDevSaveClicked = new OnClickListener() {
                @Override
                public void onClick(View v) {
                    UiUtils.hideKeyBoard(aContext);
                    String suffixDev = suffix_dev_edittext.getText().toString();
                    if (Constants.isDev && !Utility.isSet(suffixDev)) {
                        AlertsHandler.showMessage(aActivity, "Please enter valid Suffix");
                    } else {
                        String suffixSet = Constants.isDev ? ConstantsUrls.setSUFFIX_DEV(aContext, suffixDev) : ConstantsUrls.setSUFFIX_T1(aContext, suffixDev);
                        suffix_dev_edittext.setText(suffixSet);
                        AlertsHandler.showMessage(aActivity, "Suffix setString to " + suffixSet);
                    }
                }
            };
            ButterKnife.findById(v, R.id.suffix_dev_save_btn).setOnClickListener(onSuffixDevSaveClicked);
            login_slider_container_child.addView(v);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.trust_us_text)
    protected void launchInfoScreen() {
        toggleInformationView(true, true);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception.
        try {
            mCallback = (OnLoginSliderFragmentClick) activity;
        } catch (ClassCastException e) {
            Crashlytics.logException(e);
            throw new ClassCastException(activity.toString()
                    + " must implement OnLoginSliderFragmentClick");
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        BusProvider.getInstance().register(this);

    }

    @Override
    public void onStop() {
        super.onStop();
        BusProvider.getInstance().unregister(this);
    }

    @Override
    @OnClick({R.id.home_login_container, R.id.email_icon, R.id.home_new_user_link, R.id.register_fb_id, R.id.fb_register_icon})
    public void onClick(View v) {
        mCallback.onLoginSliderFragmentClick(v);
    }

    private void showHeaderBullet(int pos) {
        TrulyMadlyTrackEvent.trackEvent(getActivity(),
                TrulyMadlyActivities.login, "login_slider_screen_" + (pos + 1), 0, null, null,
                true);


        if (pos > 0) {
            // verify_tv.setTextColor(getResources().getColor(R.color.black));
            // home_login_link.setTextColor(getResources().getColor(R.color.black));
            // trust_us_text.setTextColor(getResources().getColor(R.color.black));
            // informationButton.setImageResource(R.drawable.infobutton_black);
        } else {
            // verify_tv.setTextColor(getResources().getColor(R.color.white));
            // home_login_link.setTextColor(getResources().getColor(R.color.white));
            // trust_us_text.setTextColor(getResources().getColor(R.color.white));
            // informationButton.setImageResource(R.drawable.infobutton_white);
        }
        for (int i = 0; i < headerBulletFullView.length; i++) {
            if (i == pos) {
                headerBulletFullView[i].setVisibility(View.VISIBLE);
            } else {
                headerBulletFullView[i].setVisibility(View.GONE);
            }
        }
    }


    public interface OnLoginSliderFragmentClick {
        void onLoginSliderFragmentClick(View v);
    }

    public class LoginSliderPage extends PagerAdapter {

        final LayoutInflater inflater;
        private final Context context;

        public LoginSliderPage(Context context) {
            this.context = context;
            inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View v = null;
            try {
                v = inflater.inflate(R.layout.login_slide_page, container,
                        false);
            } catch (NotFoundException e) {
                return v;
            } catch (InflateException e) {
                return v;
            }

            ButterKnife.findById(v, R.id.page_0).setVisibility(View.VISIBLE);
            ImageView imageView = ButterKnife.findById(v, R.id.page_0_imageview);
            imageView.setTag("position_" + position);
            loadBackgroundImage(imageView, position);

            container.addView(v);

            return v;
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }


        private void loadBackgroundImage(ImageView imageView, int position) {
            int imageDrawableId = getImageDrawableId(position);
            Picasso picasso = Picasso.with(getActivity());
            RequestCreator request = picasso.load(imageDrawableId);
            request.config(Bitmap.Config.RGB_565).into(imageView);
        }

        private int getImageDrawableId(int position) {

            switch (position) {
                case 1:
                    return R.drawable.splashsecondscreen;
                case 2:
                    return R.drawable.splashthirdscreen;
                case 3:
                    return R.drawable.splashfourthscreen;
                case 0:
                default:
                    return R.drawable.splashfirstscreen;
            }
        }

    }

}
