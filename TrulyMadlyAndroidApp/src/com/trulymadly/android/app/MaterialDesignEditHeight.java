package com.trulymadly.android.app;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.trulymadly.android.app.bus.ExpandFragmentEvent;
import com.trulymadly.android.app.bus.NextRegistrationFragment;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.ConstantsCities;
import com.trulymadly.android.app.listener.GetStartTimeInterface;
import com.trulymadly.android.app.listener.OnHashtagSelected;
import com.trulymadly.android.app.modal.Height;
import com.trulymadly.android.app.modal.UserData;
import com.trulymadly.android.app.utility.CityUtils;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemSelected;
import butterknife.Unbinder;

public class MaterialDesignEditHeight extends Fragment {

    @BindView(R.id.material_design_height_spinner)
    Spinner heightSpinner;
    @BindView(R.id.new_city_list_container)
    View city_list_container;
    @BindView(R.id.city_autocomplete_tv)
    AutoCompleteTextView cityAutocompleteTv;
    @BindView(R.id.popular_cities_layout)
    LinearLayout popular_cities_layout;
    @BindView(R.id.popular_cities_container)
    LinearLayout popular_cities_container;
    @BindView(R.id.height_header_tv)
    View height_header_tv;
    @BindView(R.id.height_spinner_container)
    View height_spinner_container;
    private String stateId = null, cityId = null,
            height = null, cityName = null;
    private Activity aActivity;
    private NextRegistrationFragment nextFragment;
    private GetStartTimeInterface returnStartTimeOfEditHeightFragment;
    private UserData userData;
    private boolean isCitySet = false;
    private OnHashtagSelected citySelectedListener;
    private boolean popularCitiesVisible;
    private Unbinder unbinder;
    private boolean isCityViewExpanded = false;
    private ArrayAdapter cityAutoCompleteAdapterNew;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            returnStartTimeOfEditHeightFragment = (GetStartTimeInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement ReturnStartTimeOfEditHeightFragment ");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        aActivity = getActivity();
        Bundle data = getArguments();

        isCitySet = data.getBoolean("isCitySet");
        // isFbConnected= data.getBoolean("isFbConnected");
        userData = new UserData();
        if (data.getSerializable("userData") != null) {
            userData = (UserData) data.getSerializable("userData");
        }
        if (userData != null) {
            height = userData.getHeight();
            stateId = userData.getStateId();
            cityId = userData.getCityId();
            cityName = userData.getCityName();

        }
        nextFragment = new NextRegistrationFragment();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle paramBundle) {
        View editHeightView = inflater.inflate(R.layout.material_design_edit_height,
                container, false);
        unbinder = ButterKnife.bind(this, editHeightView);

        if (!isCitySet) {
            city_list_container.setVisibility(View.VISIBLE);
            cityAutocompleteTv.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    expandCityView();
                    return false;
                }
            });

            citySelectedListener = new OnHashtagSelected() {
                @Override
                public void onHashtagSelected(String s) {

                    if (s == null || s.trim().length() == 0) {
                        cityAutocompleteTv.setText("");
                        cityAutocompleteTv.dismissDropDown();
                    } else {
                        popularCitiesVisible = CityUtils.hidePopularCities(popularCitiesVisible, popular_cities_container);
                        cityAutocompleteTv.setText(s);
                        cityAutocompleteTv.setSelection(s.length());
                        cityAutocompleteTv.dismissDropDown();
                        cityName = s;
                        HashMap<String, String> map = CityUtils.getCityAndStateId(cityAutocompleteTv.getText().toString());
                        cityId = map.get("cityId");
                        stateId = map.get("stateId");
                        closeExpandedCityView();
                    }

                }
            };

            cityAutocompleteTv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    citySelectedListener.onHashtagSelected(parent.getAdapter().getItem(position).toString());
                }
            });

            cityAutoCompleteAdapterNew = new ArrayAdapter<>(aActivity, R.layout.city_suggestion_item, R.id.city_suggestion_item_tv, ConstantsCities.cityNames);
            cityAutocompleteTv.setAdapter(cityAutoCompleteAdapterNew);
            cityAutocompleteTv.setThreshold(1);

            if (Utility.isSet(cityName)) {
                cityAutocompleteTv.setText(cityName);
                cityAutocompleteTv.setSelection(cityAutocompleteTv.getText().length());
            }
            cityAutocompleteTv.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        if (keyCode == KeyEvent.KEYCODE_ENTER) {
                            if (Utility.isSet(cityAutocompleteTv.getText().toString()) && cityAutoCompleteAdapterNew.getCount() > 0) {
                                citySelectedListener.onHashtagSelected(cityAutoCompleteAdapterNew.getItem(0).toString());
                            } else {
                                citySelectedListener.onHashtagSelected(null);
                            }
                            return true;
                        } else {
                            if (keyCode != KeyEvent.KEYCODE_BACK) {
                                expandCityView();
                            }
                            return false;
                        }
                    }
                    return false;
                }
            });
            cityAutocompleteTv.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (charSequence.length() == 0) {
                        popularCitiesVisible = CityUtils.showPopularCities(popularCitiesVisible, isCityViewExpanded, aActivity, popular_cities_container, cityAutocompleteTv);
                    } else {
                        popularCitiesVisible = CityUtils.hidePopularCities(popularCitiesVisible, popular_cities_container);
                    }

                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
            cityAutocompleteTv.setDropDownHorizontalOffset(UiUtils.dpToPx(-70));
            cityAutocompleteTv.setDropDownVerticalOffset(UiUtils.dpToPx(32));
            cityAutocompleteTv.setDropDownWidth((int) UiUtils.getScreenWidth(aActivity));

            View.OnClickListener cityClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    citySelectedListener.onHashtagSelected(view.getTag().toString());
                }
            };
            popular_cities_layout.removeAllViews();
            for (String city : ConstantsCities.popularCityNames) {
                View cityView;
                cityView = inflater.inflate(R.layout.city_suggestion_item, null);
                ((TextView) cityView.findViewById(R.id.city_suggestion_item_tv)).setText(city);
                popular_cities_layout.addView(cityView);
                cityView.setTag(city);
                cityView.setOnClickListener(cityClickListener);
            }
        } else {
            city_list_container.setVisibility(View.GONE);
        }
        View.OnTouchListener onHeightTouchListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                popularCitiesVisible = CityUtils.hidePopularCities(popularCitiesVisible, popular_cities_container);
                if (cityAutocompleteTv != null)
                    cityAutocompleteTv.dismissDropDown();
                return false;
            }
        };
        CityUtils.createHeightSpinner(Height.getHeightArray(getResources()), aActivity, heightSpinner, height, onHeightTouchListener);

        returnStartTimeOfEditHeightFragment.setStartTime((new java.util.Date())
                .getTime());
        return editHeightView;
    }

    private void expandCityView() {
        if (!isCityViewExpanded) {
            isCityViewExpanded = true;
            Utility.fireBusEvent(aActivity, true, new ExpandFragmentEvent(true));
            height_header_tv.setVisibility(View.GONE);
            height_spinner_container.setVisibility(View.GONE);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    popularCitiesVisible = CityUtils.showPopularCities(popularCitiesVisible, isCityViewExpanded, aActivity, popular_cities_container, cityAutocompleteTv);
                }
            }, 500);

        }
    }

    private void closeExpandedCityView() {
        if (isCityViewExpanded) {
            isCityViewExpanded = false;
            HashMap<String, String> map = CityUtils.getCityAndStateId(cityAutocompleteTv.getText().toString());
            cityId = map.get("cityId");
            stateId = map.get("stateId");
            validateHeightData();
            Utility.fireBusEvent(aActivity, true, new ExpandFragmentEvent(false));
            height_header_tv.setVisibility(View.VISIBLE);
            height_spinner_container.setVisibility(View.VISIBLE);
            popularCitiesVisible = CityUtils.hidePopularCities(popularCitiesVisible, popular_cities_container);
            cityAutocompleteTv.dismissDropDown();
            UiUtils.hideKeyBoard(aActivity);
            cityAutocompleteTv.setHint(R.string.city);
        }
    }


    @OnItemSelected(R.id.material_design_height_spinner)
    void onHeightSelected() {
        popularCitiesVisible = CityUtils.hidePopularCities(popularCitiesVisible, popular_cities_container);
        height = ((Height) heightSpinner.getSelectedItem()).getId();
        validateHeightData();
    }

    @OnItemSelected(value = R.id.material_design_height_spinner, callback = OnItemSelected.Callback.NOTHING_SELECTED)
    void onNothingSelected() {
        popularCitiesVisible = CityUtils.hidePopularCities(popularCitiesVisible, popular_cities_container);
    }


    private void validateHeightData() {
        Boolean isValid = true;
        if (!isCitySet) {
            if (stateId == null || stateId.equalsIgnoreCase("-1")
                    || stateId.equalsIgnoreCase("0")) {
                isValid = false;
            } else if (cityId == null || cityId.equalsIgnoreCase("-1")
                    || cityId.equalsIgnoreCase("0")) {
                isValid = false;
            }
        }
        if (height == null || height.equals("-1")
                || height.equalsIgnoreCase("Height")) {
            isValid = false;
        }

        nextFragment.setIsValid(isValid);
        String height_inches, height_feet;
        try {
            height_feet = "" + ((Integer.parseInt(height) + 48) / 12);
            height_inches = "" + ((Integer.parseInt(height) + 48) % 12);
            userData.setHeight_feet(height_feet);
            userData.setHeight_inches(height_inches);
        } catch (NumberFormatException ignored) {

        }
        userData.setCountryId(String.valueOf(Constants.COUNTRY_ID_INDIA));
        userData.setHeight(height);
        userData.setCityId(cityId);
        userData.setStateId(stateId);
        userData.setCityName(cityName);
        nextFragment.setUserData(userData);
        Utility.fireBusEvent(aActivity, true, nextFragment);

    }

    public void onBackPressed() {
        if (isCityViewExpanded) {
            closeExpandedCityView();
        }

    }

    public boolean isCityViewExpanded() {
        return isCityViewExpanded;
    }
}