package com.trulymadly.android.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;

import com.moe.pushlibrary.MoEHelper;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.listener.OnActionBarClickedInterface;
import com.trulymadly.android.app.utility.ActionBarHandler;
import com.trulymadly.android.app.utility.Utility;
import com.trulymadly.android.app.utility.WebviewHandler;

public class WebViewUrlActivity extends AppCompatActivity {

	public static final String PARAM_TITLE = "title";
	public static final String PARAM_URL = "url";
	public static final String PARAM_JS_ENABLED = "js_enabled";
	public static final String PARAM_CROSS_ENABLED = "cross_enabled";
	public static final String PARAM_HEADERS_REQUIRED = "headers_required";

	private ActionBarHandler actionBarHandler = null;
	private MoEHelper mhelper;

	private WebviewHandler mWebviewHandler;
	private WebviewHandler.WebviewActionsListener mWebviewActionsListener;
	private boolean isJSEnabled, isHeaderParamsRequired, isCrossEnabled;
	private String mTitle;
	private String mUrl;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		mhelper = new MoEHelper(this);
        try {
            setContentView(R.layout.terms_condition_layout);
        } catch (OutOfMemoryError e) {
            System.gc();
            finish();
            return;
        }
        if (savedInstanceState != null) {
			return;
		}

		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		//Webview related
		WebviewHandler.WebviewActionsListener mWebviewActionsListener = new WebviewHandler.WebviewActionsListener() {
			@Override
			public boolean shouldOverrideUrlLoading(String url) {
				mWebviewHandler.loadUrl(url);
				return true;
			}

			@Override
			public void webViewHiddenOnUrlLoad() {

			}

			@Override
			public void onWebViewCloseClicked() {

			}
		};

		parseIntent(getIntent());
		View mWebviewIncludeView = findViewById(R.id.webview_include_view);
		mWebviewHandler = new WebviewHandler(mWebviewIncludeView, mWebviewActionsListener, isJSEnabled, isCrossEnabled);
		loadDataFromIntent();
	}

	private void parseIntent(Intent intent){
		mTitle = "TERMS OF USE";
		mUrl = ConstantsUrls.get_terms_url();
		Bundle b = intent.getExtras();
		if (b != null) {
			mTitle = b.getString(PARAM_TITLE);
			mUrl = b.getString(PARAM_URL);
			isJSEnabled = b.getBoolean(PARAM_JS_ENABLED);
			isHeaderParamsRequired = b.getBoolean(PARAM_HEADERS_REQUIRED);
			isCrossEnabled = b.getBoolean(PARAM_CROSS_ENABLED);
		}
	}

	@SuppressWarnings("deprecation")
	private void loadDataFromIntent() {
//		String title = "TERMS OF USE", url = ConstantsUrls.get_terms_url();

		Context aContext = this;

//		Bundle b = intent.getExtras();
//		if (b != null) {
//			title = b.getString("mTitle");
//			url = b.getString("url");
//			isJSEnabled = b.getBoolean(PARAM_JS_ENABLED);
//			isHeaderParamsRequired = b.getBoolean(PARAM_HEADERS_REQUIRED);
//			isCrossEnabled = b.getBoolean(PARAM_CROSS_ENABLED);
//		}
		if (actionBarHandler == null) {
			actionBarHandler = new ActionBarHandler(this, mTitle, null, new OnActionBarClickedInterface() {

				@Override
				public void onUserProfileClicked() {
				}


				@Override
				public void onCuratedDealsClicked() {

				}

				@Override
				public void onConversationsClicked() {
				}

                @Override
                public void onLocationClicked() {
                }

				@Override
				public void onBackClicked() {
					onBackPressed();

				}

                @Override
                public void onTitleClicked() {
				}
				
				@Override
				public void onTitleLongClicked() {	
				}

				@Override
				public void onSearchViewOpened() {

				}

				@Override
				public void onSearchViewClosed() {

				}

				@Override
				public void onCategoriesClicked() {

				}

			}, false, false, false);
		} else {
			actionBarHandler.setTitle(mTitle);
		}

		actionBarHandler.toggleActionBar(Utility.isSet(mTitle));

//		ProgressBar progress = (ProgressBar) findViewById(R.id.termProgressBar);
//		WebView webView = (WebView) findViewById(R.id.termsWebView);
//		if (webView != null) {
//			webView.setWebViewClient(new WebViewClientWithProgressBar(progress));
//			// disabling javascript - might need it for JS based pages later
//			webView.getSettings().setJavaScriptEnabled(false);
//			if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) {
//				webView.clearView();
//			} else {
//				webView.loadUrl("about:blank");
//			}
//			webView.loadUrl(url, GetHttpRequestParams.getHeaders(aContext));
//		}


		mWebviewHandler.loadUrl(mUrl, (isHeaderParamsRequired)?GetHttpRequestParams.getHeaders(aContext):null);
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		parseIntent(intent);
		loadDataFromIntent();
	}

	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public void onStop() {
		super.onStop();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
        super.onPause();
	}
}
