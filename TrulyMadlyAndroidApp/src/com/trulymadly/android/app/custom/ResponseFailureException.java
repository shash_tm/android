package com.trulymadly.android.app.custom;

/**
 * Created by avin on 24/06/16.
 */
public class ResponseFailureException extends Exception {
    public ResponseFailureException() {
    }

    public ResponseFailureException(String detailMessage) {
        super(detailMessage);
    }

    public ResponseFailureException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public ResponseFailureException(Throwable throwable) {
        super(throwable);
    }
}
