package com.trulymadly.android.app.custom;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

import com.trulymadly.android.app.R;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by avin on 27/05/16.
 */
public class AnalogTimerView extends View {

    /////////////////////////////////////////
    //Fields
    /////////////////////////////////////////
    private TimerConfig mTimerConfig;
    private Paint mPaint;
    private Timer mTimer;
    private TimerTask mTimerTask;
    public final int DEFAULT_COLOR = Color.BLUE;

    /////////////////////////////////////////
    //Constructors
    /////////////////////////////////////////
    public AnalogTimerView(Context context) {
        super(context);
        init(null);
    }

    public AnalogTimerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public AnalogTimerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public AnalogTimerView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    /*
    * Initializes the TimerConfig object used for all the calculations and initializing the fields
    * */
    private void init(AttributeSet attributeSet) {
        mTimerConfig = new TimerConfig();

        if(attributeSet == null){
            return;
        }

        TypedArray array = getContext().obtainStyledAttributes(attributeSet, R.styleable.AnalogTimerView);

        for (int i = 0; i < array.length(); i++) {
            int type = array.getIndex(i);

            if (type == R.styleable.AnalogTimerView_start_angle) {
                int start_angle = array.getInt(R.styleable.AnalogTimerView_start_angle, 270);
                mTimerConfig.setmStartAngle(start_angle);
            }

            if (type == R.styleable.AnalogTimerView_total_value) {
                int total_value = array.getInt(R.styleable.AnalogTimerView_total_value, 100);
                mTimerConfig.setmTotalValue(total_value);
            }

            if (type == R.styleable.AnalogTimerView_initial_value) {
                int initial_value = array.getInt(R.styleable.AnalogTimerView_initial_value, 0);
                mTimerConfig.setmInitialValue(initial_value);
            }

            if (type == R.styleable.AnalogTimerView_increment) {
                boolean isIncreasing = array.getBoolean(R.styleable.AnalogTimerView_increment, true);
                mTimerConfig.setIncreasing(isIncreasing);
            }

            if (type == R.styleable.AnalogTimerView_interval_ms) {
                int interval = array.getInt(R.styleable.AnalogTimerView_interval_ms, 1000);
                mTimerConfig.setmIntervalInMilliseconds(interval);
            }

            if (type == R.styleable.AnalogTimerView_timer_icon) {
                int drawable_id = array.getResourceId(R.styleable.AnalogTimerView_timer_icon, -1);
                mTimerConfig.setmDrawableId(drawable_id);
            }

            if (type == R.styleable.AnalogTimerView_icon_size) {
                Float drawable_size = array.getDimension(R.styleable.AnalogTimerView_icon_size, 140);
                mTimerConfig.setmIconWidthInPx(drawable_size.intValue());
            }

            if (type == R.styleable.AnalogTimerView_progress_width) {
                Float progress_width = array.getDimension(R.styleable.AnalogTimerView_progress_width, 3);
                mTimerConfig.setmProgressWidthInPx(progress_width.intValue());
            }

            if (type == R.styleable.AnalogTimerView_start_delay_ms) {
                int start_delay = array.getInt(R.styleable.AnalogTimerView_start_delay_ms, 0);
                mTimerConfig.setmStartDelay(start_delay);
            }

//            if (type == R.styleable.AnalogTimerView_progress_color) {
//                int color = array.getColor(R.styleable.AnalogTimerView_progress_color, DEFAULT_COLOR);
//                mTimerConfig.setmProgressColor(color);
//                setPaintColor(color);
//            }

            if (type == R.styleable.AnalogTimerView_start_color) {
                int color = array.getColor(R.styleable.AnalogTimerView_start_color, DEFAULT_COLOR);
                mTimerConfig.setmStartColor(color);
                setPaintColor(color);
            }

            if (type == R.styleable.AnalogTimerView_end_color) {
                int color = array.getColor(R.styleable.AnalogTimerView_end_color, -1);
                mTimerConfig.setmEndColor(color);
            }
        }

        array.recycle();
    }

    private Paint getPaint(){
        if(mPaint == null){
            mPaint = new Paint();
            mPaint.setColor(mTimerConfig.getmStartColor());
            mPaint.setStrokeWidth(mTimerConfig.getmProgressWidthInPx());
            mPaint.setAntiAlias(true);
            mPaint.setStrokeCap(Paint.Cap.SQUARE);
            mPaint.setStyle(Paint.Style.STROKE);
        }

        return mPaint;
    }

    private void setPaintColor(int color){
        if(mPaint != null){
            mPaint.setColor(color);
        }
    }

    private void calculateColor(){
        if(mPaint != null && mTimerConfig != null && mTimerConfig.getmEndColor() != -1){
            setPaintColor(mTimerConfig.calculateNextColor());
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (isInEditMode()) {
            return;
        }
        if (canvas == null) {
            canvas = new Canvas();
        }
        super.onDraw(canvas);
        calculateColor();
        canvas.drawArc(mTimerConfig.getmRectF(), mTimerConfig.getmStartAngle(), mTimerConfig.getSweepAngle(),
                false, getPaint());
        Bitmap bitmap = mTimerConfig.getmBitmap();
        if(bitmap != null) {
            canvas.drawBitmap(bitmap, null, mTimerConfig.getmIconRectF(), null);
        }
    }

    /**
     * Resets the current value based on which the timer is running
     * Note : It doesn't stops the timer
     *
     * If you want to stop/start the timer as well you can call #resetAndStop or #resetAndStart respectively
     * */
    public void reset(){
        mTimerConfig.reset();
        invalidate();
    }

    /**
     * Resets the current value based on which the timer is running
     * And stops the timer
     * */
    public void resetAndStop(){
        mTimerConfig.reset();
        stop();
        invalidate();
    }

    /**
     * Resets the current value based on which the timer is running
     * And restarts the timer
     * */
    public void resetAndStart(){
        resetAndStop();
        start();
    }

    /**
     * Starts the timer
     * */
    public void start(){
        stop();
        if(mTimer == null){
            mTimer = new Timer();
        }

        if(mTimerTask == null){
            mTimerTask = new TimerTask() {
                @Override
                public void run() {
                    mTimerConfig.moveToNextValue();
                    if(mTimerConfig.isExpired()){
                        mTimer.cancel();
                        return;
                    }
                    AnalogTimerView.this.postInvalidate();
                }
            };
        }
        mTimer.schedule(mTimerTask, mTimerConfig.getmStartDelay(), mTimerConfig.getmIntervalInMilliseconds());
    }

    /**
     * Stops the timer
     * */
    public void stop(){
        if(mTimer != null){
            mTimer.cancel();
            mTimer = null;
        }
    }

    public void reconfigure(int totalValue, int currentValue){
        reset();
        mTimerConfig.setmTotalValue(totalValue);
        mTimerConfig.setmCurrentValue(currentValue);
        invalidate();
    }

    public void setCurrentValue(int currentValue){
        mTimerConfig.setmCurrentValue(currentValue);
        invalidate();
    }

//    /**
//     * @param angleInDegrees : Start angle in degrees
//     * Reconfigures the view by changing the start angle
//     * Note : Also stops and resets the timer
//     * */
//    public void setStartAngle(int angleInDegrees){
//        resetAndStop();
//        mTimerConfig.setmStartAngle(angleInDegrees);
//        invalidate();
//    }

    private class TimerConfig{
        private int mStartAngle = 270;
        private int mSweepAngle = 0;
        private int mTotalValue = 100;
        private int mCurrentValue = 0;
        private int mProgressWidthInDp = 3;
        private int mProgressWidthInPx = 0;
        private int mViewWidthInPx = 0;
        private Paint mPaint;
        private RectF mRectF, mIconRectF;
        private int mDrawableId = -1;
        private Bitmap mBitmap;
        private int mIconX, mIconY;
        private int mIconWidthInPx = 140;
        private int mInitialValue = 0;
        private boolean isIncreasing = true;
        private long mIntervalInMilliseconds = 1000;
        private long mStartDelay = 0;
//        private int mProgressColor = DEFAULT_COLOR;
        private int mStartColor = DEFAULT_COLOR, mEndColor = -1;
        private float mColorStepRed = -1f, mColorStepBlue = -1f, mColorStepGreen = -1f;
        private int mColorFirstRed = -1, mColorFirstBlue = -1, mColorFirstGreen = -1;

//        public int getmProgressColor() {
//            return mProgressColor;
//        }
//
//        public void setmProgressColor(int mProgressColor) {
//            this.mProgressColor = mProgressColor;
//        }

        public int getmStartAngle() {
            return mStartAngle;
        }

        public void setmStartAngle(int mStartAngle) {
            this.mStartAngle = mStartAngle;
        }

        public int getmSweepAngle() {
            return mSweepAngle;
        }

        public void setmSweepAngle(int mSweepAngle) {
            this.mSweepAngle = mSweepAngle;
        }

        public int getmTotalValue() {
            return mTotalValue;
        }

        public void setmTotalValue(int mTotalValue) {
            this.mTotalValue = mTotalValue;
            initializeColorParameters();
        }

        public int getmCurrentValue() {
            return mCurrentValue;
        }

        public void setmCurrentValue(int mCurrentValue) {
            this.mCurrentValue = mCurrentValue;
        }

        public int getmProgressWidthInDp() {
            return mProgressWidthInDp;
        }

        public void setmProgressWidthInDp(int mProgressWidthInDp) {
            this.mProgressWidthInDp = mProgressWidthInDp;
        }

        public int getmProgressWidthInPx() {
            if(mProgressWidthInPx == 0){
                mProgressWidthInPx = (int) ((mProgressWidthInDp * Resources.getSystem().getDisplayMetrics().density) + 0.5f);
            }

            return mProgressWidthInPx;
        }

        public void setmProgressWidthInPx(int mProgressWidthInPx) {
            this.mProgressWidthInPx = mProgressWidthInPx;
        }

        public int getmViewWidthInPx() {
            if(mViewWidthInPx == 0){
                mViewWidthInPx = getRight() - getLeft();
            }

            return mViewWidthInPx;
        }

        public void setmViewWidthInPx(int mViewWidthInPx) {
            this.mViewWidthInPx = mViewWidthInPx;
        }

        public Paint getmPaint() {
            return mPaint;
        }

        public void setmPaint(Paint mPaint) {
            this.mPaint = mPaint;
        }

        public RectF getmRectF() {
            if(mRectF == null){
                mRectF = new RectF();

                int curveWidthInPx = getmProgressWidthInPx();

                int widthInPx = getmViewWidthInPx();
                mRectF.set(curveWidthInPx / 2 + mIconWidthInPx/4, // left
                        curveWidthInPx / 2 + mIconWidthInPx/4, // top
                        widthInPx - curveWidthInPx/2 - mIconWidthInPx/4, // right
                        widthInPx - curveWidthInPx/2 - mIconWidthInPx/4); // bottom
            }

            return mRectF;
        }

        public void setmRectF(RectF mRectF) {
            this.mRectF = mRectF;
        }

        public RectF getmIconRectF() {
            if(mIconRectF == null){
                mIconRectF = new RectF();
            }
            calculateX();
            calculateY();
            mIconRectF.set(mIconX - mIconWidthInPx/2, //Left
                    mIconY - mIconWidthInPx/2, //Top
                    mIconX + mIconWidthInPx/2, //Right
                    mIconY + mIconWidthInPx/2); // Bottom
            return mIconRectF;
        }

        public void setmIconRectF(RectF mIconRectF) {
            this.mIconRectF = mIconRectF;
        }

        public int getmDrawableId() {
            return mDrawableId;
        }

        public void setmDrawableId(int mDrawableId) {
            this.mDrawableId = mDrawableId;
        }

        public Bitmap getmBitmap() {
            if(mBitmap == null && mDrawableId != -1) {
                mBitmap = BitmapFactory.decodeResource(getContext().getResources(), mDrawableId);
            }

            return mBitmap;
        }

        public void setmBitmap(Bitmap mBitmap) {
            this.mBitmap = mBitmap;
        }

        public int getmIconX() {
            return mIconX;
        }

        public void setmIconX(int mIconX) {
            this.mIconX = mIconX;
        }

        public int getmIconY() {
            return mIconY;
        }

        public void setmIconY(int mIconY) {
            this.mIconY = mIconY;
        }

        public int getmIconWidthInPx() {
            return mIconWidthInPx;
        }

        public void setmIconWidthInPx(int mIconWidthInPx) {
            this.mIconWidthInPx = mIconWidthInPx;
        }

        public int getmInitialValue() {
            return mInitialValue;
        }

        public void setmInitialValue(int mInitialValue) {
            this.mInitialValue = mInitialValue;
        }

        public boolean isIncreasing() {
            return isIncreasing;
        }

        public void setIncreasing(boolean increasing) {
            isIncreasing = increasing;
        }

        public long getmIntervalInMilliseconds() {
            return mIntervalInMilliseconds;
        }

        public void setmIntervalInMilliseconds(long mIntervalInMilliseconds) {
            this.mIntervalInMilliseconds = mIntervalInMilliseconds;
        }

        public int getmStartColor() {
            return mStartColor;
        }

        public void setmStartColor(int mStartColor) {
            this.mStartColor = mStartColor;
        }

        public int getmEndColor() {
            return mEndColor;
        }

        public void setmEndColor(int mEndColor) {
            this.mEndColor = mEndColor;
            initializeColorParameters();
        }

        private void initializeColorParameters(){
            if(mEndColor != -1 && mEndColor != mStartColor){
                mColorFirstRed = Color.red(mStartColor);
                mColorFirstBlue = Color.blue(mStartColor);
                mColorFirstGreen = Color.green(mStartColor);

                int mColorSecondRed = Color.red(mEndColor);
                int mColorSecondBlue = Color.blue(mEndColor);
                int mColorSecondGreen = Color.green(mEndColor);

                mColorStepRed = ((float)(mColorSecondRed - mColorFirstRed))/((float)mTotalValue);
                mColorStepBlue = ((float)(mColorSecondBlue - mColorFirstBlue))/((float)mTotalValue);
                mColorStepGreen = ((float)(mColorSecondGreen - mColorFirstGreen))/((float)mTotalValue);
            }
        }

        public int calculateNextColor(){
            return Color.rgb(mColorFirstRed + (int)(mColorStepRed * mCurrentValue),
                    mColorFirstGreen + (int)(mColorStepGreen * mCurrentValue),
                    mColorFirstBlue + (int)(mColorStepBlue * mCurrentValue));
        }

        private int adjustAngleForStartAngle(){
            int adjustAngle = 0;
            if(mStartAngle > 270){
                adjustAngle = mStartAngle - 270;
            }else if(mStartAngle < 270){
                adjustAngle = 90 + mStartAngle;
            }

            return adjustAngle;
        }

        private int calculateX(){
            int sweepAngle = getSweepAngle() + adjustAngleForStartAngle();
            int radius = (mViewWidthInPx/2) - mIconWidthInPx/4;
            mIconX = (int) (mIconWidthInPx/4 + radius * (1 + Math.sin(Math.toRadians(sweepAngle))));

            return mIconX;
        }

        private int calculateY(){
            int sweepAngle = getSweepAngle() + adjustAngleForStartAngle();
            int radius = (mViewWidthInPx/2) - mIconWidthInPx/4;
            mIconY = (int) (mIconWidthInPx/4 + radius * (1 - Math.cos(Math.toRadians(sweepAngle))));

            return mIconY;
        }

        private int getSweepAngle(){
            mSweepAngle = 360*mCurrentValue/mTotalValue;
            return mSweepAngle;
        }

        public void reset(){
            mCurrentValue = mInitialValue;
        }

        private void moveToNextValue(){
            mCurrentValue = mCurrentValue + ((isIncreasing)?1:(-1));
        }

        private boolean isExpired(){
            return (isIncreasing && mCurrentValue == mTotalValue) ||
                    (!isIncreasing && mCurrentValue == 0);
        }

        public long getmStartDelay() {
            return mStartDelay;
        }

        public void setmStartDelay(long mStartDelay) {
            this.mStartDelay = mStartDelay;
        }
    }

}
