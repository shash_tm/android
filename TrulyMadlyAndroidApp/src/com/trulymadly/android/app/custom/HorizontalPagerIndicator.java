package com.trulymadly.android.app.custom;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

import com.trulymadly.android.app.R;
import com.trulymadly.android.app.utility.UiUtils;

/**
 * Created by avin on 20/10/16.
 */

public class HorizontalPagerIndicator extends View {
    private Paint mBlocksPaint;
    private int mCompleteColor, mIncompleteColor, mBarColor;
    private float mRectHeight = -1, mRectWidth = -1, mBarHeight;
    private float mInBetweenPadding = 5;
    private int mTotalBlocks, mCompletedBlocks, mSelectedBlock = 0;
    private OnSelectionChangedListener mOnSelectionChangedListener;

    public HorizontalPagerIndicator(Context context) {
        super(context);
        init(null);
    }

    public HorizontalPagerIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public HorizontalPagerIndicator(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public HorizontalPagerIndicator(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        mCompletedBlocks = 0;
        mTotalBlocks = 10;
        mCompleteColor = Color.YELLOW;
        mIncompleteColor = Color.LTGRAY;
        mBarHeight = UiUtils.dpToPx(3);
        mSelectedBlock = 0;

        mBlocksPaint = new Paint();
        mBlocksPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        mBlocksPaint.setAntiAlias(true);

        if (attrs != null) {
            TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.HorizontalPagerIndicator);
            mCompleteColor = typedArray.getColor(R.styleable.HorizontalPagerIndicator_completed_color, Color.YELLOW);
            mIncompleteColor = typedArray.getColor(R.styleable.HorizontalPagerIndicator_incomplete_color, Color.LTGRAY);
            mTotalBlocks = typedArray.getInt(R.styleable.HorizontalPagerIndicator_total_blocks, 0);
            mCompletedBlocks = typedArray.getInt(R.styleable.HorizontalPagerIndicator_completed_blocks, 0);
            mBarColor = typedArray.getColor(R.styleable.HorizontalPagerIndicator_bar_color, Color.YELLOW);
            mBarHeight = typedArray.getDimensionPixelSize(R.styleable.HorizontalPagerIndicator_bar_height, (int) mBarHeight);
            mSelectedBlock = 0;
            typedArray.recycle();
        }
    }

    public void reset(int totalBlocks, int completedBlocks) {
        this.mTotalBlocks = totalBlocks;
        this.mCompletedBlocks = completedBlocks;
        mSelectedBlock = (mCompletedBlocks > 0) ? ((mCompletedBlocks == mTotalBlocks) ? (mCompletedBlocks - 1) : mCompletedBlocks) : 0;
        if (mOnSelectionChangedListener != null) {
            mOnSelectionChangedListener.onSelectionChanged(mSelectedBlock, mCompletedBlocks, mTotalBlocks);
        }
        invalidate();
    }

    public int getmTotalBlocks() {
        return mTotalBlocks;
    }

    public void setmTotalBlocks(int mTotalBlocks) {
        if (mTotalBlocks != this.mTotalBlocks) {
            this.mTotalBlocks = mTotalBlocks;
            invalidate();
        }
    }

    public int getmCompletedBlocks() {
        return mCompletedBlocks;
    }

    public void setmCompletedBlocks(int mCompletedBlocks) {
        if (mCompletedBlocks != this.mCompletedBlocks) {
            this.mCompletedBlocks = mCompletedBlocks;
            mSelectedBlock = (mCompletedBlocks > 0) ? (mCompletedBlocks - 1) : 0;
            if (mOnSelectionChangedListener != null) {
                mOnSelectionChangedListener.onSelectionChanged(mSelectedBlock, mCompletedBlocks, mTotalBlocks);
            }
            invalidate();
        }
    }

    public int getCurrentBlock() {
        return (mSelectedBlock == mTotalBlocks) ? (mTotalBlocks - 1) : mSelectedBlock;
    }

    public void setOnSelectionChangedListener(OnSelectionChangedListener onSelectionChangedListener) {
        this.mOnSelectionChangedListener = onSelectionChangedListener;
    }

    public void moveNext(boolean completeCurrent) {
        if (mSelectedBlock > mTotalBlocks - 1) {
            return;
        }

        if (mSelectedBlock == mCompletedBlocks) {
            if (completeCurrent) {
                mCompletedBlocks++;
            }
        }
        if (mSelectedBlock < mTotalBlocks - 1) {
            mSelectedBlock++;
        }
        invalidate();
        if (mOnSelectionChangedListener != null) {
            mOnSelectionChangedListener.onSelectionChanged(mSelectedBlock + 1, mCompletedBlocks, mTotalBlocks);
        }
    }

    public void moveBack() {
        if (mSelectedBlock == 0) {
            return;
        }
        mSelectedBlock--;
        invalidate();
        if (mOnSelectionChangedListener != null) {
            mOnSelectionChangedListener.onSelectionChanged(mSelectedBlock + 1, mCompletedBlocks, mTotalBlocks);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (mTotalBlocks <= 0) {
            return;
        }
        calculateDimensions();
        drawCompletedBlocks(canvas);
        drawIncompletedBlocks(canvas);
    }

    private void calculateDimensions() {
        if (mRectHeight == -1) {
            int height = getHeight() - getPaddingTop() - getPaddingBottom();
            mRectHeight = height - mBarHeight - mInBetweenPadding;
        }

        if (mRectWidth == -1) {
            int width = getWidth() - getPaddingLeft() - getPaddingRight();
            mRectWidth = (width - (mTotalBlocks - 1) * mInBetweenPadding) / mTotalBlocks;
        }
    }

    private void drawCompletedBlocks(Canvas canvas) {
        mBlocksPaint.setColor(mCompleteColor);
        for (int i = 0; i < mCompletedBlocks; i++) {
            float left = getLeftX(i);
            float right = left + mRectWidth;
            float top = 0;
            float bottom = top + mRectHeight;
            canvas.drawRect(left, top, right, bottom, mBlocksPaint);

            if (i == mSelectedBlock) {
                mBlocksPaint.setColor(mBarColor);
                canvas.drawRect(left, top + mRectHeight + mInBetweenPadding, right,
                        bottom + mInBetweenPadding + mBarHeight, mBlocksPaint);
                mBlocksPaint.setColor(mCompleteColor);
            }
        }
    }

    private void drawIncompletedBlocks(Canvas canvas) {
        mBlocksPaint.setColor(mIncompleteColor);
        for (int i = mCompletedBlocks; i < mTotalBlocks; i++) {
            float left = getLeftX(i);
            float right = left + mRectWidth;
            float top = 0;
            float bottom = top + mRectHeight;
            canvas.drawRect(left, top, right, bottom, mBlocksPaint);

            if (i == mSelectedBlock) {
                mBlocksPaint.setColor(mBarColor);
                canvas.drawRect(left, top + mRectHeight + mInBetweenPadding, right,
                        bottom + mInBetweenPadding + mBarHeight, mBlocksPaint);
                mBlocksPaint.setColor(mIncompleteColor);
            }
        }
    }

    private float getLeftX(int index) {
        return getPaddingLeft() + index * mRectWidth + index * mInBetweenPadding;
    }

    public interface OnSelectionChangedListener {
        void onSelectionChanged(int selectedPosition, int completedBlocks, int totalBlocks);
    }
}
