package com.trulymadly.android.app.custom;

import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

/**
 * Created by avin on 25/05/16.
 */
public abstract class SlideListener implements View.OnTouchListener {

    private static final int MIN_LOCK_DISTANCE = 30;
    private final String LEFT_ITEM_TAG = "left";
    private final String RIGHT_ITEM_TAG = "right";
    private final long DEF_SNAP_BACK_ANIMATION_DURATION = 250L;
    private int mEmptySpaceFromRight = 150;
    private boolean mMotionInterceptDisallowed = false;
    private float mDownX, mUpX;
    private long mSnapBackAnimationDuration = DEF_SNAP_BACK_ANIMATION_DURATION;
    private int mLeftItemWidth = 0;
    private int mRightItemWidth = 0;
    private View mLeftItemView, mRightItemView, mSlidingView;
    private ViewGroup mParentView;
    private boolean isLeftItemPresent, isRightItemPresent;
    private int mCurrentMarginFromLeft = 0;
    private int mFinalMargin = 0;
    private RecyclerView mRecyclerView;
    private int mStartMarginFromLeft;
    private AnimatorSet mAnimatorSet;

    public SlideListener(ViewGroup parentView) {
        initialize(parentView);
    }

    public SlideListener(RecyclerView recyclerView) {
        mRecyclerView = recyclerView;
    }

    public void initialize(View view) {
        mSlidingView = view;
        this.mParentView = (ViewGroup) view.getParent();
        this.mLeftItemView = mParentView.findViewWithTag(LEFT_ITEM_TAG);
        this.mRightItemView = mParentView.findViewWithTag(RIGHT_ITEM_TAG);
        isLeftItemPresent = mLeftItemView != null;
        isRightItemPresent = mRightItemView != null;
        if (isLeftItemPresent) {
            this.mLeftItemWidth = mLeftItemView.getMeasuredWidth();
        }
        if (isRightItemPresent) {
            this.mRightItemWidth = mRightItemView.getMeasuredWidth();
        }

        mRightItemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRightClicked();
                reset(mSlidingView, 0);
            }
        });

        resetRight();
    }

    private int getmLeftItemWidth(){
        if(isLeftItemPresent && mLeftItemWidth == 0){
            mLeftItemWidth = mLeftItemView.getMeasuredWidth();
        }

        return mLeftItemWidth;
    }

    private int getmRightItemWidth(){
        if(isRightItemPresent && mRightItemWidth == 0){
            mRightItemWidth = mRightItemView.getMeasuredWidth();
        }

        return mRightItemWidth;
    }

    private int getMarginFromLeft(View view){
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
        return layoutParams.leftMargin;
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
                mDownX = event.getX();
                mStartMarginFromLeft = getMarginFromLeft(view);
                break;
            case MotionEvent.ACTION_UP:
                mUpX = event.getX();
                int finalMargin = 0;

                if(Math.abs(Math.abs(getMarginFromLeft(view)) - Math.abs(mStartMarginFromLeft))
                        <= MIN_LOCK_DISTANCE){
                    onClicked();
                }

                if(mCurrentMarginFromLeft > 0 && (isLeftItemPresent && mCurrentMarginFromLeft > getmLeftItemWidth())){
                    finalMargin = getmLeftItemWidth();
                }else if(mCurrentMarginFromLeft < 0 && (isRightItemPresent && Math.abs(mCurrentMarginFromLeft) > (getmRightItemWidth() - mEmptySpaceFromRight))){
                    finalMargin = (-1)*(getmRightItemWidth() - mEmptySpaceFromRight);
                }

                if(mCurrentMarginFromLeft != 0) {
                    reset(view, finalMargin);
                }

                mFinalMargin = finalMargin;

                if (mRecyclerView != null) {
                    mRecyclerView.requestDisallowInterceptTouchEvent(false);
                    mMotionInterceptDisallowed = false;
                }

                break;
            case MotionEvent.ACTION_MOVE:
                mUpX = event.getX();
                float deltaX = mDownX-mUpX;

                if (Math.abs(deltaX) > MIN_LOCK_DISTANCE && mRecyclerView != null && !mMotionInterceptDisallowed) {
                    mRecyclerView.requestDisallowInterceptTouchEvent(true);
                    mMotionInterceptDisallowed = true;
                }

                if(mCurrentMarginFromLeft > 0){
                    if(mLeftItemView != null) {
                        mLeftItemView.setVisibility(View.VISIBLE);
                    }
                    if(mRightItemView != null) {
                        mRightItemView.setVisibility(View.INVISIBLE);
                    }
                }else if(mCurrentMarginFromLeft < 0){
                    if(mRightItemView != null) {
                        mRightItemView.setVisibility(View.VISIBLE);
                    }
                    if(mLeftItemView != null) {
                        mLeftItemView.setVisibility(View.INVISIBLE);
                    }
                }

                if(deltaX > 0 && Math.abs(mCurrentMarginFromLeft) <= (getmRightItemWidth() - mEmptySpaceFromRight)) {
                    swipe(view, (int) deltaX, deltaX > 0);
                }

                if(deltaX < 0 && (Math.abs(mFinalMargin) == (getmRightItemWidth() - mEmptySpaceFromRight))){
                    swipe(view, (int) deltaX, deltaX > 0);
                }

                break;

            case MotionEvent.ACTION_CANCEL:
                reset(view, 0);
                mMotionInterceptDisallowed = false;
                return false;
        }
        return true;
    }

    public void reset(final View view, int finalMargin){
        mCurrentMarginFromLeft = finalMargin;
        final RelativeLayout.LayoutParams layoutParamsRight = (RelativeLayout.LayoutParams) mRightItemView.getLayoutParams();
//        if(layoutParamsRight.rightMargin <= getmRightItemWidth()) {
//            layoutParamsRight.rightMargin = layoutParamsRight.rightMargin;
//            layoutParamsRight.leftMargin = layoutParamsRight.leftMargin;
//            mRightItemView.setLayoutParams(layoutParamsRight);
//        }

        final RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
        ValueAnimator animator = ValueAnimator.ofInt(layoutParams.leftMargin, finalMargin);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator)
            {

                layoutParamsRight.leftMargin = layoutParamsRight.leftMargin
                        + Math.abs(Math.abs(layoutParams.leftMargin)
                        - Math.abs(((Integer) valueAnimator.getAnimatedValue())));
                layoutParamsRight.rightMargin = layoutParamsRight.rightMargin
                        - Math.abs(Math.abs(layoutParams.rightMargin)
                        -Math.abs((Integer) valueAnimator.getAnimatedValue()));
                mRightItemView.setLayoutParams(layoutParamsRight);


                layoutParams.leftMargin = (Integer) valueAnimator.getAnimatedValue();
                layoutParams.rightMargin = (-1)*((Integer) valueAnimator.getAnimatedValue());
                view.setLayoutParams(layoutParams);
            }
        });
        animator.setDuration(mSnapBackAnimationDuration);
        animator.start();

//        if(Math.abs(finalMargin) != getmRightItemWidth()) {
//            resetRight();
//        }
    }

    public void hardReset(final View view){
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
        layoutParams.leftMargin = 0;
        layoutParams.rightMargin = 0;
        mCurrentMarginFromLeft = 0;
        view.setLayoutParams(layoutParams);
    }

    public void hardReset(){
        if(mSlidingView != null){
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mSlidingView.getLayoutParams();
            layoutParams.leftMargin = 0;
            layoutParams.rightMargin = 0;
            mCurrentMarginFromLeft = 0;
            mSlidingView.setLayoutParams(layoutParams);

            resetRight();
        }
    }

    public void resetRight(){
        RelativeLayout.LayoutParams layoutParamsRight = (RelativeLayout.LayoutParams) mRightItemView.getLayoutParams();
        layoutParamsRight.rightMargin =  - getmRightItemWidth();
        layoutParamsRight.leftMargin = getmRightItemWidth();
        mRightItemView.setLayoutParams(layoutParamsRight);
    }

    public void swipe(View view, int distance, boolean showRight){

        RelativeLayout.LayoutParams layoutParamsRight = (RelativeLayout.LayoutParams) mRightItemView.getLayoutParams();
        if(showRight) {
            if (mCurrentMarginFromLeft == 0) {
                resetRight();
            }
            if(Math.abs(layoutParamsRight.rightMargin) < mEmptySpaceFromRight) {
                return;
            }
        }

        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
        layoutParams.rightMargin = layoutParams.rightMargin + distance;
        layoutParams.leftMargin = layoutParams.leftMargin - distance;
        mCurrentMarginFromLeft = layoutParams.leftMargin;
        view.setLayoutParams(layoutParams);

        if(layoutParamsRight.rightMargin <= 0) {
            layoutParamsRight.rightMargin = layoutParamsRight.rightMargin + distance;
            layoutParamsRight.leftMargin = layoutParamsRight.leftMargin - distance;
            mRightItemView.setLayoutParams(layoutParamsRight);
        }

//        if(showRight){
//            if(mCurrentMarginFromLeft == 0){
//                resetRight();
//            }
//
//            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
//            layoutParams.rightMargin = layoutParams.rightMargin + distance;
//            layoutParams.leftMargin = layoutParams.leftMargin - distance;
//            mCurrentMarginFromLeft = layoutParams.leftMargin;
//            view.setLayoutParams(layoutParams);
//
//            RelativeLayout.LayoutParams layoutParamsRight = (RelativeLayout.LayoutParams) mRightItemView.getLayoutParams();
//            if(layoutParamsRight.rightMargin <= 0) {
//                layoutParamsRight.rightMargin = layoutParamsRight.rightMargin + distance;
//                layoutParamsRight.leftMargin = layoutParamsRight.leftMargin - distance;
//                mRightItemView.setLayoutParams(layoutParamsRight);
//            }
//        }else{
//
//            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
//            layoutParams.rightMargin = layoutParams.rightMargin + distance;
//            layoutParams.leftMargin = layoutParams.leftMargin - distance;
//            mCurrentMarginFromLeft = layoutParams.leftMargin;
//            view.setLayoutParams(layoutParams);
//
//            RelativeLayout.LayoutParams layoutParamsRight = (RelativeLayout.LayoutParams) mRightItemView.getLayoutParams();
//            if(layoutParamsRight.rightMargin <= 0) {
//                layoutParamsRight.rightMargin = layoutParamsRight.rightMargin + distance;
//                layoutParamsRight.leftMargin = layoutParamsRight.leftMargin - distance;
//                mRightItemView.setLayoutParams(layoutParamsRight);
//            }
//        }
    }

    public void startTutorialAnimation(){

        resetRight();
        final RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mSlidingView.getLayoutParams();
        final RelativeLayout.LayoutParams layoutParamsRight = (RelativeLayout.LayoutParams) mRightItemView.getLayoutParams();
        ValueAnimator revealRightOptionAnimator = ValueAnimator.ofInt(0, -getmRightItemWidth() + mEmptySpaceFromRight);
        revealRightOptionAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator)
            {

                layoutParamsRight.leftMargin = layoutParamsRight.leftMargin
                        - Math.abs(Math.abs(layoutParams.leftMargin)
                        - Math.abs(((Integer) valueAnimator.getAnimatedValue())));
                layoutParamsRight.rightMargin = layoutParamsRight.rightMargin
                        + Math.abs(Math.abs(layoutParams.rightMargin)
                        - Math.abs((Integer) valueAnimator.getAnimatedValue()));
                mRightItemView.setLayoutParams(layoutParamsRight);


                layoutParams.leftMargin = (Integer) valueAnimator.getAnimatedValue();
                layoutParams.rightMargin = (-1)*((Integer) valueAnimator.getAnimatedValue());
                mSlidingView.setLayoutParams(layoutParams);
            }
        });
        revealRightOptionAnimator.setDuration(418);

        ValueAnimator resetRightRevealAnimator = ValueAnimator.ofInt(-getmRightItemWidth() + mEmptySpaceFromRight, 0);
        resetRightRevealAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator)
            {

                layoutParamsRight.leftMargin = layoutParamsRight.leftMargin
                        + Math.abs(Math.abs(layoutParams.leftMargin)
                        - Math.abs(((Integer) valueAnimator.getAnimatedValue())));
                layoutParamsRight.rightMargin = layoutParamsRight.rightMargin
                        - Math.abs(Math.abs(layoutParams.rightMargin)
                        -Math.abs((Integer) valueAnimator.getAnimatedValue()));
                mRightItemView.setLayoutParams(layoutParamsRight);

                layoutParams.leftMargin = (Integer) valueAnimator.getAnimatedValue();
                layoutParams.rightMargin = (-1)*((Integer) valueAnimator.getAnimatedValue());
                mSlidingView.setLayoutParams(layoutParams);
            }
        });
        resetRightRevealAnimator.setDuration(418);

        if(mAnimatorSet != null){
            mAnimatorSet.cancel();
            hardReset();
        }
        mAnimatorSet = new AnimatorSet();
        mAnimatorSet.playSequentially(revealRightOptionAnimator, resetRightRevealAnimator);
        mAnimatorSet.setStartDelay(418);
        mAnimatorSet.start();

    }

    public void cancelTutorialAnimation(){
        if(mAnimatorSet != null && !mAnimatorSet.isRunning()){
            mAnimatorSet.cancel();
            hardReset();
            mAnimatorSet = null;
        }
    }

    public abstract void onRightClicked();
    public abstract void onClicked();

}