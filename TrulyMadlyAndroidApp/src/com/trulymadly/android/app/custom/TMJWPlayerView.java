package com.trulymadly.android.app.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;

import com.longtailvideo.jwplayer.JWPlayerView;
import com.longtailvideo.jwplayer.configuration.PlayerConfig;
import com.longtailvideo.jwplayer.events.listeners.VideoPlayerEvents;

/**
 * Created by avin on 27/09/16.
 */
public class TMJWPlayerView extends JWPlayerView {

    public static final String TAG = "TMJWPlayerView";
    public static int mPlayerCount = 0;

    public TMJWPlayerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        mPlayerCount++;
        Log.d(TAG, "Player initialized, current : " + mPlayerCount);
    }

    public TMJWPlayerView(Context context, PlayerConfig playerConfig) {
        super(context, playerConfig);
        mPlayerCount++;
        Log.d(TAG, "Player initialized, current : " + mPlayerCount);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPlayerCount--;
        Log.d(TAG, "Player destroyed, left : " + mPlayerCount);
    }


    @Override
    public void addOnBufferListener(VideoPlayerEvents.OnBufferListener onBufferListener) {
        Log.d(TAG, "Player buffer added");
        super.addOnBufferListener(onBufferListener);
    }

    @Override
    public boolean removeOnBufferListener(VideoPlayerEvents.OnBufferListener onBufferListener) {
        Log.d(TAG, "Player buffer removed");
        return super.removeOnBufferListener(onBufferListener);
    }
}
