package com.trulymadly.android.app.custom;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.RelativeLayout;

/**
 * Created by avin on 24/05/16.
 */
public abstract class SwipeListener implements View.OnTouchListener {

    private static final long DEF_SNAP_BACK_ANIMATION_DURATION = 250L;
    private static final long DELETE_ANIMATION_DURATION = 418L;
    private final String LEFT_ITEM_TAG = "left";
    private final String RIGHT_ITEM_TAG = "right";
    private final String BACK_ITEM_TAG = "back";
    private final int MINIMUM_MOVEMENT_FOR_SWIPE = 30;
    private float mDownX, mUpX;
    private long mSnapBackAnimationDuration = DEF_SNAP_BACK_ANIMATION_DURATION;
    private int mLeftItemWidth = 0;
    private int mRightItemWidth = 0;
    private View mLeftItemView, mRightItemView, mBackItemView;
    private ViewGroup mParentView;
    private boolean isLeftItemPresent, isRightItemPresent;
    private int mCurrentMarginFromLeft = 0, mStartMarginFromLeft = 0;
    private AnimatorSet mTutorialAnimatorSet;


    public SwipeListener(ViewGroup parentView, long snapBackAnimationDuration) {
        this.mSnapBackAnimationDuration = snapBackAnimationDuration;
        this.mParentView = parentView;
        this.mBackItemView = parentView.findViewWithTag(BACK_ITEM_TAG);
        this.mLeftItemView = mBackItemView.findViewWithTag(LEFT_ITEM_TAG);
        this.mRightItemView = mBackItemView.findViewWithTag(RIGHT_ITEM_TAG);
        isLeftItemPresent = mLeftItemView != null;
        isRightItemPresent = mRightItemView != null;
        if(isLeftItemPresent) {
            this.mLeftItemWidth = mLeftItemView.getMeasuredWidth();
            mLeftItemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onLeftClicked();
                }
            });
        }
        if(isRightItemPresent) {
            this.mRightItemWidth = mRightItemView.getMeasuredWidth();
            mRightItemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onRightClicked();
                }
            });
        }
    }

    public SwipeListener(ViewGroup parentView) {
        this(parentView, DEF_SNAP_BACK_ANIMATION_DURATION);
    }

    private int getmLeftItemWidth(){
        if(isLeftItemPresent && mLeftItemWidth == 0){
            mLeftItemWidth = mLeftItemView.getMeasuredWidth();
        }

        return mLeftItemWidth;
    }

    private int getMarginFromLeft(View view){
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
        return layoutParams.leftMargin;
    }

    private int getmRightItemWidth(){
        if(isRightItemPresent && mRightItemWidth == 0){
            mRightItemWidth = mRightItemView.getMeasuredWidth();
        }

        return mRightItemWidth;
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
                mDownX = event.getX();
                mStartMarginFromLeft = getMarginFromLeft(view);
                break;
            case MotionEvent.ACTION_UP:
                mUpX = event.getX();

                if(Math.abs(Math.abs(getMarginFromLeft(view)) - Math.abs(mStartMarginFromLeft))
                        <= MINIMUM_MOVEMENT_FOR_SWIPE){
                    onClicked();
                }

                int finalMargin = 0;
                if(mCurrentMarginFromLeft > 0 && (isLeftItemPresent && mCurrentMarginFromLeft > getmLeftItemWidth())){
                    finalMargin = getmLeftItemWidth();
                }else if(mCurrentMarginFromLeft < 0 && (isRightItemPresent && Math.abs(mCurrentMarginFromLeft) > getmRightItemWidth())){
                    finalMargin = (-1)*getmRightItemWidth();
                }
                if(mCurrentMarginFromLeft != 0) {
                    reset(view, finalMargin);
                }

//                if(mCurrentMarginFromLeft > 0 && (!isLeftItemPresent || mCurrentMarginFromLeft < getmLeftItemWidth())){
//                    reset(view, 0);
//                }else if(mCurrentMarginFromLeft < 0 && (!isRightItemPresent || Math.abs(mCurrentMarginFromLeft) < getmRightItemWidth())){
//                    reset(view, 0);
//                } else if(mCurrentMarginFromLeft > 0 && (isLeftItemPresent && mCurrentMarginFromLeft > getmLeftItemWidth())){
//                    reset(view, getmLeftItemWidth());
//                }else if(mCurrentMarginFromLeft < 0 && (isRightItemPresent && Math.abs(mCurrentMarginFromLeft) > getmRightItemWidth())){
//                    reset(view, (-1)*getmRightItemWidth());
//                }
                break;
            case MotionEvent.ACTION_MOVE:
                mUpX = event.getX();
                float deltaX = mDownX-mUpX;
                if(mCurrentMarginFromLeft > 0){
                    mLeftItemView.setVisibility(View.VISIBLE);
                    mRightItemView.setVisibility(View.INVISIBLE);
                }else if(mCurrentMarginFromLeft < 0){
                    mRightItemView.setVisibility(View.VISIBLE);
                    mLeftItemView.setVisibility(View.INVISIBLE);
                }
                swipe(view, (int) deltaX);
                break;
        }
        return true;
    }

    public void reset(final View view, int finalMargin){
        final RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
        ValueAnimator animator = ValueAnimator.ofInt(layoutParams.leftMargin, finalMargin);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator)
            {
                layoutParams.leftMargin = (Integer) valueAnimator.getAnimatedValue();
                layoutParams.rightMargin = (-1)*((Integer) valueAnimator.getAnimatedValue());
                view.setLayoutParams(layoutParams);
            }
        });
        animator.setDuration(mSnapBackAnimationDuration);
        animator.start();
    }

    public void hardReset(View view){
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
        layoutParams.leftMargin = 0;
        layoutParams.rightMargin = 0;
        mCurrentMarginFromLeft = layoutParams.leftMargin;
        view.setLayoutParams(layoutParams);
    }

    public void swipe(View view, int distance){
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
        layoutParams.rightMargin = layoutParams.rightMargin + distance;
        layoutParams.leftMargin = layoutParams.leftMargin - distance;
        mCurrentMarginFromLeft = layoutParams.leftMargin;
        view.setLayoutParams(layoutParams);
    }

    public void slideLeft(final View view, final Animation.AnimationListener animationListener) {
        int finalMargin = view.getWidth();
        final RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
        ValueAnimator animator = ValueAnimator.ofInt((-1)*layoutParams.leftMargin, finalMargin);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator)
            {
                layoutParams.leftMargin = (-1)*(Integer) valueAnimator.getAnimatedValue();
                layoutParams.rightMargin = ((Integer) valueAnimator.getAnimatedValue());
                view.setLayoutParams(layoutParams);
            }
        });
        animator.setDuration(DELETE_ANIMATION_DURATION);
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                mLeftItemView.setVisibility(View.INVISIBLE);
                mRightItemView.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (animationListener != null) {
                    animationListener.onAnimationEnd(null);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        animator.start();
    }

    public void slideRight(final View view, final Animation.AnimationListener animationListener) {
        int finalMargin = view.getWidth();
        final RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
        ValueAnimator animator = ValueAnimator.ofInt(layoutParams.leftMargin, finalMargin);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                layoutParams.leftMargin = (Integer) valueAnimator.getAnimatedValue();
                layoutParams.rightMargin = (-1) * ((Integer) valueAnimator.getAnimatedValue());
                view.setLayoutParams(layoutParams);
            }
        });
        animator.setDuration(DELETE_ANIMATION_DURATION);
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                mLeftItemView.setVisibility(View.INVISIBLE);
                mRightItemView.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if(animationListener != null) {
                    animationListener.onAnimationEnd(null);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        animator.start();
    }

    public void startTutorialAnimation(final View view, final Animation.AnimationListener animationListener){

        mLeftItemView.setVisibility(View.VISIBLE);
        mRightItemView.setVisibility(View.VISIBLE);
        int leftItemWidth = getmLeftItemWidth();
        int rightItemWidth = getmRightItemWidth();
        final RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();

        ValueAnimator revealLeftAnimator = ValueAnimator.ofInt(0, leftItemWidth);
        revealLeftAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                layoutParams.leftMargin = (Integer) valueAnimator.getAnimatedValue();
                layoutParams.rightMargin = (-1) * ((Integer) valueAnimator.getAnimatedValue());
                view.setLayoutParams(layoutParams);
            }
        });
        revealLeftAnimator.setDuration(418);

        ValueAnimator resetFromRevealLeft = ValueAnimator.ofInt(leftItemWidth, 0);
        resetFromRevealLeft.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                layoutParams.leftMargin = (Integer) valueAnimator.getAnimatedValue();
                layoutParams.rightMargin = (-1) * ((Integer) valueAnimator.getAnimatedValue());
                view.setLayoutParams(layoutParams);
            }
        });
        resetFromRevealLeft.setDuration(418);

        ValueAnimator revealRightAnimator = ValueAnimator.ofInt(0, rightItemWidth);
        revealRightAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                layoutParams.leftMargin = (-1) * (Integer) valueAnimator.getAnimatedValue();
                layoutParams.rightMargin = ((Integer) valueAnimator.getAnimatedValue());
                view.setLayoutParams(layoutParams);
            }
        });
        revealRightAnimator.setDuration(418);

        ValueAnimator resetFromRevealRight = ValueAnimator.ofInt(-rightItemWidth, 0);
        resetFromRevealRight.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                layoutParams.leftMargin = (Integer) valueAnimator.getAnimatedValue();
                layoutParams.rightMargin = (-1) * ((Integer) valueAnimator.getAnimatedValue());
                view.setLayoutParams(layoutParams);
            }
        });
        resetFromRevealRight.setDuration(418);


        if(mTutorialAnimatorSet != null){
            mTutorialAnimatorSet.cancel();
        }

        mTutorialAnimatorSet = new AnimatorSet();
        mTutorialAnimatorSet.playSequentially(revealLeftAnimator, resetFromRevealLeft, revealRightAnimator, resetFromRevealRight);
        mTutorialAnimatorSet.setStartDelay(418);
        mTutorialAnimatorSet.start();

    }

    public void cancelTutorialAnimation(View view){
        if(mTutorialAnimatorSet != null && mTutorialAnimatorSet.isRunning()){
            mTutorialAnimatorSet.cancel();
            hardReset(view);
            mTutorialAnimatorSet = null;
        }
    }

    public abstract void onClicked();
    public abstract void onLeftClicked();
    public abstract void onRightClicked();
}
