package com.trulymadly.android.app.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;

import com.trulymadly.android.app.R;
import com.trulymadly.android.app.utility.TimeUtils;

/**
 * Created by avin on 22/06/16.
 */
public class DigitalTimerView extends TextView{

    private String mPrefix, mSuffix, mTimerText;
    private int mSecondsLeft;
    private CountDownTimer mCountDownTimer;
    private boolean mShowTimeUnit;


    private String mMin, mHour, mSec, mMins, mHours;


    public DigitalTimerView(Context context) {
        super(context);
        init(null);
    }

    public DigitalTimerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public DigitalTimerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    /*
    * Initializes the TimerConfig object used for all the calculations and initializing the fields
    * */
    private void init(AttributeSet attributeSet) {
        mMin = getContext().getString(R.string.min);
        mMins = getContext().getString(R.string.mins);
        mSec = getContext().getString(R.string.second);
        mHour = getContext().getString(R.string.hour);
        mHours = getContext().getString(R.string.hours);

        if(attributeSet == null){
            return;
        }

        TypedArray array = getContext().obtainStyledAttributes(attributeSet, R.styleable.DigitalTimerView);

        for (int i = 0; i < array.length(); i++) {
            int type = array.getIndex(i);

            if (type == R.styleable.DigitalTimerView_prefix) {
                mPrefix = array.getString(R.styleable.DigitalTimerView_prefix);
            }

            if (type == R.styleable.DigitalTimerView_suffix) {
                mSuffix = array.getString(R.styleable.DigitalTimerView_suffix);
            }

            if (type == R.styleable.DigitalTimerView_show_time_unit) {
                mShowTimeUnit = array.getBoolean(R.styleable.DigitalTimerView_show_time_unit, false);
            }
        }

        array.recycle();
    }

    public void cancelTimer(){
        if(mCountDownTimer != null){
            mCountDownTimer.cancel();
        }
    }

    public void startTimer(int secondsLeft, int interval){
        if(mCountDownTimer != null){
            mCountDownTimer.cancel();
        }
        mSecondsLeft = secondsLeft;
        displayText();
        mCountDownTimer = new CountDownTimer(secondsLeft*1000, interval) {
            @Override
            public void onTick(long millisUntilFinished) {
                calculateTime(millisUntilFinished);
            }

            @Override
            public void onFinish() {
                calculateTime(0);
            }
        };

        mCountDownTimer.start();
    }

    private void calculateTime(long milliSeconds) {
        mSecondsLeft = (int) (milliSeconds/1000);
        displayText();
    }

    private void displayText() {
        StringBuffer buffer = new StringBuffer();
        if (!TextUtils.isEmpty(mPrefix)) {
            buffer.append(mPrefix);
            buffer.append(" ");
        }

        buffer.append(TimeUtils.getFormattedLeftTime(mSecondsLeft));

        if(mShowTimeUnit){
            buffer.append(" ");
            if(mSecondsLeft < 2*60){
                buffer.append(mMin);
            }else if(mSecondsLeft < 60*60){
                buffer.append(mMins);
            }else if(mSecondsLeft <= 2* 60*60){
                buffer.append(mHour);
            }else{
                buffer.append(mHours);
            }
        }

        if (!TextUtils.isEmpty(mSuffix)) {
            buffer.append(mSuffix);
        }

        setText(buffer);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        cancelTimer();
    }
}
