package com.trulymadly.android.app.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;

import com.trulymadly.android.app.modal.ImageAndText;

import java.util.ArrayList;

/**
 * Created by avin on 16/11/16.
 */

public class SelectBuyViewPagerAdapter extends PagerAdapter {

    private ArrayList<ImageAndText> mImageAndTexts;
    private Context mContext;
    private LayoutInflater mInflater;
    private int mTextColor;

    public SelectBuyViewPagerAdapter(Context mContext, ArrayList<ImageAndText> mImageAndTexts) {
        this.mImageAndTexts = mImageAndTexts;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return (mImageAndTexts == null) ? 0 : mImageAndTexts.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return false;
    }
}
