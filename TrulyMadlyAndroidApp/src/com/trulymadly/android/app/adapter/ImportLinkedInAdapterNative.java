/**
 * 
 */
package com.trulymadly.android.app.adapter;

import android.app.Activity;
import android.content.Context;

import com.linkedin.platform.APIHelper;
import com.linkedin.platform.LISession;
import com.linkedin.platform.LISessionManager;
import com.linkedin.platform.errors.LIApiError;
import com.linkedin.platform.errors.LIAppErrorCode;
import com.linkedin.platform.errors.LIAuthError;
import com.linkedin.platform.listeners.ApiListener;
import com.linkedin.platform.listeners.ApiResponse;
import com.linkedin.platform.listeners.AuthListener;
import com.linkedin.platform.utils.Scope;
import com.trulymadly.android.app.listener.OnImportFromLinkedin;
import com.trulymadly.android.app.modal.LinkedInData;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author udbhav
 * 
 */
public class ImportLinkedInAdapterNative {

	private static final String SOURCE = "NATIVE SDK";
	private final OnImportFromLinkedin onImportFromLinkedin;
	private final Context aContext;
	//private SocialAuthAdapter socialAuthAdapter = null;
	//private Profile profileMap;
	//private Career careerMap;
	private int numConnections = 0;
	private String linkedInIndustryJson = null;
	private LISessionManager linkedInSessionManager;
	
	public ImportLinkedInAdapterNative(Context ctx, OnImportFromLinkedin onImportFromLinkedin) {
		super();
		this.aContext = ctx;
		this.onImportFromLinkedin = onImportFromLinkedin;
	}

	// Build the list of member permissions our LinkedIn session requires
	private static Scope buildScope() {
		return Scope.build(Scope.R_BASICPROFILE, Scope.R_EMAILADDRESS);
	}


	public void importFromLinkedIn(Activity aActivity) {
		linkedInSessionManager = LISessionManager.getInstance(aActivity.getApplicationContext());
		linkedInSessionManager.init(aActivity, buildScope(), new AuthListenerTM(), true);
	}

	private final class AuthListenerTM implements AuthListener {
		
		@Override
		public void onAuthSuccess() {
			onImportFromLinkedin.onProgress(SOURCE);
			LISession liSession = linkedInSessionManager.getSession();
			String url = "https://api.linkedin.com/v1/people/~:(id,first-name,last-name,num-connections,email-address,positions,industry,picture-urls::(original))?format=json";
			//"educations","courses"
			APIHelper apiHelper = APIHelper.getInstance(aContext.getApplicationContext());
			apiHelper.getRequest(aContext, url, new ApiListenerTM());
		}
		

		@Override
		public void onAuthError(LIAuthError error) {
			LIAppErrorCode errorCode = LIAppErrorCode.UNKNOWN_ERROR;
			try {
				errorCode = LIAppErrorCode.findErrorCode((new JSONObject(error.toString())).optString("errorCode"));
			} catch (JSONException ignored) {
			}
			
			if(errorCode == LIAppErrorCode.USER_CANCELLED){
				onImportFromLinkedin.onCancel(SOURCE);
			} else {
				onImportFromLinkedin.onFailure(SOURCE, "Authentication failure -- " + error.toString());
			}
		}
	}

	private final class ApiListenerTM implements
			ApiListener {

		@Override
		public void onApiSuccess(ApiResponse apiResponse) {
			JSONObject apiJsonResponse = apiResponse.getResponseDataAsJson();
			onImportFromLinkedin.onSuccess(SOURCE, LinkedInData.getDataFromNative(apiJsonResponse));
		}

		@Override
		public void onApiError(LIApiError LIApiError) {
			onImportFromLinkedin.onFailure(SOURCE, "API failure");
			
		}
	}
}
