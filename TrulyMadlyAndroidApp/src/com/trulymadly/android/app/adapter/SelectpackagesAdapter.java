package com.trulymadly.android.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.trulymadly.android.app.R;
import com.trulymadly.android.app.fragments.BuySparkEventListener;
import com.trulymadly.android.app.modal.SelectPackageModal;
import com.trulymadly.android.app.utility.ABHandler;
import com.trulymadly.android.app.utility.Utility;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by avin on 13/01/17.
 */

public class SelectpackagesAdapter extends RecyclerView.Adapter<SelectpackagesAdapter.SelectPackageVH>{

    private String matchId;
    private ArrayList<SelectPackageModal> mSelectPackageModals;
    private Context mContext;
    private BuySparkEventListener mBuySparkEventListener;

    public SelectpackagesAdapter(Context mContext, ArrayList<SelectPackageModal> mSelectPackageModals,
                                 String matchId, BuySparkEventListener mBuySparkEventListener) {
        this.mContext = mContext;
        this.mSelectPackageModals = mSelectPackageModals;
        this.matchId = matchId;
        this.mBuySparkEventListener = mBuySparkEventListener;
    }

    @Override
    public SelectPackageVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.select_package_list_item, parent, false);

        return new SelectPackageVH(itemView);
    }

    @Override
    public void onBindViewHolder(SelectPackageVH holder, int position) {
        holder.initialize(mSelectPackageModals.get(position), position);
    }

    @Override
    public int getItemCount() {
        return (mSelectPackageModals != null) ? mSelectPackageModals.size() : 0;
    }

    public class SelectPackageVH extends RecyclerView.ViewHolder {

        @BindView(R.id.select_package_title_tv)
        TextView mSelectPackageTitleTV;
        @BindView(R.id.select_package_subtitle_tv)
        TextView mSelectPackageSubTitleTV;
        @BindView(R.id.select_package_price)
        TextView mSelectPackagePriceTV;

        @BindView(R.id.redeem_container)
        View mRedeemContainer;
        @BindView(R.id.top_border)
        View mTopBorder;
        @BindView(R.id.bottom_border)
        View mBottomBorder;
        @BindView(R.id.match_guarantee)
        TextView mMatchGuaranteeTv;
        @BindView(R.id.tag)
        TextView mTagTV;
        @BindView(R.id.discount_tv)
        TextView mDiscountTV;

        private View mParentView;

        public SelectPackageVH(View itemView) {
            super(itemView);
            mParentView = itemView;
            ButterKnife.bind(this, itemView);
        }

        public void initialize(SelectPackageModal selectPackageModal, int position) {

            if(selectPackageModal.getmDiscount() > 0){
                mDiscountTV.setText(String.format(mContext.getString(R.string.select_disount),
                        selectPackageModal.getmDiscount()));
                mDiscountTV.setVisibility(View.VISIBLE);
            }else{
                mDiscountTV.setVisibility(View.GONE);
            }

            mSelectPackageTitleTV.setText(selectPackageModal.getmTitle());
            if (Utility.isSet(selectPackageModal.getmDescription())) {
                mRedeemContainer.setVisibility(View.VISIBLE);
                mSelectPackageSubTitleTV.setText(selectPackageModal.getmDescription());
            } else {
                mRedeemContainer.setVisibility(View.GONE);
            }

            mSelectPackagePriceTV.setText(String.format(mContext.getString(R.string.ruppee_value), String.valueOf(selectPackageModal.getmPrice())));

            if (position == (getItemCount() - 1)) {
                mTopBorder.setVisibility(View.VISIBLE);
                mBottomBorder.setVisibility(View.VISIBLE);
            } else {
                mTopBorder.setVisibility(View.VISIBLE);
                mBottomBorder.setVisibility(View.GONE);
            }

            if (selectPackageModal.isMatchGauranteed()) {
                mMatchGuaranteeTv.setVisibility(View.VISIBLE);
                mMatchGuaranteeTv.setText(Html.fromHtml(mContext.getResources().getString(R.string.match_guarantee) + "<sup>*</sup>"));
            } else {
                mMatchGuaranteeTv.setVisibility(View.GONE);
            }

            mParentView.setTag(selectPackageModal);
            mParentView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SelectPackageModal modal = (SelectPackageModal) v.getTag();
                    mBuySparkEventListener.onBuySparkClicked(modal, matchId);
                }
            });
        }
    }
}
