package com.trulymadly.android.app.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.trulymadly.android.app.R;

import java.util.ArrayList;

/**
 * Created by deveshbatra on 12/23/15.
 */
public class TermsAndConditionsAdapter extends
        RecyclerView.Adapter<TermsAndConditionsAdapter.TermsAndConditionsViewHolder> {


    private final ArrayList<String> terms;

    public TermsAndConditionsAdapter(ArrayList<String> terms) {
        this.terms = terms;
    }


    @Override
    public TermsAndConditionsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater mInflater = LayoutInflater.from(parent
                .getContext());
        final View sView = mInflater.inflate(R.layout.terms_item_layout, parent,
                false);
        return new TermsAndConditionsViewHolder(sView);
    }

    @Override
    public void onBindViewHolder(TermsAndConditionsViewHolder holder, int position) {

        holder.termTextView.setText(terms.get(position));
        holder.itemView.setTag(position);
    }

    @Override
    public int getItemCount() {
        return terms.size();
    }

    public class TermsAndConditionsViewHolder extends RecyclerView.ViewHolder {

        public final ImageView bullet;
        public final TextView termTextView;


        public TermsAndConditionsViewHolder(View view) {
            super(view);
            bullet = (ImageView) view.findViewById(R.id.bullet);
            termTextView = (TextView) view.findViewById(R.id.term_text_view);

        }


    }


}
