package com.trulymadly.android.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.listener.CategoryListItemClickInterface;
import com.trulymadly.android.app.modal.CategoryModal;
import com.trulymadly.android.app.utility.GradientTransformation;
import com.trulymadly.android.app.utility.ImageCacheHelper;
import com.trulymadly.android.app.utility.UiUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by deveshbatra on 3/8/16.
 */
public class CategoryListAdapter extends
        RecyclerView.Adapter<CategoryListAdapter.ViewHolderCategoryListItem> implements View.OnClickListener{

    private final double screenWidth;
    private final CategoryListItemClickInterface onClick;
    private final ArrayList<CategoryModal> categoryList;
    private final Activity aActivity;
    private final LayoutInflater inflater;
    private final GradientTransformation mCDGradientTransformation;

    public CategoryListAdapter(Activity act, ArrayList<CategoryModal> categoryList, CategoryListItemClickInterface onClick) {
        this.aActivity = act;
        this.categoryList = categoryList;

        inflater = (LayoutInflater) act
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        screenWidth = UiUtils.getScreenWidth(act);
        this.onClick = onClick;
//        mCDGradientTransformation = new GradientTransformation(new int[]{Color.parseColor("#00444343"),
//                Color.parseColor("#80444343"), Color.parseColor("#00444343")});

        mCDGradientTransformation = new GradientTransformation(new int[]{
                Color.parseColor("#00000000"), Color.parseColor("#96000000")});
    }

    @Override
    public ViewHolderCategoryListItem onCreateViewHolder(ViewGroup parent, int viewType) {

        View sView = inflater.inflate(R.layout.category_list_item, parent,
                false);
        return new ViewHolderCategoryListItem(sView);
    }

    @Override
    public void onBindViewHolder(ViewHolderCategoryListItem viewHolder, int position) {

        CategoryModal category = categoryList.get(position);
        viewHolder.itemView.setTag(category);
        viewHolder.itemView.setOnClickListener(this);

        ViewGroup.LayoutParams params = viewHolder.itemView.getLayoutParams();
        params.height = (int) (screenWidth / 2);
        viewHolder.itemView.setLayoutParams(params);

        ImageCacheHelper.with(aActivity).loadWithKey(category.getBanner(), category.getId()).transform(mCDGradientTransformation).into(viewHolder.category_banner);
        ImageCacheHelper.with(aActivity).loadWithKey(category.getIcon(), category.getId() + "icon").into(viewHolder.category_icon);

//        Picasso.with(aActivity).load(category.getIcon()).into(viewHolder.category_icon);
        viewHolder.category_listings.setText(category.getListings());
        viewHolder.category_name.setText(category.getName());
        // hardcoded
//        category.setNew(true);
        viewHolder.category_is_new_badge.setVisibility(category.isNew() ? View.VISIBLE : View.GONE);
        if(category.isNew()) {
            Picasso.with(aActivity).load(R.drawable.new_label).noFade().into(viewHolder.category_is_new_badge);
        }

    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    @Override
    public void onClick(View view) {
        CategoryModal categoryIdClicked = (CategoryModal) view.getTag();
        onClick.onCategoryItemClicked(categoryIdClicked.getId(), categoryIdClicked.getName());
    }

    class ViewHolderCategoryListItem extends RecyclerView.ViewHolder {

        final View itemView;
        @BindView(R.id.category_banner)
        ImageView category_banner;

        @BindView(R.id.category_icon)
        ImageView category_icon;

        @BindView(R.id.category_is_new_badge)
        ImageView category_is_new_badge;

        @BindView(R.id.category_name)
        TextView category_name;

        @BindView(R.id.category_listings)
        TextView category_listings;


        public ViewHolderCategoryListItem(View itemView) {
            super(itemView);
            this.itemView = itemView;
            ButterKnife.bind(this, itemView);
        }
    }

}
