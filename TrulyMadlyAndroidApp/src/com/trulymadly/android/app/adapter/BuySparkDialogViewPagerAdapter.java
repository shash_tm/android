package com.trulymadly.android.app.adapter;

import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.modal.ImageAndText;

import java.util.ArrayList;

/**
 * Created by avin on 13/06/16.
 */
public class BuySparkDialogViewPagerAdapter extends PagerAdapter {
    private ArrayList<ImageAndText> mImageAndTexts;
    private Context mContext;
    private LayoutInflater mInflater;
    private int mTextColor;

    public BuySparkDialogViewPagerAdapter(ArrayList<ImageAndText> mImageAndTexts, Context mContext) {
        this(mImageAndTexts, mContext, ActivityCompat.getColor(mContext, R.color.gray_all_7));
    }

    public BuySparkDialogViewPagerAdapter(ArrayList<ImageAndText> mImageAndTexts,
                                          Context mContext, int textColor) {
        this.mImageAndTexts = mImageAndTexts;
        this.mContext = mContext;
        mTextColor = textColor;
    }

    @Override
    public int getCount() {
        return (mImageAndTexts == null) ? 0 : mImageAndTexts.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        if (mInflater == null) {
            mInflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        View itemView = mInflater.inflate(R.layout.image_with_text_item,
                container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.image);
        Picasso.with(mContext).load(mImageAndTexts.get(position).getmDrawable()).noFade().into(imageView);

        TextView messageTV = (TextView)itemView.findViewById(R.id.text_tv);
        messageTV.setTextColor(mTextColor);
        messageTV.setText(mImageAndTexts.get(position).getmText());

        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }

    public void changeList(ArrayList<ImageAndText> imageAndTexts) {
        mImageAndTexts = imageAndTexts;
        notifyDataSetChanged();
    }
}
