package com.trulymadly.android.app.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.trulymadly.android.app.CircleTransformation;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.modal.QuestionDetailModal;
import com.trulymadly.android.app.modal.QuizDetailModal;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONObject;

import java.util.ArrayList;

public class CommonAnswersAdapter extends
		RecyclerView.Adapter<CommonAnswersAdapter.BaseViewHolder> {

	private final Activity aActivity;
	private final ArrayList<QuestionDetailModal> questionList;
	private final QuizDetailModal quiz;
	private JSONObject response;

	public CommonAnswersAdapter(Activity activity,
			ArrayList<QuestionDetailModal> list, QuizDetailModal quiz) {
		this.aActivity = activity;
		this.quiz = quiz;
		this.questionList = list;
	}

	@Override
	public int getItemCount() {
		return questionList.size();
	}

	@Override
	public int getItemViewType(int position) {
		if (questionList.get(position).getOptionType().equals("text")) {
			return 0;
		} else {
			return 1;
		}

	}

	@Override
	public void onBindViewHolder(BaseViewHolder holder, int position) {
		// text
		QuestionDetailModal questionDetails = new QuestionDetailModal();
		questionDetails = questionList.get(position);
		if (holder.getItemViewType() == 0) {
			ViewHolderTextType textViewHolder = (ViewHolderTextType) holder;

			Picasso.with(aActivity).load(this.quiz.getMatchImageUrl())
					.transform(new CircleTransformation())
					.placeholder(R.drawable.place_holder_image_url).fit()
					.into(textViewHolder.matchImage);

			Picasso.with(aActivity).load(this.quiz.getUserImageUrl())
					.placeholder(R.drawable.place_holder_image_url)
					.transform(new CircleTransformation()).fit()
					.into(textViewHolder.userImage);

			textViewHolder.questionNo.setText((position + 1) + "");
			textViewHolder.questionText.setText(questionDetails
					.getQuestionText());
			textViewHolder.userOptionText.setText(questionDetails
					.getUserOptionText());
			textViewHolder.matchOptionText.setText(questionDetails
					.getMatchOptionText());
			// same ans then white border
			if (textViewHolder.userOptionText
					.getText()
					.toString()
					.equalsIgnoreCase(
							textViewHolder.matchOptionText.getText().toString())) {

				UiUtils.setBackground(aActivity,
                        textViewHolder.userOptionTextViewContainer,
                        R.drawable.border_white);
				UiUtils.setBackground(aActivity,
                        textViewHolder.matchOptionTextViewContainer,
                        R.drawable.border_white);

			}

		}
		// image
		else {

			final ViewHolderImageType imageViewHolder = (ViewHolderImageType) holder;

			Picasso.with(aActivity).load(this.quiz.getMatchImageUrl())
					.transform(new CircleTransformation())
					.placeholder(R.drawable.place_holder_image_url).fit()
					.into(imageViewHolder.matchImage);

			Picasso.with(aActivity).load(this.quiz.getUserImageUrl())
					.transform(new CircleTransformation())
					.placeholder(R.drawable.place_holder_image_url).fit()
					.into(imageViewHolder.userImage);

			Picasso.with(aActivity)
					.load(questionDetails.getMatchOptionImageUrl())
					.placeholder(R.drawable.place_holder_banner)
					.into(imageViewHolder.matchOptionImageView);

			Picasso.with(aActivity)
					.load(questionDetails.getUserOptionImageUrl())
					.placeholder(R.drawable.place_holder_banner)
					.into(imageViewHolder.userOptionImageView);

			imageViewHolder.questionNo.setText((position + 1) + "");
			imageViewHolder.questionText.setText(questionDetails
					.getQuestionText());

			if (questionDetails.getUserOptionImageUrl().equalsIgnoreCase(
					questionDetails.getMatchOptionImageUrl())) {
				UiUtils.setBackground(aActivity,
                        imageViewHolder.userOptionImageViewContainer,
                        R.drawable.border_white);

				UiUtils.setBackground(aActivity,
                        imageViewHolder.matchOptionImageViewContainer,
                        R.drawable.border_white);
			}

			imageViewHolder.itemView.post(new Runnable() {

				@Override
				public void run() {

					LayoutParams userOptionsIvParams = imageViewHolder.userOptionImageView
							.getLayoutParams();
					userOptionsIvParams.height = imageViewHolder.userOptionImageView
							.getWidth();
					imageViewHolder.userOptionImageView
							.setLayoutParams(userOptionsIvParams);

					LayoutParams matchOptionsIvParams = imageViewHolder.matchOptionImageView
							.getLayoutParams();
					matchOptionsIvParams.height = imageViewHolder.matchOptionImageView
							.getWidth();
					imageViewHolder.matchOptionImageView
							.setLayoutParams(matchOptionsIvParams);

					imageViewHolder.userImage.requestLayout();
					imageViewHolder.matchImage.requestLayout();
				}
			});

		}

	}

	@Override
	public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

		final LayoutInflater mInflater = LayoutInflater.from(parent
				.getContext());
		final View sView;
		switch (viewType) {
		case 0:
			sView = mInflater.inflate(R.layout.quiz_answer_text_type, parent,
					false);
			return new ViewHolderTextType(sView);
		case 1:
			sView = mInflater.inflate(R.layout.quiz_answer_image_type, parent,
					false);
			return new ViewHolderImageType(sView);

		default:
			return null;
		}

	}

	class BaseViewHolder extends RecyclerView.ViewHolder {

		public BaseViewHolder(View itemView) {
			super(itemView);
		}

	}

	class ViewHolderImageType extends BaseViewHolder {
		public final TextView questionNo;
		public final TextView questionText;
		public final ImageView userOptionImageView;
		public final ImageView matchOptionImageView;
		public final ImageView userImage;
		public final ImageView matchImage;
		public final LinearLayout userOptionImageViewContainer;
		public final LinearLayout matchOptionImageViewContainer;

		public ViewHolderImageType(View itemView) {
			super(itemView);

			questionNo = (TextView) itemView.findViewById(R.id.question_no);
			questionText = (TextView) itemView.findViewById(R.id.question_text);

			userOptionImageView = (ImageView) itemView
					.findViewById(R.id.user_option_image_view);

			matchOptionImageView = (ImageView) itemView
					.findViewById(R.id.match_option_image_view);

			// LinearLayout.LayoutParams layoutParams = new
			// LinearLayout.LayoutParams(userOptionImageView.getMeasuredWidth(),userOptionImageView.getMeasuredWidth());
			// LayoutParams userOptionsIvParams =
			// userOptionImageView.getLayoutParams();
			// userOptionsIvParams.height = userOptionsIvParams.width;
			// userOptionImageView.getMeasuredWidth()
			// userOptionImageView.getWidth()
			// userOptionImageView.widt
			// userOptionImageView.setLayoutParams(userOptionsIvParams);

			LayoutParams matchOptionsIvParams = matchOptionImageView
					.getLayoutParams();
			matchOptionsIvParams.height = matchOptionsIvParams.width;
			matchOptionImageView.setLayoutParams(matchOptionsIvParams);

			userImage = (ImageView) itemView.findViewById(R.id.user_image);
			matchImage = (ImageView) itemView.findViewById(R.id.match_image);
			userOptionImageViewContainer = (LinearLayout) itemView
					.findViewById(R.id.user_option_image_view_container);
			matchOptionImageViewContainer = (LinearLayout) itemView
					.findViewById(R.id.match_option_image_view_container);

			if (Utility.isMale(aActivity)) {

				UiUtils.setBackground(aActivity, userOptionImageViewContainer,
                        R.drawable.border_blue);

				UiUtils.setBackground(aActivity, matchOptionImageViewContainer,
                        R.drawable.border_pink);

			} else {
				UiUtils.setBackground(aActivity, matchOptionImageViewContainer,
                        R.drawable.border_blue);

				UiUtils.setBackground(aActivity, userOptionImageViewContainer,
                        R.drawable.border_pink);

			}

		}
	}

	class ViewHolderTextType extends BaseViewHolder {
		public final TextView questionNo;
		public final TextView questionText;
		public final TextView userOptionText;
		public final TextView matchOptionText;
		public final ImageView userImage;
		public final ImageView matchImage;
		public final LinearLayout userOptionTextViewContainer;
		public final LinearLayout matchOptionTextViewContainer;

		public ViewHolderTextType(View itemView) {
			super(itemView);

			questionNo = (TextView) itemView.findViewById(R.id.question_no);
			questionText = (TextView) itemView.findViewById(R.id.question_text);
			userOptionText = (TextView) itemView
					.findViewById(R.id.user_option_text);
			userOptionTextViewContainer = (LinearLayout) itemView
					.findViewById(R.id.user_option_text_view_container);
			matchOptionTextViewContainer = (LinearLayout) itemView
					.findViewById(R.id.match_option_text_view_container);

			matchOptionText = (TextView) itemView
					.findViewById(R.id.match_option_text);

			if (Utility.isMale(aActivity)) {
				userOptionText.setTextColor(aActivity.getResources().getColor(
						R.color.quiz_answer_blue));
				UiUtils.setBackground(aActivity, userOptionTextViewContainer,
                        R.drawable.border_blue);
				matchOptionText.setTextColor(aActivity.getResources().getColor(
						R.color.quiz_answer_pink));
				UiUtils.setBackground(aActivity, matchOptionTextViewContainer,
                        R.drawable.border_pink);

			} else {
				userOptionText.setTextColor(aActivity.getResources().getColor(
						R.color.quiz_answer_pink));
				UiUtils.setBackground(aActivity, userOptionTextViewContainer,
                        R.drawable.border_pink);
				
				matchOptionText.setTextColor(aActivity.getResources().getColor(
						R.color.quiz_answer_blue));
				UiUtils.setBackground(aActivity, matchOptionTextViewContainer,
                        R.drawable.border_blue);

			}

			userImage = (ImageView) itemView.findViewById(R.id.user_image);
			matchImage = (ImageView) itemView.findViewById(R.id.match_image);

		}
	}

}
