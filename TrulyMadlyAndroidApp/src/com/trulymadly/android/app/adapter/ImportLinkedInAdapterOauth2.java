/**
 *
 */
package com.trulymadly.android.app.adapter;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.listener.CustomOkHttpResponseHandler;
import com.trulymadly.android.app.listener.OnImportFromLinkedin;
import com.trulymadly.android.app.utility.OkHttpHandler;
import com.trulymadly.android.app.utility.TmLogger;
import com.trulymadly.android.app.utility.Utility;
import com.trulymadly.android.app.utility.WebviewHandler;

import org.json.JSONObject;

import java.util.HashMap;

/**
 * @author udbhav
 */
public class ImportLinkedInAdapterOauth2 implements WebviewHandler.WebviewActionsListener {

    /*CONSTANT FOR THE AUTHORIZATION PROCESS*/

    private static final String SOURCE = "LINKEDIN_OAuth2_SDK";
    //This is the public api key of our application
    private static final String API_KEY = Constants.LINKEDIN_CONSUMER_KEY;
    //This is the private api key of our application
    //private static final String SECRET_KEY = Constants.LINKEDIN_CONSUMER_SECRET_LIVE;
    //This is any string we want to use. This will be used for avoid CSRF attacks. You can generate one here: http://strongpasswordgenerator.com/
    private static final String STATE = "n398kk0Unfi1T2t";
    //This is the url that LinkedIn Auth process will redirect to. We can put whatever we want that starts with http:// or https:// .
    //We use a made up url that we will intercept when redirecting. Avoid Uppercases.
    private static final String REDIRECT_URI = "http://www.trulymadly.com";
    private static final String SCOPE = Constants.LINKEDIN_SCOPE;
    /*********************************************/

    //These are constants used for build the urls
    private static final String AUTHORIZATION_URL = "https://www.linkedin.com/uas/oauth2/authorization";
    private static final String ACCESS_TOKEN_URL = "https://www.linkedin.com/uas/oauth2/accessToken";
    private static final String SECRET_KEY_PARAM = "client_secret";
    private static final String RESPONSE_TYPE_PARAM = "response_type";
    private static final String GRANT_TYPE_PARAM = "grant_type";
    private static final String GRANT_TYPE = "authorization_code";
    private static final String RESPONSE_TYPE_VALUE = "code";
    private static final String CLIENT_ID_PARAM = "client_id";
    private static final String STATE_PARAM = "state";
    private static final String REDIRECT_URI_PARAM = "redirect_uri";
    private static final String SCOPE_PARAM = "scope";
    /*---------------------------------------*/
    private static final String QUESTION_MARK = "?";
    private static final String AMPERSAND = "&";
    private static final String EQUALS = "=";
    private static final String TAG = SOURCE;
    private final Context aContext;
    private final View mWebviewIncludeView;
    private final OnImportFromLinkedin onImportFromLinkedin;
    private WebviewHandler mWebviewHandler;


    public ImportLinkedInAdapterOauth2(Context aContext,
                                       OnImportFromLinkedin onImportFromLinkedin, View mWebviewIncludeView) {
        super();
        this.aContext = aContext;
        this.onImportFromLinkedin = onImportFromLinkedin;
        this.mWebviewIncludeView = mWebviewIncludeView;
    }

    private static String getAuthorizationUrl() {
        return AUTHORIZATION_URL
                + QUESTION_MARK + RESPONSE_TYPE_PARAM + EQUALS + RESPONSE_TYPE_VALUE
                + AMPERSAND + CLIENT_ID_PARAM + EQUALS + API_KEY
                + AMPERSAND + STATE_PARAM + EQUALS + STATE
                + AMPERSAND + REDIRECT_URI_PARAM + EQUALS + REDIRECT_URI
                + AMPERSAND + SCOPE_PARAM + EQUALS + SCOPE;
    }

    public void importFromLinkedIn() {
        mWebviewHandler = new WebviewHandler(mWebviewIncludeView, this, true, true);
        //Get the authorization Url
        String authUrl = getAuthorizationUrl();
        TmLogger.i(TAG, "Loading Auth Url: " + authUrl);
        //Load the authorization URL into the webView
        mWebviewHandler.loadUrl(authUrl);
        //ActivityHandler.startLinkedInOauthForResult(aActivity);
    }

    private void sendAuthCodeToServer(String authorizationToken) {
        CustomOkHttpResponseHandler responseHandler = new CustomOkHttpResponseHandler(aContext) {
            @Override
            public void onRequestSuccess(JSONObject responseJSON) {

                switch (responseJSON.optInt("responseCode")) {
                    case 200:
                        String connections = responseJSON.optString("connections");
                        onImportFromLinkedin.onSuccessfullVerification(SOURCE, connections, responseJSON.optString("profile_pic"));
                        break;
                    case 403:
                        onImportFromLinkedin.onLiFailure(SOURCE, responseJSON.optString("error", "Unable to connect with LinkedIn"));
                        break;
                    default:
                        onImportFromLinkedin.onFailure(SOURCE, responseJSON.optString("error", "Unable to connect with LinkedIn"));
                        break;

                }
            }

            @Override
            public void onRequestFailure(Exception exception) {
                onImportFromLinkedin.onLiNetworkError(SOURCE, exception);

            }
        };


        HashMap<String, String> params = new HashMap<>();
        params.put("auth_code", authorizationToken);
        OkHttpHandler.httpPost(aContext, ConstantsUrls.get_linkedinOauthUrl(), params, responseHandler);
    }

    @Override
    public boolean shouldOverrideUrlLoading(String authorizationUrl) {
        if (authorizationUrl.startsWith(REDIRECT_URI)) {
            Log.i(TAG, "");
            Uri uri = Uri.parse(authorizationUrl);
            //We take from the url the authorizationToken and the state token. We have to check that the state token returned by the Service is the same we sent.
            //If not, that means the request may be a result of CSRF and must be rejected.
            String stateToken = uri.getQueryParameter(STATE_PARAM);
            if (stateToken == null || !stateToken.equals(STATE)) {
                Log.e(TAG, "State token doesn't match");
                String error = uri.getQueryParameter("error") + " : " + uri.getQueryParameter("error_description");
                Log.e(TAG, error);
                new AuthTokenReturnTask(null, error, false).execute();
                return true;
            }

            //If the user doesn't allow authorization to our application, the authorizationToken Will be null.
            String authorizationToken = uri.getQueryParameter(RESPONSE_TYPE_VALUE);
            if (authorizationToken == null) {
                Log.i(TAG, "The user doesn't allow authorization.");
                new AuthTokenReturnTask(null, "The user doesn't allow authorization.", true).execute();
                return true;
            }
            Log.i(TAG, "Auth token received: " + authorizationToken);

            //Generate URL for requesting Access Token
            //String accessTokenUrl = getAccessTokenUrl(authorizationToken);
            //We make the request in a AsyncTask
            //new PostRequestAsyncTask().execute(accessTokenUrl);
            new AuthTokenReturnTask(authorizationToken, "Authorization Successfull", false).execute();

        } else {
            //Default behaviour
            Log.i(TAG, "Redirecting to: " + authorizationUrl);
            mWebviewHandler.loadUrl(authorizationUrl);
        }
        return true;
    }

    @Override
    public void webViewHiddenOnUrlLoad() {

    }

    @Override
    public void onWebViewCloseClicked() {
        onImportFromLinkedin.onCancel(SOURCE);
    }

    public boolean isWebViewVisible() {
        return mWebviewHandler != null && mWebviewHandler.isVisible();
    }

    public void hide() {
        if (mWebviewHandler != null) {
            mWebviewHandler.hide();
            onImportFromLinkedin.onCancel(SOURCE);
        }
    }

    private class AuthTokenReturnTask extends AsyncTask<Void, Void, Void> {
        private final String authorizationToken;
        private final String message;
        private final boolean isCancel;

        public AuthTokenReturnTask(String authorizationToken, String message, boolean isCancel) {
            this.authorizationToken = authorizationToken;
            this.message = message;
            this.isCancel = isCancel;
        }

        @Override
        protected Void doInBackground(Void... aVoid) {
            //Do nothing in background
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            mWebviewHandler.hide();
            if (Utility.isSet(message)) {
                TmLogger.d(TAG, message);
            }
            if (Utility.isSet(authorizationToken)) {
                onImportFromLinkedin.onProgress(SOURCE);
                sendAuthCodeToServer(authorizationToken);
            } else if (isCancel) {
                onImportFromLinkedin.onCancel(SOURCE);
            } else if (Utility.isSet(message)) {
                onImportFromLinkedin.onFailure(SOURCE, message);
            } else {
                onImportFromLinkedin.onFailure(SOURCE, "Could not getBool authorizationToken");
            }
        }
    }
}
