package com.trulymadly.android.app.adapter;

import android.content.Context;
import android.graphics.Bitmap.Config;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.cesards.cropimageview.CropImageView;
import com.cesards.cropimageview.CropImageView.CropType;
import com.longtailvideo.jwplayer.JWPlayerView;
import com.longtailvideo.jwplayer.configuration.PlayerConfig;
import com.longtailvideo.jwplayer.core.PlayerState;
import com.longtailvideo.jwplayer.events.listeners.VideoPlayerEvents;
import com.longtailvideo.jwplayer.media.playlists.PlaylistItem;
import com.squareup.picasso.Callback.EmptyCallback;
import com.squareup.picasso.Picasso;
import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.bus.EventInfoUpdateEvent;
import com.trulymadly.android.app.bus.NetworkChangeEvent;
import com.trulymadly.android.app.custom.TMJWPlayerView;
import com.trulymadly.android.app.listener.ActivityLifecycleListener;
import com.trulymadly.android.app.listener.NetworkChangedListener;
import com.trulymadly.android.app.listener.OnDataLoadedInterface;
import com.trulymadly.android.app.listener.VideoControlListener;
import com.trulymadly.android.app.modal.ProfileViewPagerModal;
import com.trulymadly.android.app.utility.ImageCacheHelper;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;

import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by avin on 11/08/16.
 */
public class MatchesPagesViewPagerAdapter2 extends PagerAdapter implements VideoControlListener,
        ViewPager.OnPageChangeListener, ActivityLifecycleListener, NetworkChangedListener {

    private static final String TAG = "MatchesVPagerAdapter2";

    private final Context mContext;
    private final LayoutInflater mLayoutInflater;
    private final boolean isMyProfile, isMutualMatch;
    private ProfileViewPagerModal[] mProfileViewPagerModals;
    private OnClickListener listener, firstPicListener;
    private boolean isDateSpot;
    private String key;
    private HashMap<Integer, JWPlayerView> mJWPlayersMap;
    private HashMap<Integer, View> mLoaderViewsMap, mPlayViewMap;
    private HashMap<Integer, ViewGroup> mContainerViewMap;
    private HashMap<Integer, Boolean> mVideoMuteMap, mVideoSeenMap;
    private int mCurrentPosition = 0;
    private Animation mScaleRepeatAnimation;
    private HashMap<Integer, View> mCircleImageViewsMap;
    private int mMuteMarginRight;
    private String mMatchId;
    private HashMap<Integer, Long> mDurationMap;
    private HashMap<Integer, Integer> mPlayCountMap;
    private HashMap<Integer, Boolean> mPlaySuccessTrackingSentTracker;
    private int mToolbarheight;
    private boolean hasVideoProfileAdded = false;
    private boolean isSelectProfile;

    public MatchesPagesViewPagerAdapter2(Context context, Boolean isMyProfile,
                                         boolean isMutualMatch) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.isMyProfile = isMyProfile;
        this.isMutualMatch = isMutualMatch;
        init();
    }

    public MatchesPagesViewPagerAdapter2(Context context, Boolean isMyProfile,
                                         boolean isMutualMatch,
                                         Boolean isDateSpot, String key) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.isMyProfile = isMyProfile;
        this.isDateSpot = isDateSpot;
        this.key = key;
        this.isMutualMatch = isMutualMatch;

        init();
    }

    private void init() {
        mJWPlayersMap = new HashMap<>();
        mVideoMuteMap = new HashMap<>();
        mVideoSeenMap = new HashMap<>();
        mLoaderViewsMap = new HashMap<>();
        mPlayViewMap = new HashMap<>();
        mContainerViewMap = new HashMap<>();
        mCircleImageViewsMap = new HashMap<>();
        mDurationMap = new HashMap<>();
        mPlayCountMap = new HashMap<>();
        mPlaySuccessTrackingSentTracker = new HashMap<>();
        mMuteMarginRight = UiUtils.dpToPx(50);

        mScaleRepeatAnimation = AnimationUtils.loadAnimation(mContext, R.anim.scale_animation_repeat_80);
        mScaleRepeatAnimation.setRepeatMode(Animation.REVERSE);
        mScaleRepeatAnimation.setRepeatCount(Animation.INFINITE);

        mToolbarheight = mContext.getResources().getDimensionPixelSize(R.dimen.toolbar_height);
    }

    //Datestop sends its own key
    public void setKey(String key) {
        this.key = key;
    }

    public void changeList(ProfileViewPagerModal[] profileViewPagerModals, String matchId) {
//        this.mProfileViewPagerModals = profileViewPagerModals;
//        if (mMatchId != null && matchId != null && !mMatchId.equals(matchId)) {
//            mVideoSeenMap.clear();
//            mPlaySuccessTrackingSentTracker.clear();
//        }
//        this.mMatchId = matchId;
//        this.hasVideoProfileAdded = false;
//        if (profileViewPagerModals != null) {
//            for (ProfileViewPagerModal profileViewPagerModal : profileViewPagerModals) {
//                if (profileViewPagerModal.isVideo()) {
//                    this.hasVideoProfileAdded = true;
//                    break;
//                }
//            }
//        }
        changeList(profileViewPagerModals, matchId, false);
    }

    public void changeList(ProfileViewPagerModal[] profileViewPagerModals, String matchId,
                           boolean isSelectProfile) {
        this.mProfileViewPagerModals = profileViewPagerModals;
        if (mMatchId != null && matchId != null && !mMatchId.equals(matchId)) {
            mVideoSeenMap.clear();
            mPlaySuccessTrackingSentTracker.clear();
        }
        this.mMatchId = matchId;
        this.isSelectProfile = isSelectProfile;
        this.hasVideoProfileAdded = false;
        if (profileViewPagerModals != null) {
            for (ProfileViewPagerModal profileViewPagerModal : profileViewPagerModals) {
                if (profileViewPagerModal.isVideo()) {
                    this.hasVideoProfileAdded = true;
                    break;
                }
            }
        }
    }

    public void setListener(OnClickListener listener, OnClickListener firstPicListener) {
        this.listener = listener;
        this.firstPicListener = firstPicListener;
    }

    public int getCount() {
        if (mProfileViewPagerModals == null)
            return 0;
        return mProfileViewPagerModals.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        final ProfileViewPagerModal profileViewPagerModal = mProfileViewPagerModals[position];

        View itemView = mLayoutInflater.inflate(R.layout.matches_page_view_pager_item, container, false);
        itemView.setTag(position);
        if (position == 0) {
            itemView.setOnClickListener(firstPicListener);
        } else {
            itemView.setOnClickListener(listener);
        }
        final CropImageView matches_page_view_pager_view = (CropImageView) itemView
                .findViewById(R.id.matches_page_view_pager_view);
        final View add_profile_video_on_photo_tv = itemView.findViewById(R.id.add_profile_video_on_photo);
        final ProgressBar custom_prog_bar_photo_id = (ProgressBar) itemView.findViewById(R.id.custom_prog_bar_photo_id);
        final View play_button_view = itemView.findViewById(R.id.play_button_view);
        final String sourceUrl = profileViewPagerModal.getmSource();
        final ImageView loader_iv = (ImageView) itemView.findViewById(R.id.loader_iv);
        Picasso.with(mContext).load(R.drawable.video).into(loader_iv);
        ImageView circle_iv = (ImageView) itemView.findViewById(R.id.circle_iv);
        circle_iv.setImageResource(R.drawable.white_circle_loader);
        final ImageView mute_iv = (ImageView) itemView.findViewById(R.id.toggle_mute_iv);
        mute_iv.setVisibility((profileViewPagerModal.isMuted()) ? View.GONE : View.VISIBLE);
//        mute_iv.setVisibility(View.VISIBLE);
//        mute_iv.setImageResource(R.drawable.mute_video_icon);
//        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mute_iv.getLayoutParams();
//        if (!isMyProfile && !isMutualMatch) {
//            layoutParams.topMargin = layoutParams.topMargin + mToolbarheight;
//            mute_iv.setLayoutParams(layoutParams);
//        }
//        if(isSelectProfile || true){
//            RelativeLayout.LayoutParams selectLayoutParams = (RelativeLayout.LayoutParams) mute_iv.getLayoutParams();
//            selectLayoutParams.topMargin = selectLayoutParams.topMargin
//                    + mContext.getResources().getDimensionPixelSize(R.dimen.select_tag_height)
//                    + UiUtils.dpToPx(8);
//            mute_iv.setLayoutParams(selectLayoutParams);
//        }
//        mute_iv.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//            }
//        });

        ViewGroup playerContainer = (ViewGroup) itemView.findViewById(R.id.playerview_container);
        mContainerViewMap.put(position, playerContainer);
        if (isMyProfile) {
            RelativeLayout.LayoutParams layoutParams1 = (RelativeLayout.LayoutParams) mute_iv.getLayoutParams();
            layoutParams1.rightMargin = mMuteMarginRight;
            mute_iv.setLayoutParams(layoutParams1);
        }

        boolean isVideo = false;

        add_profile_video_on_photo_tv.setVisibility(View.GONE);

        //My Profile : ProfileNew.java
        if (isMyProfile) {
            if (!profileViewPagerModal.isVideo()) {
                ImageCacheHelper.with(mContext).loadWithKey(sourceUrl, Utility.getMyId(mContext))
                        .into(matches_page_view_pager_view, new OnDataLoadedInterface() {
                            @Override
                            public void onLoadSuccess(long mTimeTaken) {
                                custom_prog_bar_photo_id.setVisibility(View.GONE);
                                matches_page_view_pager_view.setCropType(CropType.CENTER_TOP);
                            }

                            @Override
                            public void onLoadFailure(Exception e, long mTimeTaken) {
                                custom_prog_bar_photo_id.setVisibility(View.GONE);

                            }
                        });
                if (position == 0 && !hasVideoProfileAdded) {
                    add_profile_video_on_photo_tv.setVisibility(View.VISIBLE);
                }
            } else {
                isVideo = true;
            }

        }
        //Datespots : DatespotList.java
        else if (isDateSpot) {
            if (position == 0) {
                if (!profileViewPagerModal.isVideo()) {
                    ImageCacheHelper.with(mContext).loadWithKey(sourceUrl, key)
                            .into(matches_page_view_pager_view, new OnDataLoadedInterface() {
                                @Override
                                public void onLoadSuccess(long mTimeTaken) {
                                    custom_prog_bar_photo_id.setVisibility(View.GONE);
                                    matches_page_view_pager_view.setCropType(CropType.CENTER_TOP);
                                }

                                @Override
                                public void onLoadFailure(Exception e, long mTimeTaken) {
                                    custom_prog_bar_photo_id.setVisibility(View.GONE);

                                }
                            });
                }
            } else {
                if (!profileViewPagerModal.isVideo()) {
                    Picasso.with(mContext).load(sourceUrl).config(Config.RGB_565)
                            .into(matches_page_view_pager_view, new EmptyCallback() {
                                @Override
                                public void onSuccess() {
                                    custom_prog_bar_photo_id.setVisibility(View.GONE);
                                    matches_page_view_pager_view.setCropType(CropType.CENTER_TOP);
                                }

                                @Override
                                public void onError() {
                                    loadFromUrl(sourceUrl, matches_page_view_pager_view,
                                            custom_prog_bar_photo_id, true);
                                }
                            });
                }
            }
        }
        //Matches : MutualMatches ProfileNew.java as well as Matches (MatchesPageLatest2.java)
        else {
            if (Utility.isSet(sourceUrl)) {
                if (!profileViewPagerModal.isVideo()) {
                    Picasso.with(mContext).load(sourceUrl).config(Config.RGB_565)
                            .into(matches_page_view_pager_view, new EmptyCallback() {
                                @Override
                                public void onSuccess() {
                                    custom_prog_bar_photo_id.setVisibility(View.GONE);
                                    matches_page_view_pager_view.setCropType(CropType.CENTER_TOP);
                                }

                                @Override
                                public void onError() {
                                    loadFromUrl(sourceUrl, matches_page_view_pager_view,
                                            custom_prog_bar_photo_id, true);
                                }
                            });
                } else {
                    isVideo = true;
                }
            } else {
                custom_prog_bar_photo_id.setVisibility(View.GONE);
            }
        }

        if (isVideo) {
            if (!isMyProfile && !isMutualMatch) {
                RelativeLayout.LayoutParams matchLayoutParams = (RelativeLayout.LayoutParams) mute_iv.getLayoutParams();
                matchLayoutParams.topMargin = matchLayoutParams.topMargin + mToolbarheight;
                mute_iv.setLayoutParams(matchLayoutParams);
            }

            if (isSelectProfile) {
                RelativeLayout.LayoutParams selectLayoutParams = (RelativeLayout.LayoutParams) mute_iv.getLayoutParams();
                selectLayoutParams.topMargin = selectLayoutParams.topMargin
                        + mContext.getResources().getDimensionPixelSize(R.dimen.select_tag_height)
                        + UiUtils.dpToPx(8);
                mute_iv.setLayoutParams(selectLayoutParams);
            }

            PlayerConfig playerConfig = new PlayerConfig.Builder()
                    .build();
            TMJWPlayerView jwPlayerView = new TMJWPlayerView(mContext, playerConfig);
            mJWPlayersMap.put(position, jwPlayerView);
            mPlayViewMap.put(position, play_button_view);
            togglePlayerPlayButton(position, isPositionVideoSeen(position));
            mContainerViewMap.get(position).addView(jwPlayerView);
            mCircleImageViewsMap.put(position, itemView.findViewById(R.id.circle_iv));
            mLoaderViewsMap.put(position, itemView.findViewById(R.id.loader_view));
            Picasso.with(mContext).load(profileViewPagerModal.getmThumbnailUrl()).config(Config.RGB_565)
                    .into(matches_page_view_pager_view, new EmptyCallback() {
                        @Override
                        public void onSuccess() {
                            custom_prog_bar_photo_id.setVisibility(View.GONE);
                            matches_page_view_pager_view.setCropType(CropType.NONE);

                            HashMap<String, String> eventInfo = new HashMap<>();
                            eventInfo.put("video_length_ms", String.valueOf(profileViewPagerModal.getmDuration() * 1000));
                            eventInfo.put("photos", String.valueOf(getCount()));
                            eventInfo.put("position", String.valueOf(position));
                            if (Utility.isSet(profileViewPagerModal.getmId())) {
                                eventInfo.put("video_id", profileViewPagerModal.getmId());
                            }
                            if (!isMyProfile && Utility.isSet(mMatchId)) {
                                eventInfo.put("user_id", String.valueOf(mMatchId));
                            }

                            TrulyMadlyTrackEvent.trackEvent(mContext,
                                    (isMyProfile) ? TrulyMadlyEvent.TrulyMadlyActivities.my_profile :
                                            (isMutualMatch) ? TrulyMadlyEvent.TrulyMadlyActivities.profile :
                                                    TrulyMadlyEvent.TrulyMadlyActivities.matches,
                                    TrulyMadlyEvent.TrulyMadlyEventTypes.download_video_thumbnail, 0,
                                    TrulyMadlyEvent.TrulyMadlyEventStatus.success, eventInfo, true);
                        }

                        @Override
                        public void onError() {
                            loadFromUrl(profileViewPagerModal.getmThumbnailUrl(),
                                    matches_page_view_pager_view, custom_prog_bar_photo_id, false);

                            HashMap<String, String> eventInfo = new HashMap<>();
                            eventInfo.put("video_length_ms", String.valueOf(profileViewPagerModal.getmDuration() * 1000));
                            eventInfo.put("photos", String.valueOf(getCount()));
                            eventInfo.put("position", String.valueOf(position));
                            if (Utility.isSet(profileViewPagerModal.getmId())) {
                                eventInfo.put("video_id", profileViewPagerModal.getmId());
                            }
                            if (!isMyProfile && Utility.isSet(mMatchId)) {
                                eventInfo.put("user_id", String.valueOf(mMatchId));
                            }

                            TrulyMadlyTrackEvent.trackEvent(mContext,
                                    (isMyProfile) ? TrulyMadlyEvent.TrulyMadlyActivities.my_profile :
                                            (isMutualMatch) ? TrulyMadlyEvent.TrulyMadlyActivities.profile :
                                                    TrulyMadlyEvent.TrulyMadlyActivities.matches,
                                    TrulyMadlyEvent.TrulyMadlyEventTypes.download_video_thumbnail, 0,
                                    TrulyMadlyEvent.TrulyMadlyEventStatus.fail, eventInfo, true);
                        }
                    });


            PlaylistItem item = new PlaylistItem.Builder()
                    .file(sourceUrl)
                    .build();
            mJWPlayersMap.get(position).setControls(false);
            mJWPlayersMap.get(position).setFullscreenHandler(null);

            mJWPlayersMap.get(position).getConfig().setRepeat(true);
            mJWPlayersMap.get(position).load(item);

            View playerViewLayer = itemView.findViewById(R.id.player_view_layer);
            playerViewLayer.setTag(position);
            playerViewLayer.setVisibility(View.VISIBLE);
            playerViewLayer.setOnClickListener(listener);

            if (!profileViewPagerModal.isMuted()) {
                //By default, videos are muted
                //So, if the muteMap doesn't contain this position
                //OR if it contains it and its true
                //We mute the player :: setMute(true);
                if (!mVideoMuteMap.containsKey(position) ||
                        mVideoMuteMap.get(position) == null || mVideoMuteMap.get(position)) {
                    mute_iv.setImageResource(R.drawable.mute_video_icon);
                    mJWPlayersMap.get(position).setMute(true);
                } else {
                    mute_iv.setImageResource(R.drawable.unmute_video_icon);
                    mJWPlayersMap.get(position).setMute(false);
                }
            } else {
                mJWPlayersMap.get(position).setMute(true);
            }

            mJWPlayersMap.get(position).addOnBufferListener(new VideoPlayerEvents.OnBufferListener() {
                @Override
                public void onBuffer(PlayerState playerState) {
                    Log.d(TAG, position + "::onBuffer : PlayerState " + playerState);
                    if (mCurrentPosition == position) {
                        togglePlayerPlayButton(position, false);
                        togglePlayerLoader(position, true);
                    } else {
                        pause(position);
                    }
                }
            });

            mJWPlayersMap.get(position).addOnPauseListener(new VideoPlayerEvents.OnPauseListener() {
                @Override
                public void onPause(PlayerState playerState) {
                    Log.d(TAG, position + "::onPause : PlayerState " + playerState);
                    if (position != mCurrentPosition) {
                        View view = mLoaderViewsMap.get(position);
                        if (view != null) {
                            view.setVisibility(View.GONE);
                        }
                    }
                }
            });

            mJWPlayersMap.get(position).addOnFirstFrameListener(new VideoPlayerEvents.OnFirstFrameListener() {

                @Override
                public void onFirstFrame(int i) {
                    Log.d(TAG, position + "::addOnFirstFrameListener");
                    JWPlayerView jwPlayerView = mJWPlayersMap.get(position);
                    if (mCurrentPosition == position) {
                    } else {
                        if (jwPlayerView != null) {
                            jwPlayerView.pause();
                        }
                    }
                }
            });

            if (!profileViewPagerModal.isMuted()) {
                mute_iv.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        HashMap<String, String> eventInfo = new HashMap<>();
                        eventInfo.put("video_length_ms", String.valueOf(profileViewPagerModal.getmDuration() * 1000));
                        eventInfo.put("photos", String.valueOf(getCount()));
                        eventInfo.put("muted", String.valueOf(profileViewPagerModal.isMuted()));
                        eventInfo.put("position", String.valueOf(position));
                        if (Utility.isSet(profileViewPagerModal.getmId())) {
                            eventInfo.put("video_id", profileViewPagerModal.getmId());
                        }
                        if (!isMyProfile && Utility.isSet(mMatchId)) {
                            eventInfo.put("user_id", String.valueOf(mMatchId));
                        }

                        JWPlayerView jwPlayerView = mJWPlayersMap.get(position);

                        //By default, videos are muted
                        //So, if the muteMap doesn't contain this position
                        //OR if it contains it and its true
                        //PREV_STATE : MUTE
                        //NEW STATE : We unmute the player :: setMute(false);

                        if (!mVideoMuteMap.containsKey(position) ||
                                mVideoMuteMap.get(position) == null || mVideoMuteMap.get(position)) {
                            mVideoMuteMap.put(position, false);

                            if (jwPlayerView != null) {
                                jwPlayerView.setMute(false);
                            }
                            mute_iv.setImageResource(R.drawable.unmute_video_icon);

                            TrulyMadlyTrackEvent.trackEvent(mContext,
                                    (isMyProfile) ? TrulyMadlyEvent.TrulyMadlyActivities.my_profile :
                                            (isMutualMatch) ? TrulyMadlyEvent.TrulyMadlyActivities.profile :
                                                    TrulyMadlyEvent.TrulyMadlyActivities.matches,
                                    TrulyMadlyEvent.TrulyMadlyEventTypes.video_unmute, 0,
                                    TrulyMadlyEvent.TrulyMadlyEventStatus.success, eventInfo, true);

                        } else {
                            mVideoMuteMap.put(position, true);

                            if (jwPlayerView != null) {
                                jwPlayerView.setMute(true);
                            }

                            mute_iv.setImageResource(R.drawable.mute_video_icon);

                            TrulyMadlyTrackEvent.trackEvent(mContext,
                                    (isMyProfile) ? TrulyMadlyEvent.TrulyMadlyActivities.my_profile :
                                            (isMutualMatch) ? TrulyMadlyEvent.TrulyMadlyActivities.profile :
                                                    TrulyMadlyEvent.TrulyMadlyActivities.matches,
                                    TrulyMadlyEvent.TrulyMadlyEventTypes.video_mute, 0,
                                    TrulyMadlyEvent.TrulyMadlyEventStatus.success, eventInfo, true);


                        }
                    }
                });
            }

            mJWPlayersMap.get(position).addOnPlayListener(new VideoPlayerEvents.OnPlayListener() {
                @Override
                public void onPlay(PlayerState playerState) {
                    Log.d(TAG, position + "::onPlay : PlayerState " + playerState);
                    JWPlayerView jwPlayerView = mJWPlayersMap.get(position);
                    if (mCurrentPosition == position) {
                        togglePlayer(position, true);
                        togglePlayerPlayButton(position, false);
                        if (jwPlayerView != null) {
                            Utility.fireBusEvent(mContext, true, new EventInfoUpdateEvent(
                                    EventInfoUpdateEvent.KEY_VIDEOS_VIEWED, "true"));
                        }

                        togglePlayerLoader(position, false);

                        Long startedTime = mDurationMap.get(position);
                        int seconds = -1;
                        if (startedTime != null) {
                            long currentTime = Calendar.getInstance().getTimeInMillis();
                            long diff = currentTime - startedTime;
                            seconds = (int) (diff / 1000);
                            mDurationMap.remove(position);
                        }

                        if (mPlaySuccessTrackingSentTracker.get(position) == null ||
                                !mPlaySuccessTrackingSentTracker.get(position)) {
                            mPlaySuccessTrackingSentTracker.put(position, true);
                            HashMap<String, String> eventInfo = new HashMap<>();
                            eventInfo.put("video_length_ms", String.valueOf(profileViewPagerModal.getmDuration() * 1000));
                            eventInfo.put("photos", String.valueOf(getCount()));
                            eventInfo.put("muted", String.valueOf(profileViewPagerModal.isMuted()));
                            eventInfo.put("position", String.valueOf(position));
                            if (!isMyProfile && seconds > 0) {
                                eventInfo.put("time_taken", String.valueOf(seconds));
                            }
                            if (!isMyProfile && Utility.isSet(mMatchId)) {
                                eventInfo.put("user_id", String.valueOf(mMatchId));
                            }
                            if (Utility.isSet(profileViewPagerModal.getmId())) {
                                eventInfo.put("video_id", profileViewPagerModal.getmId());
                            }

                            TrulyMadlyTrackEvent.trackEvent(mContext,
                                    (isMyProfile) ? TrulyMadlyEvent.TrulyMadlyActivities.my_profile :
                                            (isMutualMatch) ? TrulyMadlyEvent.TrulyMadlyActivities.profile :
                                                    TrulyMadlyEvent.TrulyMadlyActivities.matches,
                                    TrulyMadlyEvent.TrulyMadlyEventTypes.video_previewed, 0,
                                    TrulyMadlyEvent.TrulyMadlyEventStatus.success, eventInfo, true);
                        }

                    } else {
                        if (jwPlayerView != null) {
                            jwPlayerView.pause();
                        }
                    }
                }
            });

            mPlayViewMap.get(position).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d(TAG, position + "::Play clicked");
                    togglePlayerPlayButton(position, false);
                    play(position);

                    HashMap<String, String> eventInfo = new HashMap<>();
                    eventInfo.put("video_length_ms", String.valueOf(profileViewPagerModal.getmDuration() * 1000));
                    eventInfo.put("photos", String.valueOf(getCount()));
                    eventInfo.put("position", String.valueOf(position));
                    if (Utility.isSet(profileViewPagerModal.getmId())) {
                        eventInfo.put("video_id", profileViewPagerModal.getmId());
                    }
                    if (!isMyProfile && Utility.isSet(mMatchId)) {
                        eventInfo.put("user_id", String.valueOf(mMatchId));
                    }

                    TrulyMadlyTrackEvent.trackEvent(mContext,
                            (isMyProfile) ? TrulyMadlyEvent.TrulyMadlyActivities.my_profile :
                                    (isMutualMatch) ? TrulyMadlyEvent.TrulyMadlyActivities.profile :
                                            TrulyMadlyEvent.TrulyMadlyActivities.matches,
                            TrulyMadlyEvent.TrulyMadlyEventTypes.play_clicked, 0,
                            TrulyMadlyEvent.TrulyMadlyEventStatus.success, eventInfo, true);
                }
            });

            mJWPlayersMap.get(position).addOnCompleteListener(new VideoPlayerEvents.OnCompleteListener() {
                @Override
                public void onComplete() {
                    Log.d(TAG, position + "::onComplete");
                    mVideoSeenMap.put(position, true);
                    Integer currentCount = mPlayCountMap.get(position);
                    if (currentCount == null) {
                        currentCount = 0;
                    }
                    mPlayCountMap.put(position, currentCount + 1);
                    togglePlayer(position, false);
//                    play(position);
                    togglePlayerPlayButton(position, true);

                    HashMap<String, String> eventInfo = new HashMap<>();
                    eventInfo.put("video_length_ms", String.valueOf(profileViewPagerModal.getmDuration() * 1000));
                    eventInfo.put("photos", String.valueOf(getCount()));
                    eventInfo.put("position", String.valueOf(position));
                    if (Utility.isSet(profileViewPagerModal.getmId())) {
                        eventInfo.put("video_id", profileViewPagerModal.getmId());
                    }
                    if (!isMyProfile && Utility.isSet(mMatchId)) {
                        eventInfo.put("user_id", String.valueOf(mMatchId));
                    }

                    TrulyMadlyTrackEvent.trackEvent(mContext,
                            (isMyProfile) ? TrulyMadlyEvent.TrulyMadlyActivities.my_profile :
                                    (isMutualMatch) ? TrulyMadlyEvent.TrulyMadlyActivities.profile :
                                            TrulyMadlyEvent.TrulyMadlyActivities.matches,
                            TrulyMadlyEvent.TrulyMadlyEventTypes.video_completed, 0,
                            TrulyMadlyEvent.TrulyMadlyEventStatus.success, eventInfo, true);
                }
            });

            mJWPlayersMap.get(position).addOnErrorListener(new VideoPlayerEvents.OnErrorListener() {
                @Override
                public void onError(String s) {
                    Log.d(TAG, position + "::onError : Error " + s);
                    Long startedTime = mDurationMap.get(position);
                    int seconds = -1;
                    if (startedTime != null) {
                        long currentTime = Calendar.getInstance().getTimeInMillis();
                        long diff = currentTime - startedTime;
                        seconds = (int) (diff / 1000);
                    }

                    HashMap<String, String> eventInfo = new HashMap<>();
                    eventInfo.put("video_length_ms", String.valueOf(profileViewPagerModal.getmDuration() * 1000));
                    eventInfo.put("photos", String.valueOf(getCount()));
                    eventInfo.put("muted", String.valueOf(profileViewPagerModal.isMuted()));
                    eventInfo.put("position", String.valueOf(position));
                    if (!isMyProfile && seconds > 0) {
                        eventInfo.put("time_taken", String.valueOf(seconds));
                    }
                    if (!isMyProfile && Utility.isSet(mMatchId)) {
                        eventInfo.put("user_id", String.valueOf(mMatchId));
                    }
                    eventInfo.put("error", s);
                    if (Utility.isSet(profileViewPagerModal.getmId())) {
                        eventInfo.put("video_id", profileViewPagerModal.getmId());
                    }

                    TrulyMadlyTrackEvent.trackEvent(mContext,
                            (isMyProfile) ? TrulyMadlyEvent.TrulyMadlyActivities.my_profile :
                                    (isMutualMatch) ? TrulyMadlyEvent.TrulyMadlyActivities.profile :
                                            TrulyMadlyEvent.TrulyMadlyActivities.matches,
                            TrulyMadlyEvent.TrulyMadlyEventTypes.video_previewed, 0,
                            TrulyMadlyEvent.TrulyMadlyEventStatus.fail, eventInfo, true);

                    togglePlayer(position, false);
                    pause(position);

                    if (position == mCurrentPosition) {
                        togglePlayer(mCurrentPosition, false);
                        togglePlayerLoader(position, false);
                        togglePlayerPlayButton(position, true);
                    }

                }

            });

            mJWPlayersMap.get(position).addOnSetupErrorListener(new VideoPlayerEvents.OnSetupErrorListener() {
                @Override
                public void onSetupError(String s) {
                    Log.d(TAG, position + "::onSetupError : Error " + s);
                    Long startedTime = mDurationMap.get(position);
                    int seconds = -1;
                    if (startedTime != null) {
                        long currentTime = Calendar.getInstance().getTimeInMillis();
                        long diff = currentTime - startedTime;
                        seconds = (int) (diff / 1000);
                    }

                    HashMap<String, String> eventInfo = new HashMap<>();
                    eventInfo.put("video_length_ms", String.valueOf(profileViewPagerModal.getmDuration() * 1000));
                    eventInfo.put("photos", String.valueOf(getCount()));
                    eventInfo.put("muted", String.valueOf(profileViewPagerModal.isMuted()));
                    eventInfo.put("position", String.valueOf(position));
                    if (!isMyProfile && seconds > 0) {
                        eventInfo.put("time_taken", String.valueOf(seconds));
                    }
                    if (!isMyProfile && Utility.isSet(mMatchId)) {
                        eventInfo.put("user_id", String.valueOf(mMatchId));
                    }

                    eventInfo.put("error", s);
                    if (Utility.isSet(profileViewPagerModal.getmId())) {
                        eventInfo.put("video_id", profileViewPagerModal.getmId());
                    }

                    TrulyMadlyTrackEvent.trackEvent(mContext,
                            (isMyProfile) ? TrulyMadlyEvent.TrulyMadlyActivities.my_profile :
                                    (isMutualMatch) ? TrulyMadlyEvent.TrulyMadlyActivities.profile :
                                            TrulyMadlyEvent.TrulyMadlyActivities.matches,
                            TrulyMadlyEvent.TrulyMadlyEventTypes.video_previewed, 0,
                            TrulyMadlyEvent.TrulyMadlyEventStatus.fail, eventInfo, true);

                    if (position == mCurrentPosition) {
                        togglePlayer(mCurrentPosition, false);
                        togglePlayerLoader(position, false);
                        togglePlayerPlayButton(position, true);
                    }
                }
            });

        }

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }


    private void loadFromUrl(String url,
                             final CropImageView matches_page_view_pager_view,
                             final ProgressBar custom_prog_bar_photo_id,
                             final boolean cropRequired) {

        TrulyMadlyTrackEvent.trackEvent(mContext, TrulyMadlyEvent.TrulyMadlyActivities.photo_fail,
                TrulyMadlyEvent.TrulyMadlyEventTypes.photo_retry, 0, null, null, false);


        Picasso.with(mContext).load(url).config(Config.RGB_565)
                .into(matches_page_view_pager_view, new EmptyCallback() {
                    @Override
                    public void onSuccess() {
                        custom_prog_bar_photo_id.setVisibility(View.GONE);
                        if (cropRequired) {
                            matches_page_view_pager_view.setCropType(CropType.CENTER_TOP);
                        } else {
                            matches_page_view_pager_view.setCropType(CropType.NONE);
                        }
                    }

                    @Override
                    public void onError() {
                        custom_prog_bar_photo_id.setVisibility(View.GONE);
                    }
                });
    }

    private boolean isPositionVideo(int position) {
        boolean isVideo = true;
        if (mProfileViewPagerModals == null || position < 0 || mProfileViewPagerModals.length <= position ||
                !mProfileViewPagerModals[position].isVideo()) {
            isVideo = false;
        }

        return isVideo;
    }

    private boolean isPositionVideoSeen(int position) {
        boolean isVideoSeen = false;
        if (mVideoSeenMap != null && mVideoSeenMap.containsKey(position)) {
            isVideoSeen = mVideoSeenMap.get(position);
        }

        return isVideoSeen;
    }

    @Override
    public void prePlay(int position) {

    }

    @Override
    public void play(int position) {
        boolean isVideo = isPositionVideo(position);

        if (isVideo) {
            JWPlayerView jwPlayerView = mJWPlayersMap.get(position);
            if (jwPlayerView != null) {
                jwPlayerView.onResume();

                togglePlayerLoader(position, true);

                jwPlayerView.play();
                mDurationMap.put(position, Calendar.getInstance().getTimeInMillis());
            }
        }

    }

    @Override
    public void pause(int position) {
        boolean isVideo = isPositionVideo(position);

        JWPlayerView jwPlayerView = mJWPlayersMap.get(position);
        if (jwPlayerView != null) {
            jwPlayerView.pause();
            jwPlayerView.onPause();
        }

        if (isVideo) {
            togglePlayerLoader(position, false);
            mDurationMap.remove(position);
        }
    }

    @Override
    public void resume(int position) {
        JWPlayerView jwPlayerView = mJWPlayersMap.get(position);
        if (jwPlayerView != null) {
            jwPlayerView.onResume();
        }
    }

    @Override
    public void stop(int position) {
        boolean isVideo = isPositionVideo(position);

        JWPlayerView jwPlayerView = mJWPlayersMap.get(position);
        if (jwPlayerView != null) {
            jwPlayerView.pause();
            jwPlayerView.stop();
            jwPlayerView.onPause();
            togglePlayer(position, false);
        }

        if (isVideo) {
            togglePlayerLoader(position, false);
        }

        mDurationMap.remove(position);
    }

    @Override
    public void destroy(int position) {
        boolean isVideo = isPositionVideo(position);

        JWPlayerView jwPlayerView = mJWPlayersMap.get(position);
        if (jwPlayerView != null) {
            jwPlayerView.pause();
            jwPlayerView.stop();
            jwPlayerView.onPause();
            togglePlayer(position, false);
            mContainerViewMap.get(position).removeView(jwPlayerView);
            jwPlayerView.onDestroy();
            mJWPlayersMap.remove(position);
        }

        if (isVideo) {
            togglePlayerLoader(position, false);
            mLoaderViewsMap.remove(position);
            mCircleImageViewsMap.remove(position);
            mDurationMap.remove(position);
            mPlayViewMap.remove(position);
            mContainerViewMap.remove(position);
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (position != mCurrentPosition) {
            trackCloseVideo(mCurrentPosition);
        }

        mCurrentPosition = position;
        if (!isPositionVideoSeen(position)) {
            play(position);
        }
        togglePlayerPlayButton(position, isPositionVideoSeen(position));
        pause(position - 1);
        destroy(position - 2);
        pause(position + 1);
        destroy(position + 2);
    }

    private void trackCloseVideo(int position) {
        if (position < 0 || position >= getCount() || mProfileViewPagerModals == null ||
                !mProfileViewPagerModals[position].isVideo()) {
            return;
        }

        Long startedTime = mDurationMap.get(position);
        int seconds = -1;
        if (startedTime != null) {
            long currentTime = Calendar.getInstance().getTimeInMillis();
            long diff = currentTime - startedTime;
            seconds = (int) (diff / 1000);
        }

        ProfileViewPagerModal profileViewPagerModal = mProfileViewPagerModals[position];
        HashMap<String, String> eventInfo = new HashMap<>();
        eventInfo.put("video_length_ms", String.valueOf(profileViewPagerModal.getmDuration() * 1000));
        Integer playCount = mPlayCountMap.get(position);
        if (playCount != null) {
            playCount = 0;
        }
        eventInfo.put("play_count", String.valueOf(playCount));
        eventInfo.put("duration", String.valueOf((mJWPlayersMap.get(position) != null) ?
                mJWPlayersMap.get(position).getPosition() : 0));
        eventInfo.put("photos", String.valueOf(getCount()));
        eventInfo.put("muted", String.valueOf(profileViewPagerModal.isMuted()));

        eventInfo.put("position", String.valueOf(position));

        if (!isMyProfile && seconds > 0) {
            eventInfo.put("time_taken", String.valueOf(seconds));
        }
        if (!isMyProfile && Utility.isSet(mMatchId)) {
            eventInfo.put("user_id", String.valueOf(mMatchId));
        }

        if (Utility.isSet(profileViewPagerModal.getmId())) {
            eventInfo.put("video_id", profileViewPagerModal.getmId());
        }

        TrulyMadlyTrackEvent.trackEvent(mContext,
                (isMyProfile) ? TrulyMadlyEvent.TrulyMadlyActivities.my_profile :
                        (isMutualMatch) ? TrulyMadlyEvent.TrulyMadlyActivities.profile :
                                TrulyMadlyEvent.TrulyMadlyActivities.matches,
                TrulyMadlyEvent.TrulyMadlyEventTypes.video_closed, 0,
                TrulyMadlyEvent.TrulyMadlyEventStatus.success, eventInfo, true);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        stop(mCurrentPosition);
    }

    @Override
    public void onPause() {
        trackCloseVideo(mCurrentPosition);
        pause(mCurrentPosition);
        togglePlayerLoader(mCurrentPosition, false);
        togglePlayerPlayButton(mCurrentPosition, true);
    }

    @Override
    public void onResume() {
        resume(mCurrentPosition);
        resume(mCurrentPosition - 1);
        resume(mCurrentPosition + 1);
    }

    @Override
    public void onDestroy() {
        destroy(mCurrentPosition);
        destroy(mCurrentPosition - 1);
        destroy(mCurrentPosition + 1);
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onNetworkChanged(NetworkChangeEvent networkChangeEvent) {
        if (networkChangeEvent != null) {
            if (networkChangeEvent.isConnected()) {
                if (mProfileViewPagerModals != null && mCurrentPosition >= 0 &&
                        mProfileViewPagerModals.length > mCurrentPosition) {
                    ProfileViewPagerModal profileViewPagerModal = mProfileViewPagerModals[mCurrentPosition];
                    if (profileViewPagerModal.isVideo()) {
                        if (!isPositionVideoSeen(mCurrentPosition)) {
                            Log.d(TAG, mCurrentPosition + "::onNetworkChanged");
                            play(mCurrentPosition);
                        } else {
                            togglePlayerLoader(mCurrentPosition, false);
                            togglePlayer(mCurrentPosition, false);
                            togglePlayerPlayButton(mCurrentPosition, true);
                        }
                    }
                }
            }
        }
    }

    private void togglePlayerLoader(int position, boolean makeVisible) {
        View loaderView = mLoaderViewsMap.get(position);
        if (loaderView != null) {
            loaderView.setVisibility(makeVisible ? View.VISIBLE : View.GONE);
        }

        View circleView = mCircleImageViewsMap.get(position);
        if (circleView != null) {
            if (makeVisible) {
                circleView.startAnimation(mScaleRepeatAnimation);
            } else {
                circleView.clearAnimation();
            }
        }
    }

    private void togglePlayerPlayButton(int position, boolean makeVisible) {
        View playView = mPlayViewMap.get(position);
        if (playView != null) {
            playView.setVisibility(makeVisible ? View.VISIBLE : View.GONE);
        }
    }

    private void togglePlayer(int position, boolean makePlayerVisible) {
        ViewGroup jwPlayerViewContainer = mContainerViewMap.get(position);
        JWPlayerView jwPlayerView = mJWPlayersMap.get(position);
        if (jwPlayerView != null) {
            jwPlayerViewContainer.setVisibility(makePlayerVisible ? View.VISIBLE : View.INVISIBLE);
            jwPlayerView.setVisibility(makePlayerVisible ? View.VISIBLE : View.INVISIBLE);
        }
    }

    private void togglePlayer(int position, boolean makePlayerVisible, boolean makeLoaderVisible) {
        ViewGroup jwPlayerViewContainer = mContainerViewMap.get(position);
        JWPlayerView jwPlayerView = mJWPlayersMap.get(position);
        if (jwPlayerView != null) {
            jwPlayerViewContainer.setVisibility(makePlayerVisible ? View.VISIBLE : View.INVISIBLE);
            jwPlayerView.setVisibility(makePlayerVisible ? View.VISIBLE : View.INVISIBLE);
            togglePlayerLoader(position, makeLoaderVisible);
        }
    }

}