package com.trulymadly.android.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.modal.VenueModal;
import com.trulymadly.android.app.utility.ActivityHandler;
import com.trulymadly.android.app.utility.GradientTransformation;
import com.trulymadly.android.app.utility.ImageCacheHelper;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.trulymadly.android.app.activities.CategoriesActivity.SHOW_FOLLOWING_TAB;

/**
 * Created by deveshbatra on 3/8/16.
 */
public class VenueListAdapter extends
        RecyclerView.Adapter<VenueListAdapter.ViewHolderVenueListItem> implements View.OnClickListener {

    private final double screenWidth;

    private final Activity aActivity;
    private final GradientTransformation mCDGradientTransformation;
    private final LayoutInflater inflater;
    private ArrayList<VenueModal> venueList;

    public VenueListAdapter(Activity act, ArrayList<VenueModal> venueList) {
        this.aActivity = act;
        this.venueList = venueList;
        inflater = (LayoutInflater) act
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        screenWidth = UiUtils.getScreenWidth(act);
//        mCDGradientTransformation = new GradientTransformation(new int[]{Color.parseColor("#20444343"),
//                Color.parseColor("#A0444343"), Color.parseColor("#20444343")});

        mCDGradientTransformation = new GradientTransformation(new int[]{
                Color.parseColor("#00000000"), Color.parseColor("#96000000")});

    }

    public ArrayList<VenueModal> getVenueList() {
        return venueList;
    }

    public void setVenueList(ArrayList<VenueModal> venueList) {
        this.venueList = venueList;
    }

    @Override
    public ViewHolderVenueListItem onCreateViewHolder(ViewGroup parent, int viewType) {

        View sView = inflater.inflate(R.layout.venue_list_item, parent,
                false);
        return new ViewHolderVenueListItem(sView);
    }

    @Override
    public void onBindViewHolder(ViewHolderVenueListItem viewHolder, int position) {

        VenueModal venue = venueList.get(position);
        // hardcoded
//        venue.setNewEvent(true);
//        viewHolder.event_is_new_badge.setVisibility(venue.isNewEvent() ? View.VISIBLE : View.GONE);
        viewHolder.event_is_new.setVisibility(venue.isNewEvent() ? View.VISIBLE : View.GONE);
//        viewHolder.event_from_tm.setVisibility(venue.isTMEvent() ? View.VISIBLE : View.GONE);
        if(venue.isNewEvent()){
            Picasso.with(aActivity).load(R.drawable.new_label).noFade().into(viewHolder.event_is_new);
        }

//        if(venue.isTMEvent()){
        String followersCount = venue.getNoOfFollowers();
        if (Utility.isSet(followersCount) && !Utility.stringCompare("0", followersCount) && SHOW_FOLLOWING_TAB) {
            followersCount = followersCount.replaceAll("[^\\d.]", "");
            viewHolder.event_from_tm.setVisibility(View.VISIBLE);
            viewHolder.number_intersted_tv.setText(String.format(aActivity.getString(
                    R.string.num_interested), followersCount));
            viewHolder.number_intersted_tv.setVisibility(View.VISIBLE);
            viewHolder.interested_content.setVisibility(View.VISIBLE);
//            Picasso.with(aActivity).load(R.drawable.tm_presents).noFade().into(viewHolder.event_from_tm);
        } else {
            viewHolder.event_from_tm.setVisibility(View.GONE);
            viewHolder.interested_content.setVisibility(View.GONE);
        }

        viewHolder.itemView.setTag(venue.getId());
        viewHolder.itemView.setOnClickListener(this);
        ImageCacheHelper.with(aActivity).loadWithKey(venue.getBanner(), venue.getId()).transform(mCDGradientTransformation).into(viewHolder.venue_banner);
        if (Utility.isSet(venue.getMultipleLocationText()))
            viewHolder.venue_location.setText(venue.getMultipleLocationText());
        else
            viewHolder.venue_location.setText(venue.getDate() + ", " + venue.getLocation());
        viewHolder.venue_name.setText(venue.getName());

//        createFollowersList(viewHolder, venue.getFollowers(), venue.getNoOfFollowers());
        if (Utility.isSet(venue.getDistance())) {
            viewHolder.distance_container.setVisibility(View.VISIBLE);
            viewHolder.distance_tv.setText(venue.getDistance());
        }

        boolean isTMMessagePresent = Utility.isSet(venue.getmTMMessage());
        viewHolder.message.setVisibility(isTMMessagePresent?View.VISIBLE:View.GONE);
        if(isTMMessagePresent){
            viewHolder.message.setText(venue.getmTMMessage());
        }

        ViewGroup.LayoutParams params = viewHolder.itemView.getLayoutParams();
        params.height = (int) (screenWidth / 2);
        viewHolder.itemView.setLayoutParams(params);
    }

//    private void setFollowerTv(ViewHolderVenueListItem viewHolder, String followersCount) {
//
//        if (!Utility.stringCompare("0", followersCount)) {
//            viewHolder.count_followers_tv.setText(followersCount + " " + aActivity.getResources().getString(R.string.following));
//            viewHolder.count_followers_tv.setVisibility(View.VISIBLE);
//        } else
//            viewHolder.count_followers_tv.setVisibility(View.GONE);
//    }

//    private void createFollowersList(ViewHolderVenueListItem viewHolder, ArrayList<FollowerModal> followers, String followersCount) {
//        if (followers == null) {
//            setFollowerTv(viewHolder, followersCount);
//            return;
//        }
//
//        int count = followers.size();
//        if (count == 1) {
//            ImageCacheHelper.with(aActivity).loadWithKey(followers.get(0).getImage(), followers.get(0).getId()).transform(new CircleTransformation()).into(viewHolder.three_one_iv);
//            viewHolder.three_one_iv.setVisibility(View.VISIBLE);
//            viewHolder.three_two_iv.setVisibility(View.GONE);
//            viewHolder.three_three_iv.setVisibility(View.GONE);
//            viewHolder.three_image_container_followers_layout.setVisibility(View.VISIBLE);
//        } else if (count == 2) {
//            ImageCacheHelper.with(aActivity).loadWithKey(followers.get(0).getImage(), followers.get(0).getId()).transform(new CircleTransformation()).into(viewHolder.three_one_iv);
//            ImageCacheHelper.with(aActivity).loadWithKey(followers.get(1).getImage(), followers.get(1).getId()).transform(new CircleTransformation()).into(viewHolder.three_three_iv);
//            viewHolder.three_one_iv.setVisibility(View.VISIBLE);
//            viewHolder.three_two_iv.setVisibility(View.GONE);
//            viewHolder.three_three_iv.setVisibility(View.VISIBLE);
//            viewHolder.three_image_container_followers_layout.setVisibility(View.VISIBLE);
//
//        } else if (count == 3) {
//            ImageCacheHelper.with(aActivity).loadWithKey(followers.get(0).getImage(), followers.get(0).getId()).transform(new CircleTransformation()).into(viewHolder.three_one_iv);
//            ImageCacheHelper.with(aActivity).loadWithKey(followers.get(1).getImage(), followers.get(1).getId()).transform(new CircleTransformation()).into(viewHolder.three_two_iv);
//            ImageCacheHelper.with(aActivity).loadWithKey(followers.get(2).getImage(), followers.get(2).getId()).transform(new CircleTransformation()).into(viewHolder.three_three_iv);
//            viewHolder.three_one_iv.setVisibility(View.VISIBLE);
//            viewHolder.three_two_iv.setVisibility(View.VISIBLE);
//            viewHolder.three_three_iv.setVisibility(View.VISIBLE);
//            viewHolder.three_image_container_followers_layout.setVisibility(View.VISIBLE);
//
//        }
//        setFollowerTv(viewHolder, followersCount);
//    }

    @Override
    public int getItemCount() {
        if (venueList == null)
            return 0;

        return venueList.size();
    }

    @Override
    public void onClick(View view) {
        ActivityHandler.startEventProfileActivityForResult(aActivity, (String) view.getTag());
    }


    class ViewHolderVenueListItem extends RecyclerView.ViewHolder {

        final View itemView;

        @BindView(R.id.venue_banner)
        ImageView venue_banner;

        @BindView(R.id.distance_container)
        LinearLayout distance_container;

        @BindView(R.id.venue_name)
        TextView venue_name;

        @BindView(R.id.venue_location)
        TextView venue_location;

        @BindView(R.id.distance_tv)
        TextView distance_tv;


        @BindView(R.id.event_is_new)
        ImageView event_is_new;
        @BindView(R.id.event_from_tm)
        ImageView event_from_tm;

        @BindView(R.id.interested_content)
        View interested_content;
        @BindView(R.id.number_intersted_tv)
        TextView number_intersted_tv;

        @BindView(R.id.message)
        TextView message;

//        @BindView(R.id.three_one_iv)
//        ImageView three_one_iv;
//        @BindView(R.id.three_two_iv)
//        ImageView three_two_iv;
//        @BindView(R.id.three_three_iv)
//        ImageView three_three_iv;
//
//        @BindView(R.id.count_followers_tv)
//        TextView count_followers_tv;
//
//        @BindView(R.id.image_container_following_user)
//        View three_image_container_followers_layout;

        public ViewHolderVenueListItem(View itemView) {
            super(itemView);
            this.itemView = itemView;
            ButterKnife.bind(this, itemView);
        }
    }

}
