package com.trulymadly.android.app.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.trulymadly.android.app.R;
import com.trulymadly.android.app.fragments.EditFavoriteFragment;
import com.trulymadly.android.app.modal.FavoriteDataModal;
import com.trulymadly.android.app.utility.FavoriteUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by udbhav on 12/07/16.
 */
public class EditFavoritesFragmentPagerAdapter extends FragmentStatePagerAdapter {
    private final HashMap<String, ArrayList<FavoriteDataModal>> favoritesDataMap;
    private final FragmentManager fm;
    private Context aContext;
    private String mFavoriteActivitySource;

    public EditFavoritesFragmentPagerAdapter(FragmentManager fm, Context aContext, HashMap<String, ArrayList<FavoriteDataModal>> favoritesDataMap, String favoriteActivitySource) {
        super(fm);
        this.fm = fm;
        this.aContext = aContext;
        this.favoritesDataMap = favoritesDataMap;
        this.mFavoriteActivitySource = favoriteActivitySource;
    }

    @Override
    public int getCount() {
        return FavoriteUtils.PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        return EditFavoriteFragment.newInstance(position, FavoriteUtils.TAB_CATEGORY_KEYS[position], favoritesDataMap.get(FavoriteUtils.TAB_CATEGORY_KEYS[position]), mFavoriteActivitySource);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return FavoriteUtils.tabTitles[position];
    }

    public View createTabView(int position, boolean isSelected) {
        // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
        View v = LayoutInflater.from(aContext).inflate(R.layout.edit_favorites_tab_view, null);
        return updateTabView(v, position, isSelected);
    }

    public View updateTabView(View v, int position, boolean isSelected) {
        if (v == null) {
            return createTabView(position, isSelected);
        }
        ImageView img = (ImageView) v.findViewById(R.id.edit_favorites_tab_layout_image);
        img.setImageResource(isSelected ? FavoriteUtils.imageResIdSelected[position] : FavoriteUtils.imageResId[position]);
        // Picasso is lagging in loading the view
        //Picasso.with(aContext).load(isSelected ? imageResIdSelected[position] : imageResId[position]).noFade().into(img);
        return v;
    }


    public void removeAllFragments() {
        List<Fragment> fragments = fm.getFragments();
        if (fragments != null) {
            FragmentTransaction ft = fm.beginTransaction();
            for (Fragment f : fragments) {
                //You can perform additional check to remove some (not all) fragments:
                if (f instanceof EditFavoriteFragment) {
                    ((EditFavoriteFragment) f).destroy();
                    ft.remove(f);
                }
            }
            ft.commitAllowingStateLoss();
        }
    }
}