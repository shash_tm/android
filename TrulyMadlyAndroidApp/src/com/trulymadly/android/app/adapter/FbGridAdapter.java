package com.trulymadly.android.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.trulymadly.android.app.CircleTransformation;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.modal.FbFriendModal;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by deveshbatra on 6/23/16.
 */
public class FbGridAdapter extends RecyclerView.Adapter<FbGridAdapter.FriendItem> {

    private final Context aContext;
    private final LayoutInflater inflater;
    private ArrayList<FbFriendModal> friendList;

    public FbGridAdapter(Context ctx, ArrayList<FbFriendModal> friendList) {
        this.aContext = ctx;
        this.friendList = friendList;
        inflater = (LayoutInflater) aContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public ArrayList<FbFriendModal> getFriendList() {
        return friendList;
    }

    public void setFriendList(ArrayList<FbFriendModal> friendList) {
        this.friendList = friendList;
    }

    @Override
    public FriendItem onCreateViewHolder(ViewGroup parent, int viewType) {
        View sView = inflater.inflate(R.layout.fb_friend_item, parent,
                false);
        return new FriendItem(sView);
    }

    @Override
    public void onBindViewHolder(FriendItem holder, int position) {
        FbFriendModal friend = friendList.get(position);
        holder.friend_name.setText(friend.getName());
        Picasso.with(aContext).load(friend.getThumbnail()).transform(new CircleTransformation()).into(holder.friend_image);
    }

    @Override
    public int getItemCount() {
        if (friendList == null)
            return 0;

        return friendList.size();

    }

    public class FriendItem extends RecyclerView.ViewHolder {

        final View itemView;

        @BindView(R.id.friend_image)
        ImageView friend_image;

        @BindView(R.id.friend_name)
        TextView friend_name;

        public FriendItem(View itemView) {
            super(itemView);
            this.itemView = itemView;
            ButterKnife.bind(this, itemView);


        }
    }
}
