package com.trulymadly.android.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.listener.OnDataLoadedInterface;
import com.trulymadly.android.app.utility.ImageCacheHelper;
import com.trulymadly.android.app.utility.UiUtils;

/**
 * Created by avin on 21/03/16.
 */
public class FullImageViewerPagerAdapter extends PagerAdapter {

    private final Context mContext;
    private final String[] mImages;
    private final Activity mActivity;
    private final String mFolderName;
    private LayoutInflater mInflater;
    //Decides if we need a zoomable image or not and accordingly changes the layout
    private boolean isZoomable = true;
    //Decides if we need a cached image to be loaded or not and accordingly switches between ImageCacheHelper and Picasso
    private boolean isImageCached = true;

    public FullImageViewerPagerAdapter(Activity activity, String[] images, boolean isZoomable,
                                       boolean isImageCached, String folderName) {
        this.mContext = activity;
        this.mImages = images;
        this.mActivity = activity;
        this.isZoomable = isZoomable;
        this.isImageCached = isImageCached;
        this.mFolderName = folderName;
    }

    @Override
    public int getCount() {
        return (mImages != null) ? mImages.length : 0;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        if (isZoomable) {
            return instantiateZoomableItem(container, position);
        } else {
            return instantiateNonZoomableItem(container, position);
        }
    }

    //Used when we want to have a zoomable imageview based on the isZoomable flag
    private Object instantiateZoomableItem(ViewGroup container, final int position) {

        mInflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = mInflater.inflate(R.layout.imageviewer_zoomable_item,
                container, false);
        final ImageView backupIV = (ImageView) itemView.findViewById(R.id.backup_iv);
        final SubsamplingScaleImageView zoomableIV = (SubsamplingScaleImageView) itemView.findViewById(R.id.zoomable_iv);
        final ProgressBar progressBar = (ProgressBar) itemView.findViewById(R.id.progress_bar);

        if (UiUtils.isXxhdpi(mActivity)) {
            zoomableIV.setMaxScale(5F);
        } else {
            zoomableIV.setMaxScale(3F);
        }

        Target mTarget = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                progressBar.setVisibility(View.GONE);
                zoomableIV.setImage(ImageSource.bitmap(bitmap));
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                backupIV.setVisibility(View.VISIBLE);
                zoomableIV.setVisibility(View.GONE);
                Picasso.with(mContext).load(mImages[position])
                        .config(Bitmap.Config.RGB_565).fit().centerInside().into(backupIV);

                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };

        if (isImageCached) {
            ImageCacheHelper.with(mContext).loadInFolder(mImages[position], mFolderName)
                    .config(Bitmap.Config.RGB_565).into(mTarget);
        } else {
            Picasso.with(mContext).load(mImages[position])
                    .config(Bitmap.Config.RGB_565).into(mTarget);
        }

        container.addView(itemView);
        return itemView;
    }

    //Used when we want to have a non-zoomable imageview based on the isZoomable flag
    private Object instantiateNonZoomableItem(ViewGroup container, final int position) {
        mInflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = mInflater.inflate(R.layout.imageviewer_item,
                container, false);
        final ImageView imageIV = (ImageView) itemView.findViewById(R.id.image_iv);
        final ProgressBar progressBar = (ProgressBar) itemView.findViewById(R.id.progress_bar);

        if (isImageCached) {
            ImageCacheHelper.with(mContext).loadInFolder(mImages[position], mFolderName)
                    .config(Bitmap.Config.RGB_565).fit().centerInside().into(imageIV, new OnDataLoadedInterface() {
                @Override
                public void onLoadSuccess(long mTimeTaken) {
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onLoadFailure(Exception e, long mTimeTaken) {
                    progressBar.setVisibility(View.GONE);
                    Picasso.with(mContext).load(mImages[position])
                            .config(Bitmap.Config.RGB_565).fit().centerInside().into(imageIV);
                }
            });
        } else {
            Picasso.with(mContext).load(mImages[position])
                    .config(Bitmap.Config.RGB_565).fit().centerInside().into(imageIV, new Callback() {
                @Override
                public void onSuccess() {
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onError() {
                    progressBar.setVisibility(View.GONE);
                }
            });
        }

        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}
