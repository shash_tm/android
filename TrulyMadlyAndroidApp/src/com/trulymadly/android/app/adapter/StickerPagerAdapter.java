package com.trulymadly.android.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.trulymadly.android.app.R;
import com.trulymadly.android.app.bus.StickerClickEvent;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.listener.OnDataLoadedInterface;
import com.trulymadly.android.app.modal.StickerData;
import com.trulymadly.android.app.utility.FilesHandler;
import com.trulymadly.android.app.utility.ImageCacheHelper;
import com.trulymadly.android.app.utility.Utility;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import static com.google.common.base.Preconditions.checkNotNull;

public class StickerPagerAdapter extends PagerAdapter implements
        OnItemClickListener {

    private final LayoutInflater mLayoutInflater;
    private final LinkedHashMap<StickerData, ArrayList<StickerData>> hashMapStickers;
    private final LinkedHashMap<Integer, StickerData> hashMapGalleries;
    private final Activity aActivity;
    private final ArrayList<Integer> galleryItems;
    private ArrayList<StickerData> recentStickers;

    public StickerPagerAdapter(Activity activity,
                               LinkedHashMap<Integer, StickerData> hashmapGalleries,
                               LinkedHashMap<StickerData, ArrayList<StickerData>> hmapStickers,
                               ArrayList<StickerData> recentStickers) {
        this.aActivity = activity;
        this.hashMapGalleries = hashmapGalleries;
        this.hashMapStickers = hmapStickers;
        this.recentStickers = recentStickers;
        galleryItems = new ArrayList<>();

        for (Entry<Integer, StickerData> integerStickerDataEntry : hashMapGalleries.entrySet()) {
            Entry<Integer, StickerData> pair = checkNotNull(integerStickerDataEntry);
            galleryItems.add(pair.getKey());
        }

        mLayoutInflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public ArrayList<StickerData> getRecentStickers() {
        return recentStickers;
    }

    private void setRecentStickers(ArrayList<StickerData> recentStickers) {
        this.recentStickers = recentStickers;
    }

    @Override
    public int getCount() {
        return hashMapGalleries.size() + 1;

    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.pager_item, container,
                false);

        StickerData gallery = new StickerData();
        ArrayList<StickerData> stickers = new ArrayList<>();

        GridView localGridView = (GridView) itemView
                .findViewById(R.id.sticker_grid_view_2);

        if (position == 0) {
            localGridView.setAdapter(new StickerGridAdapter(aActivity,
                    recentStickers));
            localGridView.setTag(Constants.STICKER_RECENT_GALLERY_ID);
        } else {
            gallery = hashMapGalleries.get(galleryItems.get(position - 1));
            stickers = hashMapStickers.get(gallery);
            localGridView
                    .setAdapter(new StickerGridAdapter(aActivity, stickers));
            localGridView.setTag(gallery.getId());
        }

        localGridView.setOnItemClickListener(this);
        container.addView(itemView);
        return itemView;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View v, int arg2, long arg3) {
        ViewHolder holder = (ViewHolder) v.getTag();
        StickerData s = holder.sticker;
        Utility.fireBusEvent(aActivity, true, new StickerClickEvent(s));

    }

    public void updateRecentStickerGridView(
            ArrayList<StickerData> recentStickers, GridView gridView) {
        setRecentStickers(recentStickers);
        ((StickerGridAdapter) gridView.getAdapter())
                .setStickers(recentStickers);
        ((StickerGridAdapter) gridView.getAdapter()).notifyDataSetChanged();
    }

    public class StickerGridAdapter extends BaseAdapter {
        private final Context context;
        private ArrayList<StickerData> stickers;

        public StickerGridAdapter(Context ctx, ArrayList<StickerData> stickers) {
            this.stickers = stickers;
            this.context = ctx;
        }

        public void setStickers(ArrayList<StickerData> stickers) {
            this.stickers = stickers;
        }

        public int getCount() {
            return stickers.size();
        }

        public Object getItem(int paramInt) {

            return new StickerData();
        }

        public long getItemId(int paramInt) {
            return paramInt;
        }

        public View getView(int paramInt, View paramView,
                            ViewGroup paramViewGroup) {
            View gridItem = paramView;
            if (gridItem == null) {

                gridItem = View.inflate(context,
                        R.layout.grid_item_image, null);
                ViewHolder localHolder = new ViewHolder();
                localHolder.image = (ImageView) gridItem.findViewById(R.id.grid_image);
                localHolder.photoloader = (ProgressBar) gridItem.findViewById(R.id.sticker_grid_loader);
                gridItem.setTag(localHolder);
            }

            final ViewHolder holder = (ViewHolder) gridItem.getTag();
            holder.sticker = stickers.get(paramInt);
            String url = FilesHandler.createStickerUrl(holder.sticker, "thumbnail");
            String stickerKey = FilesHandler.getStickerKey(url);
            if (Utility.isSet(stickerKey)) {
                ImageCacheHelper.with(aActivity).loadWithKey(url, stickerKey).into(holder.image, new OnDataLoadedInterface() {
                    @Override
                    public void onLoadSuccess(long mTimeTaken) {
                        holder.photoloader.setVisibility(View.GONE);
                        holder.image.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onLoadFailure(Exception e, long mTimeTaken) {
                        holder.photoloader.setVisibility(View.GONE);
                        holder.image.setVisibility(View.VISIBLE);
                    }
                });
            }
            return gridItem;
        }

    }

    private class ViewHolder {
        ImageView image;
        StickerData sticker;
        ProgressBar photoloader;

        private ViewHolder() {
        }
    }

}
