/**
 *
 */
package com.trulymadly.android.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.modal.FavoriteDataModal;
import com.trulymadly.android.app.utility.OkHttpHandler;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author udbhav
 */
public class FavoriteAutoCompleteAdapter extends ArrayAdapter<FavoriteDataModal> implements Filterable {

    private static final String API_BASE = ConstantsUrls.get_favorites_url();
    private final Context aContext;
    private final String category;
    private final LayoutInflater inflater;
    private final ArrayList<FavoriteDataModal> mAllSelectedFavoritesList;
    long currentTime = System.currentTimeMillis();
    long previousTime = 0;
    private ArrayList<FavoriteDataModal> resultList;

    public FavoriteAutoCompleteAdapter(Context aContext, String category, ArrayList<FavoriteDataModal> mFavoritesList) {
        super(aContext, R.layout.favorite_list_item, R.id.hobby_list_item_text);
        this.aContext = aContext;
        this.category = category;
        this.mAllSelectedFavoritesList = mFavoritesList;
        inflater = (LayoutInflater) aContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return resultList != null ? resultList.size() : 0;
    }

    @Override
    public FavoriteDataModal getItem(int index) {
        int size = resultList.size();
        try {
            return index < size ? resultList.get(index) : resultList.get(size - 1);
        } catch (IndexOutOfBoundsException e) {
            if (size > 0) {
                return resultList.get(0);
            } else {
                return null;
            }
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        FavoriteAutoCompeleteViewHolder holder;
        if (convertView != null) {
            holder = (FavoriteAutoCompeleteViewHolder) convertView.getTag();
        } else {
            convertView = inflater.inflate(R.layout.favorite_list_item, parent, false);
            holder = new FavoriteAutoCompeleteViewHolder(convertView);
            convertView.setTag(holder);
        }

        FavoriteDataModal item = getItem(position);
        if (item != null) {
            holder.hobby_list_item_text.setText(item.getName());
            holder.already_selected_checkmark.setVisibility(mAllSelectedFavoritesList.contains(item) ? View.VISIBLE : View.GONE);
        }
        return convertView;
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    autocomplete(constraint.toString());

                    filterResults.values = resultList;
                    filterResults.count = resultList == null ? 0 : resultList.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                //noinspection unchecked
                resultList = (ArrayList<FavoriteDataModal>) results.values;
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };
    }

    private void autocomplete(String input) {

        String apiCategory = category;
        Map<String, String> params = new HashMap<>();
        String results;
        try {
            input = input.trim();
            // checking if call is within 300 ms
            long currentTime = System.currentTimeMillis();
            if (previousTime == 0) {
                previousTime = currentTime;
            } else if (currentTime - previousTime < 300) {
                return;
            } else {
                previousTime = currentTime;
            }

            params.put("category", URLEncoder.encode(apiCategory, "utf8"));
            params.put("search_text", URLEncoder.encode(input, "utf8"));
            params.put("action", "search_page");
            results = OkHttpHandler.httpGetSynchronous(aContext, API_BASE, params, TrulyMadlyEvent.TrulyMadlyActivities.favorites, TrulyMadlyEvent.TrulyMadlyEventTypes.favorites_autocomplete_server_call);

        } catch (UnsupportedEncodingException e) {
            return;
        }

        if (Utility.isSet(results)) {
            try {
                JSONObject responseJSON = new JSONObject(results);

                resultList = new ArrayList<>();

                if (responseJSON.optJSONObject("pages") != null && responseJSON.optJSONObject("pages").optJSONArray(apiCategory) != null) {
                    JSONArray suggestionsJsonArray = responseJSON.optJSONObject("pages").optJSONArray(apiCategory);
                    if (suggestionsJsonArray != null && suggestionsJsonArray.length() > 0) {
                        int len = suggestionsJsonArray.length();
                        for (int i = 0; i < len; i++) {
                            JSONObject singleFavJson = suggestionsJsonArray.optJSONObject(i);
                            if (singleFavJson != null) {
                                FavoriteDataModal favDataModal = new FavoriteDataModal(singleFavJson.optString("id"), singleFavJson.optString("name"), apiCategory, singleFavJson.optString("pic"), false, true, false, true, true);
                                if (!resultList.contains(favDataModal)) {
                                    resultList.add(favDataModal);
                                }
                            }
                        }
                    }
                }
            } catch (JSONException ignored) {
            }
        }
    }

    static class FavoriteAutoCompeleteViewHolder {
        @BindView(R.id.hobby_list_item_text)
        TextView hobby_list_item_text;
        @BindView(R.id.already_selected_checkmark)
        View already_selected_checkmark;

        public FavoriteAutoCompeleteViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
