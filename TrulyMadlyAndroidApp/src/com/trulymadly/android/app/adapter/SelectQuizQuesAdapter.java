package com.trulymadly.android.app.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.modal.SelectQuizModal;

import java.util.ArrayList;

/**
 * Created by avin on 24/10/16.
 */

public class SelectQuizQuesAdapter extends PagerAdapter {

    private Context mContext;
    private int mCount = 0;
    private ArrayList<SelectQuizModal.SelectQuizQuestionModal> mSelectQuizQuestionModals;

    public SelectQuizQuesAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void changeList(ArrayList<SelectQuizModal.SelectQuizQuestionModal> selectQuizQuestionModals,
                           int count) {
        this.mSelectQuizQuestionModals = selectQuizQuestionModals;
        if (count != -1) {
            mCount = count;
        } else {
            mCount = mSelectQuizQuestionModals.size();
        }
        notifyDataSetChanged();
    }

    public void incCount() {
        if (mSelectQuizQuestionModals == null || mCount == mSelectQuizQuestionModals.size()) {
            return;
        }
        mCount++;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mCount;//(mSelectQuizQuestionModals == null) ? 0 : mSelectQuizQuestionModals.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((ScrollView) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.select_quiz_ques_item,
                container, false);

        ImageView mIllustrationIV = (ImageView) itemView.findViewById(R.id.illustration_iv);
        TextView mQuestionTV = (TextView) itemView.findViewById(R.id.question_tv);
        SelectQuizModal.SelectQuizQuestionModal selectQuizQuestionModal = mSelectQuizQuestionModals.get(position);
        mQuestionTV.setText(selectQuizQuestionModal.getmQuestion());
        Picasso.with(mContext).load(selectQuizQuestionModal.getmImageUrl()).into(mIllustrationIV);

        container.addView(itemView);
        return itemView;
    }
}
