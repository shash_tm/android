package com.trulymadly.android.app.adapter;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.trulymadly.android.app.fragments.FollowingVenuesFragment;
import com.trulymadly.android.app.fragments.VenuesFragment;
import com.trulymadly.android.app.modal.VenueModal;

import java.util.ArrayList;

/**
 * Created by deveshbatra on 3/29/16.
 */
public class VenueListPagerAdapter extends FragmentStatePagerAdapter {
    private final int mNumOfTabs;
    public VenuesFragment tab1;
    public FollowingVenuesFragment tab2;
    private String categoryId;
    private ArrayList<VenueModal> venueList, followingVenueList;

    public VenueListPagerAdapter(FragmentManager fm, int NumOfTabs, ArrayList<VenueModal> venueList, ArrayList<VenueModal> followingEventList) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.venueList = venueList;
        this.followingVenueList = followingEventList;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public ArrayList<VenueModal> getVenueList() {
        return venueList;
    }

    public void setVenueList(ArrayList<VenueModal> venueList) {
        this.venueList = venueList;
    }

    public ArrayList<VenueModal> getFollwingVenueList() {
        return followingVenueList;
    }

    public void setFollwingVenueList(ArrayList<VenueModal> follwingVenueList) {
        this.followingVenueList = follwingVenueList;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                Bundle data = new Bundle();
                data.putString("categoryId", this.categoryId);
                data.putSerializable("venueList", venueList);
                VenuesFragment tab1 = new VenuesFragment();
                tab1.setArguments(data);
                return tab1;
            case 1:
                data = new Bundle();
                data.putString("categoryId", this.categoryId);
                data.putSerializable("venueList", followingVenueList);
                FollowingVenuesFragment tab2 = new FollowingVenuesFragment();
                tab2.setArguments(data);
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Recommended";
            case 1:
                return "Following";
            default:
                return "Recommended";
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}

