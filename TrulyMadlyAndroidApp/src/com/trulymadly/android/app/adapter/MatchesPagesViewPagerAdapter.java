package com.trulymadly.android.app.adapter;

import android.content.Context;
import android.graphics.Bitmap.Config;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.cesards.cropimageview.CropImageView;
import com.cesards.cropimageview.CropImageView.CropType;
import com.longtailvideo.jwplayer.JWPlayerView;
import com.longtailvideo.jwplayer.media.playlists.PlaylistItem;
import com.squareup.picasso.Callback.EmptyCallback;
import com.squareup.picasso.Picasso;
import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.listener.OnDataLoadedInterface;
import com.trulymadly.android.app.utility.ImageCacheHelper;
import com.trulymadly.android.app.utility.Utility;

import static com.google.common.base.Preconditions.checkNotNull;

public class MatchesPagesViewPagerAdapter extends PagerAdapter {
    private final Context mContext;
    private final LayoutInflater mLayoutInflater;
    private final boolean isMyProfile;
    private String[] imagesId;
    private OnClickListener viewPagerClickedListener, firstPicListener;
    private boolean isDateSpot;
    private String key;
    private String[] videoUrls;
    private OnClickListener showVideoListner;

    public MatchesPagesViewPagerAdapter(Context context, Boolean isMyProfile) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.isMyProfile = isMyProfile;
        showVideoListner = new OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = (int) v.getTag();
            }
        };

    }

    public MatchesPagesViewPagerAdapter(Context context, Boolean isMyProfile, Boolean isDateSpot, String key) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.isMyProfile = isMyProfile;
        this.isDateSpot = isDateSpot;
        this.key = key;
    }

    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public void destroyItem(View container, int position, Object object) {
//        JWPlayerView jwPlayerView = (JWPlayerView) container.findViewById(R.id.playerViewItem);
//        jwPlayerView.stop();
//        jwPlayerView.onDestroy();
        super.destroyItem(container, position, object);
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setImages(String[] imagesId) {
        this.imagesId = imagesId;
    }

    public void setListener(OnClickListener listener, OnClickListener firstPicListener) {
        this.viewPagerClickedListener = listener;
        this.firstPicListener = firstPicListener;
    }

    public int getCount() {
        if (imagesId == null)
            return 0;
        return imagesId.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View itemView = mLayoutInflater.inflate(R.layout.matches_page_view_pager_item, container, false);
        itemView.setTag(position);
        final CropImageView matches_page_view_pager_view = (CropImageView) itemView
                .findViewById(R.id.matches_page_view_pager_view);
        final ProgressBar custom_prog_bar_photo_id = (ProgressBar) itemView.findViewById(R.id.custom_prog_bar_photo_id);
        final JWPlayerView playerView = checkNotNull(null);//(JWPlayerView) itemView.findViewById(R.id.playerViewItem);
        final View player_view_layer = itemView.findViewById(R.id.player_view_layer);
        player_view_layer.setTag(position);
        final String url;

        if (position == 0) {
            itemView.setOnClickListener(firstPicListener);
            playerView.setOnClickListener(firstPicListener);
            player_view_layer.setOnClickListener(firstPicListener);
        } else {
            itemView.setOnClickListener(viewPagerClickedListener);
            playerView.setOnClickListener(viewPagerClickedListener);
            player_view_layer.setOnClickListener(viewPagerClickedListener);
        }


        url = imagesId[position];
        if (isMyProfile) {
            ImageCacheHelper.with(mContext).loadWithKey(imagesId[position], Utility.getMyId(mContext))
                    .into(matches_page_view_pager_view, new OnDataLoadedInterface() {
                        @Override
                        public void onLoadSuccess(long mTimeTaken) {
                            custom_prog_bar_photo_id.setVisibility(View.GONE);
                            matches_page_view_pager_view.setCropType(CropType.CENTER_TOP);
                        }

                        @Override
                        public void onLoadFailure(Exception e, long mTimeTaken) {
                            custom_prog_bar_photo_id.setVisibility(View.GONE);

                        }

                    });

        } else if (isDateSpot) {
            if (position == 0) {
                ImageCacheHelper.with(mContext).loadWithKey(url, key)
                        .into(matches_page_view_pager_view, new OnDataLoadedInterface() {
                            @Override
                            public void onLoadSuccess(long mTimeTaken) {
                                custom_prog_bar_photo_id.setVisibility(View.GONE);
                                matches_page_view_pager_view.setCropType(CropType.CENTER_TOP);
                            }

                            @Override
                            public void onLoadFailure(Exception e, long mTimeTaken) {
                                custom_prog_bar_photo_id.setVisibility(View.GONE);

                            }
                        });
            } else {
                Picasso.with(mContext).load(url).config(Config.RGB_565)
                        .into(matches_page_view_pager_view, new EmptyCallback() {
                            @Override
                            public void onSuccess() {
                                custom_prog_bar_photo_id.setVisibility(View.GONE);
                                matches_page_view_pager_view.setCropType(CropType.CENTER_TOP);
                            }

                            @Override
                            public void onError() {
                                loadFromUrl(url, matches_page_view_pager_view, custom_prog_bar_photo_id);
                            }
                        });
            }
        } else {

            if (Utility.isSet(url)) {
                Picasso.with(mContext).load(url).config(Config.RGB_565)
                        .into(matches_page_view_pager_view, new EmptyCallback() {
                            @Override
                            public void onSuccess() {
                                custom_prog_bar_photo_id.setVisibility(View.GONE);
                                matches_page_view_pager_view.setCropType(CropType.CENTER_TOP);
                            }

                            @Override
                            public void onError() {
                                loadFromUrl(url, matches_page_view_pager_view, custom_prog_bar_photo_id);
                            }
                        });
            } else {
                custom_prog_bar_photo_id.setVisibility(View.GONE);
            }
        }
        if (Utility.isSet(videoUrls[position])) {
            String videoLink = videoUrls[position];
            custom_prog_bar_photo_id.setVisibility(View.GONE);
            matches_page_view_pager_view.setVisibility(View.GONE);
            player_view_layer.setVisibility(View.VISIBLE);
            playerView.setVisibility(View.VISIBLE);
            playerView.load(new PlaylistItem(videoLink));
            playerView.play();
        } else {

        }
        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }


    private void loadFromUrl(String url, final CropImageView matches_page_view_pager_view, final ProgressBar custom_prog_bar_photo_id) {

        TrulyMadlyTrackEvent.trackEvent(mContext, TrulyMadlyEvent.TrulyMadlyActivities.photo_fail,
                TrulyMadlyEvent.TrulyMadlyEventTypes.photo_retry, 0, null, null, false);


        Picasso.with(mContext).load(url).config(Config.RGB_565)
                .into(matches_page_view_pager_view, new EmptyCallback() {
                    @Override
                    public void onSuccess() {
                        custom_prog_bar_photo_id.setVisibility(View.GONE);
                        matches_page_view_pager_view.setCropType(CropType.CENTER_TOP);
                    }

                    @Override
                    public void onError() {
                        custom_prog_bar_photo_id.setVisibility(View.GONE);
                    }
                });
    }

    public String[] getVideoUrls() {
        return videoUrls;
    }

    public void setVideoUrls(String[] videoUrls) {
        this.videoUrls = videoUrls;
    }
}
