package com.trulymadly.android.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.trulymadly.android.app.R;
import com.trulymadly.android.app.listener.FavoriteItemClickInterface;
import com.trulymadly.android.app.modal.FavoriteDataModal;
import com.trulymadly.android.app.utility.FavoriteUtils;

import java.util.ArrayList;

/**
 * Created by udbhav on 13/07/16.
 */
public class MyFavoritesGridViewAdapter extends RecyclerView.Adapter<MyFavoriteGridItem> implements View.OnClickListener {
    private Context aContext;
    private LayoutInflater inflater;
    private FavoriteItemClickInterface favoriteItemClickInterface;
    private ArrayList<FavoriteDataModal> mFavoriteDataModal;


    public MyFavoritesGridViewAdapter(Context ctx, ArrayList<FavoriteDataModal> favDataModal, FavoriteItemClickInterface favoriteItemClickInterface) {
        this.aContext = ctx;
        this.mFavoriteDataModal = favDataModal;
        this.favoriteItemClickInterface = favoriteItemClickInterface;
        inflater = (LayoutInflater) aContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public MyFavoriteGridItem onCreateViewHolder(ViewGroup parent, int viewType) {
        View sView = inflater.inflate(R.layout.favorite_item, parent,
                false);
        return new MyFavoriteGridItem(sView);
    }

    @Override
    public void onBindViewHolder(final MyFavoriteGridItem viewHolder, int position) {
        FavoriteDataModal favoriteData = mFavoriteDataModal.get(position);
        FavoriteUtils.bindView(aContext, viewHolder, favoriteData, this);
    }

    @Override
    public int getItemCount() {
        if (mFavoriteDataModal == null) {
            return 0;
        }
        return mFavoriteDataModal.size();
    }

    @Override
    public void onClick(View v) {
        MyFavoriteGridItem holder = (MyFavoriteGridItem) v.getTag();
        FavoriteDataModal favoriteData = holder.getFavoriteData();
        if (favoriteData.isShowMoreItem()) {
            if (favoriteItemClickInterface != null) {
                favoriteItemClickInterface.showMoreFavorites();
            }
            return;
        }

        if (favoriteData.isSelected()) {
            favoriteData.setSelected(false);
            //UiUtils.doRevealAnimationOnView(holder.myFavoriteOverLay, false, 100, 100);
            holder.myFavoriteOverLay.setVisibility(View.GONE);
        } else {
            holder.myFavoriteOverLayImage.setImageResource(R.drawable.select_check_mark_white_circle);
//            UiUtils.doRevealAnimationOnView(holder.myFavoriteOverLay, true, 100, 100);
            holder.myFavoriteOverLay.setVisibility(View.VISIBLE);
            favoriteData.setSelected(true);
        }
        holder.setFavoriteData(favoriteData);
        v.setTag(holder);
        if (favoriteItemClickInterface != null) {
            favoriteItemClickInterface.onFavoriteClicked(favoriteData, favoriteData.isSelected());
        }
    }

    public void updateFavoritesList(ArrayList<FavoriteDataModal> favoriteDataModals) {
        this.mFavoriteDataModal = favoriteDataModals;
        notifyDataSetChanged();
    }

}
