package com.trulymadly.android.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.trulymadly.android.app.CircleTransformation;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.listener.OnDataLoadedInterface;
import com.trulymadly.android.app.listener.ShareEventUserPicClickedInterface;
import com.trulymadly.android.app.modal.ActiveConversationModal;
import com.trulymadly.android.app.utility.ImageCacheHelper;
import com.trulymadly.android.app.utility.Utility;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by deveshbatra on 3/19/16.
 */
public class ShareEventListAdapter extends RecyclerView.Adapter<ShareEventListAdapter.ViewHolderShareEventUserListItem> implements View.OnClickListener {

    private final ShareEventUserPicClickedInterface onClick;
    private final Activity aActivity;
    private final LayoutInflater inflater;
    private ArrayList<ActiveConversationModal> activeConversations;

    public ShareEventListAdapter(Activity act, ArrayList<ActiveConversationModal> activeConversations, ShareEventUserPicClickedInterface onClick) {
        this.aActivity = act;
        this.activeConversations = activeConversations;

        inflater = (LayoutInflater) act
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.onClick = onClick;
    }

    @Override
    public ViewHolderShareEventUserListItem onCreateViewHolder(ViewGroup parent, int viewType) {
        View sView = inflater.inflate(R.layout.share_event_user_list_item, parent,
                false);
        return new ViewHolderShareEventUserListItem(sView);
    }


    @Override
    public void onBindViewHolder(final ViewHolderShareEventUserListItem viewHolder, int position) {

        ActiveConversationModal activeConversation = activeConversations.get(position);
        viewHolder.itemView.setTag(activeConversation);
        viewHolder.itemView.setOnClickListener(this);

        ImageCacheHelper.with(aActivity).loadWithKey(activeConversation.getProfile_pic(), activeConversation.getMatch_id())
                .transform(new CircleTransformation())
                .into(viewHolder.dp_holder, new OnDataLoadedInterface() {

                    @Override
                    public void onLoadSuccess(long mTimeTaken) {
                    }

                    @Override
                    public void onLoadFailure(Exception e, long mTimeTaken) {
                        int image = R.drawable.dummy_male;
                        if (Utility.isMale(aActivity)) {
                            image = R.drawable.dummy_female;
                        }
                        Picasso.with(aActivity).load(image).transform(new CircleTransformation()).into(viewHolder.dp_holder);
                    }
                });
        viewHolder.user_name_tv.setText(activeConversation.getFname());

    }

    @Override
    public int getItemCount() {
        return activeConversations.size();
    }

    @Override
    public void onClick(View view) {
        ActiveConversationModal activeConversationModal = (ActiveConversationModal) view.getTag();
        onClick.onShareEventUserPicClicked(activeConversationModal);


    }

    public void updateActiveConversations(ArrayList<ActiveConversationModal> ac) {
        this.activeConversations = ac;
        notifyDataSetChanged();
    }

    class ViewHolderShareEventUserListItem extends RecyclerView.ViewHolder {

        final View itemView;
        @BindView(R.id.dp_holder)
        ImageView dp_holder;

        @BindView(R.id.user_name_tv)
        TextView user_name_tv;

        public ViewHolderShareEventUserListItem(View itemView) {
            super(itemView);
            this.itemView = itemView;
            ButterKnife.bind(this, itemView);
        }
    }


}
