package com.trulymadly.android.app.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes;
import com.trulymadly.android.app.CircleTransformation;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.bus.QuizClickEvent;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.listener.ImageWriteCompleteInterface;
import com.trulymadly.android.app.modal.QuizData;
import com.trulymadly.android.app.sqlite.MessageDBHandler;
import com.trulymadly.android.app.utility.ImageCacheHelper;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.Utility;

import java.util.ArrayList;
import java.util.HashMap;

public class QuizGridAdapter extends BaseAdapter {

    private static final int QUIZ_MATCH_ICON_ANIMATION_DURATION = 2000;
    private final Activity aActivity;
    private final ArrayList<QuizData> quizList;
    private final HashMap<Integer, Boolean> flareMap;
    private final HashMap<Integer, Boolean> animationMap;
    private final String userImageUrl;
    private final String matchImageUrl;
    private final int maxQuizId;
    private HashMap<Integer, String> quizStatus;

    public QuizGridAdapter(Activity aActivity, ArrayList<QuizData> quizList,
                           HashMap<Integer, String> quizStatus, String matchId,
                           HashMap<Integer, Boolean> flareMap,
                           HashMap<Integer, Boolean> animationMap, int maxQuizId) {
        this.aActivity = aActivity;
        this.quizList = quizList;
        this.quizStatus = quizStatus;
        this.flareMap = flareMap;
        this.maxQuizId = maxQuizId;
        this.animationMap = animationMap;
        String userId = Utility.getMyId(aActivity);
        userImageUrl = SPHandler.getString(aActivity,
                ConstantsSP.SHARED_KEYS_USER_PROFILE_THUMB_URL);
        matchImageUrl = MessageDBHandler.getMatchMessageMetaData(userId,
                matchId, aActivity).getProfilePic();

    }

    public void setQuizStatus(HashMap<Integer, String> quizStatus) {
        this.quizStatus = quizStatus;
    }

    @Override
    public int getCount() {
        return this.quizList.size();
    }

    @Override
    public Object getItem(int position) {
        return quizList.get(position);

    }

    @Override
    public long getItemId(int position) {
        return quizList.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = View.inflate(aActivity, R.layout.quiz_grid, null);
            QuizViewHolder localHolder = new QuizViewHolder(convertView);
            convertView.setTag(localHolder);
        }
        final QuizViewHolder holder = (QuizViewHolder) convertView.getTag();
        holder.quiz = quizList.get(position);

        ImageWriteCompleteInterface onComplete = new ImageWriteCompleteInterface() {

            @Override
            public void onSuccess(long mTimeTaken) {
            }

			@Override
            public void onFail(Exception e, long mTimeTaken) {
                Picasso.with(aActivity).load(holder.quiz.getImageUrl()).transform(new CircleTransformation()).into(holder.image);
            }
		};
		ImageCacheHelper.with(aActivity)
				.loadWithKey(holder.quiz.getImageUrl(),
						holder.quiz.getImageCacheKey())
                .transform(new CircleTransformation())
                .into(holder.image, null);

        if (quizList.get(position).getId() > maxQuizId && maxQuizId > 0) {
            holder.newQuizTv.setVisibility(View.VISIBLE);
            holder.flare.setVisibility(View.GONE);

        } else if (flareMap != null
                && flareMap.get(quizList.get(position).getId()) != null) {
            holder.newQuizTv.setVisibility(View.GONE);
            if (flareMap.get(quizList.get(position).getId()))
                holder.flare.setVisibility(View.VISIBLE);
            else
                holder.flare.setVisibility(View.GONE);


        } else {
            holder.newQuizTv.setVisibility(View.GONE);
            holder.flare.setVisibility(View.GONE);
        }

        holder.textView.setText(quizList.get(position).getName());

        if (quizStatus != null) {
            String status = quizStatus.get(quizList.get(position).getId());
            if (status != null) {
                boolean animationFlag = false;
                if (animationMap != null && animationMap
                        .get(quizList.get(position).getId()) != null) {
                    animationFlag = animationMap
                            .get(quizList.get(position).getId());
                }

                setOverlay(holder.overlay_1, holder.overlay_2, holder.overlay_3,
                        status, animationFlag);
            } else {
                setOverlay(holder.overlay_1, holder.overlay_2, holder.overlay_3,
                        "NONE", false);
            }
        } else {
            setOverlay(holder.overlay_1, holder.overlay_2, holder.overlay_3,
                    "NONE", false);
        }
        return convertView;

    }

    private void setOverlay(ImageView overlay_1, ImageView overlay_2,
                            ImageView overlay_3, String status, boolean animationFlag) {

        if (status.equalsIgnoreCase("BOTH")) {
            overlay_1.setVisibility(View.VISIBLE);
            Picasso.with(aActivity).load(userImageUrl)
                    .transform(new CircleTransformation())
                    .placeholder(R.drawable.place_holder_image_url).fit()
                    .into(overlay_1);
            callAlphaAnimation(animationFlag, overlay_2, matchImageUrl);
            overlay_3.setVisibility(View.GONE);
        } else if (status.equals("USER")) {
            overlay_1.setVisibility(View.GONE);
            overlay_2.setVisibility(View.GONE);
            overlay_3.setVisibility(View.VISIBLE);
            Picasso.with(aActivity).load(userImageUrl)
                    .transform(new CircleTransformation())
                    .placeholder(R.drawable.place_holder_image_url).fit()
                    .into(overlay_3);
        } else if (status.equals("MATCH")) {
            overlay_1.setVisibility(View.GONE);
            overlay_2.setVisibility(View.GONE);

            callAlphaAnimation(animationFlag, overlay_3, matchImageUrl);
        } else {
            overlay_1.setVisibility(View.GONE);
            overlay_2.setVisibility(View.GONE);
            overlay_3.setVisibility(View.GONE);
        }

    }

    private void callAlphaAnimation(final boolean animationFlag,
                                    final ImageView overlay, final String imageUrl) {
        if (animationFlag) {
            overlay.setVisibility(View.INVISIBLE);

            Picasso.with(aActivity).load(imageUrl).fetch(new Callback() {

                @Override
                public void onSuccess() {
                    Picasso.with(aActivity).load(imageUrl)
                            .transform(new CircleTransformation()).fit()
                            .into(overlay, new Callback() {
                                @Override
                                public void onSuccess() {
                                    showAlphaAnimation(overlay, animationFlag);
                                }

                                @Override
                                public void onError() {
                                    showAlphaAnimation(overlay, animationFlag);
                                }
                            });
                }

                @Override
                public void onError() {
                    Picasso.with(aActivity)
                            .load(R.drawable.place_holder_image_url)
                            .transform(new CircleTransformation()).fit()
                            .into(overlay, new Callback() {
                                @Override
                                public void onSuccess() {
                                    showAlphaAnimation(overlay, animationFlag);
                                }

                                @Override
                                public void onError() {
                                    showAlphaAnimation(overlay, animationFlag);
                                }
                            });
                }
            });

        } else {
            overlay.setVisibility(View.VISIBLE);
            Picasso.with(aActivity).load(imageUrl)
                    .transform(new CircleTransformation())
                    .placeholder(R.drawable.place_holder_image_url).fit()
                    .into(overlay);
        }
    }

    private void showAlphaAnimation(final View overlay,
                                    boolean animationFlag) {
        overlay.setVisibility(View.VISIBLE);
        if (animationFlag) {
            AlphaAnimation alphaAnimation = new AlphaAnimation(0, 1);
            alphaAnimation.setDuration(QUIZ_MATCH_ICON_ANIMATION_DURATION);
            alphaAnimation.setFillAfter(true);
            overlay.startAnimation(alphaAnimation);
        }
    }

    public class QuizViewHolder implements View.OnClickListener {
        final ImageView image;
        final ImageView flare;
        final ImageView overlay_1;
        final ImageView overlay_2;
        final ImageView overlay_3;
        final TextView textView;
        final TextView newQuizTv;
        QuizData quiz;

        public QuizViewHolder(View gridItem) {
            super();

            image = (ImageView) gridItem.findViewById(R.id.grid_main_image);
            flare = (ImageView) gridItem.findViewById(R.id.flare_image);

            Picasso.with(aActivity)
                    .load(R.drawable.quiz_image_option_placeholder).noFade().into(image);
            textView = (TextView) gridItem.findViewById(R.id.grid_text);
            newQuizTv = (TextView) gridItem.findViewById(R.id.new_quiz_text);
            overlay_1 = (ImageView) gridItem.findViewById(R.id.overlay_1);
            overlay_2 = (ImageView) gridItem.findViewById(R.id.overlay_2);
            overlay_3 = (ImageView) gridItem.findViewById(R.id.overlay_3);
            overlay_1.setVisibility(View.GONE);
            overlay_2.setVisibility(View.GONE);
            overlay_3.setVisibility(View.GONE);

            textView.setOnClickListener(this);
            image.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Utility.fireBusEvent(aActivity, true,
                    new QuizClickEvent(this.quiz.getId(), this.quiz.getName(),
                            false, quizStatus.get(this.quiz.getId()),
                            TrulyMadlyEventTypes.quiz_item_in_drawer_clicked));
        }

    }

}
