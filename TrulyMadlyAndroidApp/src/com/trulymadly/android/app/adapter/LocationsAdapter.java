package com.trulymadly.android.app.adapter;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.modal.LocationModal;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LocationsAdapter extends
        RecyclerView.Adapter<LocationsAdapter.ViewHolderLocationListItem> implements View.OnClickListener {

    private final LayoutInflater inflater;
    private final Activity aActivity;
    private final ArrayList<LocationModal> locations;
    private final String dealId;
    private final String trkActivity;
    private final HashMap<String, String> trkEventInfo;

    public LocationsAdapter(Activity act, ArrayList<LocationModal> locations, String dealId, String trkActivity, HashMap<String, String> trkEventInfo) {
        this.aActivity = act;
        this.locations = locations;
        this.dealId=dealId;
        this.trkActivity = trkActivity;
        this.trkEventInfo = trkEventInfo;
        inflater = (LayoutInflater) act
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ViewHolderLocationListItem onCreateViewHolder(ViewGroup parent, int viewType) {

        View sView = inflater.inflate(R.layout.location_item_layout, parent,
                false);
        return new ViewHolderLocationListItem(sView);

    }

    @Override
    public void onBindViewHolder(ViewHolderLocationListItem viewHolder, int position) {
        if (viewHolder != null) {
            LocationModal location = locations.get(position);
            viewHolder.contact_no_text_view.setText(location.getContactNo());
            viewHolder.address_text_view.setText(location.getAddress());
            viewHolder.itemView.setTag(location);
            viewHolder.contact_no_text_view.setTag(location);
            viewHolder.contact_no_text_view.setOnClickListener(this);


        }

    }

    @Override
    public int getItemCount() {
        return locations.size();
    }

    @Override
    public void onClick(View view) {
        LocationModal location = (LocationModal) view.getTag();
        TrulyMadlyTrackEvent.trackEvent(aActivity, trkActivity,
                TrulyMadlyEvent.TrulyMadlyEventTypes.phone_called, 0, dealId, trkEventInfo, true);

        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + location.getContactNo()));
        aActivity.startActivity(intent);

    }


    class ViewHolderLocationListItem extends RecyclerView.ViewHolder {

        final View itemView;

        @BindView(R.id.address)
        TextView address_text_view;

        @BindView(R.id.contact_no_text_view)
        TextView contact_no_text_view;

        @BindView(R.id.call_image_view)
        ImageView contact_iv;


        @BindView(R.id.address_image_view)
        ImageView address_iv;


        public ViewHolderLocationListItem(View itemView) {
            super(itemView);
            this.itemView = itemView;
            ButterKnife.bind(this, itemView);
        }
    }


}
