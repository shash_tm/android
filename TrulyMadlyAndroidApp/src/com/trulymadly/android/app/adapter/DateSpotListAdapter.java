package com.trulymadly.android.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.listener.DateSpotListItemClickInterface;
import com.trulymadly.android.app.listener.OnDataLoadedInterface;
import com.trulymadly.android.app.modal.DateSpotModal;
import com.trulymadly.android.app.utility.GradientTransformation;
import com.trulymadly.android.app.utility.ImageCacheHelper;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DateSpotListAdapter extends
        RecyclerView.Adapter<DateSpotListAdapter.ViewHolderDateSpotListItem> implements View.OnClickListener {

    private final double screenWidth;
    private final String tag = "Date_spot_list_adapter";
    private final DateSpotListItemClickInterface onClick;

    private final ArrayList<DateSpotModal> dateSpotList;
    private final Activity aActivity;
    private final LayoutInflater inflater;
    private final GradientTransformation mCDGradientTransformation;

    public DateSpotListAdapter(Activity act, ArrayList<DateSpotModal> dateSpotList, DateSpotListItemClickInterface onClick) {
        this.aActivity = act;
        this.dateSpotList = dateSpotList;
        inflater = (LayoutInflater) act
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        screenWidth = UiUtils.getScreenWidth(act);
        this.onClick = onClick;
        mCDGradientTransformation = new GradientTransformation(new int[]{Color.parseColor("#00444343"),
                Color.parseColor("#80444343"), Color.parseColor("#00444343")});
    }

    @Override
    public int getItemCount() {
        return dateSpotList.size();
    }


    @Override
    public ViewHolderDateSpotListItem onCreateViewHolder(ViewGroup parent, int viewType) {

        View sView = inflater.inflate(R.layout.date_spot_list_item, parent,
                false);
        return new ViewHolderDateSpotListItem(sView);

    }

    @Override
    public void onBindViewHolder(DateSpotListAdapter.ViewHolderDateSpotListItem viewHolder, int position) {
        if (viewHolder != null) {

            DateSpotModal date = dateSpotList.get(position);

            ImageCacheHelper.with(aActivity).loadWithKey(date.getImageProfile(),
                    date.getId())
                    .transform(mCDGradientTransformation)
                    .into(viewHolder.date_spot_image, new OnDataLoadedInterface() {
                        @Override
                        public void onLoadSuccess(long mTimeTaken) {
                        }

                        @Override
                        public void onLoadFailure(Exception e, long mTimeTaken) {

                        }
                    });

            if (date.isMissTmRecommends()) {
                viewHolder.miss_tm_recommends_image.setVisibility(View.VISIBLE);
                Picasso.with(aActivity).load(R.drawable.tm_recommands_06).into(viewHolder.miss_tm_recommends_image);
            } else {
                viewHolder.miss_tm_recommends_image.setVisibility(View.GONE);
            }

            viewHolder.date_is_new_badge.setVisibility(date.isNew() ? View.VISIBLE : View.GONE);

            viewHolder.friendly_name_tv.setText(date.getName());
            viewHolder.location.setText(date.getLocation());

            viewHolder.itemView.setTag(date.getId());
            viewHolder.itemView.setOnClickListener(this);

            if (Utility.isSet(date.getDistance())) {
                viewHolder.distance_tv.setText(date.getDistance());
                viewHolder.distance_container.setVisibility(View.VISIBLE);
                Picasso.with(aActivity).load(R.drawable.place).noFade().into(viewHolder.distance_icon);
            } else {
                viewHolder.distance_container.setVisibility(View.GONE);
            }

            ViewGroup.LayoutParams params = viewHolder.itemView.getLayoutParams();
            params.height = (int) (screenWidth / 2);
            viewHolder.itemView.setLayoutParams(params);
        }
    }

    @Override
    public void onClick(View view) {
        String dateSpotIdClicked = (String) view.getTag();
        onClick.onDateSpotItemClicked(dateSpotIdClicked);
    }


    class ViewHolderDateSpotListItem extends RecyclerView.ViewHolder {

        @BindView(R.id.date_spot_image)
        ImageView date_spot_image;
        @BindView(R.id.friendly_name)
        TextView friendly_name_tv;
        @BindView(R.id.location)
        TextView location;
        @BindView(R.id.miss_tm_recommends_image)
        ImageView miss_tm_recommends_image;
        @BindView(R.id.date_is_new_badge)
        View date_is_new_badge;
        @BindView(R.id.distance_tv)
        TextView distance_tv;
        @BindView(R.id.distance_icon)
        ImageView distance_icon;
        @BindView(R.id.distance_container)
        View distance_container;

        public ViewHolderDateSpotListItem(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
