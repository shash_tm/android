package com.trulymadly.android.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.trulymadly.android.app.R;
import com.trulymadly.android.app.fragments.BuySparkEventListener;
import com.trulymadly.android.app.modal.SparkPackageModal;
import com.trulymadly.android.app.utility.ABHandler;
import com.trulymadly.android.app.utility.TmLogger;
import com.trulymadly.android.app.utility.Utility;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by avin on 31/05/16.
 */
public class SparkPackagesAdapter extends RecyclerView.Adapter<SparkPackagesAdapter.SparkPackageViewHolder> {

    private final BuySparkEventListener mBuySparkEventListener;
    private final String matchId;
    private ArrayList<SparkPackageModal> mSparkPackageModals;
    private Context mContext;

    public SparkPackagesAdapter(Context mContext, ArrayList<SparkPackageModal> mSparkPackageModals, BuySparkEventListener mBuySparkEventListener, String matchId) {
        this.mContext = mContext;
        this.mSparkPackageModals = mSparkPackageModals;
        this.mBuySparkEventListener = mBuySparkEventListener;
        this.matchId = matchId;
    }

    @Override
    public SparkPackageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.spark_package_list_item, parent, false);

        return new SparkPackageViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SparkPackageViewHolder holder, int position) {
        holder.initialize(mSparkPackageModals.get(position), position);
    }

    @Override
    public int getItemCount() {
        return (mSparkPackageModals != null) ? mSparkPackageModals.size() : 0;
    }

    public class SparkPackageViewHolder extends RecyclerView.ViewHolder {
        //        @BindView(R.id.spark_package_iv)
//        ImageView mSparkPackageIV;
        @BindView(R.id.spark_package_title_tv)
        TextView mSparkPackageTitleTV;
        @BindView(R.id.spark_package_subtitle_tv)
        TextView mSparkPackageSubTitleTV;
        @BindView(R.id.spark_package_price)
        TextView mSparkPackagePriceTV;

        @BindView(R.id.redeem_container)
        View mRedeemContainer;
        @BindView(R.id.top_border)
        View mTopBorder;
        @BindView(R.id.bottom_border)
        View mBottomBorder;
        @BindView(R.id.match_guarantee)
        TextView mMatchGuaranteeTv;
        @BindView(R.id.tag)
        TextView mTagTV;
        @BindView(R.id.discount)
        TextView mDiscountTV;

        //        @BindView(R.id.sparks_counter_tv)
//        TextView mSparksCounterTV;
        View mParentView;

        public SparkPackageViewHolder(View itemView) {
            super(itemView);
            mParentView = itemView;
            ButterKnife.bind(this, itemView);
        }

        public void initialize(SparkPackageModal sparkPackageModal, int position) {
//            mSparkPackageTitleTV.setText(sparkPackageModal.getmTitle());
            mSparkPackageTitleTV.setText(Html.fromHtml(String.format(mContext.getString(R.string.get_count_sparks),
                    String.valueOf(sparkPackageModal.getmSparkCount()))));
            if (Utility.isSet(sparkPackageModal.getmSubtitle())) {
                mRedeemContainer.setVisibility(View.VISIBLE);
                mSparkPackageSubTitleTV.setText(sparkPackageModal.getmSubtitle());
            } else {
                mRedeemContainer.setVisibility(View.GONE);
            }

            mSparkPackagePriceTV.setText(String.format(mContext.getString(R.string.ruppee_value), sparkPackageModal.getmPrice()));

            if (position == (getItemCount() - 1)) {
                mTopBorder.setVisibility(View.VISIBLE);
                mBottomBorder.setVisibility(View.VISIBLE);
            } else {
                mTopBorder.setVisibility(View.VISIBLE);
                mBottomBorder.setVisibility(View.GONE);
            }

            if (!ABHandler.getABValue(mContext, ABHandler.ABTYPE.AB_SPARKS_MATCH_GUARANTEE_TAG)) {
                mMatchGuaranteeTv.setVisibility(View.GONE);

                if (sparkPackageModal.getmDiscount() > 0) {
                    mDiscountTV.setVisibility(View.VISIBLE);
                    mDiscountTV.setText(String.format(mContext.getString(R.string.save_number),
                            String.valueOf(sparkPackageModal.getmDiscount())) + "%");
                } else {
                    mDiscountTV.setVisibility(View.GONE);
                }
            } else {
                mDiscountTV.setVisibility(View.GONE);

                if (sparkPackageModal.isMatchGuarantee()) {
                    mMatchGuaranteeTv.setVisibility(View.VISIBLE);
                    mMatchGuaranteeTv.setText(Html.fromHtml(mContext.getResources().getString(R.string.match_guarantee) + "<sup>*</sup>"));
                } else {
                    mMatchGuaranteeTv.setVisibility(View.GONE);
                }
            }

            if (Utility.isSet(sparkPackageModal.getmTag())) {
                mTagTV.setText(sparkPackageModal.getmTag());
                mTagTV.setVisibility(View.VISIBLE);
            } else {
                mTagTV.setVisibility(View.GONE);
            }


//            mSparksCounterTV.setText(String.valueOf(sparkPackageModal.getmSparkCount()));

            mParentView.setTag(sparkPackageModal);
            mParentView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SparkPackageModal modal = (SparkPackageModal) v.getTag();
                    TmLogger.d("SparksBillingHandler", "Spark buy clicked in : " + modal.getmPackageSku());
                    mBuySparkEventListener.onBuySparkClicked(modal, matchId);
                }
            });
        }
    }
}
