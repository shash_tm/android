/**
 * 
 */
package com.trulymadly.android.app.adapter;

import android.app.Activity;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.listener.ConfirmDialogInterface;
import com.trulymadly.android.app.utility.AlertsHandler;

/**
 * @author udbhav
 *
 */
public class FacebookLoginCallback implements FacebookCallback<LoginResult> {

	private final String trkActivity;
	private final Activity aActivity;
	private final CallbackManager callbackManager;
	private final FacebookLoginCallbackInterface callbackInterface;
	private boolean forceBirthday = false, forcePhotos = false;
	private String mHeaderText1, mHeaderText2, mHeaderBirthdayString, mCentralBirthdayString,
		mFacebookString, mTrulyMadlyString, mCentralText1, mCentralText2;

	public FacebookLoginCallback(Activity aActivity, CallbackManager callbackManager,
								 FacebookLoginCallbackInterface callbackInterface, boolean forceBirthday, boolean forcePhotos, String trkActivity) {
		this.aActivity = aActivity;
		this.callbackManager = callbackManager;
		this.callbackInterface = callbackInterface;
		this.forceBirthday = forceBirthday;
		this.forcePhotos = forcePhotos;
		this.trkActivity = trkActivity;
		initializeStrings();
	}
	
	private void initializeStrings() {
		mCentralBirthdayString = "<b><font color=\"black\">" + aActivity.getResources().getString(R.string.birthday) + "</font></b>";
		mHeaderBirthdayString = "<b>" + aActivity.getResources().getString(R.string.birthday) + "</b>";
		mFacebookString = "<b>" + aActivity.getResources().getString(R.string.facebook) + "</b>";
		mTrulyMadlyString = "<b>" + aActivity.getResources().getString(R.string.truly_madly_bday_nudge) + "</b>";
		mHeaderText1 = aActivity.getResources().getString(R.string.fb_bday_header_part1);
		mHeaderText2 = aActivity.getResources().getString(R.string.fb_bday_header_part2);
		mCentralText1 = aActivity.getResources().getString(R.string.bday_nudge_message_1);
		mCentralText2 = aActivity.getResources().getString(R.string.bday_nudge_message_2);
	}

	@Override
	public void onSuccess(LoginResult loginResult) {
		boolean isBirthdayDeclined = loginResult.getAccessToken().getDeclinedPermissions().contains("user_birthday");
		boolean isPhotoDeclined = loginResult.getAccessToken().getDeclinedPermissions().contains("user_photos");
		final View view = aActivity.findViewById(R.id.facebook_nudge_layout);
		if (forceBirthday && isBirthdayDeclined) {
			TrulyMadlyTrackEvent.trackEvent(aActivity,
					trkActivity,
					TrulyMadlyEvent.TrulyMadlyEventTypes.fb_birthday_permission_shown, 0, null,
					null, true);
			callbackInterface.onFail();
			if(view != null){
				view.setVisibility(View.VISIBLE);

				String space = " ";
				((TextView)view.findViewById(R.id.fb_birthday_header)).setText(
						Html.fromHtml(mHeaderText1 + space + mHeaderBirthdayString + space + 
								mHeaderText2 + space + mFacebookString));
				
				((TextView)view.findViewById(R.id.fb_bday_desc_tv)).setText(
						Html.fromHtml(mTrulyMadlyString + space + mCentralText1 +
								space + mCentralBirthdayString + mCentralText2));
				
				view.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						
					}
				});
				view.findViewById(R.id.try_again_fb).setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						callbackInterface.onRetry();
						login();
					}
				});
				
				view.findViewById(R.id.fb_birthday_close).setOnClickListener(new OnClickListener() {
						
					@Override
					public void onClick(View v) {
						callbackInterface.onFail();
						view.setVisibility(View.GONE);
					}
				});
			}
		} else if (forcePhotos && isPhotoDeclined) {
			AlertsHandler.showConfirmDialog(aActivity, R.string.fb_re_ask_photo, R.string.log_in, R.string.cancel,
                    new ConfirmDialogInterface() {

                        @Override
                        public void onPositiveButtonSelected() {
                            login();
                        }

                        @Override
                        public void onNegativeButtonSelected() {
                            callbackInterface.onFail();
                        }
                    });
		} else {
			callbackInterface.onLogin(loginResult.getAccessToken().getToken(), isBirthdayDeclined, isPhotoDeclined);
			if(view != null)
				view.setVisibility(View.GONE);
		}
	}

	@Override
	public void onCancel() {
		LoginManager.getInstance().logOut();
		AlertsHandler.showMessage(aActivity, R.string.ERROR_FACEBOOK_FAILURE);
		callbackInterface.onFail();
	}

	@Override
	public void onError(FacebookException exception) {
		AlertsHandler.showMessage(aActivity, R.string.ERROR_FACEBOOK_FAILURE);
		logout();
		callbackInterface.onFail();
	}

	public void logout() {
		LoginManager.getInstance().logOut();
	}

	public void login() {
		LoginManager.getInstance().registerCallback(callbackManager, this);
		LoginManager.getInstance().logInWithReadPermissions(aActivity, Constants.FACEBOOK_READ_PERMISSIONS);
	}

	public interface FacebookLoginCallbackInterface {
		@SuppressWarnings("UnusedParameters")
		void onLogin(String accessToken, boolean isBirthdayDeclined, boolean isPhotoDeclined);

		void onFail();

		void onRetry();
	}

}
