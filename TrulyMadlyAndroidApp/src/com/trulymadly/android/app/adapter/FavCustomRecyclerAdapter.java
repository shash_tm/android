package com.trulymadly.android.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.trulymadly.android.app.R;
import com.trulymadly.android.app.modal.FavoriteDataModal;
import com.trulymadly.android.app.utility.FavoriteUtils;

import java.util.ArrayList;

public class FavCustomRecyclerAdapter
        extends RecyclerView.Adapter<MyFavoriteGridItem> {

    private Context aContext;
    private LayoutInflater aLayoutInflater;
    private View.OnClickListener onClickListener;
    private ArrayList<FavoriteDataModal> favList;

    public FavCustomRecyclerAdapter(Context context, View.OnClickListener editProfileClickListener) {
        aContext = context;
        this.onClickListener = editProfileClickListener;
        aLayoutInflater = (LayoutInflater) aContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getItemCount() {
        return favList == null ? 0 : favList.size();
    }

    public void changeData(ArrayList<FavoriteDataModal> favList) {
        this.favList = favList;
        notifyDataSetChanged();
    }

    @Override
    public MyFavoriteGridItem onCreateViewHolder(ViewGroup viewGroup, int position) {
        View itemView = aLayoutInflater.inflate(R.layout.favorite_item, viewGroup, false);
        return new MyFavoriteGridItem(itemView);
    }

    @Override
    public void onBindViewHolder(final MyFavoriteGridItem viewHolder, int position) {
        FavoriteDataModal favoriteData = favList.get(position);
        FavoriteUtils.bindView(aContext, viewHolder, favoriteData, onClickListener);
    }

}