package com.trulymadly.android.app.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.trulymadly.android.app.EditPartnerPreferencesNew;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.modal.EditPrefModel;
import com.trulymadly.android.app.utility.SPHandler;

import java.util.ArrayList;
import java.util.HashMap;

public class EditPrefAdapter extends ArrayAdapter<EditPrefModel> {

    private final ArrayList<EditPrefModel> list;
    private final Activity context;
    private final CheckBox other_row_cb_1;
    private final CheckBox other_row_cb_2;
    private final CheckBox other_row_cb_3;
    private String bachelorIds = null, masterIds = null;

    public EditPrefAdapter(Activity context, ArrayList<EditPrefModel> list,
                           CheckBox other_row_cb_1, CheckBox other_row_cb_2,
                           CheckBox other_row_cb_3) {
        super(context, R.layout.edit_pref_row_list_item);
        this.context = context;
        this.list = list;
        this.other_row_cb_1 = other_row_cb_1;
        this.other_row_cb_2 = other_row_cb_2;
        this.other_row_cb_3 = other_row_cb_3;

        bachelorIds = SPHandler.getString(context,
                ConstantsSP.SHARED_KEYS_BACHELORS);
        masterIds = SPHandler.getString(context,
                ConstantsSP.SHARED_KEYS_MASTERS);
    }

    public static boolean isIdBetweenLimits(int id, String limits) {
        if (limits != null && !(limits).equals("")) {
            String idArr[] = limits.split("-");
            int startId = Integer.parseInt(idArr[0]);
            int endId = Integer.parseInt(idArr[1]);
            return (id >= startId && id <= endId);
        }
        return false;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;

        final ViewHolder viewHolder;

        RelativeLayout editpref_menu_lay;

        if (convertView == null) {

            LayoutInflater inflator = context.getLayoutInflater();
            view = inflator.inflate(R.layout.edit_pref_row_list_item, null);

            editpref_menu_lay = (RelativeLayout) view
                    .findViewById(R.id.editpref_menu_lay);

            viewHolder = new ViewHolder();
            viewHolder.text = (TextView) view.findViewById(R.id.name);

            viewHolder.checkbox = (CheckBox) view
                    .findViewById(R.id.row_select_checkbox);
            viewHolder.checkbox
                    .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView,
                                                     boolean isChecked) {

                            EditPrefModel element = (EditPrefModel) viewHolder.checkbox
                                    .getTag();
                            element.setSelected(buttonView.isChecked());
                        }
                    });
            editpref_menu_lay.setTag(list.get(position).getID());
            editpref_menu_lay.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    int id = ((ViewHolder) (v.getTag())).id;
                    other_row_cb_1.setChecked(false);

                    if (other_row_cb_2.isChecked()) {
                        if (isIdBetweenLimits(id, bachelorIds)) {
                            other_row_cb_2.setChecked(false);
                        }
                    }
                    if (other_row_cb_3.isChecked()) {
                        if (isIdBetweenLimits(id, masterIds)) {
                            other_row_cb_3.setChecked(false);
                        }
                    }

                    viewHolder.checkbox.setChecked(!viewHolder.checkbox
                            .isChecked());
                }
            });
            view.setTag(viewHolder);
            viewHolder.checkbox.setTag(list.get(position));
        } else {
            view = convertView;
            ((ViewHolder) view.getTag()).checkbox.setTag(list.get(position));
        }

        ViewHolder holder = (ViewHolder) view.getTag();
        holder.text.setText(list.get(position).getName());
        holder.checkbox.setChecked(list.get(position).isSelected());
        holder.id = list.get(position).getID();

        return view;
    }

    public String[] getSelectedItems(int n) {

        String[] selectedItemArr = new String[2];

        String selectedNames = "";
        String selectedIds = "";

        boolean isFirstEle = true;
        for (EditPrefModel rel : list) {
            if (rel.isSelected()) {
                if (isFirstEle) {
                    selectedNames += rel.getName();
                    isFirstEle = false;
                    if (n == EditPartnerPreferencesNew.ANNUAL_INCOME)
                        selectedIds += rel.getStrID();
                    else
                        selectedIds += rel.getID();
                } else {
                    selectedNames += "," + rel.getName();
                    if (n == EditPartnerPreferencesNew.ANNUAL_INCOME)
                        selectedIds += "," + rel.getStrID();
                    else
                        selectedIds += "," + rel.getID();
                }
            }
        }

        selectedItemArr[0] = selectedNames;
        selectedItemArr[1] = selectedIds;

        return selectedItemArr;
    }

    public HashMap<String, Boolean> getSelectedItemIDs(int n) {

        HashMap<String, Boolean> selectedItemIDsMap = new HashMap<>();
        for (EditPrefModel rel : list) {
            if (rel.isSelected()) {
                if (n == EditPartnerPreferencesNew.ANNUAL_INCOME)
                    selectedItemIDsMap.put(rel.getStrID() + "", true);
                else
                    selectedItemIDsMap.put(rel.getID() + "", true);
            }
        }
        return selectedItemIDsMap;
    }

    class ViewHolder {
        TextView text;
        CheckBox checkbox;
        int id;
    }
}