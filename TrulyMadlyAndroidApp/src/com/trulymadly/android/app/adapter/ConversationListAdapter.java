package com.trulymadly.android.app.adapter;

import android.content.Context;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.graphics.Typeface;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FilterQueryProvider;
import android.widget.ImageView;
import android.widget.TextView;

import com.rockerhieu.emojicon.EmojiconTextView;
import com.squareup.picasso.Picasso;
import com.trulymadly.android.app.CircleTransformation;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.ads.BasicNativeAdEventsListener;
import com.trulymadly.android.app.ads.NativeAdConvItemUIUnit;
import com.trulymadly.android.app.json.Constants.MessageState;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.modal.ConversationModal;
import com.trulymadly.android.app.sqlite.MessageDBHandler;
import com.trulymadly.android.app.utility.ImageCacheHelper;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.TMSelectHandler;
import com.trulymadly.android.app.utility.TimeUtils;
import com.trulymadly.android.app.utility.Utility;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author udbhav
 */
public class ConversationListAdapter extends CursorAdapter {

    private final Context aContext;
    private final OnClickListener unmatchListener;
    private final OnClickListener shareListener;
    private final String gender;
    private final int tmSelectLabelFull;
    private Cursor cursor;

    //Native ads related
    private boolean isAdsEnabled = true;
    private int mAdPostion = 3;
    private NativeAdConvItemUIUnit mNativeAdConvItemUIUnit;
    private View mAdContainer;
    private BasicNativeAdEventsListener mBasicNativeAdEventsListener;

    public ConversationListAdapter(Context context, Cursor cursor,
                                   OnClickListener unmatchListener,
                                   OnClickListener shareListener,
                                   BasicNativeAdEventsListener basicNativeAdEventsListener,
                                   NativeAdConvItemUIUnit nativeAdConvItemUIUnit) {
        super(context, cursor, 0);
        this.aContext = context;
        setCursor(cursor);
        mNativeAdConvItemUIUnit = nativeAdConvItemUIUnit;
        this.unmatchListener = unmatchListener;
        this.shareListener = shareListener;
        this.mBasicNativeAdEventsListener = basicNativeAdEventsListener;
        gender = SPHandler.getString(aContext,
                ConstantsSP.SHARED_KEYS_USER_GENDER);
        tmSelectLabelFull = R.drawable.tm_select_label_full;
    }

    public void setCursor(Cursor cursor) {
        this.cursor = cursor;
        if (cursor != null) {
            calculateAdPosition(cursor.getCount());
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (!isAdsEnabled || !isAdFetched()) {
            return super.getView(position, convertView, parent);
        }

        if (!mDataValid) {
            throw new IllegalStateException("this should only be called when the cursor is valid");
        }


        boolean showAdd = false;
        if (position == mAdPostion) {
            showAdd = true;
        } else if (position > mAdPostion) {
            position--;
        }

        if (!showAdd && !mCursor.moveToPosition(position)) {
            throw new IllegalStateException("couldn't move cursor to position " + position);
        }
        View v;
        if (convertView == null) {
            v = newView(mContext, mCursor, parent);
        } else {
            v = convertView;
        }

        bindView(v, mContext, mCursor, showAdd);
        return v;
    }

    private boolean isAdFetched() {
        return mNativeAdConvItemUIUnit != null || mAdContainer != null;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.conversation_list_item, parent,
                false);
        ConversationMessageViewHolder cmvh = new ConversationMessageViewHolder(view);

        view.setTag(cmvh);
        return view;
    }

    @Override
    public int getCount() {
        int count = super.getCount();
        if (count > 0) {
            count += ((isAdsEnabled && isAdFetched()) ? 1 : 0);
        }

        return count;
    }

    private void calculateAdPosition(int count) {
        if (count >= 4)
            mAdPostion = 3;
        else
            mAdPostion = count;
    }

    private void bindView(final View view, Context context, Cursor cursor, boolean showAdd) {
        ((ViewGroup) view).removeView(mAdContainer);
        if (showAdd) {
            if (mAdContainer != null) {
                ConversationMessageViewHolder cmvh = (ConversationMessageViewHolder) view
                        .getTag();

                cmvh.profile_pic_spark_indicator.setVisibility(View.GONE);
                cmvh.profileTitleTv.setVisibility(View.GONE);
                cmvh.profileTitleTv.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
//                cmvh.conversationSelectLabel.setVisibility(View.GONE);
                cmvh.lastMessageTv.setVisibility(View.GONE);
                cmvh.conversationDateTv.setVisibility(View.GONE);
                cmvh.sponsoredTv.setVisibility(View.GONE);
                cmvh.conversationListActionBlockIv.setVisibility(View.GONE);
                cmvh.front.setBackgroundResource(R.drawable.conversation_list_item_front);
                cmvh.profilePicIv.setVisibility(View.GONE);

                //Hack to fix :
                //https://fabric.io/trulymadly/android/apps/com.trulymadly.android.app.t1/issues/5819943c0aeb16625b635fc3
                if (mAdContainer.getParent() != null) {
                    ((ViewGroup) mAdContainer.getParent()).removeView(mAdContainer);
                }
                ((ViewGroup) view).addView(mAdContainer);
                if (mBasicNativeAdEventsListener != null) {
                    mBasicNativeAdEventsListener.reportImpression();
                }
                return;
            }
            ConversationMessageViewHolder cmvh = (ConversationMessageViewHolder) view
                    .getTag();

            cmvh.profile_pic_spark_indicator.setVisibility(View.GONE);
            cmvh.profileTitleTv.setVisibility(View.VISIBLE);
            cmvh.profileTitleTv.setText(mNativeAdConvItemUIUnit.getmTitle());
            cmvh.profileTitleTv.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            //cmvh.conversationSelectLabel.setVisibility(View.GONE);
            cmvh.lastMessageTv.setVisibility(View.VISIBLE);
            cmvh.lastMessageTv.setText(mNativeAdConvItemUIUnit.getmDescription());
            cmvh.conversationDateTv.setVisibility(View.GONE);
            cmvh.sponsoredTv.setVisibility(View.VISIBLE);
            cmvh.conversationListActionBlockIv.setVisibility(View.GONE);
            //cmvh.profileTitleTv.setTypeface(null, Typeface.NORMAL);
            cmvh.lastMessageTv.setTypeface(null, Typeface.NORMAL);
            cmvh.lastMessageTv.setTextColor(aContext.getResources().getColor(R.color.grey_text_color));
            cmvh.front.setVisibility(View.VISIBLE);
            cmvh.front.setBackgroundResource(R.drawable.conversation_list_item_front);
            cmvh.profilePicIv.setVisibility(View.VISIBLE);
            Picasso.with(aContext).load(mNativeAdConvItemUIUnit.getmImageUrl()).transform(new CircleTransformation())
                    .into(cmvh.profilePicIv);

            if (mBasicNativeAdEventsListener != null) {
                mBasicNativeAdEventsListener.reportImpression();
            }
        } else {
            bindView(view, context, cursor);
        }
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        final ConversationMessageViewHolder cmvh = (ConversationMessageViewHolder) view
                .getTag();

        ConversationModal c = ConversationModal
                .getConversationFromDBCursor(cursor);

        cmvh.profileTitleTv.setVisibility(View.VISIBLE);
        //cmvh.conversationSelectLabel.setVisibility((TMSelectHandler.isSelectEnabled(aContext) && c.isSelectMatch()) ? View.VISIBLE : View.GONE);
        cmvh.profileTitleTv.setCompoundDrawablesWithIntrinsicBounds(0, 0, (TMSelectHandler.isSelectEnabled(aContext) && c.isSelectMatch()) ? tmSelectLabelFull : 0, 0);
        cmvh.lastMessageTv.setVisibility(View.VISIBLE);
        cmvh.front.setVisibility(View.VISIBLE);
        cmvh.profilePicIv.setVisibility(View.VISIBLE);

        if (c.isSparkReceived() && c.isMutualSpark()) {
            Picasso.with(aContext).load(R.drawable.ic_spark_indicator).into(cmvh.profile_pic_spark_indicator);
            cmvh.profile_pic_spark_indicator.setVisibility(View.VISIBLE);
        } else {
            cmvh.profile_pic_spark_indicator.setVisibility(View.GONE);
        }

        cmvh.profileTitleTv.setText(c.getFname());
        cmvh.conversationDateTv.setVisibility(View.VISIBLE);
        cmvh.sponsoredTv.setVisibility(View.GONE);
        if (c.getTstamp() != null) {
            if (c.getClear_chat_tstamp() != null && c.getClear_chat_tstamp().compareTo(c.getTstamp()) >= 0) {
                cmvh.conversationDateTv.setText(" ");
            } else {
                cmvh.conversationDateTv.setText(TimeUtils.getFormatteddateForChatItem(TimeUtils
                        .getFormattedTime(c.getTstamp())));
            }
        }
        setAllGone(cmvh);

        // setString background color
        switch (c.getLastMessageState()) {
            case INCOMING_DELIVERED:
            case MATCH_ONLY_NEW:
            case GCM_FETCHED:
            case CONVERSATION_FETCHED_NEW:
                cmvh.conversationDateTv.setTextColor(ActivityCompat.getColor(aContext, R.color.blue_text_color));
                //cmvh.profileTitleTv.setTypeface(null, Typeface.BOLD);
                cmvh.lastMessageTv.setTypeface(null, Typeface.NORMAL);
                cmvh.lastMessageTv.setTextColor(aContext.getResources().getColor(R.color.blue_text_color));
                break;
            default:
                cmvh.conversationDateTv.setTextColor(ActivityCompat.getColor(aContext, R.color.conv_date_color));
                //cmvh.profileTitleTv.setTypeface(null, Typeface.NORMAL);
                cmvh.lastMessageTv.setTypeface(null, Typeface.NORMAL);
                cmvh.lastMessageTv.setTextColor(aContext.getResources().getColor(R.color.grey_text_color));
                break;
        }

        if (c.getTstamp() != null) {

            if (c.getClear_chat_tstamp() != null && c.getClear_chat_tstamp().compareTo(c.getTstamp()) >= 0) {
                cmvh.lastMessageTv.setText(" ");
            } else {
                if (Utility.isSet(c.getLast_message())) {
                    String message = Utility.stripHtml(c.getLast_message());
                    String sentOrReceived = "received";
                    switch (c.getLastMessageState()) {
                        case OUTGOING_SENT:
                        case OUTGOING_DELIVERED:
                        case OUTGOING_READ:
                            sentOrReceived = "sent";
                            break;
                        case INCOMING_DELIVERED:
                        case INCOMING_READ:
                            sentOrReceived = "received";
                        default:
                            break;
                    }
                    switch (c.getLast_message_type()) {
                        case STICKER:
                            message = "You've " + sentOrReceived;
                            message += " a sticker!";
                            break;

                        case IMAGE:
                            message = "You've " + sentOrReceived;
                            message += " an image!";
                            break;

                        default:
                            break;
                    }

                    cmvh.lastMessageTv.setText(message);
                } else if (c.getLastMessageState() == MessageState.MATCH_ONLY
                        || c.getLastMessageState() == MessageState.MATCH_ONLY_NEW) {
                    String messageText = !Utility.isMale(aContext) ? "He" : "She" + " has liked you too. Say hello!";
                    cmvh.lastMessageTv.setText(messageText);
                } else {
                    cmvh.lastMessageTv.setText("");
                }
            }
        }


        int dummyImage;

        if (Utility.isSet(gender)) {
            dummyImage = Utility.stringCompare(gender, "m") ? R.drawable.dummy_female_circular
                    : R.drawable.dummy_male_circular;
        } else {
            dummyImage = R.drawable.dummyuser;
        }
        if (Utility.isSet(c.getProfile_pic())) {
            ImageCacheHelper.with(aContext).loadWithKey(c.getProfile_pic(), c.getMatch_id())
                    .placeholder(dummyImage)
                    .transform(new CircleTransformation())
                    .into(cmvh.profilePicIv);
        }
        if (Utility.stringCompare(gender, "f")) {
            Picasso.with(aContext).load(R.drawable.share_white).into(cmvh.shareIv);
            cmvh.share.setVisibility(View.VISIBLE);
            cmvh.share.setOnClickListener(shareListener);
            cmvh.share.setTag(c);
        }

        cmvh.unmatch.setTag(c);

        // setString blocked icon
        switch (c.getLastMessageState()) {
            case BLOCKED_SHOWN:
                // should never happen
                break;
            case BLOCKED_UNSHOWN:
                Picasso.with(aContext)
                        .load(R.drawable.blockedicon)
                        .into(cmvh.conversationListActionBlockIv);
                cmvh.conversationListActionBlockIv.setVisibility(View.VISIBLE);
//                cmvh.conversationListActionMessageIv.setVisibility(View.GONE);
                cmvh.lastMessageTv.setText("");
                break;
            default:
                Picasso.with(aContext)
                        .load(R.drawable.unmatch)
                        .into(cmvh.unmatchIv);
                cmvh.unmatch.setOnClickListener(unmatchListener);
                break;

        }
    }

    private void setAllGone(ConversationMessageViewHolder cmvh) {
        cmvh.conversationListActionBlockIv.setVisibility(View.GONE);
    }

    @SuppressWarnings({"UnusedParameters", "EmptyMethod"})
    public void setFrontClickable(int position, boolean isClickable) {
        // TODO: CONVERSATIONLIST: fix ui is swipe list view here if possible
    }

    public void changeCursor(Cursor cursor) {
        super.changeCursor(cursor);
        setCursor(cursor);
    }

    public void loadAd(NativeAdConvItemUIUnit nativeAdConvItemUIUnit) {
        mNativeAdConvItemUIUnit = nativeAdConvItemUIUnit;
        if (mAdContainer != null && mAdContainer.getParent() != null) {
            ((ViewGroup) mAdContainer.getParent()).removeView(mAdContainer);
        }
        mAdContainer = null;
        notifyDataSetChanged();
    }

    public void loadAd(View adContainer) {
        if (mAdContainer != null && mAdContainer.getParent() != null) {
            ((ViewGroup) mAdContainer.getParent()).removeView(mAdContainer);
        }
        mAdContainer = adContainer;
        mNativeAdConvItemUIUnit = null;
        notifyDataSetChanged();
    }

    public void removeAd() {
        if (mAdContainer != null && mAdContainer.getParent() != null) {
            ((ViewGroup) mAdContainer.getParent()).removeView(mAdContainer);
        }
        mAdContainer = null;
        mNativeAdConvItemUIUnit = null;
        notifyDataSetChanged();
    }

    public ConversationModal getConversation(int position) throws CursorIndexOutOfBoundsException {
        if (isAdsEnabled && isAdFetched() && position > mAdPostion)
            position--;
        cursor.moveToPosition(position);
        return ConversationModal.getConversationFromDBCursor(cursor);
    }

    public int getAdPosition() {
        if (!isAdsEnabled || !isAdFetched())
            return -1;
        else
            return mAdPostion;
    }

    public void toggleAds(boolean showAd) {
        if (showAd == isAdsEnabled) {
            return;
        }
        isAdsEnabled = showAd;
        notifyDataSetChanged();
    }

    /**
     * @param position
     * @return
     */
    public boolean isCurrentItemAd(int position) {
        return isAdsEnabled && isAdFetched() && position == mAdPostion;

    }

    public boolean isCurrentItemSwipable(int position) {
        if (isCurrentItemAd(position)) {
            return false;
        }

        try {
            ConversationModal c = getConversation(position);
            return !(c.getLastMessageState() == MessageState.BLOCKED_UNSHOWN || c.isMissTm());
        } catch (CursorIndexOutOfBoundsException ignored) {

        }
        return true;
    }

    @Override
    public FilterQueryProvider getFilterQueryProvider() {
        return new FilterQueryProvider() {
            @Override
            public Cursor runQuery(CharSequence constraint) {
                return MessageDBHandler.getConversationDetails(Utility.getMyId(aContext),
                        aContext, (constraint != null ? constraint.toString() : ""));
            }
        };
//        return super.getFilterQueryProvider();
    }

    public static class ConversationMessageViewHolder {
        @BindView(R.id.profile_title)
        TextView profileTitleTv;
        @BindView(R.id.conversation_select_label)
        View conversationSelectLabel;
        @BindView(R.id.conversation_date)
        TextView conversationDateTv;
        @BindView(R.id.sponsored_tv)
        TextView sponsoredTv;
        @BindView(R.id.last_message)
        EmojiconTextView lastMessageTv;
        @BindView(R.id.profile_pic)
        ImageView profilePicIv;
        @BindView(R.id.profile_pic_spark_indicator)
        ImageView profile_pic_spark_indicator;
        @BindView(R.id.conversation_list_action_block)
        ImageView conversationListActionBlockIv;
        @BindView(R.id.front)
        View front;
        @BindView(R.id.back)
        View back;
        @BindView(R.id.share)
        View share;
        @BindView(R.id.unmatch)
        View unmatch;
        @BindView(R.id.unmatchIv)
        ImageView unmatchIv;
        @BindView(R.id.shareIv)
        ImageView shareIv;

        public ConversationMessageViewHolder(View v) {
            ButterKnife.bind(this, v);
        }

        public TextView getProfileTitleTv() {
            return profileTitleTv;
        }

        public View getConversationSelectLabel() {
            return conversationSelectLabel;
        }

        public TextView getConversationDateTv() {
            return conversationDateTv;
        }

        public TextView getSponsoredTv() {
            return sponsoredTv;
        }

        public EmojiconTextView getLastMessageTv() {
            return lastMessageTv;
        }

        public ImageView getProfilePicIv() {
            return profilePicIv;
        }

        public ImageView getProfile_pic_spark_indicator() {
            return profile_pic_spark_indicator;
        }

        public ImageView getConversationListActionBlockIv() {
            return conversationListActionBlockIv;
        }

        public View getFront() {
            return front;
        }

        public View getBack() {
            return back;
        }

        public View getShare() {
            return share;
        }

        public View getUnmatch() {
            return unmatch;
        }

        public ImageView getUnmatchIv() {
            return unmatchIv;
        }

        public ImageView getShareIv() {
            return shareIv;
        }
    }
}
