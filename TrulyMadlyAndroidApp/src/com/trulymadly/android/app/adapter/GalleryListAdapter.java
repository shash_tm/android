package com.trulymadly.android.app.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.squareup.picasso.Picasso;
import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.listener.OnDataLoadedInterface;
import com.trulymadly.android.app.modal.StickerData;
import com.trulymadly.android.app.utility.FilesHandler;
import com.trulymadly.android.app.utility.ImageCacheHelper;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import static com.google.common.base.Preconditions.checkNotNull;

public class GalleryListAdapter extends
        RecyclerView.Adapter<GalleryListAdapter.GalleryListViewHolder> {

    private final ArrayList<Integer> galleryIds;
    private final LinkedHashMap<Integer, StickerData> hashMapGalleries;
    private final Activity aActivity;
    private OnClickInterface mClick;
    private int selectedIndex;

    public GalleryListAdapter(Activity aActivity,
                              LinkedHashMap<Integer, StickerData> hmap) {
        this.aActivity = aActivity;
        this.hashMapGalleries = hmap;
        this.galleryIds = new ArrayList<>();
        createGalleryIds();
    }

    public int getSelectedIndex() {
        return this.selectedIndex;
    }

    public void setSelectedIndex(int index) {
        this.selectedIndex = index;
    }

    @Override
    public GalleryListViewHolder onCreateViewHolder(ViewGroup parent,
                                                    int viewType) {

        final LayoutInflater mInflater = LayoutInflater.from(parent
                .getContext());
        final View sView = mInflater.inflate(R.layout.list_item_image, parent,
                false);
        return new GalleryListViewHolder(sView);

    }

    public void onItemChanged(int position) {
        String galleryId = "RECENT";
        if (position > 0) {
            galleryId = hashMapGalleries.get(galleryIds
                    .get(position - 1)).getId() + "";
        }
        TrulyMadlyTrackEvent.trackEvent(aActivity,
                TrulyMadlyEvent.TrulyMadlyActivities.sticker,
                TrulyMadlyEvent.TrulyMadlyEventTypes.gallery_shown,
                0, galleryId, null,
                true);
    }

    @Override
    public void onBindViewHolder(final GalleryListViewHolder holder,
                                 int position) {

        if (position == 0) {
            Picasso.with(aActivity).load(R.drawable.recent).noFade().into(holder.vImage);
            holder.vPhotoLoader.setVisibility(View.GONE);
        } else {

            final StickerData gallery = hashMapGalleries.get(galleryIds
                    .get(position - 1));
            holder.vPhotoLoader.setVisibility(View.VISIBLE);

            String url = FilesHandler.createStickerUrl(gallery, "thumbnail");
            String stickerKey = FilesHandler.getStickerKey(url);
            if (Utility.isSet(stickerKey)) {
                ImageCacheHelper.with(aActivity).loadWithKey(url, stickerKey).into(holder.vImage, new OnDataLoadedInterface() {
                    @Override
                    public void onLoadSuccess(long mTimeTaken) {
                        holder.vPhotoLoader.setVisibility(View.GONE);
                    }

                    @Override
                    public void onLoadFailure(Exception e, long mTimeTaken) {
                        holder.vPhotoLoader.setVisibility(View.GONE);
                    }
                });
            }
            holder.itemView.setTag(position);
        }

        if (position == this.selectedIndex) {
            holder.itemView.setBackgroundColor(aActivity.getResources()
                    .getColor(R.color.white));
        } else {
            UiUtils.setBackground(aActivity, holder.itemView, R.drawable.right_border_selected);
        }
    }

    @Override
    public int getItemCount() {
        return galleryIds.size() + 1;
    }

    public void setOnClickInterface(OnClickInterface onClick) {
        this.mClick = onClick;
    }

    private void createGalleryIds() {
        for (Entry<Integer, StickerData> integerStickerDataEntry : hashMapGalleries.entrySet()) {
            Entry<Integer, StickerData> pair = checkNotNull(integerStickerDataEntry);
            galleryIds.add(pair.getKey());
        }
    }

    public interface OnClickInterface {
        void onClick(int position);
    }

    public class GalleryListViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        public final ImageView vImage;
        public final ProgressBar vPhotoLoader;

        public GalleryListViewHolder(View view) {
            super(view);
            vImage = (ImageView) view.findViewById(R.id.list_image);
            vPhotoLoader = (ProgressBar) view.findViewById(R.id.sticker_loader);
            vImage.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (mClick != null)
                mClick.onClick(getAdapterPosition());
        }

    }

}
