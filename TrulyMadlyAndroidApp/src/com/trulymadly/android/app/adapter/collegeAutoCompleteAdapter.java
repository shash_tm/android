/**
 *
 */
package com.trulymadly.android.app.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.utility.OkHttpHandler;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @author udbhav
 */
class collegeAutoCompleteAdapter extends ArrayAdapter<String> implements
        Filterable {

    private static final String API_BASE = ConstantsUrls.get_colleges_url();
    private final Context aContext;
    private ArrayList<String> resultList;

    /**
     * @param context
     * @param resource
     */
    public collegeAutoCompleteAdapter(Context context, int resource) {
        super(context, resource);
        this.aContext = context;
    }

    @Override
    public int getCount() {
        return resultList.size();
    }

    @Override
    public String getItem(int index) {
        return resultList.get(index);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = super.getView(position, convertView, parent);
        //((TextView) v).setTypeface(Utility.getNormalFontTypeface(getContext()));
        ((TextView) v).setTextSize(20);
        ((TextView) v).setTextColor(getContext().getResources().getColor(R.color.final_bg_grey));
        return v;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    // Retrieve the autocomplete results.
                    resultList = autocomplete(constraint.toString());

                    // Assign the data to the FilterResults
                    filterResults.values = resultList;
                    filterResults.count = resultList.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint,
                                          FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };
    }

    private ArrayList<String> autocomplete(String input) {
        ArrayList<String> resultList = null;


        Map<String, String> params = new HashMap<>();
        String results;
        try {
            params.put("institute", "true");
            params.put("value", URLEncoder.encode(input, "utf8"));
            results = OkHttpHandler.httpGetSynchronous(aContext, API_BASE, params);
        } catch (UnsupportedEncodingException e) {
            Crashlytics.logException(e);
            return null;
        }


        if (Utility.isSet(results)) {
            try {
                JSONArray jsonArray = new JSONArray(results);

                resultList = new ArrayList<>(jsonArray.length());
                for (int i = 0; i < jsonArray.length(); i++) {
                    resultList.add(jsonArray.getJSONObject(i).getString("name"));
                }
            } catch (JSONException e) {
                Crashlytics.logException(e);
            }
        }

        return resultList;
    }

}
