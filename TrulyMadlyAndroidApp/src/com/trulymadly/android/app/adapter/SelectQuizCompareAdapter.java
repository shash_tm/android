package com.trulymadly.android.app.adapter;

import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.trulymadly.android.app.CircleTransformation;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.modal.SelectQuizQuesCompareResp;
import com.trulymadly.android.app.utility.Utility;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by avin on 25/10/16.
 */

public class SelectQuizCompareAdapter extends RecyclerView.Adapter<SelectQuizCompareAdapter.SelectQuizCompareVH> {

    private Context mContext;
    private String mMatchUrl, mUserUrl;
    private ArrayList<SelectQuizQuesCompareResp> mSelectQuizQuesCompareResps;
    private int mLikeColor, mHideColor;

    public SelectQuizCompareAdapter(Context mContext, String mMatchUrl, String mUserUrl,
                                    ArrayList<SelectQuizQuesCompareResp> mSelectQuizQuesCompareResps) {
        this.mContext = mContext;
        this.mMatchUrl = mMatchUrl;
        this.mUserUrl = mUserUrl;
        this.mSelectQuizQuesCompareResps = mSelectQuizQuesCompareResps;
        mLikeColor = ActivityCompat.getColor(mContext, R.color.colorSecondary);
        mHideColor = ActivityCompat.getColor(mContext, R.color.colorPrimary);
    }

    public void changeList(ArrayList<SelectQuizQuesCompareResp> mSelectQuizQuesCompareResps) {
        changeList(mSelectQuizQuesCompareResps, mMatchUrl, mUserUrl);
    }

    public void changeList(ArrayList<SelectQuizQuesCompareResp> mSelectQuizQuesCompareResps
            , String matchUrl, String userUrl) {
        this.mSelectQuizQuesCompareResps = mSelectQuizQuesCompareResps;
        this.mMatchUrl = matchUrl;
        this.mUserUrl = userUrl;
        notifyDataSetChanged();
    }

    @Override
    public SelectQuizCompareVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.select_quiz_result_item, parent, false);

        return new SelectQuizCompareVH(itemView);
    }

    @Override
    public void onBindViewHolder(SelectQuizCompareVH holder, int position) {
        holder.initialize(mSelectQuizQuesCompareResps.get(position));
    }

    @Override
    public int getItemCount() {
        return (mSelectQuizQuesCompareResps == null) ? 0 : mSelectQuizQuesCompareResps.size();
    }

    public class SelectQuizCompareVH extends RecyclerView.ViewHolder {

        @BindView(R.id.question_tv)
        TextView mQuestionTV;
        @BindView(R.id.match_iv)
        ImageView mMatchIV;
        @BindView(R.id.user_iv)
        ImageView mUserIV;
        @BindView(R.id.match_iv_boundry)
        View mMatchBoundry;
        @BindView(R.id.user_iv_boundry)
        View mUserBoundry;
        @BindView(R.id.common_answer_indicator)
        View mCommonAnswerIndicator;
        @BindView(R.id.right_connector)
        View mRightConnector;
        @BindView(R.id.left_connector)
        View mLeftConnector;
        @BindView(R.id.common_answer_iv_container)
        View mCommonAnswerIVContainer;
        @BindView(R.id.common_answer_iv)
        ImageView mCommonAnswerIV;

        @BindView(R.id.user_answer_indicator)
        View mUserAnswerIndicator;
        @BindView(R.id.user_answer_iv)
        ImageView mUserAnswerIV;
        @BindView(R.id.match_answer_indicator)
        View mMatchAnswerIndicator;
        @BindView(R.id.match_answer_iv)
        ImageView mMatchAnswerIV;


        public SelectQuizCompareVH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void initialize(SelectQuizQuesCompareResp selectQuizQuesCompareResp) {
            mQuestionTV.setText(selectQuizQuesCompareResp.getmQuestion());
            Picasso.with(mContext).load(mMatchUrl).transform(new CircleTransformation()).into(mMatchIV);
            if (Utility.isSet(mUserUrl)) {
                Picasso.with(mContext).load(mUserUrl).transform(new CircleTransformation()).into(mUserIV);
            } else {
                int dummyImage = (Utility.isMale(mContext)) ? R.drawable.dummy_male : R.drawable.dummy_female;
                Picasso.with(mContext).load(dummyImage).transform(new CircleTransformation()).into(mUserIV);
            }

            if (selectQuizQuesCompareResp.isAnsCommon()) {
                mCommonAnswerIndicator.setVisibility(View.VISIBLE);
                mLeftConnector.setBackgroundColor((selectQuizQuesCompareResp.isUserLiked()) ? mLikeColor : mHideColor);
                mRightConnector.setBackgroundColor((selectQuizQuesCompareResp.isUserLiked()) ? mLikeColor : mHideColor);
                mCommonAnswerIVContainer.setBackgroundResource((selectQuizQuesCompareResp.isUserLiked())
                        ? R.drawable.circle_pink : R.drawable.circle_blue);
                Picasso.with(mContext).load((selectQuizQuesCompareResp.isUserLiked())
                        ? R.drawable.tickmark : R.drawable.ic_dislike).into(mCommonAnswerIV);
                mCommonAnswerIVContainer.setBackgroundResource((selectQuizQuesCompareResp.isUserLiked())
                        ? R.drawable.circle_pink : R.drawable.circle_blue);

                mUserAnswerIndicator.setVisibility(View.INVISIBLE);
                mUserAnswerIV.setVisibility(View.INVISIBLE);
                mMatchAnswerIndicator.setVisibility(View.INVISIBLE);
                mMatchAnswerIV.setVisibility(View.INVISIBLE);
            } else {
                mCommonAnswerIndicator.setVisibility(View.INVISIBLE);

                mUserAnswerIndicator.setBackgroundResource((selectQuizQuesCompareResp.isUserLiked())
                        ? R.drawable.circle_pink : R.drawable.circle_blue);
                Picasso.with(mContext).load((selectQuizQuesCompareResp.isUserLiked())
                        ? R.drawable.tickmark : R.drawable.ic_dislike).into(mUserAnswerIV);
                mMatchAnswerIndicator.setBackgroundResource((selectQuizQuesCompareResp.isMatchLiked())
                        ? R.drawable.circle_pink : R.drawable.circle_blue);
                Picasso.with(mContext).load((selectQuizQuesCompareResp.isMatchLiked())
                        ? R.drawable.tickmark : R.drawable.ic_dislike).into(mMatchAnswerIV);

                mUserAnswerIndicator.setVisibility(View.VISIBLE);
                mUserAnswerIV.setVisibility(View.VISIBLE);
                mMatchAnswerIndicator.setVisibility(View.VISIBLE);
                mMatchAnswerIV.setVisibility(View.VISIBLE);
            }

            mUserBoundry.setBackgroundResource((selectQuizQuesCompareResp.isUserLiked()) ?
                    R.drawable.circle_boundry_pink : R.drawable.circle_boundry_blue);
            mMatchBoundry.setBackgroundResource((selectQuizQuesCompareResp.isMatchLiked()) ?
                    R.drawable.circle_boundry_pink : R.drawable.circle_boundry_blue);

        }

    }
}
