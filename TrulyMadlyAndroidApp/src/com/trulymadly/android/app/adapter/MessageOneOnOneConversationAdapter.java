package com.trulymadly.android.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rockerhieu.emojicon.EmojiconTextView;
import com.squareup.picasso.Picasso;
import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.CircleTransformation;
import com.trulymadly.android.app.MessageModal;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.bus.CancelEvent;
import com.trulymadly.android.app.bus.QueryEvent;
import com.trulymadly.android.app.custom.DigitalTimerView;
import com.trulymadly.android.app.custom.SlideListener;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.listener.OnDataLoadedInterface;
import com.trulymadly.android.app.modal.CuratedDealsChatsModal;
import com.trulymadly.android.app.modal.EventChatModal;
import com.trulymadly.android.app.modal.MatchMessageMetaData;
import com.trulymadly.android.app.modal.QuizData;
import com.trulymadly.android.app.modal.QuizImage;
import com.trulymadly.android.app.modal.SparkModal;
import com.trulymadly.android.app.sqlite.DateSpotsDBHandler;
import com.trulymadly.android.app.sqlite.MessageDBHandler;
import com.trulymadly.android.app.sqlite.SparksDbHandler;
import com.trulymadly.android.app.utility.ActivityHandler;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.CropTransformation;
import com.trulymadly.android.app.utility.GradientTransformation;
import com.trulymadly.android.app.utility.ImageCacheHelper;
import com.trulymadly.android.app.utility.MaskTransformation;
import com.trulymadly.android.app.utility.PermissionsHelper;
import com.trulymadly.android.app.utility.SparksHandler;
import com.trulymadly.android.app.utility.TimeUtils;
import com.trulymadly.android.app.utility.TmLogger;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;

import java.io.FileNotFoundException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.annotation.Nullable;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.trulymadly.android.app.MessageModal.MessageType.CD_ASK;
import static com.trulymadly.android.app.MessageModal.MessageType.CD_VOUCHER;
import static com.trulymadly.android.app.MessageModal.MessageType.LOAD_MORE;
import static com.trulymadly.android.app.MessageModal.MessageType.SPARK_HEADER;
import static com.trulymadly.android.app.MessageModal.MessageType.TEXT;

/**
 * Created by avin on 01/04/16.
 */
public class MessageOneOnOneConversationAdapter extends RecyclerView.Adapter<MessageOneOnOneConversationAdapter.ChatViewHolder> {

    private final String TAG = "MessageOneOnOneConversationAdapter";

    private final ArrayList<MessageModal> mMessageModals;
    private final Context mContext;
    private final int mScreenWidthPx;
    private final int mTextColorGray;
    private final GradientTransformation mCDGradientTransformation;
    private final CropTransformation mCDAskCropTransformation;
    private final CropTransformation mCDVoucherCropTransformation;
    private final MaskTransformation mCDAskMaskTransformationSent;
    private final MaskTransformation mCDAskMaskTransformationReceived;
    private final MaskTransformation mCDVoucherMaskTransformation;
    private final HashMap<String, Boolean> mIdToDownloadingMap;
    private final HashMap<String, ProgressBar> mIdToProgressbarMap;
    private final Activity mActivity;
    private final View.OnClickListener retryMessageClickListener;
    private final View.OnClickListener onQuizItemInChatClickedListener;
    private final View.OnClickListener onCDChatItemClickedListener;
    private final View.OnClickListener onBlogLinkItemInChatClickedListener;
    private final View.OnClickListener onEventChatItemClickedListener;
    private final View.OnClickListener onLoadMoreClickedListener;
    private String last_seen_receiver_tstamp;
    private boolean isLoadMoreEnabled = false;
    private boolean isLoadMoreAdded = false;
    private boolean isSparkHeaderRequired = false;
    private String mMatchId;
    private Handler mHandler;
    private SlideListener mSlideListener;
    private SparkModal mSparkModal;
    private MatchMessageMetaData mSparkmatchMsgMetadata;

    public MessageOneOnOneConversationAdapter(ArrayList<MessageModal> mMessageModals, Activity activity,
                                              String last_seen_receiver_tstamp,
                                              double screenWidth, boolean isLoadMoreEnabled, View.OnClickListener retryMessageClickListener,
                                              View.OnClickListener onQuizItemInChatClickedListener,
                                              View.OnClickListener onCDChatItemClickedListener,
                                              View.OnClickListener onBlogLinkItemInChatClickedListener,
                                              View.OnClickListener onLoadMoreClickedListener,
                                              View.OnClickListener onEventChatItemClickedListener,
                                              String match_id, boolean isSparkHeaderRequired, SlideListener mSlideListener) {
        this.mMessageModals = mMessageModals;
        this.mContext = activity;
        this.mActivity = activity;
        this.last_seen_receiver_tstamp = last_seen_receiver_tstamp;
        this.retryMessageClickListener = retryMessageClickListener;
        this.onQuizItemInChatClickedListener = onQuizItemInChatClickedListener;
        this.onCDChatItemClickedListener = onCDChatItemClickedListener;
        this.onBlogLinkItemInChatClickedListener = onBlogLinkItemInChatClickedListener;
        this.onLoadMoreClickedListener = onLoadMoreClickedListener;
        this.onEventChatItemClickedListener = onEventChatItemClickedListener;
        this.mMatchId = match_id;
        this.mSlideListener = mSlideListener;

        mScreenWidthPx = (int) screenWidth - UiUtils.dpToPx(16);
        mCDGradientTransformation = new GradientTransformation(new int[]{Color.parseColor("#CC403224"),
                Color.parseColor("#3F957E68"), Color.parseColor("#CC261D15")});
        mCDAskCropTransformation = new CropTransformation(UiUtils.dpToPx(216), UiUtils.dpToPx(166), CropTransformation.CropType.CENTER);
        mCDVoucherCropTransformation = new CropTransformation(mScreenWidthPx, UiUtils.dpToPx(180), CropTransformation.CropType.CENTER);
        mCDAskMaskTransformationSent = new MaskTransformation(mContext, R.drawable.message_mask_right);
        mCDAskMaskTransformationReceived = new MaskTransformation(mContext, R.drawable.message_mask_left);
        mCDVoucherMaskTransformation = new MaskTransformation(mContext, R.drawable.voucher_mask);
        mTextColorGray = ActivityCompat.getColor(mContext, R.color.waiting_response_gray);

        mIdToProgressbarMap = new HashMap<>();
        mIdToDownloadingMap = new HashMap<>();
        addSparkHeader(isSparkHeaderRequired);
        toggleLoadMore(isLoadMoreEnabled);
    }

    public void setLastSeenReceiverTstamp(String last_seen_receiver_tstamp) {
        this.last_seen_receiver_tstamp = last_seen_receiver_tstamp;
    }

    public void changeList(ArrayList<MessageModal> messageModals,
                           boolean isLoadMoreEnabled, boolean isSparkHeaderRequired, String match_id) {
        modifyMessageModals(MESSAGE_MODALS_ACTION.CHANGE_LIST, null, -1,
                messageModals, isLoadMoreEnabled, isSparkHeaderRequired, match_id, null, true);
    }

    public void addSparkHeader(boolean isSparkHeaderRequired) {
        this.isSparkHeaderRequired = isSparkHeaderRequired;
        if (isSparkHeaderRequired) {
            if (mSparkModal == null || !Utility.stringCompare(mSparkModal.getmMatchId(), mMatchId)) {
                mSparkModal = SparksDbHandler.getSpark(mContext, Utility.getMyId(mContext), mMatchId);
                mSparkmatchMsgMetadata = MessageDBHandler.getMatchMessageMetaData(Utility.getMyId(mContext), mMatchId, mContext);
            }
            MessageModal sparkHeaderMessageModal = new MessageModal(Utility.getMyId(mContext));
            sparkHeaderMessageModal.setMessageType(SPARK_HEADER.name());
            modifyMessageModals(MESSAGE_MODALS_ACTION.ADD_ITEM, sparkHeaderMessageModal, 0, null, false, false, null, null, true);
        }
    }

    public void hideSparkHeader() {
        this.isSparkHeaderRequired = false;
        int newPosition = ((isLoadMoreAdded) ? 1 : 0);
        if (modifyMessageModals(MESSAGE_MODALS_ACTION.GET_ITEM, null, newPosition,
                null, false, false, null, null, true).getMessageType() == SPARK_HEADER) {
            removeAt(newPosition, true);
        }
    }

    public void toggleLoadMore(boolean isLoadMoreEnabled) {
        this.isLoadMoreEnabled = isLoadMoreEnabled;
        boolean refreshList = false;
        if (isLoadMoreEnabled) {
            if (!isLoadMoreAdded) {
                MessageModal messageModal = new MessageModal(Utility.getMyId(mContext));
                messageModal.setMessageType(MessageModal.MessageType.LOAD_MORE.name());
                modifyMessageModals(MESSAGE_MODALS_ACTION.ADD_ITEM, messageModal, 0, null, false, false, null, null, true);
                isLoadMoreAdded = true;
                refreshList = true;
            }
        } else if (isLoadMoreAdded) {
            removeAt(0, false);
            isLoadMoreAdded = false;
            refreshList = true;
        }

        if (refreshList) {
            notifyDataSetChanged();
        }
    }

    public void updateProgress(String messageId, int progress) {
        if (progress == -1) {
            return;
        }
        ProgressBar progressBar = mIdToProgressbarMap.get(messageId);
        if (progressBar != null) {
            progressBar.setProgress(progress);
        }
    }

    public void replaceAnItem(String oldMessageId, MessageModal messageModal, int newPosition) {
        modifyMessageModals(MESSAGE_MODALS_ACTION.REPLACE_AND_MOVE_ITEM_ON_NEW_POSITION, messageModal, newPosition, null, false, false, null, oldMessageId, true);
    }

    public void replaceAnItem(String oldMessageId, MessageModal messageModal) {
        modifyMessageModals(MESSAGE_MODALS_ACTION.REPLACE_ITEM, messageModal, -1, null, false, false, null, oldMessageId, true);
    }

    public void updateAnItem(MessageModal messageModal) {
        modifyMessageModals(MESSAGE_MODALS_ACTION.UPDATE_ITEM, messageModal, -1, null, false, false, null, null, true);
    }

    /**
     * @param oldMessageId
     * @param action                  MESSAGE_MODALS_ACTION
     * @param messageModal            Default null    : For UPDATE_ITEM, ADD_ITEM, REPLACE_ITEM, REPLACE_AND_MOVE_ITEM_ON_NEW_POSITION
     * @param position                Default -1      : For REMOVE_ITEM_BY_POSITION, ADD_ITEM, REPLACE_AND_MOVE_ITEM_ON_NEW_POSITION, GET_ITEM
     * @param newMessageModals        Default null    : For CHANGE_LIST
     * @param isLoadMoreEnabled       Default false   : For CHANGE_LIST
     * @param isSparkHeaderRequired   Default false   : For CHANGE_LIST
     * @param match_id                Default null    : For CHANGE_LIST
     * @param messageId               Default null    : For REPLACE_ITEM, REPLACE_AND_MOVE_ITEM_ON_NEW_POSITION
     * @param notifyAfterModification Default true    : For REMOVE_ITEM_BY_POSITION, REMOVE_ITEM_BY_MESSAGE_ID
     */
    synchronized private MessageModal modifyMessageModals(MESSAGE_MODALS_ACTION action,
                                                          MessageModal messageModal,
                                                          int position,
                                                          ArrayList<MessageModal> newMessageModals, boolean isLoadMoreEnabled,
                                                          boolean isSparkHeaderRequired, String match_id,
                                                          String messageId,
                                                          boolean notifyAfterModification) {
        switch (action) {
            case UPDATE_ITEM:
                if (mMessageModals != null) {
                    int existingMessageModalIndex = mMessageModals.indexOf(messageModal);
                    if (existingMessageModalIndex != -1) {
                        mMessageModals.remove(existingMessageModalIndex);
                        mMessageModals.add(existingMessageModalIndex, messageModal);
                        notifyItemChanged(existingMessageModalIndex);
                    }
                }
                break;
            case REMOVE_ITEM_BY_POSITION:
                if (mMessageModals == null || mMessageModals.size() < position || position < 0) {
                    return null;
                }
                mMessageModals.remove(position);
                if (notifyAfterModification) {
                    notifyItemRemoved(position);
                    //notifyItemRangeChanged(position, mMessageModals.size());
                }
                break;
            case REMOVE_ITEM_BY_MESSAGE_ID:
                if (mMessageModals == null) {
                    return null;
                }
                MessageModal messageModalTemp = new MessageModal(Utility.getMyId(mContext));
                messageModalTemp.setMsg_id(messageId);
                position = mMessageModals.indexOf(messageModalTemp);
                mMessageModals.remove(position);
                if (notifyAfterModification) {
                    notifyItemRemoved(position);
                }
                break;
            case CHANGE_LIST:
                this.mMessageModals.clear();
                this.mMessageModals.addAll(newMessageModals);
                this.isLoadMoreEnabled = false;
                this.isLoadMoreAdded = false;
                this.mMatchId = match_id;
                addSparkHeader(isSparkHeaderRequired);
                toggleLoadMore(isLoadMoreEnabled);
                notifyDataSetChanged();
                break;
            case ADD_ITEM:
                if (mMessageModals == null || messageModal == null || position < 0) {
                    return null;
                }
                mMessageModals.add(position, messageModal);
                break;
            case REPLACE_ITEM:
                if (mMessageModals != null && Utility.isSet(messageId)) {
                    MessageModal oldMessageModal = new MessageModal(Utility.getMyId(mContext));
                    oldMessageModal.setMsg_id(messageId);
                    int existingMessageModalIndex = mMessageModals.indexOf(oldMessageModal);
                    if (existingMessageModalIndex != -1) {
                        mMessageModals.remove(existingMessageModalIndex);
                        mMessageModals.add(existingMessageModalIndex, messageModal);
                        notifyItemChanged(existingMessageModalIndex);
                    }
                }
                break;
            case REPLACE_AND_MOVE_ITEM_ON_NEW_POSITION:
                if (mMessageModals != null && Utility.isSet(messageId)) {
                    MessageModal oldMessageModal = new MessageModal(Utility.getMyId(mContext));
                    oldMessageModal.setMsg_id(messageId);
                    int existingMessageModalIndex = mMessageModals.indexOf(oldMessageModal);
                    if (existingMessageModalIndex != -1) {
                        mMessageModals.remove(existingMessageModalIndex);
                        if (isLoadMoreAdded) {
                            position++;
                        }

                        //Fix for : https://fabric.io/trulymadly/android/apps/com.trulymadly.android.app/issues/57583717ffcdc0425002c749
                        if (position > mMessageModals.size()) {
                            position = mMessageModals.size();
                        }

                        //Fix for : https://fabric.io/trulymadly/android/apps/com.trulymadly.android.app.t1/issues/5763892cffcdc042506ab27c
                        //JIRA : https://trulymadly.atlassian.net/browse/ANDROID-2371
                        if (position < 0) {
                            position = existingMessageModalIndex;
                        }

                        mMessageModals.add(position, messageModal);
                        if (position == existingMessageModalIndex) {
                            notifyItemChanged(existingMessageModalIndex);
                        } else {
                            if (existingMessageModalIndex > position) {
                                existingMessageModalIndex = position;
                            }
                            notifyItemRangeChanged(existingMessageModalIndex, mMessageModals.size());
                        }
                    }
                }
                break;
            case GET_ITEM:
                return mMessageModals.get(position);
            default:
                break;
        }
        return null;
    }

    public void removeAt(int position, boolean notifyAfterModification) {
        modifyMessageModals(MESSAGE_MODALS_ACTION.REMOVE_ITEM_BY_POSITION, null, position, null, false, false, null, null, notifyAfterModification);
    }

    public void removeItem(String messageId) {
        modifyMessageModals(MESSAGE_MODALS_ACTION.REMOVE_ITEM_BY_MESSAGE_ID, null, -1, null, false, false, null, messageId, true);
    }

    @Override
    public ChatViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ChatViewHolder messageViewHolder = null;
        View itemLayoutView = null;

        MessageModal.MessageType messageType = MessageModal.MessageType.getMessageTypeFromValue(viewType);
        switch (messageType) {
            case LOAD_MORE:
                itemLayoutView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.msg_item_load_more, null);
                messageViewHolder = new LoadMoreViewHolder(itemLayoutView);
                break;
            case TEXT:
            case SPARK:
                itemLayoutView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.msg_item_text, null);
                messageViewHolder = new TextViewHolder(itemLayoutView);
                break;
            case STICKER:
                itemLayoutView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.msg_item_sticker, null);
                messageViewHolder = new StickerViewHolder(itemLayoutView);
                break;
            case MUTUAL_LIKE:
                //Handling dummy message type when server sents this as there is no message exhanged between users yet.
                break;
            case NUDGE_SINGLE:
            case NUDGE_DOUBLE:
                itemLayoutView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.msg_item_quiz, null);
                messageViewHolder = new NudgeViewHolder(itemLayoutView);
                break;
            case QUIZ_MESSAGE:
                itemLayoutView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.msg_item_quiz_msg, null);
                messageViewHolder = new QuizViewHolder(itemLayoutView);
                break;
            case CD_ASK:
                itemLayoutView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.msg_item_cd_nudge, null);
                messageViewHolder = new CDAskViewHolder(itemLayoutView);
                break;
            case CD_VOUCHER:
                itemLayoutView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.msg_item_cd_voucher, null);
                messageViewHolder = new CDVoucherViewHolder(itemLayoutView);
                break;
            case IMAGE:
                itemLayoutView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.msg_item_image, null);
                messageViewHolder = new ImageViewHolder(itemLayoutView);
                break;
            case IMAGE_LINK:
                itemLayoutView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.msg_item_imglink, null);
                messageViewHolder = new ImageLinkViewHolder(itemLayoutView);
                break;
            case SHARE_EVENT:
                itemLayoutView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.msg_item_share_event, null);
                messageViewHolder = new EventViewHolder(itemLayoutView);
                break;
            case SPARK_HEADER:
                itemLayoutView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.msg_item_spark_header, null);
                messageViewHolder = new SparkHeaderViewHolder(itemLayoutView);
                break;
        }
        return messageViewHolder;
    }

    @Override
    public void onBindViewHolder(ChatViewHolder holder, int position) {
        TmLogger.d(TAG, "onBindViewHolder ::: position : " + position +
                " mMessageModals.size() : " + mMessageModals.size());
        MessageModal messageModal = modifyMessageModals(MESSAGE_MODALS_ACTION.GET_ITEM, null, position,
                null, false, false, null, null, true);
        if (messageModal != null) {
            holder.initialize(messageModal);
            holder.initializeRetryContainer(messageModal, position);
        }
    }

    /*
     * code for showing the time and ticker to be used by text message and
     * image(sticker)
     */
    private boolean showTimeAndTicker(MessageModal messageModal, ChatViewHolder cvh) {

        boolean toRetry = false;

        checkNotNull(cvh.msg_read_image).setVisibility(View.GONE);
        checkNotNull(cvh.msg_send_image).setVisibility(View.GONE);
        checkNotNull(cvh.msg_fail_image).setVisibility(View.GONE);

        switch (messageModal.getMessageState()) {
            case OUTGOING_READ:
            case OUTGOING_DELIVERED:
            case OUTGOING_SENT:
                if (!messageModal.getMatch_id().equalsIgnoreCase("admin")) {
                    if (Utility.isSet(last_seen_receiver_tstamp)
                            && last_seen_receiver_tstamp.compareTo(messageModal.getTstamp()) >= 0) {
                        cvh.msg_read_image.setVisibility(View.VISIBLE);
                    } else {
                        cvh.msg_send_image.setVisibility(View.VISIBLE);
                    }
                }
                checkNotNull(cvh.time).setText(messageModal.getMessageDisplayTime());
                break;
            case OUTGOING_FAILED:
                toRetry = true;
                cvh.msg_fail_image.setVisibility(View.VISIBLE);
                int messageId = R.string.not_delivered;
                if (messageModal.getMessageType() == CD_ASK) {
                    messageId = R.string.not_delivered_cd_ask;
                } else if (messageModal.getMessageType() == CD_VOUCHER) {
                    messageId = R.string.not_delivered_cd_voucher;
                }
                checkNotNull(cvh.time).setText(messageId);
                break;
            case OUTGOING_SENDING:
                checkNotNull(cvh.time).setText(R.string.sending);
                break;
            case INCOMING_DELIVERED:
            case INCOMING_READ:
                checkNotNull(cvh.time).setText(messageModal.getMessageDisplayTime());
                break;

            default:
                break;
        }
        return toRetry;
    }

    /**
     * @param context
     * @param messageModal
     * @return CuratedDealsChatsModal
     * <p/>
     * Checks if the given messageModal has a curatedDealsChatModal.
     * If yes, returns it.
     * Else fetches it from DB, sets it and returns it.
     */
    private CuratedDealsChatsModal getAndSetCuratedDealsChatModal(Context context, MessageModal messageModal) {
        CuratedDealsChatsModal curatedDealsChatsModal = messageModal.getmCuratedDealsChatsModal();
        if (curatedDealsChatsModal == null) {
            curatedDealsChatsModal = DateSpotsDBHandler.getDateSpot(Utility.getMyId(context),
                    messageModal.getmDateSpotId(), context);
            if (curatedDealsChatsModal == null)
                return null;
            if (messageModal.getmDealId() != null)
                curatedDealsChatsModal.setmDealId(messageModal.getmDealId());
            curatedDealsChatsModal.setmMessageType(messageModal.getMessageType());
            messageModal.setmCuratedDealsChatsModal(curatedDealsChatsModal);
        }

        return curatedDealsChatsModal;
    }

    @Override
    public int getItemCount() {
        return (mMessageModals != null) ? mMessageModals.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        //Fixing : https://fabric.io/trulymadly/android/apps/com.trulymadly.android.app/issues/5758069dffcdc042500097a5
        MessageModal messageModal = modifyMessageModals(MESSAGE_MODALS_ACTION.GET_ITEM, null, position,
                null, false, false, null, null, true);
        if (messageModal != null) {
            return messageModal.getMessageType().getValue();
        } else {
            return TEXT.getValue();
        }
    }

    private enum MESSAGE_MODALS_ACTION {
        GET_ITEM,
        ADD_ITEM,
        UPDATE_ITEM,
        REPLACE_ITEM,
        REPLACE_AND_MOVE_ITEM_ON_NEW_POSITION,
        REMOVE_ITEM_BY_MESSAGE_ID,
        REMOVE_ITEM_BY_POSITION,
        CHANGE_LIST
    }

    public abstract class ChatViewHolder extends RecyclerView.ViewHolder {
        @Nullable
        @BindView(R.id.wrapper_container_holder)
        LinearLayout wrapper_container_holder;
        @Nullable
        @BindView(R.id.wrapper_container)
        LinearLayout wrapper_container;
        @Nullable
        @BindView(R.id.msg_fail_image)
        ImageView msg_fail_image;
        @Nullable
        @BindView(R.id.msg_read_image)
        View msg_read_image;
        @Nullable
        @BindView(R.id.msg_send_image)
        View msg_send_image;
        @Nullable
        @BindView(R.id.time)
        TextView time;
        @Nullable
        @BindView(R.id.show_date_id)
        View date_layout;
        @Nullable
        @BindView(R.id.show_date)
        TextView show_date;
        private RelativeLayout wrapper_layout;

        public ChatViewHolder(View itemLayoutView) {
            super(itemLayoutView);
//            ButterKnife.bind(this, itemLayoutView);
        }

        public RelativeLayout getWrapper_layout() {
            return wrapper_layout;
        }

        public void setWrapper_layout(RelativeLayout wrapper_layout) {
            this.wrapper_layout = wrapper_layout;
        }

        public LinearLayout getWrapper_container() {
            return checkNotNull(wrapper_container);
        }

        public abstract void initialize(MessageModal messageModal);

        public void initializeRetryContainer(final MessageModal messageModal, final int position) {
            if (messageModal.getMessageType() == LOAD_MORE ||
                    messageModal.getMessageType() == SPARK_HEADER) {
                return;
            }

            Picasso.with(mContext).load(R.drawable.msg_fail_red).noFade().into(msg_fail_image);
            boolean toRetry = showTimeAndTicker(messageModal, this);
            if (toRetry) {
                msg_fail_image.setTag(messageModal);
                getWrapper_container().setTag(messageModal);
                msg_fail_image.setOnClickListener(retryMessageClickListener);
                getWrapper_container().setOnClickListener(retryMessageClickListener);
                wrapper_layout.setOnClickListener(null);
            } else {
                switch (messageModal.getMessageType()) {
                    case NUDGE_DOUBLE:
                    case NUDGE_SINGLE:
                    case QUIZ_MESSAGE:
                        wrapper_layout.setTag(messageModal);
                        checkNotNull(msg_fail_image).setOnClickListener(null);
                        getWrapper_container().setOnClickListener(null);
                        wrapper_layout.setOnClickListener(onQuizItemInChatClickedListener);
                        break;
                    case CD_ASK:
                    case CD_VOUCHER:
                        wrapper_layout.setTag(messageModal);
                        checkNotNull(msg_fail_image).setOnClickListener(null);
                        getWrapper_container().setOnClickListener(null);
                        //We dont want the message to be clickable in case of "sending" state as well
                        if (messageModal.getMessageState() == Constants.MessageState.OUTGOING_SENDING)
                            wrapper_layout.setOnClickListener(null);
                        else
                            wrapper_layout.setOnClickListener(onCDChatItemClickedListener);
                        break;
                    case IMAGE:
                        wrapper_layout.setTag(messageModal);
                        checkNotNull(msg_fail_image).setOnClickListener(null);
                        getWrapper_container().setOnClickListener(null);
                        break;
                    case IMAGE_LINK:
                        wrapper_layout.setTag(messageModal);
                        wrapper_layout.setOnClickListener(onBlogLinkItemInChatClickedListener);
                        break;
                    case SHARE_EVENT:
                        wrapper_layout.setTag(messageModal);
                        checkNotNull(msg_fail_image).setOnClickListener(null);
                        getWrapper_container().setOnClickListener(null);
                        if (messageModal.getMessageState() == Constants.MessageState.OUTGOING_SENDING) {
                            wrapper_layout.setOnClickListener(null);
                        } else {
                            wrapper_layout.setOnClickListener(onEventChatItemClickedListener);
                        }
                        break;
                    default:
                        checkNotNull(msg_fail_image).setOnClickListener(null);
                        getWrapper_container().setOnClickListener(null);
                        wrapper_layout.setOnClickListener(null);
                }
            }

            // check if we have to show the date
            String view_date = messageModal.getMessageDisplayDate();

            // for setting the date row
            if (Utility.isSet(view_date)) {
                String previous_date = null;
                if (position > 0) {
                    MessageModal messageModalPrevious = modifyMessageModals(MESSAGE_MODALS_ACTION.GET_ITEM, null, position - 1,
                            null, false, false, null, null, true);
                    previous_date = messageModalPrevious.getMessageDisplayDate();
                }

                if (Utility.stringCompare(previous_date, view_date)) {
                    date_layout.setVisibility(View.GONE);
                } else {
                    show_date.setText(view_date);
                    date_layout.setVisibility(View.VISIBLE);
                }
            }

            wrapper_container.setGravity(messageModal.isMessageSentByMe() ? Gravity.RIGHT : Gravity.LEFT);
            wrapper_container_holder.setGravity(messageModal.isMessageSentByMe() ? Gravity.RIGHT : Gravity.LEFT);

        }
    }

    class SparkHeaderViewHolder extends ChatViewHolder {

        @BindView(R.id.spark_header_card)
        View spark_header_card;
        @BindView(R.id.spark_image_center)
        ImageView spark_image_center;
        @BindView(R.id.spark_image_left)
        ImageView spark_image_left;
        @BindView(R.id.spark_image_right)
        ImageView spark_image_right;
        @BindView(R.id.spark_name_tv)
        TextView spark_name_tv;
        @BindView(R.id.spark_designation_tv)
        TextView spark_designation_tv;
        @BindView(R.id.reply_to_get_matched_tv)
        DigitalTimerView reply_to_get_matched_tv;
        private View mItemView;

        public SparkHeaderViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            mItemView = itemLayoutView;
            ButterKnife.bind(SparkHeaderViewHolder.this, itemLayoutView);
            mSlideListener.initialize(spark_header_card);
            spark_header_card.setOnTouchListener(mSlideListener);

            if (mSparkModal != null && mSparkModal.getmStartTime() > 0) {
                long timeLeftInMillis = TimeUtils.getTimeoutDifference(mSparkModal.getmStartTime(),
                        mSparkModal.getmExpiredTimeInSeconds() * 1000);
                long timeLeftInSecs = timeLeftInMillis / 1000;
                int interval = 1;
                if (timeLeftInSecs > 60 * 60) {
                    interval = 60;
                }
                reply_to_get_matched_tv.startTimer((int) (TimeUtils.getTimeoutDifference(
                        mSparkModal.getmStartTime(), mSparkModal.getmExpiredTimeInSeconds() * 1000) / 1000), interval * 1000);
            } else {
                reply_to_get_matched_tv.setText(R.string.reply_to_get_matched);
            }

            if (SparksHandler.shouldShowSparkMsgOneononeAnim(mContext)) {
                mItemView.post(new Runnable() {
                    @Override
                    public void run() {
                        mSlideListener.startTutorialAnimation();
                        SparksHandler.incSparkMsgOneononeAnimCounter(mContext);
                    }
                });
            }
        }

        @Override
        public void initialize(MessageModal messageModal) {
            MatchMessageMetaData matchMessageMetaData = mSparkmatchMsgMetadata;

            if (matchMessageMetaData == null) {
                matchMessageMetaData = MessageDBHandler.getMatchMessageMetaData(Utility.getMyId(mContext), mMatchId, mContext);
            }

            if (matchMessageMetaData.isMutualSpark()) {
                mItemView.setVisibility(View.GONE);
                return;
            } else {
                mItemView.setVisibility(View.VISIBLE);
            }
            String location = "";
            if (mSparkModal != null && Utility.isSet(mSparkModal.getmCity())) {
                location = " " + String.format(mContext.getString(R.string.sparks_header_city), mSparkModal.getmCity());
            }
            String age = "";
            if (Utility.isSet(matchMessageMetaData.getAge())) {
                age = " " + String.format(mContext.getString(R.string.sparks_header_age), matchMessageMetaData.getAge());
            }

            String text = matchMessageMetaData.getFName() + age + location;

//            String text = String.format(mContext.getString(R.string.sparks_header_name), mMatchMessageMetaData.getFName(),
//                    mMatchMessageMetaData.getAge() + " Years", location);
            spark_name_tv.setText(Html.fromHtml(text));
            if (mSparkModal != null && Utility.isSet(mSparkModal.getmDesignation())) {
                spark_designation_tv.setVisibility(View.VISIBLE);
                spark_designation_tv.setText(mSparkModal.getmDesignation());
            } else {
                spark_designation_tv.setVisibility(View.GONE);
            }

            setImages(mSparkModal, matchMessageMetaData);
        }

        private void setImages(SparkModal sparkModal, MatchMessageMetaData matchMessageMetaData) {
            int totalImages = 1;
            String[] imagesArray = (sparkModal != null) ? sparkModal.getmImagesArray() : (null);
            totalImages += ((imagesArray == null) ? 0 : imagesArray.length);
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) spark_image_center.getLayoutParams();
            switch (totalImages) {
                case 1:
                    Picasso.with(mContext).load(matchMessageMetaData.getProfilePic())
                            .placeholder(UiUtils.getProfileDummyImageForMatch(mContext))
                            .transform(new CircleTransformation()).into(spark_image_center);
                    spark_image_center.setVisibility(View.VISIBLE);
                    spark_image_left.setVisibility(View.GONE);
                    spark_image_right.setVisibility(View.GONE);
                    layoutParams.leftMargin = UiUtils.dpToPx(0);
                    break;
                case 2:
                    Picasso.with(mContext).load(matchMessageMetaData.getProfilePic())
                            .placeholder(UiUtils.getProfileDummyImageForMatch(mContext))
                            .transform(new CircleTransformation()).into(spark_image_center);
                    Picasso.with(mContext).load(imagesArray[0])
                            .placeholder(UiUtils.getProfileDummyImageForMatch(mContext))
                            .transform(new CircleTransformation()).into(spark_image_right);
                    spark_image_center.setVisibility(View.VISIBLE);
                    spark_image_left.setVisibility(View.GONE);
                    spark_image_right.setVisibility(View.VISIBLE);
                    layoutParams.leftMargin = UiUtils.dpToPx(0);
                    break;
                default:
                    Picasso.with(mContext).load(matchMessageMetaData.getProfilePic())
                            .placeholder(UiUtils.getProfileDummyImageForMatch(mContext))
                            .transform(new CircleTransformation()).into(spark_image_center);
                    Picasso.with(mContext).load(imagesArray[0])
                            .placeholder(UiUtils.getProfileDummyImageForMatch(mContext))
                            .transform(new CircleTransformation()).into(spark_image_right);
                    Picasso.with(mContext).load(imagesArray[1])
                            .placeholder(UiUtils.getProfileDummyImageForMatch(mContext))
                            .transform(new CircleTransformation()).into(spark_image_left);
                    spark_image_center.setVisibility(View.VISIBLE);
                    spark_image_left.setVisibility(View.VISIBLE);
                    spark_image_right.setVisibility(View.VISIBLE);
                    layoutParams.leftMargin = UiUtils.dpToPx(60);
                    break;
            }
            spark_image_center.setLayoutParams(layoutParams);
        }
    }

    class LoadMoreViewHolder extends ChatViewHolder {

        @BindView(R.id.load_more_tv)
        TextView mLoadMoreTV;

        public LoadMoreViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            ButterKnife.bind(LoadMoreViewHolder.this, itemLayoutView);
            mLoadMoreTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    toggleLoadMore(false);
                    onLoadMoreClickedListener.onClick(v);
                }
            });
        }

        @Override
        public void initialize(MessageModal messageModal) {

        }
    }

    class TextViewHolder extends ChatViewHolder {
        @BindView(R.id.comment)
        EmojiconTextView message;

        public TextViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            ButterKnife.bind(TextViewHolder.this, itemLayoutView);
            setWrapper_layout((RelativeLayout) itemLayoutView.findViewById(R.id.wrapper));
        }

        @Override
        public void initialize(MessageModal messageModal) {
            message.setText(messageModal.getDisplayMessage());
            // setting the blub color based on who sent the message
            try {
                getWrapper_layout().setBackgroundResource(
                        messageModal.isMessageSentByMe() ? R.drawable.chat_pink_round_rect_right : R.drawable.chat_yellow_round_rect_left);
            } catch (OutOfMemoryError ignored) {
            }
        }
    }

    class StickerViewHolder extends ChatViewHolder {
        @BindView(R.id.sticker_image)
        ImageView sticker_image;
        @BindView(R.id.photo_loader)
        ProgressBar photoLoader;

        public StickerViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            ButterKnife.bind(this, itemLayoutView);

            setWrapper_layout((RelativeLayout) itemLayoutView.findViewById(R.id.wrapper_image));
        }

        @Override
        public void initialize(MessageModal messageModal) {
            String image_url = messageModal.getAndSetOptStickerImageUrl(mContext);
            if (Utility.isSet(image_url)) {
                String stickerKey = messageModal.getAndSetOptStickerKey(mContext);
                if (Utility.isSet(stickerKey)) {
                    sticker_image.setVisibility(View.INVISIBLE);
                    photoLoader.setVisibility(View.VISIBLE);
                    ImageCacheHelper.with(mContext).loadWithKey(image_url, stickerKey)
                            .into(sticker_image, new OnDataLoadedInterface() {
                                @Override
                                public void onLoadSuccess(long mTimeTaken) {
                                    photoLoader.setVisibility(View.GONE);
                                    sticker_image.setVisibility(View.VISIBLE);
                                }

                                @Override
                                public void onLoadFailure(Exception e, long mTimeTaken) {
                                    photoLoader.setVisibility(View.GONE);
                                    sticker_image.setVisibility(View.VISIBLE);
                                }
                            });


                } else {
                    Picasso.with(mContext).load(image_url).into(sticker_image);
                }
            }
        }
    }

    class NudgeViewHolder extends ChatViewHolder {
        @BindView(R.id.quiz_nudge_name)
        TextView quiz_name;
        @BindView(R.id.quiz_nudge_text)
        TextView quiz_nudge_text;
        @BindView(R.id.quiz_nudge_image)
        ImageView nudge_image;

        public NudgeViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            ButterKnife.bind(this, itemLayoutView);
            setWrapper_layout((RelativeLayout) itemLayoutView.findViewById(R.id.quiz_nudge_wrapper));
        }

        @Override
        public void initialize(MessageModal messageModal) {
            quiz_name.setText(messageModal.getAndSetOptQuizName(mContext));
            quiz_nudge_text.setText(messageModal.getAndSetOptNewMessageText(mContext));
            QuizImage qi = messageModal.getAndSetOptQuizImage(mContext);
            if (qi != null) {
                ImageCacheHelper.with(mContext)
                        .loadWithKey(qi.getThumbnailServerLocation(),
                                QuizData.getImageCacheKeyFromId(messageModal.getQuiz_id()))
                        .transform(new CircleTransformation())
                        .into(nudge_image, null);
            }

            try {
                getWrapper_layout().setBackgroundResource(
                        messageModal.isMessageSentByMe() ? R.drawable.chat_pink_round_rect_right : R.drawable.chat_yellow_round_rect_left);
            } catch (OutOfMemoryError ignored) {
            }
        }
    }

    class QuizViewHolder extends ChatViewHolder {
        @BindView(R.id.quiz_message_text)
        EmojiconTextView quiz_message_text;
        @BindView(R.id.quiz_message_image_flare)
        ImageView quiz_message_image_flare;
        @BindView(R.id.quiz_message_image)
        ImageView quiz_message_image;

        public QuizViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            ButterKnife.bind(this, itemLayoutView);
            setWrapper_layout((RelativeLayout) itemLayoutView.findViewById(R.id.wrapper_quiz_message));
        }

        @Override
        public void initialize(MessageModal messageModal) {
            Picasso.with(mContext).load(R.drawable.quiz_flare_flame).noFade()
                    .into(quiz_message_image_flare);
            quiz_message_text.setText(messageModal.getDisplayMessage());
            boolean toShowFlare = messageModal.getAndSetOptToShowVariable(mContext);
            quiz_message_image_flare.setVisibility(toShowFlare ? View.VISIBLE : View.GONE);

            QuizImage quizImage = messageModal.getAndSetOptQuizMsgImage(mContext);
            if (quizImage != null) {
                ImageCacheHelper.with(mContext)
                        .loadWithKey(quizImage.getThumbnailServerLocation(),
                                QuizData.getImageCacheKeyFromId(messageModal.getQuiz_id()))
                        .transform(new CircleTransformation())
                        .into(quiz_message_image, null);
            }
            try {
                getWrapper_layout().setBackgroundResource(
                        messageModal.isMessageSentByMe() ? R.drawable.chat_pink_round_rect_right : R.drawable.chat_yellow_round_rect_left);
            } catch (OutOfMemoryError ignored) {
            }
        }
    }

    class CDAskViewHolder extends ChatViewHolder {
        @BindView(R.id.cd_nudge_text)
        TextView cdNudgeMessageTV;
        @BindView(R.id.cd_nudge_image)
        ImageView cdNudgeIV;

        public CDAskViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            ButterKnife.bind(this, itemLayoutView);
            setWrapper_layout((RelativeLayout) itemLayoutView.findViewById(R.id.cd_nudge_wrapper));
        }

        @Override
        public void initialize(MessageModal messageModal) {
            CuratedDealsChatsModal askCuratedDealsChatsModal =
                    getAndSetCuratedDealsChatModal(mContext, messageModal);

            MaskTransformation maskTransformation =
                    messageModal.getAndSetOptCDAskMaskTransformation(
                            mCDAskMaskTransformationSent, mCDAskMaskTransformationReceived);
            int askPlaceholder = messageModal.getAndSetOptCDAskPlaceholderDrawable(
                    R.drawable.cd_nudge_placeholder_right, R.drawable.cd_nudge_placeholder_left);

            if (askCuratedDealsChatsModal != null) {
                cdNudgeMessageTV.setTextColor(mTextColorGray);
                ImageCacheHelper.with(mContext).loadWithKey(askCuratedDealsChatsModal.getmRectUri(),
                        messageModal.getmDateSpotId())
                        .placeholder(askPlaceholder)
                        .resize(UiUtils.dpToPx(332), UiUtils.dpToPx(166))
                        .transform(mCDAskCropTransformation)
                        .transform(mCDGradientTransformation)
                        .transform(maskTransformation)
                        .into(cdNudgeIV, new OnDataLoadedInterface() {
                            @Override
                            public void onLoadSuccess(long mTimeTaken) {
                                cdNudgeMessageTV.setTextColor(Color.WHITE);
                            }

                            @Override
                            public void onLoadFailure(Exception e, long mTimeTaken) {

                            }
                        });
            }
            cdNudgeMessageTV.setText(messageModal.getMessage());
        }
    }

    class CDVoucherViewHolder extends ChatViewHolder {
        @BindView(R.id.cd_voucher_text)
        TextView cdVoucherMessageTV;
        @BindView(R.id.cd_voucher_address_text)
        TextView cdVoucherAddressTV;
        @BindView(R.id.cd_voucher_image)
        ImageView cdVoucherIV;
        @BindView(R.id.cd_voucher_overlay)
        View cdVoucherBorder;

        public CDVoucherViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            ButterKnife.bind(this, itemLayoutView);
            setWrapper_layout((RelativeLayout) itemLayoutView.findViewById(R.id.cd_voucher_wrapper));
        }

        @Override
        public void initialize(MessageModal messageModal) {
            final CuratedDealsChatsModal voucherCuratedDealsChatsModal =
                    getAndSetCuratedDealsChatModal(mContext, messageModal);
            if (voucherCuratedDealsChatsModal != null) {
                //cdVoucherIV.setScaleType(ImageView.ScaleType.FIT_XY);
                cdVoucherMessageTV.setTextColor(mTextColorGray);
                cdVoucherAddressTV.setTextColor(mTextColorGray);
                ImageCacheHelper.with(mContext).loadWithKey(voucherCuratedDealsChatsModal.getmRectUri(),
                        messageModal.getmDateSpotId())
                        .placeholder(R.drawable.voucher_placeholder)
                        .resize(mScreenWidthPx, mScreenWidthPx / 2)
                        .transform(mCDVoucherCropTransformation)
                        .transform(mCDGradientTransformation)
                        .transform(mCDVoucherMaskTransformation)
                        .into(cdVoucherIV, new OnDataLoadedInterface() {
                            @Override
                            public void onLoadSuccess(long mTimeTaken) {
                                cdVoucherMessageTV.setTextColor(Color.WHITE);
                                cdVoucherAddressTV.setTextColor(Color.WHITE);
                                //cdVoucherIV.setScaleType(ImageView.ScaleType.FIT_CENTER);
                                cdVoucherBorder.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onLoadFailure(Exception e, long mTimeTaken) {

                            }
                        });

                cdVoucherAddressTV.setText(voucherCuratedDealsChatsModal.getmAddress());
            }
            cdVoucherMessageTV.setText(messageModal.getMessage());
        }
    }

    class ImageViewHolder extends ChatViewHolder {
        public ProgressBar mImageProgressbar;
        public boolean isFullViewAllowed;

        @BindView(R.id.image_wrapper)
        RelativeLayout image_wrapper;
        @BindView(R.id.image_iv)
        ImageView image_iv;
        @BindView(R.id.image_size)
        TextView image_size;
        @BindView(R.id.image_retry_iv)
        ImageView image_retry_iv;

        @BindView(R.id.image_download_progressbar)
        ProgressBar image_download_progressbar;

        @BindView(R.id.progressbar_container)
        View progressbar_container;

        @BindView(R.id.image_progressbar)
        ProgressBar image_progressbar;
        @BindView(R.id.upload_container)
        View upload_container;
        @BindView(R.id.cancel_iv)
        ImageView cancel_iv;

        public ImageViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            ButterKnife.bind(this, itemLayoutView);
            setWrapper_layout((RelativeLayout) itemLayoutView.findViewById(R.id.image_wrapper));
        }

        @Override
        public void initialize(final MessageModal messageModal) {
            final Constants.MessageState imageMessageState = messageModal.getMessageState();
            final MaskTransformation imageTransformation = messageModal
                    .getAndSetOptImageMaskTransformation(mCDAskMaskTransformationSent, mCDAskMaskTransformationReceived);
            final int imagePlaceholder = messageModal.getAndSetOptImagePlaceholderDrawable(
                    R.drawable.cd_nudge_placeholder_right, R.drawable.cd_nudge_placeholder_left);
            final boolean isWebPUsed = messageModal.getAndSetOptIsWebPUsed();
            final String uri = messageModal.getAndSetOptImageUri();
            String fallback = messageModal.getAndSetOptFallbackUri();

            final boolean isOriginalImage = Utility.isSet(uri) && ImageCacheHelper.with(mContext)
                    .loadInFolder(uri, messageModal.getMatch_id())
                    .config(Bitmap.Config.RGB_565)
//                    .tag(PicassoOnScrollListener.TAG)
                    .fallback(fallback, new OnDataLoadedInterface() {

                        @Override
                        public void onLoadSuccess(long mTimeTaken) {
                            HashMap<String, String> eventInfo = new HashMap<>();
                            String size = messageModal.getAndSetOptImageSize();
                            if (Utility.isSet(size)) {
                                eventInfo.put("size", size);
                            }
                            eventInfo.put("match_id", messageModal.getMatch_id());
                            //Adding permission info to the event_info
                            eventInfo.put("storage_permission", String.valueOf(PermissionsHelper.hasPermission(mActivity,
                                    android.Manifest.permission.READ_EXTERNAL_STORAGE)));
                            //Trk
                            TrulyMadlyTrackEvent.trackEvent(mContext,
                                    TrulyMadlyEvent.TrulyMadlyActivities.photo_share,
                                    TrulyMadlyEvent.TrulyMadlyEventTypes.download_thumbnail, mTimeTaken,
                                    TrulyMadlyEvent.TrulyMadlyEventStatus.success,
                                    eventInfo, true, false);
                            //GA
                            TrulyMadlyTrackEvent.trackEvent(mContext,
                                    TrulyMadlyEvent.TrulyMadlyActivities.photo_share,
                                    TrulyMadlyEvent.TrulyMadlyEventTypes.download_thumbnail_success, mTimeTaken,
                                    TrulyMadlyEvent.TrulyMadlyEventStatus.success,
                                    null, false, true, true);
                        }

                        @Override
                        public void onLoadFailure(Exception e, long mTimeTaken) {
                            String message = null;
                            if (e != null && e instanceof SocketTimeoutException || e instanceof ConnectException) {
                                message = mContext.getString(R.string.ERROR_NETWORK_FAILURE);
                            } else {
                                message = mContext.getString(R.string.image_download_failed);
                            }

                            HashMap<String, String> eventInfo = new HashMap<>();
                            eventInfo.put("reason", (e != null) ? e.getMessage() : message);
                            eventInfo.put("match_id", messageModal.getMatch_id());
                            String size = messageModal.getAndSetOptImageSize();
                            if (Utility.isSet(size)) {
                                eventInfo.put("size", size);
                            }
                            //Trk
                            TrulyMadlyTrackEvent.trackEvent(mContext,
                                    TrulyMadlyEvent.TrulyMadlyActivities.photo_share,
                                    TrulyMadlyEvent.TrulyMadlyEventTypes.download_thumbnail, mTimeTaken,
                                    TrulyMadlyEvent.TrulyMadlyEventStatus.fail,
                                    eventInfo, true, false);
                            //GA
                            TrulyMadlyTrackEvent.trackEvent(mContext,
                                    TrulyMadlyEvent.TrulyMadlyActivities.photo_share,
                                    TrulyMadlyEvent.TrulyMadlyEventTypes.download_thumbnail_fail, mTimeTaken,
                                    (e != null) ? e.getMessage() : message, null, false, true, true);
                        }
                    })
                    .placeholder(imagePlaceholder)
                    .resize(UiUtils.dpToPx(432), UiUtils.dpToPx(332))
                    .transform(imageTransformation)
                    .centerCrop()
                    .into(image_iv);

            if (Utility.isSet(uri)) {
                image_iv.setTag(uri);
            }

            switch (imageMessageState) {
                case OUTGOING_READ:
                case OUTGOING_DELIVERED:
                case OUTGOING_SENT:
                case INCOMING_DELIVERED:
                case INCOMING_READ:
                    mImageProgressbar = image_download_progressbar;
                    upload_container.setVisibility(View.GONE);
                    image_progressbar.setVisibility(View.GONE);
                    image_progressbar.setProgress(1);
                    image_progressbar.setProgress(0);
                    image_download_progressbar.setVisibility(View.GONE);
                    image_retry_iv.setVisibility(View.GONE);
                    progressbar_container.setVisibility(View.GONE);
                    break;

                case OUTGOING_FAILED:
                    mImageProgressbar = image_progressbar;
                    image_download_progressbar.setVisibility(View.GONE);
                    upload_container.setVisibility(View.GONE);
                    image_progressbar.setProgress(1);
                    image_progressbar.setProgress(0);
                    image_progressbar.setVisibility(View.GONE);
                    Picasso.with(mContext).load(R.drawable.retry).into(image_retry_iv);
                    image_retry_iv.setVisibility(View.VISIBLE);
                    progressbar_container.setVisibility(View.VISIBLE);
                    break;

                case OUTGOING_SENDING:
                    mImageProgressbar = image_progressbar;
                    image_download_progressbar.setVisibility(View.GONE);
                    upload_container.setVisibility(View.VISIBLE);
                    image_progressbar.setVisibility(View.VISIBLE);
                    image_retry_iv.setVisibility(View.GONE);
                    progressbar_container.setVisibility(View.VISIBLE);
                    mIdToProgressbarMap.put(messageModal.getMsg_id(), image_progressbar);
                    if (mImageProgressbar.getProgress() <= 1) {
                        if (Utility.isSet(messageModal.getmWebPUrl()) || Utility.isSet(messageModal.getmJpegUrl())) {
                            mImageProgressbar.setProgress(100);
                        } else {
                            Utility.fireBusEvent(mContext, true,
                                    new QueryEvent(messageModal.getMatch_id(), 0));
                        }
                    }
                    break;

                default:
                    break;
            }

            isFullViewAllowed = (messageModal.isMessageSentByMe() &&
                    (imageMessageState != Constants.MessageState.OUTGOING_FAILED
                            && imageMessageState != Constants.MessageState.OUTGOING_SENDING))
                    || (!messageModal.isMessageSentByMe() && isOriginalImage);

            if (!messageModal.isMessageSentByMe() && !isOriginalImage) {
                progressbar_container.setVisibility(View.VISIBLE);
                if (mIdToDownloadingMap.containsKey(messageModal.getMsg_id())) {
                    mImageProgressbar.setVisibility(View.VISIBLE);
                } else {
                    Picasso.with(mContext).load(R.drawable.download).into(image_retry_iv);
                    image_retry_iv.setVisibility(View.VISIBLE);
                    image_size.setVisibility(View.VISIBLE);
                    image_size.setText((isWebPUsed) ? messageModal.getmWebPSize() : messageModal.getmJpegSize());
                }
            } else {
                image_size.setVisibility(View.GONE);
            }

            Picasso.with(mContext).load(R.drawable.crossicon).noFade().into(cancel_iv);
            cancel_iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utility.fireBusEvent(mContext, true, new CancelEvent(messageModal.getMsg_id(), messageModal.getMatch_id()));
                }
            });

            image_iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isFullViewAllowed) {
                        String uri = String.valueOf(image_iv.getTag());
//                        if (Utility.isSet(uri) && !uri.contains("Pending_")) { //Old Implementation
                        if (Utility.isSet(uri)
                                && !Utility.stringCompare(messageModal.getmLocalUrl(), uri)) { // New implementation
                            ActivityHandler.startFullImageViewerActivity(mContext, new String[]{uri},
                                    false, true, messageModal.getMatch_id());
                        }
                    } else {
                        //Message received
                        if (!messageModal.isMessageSentByMe()) {

                            if (!(PermissionsHelper.checkAndAskForPermission(mActivity,
                                    android.Manifest.permission.READ_EXTERNAL_STORAGE,
                                    PermissionsHelper.INITIAL_STORAGE_PERMISSION_CODE))) {
                                return;
                            }

                            mImageProgressbar.setVisibility(View.VISIBLE);
                            image_retry_iv.setVisibility(View.GONE);
                            mIdToDownloadingMap.put(messageModal.getMsg_id(), true);
                            ImageCacheHelper.with(mContext)
                                    .loadInFolder(uri, messageModal.getMatch_id())
                                    .config(Bitmap.Config.RGB_565)
//                                    .tag(PicassoOnScrollListener.TAG)
                                    .transform(imageTransformation)
                                    .fetch(new OnDataLoadedInterface() {
                                        @Override
                                        public void onLoadSuccess(long mTimeTaken) {
                                            ImageCacheHelper.with(mContext)
                                                    .loadInFolder(uri, messageModal.getMatch_id())
//                                                    .tag(PicassoOnScrollListener.TAG)
                                                    .placeholder(imagePlaceholder)
                                                    .resize(UiUtils.dpToPx(432), UiUtils.dpToPx(332))
                                                    .transform(imageTransformation)
                                                    .centerCrop()
                                                    .into(image_iv);

                                            HashMap<String, String> eventInfo = new HashMap<>();
                                            if (Utility.isSet(image_size.getText().toString())) {
                                                eventInfo.put("size", image_size.getText().toString());
                                            }
                                            eventInfo.put("match_id", messageModal.getMatch_id());
                                            //Trk
                                            TrulyMadlyTrackEvent.trackEvent(mContext,
                                                    TrulyMadlyEvent.TrulyMadlyActivities.photo_share,
                                                    TrulyMadlyEvent.TrulyMadlyEventTypes.download_image, mTimeTaken,
                                                    TrulyMadlyEvent.TrulyMadlyEventStatus.success,
                                                    eventInfo, true, false);
                                            //GA
                                            TrulyMadlyTrackEvent.trackEvent(mContext,
                                                    TrulyMadlyEvent.TrulyMadlyActivities.photo_share,
                                                    TrulyMadlyEvent.TrulyMadlyEventTypes.download_image_success, mTimeTaken,
                                                    TrulyMadlyEvent.TrulyMadlyEventStatus.success,
                                                    null, false, true, true);

                                            mImageProgressbar.setVisibility(View.GONE);
                                            image_size.setVisibility(View.GONE);
                                            progressbar_container.setVisibility(View.GONE);
                                            mIdToDownloadingMap.remove(messageModal.getMsg_id());
                                            isFullViewAllowed = true;
                                        }

                                        @Override
                                        public void onLoadFailure(Exception e, long mTimeTaken) {
                                            mImageProgressbar.setProgress(1);
                                            mImageProgressbar.setProgress(0);
                                            mImageProgressbar.setVisibility(View.GONE);
                                            image_retry_iv.setVisibility(View.VISIBLE);
                                            progressbar_container.setVisibility(View.VISIBLE);
                                            mIdToDownloadingMap.remove(messageModal.getMsg_id());

                                            String message = null;
                                            if (e != null) {
                                                if (e instanceof SocketTimeoutException || e instanceof ConnectException) {
                                                    message = mContext.getString(R.string.ERROR_NETWORK_FAILURE);
                                                } else if (e instanceof FileNotFoundException) {
                                                    message = mContext.getString(R.string.download_image_not_avialable);
                                                } else {
                                                    message = mContext.getString(R.string.image_download_failed);
                                                }
                                            } else {
                                                message = mContext.getString(R.string.image_download_failed);
                                            }

                                            AlertsHandler.showMessage(mActivity, message, false);

                                            HashMap<String, String> eventInfo = new HashMap<>();
                                            eventInfo.put("reason", (e != null) ? e.getMessage() : message);
                                            if (Utility.isSet(image_size.getText().toString())) {
                                                eventInfo.put("size", image_size.getText().toString());
                                            }
                                            eventInfo.put("match_id", messageModal.getMatch_id());
                                            //Trk
                                            TrulyMadlyTrackEvent.trackEvent(mContext,
                                                    TrulyMadlyEvent.TrulyMadlyActivities.photo_share,
                                                    TrulyMadlyEvent.TrulyMadlyEventTypes.download_image, mTimeTaken,
                                                    TrulyMadlyEvent.TrulyMadlyEventStatus.fail,
                                                    eventInfo, true, false);
                                            //GA
                                            TrulyMadlyTrackEvent.trackEvent(mContext,
                                                    TrulyMadlyEvent.TrulyMadlyActivities.photo_share,
                                                    TrulyMadlyEvent.TrulyMadlyEventTypes.download_image_fail, mTimeTaken,
                                                    (e != null) ? e.getMessage() : message, null, false, true, true);
                                        }
                                    });
                        }
                        //Message sent
                        else if (imageMessageState != Constants.MessageState.OUTGOING_SENDING) {
                            //Handles both the cases:
                            //1. Image uploaded but message not sent
                            //2. Image not uploaded yet
                            if (Utility.isSet(messageModal.getmJpegUrl())) {
                                image_progressbar.setProgress(100);
                            }
                            retryMessageClickListener.onClick(getWrapper_container());
                        }
                    }
                }
            });
        }
    }

    class ImageLinkViewHolder extends ChatViewHolder {
        @BindView(R.id.image_link_image)
        ImageView imageLinkIV;

        public ImageLinkViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            ButterKnife.bind(this, itemLayoutView);
            setWrapper_layout((RelativeLayout) itemLayoutView.findViewById(R.id.image_link_wrapper));
        }

        @Override
        public void initialize(MessageModal messageModal) {
            //"https://upload.wikimedia.org/wikipedia/commons/a/ac/SilburyHill_gobeirne.jpg"
            if (messageModal.getaLinkImageUrl() != null) {
                ImageCacheHelper.with(mContext).loadWithKey(messageModal.getaLinkImageUrl(),
                        messageModal.getMatch_id())
                        .resize(UiUtils.dpToPx(332), UiUtils.dpToPx(166))
                        .transform(mCDAskCropTransformation)
                        .transform(mCDAskMaskTransformationReceived)
                        .into(imageLinkIV, new OnDataLoadedInterface() {
                            @Override
                            public void onLoadSuccess(long mTimeTaken) {
                            }

                            @Override
                            public void onLoadFailure(Exception e, long mTimeTaken) {

                            }
                        });
            }
        }
    }

    class EventViewHolder extends ChatViewHolder {
        @BindView(R.id.event_image)
        ImageView eventImage;
        @BindView(R.id.event_text)
        TextView eventText;

        public EventViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            ButterKnife.bind(this, itemLayoutView);
            setWrapper_layout((RelativeLayout) itemLayoutView.findViewById(R.id.event_wrapper));
        }

        @Override
        public void initialize(MessageModal messageModal) {
            EventChatModal eventChatModal = messageModal.getmEventChatModal();
//            String image_url = "http://blogs-images.forbes.com/steveolenski/files/2015/07/Messe_Luzern_Corporate_Event.jpg",
//                    name = "Hello Event";
//            if(eventChatModal != null){
//                name = eventChatModal.getmName();
//                image_url = eventChatModal.getmImageUrl();
//            }

            MaskTransformation maskTransformation =
                    messageModal.getAndSetOptEventMaskTransformation(
                            mCDAskMaskTransformationSent, mCDAskMaskTransformationReceived);
            int askPlaceholder = messageModal.getAndSetOptEventPlaceholderDrawable(
                    R.drawable.cd_nudge_placeholder_right, R.drawable.cd_nudge_placeholder_left);

            if (eventChatModal != null) {
                eventText.setTextColor(mTextColorGray);
                ImageCacheHelper.with(mContext).loadWithKey(eventChatModal.getmImageUrl(), eventChatModal.getmEventId())
                        .placeholder(askPlaceholder)
                        .resize(UiUtils.dpToPx(332), UiUtils.dpToPx(166))
                        .transform(mCDAskCropTransformation)
                        .transform(mCDGradientTransformation)
                        .transform(maskTransformation)
                        .into(eventImage, new OnDataLoadedInterface() {
                            @Override
                            public void onLoadSuccess(long mTimeTaken) {
                                eventText.setTextColor(Color.WHITE);
                            }

                            @Override
                            public void onLoadFailure(Exception e, long mTimeTaken) {

                            }
                        });
            }
            eventText.setText(messageModal.getMessage());
        }
    }
}
