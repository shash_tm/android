package com.trulymadly.android.app.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


class TrulyPageAdapter extends PagerAdapter {
	private final Context ctx;
	public TrulyPageAdapter(Context ctx){  
		this.ctx = ctx;  
	}  
	@Override  
	public Object instantiateItem(ViewGroup container, int position) {  
		TextView tView = new TextView(ctx);  
		position++;  
		tView.setText("Page number: " + position);  
		tView.setTextColor(Color.RED);  
		tView.setTextSize(20);  
		container.addView(tView);  
		return tView;  
	}  
	@Override  
	public int getCount() {  
		return 3;  
	}  
	@Override  
	public boolean isViewFromObject(View view, Object object) {  
		return (view == object);  
	}  

	@Override
	public void destroyItem(View container, int position, Object object) {
		((ViewPager) container).removeView((View) object);
	}
}  
