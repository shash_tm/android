package com.trulymadly.android.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.modal.FacebookAlbumItem;

import java.util.Vector;

public class FacebookAlbumAdapter extends BaseAdapter {
	private final Context context;
	private final Vector<FacebookAlbumItem> albums;

	public FacebookAlbumAdapter(Context context,
			Vector<FacebookAlbumItem> albums) {
		this.context = context;
		this.albums = albums;
	}

	public int getCount() {
		return albums.size();
	}

	public Object getItem(int position) {
		return null;
	}

	public long getItemId(int position) {
		return 0;
	}

	public View getView(int position, View convertView, ViewGroup parent) {

		if (convertView == null) {
			LayoutInflater vi = (LayoutInflater) this.context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = vi.inflate(R.layout.facebook_album_row_item, null);
		}
		FacebookAlbumItem aFacebookAlbumItem = albums.elementAt(position);
		if (aFacebookAlbumItem != null) {
			ImageView facebook_album_img_id = (ImageView) convertView
					.findViewById(R.id.facebook_album_img_id);
			TextView facebook_album_name_id = (TextView) convertView
					.findViewById(R.id.facebook_album_name_id);

			if(aFacebookAlbumItem.getCover_url()!=null) {
				Picasso.with(context).load(aFacebookAlbumItem.getCover_url()).into(facebook_album_img_id);
				facebook_album_img_id.setScaleType(ScaleType.CENTER_CROP);
			} else {
				Picasso.with(context).load(R.drawable.fbblank).into(facebook_album_img_id);
				facebook_album_img_id.setScaleType(ScaleType.CENTER_INSIDE);
			}

			facebook_album_name_id.setText(aFacebookAlbumItem.getName());
			convertView.setPadding(2, 2, 2, 2);
		}
		return convertView;
	}

}
