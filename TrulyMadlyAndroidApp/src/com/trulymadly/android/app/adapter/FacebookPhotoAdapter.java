package com.trulymadly.android.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.modal.FacebookPhotoItem;
import com.trulymadly.android.app.utility.Utility;

import java.util.Vector;

public class FacebookPhotoAdapter extends BaseAdapter {
	private final Context context;
	private final Vector<FacebookPhotoItem> photos;

	public FacebookPhotoAdapter(Context context,
			Vector<FacebookPhotoItem> photos) {
		this.context = context;
		this.photos = photos;
	}

	public int getCount() {
		return photos.size();
	}

	public Object getItem(int position) {
		return null;
	}

	public long getItemId(int position) {
		return 0;
	}

	public View getView(int position, View convertView, ViewGroup parent) {

		if (convertView == null) {
			LayoutInflater vi = (LayoutInflater) this.context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = vi.inflate(R.layout.facebook_photo_row_item, parent, false);
		}
		FacebookPhotoItem aFacebookPhotoItem = photos.elementAt(position);
		if (aFacebookPhotoItem != null && Utility.isSet(aFacebookPhotoItem.getSourceUrl())) {
			ImageView facebook_photo_id = (ImageView) convertView
					.findViewById(R.id.facebook_photo_id);

			Picasso.with(context).load(aFacebookPhotoItem.getSourceUrl()).into(facebook_photo_id);
			convertView.setPadding(2, 2, 2, 2);
		}
		return convertView;
	}
}
