package com.trulymadly.android.app.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.PagerAdapter;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.trulymadly.android.app.R;

import java.util.ArrayList;

/**
 * Created by avin on 31/05/16.
 */
public class SparkMsgSuggestionsAdapter extends PagerAdapter {

    private ArrayList<String> mMessages;
    private Context mContext;
    private LayoutInflater mInflater;
    private int mTextColor;

    public SparkMsgSuggestionsAdapter(ArrayList<String> mMessages, Context mContext) {
        this.mMessages = mMessages;
        this.mContext = mContext;
        mTextColor = ActivityCompat.getColor(mContext, R.color.colorTertiary);
    }

    @Override
    public int getCount() {
        return (mMessages == null)?0:mMessages.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        if(mInflater == null) {
            mInflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        View itemView = mInflater.inflate(R.layout.text_regular_item,
                container, false);

        TextView messageTV = (TextView)itemView.findViewById(R.id.text_tv);
        messageTV.setTextColor(mTextColor);
        messageTV.setText(Html.fromHtml(Uri.decode(mMessages.get(position))));

        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((TextView) object);
    }

    public void changeList(ArrayList<String> messages) {
        mMessages = messages;
        notifyDataSetChanged();
        
    }
}
