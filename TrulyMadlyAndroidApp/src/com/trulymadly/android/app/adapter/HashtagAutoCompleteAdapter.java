package com.trulymadly.android.app.adapter;

import android.app.Activity;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import com.trulymadly.android.app.R;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.listener.AlertDialogInterface;
import com.trulymadly.android.app.listener.OnHashtagSelected;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.BadWordsHandler;
import com.trulymadly.android.app.utility.HashtagHandler;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONArray;

import java.util.ArrayList;

public class HashtagAutoCompleteAdapter extends ArrayAdapter<String> implements
        Filterable {

    private final String API_BASE = ConstantsUrls.get_hashtag_url();
    private final Activity aActivity;
    private OnHashtagSelected hashtagSelectListener;
    private JSONArray resultList;
    private String error = null;
    private ArrayList<String> badWordList;
    private HashtagHandler hashtagHandler = null;


    public HashtagAutoCompleteAdapter(Activity activity, int listItem,
                                      int textViewId, OnHashtagSelected hashtagSelectListener) {
        super(activity, listItem, textViewId);
        this.aActivity = activity;
        createHashtagHandler();
        this.hashtagSelectListener = hashtagSelectListener;
        badWordList = BadWordsHandler.getBadWordList(aActivity);
    }

    private void createHashtagHandler() {
        if (hashtagHandler == null) {
            hashtagHandler = new HashtagHandler();
        }
    }

    @Override
    public int getCount() {
        return resultList != null ? resultList.length() : 0;
    }

    @Override
    public String getItem(int index) {
        return resultList != null ? resultList.optString(index) : "";
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                synchronized (filterResults) {
                    if (constraint != null) {
                        autocomplete(constraint.toString());
                        filterResults.values = resultList;
                        filterResults.count = resultList != null ? resultList.length() : 0;
                    }
                    return filterResults;
                }
            }

            @Override
            protected void publishResults(final CharSequence constraint,
                                          FilterResults filterResults) {
                synchronized (filterResults) {
                    if (filterResults.values != null) {
                        resultList = (JSONArray) filterResults.values;
                        int currentCount = filterResults.count;
                        if (currentCount > 0) {
                            notifyDataSetChanged();
                        } else {
                            notifyDataSetInvalidated();
                        }
                        //check if there is any space in hashtags
                        if (constraint.charAt(constraint.length() - 1) == ' ' && hashtagSelectListener != null) {
                            final String hashtag = constraint.toString();
                            aActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    hashtagSelectListener.onHashtagSelected(hashtag);
                                }
                            });
                            clearResultList();
                        }
                    } else {
                        notifyDataSetInvalidated();
                        if (error != null) {
                            AlertsHandler.showAlertDialog(aActivity, error,
                                    new AlertDialogInterface() {

                                        @Override
                                        public void onButtonSelected() {
                                            UiUtils.hideKeyBoard(aActivity);
                                            if (hashtagSelectListener != null) {
                                                aActivity.runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        hashtagSelectListener.onHashtagSelected(null);
                                                    }
                                                });
                                            }
                                            error = null;
                                        }
                                    });
                        }
                    }
                }
            }
        };
    }

    private void autocomplete(String input) {
        int errorMessageResId = Utility.validateHashtagString(input);
        String results = null;
        input = input.trim();
        if (!input.isEmpty()) {
            if (BadWordsHandler.hasBadWord(aActivity, input, badWordList)) {
                UiUtils.hideKeyBoard(aActivity);
                clearResultList();
                error = aActivity.getResources().getString(R.string.bad_word_text_occupation);
                return;
            }
        } else {
            return;
        }

        if (errorMessageResId == 0) {
            createHashtagHandler();
            resultList = hashtagHandler.getHashtags(input);
        } else {
            UiUtils.hideKeyBoard(aActivity);
            AlertsHandler.showMessage(aActivity, errorMessageResId);
            clearResultList();
        }

    }

    public void clearResultList() {
        resultList = null;
    }

}
