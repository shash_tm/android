package com.trulymadly.android.app.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.trulymadly.android.app.R;
import com.trulymadly.android.app.modal.FavoriteDataModal;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by udbhav on 26/07/16.
 */
public class MyFavoriteGridItem extends RecyclerView.ViewHolder {

    @BindView(R.id.favorite_item)
    public View itemView;
    @BindView(R.id.my_favorite_name)
    public TextView myFavoriteName;
    @BindView(R.id.my_favorite_image)
    public ImageView myFavoriteImage;
    @BindView(R.id.my_favorite_progress_bar)
    public View myFavoriteProgressBar;
    @BindView(R.id.my_favorite_overlay)
    public View myFavoriteOverLay;
    @BindView(R.id.my_favorite_overlay_image)
    public ImageView myFavoriteOverLayImage;
    @BindView(R.id.favorite_border)
    public View favoriteBorder;
    @BindView(R.id.show_more_text_image)
    public View showMoreTextImage;
    @BindView(R.id.show_more_imageview)
    public ImageView showMoreImageView;
    private FavoriteDataModal favoriteData;

    public MyFavoriteGridItem(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public FavoriteDataModal getFavoriteData() {
        return favoriteData;
    }

    public void setFavoriteData(FavoriteDataModal favoriteData) {
        this.favoriteData = favoriteData;
    }
}
