package com.trulymadly.android.app.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.trulymadly.android.app.R;
import com.trulymadly.android.app.modal.EditPrefModel;

import java.util.ArrayList;
import java.util.HashMap;

class EditPrefMenuSlideAdapter extends BaseAdapter {
	// Declare Variables
	private final Context context;
	private final ArrayList<EditPrefModel> editPrefList;

	public EditPrefMenuSlideAdapter(Context context, ArrayList<EditPrefModel> editPrefList) {
		this.context = context;
		this.editPrefList = editPrefList;
	}

	public int getCount() {
		return editPrefList.size();
	}

	public Object getItem(int position) {
		return null;
	}

	public long getItemId(int position) {
		return 0;
	}

	public View getView(int position, View convertView, ViewGroup parent) {

		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		@SuppressLint("ViewHolder") View itemView = inflater.inflate(R.layout.edit_pref_row_list_item, parent, false);
		
		TextView name=(TextView)itemView.findViewById(R.id.name);
		CheckBox row_select_checkbox=(CheckBox)itemView.findViewById(R.id.row_select_checkbox);

		EditPrefModel aEditPrefModel = new EditPrefModel();
		
		aEditPrefModel = editPrefList.get(position);
		
		if(aEditPrefModel!=null){
			
			if(aEditPrefModel!=null && !(aEditPrefModel.getName()).equals(""))
				name.setText(aEditPrefModel.getName());
			
			if(aEditPrefModel!=null && (aEditPrefModel.isSelected()))
				row_select_checkbox.setChecked(true);
			else
				row_select_checkbox.setChecked(false);
		}
		return itemView;
	}
	
	public String[] getSelectedItems(){

		String[] selectedItemArr = new String[2];

		String selectedNames="";
		String selectedIds="";

		boolean isFirstEle=true;
		for(EditPrefModel rel : editPrefList){
			if(rel.isSelected()){
				if(isFirstEle){
					selectedNames+=rel.getName();
					isFirstEle=false;
					selectedIds += rel.getID();
				}
				else{
					selectedNames+="," + rel.getName();
					selectedIds += "," +rel.getID();
				}
			}
		}
		selectedItemArr[0]=selectedNames;
		selectedItemArr[1]=selectedIds;

		return selectedItemArr;
	}


	public HashMap<String, Boolean> getSelectedItemIDs(){

		HashMap<String, Boolean> selectedItemIDsMap = new HashMap<>();
		for(EditPrefModel rel : editPrefList){
			if(rel.isSelected()){
				selectedItemIDsMap.put(rel.getID()+"", true);
			}
		}
		return selectedItemIDsMap;
	}
	
}

