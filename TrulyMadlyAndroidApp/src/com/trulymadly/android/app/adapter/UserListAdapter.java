package com.trulymadly.android.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.trulymadly.android.app.CircleTransformation;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.listener.UserPicClickedInterface;
import com.trulymadly.android.app.modal.UserInfoModal;
import com.trulymadly.android.app.utility.Utility;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by deveshbatra on 3/19/16.
 */
public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.ViewHolderUserListItem> implements View.OnClickListener {

    private final UserPicClickedInterface onClick;
    private final Activity aActivity;
    private final LayoutInflater inflater;
    private ArrayList<UserInfoModal> userList;
    //    private GradientTransformation mCDGradientTransformation;
    private boolean followStatus;

    public UserListAdapter(Activity act, ArrayList<UserInfoModal> userList, UserPicClickedInterface onClick, boolean followStatus) {
        this.aActivity = act;
        this.userList = userList;

        inflater = (LayoutInflater) act
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.onClick = onClick;
        this.followStatus = followStatus;
//        mCDGradientTransformation = new GradientTransformation(new int[]{Color.parseColor("#DDFFFFFF"),
//                Color.parseColor("#DDFFFFFF"), Color.parseColor("#DDFFFFFF")});

    }

    public boolean isFollowStatus() {
        return followStatus;
    }

    public void setFollowStatus(boolean followStatus) {
        this.followStatus = followStatus;
    }

    public void setUserList(ArrayList<UserInfoModal> userList) {
        this.userList = userList;
    }

    @Override
    public ViewHolderUserListItem onCreateViewHolder(ViewGroup parent, int viewType) {

        View sView = inflater.inflate(R.layout.user_list_item, parent,
                false);
        return new ViewHolderUserListItem(sView);
    }


    @Override
    public void onBindViewHolder(final ViewHolderUserListItem viewHolder, int position) {

        UserInfoModal user = userList.get(position);
        viewHolder.itemView.setTag(user);
        viewHolder.itemView.setOnClickListener(this);
        Picasso.with(aActivity).load(user.getProfilePicLink()).transform(new CircleTransformation()).into(viewHolder.dp_holder, new Callback() {
            @Override
            public void onSuccess() {
            }

            @Override
            public void onError() {
                if (!Utility.isMale(aActivity))
                    Picasso.with(aActivity).load(R.drawable.dummy_male).transform(new CircleTransformation()).into(viewHolder.dp_holder);
                else
                    Picasso.with(aActivity).load(R.drawable.dummy_female).transform(new CircleTransformation()).into(viewHolder.dp_holder);

            }
        });

//        viewHolder.blur_view.setVisibility(!user.isMutualMatch() && !followStatus ? View.VISIBLE : View.INVISIBLE);
        viewHolder.tick_mark.setVisibility(!user.isSparkedByMe() && user.isLikedByMe() && !user.isMutualMatch() ? View.VISIBLE : View.INVISIBLE);
        viewHolder.spark_mark.setVisibility(user.isSparkedByMe() && !user.isMutualMatch() ? View.VISIBLE : View.INVISIBLE);
        viewHolder.user_name_tv.setText(user.getName());
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    @Override
    public void onClick(View view) {
        UserInfoModal user = (UserInfoModal) view.getTag();
        onClick.onUserPicClicked(user);


    }

    class ViewHolderUserListItem extends RecyclerView.ViewHolder {

        final View itemView;
        @BindView(R.id.dp_holder)
        ImageView dp_holder;

        @BindView(R.id.user_name_tv)
        TextView user_name_tv;


        @BindView(R.id.tick_mark)
        ImageView tick_mark;
        @BindView(R.id.spark_mark)
        ImageView spark_mark;

        @BindView(R.id.blur_view)
        View blur_view;


        public ViewHolderUserListItem(View itemView) {
            super(itemView);
            this.itemView = itemView;
            ButterKnife.bind(this, itemView);
        }
    }


}
