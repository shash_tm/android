/**
 *
 */
package com.trulymadly.android.app.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.trulymadly.android.app.R;

/**
 * @author udbhav
 */
public class CustomSpinnerArrayAdapter extends ArrayAdapter<Object> {

    public CustomSpinnerArrayAdapter(Context context, int resource,
                                     Object[] objects) {
        super(context, resource, objects);
    }

    @Override
    public Object getItem(int position) {
        if (position >= 0 && position < getCount()) {
            return super.getItem(position);
        } else {
            return super.getItem(getCount() - 1);
        }
    }

    @Override
    public int getCount() {
        return super.getCount();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = super.getView(position, convertView, parent);
        ((TextView) v).setTextColor(getContext().getResources().getColor(
                R.color.final_bg_grey));
        return v;
    }

}
