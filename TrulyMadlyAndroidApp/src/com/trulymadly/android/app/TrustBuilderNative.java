package com.trulymadly.android.app;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.facebook.CallbackManager;
import com.koushikdutta.ion.future.ResponseFuture;
import com.linkedin.platform.LISessionManager;
import com.moe.pushlibrary.MoEHelper;
import com.squareup.picasso.Picasso;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.adapter.CustomSpinnerArrayAdapter;
import com.trulymadly.android.app.adapter.FacebookLoginCallback;
import com.trulymadly.android.app.adapter.FacebookLoginCallback.FacebookLoginCallbackInterface;
import com.trulymadly.android.app.adapter.ImportLinkedInAdapterNative;
import com.trulymadly.android.app.adapter.ImportLinkedInAdapterOauth2;
import com.trulymadly.android.app.asynctasks.PhotoUploader;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.Constants.HttpRequestType;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.listener.CachedDataInterface;
import com.trulymadly.android.app.listener.ConfirmDialogInterface;
import com.trulymadly.android.app.listener.ConnectFBviaTrustBuilderCallbackInterface;
import com.trulymadly.android.app.listener.CustomOkHttpResponseHandler;
import com.trulymadly.android.app.listener.GetFileCallback;
import com.trulymadly.android.app.listener.OnActionBarClickedInterface;
import com.trulymadly.android.app.listener.OnImportFromLinkedin;
import com.trulymadly.android.app.listener.TapToRetryListener;
import com.trulymadly.android.app.modal.EndorserModal;
import com.trulymadly.android.app.modal.LinkedInData;
import com.trulymadly.android.app.modal.PhotoIdType;
import com.trulymadly.android.app.modal.TrustBuilderData;
import com.trulymadly.android.app.sqlite.CachingDBHandler;
import com.trulymadly.android.app.utility.ActionBarHandler;
import com.trulymadly.android.app.utility.ActivityHandler;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.CachedDataHandler;
import com.trulymadly.android.app.utility.FilesHandler;
import com.trulymadly.android.app.utility.GooGlUrlShortner;
import com.trulymadly.android.app.utility.GooGlUrlShortner.GooGlUrlShortnerInterface;
import com.trulymadly.android.app.utility.MoEHandler;
import com.trulymadly.android.app.utility.OkHttpHandler;
import com.trulymadly.android.app.utility.PermissionsHelper;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.TapToRetryHandler;
import com.trulymadly.android.app.utility.TimeUtils;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TrustBuilderNative extends AppCompatActivity
        implements OnClickListener, OnItemSelectedListener, OnEditorActionListener {

    private static String photoIdTypeKey;
    private static DOC_UPLOAD_TYPE docUploadType;
    protected Canvas canvas;
    @BindView(R.id.continue_trust_view)
    Button continue_trust_view;
    @BindView(R.id.trust_header_facebook)
    View header_fb;
    @BindView(R.id.trust_content_facebook)
    View content_fb;
    @BindView(R.id.trust_verify_facebook)
    View verify_fb;
    @BindView(R.id.trust_message_fb_notice)
    View trust_message_fb_notice;
    @BindView(R.id.trust_message_normal_fb)
    View message_fb;
    @BindView(R.id.trust_message_error_fb)
    View error_fb;
    @BindView(R.id.header_error_fb)
    View error_fb_header;
    @BindView(R.id.header_text_fb_connections)
    View header_text_fb_connections;
    @BindView(R.id.fb_icon)
    ImageView fb_icon;
    @BindView(R.id.linkedin_icon)
    ImageView li_icon;
    @BindView(R.id.phone_icon)
    ImageView phone_icon;
    @BindView(R.id.photoid_icon)
    ImageView photoid_icon;
    @BindView(R.id.endorsement_icon)
    ImageView endorsement_icon;
    @BindView(R.id.add_pic_imageview)
    ImageView addOwnProfilePic;
    @BindView(R.id.tb_vertical_line)
    View verticalLine;
    @BindView(R.id.header_text_id_name)
    TextView header_text_id_name;
    @BindView(R.id.add_pic_imageview_endorsement)
    ImageView add_pic_imageview_endorsement;
    @BindView(R.id.name_endorsement)
    TextView name_endorsement;
    @BindView(R.id.city_endorsement)
    TextView city_endorsement;
    @BindView(R.id.age_endorsement)
    TextView age_endorsement;
    @BindView(R.id.profile_pic_endorsement)
    View profile_pic_endorsement;
    @BindView(R.id.profile_pic_id)
    View profile_pic_id;
    @BindView(R.id.webview_include_view)
    View mWebviewIncludeView;
    private boolean isCallMeEnabled;
    private boolean isCallVerificationInProgress;
    private Button verify_ph;
    private View upload_bar;
    private PhotoUploader.UPLOAD_TYPE uploadType;
    private String loadingMessage;
    private String TAG = "TrustBuilder";
    private String mCurrentPhotoPath = null;
    private ProgressDialog mProgressDialog;
    private PhotoUploader photoUploader = null;
    private boolean isRegistration = false, isActivityForResult = false;
    private Context aContext;
    private Activity aActivity;
    private TrustBuilderData tbData = new TrustBuilderData();
    private View show_photo_slider_layout;
    private ConnectFBviaTrustBuilderCallbackInterface connectFBviaTrustBuilderCallbackInterface;
    private boolean fbReimport;
    private View header_li;
    private View content_li;
    private View message_li;
    private View error_li;
    private View error_li_header;
    private View header_text_li_connections;
    private ImportLinkedInAdapterOauth2 importLinkedInAdapterOauth2 = null;
    private ImportLinkedInAdapterNative importLinkedInAdapterNative = null;
    private View header_id;
    private View content_id;
    private View verify_id;
    private View message_id;
    private View error_id;
    private View error_id_header;
    private View success_id;
    private View trust_photoid_buttons;
    private View trust_photoid_password_buttons;
    private View trust_photoid_missmatch_buttons;
    private View trust_verify_photoid_mismatch;
    private View header_text_id_connections;
    private EditText trust_photoid_password_edittext;
    private JSONArray photoIdTypesJson;
    private Spinner photoIdTypeSpinner;
    private View header_ph, content_ph, error_ph, error_ph_header, header_text_ph_connections, phone_verifying;
    private EditText phone_no_text;
    private View header_en;
    private View content_en;
    private View trust_msg_endorsement_error_en;
    private View endorsement_header_error_en_tv;
    private View trust_msg_endorsement_success_en;
    private View verify_en;
    private View endorsers_container;
    private LinearLayout endorsers_container_sub;
    private TextView trust_msg_endorsement_normal_en, endorsement_header_text_en_name, incorrect_otp_text_view;
    private boolean isPhotoOptionShowing = false;
    private View otp_view;
    private TextView import_camera_text, import_gallery_text;
    private View import_webcam_icon, import_gallery_icon;
    private ResponseFuture<String> docUploadRequest;
    // private View tb_header_not_verified, tb_header_verified;
    private View id_upload_bar;
    private View id_upload_bar_endorsement;
    // private View edit_network_fail, edit_network_retry;
    // private View tb_data_container_scrollview;
    private TapToRetryHandler tapToRetryHandler;
    private boolean isPush = false, isOtpViewVisible;
    private String tbEventActivity;
    private TextView trust_score_value_material_design;
    private MoEHelper mhelper = null;
    private boolean isFromActivityResult = false;
    private LinearLayout informationView;
    private TextView native_tb_header_sub_text;
    private TextView native_tb_header_min_trust_score;
    private TextView endorsement_profile_pic_success_en, otp_timer;
    private TextView header_text_li_name;
    private String facebookRightAwayText = "";
    private String picSmallError = "";
    private boolean isInformationViewVisible;
    private EditText otpEditText;
    private View sending_view, timer_icon;
    private Button call_button;
    private int seconds = 59;
    private int minutes = 9, previousLength;
    private Timer callMeEnableTimer = null;
    private View main_view, crop_view;
    private CallbackManager callbackManager;
    private FacebookLoginCallback facebookLoginCallBack;
    private View header_text_ph_name;
    private boolean enableLinkedIn = true;
    private CachedDataInterface cachedDataInterface;
    private GetFileCallback onFileComplete;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("mCurrentPhotoPath", mCurrentPhotoPath);
        outState.putBoolean("isRegistration", isRegistration);

        if (docUploadType != null) {
            outState.putString("docUploadType", docUploadType.toString());
        }
        if (photoIdTypeKey != null) {
            outState.putString("photoIdTypeKey", photoIdTypeKey);
        }
    }

    // checks its number only
    private boolean validateOTP(CharSequence s) {
        if (s.length() != 4)
            return false;

        for (int i = 0; i < 4; i++) {
            if (s.charAt(i) < '0' || s.charAt(i) > '9')
                return false;
        }
        return true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.trustbuilder_native_material_design);
        } catch (OutOfMemoryError e) {
            finish();
            return;
        }
        ButterKnife.bind(this);

        facebookRightAwayText += getResources().getString(R.string.also_verify_with_facebook);
        picSmallError = getResources().getString(R.string.min_photos_size);
        aContext = this;
        aActivity = this;
        try {
            photoIdTypesJson = new JSONArray(
                    "[{\"key\": \"Passport\",\"value\": \"Passport\"},{\"key\": \"Driving License\",\"value\": \"Driving License\"},{\"key\": \"Pan Card\",\"value\": \"Pan Card\"},{\"key\": \"Voter ID\",\"value\": \"Voter Id\"},{\"key\": \"Aadhar Card\",\"value\": \"Aadhar Card\"}]");
        } catch (JSONException ignored) {
        }
        mhelper = new MoEHelper(this);
        Intent i = getIntent();
        Bundle b = i.getExtras();
        if (b != null) {
            isActivityForResult = b.getBoolean("isActivityForResult", false);
            isRegistration = b.getBoolean("isRegistration", false);
            isPush = b.getBoolean("isPush", false);
        }

        tbEventActivity = isRegistration ? TrulyMadlyActivities.trustbuilder_register
                : TrulyMadlyActivities.trustbuilder;

        String headerText;
        if (isRegistration) {
            headerText = getResources().getString(R.string.header_trust_registration);
            loadingMessage = "Almost done! Get ready to verify yourself.";
        } else {
            loadingMessage = null;
            headerText = getResources().getString(R.string.header_trust_normal);
        }
        showTrustBuilderContinueButton();
        ActionBarHandler actionBarHandler = new ActionBarHandler(this, headerText, null, new OnActionBarClickedInterface() {
            @Override
            public void onBackClicked() {
                onBackPressed();
            }

            @Override
            public void onLocationClicked() {

            }

            @Override
            public void onUserProfileClicked() {

            }

            @Override
            public void onConversationsClicked() {

            }

            @Override
            public void onTitleClicked() {

            }

            @Override
            public void onTitleLongClicked() {

            }

            @Override
            public void onSearchViewOpened() {

            }

            @Override
            public void onSearchViewClosed() {

            }

            @Override
            public void onCategoriesClicked() {

            }

            @Override
            public void onCuratedDealsClicked() {

            }
        }, false, false, false);

        connectFBviaTrustBuilderCallbackInterface = new ConnectFBviaTrustBuilderCallbackInterface() {

            @Override
            public void onSuccess(String access_token, String fbConnections, boolean isFbPhotoDeclined) {
                clearCachedData();
                setFbVerified(fbConnections, isFbPhotoDeclined);
                drawTrustScore();
                hideLoaders();
            }

            @Override
            public void onFail(String errorMessage) {
                hideLoaders();
                showFbError(errorMessage);
            }

            @Override
            public void onFail(int error_msg_res_id) {
                hideLoaders();
                showFbError(error_msg_res_id);

            }

            @Override
            public void onMismatch(String mismatch, JSONObject diff, boolean isFbPhotoDeclined) {
                hideLoaders();
                onMismatchFacebook(mismatch, diff, isFbPhotoDeclined);

            }
        };

        OnImportFromLinkedin onImportFromLinkedin = new OnImportFromLinkedin() {

            @Override
            public void onSuccess(String source, LinkedInData liData) {
                enableLinkedIn = true;
                hideLoaders();
                isFromActivityResult = false;
                if (liData == null) {
                    onFailure(source, "Invalid Data");
                } else {
                    sendLinkedInResponseToServer(liData);
                }
            }

            @Override
            public void onSuccessfullVerification(String source, String connections, String profile_pic) {
                enableLinkedIn = true;
                hideLoaders();
                isFromActivityResult = false;
                clearCachedData();
                if (Utility.isSet(profile_pic)) {
                    profilePhotoSuccess();
                }
                setLiVerified(connections);
                drawTrustScore();
            }

            @Override
            public void onFailure(String source, String reason) {
                enableLinkedIn = true;
                hideLoaders();
                isFromActivityResult = false;
                TrulyMadlyTrackEvent.trackEvent(aContext, tbEventActivity, TrulyMadlyEventTypes.linkedin_failed, 0,
                        source + reason != null ? ("--" + reason) : "", null, true);
            }

            @Override
            public void onCancel(String source) {
                enableLinkedIn = true;
                hideLoaders();
                isFromActivityResult = false;
                TrulyMadlyTrackEvent.trackEvent(aContext, tbEventActivity, TrulyMadlyEventTypes.linkedin_cancel, 0,
                        source, null, true);
            }

            @Override
            public void onProgress(String source) {
                enableLinkedIn = true;
                isFromActivityResult = false;
                hideLoaders();
                mProgressDialog = UiUtils.showProgressBar(aContext, mProgressDialog,
                        R.string.importing_data_loader_message);
            }

            @Override
            public void onLiFailure(String source, String reason) {
                onFailure(source, reason);
                showLiError(reason);
            }

            @Override
            public void onLiNetworkError(String source, Exception exception) {
                onFailure(source, "Network Error");
                AlertsHandler.showNetworkError(aActivity, exception);
            }
        };

        callbackManager = CallbackManager.Factory.create();
        FacebookLoginCallbackInterface callbackInterface = new FacebookLoginCallbackInterface() {
            @Override
            public void onFail() {
                hideLoaders();
            }

            @Override
            public void onLogin(String accessToken, boolean isBirthdayDeclined, boolean isPhotoDeclined) {
                Utility.connectFBviaTrustBuilder(accessToken,
                        connectFBviaTrustBuilderCallbackInterface,
                        "trustbuilder", fbReimport, tbEventActivity, aContext, isPhotoDeclined);

            }

            @Override
            public void onRetry() {
                mProgressDialog = UiUtils.showProgressBar(aContext, mProgressDialog);
            }
        };

        facebookLoginCallBack = new FacebookLoginCallback(this, callbackManager, callbackInterface, true, false, tbEventActivity);

        // tb_data_container_scrollview =
        // findViewById(R.id.tb_data_container_scrollview);

        main_view = findViewById(R.id.main_trust_buider_layout);

        crop_view = findViewById(R.id.crop_layout_trustbuilder);
        importLinkedInAdapterOauth2 = new ImportLinkedInAdapterOauth2(aContext, onImportFromLinkedin, mWebviewIncludeView);
        importLinkedInAdapterNative = new ImportLinkedInAdapterNative(aContext, onImportFromLinkedin);
        incorrect_otp_text_view = (TextView) findViewById(R.id.incorrect_otp_text_view);
        add_pic_imageview_endorsement.setOnClickListener(this);
        call_button = (Button) findViewById(R.id.call_button);
        call_button.setOnClickListener(this);
        header_text_ph_name = findViewById(R.id.header_text_ph_name);
        otp_view = findViewById(R.id.otp_view);
        otp_view.setOnClickListener(this);
        sending_view = findViewById(R.id.sending_view);
        sending_view.setOnClickListener(this);
        otpEditText = (EditText) findViewById(R.id.otpEditText);
        header_li = findViewById(R.id.trust_header_linkedin);
        content_li = findViewById(R.id.trust_content_linkedin);
        View verify_li = findViewById(R.id.trust_verify_linkedin);
        message_li = findViewById(R.id.trust_message_normal_li);
        error_li = findViewById(R.id.trust_message_error_li);
        error_li_header = findViewById(R.id.header_error_li);
        header_text_li_connections = findViewById(R.id.header_text_li_connections);
        informationView = (LinearLayout) findViewById(R.id.why_button_layout);
        native_tb_header_min_trust_score = (TextView) findViewById(R.id.native_tb_header_min_trust_score);
        native_tb_header_sub_text = (TextView) findViewById(R.id.native_tb_header_sub_text);
        informationView.setOnClickListener(this);
        ImageView informationButton = (ImageView) findViewById(R.id.why_button);
        informationButton.setOnClickListener(this);
        TextView closeInformationTextview = (TextView) informationView.findViewById(R.id.tb_information_close_textview);
        closeInformationTextview.setOnClickListener(this);
        header_id = findViewById(R.id.trust_header_photoid);
        header_text_li_name = (TextView) findViewById(R.id.header_text_li_name);
        content_id = findViewById(R.id.trust_content_photoid);
        verify_id = findViewById(R.id.trust_verify_photoid);
        message_id = findViewById(R.id.trust_message_normal_id);
        error_id = findViewById(R.id.trust_message_error_id);
        error_id_header = findViewById(R.id.header_error_id);
        success_id = findViewById(R.id.trust_message_success_id);
        photoIdTypeSpinner = (Spinner) findViewById(R.id.trust_spinner_id);
        header_text_id_connections = findViewById(R.id.header_text_id_connections);
        trust_photoid_buttons = findViewById(R.id.trust_photoid_buttons);
        trust_photoid_password_buttons = findViewById(R.id.trust_photoid_password_buttons);
        trust_photoid_missmatch_buttons = findViewById(R.id.trust_photoid_missmatch_buttons);
        trust_photoid_password_edittext = (EditText) findViewById(R.id.trust_photoid_password_edittext);
        View trust_verify_photoid_password = findViewById(R.id.trust_verify_photoid_password);
        trust_verify_photoid_mismatch = findViewById(R.id.trust_verify_photoid_mismatch);
        endorsement_profile_pic_success_en = (TextView) findViewById(R.id.endorsement_profile_pic_success_en);
        trust_score_value_material_design = (TextView) findViewById(R.id.trust_score_value_material_design);

        timer_icon = findViewById(R.id.timer_icon);
        otp_timer = (TextView) findViewById(R.id.otp_timer);
        header_ph = findViewById(R.id.trust_header_phone);
        content_ph = findViewById(R.id.trust_content_phone);
        verify_ph = (Button) findViewById(R.id.trust_verify_phone);
        // message_ph = findViewById(R.id.trust_message_normal_ph);
        error_ph = findViewById(R.id.trust_message_error_ph);
        error_ph_header = findViewById(R.id.header_error_ph);
        header_text_ph_connections = findViewById(R.id.header_text_ph_connections);
        phone_no_text = (EditText) findViewById(R.id.trust_phone_no);

        enableSpinnerUploadButton(verify_ph, false);

        TextWatcher phoneNumberTextWatcher = new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (Utility.isValidPhoneNo(s.toString())) {
                    hideKeyboard();
                    enableSpinnerUploadButton(verify_ph, true);
                } else
                    enableSpinnerUploadButton(verify_ph, false);

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        phone_no_text.addTextChangedListener(phoneNumberTextWatcher);
        TextWatcher otpTextWatcher = new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 4 && validateOTP(s) && previousLength == 3) {
                    sendOTP();
                } else if (s.length() > 4) {
                    s = s.subSequence(0, 4);
                    otpEditText.setText(s);
                    hideKeyboard();
                    AlertsHandler.showMessage(aActivity, R.string.len_alert_message);
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                previousLength = s.length();

            }
        };
        otpEditText.addTextChangedListener(otpTextWatcher);

        phone_verifying = findViewById(R.id.phone_verifying);

        show_photo_slider_layout = findViewById(R.id.show_photo_slider_layout);
        View take_from_camera_layout = findViewById(R.id.take_from_camera_layout);
        View add_from_gallery_layout = findViewById(R.id.add_from_gallery_layout);
        import_webcam_icon = findViewById(R.id.import_webcam_icon);
        import_gallery_icon = findViewById(R.id.import_gallery_icon);
        import_camera_text = (TextView) findViewById(R.id.import_camera_text);
        import_gallery_text = (TextView) findViewById(R.id.import_gallery_text);

        endorsement_header_text_en_name = (TextView) findViewById(R.id.endorsement_header_text_en_name);
        header_en = findViewById(R.id.trust_header_endorsement);
        content_en = findViewById(R.id.trust_content_endorsement);
        trust_msg_endorsement_normal_en = (TextView) findViewById(R.id.trust_msg_endorsement_normal_en);
        trust_msg_endorsement_error_en = findViewById(R.id.trust_msg_endorsement_error_en);
        endorsement_header_error_en_tv = findViewById(R.id.endorsement_header_error_en);
        trust_msg_endorsement_success_en = findViewById(R.id.trust_msg_endorsement_success_en);
        // endorsement_header_text_en_connections =
        // findViewById(R.id.endorsement_header_text_en_connections);
        verify_en = findViewById(R.id.trust_verify_endorsement);
        View add_more_en = findViewById(R.id.add_more_endorsement);
        endorsers_container = findViewById(R.id.endorsers_container);
        endorsers_container_sub = (LinearLayout) findViewById(R.id.endorsers_container_sub);

        id_upload_bar = findViewById(R.id.id_upload_bar);

        id_upload_bar_endorsement = findViewById(R.id.id_upload_bar_endorsement);

        // Setting listeners
        createPhotoIdTypeSpinner(PhotoIdType.getPhotoIdType(photoIdTypesJson));

        header_fb.setOnClickListener(this);
        verify_fb.setOnClickListener(this);

        header_li.setOnClickListener(this);
        verify_li.setOnClickListener(this);
        header_id.setOnClickListener(this);
        verify_id.setOnClickListener(this);
        trust_verify_photoid_password.setOnClickListener(this);
        trust_verify_photoid_mismatch.setOnClickListener(this);

        header_ph.setOnClickListener(this);
        verify_ph.setOnClickListener(this);

        verify_en.setOnClickListener(this);
        add_more_en.setOnClickListener(this);

        trust_photoid_password_edittext.setOnEditorActionListener(this);

        show_photo_slider_layout.setVisibility(View.GONE);
        show_photo_slider_layout.setOnClickListener(this);
        take_from_camera_layout.setOnClickListener(this);
        add_from_gallery_layout.setOnClickListener(this);

        photoIdTypeSpinner.setOnItemSelectedListener(this);
        addOwnProfilePic.setOnClickListener(this);

        name_endorsement.setText(SPHandler.getString(aContext, ConstantsSP.SHARED_KEYS_USER_NAME));
        age_endorsement.setText(SPHandler.getString(aContext, ConstantsSP.SHARED_KEYS_USER_AGE));
        city_endorsement.setText(SPHandler.getString(aContext, ConstantsSP.SHARED_KEYS_USER_CITY));

        id_upload_bar.setOnClickListener(this);
        id_upload_bar_endorsement.setOnClickListener(this);
//        ----
        cachedDataInterface = new CachedDataInterface() {
            @Override
            public void onSuccess(JSONObject response) {
                tapToRetryHandler.onSuccessFull();
                processTrustBuilderData(response.optJSONObject("data"));

            }

            @Override
            public void onError(Exception e) {
                tapToRetryHandler.onNetWorkFailed(e);

            }
        };
        tapToRetryHandler = new TapToRetryHandler(this, findViewById(android.R.id.content), new TapToRetryListener() {
            @Override
            public void reInitialize() {
                fetchTrustBuilderData();
            }
        }, null);
        if (savedInstanceState != null) {
            if (Utility.isSet(savedInstanceState.getString("docUploadType"))) {
                docUploadType = DOC_UPLOAD_TYPE.valueOf(savedInstanceState.getString("docUploadType"));
            }
            photoIdTypeKey = savedInstanceState.getString("photoIdTypeKey");
            mCurrentPhotoPath = savedInstanceState.getString("mCurrentPhotoPath");
            isRegistration = savedInstanceState.getBoolean("isRegistration", false);
        }

        onFileComplete = new GetFileCallback() {
            @Override
            public void onGetFileComplete(File file, Uri selectedImage, HttpRequestType httpRequestType) {
                if (file == null) {
                    AlertsHandler.showMessage(aActivity, R.string.cannot_select_file);
                    return;
                }
                getUploadBarAndType();
                photoUploader.startPhotoUploadFile(file, httpRequestType, upload_bar, tbEventActivity, uploadType, photoIdTypeKey, null);
            }
        };
        PhotoUploader.PhotoUploaderRequestInterface photoUploaderRequestInterface = new PhotoUploader.PhotoUploaderRequestInterface() {

            @Override
            public void onSuccess(String jsonResponseString) {
                hideLoaders();
                processDocResponse();
            }

            @Override
            public void onFail(String failMessage) {
                onComplete();
                AlertsHandler.showMessage(aActivity, failMessage);
            }

            @Override
            public void onComplete() {
                hideLoaders();

            }

            @Override
            public void onActivityResultSuccess(HttpRequestType type, String filePath) {
                getUploadBarAndType();
                photoUploader.startPhotoUpload(filePath, type, upload_bar, tbEventActivity, uploadType, photoIdTypeKey);

            }

            @Override
            public void onActivityResultSuccessFile(HttpRequestType httpRequestType, Uri selectedImage, String scheme) {
                FilesHandler.getFile(aActivity, selectedImage, scheme, onFileComplete, httpRequestType);
            }
        };
        photoUploader = new PhotoUploader(aActivity, photoUploaderRequestInterface, main_view, crop_view);
    }

    @Override
    protected void onDestroy() {
        hideLoaders();
        super.onDestroy();
    }

    private void getUploadBarAndType() {
        if (docUploadType == null) {
            AlertsHandler.showMessage(aActivity, R.string.cannot_select_file);
            return;
        }

        switch (docUploadType) {
            case PHOTO_ID:
                upload_bar = id_upload_bar;
                uploadType = PhotoUploader.UPLOAD_TYPE.PHOTO_ID;
                toggleView(content_id, true);
                photoIdTypeSpinner.setEnabled(false);
                break;
            case PROFILE_PIC:
                upload_bar = id_upload_bar;
                uploadType = PhotoUploader.UPLOAD_TYPE.PICTURE;
                toggleView(content_id, true);
                photoIdTypeSpinner.setEnabled(false);
                break;
            case PROFILE_PIC_ENDORSEMENT:
                upload_bar = id_upload_bar_endorsement;
                uploadType = PhotoUploader.UPLOAD_TYPE.PICTURE;
                toggleView(content_en, true);
                enableSpinnerUploadButton(verify_en, false);
                verify_en.setVisibility(View.GONE);
                break;
            default:
        }
    }

    private void showTrustBuilderContinueButton() {
        // String buttonText = isSkip ? "Skip for now" : "Continue";
        // continue_trust_view.setEnabled(!isSkip);
        continue_trust_view.setEnabled(tbData.isVerified());
        // continue_trust_view.setText(buttonText);
    }

    // isSilent is always False in this case
    private void fetchTrustBuilderData() {

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
                hideKeyboard();

            }
        }, 100);

//        if (!isSilent) {
        tapToRetryHandler.showLoaderWithCustomText(loadingMessage);
//        }
        loadingMessage = null;


        boolean showFailure = CachedDataHandler.showDataFromDb(aContext, ConstantsUrls.get_trust_builder_api_url(), cachedDataInterface);

        CachedDataHandler cachedDataHandler = new CachedDataHandler(ConstantsUrls.get_trust_builder_api_url(), cachedDataInterface, this, tbEventActivity, TrulyMadlyEventTypes.page_load, null, showFailure);
        Map<String, String> map = GetOkHttpRequestParams.getHttpRequestParams(HttpRequestType.TRUST_BUILDER);
        map.put("hash", CachingDBHandler.getHashValue(aContext, ConstantsUrls.get_trust_builder_api_url(), Utility.getMyId(aContext)));
        cachedDataHandler.httpGet(this, ConstantsUrls.get_trust_builder_api_url(), map);


//----
//        CustomOkHttpResponseHandler okHttpResponseHandler = new CustomOkHttpResponseHandler(aContext, tbEventActivity,
//                TrulyMadlyEventTypes.page_load) {
//            @Override
//            public void onRequestSuccess(JSONObject response) {
////                if (!isSilent) {
//                    tapToRetryHandler.onSuccessFull();
////                }
//                processTrustBuilderData(response.optJSONObject("data"));
//            }
//
//            @Override
//            public void onRequestFailure(Exception exception) {
////                if (!isSilent) {
//                    tapToRetryHandler.onNetWorkFailed(exception);
////                }
//            }
//        };
//        OkHttpHandler.httpGet(aContext, Constants.trust_builder_api_url,
//                GetOkHttpRequestParams.getHttpRequestParams(HttpRequestType.TRUST_BUILDER), okHttpResponseHandler);
//


    }

    public void clearCachedData() {
        CachingDBHandler.deleteURL(aContext, ConstantsUrls.get_trust_builder_api_url(), Utility.getMyId(aContext));
        CachingDBHandler.deleteURL(aContext, ConstantsUrls.get_my_profile_url(), Utility.getMyId(aContext));

    }

    private void processTrustBuilderData(JSONObject data) {

        tbData = new TrustBuilderData(data);

        setHeaderPercent(header_fb, tbData.getScoreFb());
        setHeaderPercent(header_li, tbData.getScoreLi());
        setHeaderPercent(header_id, tbData.getScoreId());
        setHeaderPercent(header_ph, tbData.getScorePh());
        setHeaderPercent(header_en, tbData.getScoreEn());
        if (tbData.getIsFbVerified()) {
            setFbVerified(tbData.getFbConnection(), !tbData.getIsAnyprofilePic());
        } else if (tbData.getFbDiff() != null) {
            onMismatchFacebook(tbData.getFbMismatch(), tbData.getFbDiff(), !tbData.getIsAnyprofilePic());
        }
        if (tbData.getIsLinkedInVerified()) {
            setLiVerified(tbData.getLiConnections());
        }
        showPhotoIdButton(tbData.getIsAnyprofilePic());
        if (tbData.getIsAnyprofilePic()) {
            profilePhotoSuccess();
        } else {
            if (!Utility.isSet(tbData.getIdStatus())) {
                error_id.setVisibility(View.GONE);
                success_id.setVisibility(View.GONE);
                message_id.setVisibility(View.VISIBLE);
            }
            trust_msg_endorsement_error_en.setVisibility(View.GONE);
            endorsement_profile_pic_success_en.setVisibility(View.GONE);
            trust_msg_endorsement_success_en.setVisibility(View.GONE);
            trust_msg_endorsement_normal_en.setVisibility(View.GONE);
        }

        if (tbData.getIsPhotoIdVerified()) {
            setIdVerified();
        } else if (tbData.getIdStatus() != null) {
            if (tbData.getIdStatus().equalsIgnoreCase("under_moderation")) {
                String successText = tbData.getId_messages().optString("moderation").replaceAll("idType",
                        tbData.getIsProofType());

                if (!tbData.getIsFbVerified()) {
                    successText += facebookRightAwayText;
                }
                showIdSuccess(successText);
            } else if (tbData.getIdStatus().equalsIgnoreCase("rejected")) {
                String rejectReason = tbData.getIsRejectReason();
                if (rejectReason.equalsIgnoreCase("password_required")) {
                    showIdError(tbData.getId_messages().optString("pwd_reqired"));
                    showIdPassword();
                }
                if (rejectReason.equalsIgnoreCase("not_clear")) {
                    showIdError(tbData.getId_messages().optString("not_clear"));
                    showPhotoIdButton(true);
                }
                if (rejectReason.equalsIgnoreCase("name_mismatch") || rejectReason.equalsIgnoreCase("age_mismatch")
                        || rejectReason.equalsIgnoreCase("name_age_mismatch")) {
                    onMismatchPhotoId(rejectReason, tbData.getId_diff_data());
                }
            } else if (tbData.getIdStatus().equalsIgnoreCase("fail")) {
                showIdError("Contact Trulymadly customer Care.");
                trust_photoid_buttons.setVisibility(View.GONE);
            }
        }

        if (tbData.getIsPhoneVerified()) {
            setPhVerified(tbData.getPhoneNo(), tbData.getNoOftrials());
        } else if (tbData.getNoOftrials() >= 3) {
            setPhRejected(tbData.getPhoneNo(), tbData.getNoOftrials());
        }

        if (tbData.getIsEndorsementVerified()) {
            setEnVerified();
        } else if (tbData.getEndorsers() != null && tbData.getEndorsers().size() > 0) {
            showEndorsers();
        } else {
            header_en.setOnClickListener(this);
            if (tbData.getIsAnyprofilePic()) {
                profilePhotoSuccess();
            } else {
                trust_msg_endorsement_error_en.setVisibility(View.GONE);
                endorsement_profile_pic_success_en.setVisibility(View.GONE);
                trust_msg_endorsement_success_en.setVisibility(View.GONE);
                trust_msg_endorsement_normal_en.setVisibility(View.GONE);
                profile_pic_endorsement.setVisibility(View.VISIBLE);
                enableSpinnerUploadButton(verify_en, false);
            }
        }

        showTrustBuilderContinueButton();
        drawTrustScore();

    }

    private void drawTrustScore() {
        if (tbData != null) {

            int score = tbData.getTrustScore();

            if (tbData.getTrustScore() == 0) {
                UiUtils.setBackground(aContext, trust_score_value_material_design,
                        R.drawable.trust_score_gradient_zero);
            } else {
                UiUtils.setBackground(aContext, trust_score_value_material_design, R.drawable.trust_score_gradient);
            }
            trust_score_value_material_design.setText(score + "%");
            native_tb_header_min_trust_score.setTextColor(getResources().getColor(R.color.final_red_1));

            if (!tbData.isVerified()) {
//                native_tb_header_min_trust_score.setText(tbData.getNotVerifiedText());
                native_tb_header_min_trust_score.setText(R.string.trust_score_message_0);
                native_tb_header_min_trust_score.setTextColor(getResources().getColor(R.color.red));

                if (!Utility.isMale(aContext)) {
                    native_tb_header_sub_text.setVisibility(View.VISIBLE);
                    native_tb_header_sub_text.setText(tbData.getFemaleNonAuthenticSubtext());
                }
            } else if (score == 100) {
                native_tb_header_sub_text.setVisibility(View.GONE);
                native_tb_header_min_trust_score.setTextColor(getResources().getColor(R.color.black));
//                native_tb_header_min_trust_score.setText(tbData.getTrustScoreMessageHundredScore());
                native_tb_header_min_trust_score.setText(R.string.trust_score_message_100);
            } else if (score >= 75) {
                native_tb_header_sub_text.setVisibility(View.GONE);
                native_tb_header_min_trust_score.setTextColor(getResources().getColor(R.color.black));
//                native_tb_header_min_trust_score.setText(tbData.getTrustScoreMessageHundredScore());
                native_tb_header_min_trust_score.setText(R.string.trust_score_message_75);
            } else if(score >= 30){
                native_tb_header_sub_text.setVisibility(View.GONE);
                native_tb_header_min_trust_score.setTextColor(getResources().getColor(R.color.black));
//                native_tb_header_min_trust_score.setText(tbData.getTrustScoreMessageZeroToHundredScore());
                native_tb_header_min_trust_score.setText(R.string.trust_score_message_30_45_55);
            } else{
                native_tb_header_sub_text.setVisibility(View.GONE);
                native_tb_header_min_trust_score.setTextColor(getResources().getColor(R.color.black));
//                native_tb_header_min_trust_score.setText(tbData.getTrustScoreMessageZeroToHundredScore());
                native_tb_header_min_trust_score.setText(R.string.trust_score_message_0_30);
            }

        }
    }

    @OnClick(R.id.continue_trust_view)
    public void finishTrustBuilder() {
        if (isActivityForResult) {
            if (isRegistration) {
                TrulyMadlyTrackEvent.trackEvent(aContext, tbEventActivity, TrulyMadlyEventTypes.skip, 0, null, null, true);
            }
            Intent intent = new Intent();
            intent.putExtra("isVerified", tbData != null && tbData.isVerified());
            setResult(RESULT_OK, intent);
            finish();
        } else {
            if (isPush) {
                ActivityHandler.startMatchesActivity(aContext);
            }
            if (isRegistration) {
                ActivityHandler.startProfileActivity(aContext, ConstantsUrls.get_my_profile_url(), false, true, true);
            }
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        isFromActivityResult = true;

        LISessionManager.getInstance(getApplicationContext()).onActivityResult(
                this, requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case Constants.SHARE_VIA_TEXT_INTENT:
                if (!tbData.getIsFbVerified()) {
                    trust_msg_endorsement_normal_en.setText(tbData.getEnMessage() + facebookRightAwayText);
                }
                AlertsHandler.showMessage(aActivity, tbData.getEnRequestMessage(), false);
                endorsement_profile_pic_success_en.setVisibility(View.GONE);
                break;
            case Constants.PICK_FROM_CAMERA_OWN:
            case Constants.PICK_IMAGE_FROM_SYSTEM:
            case Constants.PICK_FROM_GALLERY:
            case Constants.PICK_FILE_FROM_SYSTEM:
                photoUploader.onActivityResult(requestCode, resultCode, data);
                break;
            default:
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        if (isPhotoOptionShowing) {
            launchDocUploaderSlider(docUploadType, false, true);
        } else if (crop_view.getVisibility() == View.VISIBLE) {
            crop_view.setVisibility(View.GONE);
            main_view.setVisibility(View.VISIBLE);
        } else if (isOtpViewVisible) {
            otp_view.setVisibility(View.GONE);
            isOtpViewVisible = false;
            stopTimer();
        } else if (isInformationViewVisible) {
            informationView.setVisibility(View.GONE);
            isInformationViewVisible = false;
        } else if (importLinkedInAdapterOauth2 != null && importLinkedInAdapterOauth2.isWebViewVisible()) {
            importLinkedInAdapterOauth2.hide();
        } else if (isRegistration) {
            Utility.minimizeApp(this);
        } else {
            finishTrustBuilder();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        showTrustBuilderContinueButton();
        boolean isUploading = docUploadRequest != null && !docUploadRequest.isCancelled() && !docUploadRequest.isDone();
        if (!isFromActivityResult && !isUploading && !isOtpViewVisible && !isCallVerificationInProgress) {
            fetchTrustBuilderData();
        }
        isFromActivityResult = false;
    }

    @Override
    protected void onPause() {
        isInformationViewVisible = false;
        super.onPause();
    }

    @Override
    public void onStart() {
        super.onStart();

        if (isInformationViewVisible)
            informationView.setVisibility(View.VISIBLE);
        else
            informationView.setVisibility(View.GONE);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private void startTimer() {
        stopTimer();

        callMeEnableTimer = new Timer();
        timer_icon.setVisibility(View.VISIBLE);
        callMeEnableTimer.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        TextView tv = (TextView) findViewById(R.id.otp_timer);
                        if (seconds >= 10) {
                            tv.setText(String.valueOf(minutes) + ":" + String.valueOf(seconds));
                        } else {
                            tv.setText(String.valueOf(minutes) + ":0" + String.valueOf(seconds));
                        }

                        seconds -= 1;

                        if (minutes == 0 && seconds == 0) {
                            enableSpinnerUploadButton(call_button, true);
                            tv.setVisibility(View.GONE);
                            stopTimer();
                        } else if (seconds == 0) {
                            tv.setText(String.valueOf(minutes) + ":" + String.valueOf(seconds));

                            seconds = 60;
                            minutes = minutes - 1;

                        }

                    }

                });
            }

        }, 0, 1000);

    }

    private void stopTimer() {
        if (callMeEnableTimer != null) {
            callMeEnableTimer.cancel();
            callMeEnableTimer = null;
            minutes = 9;
            seconds = 59;
            otp_timer.setVisibility(View.GONE);
            timer_icon.setVisibility(View.GONE);
        }
    }

    private void sendOTP() {

        showLoaders();

        CustomOkHttpResponseHandler sendOTPHttpResponseHandler = new CustomOkHttpResponseHandler(aContext,
                tbEventActivity, TrulyMadlyEventTypes.otp_send) {

            @Override
            public void onRequestSuccess(JSONObject responseJSON) {
                hideLoaders();
                int responseCode = responseJSON.optInt("responseCode");

                if (responseCode == 403) {
                    hideLoaders();
//                    setCallMeEnabled(responseJSON.optBoolean("call_me"));
                    setCallMeEnabled(false);
                    hideKeyboard();
                    stopTimer();
                    TrulyMadlyTrackEvent.trackEvent(aContext, tbEventActivity,
                            TrulyMadlyEventTypes.otp_number_verify_403, 0, responseJSON.optString("error"), null,
                            false);
                    enableSpinnerUploadButton(call_button, true);
                    incorrect_otp_text_view.setVisibility(View.VISIBLE);
                    incorrect_otp_text_view.setText(responseJSON.optString("error"));

                } else if (responseCode == 200) {
                    hideLoaders();
                    if (responseJSON.optBoolean("is_otp_matched")) {
                        clearCachedData();
                        otp_view.setVisibility(View.GONE);
                        stopTimer();
                        isOtpViewVisible = false;
                        setPhVerified(phone_no_text.getText().toString(), 0);
                        hideKeyboard();
                        drawTrustScore();
                        TrulyMadlyTrackEvent.trackEvent(aContext, tbEventActivity,
                                TrulyMadlyEventTypes.otp_number_verify_success, 0, null, null, false);
                    } else {
                        incorrect_otp_text_view.setVisibility(View.VISIBLE);
                        incorrect_otp_text_view.setText(getResources().getString(R.string.incorrect_otp));
                        hideKeyboard();

                        otp_view.setVisibility(View.VISIBLE);
                        isOtpViewVisible = true;
                        TrulyMadlyTrackEvent.trackEvent(aContext, tbEventActivity,
                                TrulyMadlyEventTypes.otp_number_verify_incorrect, 0, null, null, false);

                    }
                }

            }

            @Override
            public void onRequestFailure(Exception exeption) {
                hideLoaders();
                AlertsHandler.showMessage(aActivity, R.string.ERROR_NETWORK_FAILURE);
            }

        };
        HashMap<String, String> params = new HashMap<>();
        params.put("action", "checkOTP");
        params.put("number", phone_no_text.getText().toString());
        params.put("otp", otpEditText.getText().toString());
        OkHttpHandler.httpPost(aContext, ConstantsUrls.get_otp_url(), params, sendOTPHttpResponseHandler);

    }

    private void initializeOtpView() {
        otpEditText.setText("");
        otpEditText.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(otpEditText, InputMethodManager.SHOW_IMPLICIT);
        // Utility.showKeyBoard(aActivity);
        incorrect_otp_text_view.setVisibility(View.GONE);
        otp_timer.setVisibility(View.VISIBLE);
        stopTimer();

    }

    private void otpVerification() {
        sending_view.setVisibility(View.VISIBLE);
        CustomOkHttpResponseHandler otpHttpResponseHandler = new CustomOkHttpResponseHandler(aContext, tbEventActivity,
                TrulyMadlyEventTypes.otp_verify) {

            @Override
            public void onRequestSuccess(JSONObject responseJSON) {
                hideLoaders();
                int responseCode = responseJSON.optInt("responseCode");

                if (responseCode == 200) {
                    sending_view.setVisibility(View.GONE);
                    otp_view.setVisibility(View.VISIBLE);
                    isOtpViewVisible = true;
                    enableSpinnerUploadButton(call_button, false);
                    TrulyMadlyTrackEvent.trackEvent(aContext, tbEventActivity,
                            TrulyMadlyEventTypes.otp_number_send_success, 0, null, null, false);
                    initializeOtpView();
                    //startTimer();
                } else if (responseCode == 403) {
                    showPhError(responseJSON.optString("error"));
                    //setCallMeEnabled(responseJSON.optBoolean("call_me"));
                    setCallMeEnabled(false);
                    sending_view.setVisibility(View.GONE);
                    TrulyMadlyTrackEvent.trackEvent(aContext, tbEventActivity, TrulyMadlyEventTypes.otp_number_send_403,
                            0, responseJSON.optString("error"), null, false);
                }
            }

            @Override
            public void onRequestFailure(Exception exception) {
                hideLoaders();
                sending_view.setVisibility(View.GONE);
                AlertsHandler.showNetworkError(aActivity, exception);
            }

        };

        HashMap<String, String> params = new HashMap<>();
        params.put("action", "numberVerifyOTP");
        params.put("number", phone_no_text.getText().toString());
        OkHttpHandler.httpPost(aContext, ConstantsUrls.get_otp_url(), params, otpHttpResponseHandler);

    }

    private void setCallMeEnabled(boolean isEnabled) {
        isCallMeEnabled = isEnabled;
        if (isCallMeEnabled) {
            verify_ph.setText(R.string.call_me);
        } else {
            verify_ph.setText(R.string.verify);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.call_button:
                otp_view.setVisibility(View.GONE);
                stopTimer();
                isOtpViewVisible = false;
                startPhoneVerification();
                hideKeyboard();

                break;
            case R.id.why_button:
                informationView.setVisibility(View.VISIBLE);
                isInformationViewVisible = true;
                TrulyMadlyTrackEvent.trackEvent(aContext, tbEventActivity, TrulyMadlyEventTypes.why_verify_clicked, 0, null,
                        null, false);
                break;

            case R.id.otp_view:
                break;

            case R.id.why_button_layout:
                break;

            case R.id.sending_view:
                break;

            case R.id.tb_information_close_textview:
                informationView.setVisibility(View.GONE);
                isInformationViewVisible = false;

                break;
            case R.id.trust_header_facebook:
                if (!tbData.getIsFbVerified())
                    toggleView(content_fb);
                break;
            case R.id.trust_header_linkedin:
                if (!tbData.getIsLinkedInVerified())
                    toggleView(content_li);
                break;
            case R.id.trust_header_photoid:
                if (tbData.getIsAnyprofilePic()) {
                    profilePhotoSuccess();

                }

                if (!tbData.getIsPhotoIdVerified()) {
                    toggleView(content_id);
                }

                break;
            case R.id.trust_header_phone:
                if (!tbData.getIsPhoneVerified()) {
                    toggleView(content_ph);
                }
                break;
            case R.id.trust_header_endorsement:
                MoEHandler.trackEvent(aContext, MoEHandler.Events.ENDORSEMENT_CLICK);
                if (tbData.getIsAnyprofilePic()) {
                    profilePhotoSuccess();
                }
                if (!tbData.getIsEndorsementVerified()) {
                    toggleView(content_en);
                }
                break;
            case R.id.trust_verify_facebook:
                facebookLoginCallBack.logout();
                if (v.getTag() == null) {
                    fbReimport = false;
                } else if (((String) v.getTag()).equalsIgnoreCase("reimport")) {
                    fbReimport = true;
                }
                facebookLoginCallBack.login();
                TrulyMadlyTrackEvent.trackEvent(aContext, tbEventActivity, TrulyMadlyEventTypes.fb_click, 0, null, null, true);
                showLoaders();
                break;
            case R.id.trust_verify_linkedin:
                if (enableLinkedIn) {
                    enableLinkedIn = false;
                    linkedInVerifyButtonClicked();
                }
                break;
            case R.id.trust_verify_photoid:
                if (isPhotoOptionShowing) {
                    launchDocUploaderSlider(DOC_UPLOAD_TYPE.PHOTO_ID, false, true);
                } else {
                    TrulyMadlyTrackEvent.trackEvent(aContext, tbEventActivity, TrulyMadlyEventTypes.id_click, 0, null,
                            null, true);
                    launchDocUploaderSlider(DOC_UPLOAD_TYPE.PHOTO_ID, true, true);
                }
                break;

            case R.id.add_pic_imageview_endorsement:
                upload(DOC_UPLOAD_TYPE.PROFILE_PIC_ENDORSEMENT);
                break;
            case R.id.add_pic_imageview:
                upload(DOC_UPLOAD_TYPE.PROFILE_PIC);
                break;
            case R.id.trust_verify_photoid_password:
                sendPhotoIdPassword();
                break;
            case R.id.trust_verify_photoid_mismatch:
                sendPhotoIsMismatchSync((String) v.getTag());
                break;
            case R.id.trust_verify_phone:
                TrulyMadlyTrackEvent.trackEvent(aContext, tbEventActivity, TrulyMadlyEventTypes.phone_click, 0, null, null, true);
                if (isCallMeEnabled) {
                    startPhoneVerification();
                    hideKeyboard();
                } else if (Utility.isValidPhoneNo(phone_no_text.getText().toString())) {

                    AlertsHandler.showConfirmDialog(aActivity,
                            getResources().getString(R.string.phone_number_confirmation_message) + " +91-"
                                    + phone_no_text.getText().toString() + " ?",
                            R.string.yes, R.string.no, new ConfirmDialogInterface() {

                                @Override
                                public void onPositiveButtonSelected() {
                                    otpVerification();
                                    // otp_view.setVisibility(View.VISIBLE);

                                }

                                @Override
                                public void onNegativeButtonSelected() {

                                }
                            }, false);
                }
                break;
            case R.id.trust_verify_endorsement:
            case R.id.add_more_endorsement:
                TrulyMadlyTrackEvent.trackEvent(aContext, tbEventActivity, TrulyMadlyEventTypes.endorsement_click, 0, null,
                        null, true);
                if (ActivityHandler.appInstalledOrNot(aContext, Constants.packageFacebookmessenger)) {
                    launchDocUploaderSlider(DOC_UPLOAD_TYPE.ENDORSEMENT, true, true);
                } else {
                    shareEndorsementUrlViaIntent(false);
                }
                break;
            case R.id.show_photo_slider_layout:
                launchDocUploaderSlider(docUploadType, false, true);
                break;
            case R.id.take_from_camera_layout:
                launchDocUploaderSlider(docUploadType, false, false);
                switch (docUploadType) {
                    case PROFILE_PIC:
                    case PROFILE_PIC_ENDORSEMENT:
                        photoUploader.takeFromCamera(PhotoUploader.UPLOAD_TYPE.PICTURE);
                        break;
                    case PHOTO_ID:
                        photoUploader.takeFromCamera(PhotoUploader.UPLOAD_TYPE.PHOTO_ID);
                        break;
                    case ENDORSEMENT:
                        shareEndorsementUrlViaIntent(false);
                        break;
                    default:
                        break;
                }

                break;
            case R.id.add_from_gallery_layout:
                launchDocUploaderSlider(docUploadType, false, false);
                switch (docUploadType) {
                    case PROFILE_PIC:
                    case PROFILE_PIC_ENDORSEMENT:
                        photoUploader.fetchFromGallery(PhotoUploader.UPLOAD_TYPE.PICTURE);
                        break;
                    case PHOTO_ID:
                        photoUploader.fetchFileFromSystem(PhotoUploader.UPLOAD_TYPE.PHOTO_ID);
                        break;
                    case ENDORSEMENT:
                        shareEndorsementUrlViaIntent(true);
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }

    }

    private void profilePhotoSuccess() {

        profile_pic_endorsement.setVisibility(View.GONE);
        if(!tbData.getIsEndorsementVerified()) {
            trust_msg_endorsement_normal_en.setVisibility(View.VISIBLE);
            trust_msg_endorsement_normal_en.setText(tbData.getEnMessage());
        }else{
            trust_msg_endorsement_success_en.setVisibility(View.VISIBLE);
            ((TextView)trust_msg_endorsement_success_en).setText(tbData.getEnSuccessMessage());
        }
        tbData.setIsAnyprofilePic(true);
        verify_en.setVisibility(View.VISIBLE);
        verify_en.setEnabled(true);

        enableSpinnerUploadButton(verify_en, true);

        trust_photoid_buttons.setVisibility(View.VISIBLE);
        addOwnProfilePic.setVisibility(View.GONE);
        verticalLine.setVisibility(View.GONE);

        if (tbData.getIdStatus() != null && !(tbData.getIdStatus().equalsIgnoreCase("under_moderation")
                || tbData.getIdStatus().equalsIgnoreCase("fail"))) {
            showPhotoIdButton(true);
        } else if (!tbData.getIsPhotoIdVerified()) {
            trust_photoid_buttons.setVisibility(View.GONE);
            verify_id.setVisibility(View.GONE);
        }
    }

    private void linkedInVerifyButtonClicked() {

        TrulyMadlyTrackEvent.trackEvent(aContext, tbEventActivity, TrulyMadlyEventTypes.linkedin_popup, 0, null, null, true);
        isFromActivityResult = true;
        if (ActivityHandler.appInstalledOrNot(aContext, Constants.packageLinkedIn)) {
            showLoaders();
            importLinkedInAdapterNative.importFromLinkedIn(aActivity);
        } else {
            importLinkedInAdapterOauth2.importFromLinkedIn();
        }

    }

    private void upload(DOC_UPLOAD_TYPE uploadType) {
        if (isPhotoOptionShowing) {
            launchDocUploaderSlider(uploadType, false, true);
        } else {
            TrulyMadlyTrackEvent.trackEvent(aContext, tbEventActivity, TrulyMadlyEventTypes.uploadpic_click, 0, null,
                    null, true);
            launchDocUploaderSlider(uploadType, true, true);
        }
    }

    private void shareEndorsementUrlViaIntent(final boolean forceFacebook) {
        // String gender = Utility.getBool(aContext,
        // Constants.SHARED_KEYS_USER_GENDER);
//        final String text = "Hey back me up on TrulyMadly! ";
        final String text = getString(R.string.endorse_share_message) + " ";
        final String subject = getString(R.string.endorse_share_subject);

        GooGlUrlShortnerInterface responseInterface = new GooGlUrlShortnerInterface() {

            @Override
            public void onComplete(String shortUrl) {
                if (forceFacebook) {
                    ActivityHandler.createTextIntentForFacebook(aActivity, text + shortUrl, true);
                } else {
                    ActivityHandler.createTextIntent(aActivity, text + shortUrl, subject, true);
                }

            }
        };
        new GooGlUrlShortner(aContext, responseInterface, tbData.getEnLink());

    }

    /* FACEBOOK */
    private void onMismatchFacebook(String mismatch, JSONObject diff, boolean isFbPhotoDeclined) {
        String text = mismatch;
        tbData.setIsAnyprofilePic(!isFbPhotoDeclined);
        if (tbData.getFb_messages() != null) {
            if (mismatch.equalsIgnoreCase("Name and Age mismatch")) {
                text = tbData.getFb_messages().optString("name_age_mismatch").replace("tmAge", diff.optString("TMage"))
                        .replace("fbAge", diff.optString("fbAge")).replace("tmName", diff.optString("TMname"))
                        .replace("fbName", diff.optString("fbName"));
            }
            if (mismatch.equalsIgnoreCase("Age mismatch")) {
                text = tbData.getFb_messages().optString("age_mismatch").replace("tmAge", diff.optString("TMage"))
                        .replace("fbAge", diff.optString("fbAge"));
            }
            if (mismatch.equalsIgnoreCase("Name mismatch")) {
                text = tbData.getFb_messages().optString("name_mismatch").replace("tmName", diff.optString("TMname"))
                        .replace("fbName", diff.optString("fbName"));
            }
        }
        showFbError(text);
        ((TextView) verify_fb).setText(R.string.sync_with_facebook);
        verify_fb.setTag("reimport");
    }

    private void showFbError(int text_res_id) {
        showFbError(getResources().getString(text_res_id));
    }

    private void showFbError(String text) {

        message_fb.setVisibility(View.GONE);
        ((TextView) error_fb).setText(text);
        error_fb_header.setVisibility(View.VISIBLE);
        error_fb.setVisibility(View.VISIBLE);
    }

    private void setFbVerified(String fbConnections, boolean isFbPhotoDeclined) {

        if (!tbData.getIsFbVerified()) {
            tbData.setIsFbVerified(true);
            tbData.setTrustScore(tbData.getTrustScore() + tbData.getScoreFb());
        }
        tbData.setIsAnyprofilePic(!isFbPhotoDeclined);
        tbData.setFbConnection(fbConnections);
        header_text_fb_connections.setVisibility(View.VISIBLE);
        ((TextView) header_text_fb_connections).setText(fbConnections + " " + getResources().getString(R.string.friends));
        setVerified(header_fb, content_fb);
        error_fb_header.setVisibility(View.GONE);
        showTrustBuilderContinueButton();
    }

    private void showLiError(String text) {
        hideLoaders();
        message_li.setVisibility(View.GONE);
        ((TextView) error_li).setText(text);
        error_li.setVisibility(View.VISIBLE);
        error_li_header.setVisibility(View.VISIBLE);
    }

	/*-------------*/

	/* LINKED IN */

    private void setLiVerified(String liConnections) {

        if (!tbData.getIsLinkedInVerified()) {
            tbData.setIsLinkedInVerified(true);
            tbData.setTrustScore(tbData.getTrustScore() + tbData.getScoreLi());
        }
        tbData.setLiConnections(liConnections);
        ((TextView) header_text_li_connections).setText(liConnections);
        header_text_li_connections.setVisibility(View.VISIBLE);
        // REMOVING RE-VERIFY
        // ((TextView) verify_li).setText("RE-VERIFY");
        setVerified(header_li, content_li);
        error_li_header.setVisibility(View.GONE);
        showTrustBuilderContinueButton();
    }

    private void sendLinkedInResponseToServer(final LinkedInData liData) {

        HashMap<String, String> params = new HashMap<>();

        params.put("member_id", liData.getMember_id());
        params.put("firstName", liData.getFirstName());
        params.put("lastName", liData.getLastName());
        params.put("numConnections", liData.getNumConnections());

        params.put("connected_from", "trustbuilder");

        if (Utility.isSet(liData.getCurrent_designation())) {
            params.put("current_designation", liData.getCurrent_designation());
        }
        if (Utility.isSet(liData.getProfile_pic_url())) {
            params.put("profile_pic", liData.getProfile_pic_url());
        }

        params.put("linkedin_data", liData.getLinkedin_data());

        CustomOkHttpResponseHandler okHttpResponseHandler = new CustomOkHttpResponseHandler(aContext, tbEventActivity,
                TrulyMadlyEventTypes.linkedin_server_call) {
            @Override
            public void onRequestSuccess(JSONObject response) {
                hideLoaders();
                switch (response.optInt("responseCode")) {
                    case 200:
                        clearCachedData();
                        if (Utility.isSet(liData.getProfile_pic_url())) {
                            profilePhotoSuccess();
                        }
                        setLiVerified(liData.getNumConnections() + " Connections");
                        drawTrustScore();
                        break;
                    case 403:
                        showLiError(response.optString("error", "An error occured"));
                        break;
                    default:
                        onRequestFailure(null);
                        break;
                }
            }

            @Override
            public void onRequestFailure(Exception exception) {
                hideLoaders();
                AlertsHandler.showNetworkError(aActivity, exception);
            }
        };
        OkHttpHandler.httpPost(aContext, ConstantsUrls.get_linkedin_url(), params, okHttpResponseHandler);
    }

    private void onMismatchPhotoId(String mismatch, JSONObject diff) {
        String text = mismatch;
        if (tbData.getId_messages() != null) {
            if (mismatch.equalsIgnoreCase("name_age_mismatch")) {
                text = tbData.getId_messages().optString("name_age_mismatch").replace("tmAge", diff.optString("TMage"))
                        .replace("fbAge", diff.optString("IDage")).replace("tmName", diff.optString("TMname"))
                        .replace("fbName", diff.optString("IDname"));
            }
            if (mismatch.equalsIgnoreCase("age_mismatch")) {
                text = tbData.getId_messages().optString("age_mismatch").replace("tmAge", diff.optString("TMage"))
                        .replace("fbAge", diff.optString("IDage"));
            }
            if (mismatch.equalsIgnoreCase("name_mismatch")) {
                text = tbData.getId_messages().optString("name_mismatch").replace("tmName", diff.optString("TMname"))
                        .replace("fbName", diff.optString("IDname"));
            }
        }
        showIdError(text);
        trust_verify_photoid_mismatch.setTag(mismatch);
        trust_photoid_password_buttons.setVisibility(View.GONE);
        trust_photoid_missmatch_buttons.setVisibility(View.VISIBLE);
        showPhotoIdButton(true);
    }

	/*-------------*/

	/* PHOTO ID */

    protected void onRejectedPhotoId(String message) {
        tbData.setIsPhotoIdVerified(false);
        tbData.setRejected_message(message);
        verify_id.setVisibility(View.GONE);
        showIdError(message);
    }

    private void showIdError(String text) {
        success_id.setVisibility(View.GONE);
        message_id.setVisibility(View.GONE);
        ((TextView) error_id).setText(text);
        error_id.setVisibility(View.VISIBLE);
        error_id_header.setVisibility(View.VISIBLE);
        trust_photoid_password_buttons.setVisibility(View.GONE);
        trust_photoid_missmatch_buttons.setVisibility(View.GONE);
        if (!text.equalsIgnoreCase(picSmallError)) {
            addOwnProfilePic.setVisibility(View.GONE);
            verticalLine.setVisibility(View.GONE);
        }
    }

    private void showIdPassword() {
        trust_photoid_password_buttons.setVisibility(View.VISIBLE);
        trust_photoid_missmatch_buttons.setVisibility(View.GONE);
        showPhotoIdButton(true);
        enableSpinnerUploadButton(verify_id, false);
    }

    private void showIdSuccess(String text) {
        message_id.setVisibility(View.GONE);
        error_id.setVisibility(View.GONE);
        error_id_header.setVisibility(View.GONE);
        ((TextView) success_id).setText(text);
        success_id.setVisibility(View.VISIBLE);
        addOwnProfilePic.setVisibility(View.GONE);
        verticalLine.setVisibility(View.GONE);
        // --
        // trust_verify_photoid

        trust_photoid_password_buttons.setVisibility(View.GONE);
        trust_photoid_missmatch_buttons.setVisibility(View.GONE);
        trust_photoid_buttons.setVisibility(View.GONE);
        verify_id.setVisibility(View.GONE);

    }

    private void setIdVerified() {

        if (!tbData.getIsPhotoIdVerified()) {
            tbData.setIsPhotoIdVerified(true);
            tbData.setTrustScore(tbData.getTrustScore() + tbData.getScoreId());
        }
        error_id_header.setVisibility(View.GONE);
        header_text_id_connections.setVisibility(View.VISIBLE);

        ((TextView) header_text_id_connections).setText(tbData.getIsProofType());
        setVerified(header_id, content_id);
        showTrustBuilderContinueButton();
    }

    private void showPhotoIdButton(boolean isAnyProfilePic) {
        trust_photoid_buttons.setVisibility(View.VISIBLE);
        if (!isAnyProfilePic) {
            addOwnProfilePic.setVisibility(View.VISIBLE);
            verticalLine.setVisibility(View.VISIBLE);
            photoIdTypeSpinner.setVisibility(View.VISIBLE);
            photoIdTypeSpinner.setEnabled(false);
            if (photoIdTypeSpinner.getChildAt(0) != null) {
                ((TextView) photoIdTypeSpinner.getChildAt(0)).setTextColor(getResources().getColor(R.color.gray));
                UiUtils.setBackground(aContext, photoIdTypeSpinner.getChildAt(0),
                        R.drawable.material_design_edit_register_spinner_icon_id);
                verify_id.setVisibility(View.VISIBLE);
            }

        } else {
            addOwnProfilePic.setVisibility(View.GONE);
            verticalLine.setVisibility(View.GONE);
            photoIdTypeSpinner.setVisibility(View.VISIBLE);
            photoIdTypeSpinner.setEnabled(true);
            if (photoIdTypeSpinner.getChildAt(0) != null) {
                ((TextView) photoIdTypeSpinner.getChildAt(0)).setTextColor(getResources().getColor(R.color.dark_gray));
                UiUtils.setBackground(aContext, photoIdTypeSpinner.getChildAt(0),
                        R.drawable.material_design_edit_register_spinner_icon);
                verify_id.setVisibility(View.VISIBLE);
            }
        }
    }

    private void createPhotoIdTypeSpinner(PhotoIdType[] photoIdTypes) {

        CustomSpinnerArrayAdapter spinnerPhotoIdTypesAdapter = new CustomSpinnerArrayAdapter(this,
                (tbData != null && tbData.getIsAnyprofilePic()) ? R.layout.custom_spinner_4 : R.layout.custom_spinner_3,
                photoIdTypes);
        spinnerPhotoIdTypesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        if (photoIdTypeSpinner != null) {
            photoIdTypeSpinner.setAdapter(spinnerPhotoIdTypesAdapter);
        }

    }

    private void sendPhotoIsMismatchSync(String tag) {

        CustomOkHttpResponseHandler okHttpResponseHandler = new CustomOkHttpResponseHandler(aContext) {

            @Override
            public void onRequestSuccess(JSONObject response) {
                clearCachedData();
                setIdVerified();
                drawTrustScore();
            }

            @Override
            public void onRequestFailure(Exception exeption) {
                showPhError(R.string.ERROR_NETWORK_FAILURE);
            }
        };
        OkHttpHandler.httpPost(aContext, ConstantsUrls.get_trust_builder_api_url(),
                GetOkHttpRequestParams.getHttpRequestParams(HttpRequestType.VERIFY_PHOTOID_MISMATCH_SYNC, tag),
                okHttpResponseHandler);
    }

    private void sendPhotoIdPassword() {
        String pass = trust_photoid_password_edittext.getText().toString().trim();
        if (pass.length() == 0) {
            return;
        }

        CustomOkHttpResponseHandler okHttpResponseHandler = new CustomOkHttpResponseHandler(aContext) {

            @Override
            public void onRequestSuccess(JSONObject response) {
                String successText = getResources().getString(R.string.document_upload_successfull);
                if (!tbData.getIsFbVerified()) {
                    successText += facebookRightAwayText;
                }
                showIdSuccess(successText);
            }

            @Override
            public void onRequestFailure(Exception exeption) {
                showPhError(R.string.ERROR_NETWORK_FAILURE);
            }
        };
        OkHttpHandler.httpPost(aContext, ConstantsUrls.get_trust_builder_api_url(),
                GetOkHttpRequestParams.getHttpRequestParams(HttpRequestType.VERIFY_PHOTOID_PASSWORD, pass),
                okHttpResponseHandler);
    }

    /*-------------*/

    /* PHONE */

    private void setPhVerified(String phoneNo, int noOfTrials) {

        if (!tbData.getIsPhoneVerified()) {
            MoEHandler.trackEvent(aContext, MoEHandler.Events.PHONE_VERIFICATION);
            tbData.setIsPhoneVerified(true);
            tbData.setTrustScore(tbData.getTrustScore() + tbData.getScorePh());
        }
        tbData.setPhoneNo(phoneNo);
        tbData.setNoOftrials(noOfTrials);
        ((TextView) header_text_ph_connections).setText("+91-" + phoneNo);
        header_text_ph_connections.setVisibility(View.VISIBLE);
        setVerified(header_ph, content_ph);
        error_ph_header.setVisibility(View.GONE);
        showTrustBuilderContinueButton();

    }


    private void setPhRejected(String phoneNo, int noOfTrials) {


        tbData.setIsPhoneVerified(false);
        tbData.setPhoneNo(phoneNo);
        tbData.setNoOftrials(noOfTrials);
        showPhError(tbData.getRejected_message());
        phone_no_text.setVisibility(View.GONE);
        verify_ph.setVisibility(View.GONE);
        phone_verifying.setVisibility(View.GONE);
    }

    private void showPhError(int error_res_id) {
        showPhError(getResources().getString(error_res_id));
    }

    private void showPhError(String error) {
        ((TextView) error_ph).setText(error);
        error_ph.setVisibility(View.VISIBLE);
        error_ph_header.setVisibility(View.VISIBLE);
        verify_ph.setVisibility(View.VISIBLE);
        phone_verifying.setVisibility(View.GONE);
    }

    private void startPhoneVerification() {
        String phoneNo = phone_no_text.getText().toString().trim();
        error_ph.setVisibility(View.GONE);
        if (phoneNo.length() == 0) {
        } else if (!Utility.isValidPhoneNo(phoneNo)) {
            showPhError(R.string.enter_phone_error);
        } else {
            sendPhoneRequestToServer(phoneNo);
        }
    }


    private void sendPhoneRequestToServer(String phoneNo) {
        verify_ph.setVisibility(View.GONE);
        phone_verifying.setVisibility(View.VISIBLE);
        phone_no_text.setEnabled(false);

        final long trkStartTime = (new java.util.Date()).getTime();
        CustomOkHttpResponseHandler okHttpResponseHandler = new CustomOkHttpResponseHandler(aContext, tbEventActivity,
                TrulyMadlyEventTypes.phone_call_number_send) {

            @Override
            public void onRequestSuccess(JSONObject response) {
                switch (response.optInt("responseCode")) {
                    case 200:
                        pollForPhoneVerification(trkStartTime);
                        break;
                    case 403:
                        phone_no_text.setEnabled(true);
                        showPhError(response.optString("error", "An error occured"));
                        break;
                    default:
                        onRequestFailure(null);
                        phone_no_text.setEnabled(true);
                        break;
                }
            }

            @Override
            public void onRequestFailure(Exception exeption) {
                phone_no_text.setEnabled(true);
                showPhError(R.string.ERROR_NETWORK_FAILURE);
            }
        };
        isCallVerificationInProgress = true;
        OkHttpHandler.httpPost(aContext, ConstantsUrls.get_trust_builder_api_url(),
                GetOkHttpRequestParams.getHttpRequestParams(HttpRequestType.VERIFY_PHONE, phoneNo),
                okHttpResponseHandler);

    }

    private void pollForPhoneVerification(final long trkStartTime) {

        CustomOkHttpResponseHandler okHttpResponseHandler = new CustomOkHttpResponseHandler(aContext) {

            @Override
            public void onRequestSuccess(JSONObject response) {

                switch (response.optInt("responseCode")) {
                    case 200:
                        processPollForPhoneResponse(response, trkStartTime);
                        break;
                    case 403:
                        phone_no_text.setEnabled(true);
                        processPollForPhoneResponse(response, trkStartTime);
                        break;
                    default:
                        isCallVerificationInProgress = false;
                        onRequestFailure(null);
                        phone_no_text.setEnabled(true);
                        break;
                }
            }

            @Override
            public void onRequestFailure(Exception exeption) {
                isCallVerificationInProgress = false;
                phone_no_text.setEnabled(true);
                showPhError(R.string.ERROR_NETWORK_FAILURE);
            }
        };
        isCallVerificationInProgress = true;
        OkHttpHandler.httpPost(aContext, ConstantsUrls.get_trust_builder_api_url(),
                GetOkHttpRequestParams.getHttpRequestParams(HttpRequestType.POLL_PHONE), okHttpResponseHandler);

    }

    private void processPollForPhoneResponse(JSONObject response, final long trkStartTime) {
        int noOfTrials = response.optInt("number_of_trials", 0);
        String status = response.optString("status");
        String phNo = response.optString("user_number");
        String event_status = null;
        if (status.equalsIgnoreCase("under_moderation")) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            pollForPhoneVerification(trkStartTime);
                        }
                    }, 5000);
                }
            });
        } else if (status.equalsIgnoreCase("notpick")) {
            phone_no_text.setEnabled(true);
            isCallVerificationInProgress = false;
            event_status = status;
            showPhError(response.optString("error"));
        } else if (status.equalsIgnoreCase("rejected")) {
            phone_no_text.setEnabled(true);
            isCallVerificationInProgress = false;
            event_status = status;
            if (noOfTrials >= 3) {
                clearCachedData();
                setPhRejected(phNo, noOfTrials);
            } else {
                showPhError(response.optString("error"));
            }
        } else if (status.equalsIgnoreCase("active")) {
            phone_no_text.setEnabled(true);
            isCallVerificationInProgress = false;
            event_status = "success";
            setPhVerified(phNo, noOfTrials);
            drawTrustScore();
            clearCachedData();
        }
        if (event_status != null) {
            long timeDiff = TimeUtils.getSystemTimeInMiliSeconds() - trkStartTime;
            TrulyMadlyTrackEvent.trackEvent(aContext, tbEventActivity, TrulyMadlyEventTypes.phone_server_call, timeDiff,
                    event_status, null, true);
        }

    }

    /*-------------*/

    /* ENDORSEMENT */
    private void showEnSuccess(String text) {

        if (tbData.getEndorsers().size() < tbData.getEnThresholdCount()) {
            trust_msg_endorsement_normal_en.setVisibility(View.VISIBLE);
            trust_msg_endorsement_normal_en.setText(tbData.getEnMessage());
        } else {
            ((TextView) trust_msg_endorsement_success_en).setText(text);
            trust_msg_endorsement_success_en.setVisibility(View.VISIBLE);
        }
        trust_msg_endorsement_error_en.setVisibility(View.GONE);
        endorsement_header_error_en_tv.setVisibility(View.GONE);
        verify_en.setVisibility(View.GONE);
    }


    private void setEnVerified() {
        endorsement_header_error_en_tv.setVisibility(View.GONE);
        if (!tbData.getIsEndorsementVerified()) {
            tbData.setIsEndorsementVerified(true);
            tbData.setTrustScore(tbData.getTrustScore() + tbData.getScoreEn());
        }
        showTrustBuilderContinueButton();
        showEnSuccess(tbData.getEnSuccessMessage());
        showEndorsers();
        setVerified(header_en, null);
        endorsement_header_text_en_name.setText(tbData.getEndorsers().size() + " Reference" + ((tbData.getEndorsers().size() > 1)?"s":""));
    }

    private void showEndorsers() {
        // force open the content view of endorsements
        content_en.setVisibility(View.VISIBLE);
        //
        endorsers_container.setVisibility(View.VISIBLE);
        endorsers_container_sub.removeAllViews();
        verify_en.setVisibility(View.GONE);
        //
        if (tbData.getIsEndorsementVerified()) {
            trust_msg_endorsement_normal_en.setVisibility(View.GONE);

        } else {
            trust_msg_endorsement_normal_en.setVisibility(View.VISIBLE);
            trust_msg_endorsement_normal_en.setText(tbData.getEnMessage());
        }
        for (EndorserModal endorser : tbData.getEndorsers()) {

            View endorserLayout = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                    .inflate(R.layout.endorser, (ViewGroup) endorsers_container, false);

            Picasso.with(aContext).load(endorser.getPicUrl()).transform(new CircleTransformation()).fit()
                    .into((ImageView) endorserLayout.findViewById(R.id.endorser_image));

            ((TextView) endorserLayout.findViewById(R.id.endorser_name)).setText(endorser.getFname());

            endorsers_container_sub.addView(endorserLayout);
        }
    }

    /*-------------*/
    private void hideLoaders() {
        mProgressDialog = UiUtils.hideProgressBar(mProgressDialog);
        // custom_prog_bar_id.setVisibility(View.GONE);
        // tb_data_container_scrollview.setVisibility(View.VISIBLE);
    }

    private void showLoaders() {

        mProgressDialog = UiUtils.showProgressBar(aContext, mProgressDialog, false);
        if (loadingMessage != null) {
            // loadingMessage = null;
        } else {
            // custom_prog_bar_id.setVisibility(View.VISIBLE);
            // tb_data_container_scrollview.setVisibility(View.GONE);
        }
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(verify_ph.getWindowToken(), 0);
    }

    private void setHeaderPercent(View headerView, int score) {
        TextView percentTextView = (TextView) headerView.findViewWithTag("percent_item");
        percentTextView.setText(score + "%");
    }

    private void setVerified(View headerView, View contentView) {

        if (contentView != null) {
            contentView.setVisibility(View.GONE);
        }
        UiUtils.setBackground(aContext, headerView, R.drawable.rounded_button_blue_disabled_material);

        // Utility.setBackground(aContext,
        // headerView.findViewWithTag("percent_item"),
        // R.drawable.circle_bg_pink_selected, R.color.final_pink_1);

        ((TextView) headerView.findViewWithTag("percent_item"))
                .setTextColor(aContext.getResources().getColor(R.color.white));

        // now change the color of text and icon of the header
        if (headerView == header_fb) {
            fb_icon.setImageResource(R.drawable.facebookwhite);
            ((TextView) header_text_fb_connections).setTextColor(getResources().getColor(R.color.white));
        } else if (headerView == header_li) {
            li_icon.setImageResource(R.drawable.linkedin_white);
            header_text_li_name.setVisibility(View.GONE);
            ((TextView) header_text_li_connections).setTextColor(getResources().getColor(R.color.white));
        } else if (headerView == header_ph) {
            phone_icon.setImageResource(R.drawable.phonenumberwhite);
            header_text_ph_name.setVisibility(View.GONE);
            ((TextView) header_text_ph_connections).setTextColor(getResources().getColor(R.color.white));
        } else if (headerView == header_id) {
            photoid_icon.setImageResource(R.drawable.photoidwhite);
            header_text_id_name.setVisibility(View.GONE);
            ((TextView) header_text_id_connections).setTextColor(getResources().getColor(R.color.white));

        } else if (headerView == header_en) {
            endorsement_icon.setImageResource(R.drawable.endorsement);
            endorsement_header_text_en_name.setTextColor(getResources().getColor(R.color.white));
        }

    }

    private void toggleView(View contentView) {
        if (contentView.getVisibility() == View.VISIBLE) {
            toggleView(contentView, false);
        } else {
            toggleView(contentView, true);
        }
    }

    private void toggleView(View contentView, boolean toShow) {
        hideAllViews();
        hideKeyboard();
        if (toShow) {
            contentView.setVisibility(View.VISIBLE);
        } else {
            contentView.setVisibility(View.GONE);
        }
    }

    private void hideAllViews() {
        content_fb.setVisibility(View.GONE);
        content_li.setVisibility(View.GONE);
        content_ph.setVisibility(View.GONE);
        content_id.setVisibility(View.GONE);
        if (tbData == null || tbData.getEndorsers() == null || tbData.getEndorsers().size() == 0) {
            content_en.setVisibility(View.GONE);
        }
    }

    private void launchDocUploaderSlider(DOC_UPLOAD_TYPE type, boolean showPhotoOption, boolean animate) {
        isPhotoOptionShowing = showPhotoOption;
        boolean isUploading = docUploadRequest != null && !docUploadRequest.isCancelled() && !docUploadRequest.isDone();
        if (showPhotoOption) {
            if (isUploading) {
                switch (type) {
                    case PROFILE_PIC:
                    case PHOTO_ID:
                    case PROFILE_PIC_ENDORSEMENT:
                        AlertsHandler.showMessage(aActivity,
                                R.string.cant_upload_document_yet);
                        break;
                    case ENDORSEMENT:
                        AlertsHandler.showMessage(aActivity,
                                R.string.cant_add_reference_yet);
                        break;
                    default:
                        break;
                }

                launchDocUploaderSlider(type, false, false);
            } else {
                docUploadType = type;
                switch (type) {
                    case PROFILE_PIC:
                    case PHOTO_ID:
                    case PROFILE_PIC_ENDORSEMENT:
                        import_gallery_icon.setVisibility(View.VISIBLE);
                        import_webcam_icon.setVisibility(View.VISIBLE);
                        import_camera_text.setText(R.string.take_from_camera);
                        import_gallery_text.setText(R.string.add_from_device);
                        break;
                    case ENDORSEMENT:
                        import_gallery_icon.setVisibility(View.GONE);
                        import_webcam_icon.setVisibility(View.GONE);
                        import_gallery_text.setText(R.string.facebook);
                        import_camera_text.setText(R.string.other);
                        break;
                    default:
                        break;
                }
                show_photo_slider_layout.setVisibility(View.VISIBLE);
                if (animate) {
                    Animation slideUpAnimation = AnimationUtils.loadAnimation(this, R.anim.slide_up);
                    show_photo_slider_layout.startAnimation(slideUpAnimation);
                }
            }
        } else {
            show_photo_slider_layout.setVisibility(View.GONE);
            if (animate) {
                Animation slideDownAnimation = AnimationUtils.loadAnimation(this, R.anim.slide_down);
                show_photo_slider_layout.startAnimation(slideDownAnimation);
            }
        }
    }

    private void processDocResponse() {
        switch (docUploadType) {
            case PROFILE_PIC:
                showIdSuccess("Great! Photo uploaded successfully.");
                profilePhotoSuccess();
                break;
            case PHOTO_ID:
                String successText = "Cool! Give us 48 hours to review your " + photoIdTypeKey + ".";
                if (!tbData.getIsFbVerified()) {
                    successText += facebookRightAwayText;
                }
                tbData.setIdStatus("under_moderation");
                showIdSuccess(successText);
                break;
            case PROFILE_PIC_ENDORSEMENT:
                endorsement_profile_pic_success_en.setVisibility(View.VISIBLE);
                endorsement_profile_pic_success_en.setText(tbData.getEnProfilePicMessage());
                profilePhotoSuccess();
                break;
            default:
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View v, int pos, long Id) {
        hideKeyboard();
        switch (parent.getId()) {
            case R.id.trust_spinner_id:
                if (pos > 0) {
                    if (parent != null && parent.getChildAt(0) != null) {

                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        UiUtils.setBackground(aContext, parent.getChildAt(0),
                                R.drawable.material_design_edit_register_spinner_icon);
                    }

                    photoIdTypeKey = photoIdTypeSpinner.getSelectedItem().toString();
                    enableSpinnerUploadButton(verify_id, true);

                } else {

                    photoIdTypeKey = null;
                    // this will change the icon and background color
                    enableSpinnerUploadButton(verify_id, false);
                    if (parent != null && parent.getChildAt(0) != null) {
                        TextView item = (TextView) parent.getChildAt(0);
                        if (photoIdTypeSpinner.isEnabled()) {
                            item.setTextColor(getResources().getColor(R.color.dark_gray));
                            UiUtils.setBackground(aContext, item, R.drawable.material_design_edit_register_spinner_icon);
                        } else {
                            item.setTextColor(getResources().getColor(R.color.final_grey_2));
                            UiUtils.setBackground(aContext, item, R.drawable.material_design_edit_register_spinner_icon_id);
                        }
                    }

                }
                break;
            default:
                break;

        }

    }

    private void enableSpinnerUploadButton(View verify_button, boolean toEnable) {
        verify_button.setOnClickListener(null);
        if (toEnable) {
            verify_button.setOnClickListener(this);
            UiUtils.setBackground(aContext, verify_button, R.drawable.button_rect_pink);
            ((TextView) verify_button).setTextColor(aContext.getResources().getColor(R.color.white));
        } else {
            UiUtils.setBackground(aContext, verify_button, R.drawable.rounded_button_gray_material);
            ((TextView) verify_button).setTextColor(aContext.getResources().getColor(R.color.black));
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {

    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))
                || (actionId == EditorInfo.IME_ACTION_NEXT) || (actionId == EditorInfo.IME_ACTION_GO)) {
            hideKeyboard();
        }
        return false;
    }

    @SuppressLint({"Override", "NewApi"})
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (grantResults == null || grantResults.length == 0) {
            return;
        }
        switch (requestCode) {
            case PermissionsHelper.REQUEST_CAMERA_STORAGE_PERMISSIONS_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && (grantResults.length == 1 || grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
                    //TODO: FILE HANDLING: Modify this line to have a better implementation
                    FilesHandler.createImageCacheFolder();
                    photoUploader.onPermissionGranted(requestCode);
                } else {
                    photoUploader.onPermissionRejected();
                }
                break;

            case PermissionsHelper.REQUEST_STORAGE_PERMISSIONS_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //TODO: FILE HANDLING: Modify this line to have a better implementation
                    FilesHandler.createImageCacheFolder();
                    photoUploader.onPermissionGranted(requestCode);
                } else {
                    photoUploader.onPermissionRejected();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    private enum DOC_UPLOAD_TYPE {
        PROFILE_PIC, PHOTO_ID, ENDORSEMENT, PROFILE_PIC_ENDORSEMENT
    }

}
