package com.trulymadly.android.app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.moe.pushlibrary.MoEHelper;
import com.squareup.picasso.Picasso;
import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.activities.MultipleLocationsFragment;
import com.trulymadly.android.app.adapter.MatchesPagesViewPagerAdapter2;
import com.trulymadly.android.app.json.ConstantsRF;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.json.DateSpotParser;
import com.trulymadly.android.app.listener.ConfirmDialogInterface;
import com.trulymadly.android.app.listener.CustomOkHttpResponseHandler;
import com.trulymadly.android.app.listener.OnActionBarClickedInterface;
import com.trulymadly.android.app.listener.TapToRetryListener;
import com.trulymadly.android.app.modal.CuratedDealsChatsModal;
import com.trulymadly.android.app.modal.DateSpotModal;
import com.trulymadly.android.app.modal.ProfileViewPagerModal;
import com.trulymadly.android.app.sqlite.RFHandler;
import com.trulymadly.android.app.utility.ActionBarHandler;
import com.trulymadly.android.app.utility.ActivityHandler;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.CirclePageIndicator;
import com.trulymadly.android.app.utility.FilesHandler;
import com.trulymadly.android.app.utility.OkHttpHandler;
import com.trulymadly.android.app.utility.TapToRetryHandler;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;

import org.apmem.tools.layouts.FlowLayout;
import org.json.JSONObject;

import java.util.HashMap;

import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities.datespots;

/**
 * Created by deveshbatra on 12/23/15.
 */
public class DateSpotProfile extends AppCompatActivity implements OnClickListener {

    private Context aContext;
    private Activity aActivity;
    private MoEHelper mhelper = null;
    private LinearLayout termsContainer, menuContainer, datespot_cardview_container, status_layout, voucher_tutorial_container;
    private ViewPager date_spot_view_pager;
    private MatchesPagesViewPagerAdapter2 mAdapter;
    private DateSpotModal dateSpotModal;
    private boolean isFetched;
    private FlowLayout dateSpotHashtags;
    private LayoutInflater inflater;
    private TextView contact_no_tv, address_tv, offer_description, recommendation_tv, friendly_name_tv, location_tv, status_tv, datelicious_offer_tv, multiple_location_tv;
    private ImageView status_icon;
    private ImageView recommendation_icon;
    private ImageView contact_no_icon;
    private ImageView address_icon;
    private ImageView datelicious_offer_icon;
    private String deal_status, deal_text, dealId, matchId, dateSpotId, messageOneOnOneUrl, existing_phone_no;
    private TapToRetryHandler tapToRetryHandler;
    private ActionBarHandler actionBarHandler;
    private RelativeLayout terms_layout, voucher_tutorial;
    private View phoneNumberView, mainView, location_list_framelayout;
    private EditText phoneNumberEditText;
    private Button save_button;
    private boolean get_number;
    private ProgressDialog mProgressDialog = null;
    private MultipleLocationsFragment localMultipleLocationListFragment;
    private FragmentManager fragmentManager;
    private boolean isMultipleLocationsFragmentVisible;
    private HashMap<String, String> trkEventInfo;

//    private Handler mHandler;
//    private Runnable voucher_tutorial_hide_runnable;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.datespot_layout);
        } catch (OutOfMemoryError e) {
            finish();
            return;
        }


        aContext = this;
        aActivity = this;
        inflater = (LayoutInflater) aActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mhelper = new MoEHelper(this);
        isFetched = false;


        mainView = findViewById(R.id.datespot_main_view);
        date_spot_view_pager = (ViewPager) findViewById(R.id.date_spot_view_pager);
        mAdapter = new MatchesPagesViewPagerAdapter2(aActivity, false, false, true, null);
        dateSpotHashtags = (FlowLayout) findViewById(R.id.date_spot_hashtags);
        termsContainer = (LinearLayout) findViewById(R.id.terms_container);
        voucher_tutorial_container = (LinearLayout) findViewById(R.id.voucher_tutorial_container);
        menuContainer = (LinearLayout) findViewById(R.id.menu_images_container);
        contact_no_tv = (TextView) findViewById(R.id.contact_no_tv);
        address_tv = (TextView) findViewById(R.id.address_tv);
        offer_description = (TextView) findViewById(R.id.offer_description);
        recommendation_tv = (TextView) findViewById(R.id.recommendation_tv);
        friendly_name_tv = (TextView) findViewById(R.id.friendly_name_tv);
        location_tv = (TextView) findViewById(R.id.location_tv);
        dateSpotModal = new DateSpotModal();
        datespot_cardview_container = (LinearLayout) findViewById(R.id.datespot_cardview_container);
        status_tv = (TextView) findViewById(R.id.status_tv);
        multiple_location_tv = (TextView) findViewById(R.id.multiple_locations);
        status_icon = (ImageView) findViewById(R.id.status_icon);
        recommendation_icon = (ImageView) findViewById(R.id.recommendation_icon);
        contact_no_icon = (ImageView) findViewById(R.id.contact_no_icon);
        address_icon = (ImageView) findViewById(R.id.address_icon);
        terms_layout = (RelativeLayout) findViewById(R.id.terms_layout);
        datelicious_offer_tv = (TextView) findViewById(R.id.datelicious_offer_tv);
        datelicious_offer_icon = (ImageView) findViewById(R.id.datelicious_offer_icon);
        voucher_tutorial = (RelativeLayout) findViewById(R.id.voucher_tutorial);
        phoneNumberView = findViewById(R.id.get_phone_number_view);
        phoneNumberEditText = (EditText) phoneNumberView.findViewById(R.id.phone_number_edit_box);
        save_button = (Button) phoneNumberView.findViewById(R.id.save_button);
        status_layout = (LinearLayout) findViewById(R.id.status_layout);
        ImageView terms_icon = (ImageView) findViewById(R.id.terms_icon);
        location_list_framelayout = findViewById(R.id.location_list_framelayout);
        //setting  icons
        Picasso.with(aContext).load(R.drawable.green_coffee_mug).into(datelicious_offer_icon);
        Picasso.with(aContext).load(R.drawable.miss_tm_icon).into(recommendation_icon);
        Picasso.with(aContext).load(R.drawable.call_icon).into(contact_no_icon);
        Picasso.with(aContext).load(R.drawable.location_green).into(address_icon);
        Picasso.with(aContext).load(R.drawable.info).into(terms_icon);
        UiUtils.setBackground(aContext, phoneNumberView, R.drawable.blur_background);


        contact_no_tv.setOnClickListener(this);
        status_layout.setOnClickListener(this);
        voucher_tutorial.setOnClickListener(this);
        contact_no_icon.setOnClickListener(this);
        save_button.setOnClickListener(this);
        multiple_location_tv.setOnClickListener(this);
        phoneNumberEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.length() == 10) {

                    save_button.setEnabled(true);
//                    UiUtils.hideKeyBoard(aActivity);
                } else
                    save_button.setEnabled(false);

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        phoneNumberEditText.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {

                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))
                        || (actionId == EditorInfo.IME_ACTION_NEXT) || (actionId == EditorInfo.IME_ACTION_DONE)) {

                    UiUtils.hideKeyBoard(aActivity);
                    validatePhoneNumber();
                }


                return false;
            }
        });

        OnActionBarClickedInterface actionBarClickedInterface = new OnActionBarClickedInterface() {
            @Override
            public void onConversationsClicked() {
            }


            @Override
            public void onCuratedDealsClicked() {

            }

            @Override
            public void onLocationClicked() {
            }

            @Override
            public void onBackClicked() {
                onBackPressed();

            }

            @Override
            public void onUserProfileClicked() {
            }

            @Override
            public void onTitleClicked() {
            }

            @Override
            public void onTitleLongClicked() {
            }

            @Override
            public void onSearchViewOpened() {

            }

            @Override
            public void onSearchViewClosed() {

            }

            @Override
            public void onCategoriesClicked() {

            }
        };
        actionBarHandler = new ActionBarHandler(this, "", null,
                actionBarClickedInterface, false, false, false);

        actionBarHandler.toggleNotificationCenter(false);
        tapToRetryHandler = new TapToRetryHandler(this, findViewById(android.R.id.content), new TapToRetryListener() {
            @Override
            public void reInitialize() {
                issueRequest();
            }
        }, "Loading ...", false);

        fragmentManager = getSupportFragmentManager();
        getDataFromIntentAndMakeRequest(getIntent());

//        mHandler = new Handler();
    }

    private void getDataFromIntentAndMakeRequest(Intent intent) {
        trkEventInfo = new HashMap<>();
        trkEventInfo.put("nudge_shown", TrulyMadlyApplication.cdChatTutorialShownInThisSession ? "true" : "false");
        Bundle b = intent.getExtras();
        if (b != null) {
            this.messageOneOnOneUrl = b.getString("messageOneOnOneUrl");
            this.matchId = b.getString("matchId");
            trkEventInfo.put("match_id", matchId);
            this.dateSpotId = b.getString("dateSpotId");
            trkEventInfo.put("datespot_id", dateSpotId);
            this.dealId = b.getString("dealId");
            if (Utility.isSet(dealId)) {
                trkEventInfo.put("deal_id", dealId);
            }
            if (b.containsKey("isSpecial")) {
                trkEventInfo.put("isSpecial", b.getBoolean("isSpecial", false) ? "true" : "false");
            }
            if (b.containsKey("isNew")) {
                trkEventInfo.put("isNew", b.getBoolean("isNew", false) ? "true" : "false");
            }
            if (Utility.isSet(b.getString("locationScore"))) {
                trkEventInfo.put("locationScore", b.getString("locationScore"));
            }
            if (Utility.isSet(b.getString("popularityScore"))) {
                trkEventInfo.put("popularityScore", b.getString("popularityScore"));
            }
            if (Utility.isSet(b.getString("totalScore"))) {
                trkEventInfo.put("totalScore", b.getString("totalScore"));
            }
            if (Utility.isSet(b.getString("pricing"))) {
                trkEventInfo.put("pricing", b.getString("pricing"));
            }
            if (Utility.isSet(b.getString("zone_id"))) {
                trkEventInfo.put("zone_id", b.getString("zone_id"));
            }
            if (Utility.isSet(b.getString("geo_location"))) {
                trkEventInfo.put("geo_location", b.getString("geo_location"));
            }
            if (Utility.isSet(b.getString("rank"))) {
                trkEventInfo.put("rank", b.getString("rank"));
            }
        }
        issueRequest();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        getDataFromIntentAndMakeRequest(intent);
    }


    @Override
    public void onBackPressed() {

        if (isMultipleLocationsFragmentVisible) {
            actionBarHandler.setToolbarTransparent(true, getString(R.string.datespots));
            actionBarHandler.toggleNotificationCenter(false);
            mainView.setVisibility(View.VISIBLE);
            location_list_framelayout.setVisibility(View.GONE);
            isMultipleLocationsFragmentVisible = false;
            fragmentManager.beginTransaction().remove(localMultipleLocationListFragment)
                    .commitAllowingStateLoss();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
//        if (mHandler != null) {
//            mHandler.removeCallbacks(voucher_tutorial_hide_runnable);
//        }
        super.onDestroy();
    }

    private void issueRequest() {
        HashMap<String, String> eventInfo = new HashMap<>();
        eventInfo.put("nudge_shown", TrulyMadlyApplication.cdChatTutorialShownInThisSession ? "true" : "false");
        CustomOkHttpResponseHandler okHttpResponseHandler = new CustomOkHttpResponseHandler(aActivity, datespots, TrulyMadlyEvent.TrulyMadlyEventTypes.page_load, eventInfo) {
            @Override
            public void onRequestSuccess(JSONObject responseJSON) {
                tapToRetryHandler.onSuccessFull();

                dateSpotModal = DateSpotParser.parseDateSpotResponse(responseJSON, aContext);


                FilesHandler.createImageCacheFolder();
                mAdapter.setKey("dt_" + dateSpotModal.getId());

                JSONObject deal_details = responseJSON.optJSONObject("deal_details");
                if (deal_details != null) {

                    deal_status = deal_details.optString("deal_status", "ask_him");
                    deal_text = deal_details.optString("deal_text", getResources().getString(R.string.ask_him));
                    dateSpotModal.setDealStatus(deal_status);

                }

                JSONObject phone_details = responseJSON.optJSONObject("phone_details");

                if (phone_details != null) {
                    get_number = phone_details.optBoolean("get_number");
                    existing_phone_no = phone_details.optString("existing_no");


                    if (get_number) {
                        phoneNumberView.setVisibility(View.VISIBLE);
                        if (Utility.isSet(existing_phone_no)) {
                            phoneNumberEditText.setText(existing_phone_no);
                            save_button.setEnabled(true);
                        }
                    } else {
                        showMainLayout();
                    }
                } else {
                    showMainLayout();
                }
                actionBarHandler.setToolbarTransparent(true, getString(R.string.datespots));
            }

            @Override
            public void onRequestFailure(Exception exception) {
                tapToRetryHandler.onNetWorkFailed(exception);

            }
        };

        tapToRetryHandler.showLoader();
        actionBarHandler.setToolbarTransparent(false, getString(R.string.datespots));

        HashMap<String, String> params = new HashMap<>();
        params.put("match_id", matchId);
        params.put("action", "get_deals");
        if (Utility.isSet(dealId))
            params.put("deal_id", dealId);
        else
            params.put("datespot_id", dateSpotId);


        OkHttpHandler.httpGet(aContext, ConstantsUrls.get_date_spot_url(), params,
                okHttpResponseHandler);
    }


    private void showMainLayout() {

        datespot_cardview_container.setVisibility(View.VISIBLE);
        setCardView1();
        setCardView2();
        setCardView3();
        setCardView4();
        setFooter();
        showVoucherTutorial();
    }

    private void showVoucherTutorial() {

        if (!RFHandler.getBool(aContext, ConstantsRF.RIGID_FIELD_VOUCHER_FIRST_TIME) && Utility.stringCompare(deal_status, "voucher")) {
            RFHandler.insert(aContext, ConstantsRF.RIGID_FIELD_VOUCHER_FIRST_TIME, true);

            voucher_tutorial.setVisibility(View.INVISIBLE);

//            if (voucher_tutorial_hide_runnable == null) {
//                voucher_tutorial_hide_runnable = new Runnable() {
//                    @Override
//                    public void run() {
//                        voucher_tutorial.setVisibility(View.VISIBLE);
//                        voucher_tutorial_container.startAnimation(AnimationUtils.loadAnimation(aContext, R.anim.voucher_tutorial_slide_down_animation));
//                    }
//                };
//            }
//            mHandler.postDelayed(voucher_tutorial_hide_runnable, 300);


            Animation voucherAnimation = AnimationUtils.loadAnimation(aContext, R.anim.voucher_tutorial_slide_down_animation);
            voucherAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    voucher_tutorial.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animation animation) {

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            voucherAnimation.setStartOffset(300);
            voucher_tutorial_container.startAnimation(voucherAnimation);

        }

    }

    private void setCardView1() {
        //textviews

        if (Utility.isSet(dateSpotModal.getFriendlyName()) && Utility.isSet(dateSpotModal.getName())) {
            friendly_name_tv.setVisibility(View.VISIBLE);
            //friendly_name_tv.setText(dateSpotModal.getFriendlyName() + " " + getResources().getString(R.string.at) + " " + dateSpotModal.getName());
            friendly_name_tv.setText(dateSpotModal.getName());
        }

        if (Utility.isSet(dateSpotModal.getLocation())) {
            location_tv.setVisibility(View.VISIBLE);
            location_tv.setText(dateSpotModal.getLocation());
        }

        //pager
        OnClickListener firstPicListener = new OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityHandler.startAlbumFullViewPagerForResult(aActivity, Integer.parseInt(view.getTag().toString()),
                        dateSpotModal.getImages(), null,
                        getResources().getString(R.string.photo_gallery), datespots, null);
            }
        };


        // setting height

        int widthPx = (int) UiUtils.getScreenWidth(aActivity);

        ViewGroup.LayoutParams params = date_spot_view_pager.getLayoutParams();
        params.height = widthPx;
        date_spot_view_pager.setLayoutParams(params);

        String urls[] = dateSpotModal.getImages();

        if (urls.length >= 2) {
            Utility.prefetchPhoto(aContext, urls[1], datespots);
        }

        mAdapter.setListener(firstPicListener, firstPicListener);
        mAdapter.changeList(ProfileViewPagerModal.createImagesArrayFromImages(urls), null);
        date_spot_view_pager.setAdapter(mAdapter);

        date_spot_view_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                if (isFetched)
                    return;

                String urls[] = dateSpotModal.getImages();
                if (urls != null && urls.length <= 2)
                    return;

                Utility.prefetchPhotos(aActivity, urls, 2, datespots);
                isFetched = true;

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        CirclePageIndicator indicator = (CirclePageIndicator) findViewById(R.id.image_indicator);
        indicator.setStrokeColor(
                aActivity.getResources().getColor(R.color.default_circle_indicator_stroke_color_alternate));
        indicator
                .setFillColor(aActivity.getResources().getColor(R.color.default_circle_indicator_fill_color_alternate));
        indicator.setViewPager(date_spot_view_pager);

        if (mAdapter.getCount() > 1) {
            indicator.setVisibility(View.VISIBLE);
        } else {
            indicator.setVisibility(View.GONE);
        }

        if (dateSpotModal.getHashtags() != null) {

            String[] hashtags = dateSpotModal.getHashtags();
            dateSpotHashtags.removeAllViews();
            for (String hashtag : hashtags)
                if (Utility.isSet(hashtag)) {
                    View v = inflater.inflate(R.layout.hashtags_or_fav_list_lay, dateSpotHashtags, false);
                    TextView text = (TextView) v.findViewById(R.id.hashtags_or_fav_list_tv);
                    text.setText(hashtag);
                    dateSpotHashtags.addView(v);
                }
        }


    }


    private void setCardView2() {
        // setting TextViews

        if (Utility.isSet(dateSpotModal.getOffer())) {
            datelicious_offer_tv.setVisibility(View.VISIBLE);
            datelicious_offer_icon.setVisibility(View.VISIBLE);
            offer_description.setVisibility(View.VISIBLE);
            offer_description.setText(dateSpotModal.getOffer());
        }

        if (Utility.isSet(dateSpotModal.getRecommendation())) {
            recommendation_icon.setVisibility(View.VISIBLE);
            recommendation_tv.setVisibility(View.VISIBLE);
            recommendation_tv.setText(dateSpotModal.getRecommendation());
        }

        //menu Images
        View menuImage = null;
        ImageView menuImageView = null;

        String[] images = dateSpotModal.getMenuImages();

        menuContainer.removeAllViews();
        OnClickListener menuClickListner = new OnClickListener() {
            @Override
            public void onClick(View view) {
                TrulyMadlyTrackEvent.trackEvent(aActivity, datespots,
                        TrulyMadlyEvent.TrulyMadlyEventTypes.menu_viewed, 0, Utility.isSet(dealId) ? dealId : dateSpotModal.getId(), trkEventInfo, true);

                ActivityHandler.startMenuFullViewPagerForResult(aActivity, Integer.parseInt(view.getTag().toString()), dateSpotModal.getMenuImages(), getResources().getString(R.string.menu));


            }
        };

        if (images.length > 0)
            menuContainer.setVisibility(View.VISIBLE);

        for (int i = 0; i < images.length; i++) {

            menuImage = inflater.inflate(R.layout.menu_image_layout, menuContainer, false);
            menuImageView = (ImageView) menuImage.findViewById(R.id.menu_image_view);
            menuImageView.setTag(i);

            menuImageView.setOnClickListener(menuClickListner);
            Picasso.with(aActivity).load(images[i]).placeholder(R.drawable.gray_rectangle).into(menuImageView);

            menuContainer.addView(menuImage);
        }


    }

    private void setCardView3() {


        if (dateSpotModal.getLocationCount() > 1) {
            multiple_location_tv.setVisibility(View.VISIBLE);
            address_icon.setVisibility(View.VISIBLE);
            address_tv.setVisibility(View.VISIBLE);
            address_tv.setText(dateSpotModal.getLocationCount() + " " + getResources().getString(R.string.locations));

        } else {
            if (Utility.isSet(dateSpotModal.getPhoneNo())) {
                contact_no_icon.setVisibility(View.VISIBLE);
                contact_no_tv.setVisibility(View.VISIBLE);
                contact_no_tv.setText(dateSpotModal.getPhoneNo());
            }
            if (Utility.isSet(dateSpotModal.getPhoneNo())) {
                address_icon.setVisibility(View.VISIBLE);
                address_tv.setVisibility(View.VISIBLE);
                address_tv.setText(dateSpotModal.getAddress());
            }

        }
    }

    private void setCardView4() {
        String[] terms = dateSpotModal.getTerms();

        if (terms != null && terms.length > 0) {
            terms_layout.setVisibility(View.VISIBLE);
            View term;
            TextView termText;
            termsContainer.removeAllViews();
            for (String term1 : terms) {

                term = inflater.inflate(R.layout.terms_item_layout, null);
                termText = (TextView) term.findViewById(R.id.term_text_view);
                termText.setText(term1);
                termsContainer.addView(term);
            }
        }
    }

    private void setFooter() {
        status_tv.setText(deal_text);

        if (!Utility.isSet(deal_status) || deal_status.equalsIgnoreCase("ask_him") || deal_status.equalsIgnoreCase("ask_her")) {
            status_icon.setVisibility(View.VISIBLE);
            UiUtils.setBackground(aContext, status_layout, R.color.colorSecondary);
            Picasso.with(aContext).load(R.drawable.ask_her_icon).into(status_icon);
            TrulyMadlyTrackEvent.trackEvent(aActivity, datespots,
                    TrulyMadlyEvent.TrulyMadlyEventTypes.suggest_viewed, 0, dateSpotModal.getId(), trkEventInfo, true);
        } else if (deal_status.equalsIgnoreCase("awaiting_response")) {
            status_icon.setVisibility(View.VISIBLE);
            TrulyMadlyTrackEvent.trackEvent(aActivity, datespots,
                    TrulyMadlyEvent.TrulyMadlyEventTypes.awaiting_response, 0, dealId, trkEventInfo, true);
            UiUtils.setBackground(aContext, status_layout, R.color.waiting_response_gray);
            Picasso.with(aContext).load(R.drawable.awaiting_her_response).into(status_icon);
        } else if (deal_status.equalsIgnoreCase("lets_go")) {
            status_icon.setVisibility(View.GONE);
            TrulyMadlyTrackEvent.trackEvent(aActivity, datespots,
                    TrulyMadlyEvent.TrulyMadlyEventTypes.lets_go_viewed, 0, dealId, trkEventInfo, true);
            UiUtils.setBackground(aContext, status_layout, R.color.colorSecondary);
        } else if (deal_status.equalsIgnoreCase("voucher")) {
            status_icon.setVisibility(View.GONE);
            TrulyMadlyTrackEvent.trackEvent(aActivity, datespots,
                    TrulyMadlyEvent.TrulyMadlyEventTypes.voucher_code_viewed, 0, dateSpotModal.getId(), trkEventInfo, true);
            UiUtils.setBackground(aContext, status_layout, R.color.voucher_green);
        }
        status_layout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.status_layout:
                final CuratedDealsChatsModal curatedDealsChatsModal = new CuratedDealsChatsModal(
                        MessageModal.MessageType.CD_VOUCHER, dealId, dateSpotId,
                        dateSpotModal.getCommunicationImage(),
                        null, dateSpotModal.getLocation());
                curatedDealsChatsModal.setmMessage(dateSpotModal.getCommunicationText());
                String message;
                if (!Utility.isSet(deal_status) || deal_status.equalsIgnoreCase("ask_him")) {
                    TrulyMadlyTrackEvent.trackEvent(aActivity, datespots,
                            TrulyMadlyEvent.TrulyMadlyEventTypes.suggest_clicked, 0, dateSpotModal.getId(), trkEventInfo, true);
                    message = getResources().getString(R.string.ask_message_male) + " " + dateSpotModal.getName() + "?";
                    curatedDealsChatsModal.setmMessageType(MessageModal.MessageType.CD_ASK);
                    showConfirmationDialogBox(message, curatedDealsChatsModal, true, R.string.ask_him);
                } else if (deal_status.equalsIgnoreCase("ask_her")) {
                    TrulyMadlyTrackEvent.trackEvent(aActivity, datespots,
                            TrulyMadlyEvent.TrulyMadlyEventTypes.suggest_clicked, 0, dateSpotModal.getId(), trkEventInfo, true);
                    message = getResources().getString(R.string.ask_message_female) + " " + dateSpotModal.getName() + "?";
                    curatedDealsChatsModal.setmMessageType(MessageModal.MessageType.CD_ASK);
                    showConfirmationDialogBox(message, curatedDealsChatsModal, true, R.string.ask_her);
                } else if (deal_status.equalsIgnoreCase("lets_go")) {
                    TrulyMadlyTrackEvent.trackEvent(aActivity, datespots,
                            TrulyMadlyEvent.TrulyMadlyEventTypes.lets_go_clicked, 0, dealId, trkEventInfo, true);
                    message = getResources().getString(R.string.lets_go_message);
                    curatedDealsChatsModal.setmMessage(dateSpotModal.getCommunicationText());
                    curatedDealsChatsModal.setmMessageType(MessageModal.MessageType.CD_VOUCHER);
                    showConfirmationDialogBox(message, curatedDealsChatsModal, false, R.string.get_voucher);
                }
                break;
            case R.id.voucher_tutorial:
                voucher_tutorial.setVisibility(View.GONE);
                break;
            case R.id.save_button:
                validatePhoneNumber();
                break;

            case R.id.contact_no_tv:
            case R.id.contact_no_icon:
                TrulyMadlyTrackEvent.trackEvent(aActivity, datespots,
                        TrulyMadlyEvent.TrulyMadlyEventTypes.phone_called, 0, Utility.isSet(dealId) ? dealId : dateSpotModal.getId(), trkEventInfo, true);
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + dateSpotModal.getPhoneNo()));
                startActivity(intent);
                break;
            case R.id.multiple_locations:

                launchLocationListFragment();
                break;

        }
    }

    private void sendPhoneNumberToServer() {

        mProgressDialog = UiUtils.showProgressBar(aActivity, mProgressDialog);
        HashMap<String, String> eventInfo = new HashMap<>();
        eventInfo.put("nudge_shown", TrulyMadlyApplication.cdChatTutorialShownInThisSession ? "true" : "false");
        CustomOkHttpResponseHandler okHttpResponseHandler = new CustomOkHttpResponseHandler(aActivity, datespots, TrulyMadlyEvent.TrulyMadlyEventTypes.save_number, dateSpotModal.getId(), eventInfo) {
            @Override
            public void onRequestSuccess(JSONObject responseJSON) {
                mProgressDialog = UiUtils.hideProgressBar(mProgressDialog);
                RFHandler.insert(aContext, Utility.getMyId(aContext),
                        ConstantsRF.RIGID_FIELD_CD_PHONE_NUMBER_TAKEN, true);
                phoneNumberView.setVisibility(View.GONE);
                showMainLayout();
            }

            @Override
            public void onRequestFailure(Exception exception) {
                mProgressDialog = UiUtils.hideProgressBar(mProgressDialog);
                AlertsHandler.showNetworkError(aActivity, exception);
            }
        };

        HashMap<String, String> params = new HashMap<>();
        params.put("action", "save_number");
        params.put("phone_number", phoneNumberEditText.getText().toString());

        if (Utility.isSet(dealId))
            params.put("deal_id", dealId);


        OkHttpHandler.httpGet(aContext, ConstantsUrls.get_datespotListUrl(), params,
                okHttpResponseHandler);

    }

    private void launchLocationListFragment() {
        try {

            actionBarHandler.toggleNotificationCenter(false);
            mainView.setVisibility(View.GONE);
            location_list_framelayout.setVisibility(View.VISIBLE);
            localMultipleLocationListFragment = new MultipleLocationsFragment();
            Bundle data = new Bundle();
            data.putParcelableArrayList("locations",
                    dateSpotModal.getLocations());
            data.putString("dealId", Utility.isSet(dealId) ? dealId : dateSpotModal.getId());
            localMultipleLocationListFragment.setArguments(data);
            localMultipleLocationListFragment.setTrkActivity(datespots);
            localMultipleLocationListFragment.setTrkEventInfo(trkEventInfo);
            actionBarHandler.setToolbarTransparent(false, dateSpotModal.getName());


            fragmentManager.beginTransaction()
                    .replace(R.id.location_list_framelayout,
                            localMultipleLocationListFragment,
                            MultipleLocationsFragment.class.getSimpleName())
                    .commitAllowingStateLoss();

            isMultipleLocationsFragmentVisible = true;

        } catch (IllegalStateException ignored) {
        }
    }

    private void validatePhoneNumber() {
        UiUtils.hideKeyBoard(aActivity);
        if (Utility.isValidPhoneNo(phoneNumberEditText.getText().toString())) {
            String message = getResources().getString(R.string.phone_number_confirmation);
            message += " " + phoneNumberEditText.getText().toString() + "?";
            AlertsHandler.showConfirmDialog(
                    aActivity, message,
                    R.string.yes, R.string.oops_try_again, new ConfirmDialogInterface() {

                        @Override
                        public void onPositiveButtonSelected() {
                            sendPhoneNumberToServer();
                        }

                        @Override
                        public void onNegativeButtonSelected() {
                        }
                    }, true);
        } else {
            AlertsHandler.showMessage(aActivity, R.string.enter_phone_error);
        }
    }


    private void showConfirmationDialogBox(String message, final CuratedDealsChatsModal curatedDealsChatsModal, final boolean AskOut, int positiveMessageId) {
        AlertsHandler.showConfirmDialog(
                aActivity, message,
                positiveMessageId, R.string.may_be_later, new ConfirmDialogInterface() {

                    @Override
                    public void onPositiveButtonSelected() {

                        String event_label = Utility.isSet(curatedDealsChatsModal.getmDealId()) ? curatedDealsChatsModal.getmDealId() : dateSpotModal.getId();

                        if (AskOut) {
                            TrulyMadlyTrackEvent.trackEvent(aActivity, datespots,
                                    TrulyMadlyEvent.TrulyMadlyEventTypes.suggest_confirmed, 0, event_label, trkEventInfo, true);
                        } else {
                            TrulyMadlyTrackEvent.trackEvent(aActivity, datespots,
                                    TrulyMadlyEvent.TrulyMadlyEventTypes.lets_go_confirmed, 0, event_label, trkEventInfo, true);
                        }

                        ActivityHandler.startMessageOneOnOneActivity(aContext, matchId,
                                messageOneOnOneUrl, curatedDealsChatsModal);
                        finish();
                    }

                    @Override
                    public void onNegativeButtonSelected() {
                    }
                }, false);
    }
}
