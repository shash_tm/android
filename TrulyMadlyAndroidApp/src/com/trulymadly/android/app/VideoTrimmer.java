package com.trulymadly.android.app;

import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.google.common.base.Preconditions;
import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.listener.ConfirmDialogInterface;
import com.trulymadly.android.app.listener.OnProgressVideoListener;
import com.trulymadly.android.app.listener.OnRangeSeekBarListener;
import com.trulymadly.android.app.listener.OnTrimVideoListener;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.FilesHandler;
import com.trulymadly.android.app.utility.TmLogger;
import com.trulymadly.android.app.utility.TrimVideoUtils;
import com.trulymadly.android.app.utility.Utility;
import com.trulymadly.android.app.utility.VideoUtils;
import com.trulymadly.android.app.views.CustomVideoView;
import com.trulymadly.android.app.views.ProgressBarView;
import com.trulymadly.android.app.views.TimeLineView;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities.photo;
import static com.trulymadly.android.app.utility.VideoUtils.VIDEO_DURATION_MAX_SECS_DEFAULT;
import static com.trulymadly.android.app.utility.VideoUtils.VIDEO_DURATION_MIN_SECS_DEFAULT;


/**
 * Created by deveshbatra on 7/21/16.
 */
public class VideoTrimmer extends FrameLayout {
    private static final String TAG = VideoTrimmer.class.getSimpleName();
    private static final int MIN_TIME_FRAME = 1000;
    private static final int SHOW_PROGRESS = 2;
    private final MessageHandler mMessageHandler = new MessageHandler(this);
    @BindView(R.id.handlerTop)
    SeekBar mHolderTopView;
    @BindView(R.id.timeLineBar)
    RangeSeekBarView mRangeSeekBarView;
    @BindView(R.id.layout_surface_view)
    RelativeLayout mLinearVideo;
    @BindView(R.id.timeText)
    View mTimeInfoContainer;
    @BindView(R.id.video_loader)
    CustomVideoView mVideoView;
    @BindView(R.id.icon_play_button)
    ImageView mPlayView;
    @BindView(R.id.textSize)
    TextView mTextSize;
    @BindView(R.id.textTimeSelection)
    TextView mTextTimeFrame;
    @BindView(R.id.textTime)
    TextView mTextTime;
    @BindView(R.id.timeLineView)
    TimeLineView mTimeLineView;
    @BindView(R.id.timeVideoView)
    ProgressBarView mVideoProgressIndicator;
    private Uri mSrc;
    private String mFinalPath;
    private List<OnProgressVideoListener> mListeners;
    private OnTrimVideoListener mOnTrimVideoListener;
    private int mDuration = 0;
    private int mTimeVideo = 0;
    private int mStartPosition = 0;
    private int mEndPosition = 0;
    private long mOriginSizeFile;
    private boolean mResetSeekBar = true;
    private ImageView icon_play_video;
    private boolean flag;
    private AudioManager mAudioManager;
    private Context aContext;
    private MediaPlayer mediaPlayer;
    private boolean isMuted;
    private Unbinder unbinder;
    private int screenWidth;
    private String videoSource, camera;
    private WeakReference<Activity> mWeakActivity;


    public VideoTrimmer(@NonNull Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public VideoTrimmer(@NonNull Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public static String stringForTime(int timeMs) {
        int totalSeconds = timeMs / 1000;

        int seconds = totalSeconds % 60;
        int minutes = (totalSeconds / 60) % 60;
        int hours = totalSeconds / 3600;

        Formatter mFormatter = new Formatter();
        if (hours > 0) {
            return mFormatter.format("%d:%02d:%02d", hours, minutes, seconds).toString();
        } else {
            return mFormatter.format("%02d:%02d", minutes, seconds).toString();
        }
    }

    public void setActivity(Activity activity) {
        if (activity == null) {
            if (mWeakActivity.get() != null) {
                mWeakActivity.clear();
            }
            mWeakActivity = null;
            return;
        }
        mWeakActivity = new WeakReference<>(activity);
    }

    private void init(Context context) {
        this.aContext = context;
        LayoutInflater.from(context).inflate(R.layout.view_time_line, this, true);

        unbinder = ButterKnife.bind(this, this);


        screenWidth = context.getResources().getDisplayMetrics().widthPixels;


        ViewGroup.LayoutParams params = mLinearVideo.getLayoutParams();
        params.height = screenWidth;
        mLinearVideo.setLayoutParams(params);

        mVideoView.setDimensions(screenWidth, screenWidth);


        MediaController mc = new MediaController(aContext);
        mc.setMediaPlayer(mVideoView);
        mediaPlayer = new MediaPlayer();


        setUpListeners();
        setUpMargins();

    }

    @OnCheckedChanged(R.id.sound_toggle_button)
    void muteVideo(boolean toMute) {
        isMuted = toMute;
        if (isMuted) {
            mediaPlayer.setVolume(0.0f, 0.0f);
            if (mWeakActivity != null) {
                Activity activity = mWeakActivity.get();
                if (activity != null) {
                    AlertsHandler.showMessage(activity, R.string.video_no_sound);
                }
            }
        } else {
            mediaPlayer.setVolume(1.0f, 1.0f);
        }
    }

    private void setUpListeners() {
        mListeners = new ArrayList<>();
        mListeners.add(new OnProgressVideoListener() {
            @Override
            public void updateProgress(int time, int max, float scale) {
                updateVideoProgress(time);
            }
        });
        mListeners.add(mVideoProgressIndicator);

        GestureDetector gestureDetector = new
                GestureDetector(getContext(),
                new GestureDetector.SimpleOnGestureListener() {
                    @Override
                    public boolean onSingleTapConfirmed(MotionEvent e) {
                        onClickVideoPlayPause();
                        return true;
                    }
                }
        );


        mRangeSeekBarView.addOnRangeSeekBarListener(new OnRangeSeekBarListener() {
            @Override
            public void onCreate(RangeSeekBarView rangeSeekBarView, int index, float value) {
                // Do nothing
            }

            @Override
            public void onSeek(RangeSeekBarView rangeSeekBarView, int index, float value) {
                onSeekThumbs(index, value);
            }

            @Override
            public void onSeekStart(RangeSeekBarView rangeSeekBarView, int index, float value) {
                // Do nothing
            }

            @Override
            public void onSeekStop(RangeSeekBarView rangeSeekBarView, int index, float value) {
                onStopSeekThumbs();
            }
        });
        mRangeSeekBarView.addOnRangeSeekBarListener(mVideoProgressIndicator);

        mHolderTopView.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                onPlayerIndicatorSeekChanged(progress, fromUser);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                onPlayerIndicatorSeekStart();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                onPlayerIndicatorSeekStop(seekBar);
            }
        });

        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                if (mVideoView != null) {
                    onVideoPrepared(mp);
                }
            }
        });

        mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                if (mVideoView != null) {
                    mediaPlayer = mp;
                    onVideoCompleted();
                }
            }
        });

    }

    private void setUpMargins() {
        int marge = mRangeSeekBarView.getThumbs().get(0).getWidthBitmap();
        int widthSeek = mHolderTopView.getThumb().getMinimumWidth() / 2;

        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mHolderTopView.getLayoutParams();
        lp.setMargins(marge - widthSeek, 0, marge - widthSeek, 0);
        mHolderTopView.setLayoutParams(lp);

        lp = (RelativeLayout.LayoutParams) mTimeLineView.getLayoutParams();
        lp.setMargins(marge, 0, marge, 0);
        mTimeLineView.setLayoutParams(lp);

        lp = (RelativeLayout.LayoutParams) mVideoProgressIndicator.getLayoutParams();
        lp.setMargins(marge, 0, marge, 0);
        mVideoProgressIndicator.setLayoutParams(lp);
    }

    @OnClick(R.id.save_tv)
    void onSaveClicked() {
        if (!Utility.isNetworkAvailable(aContext)) {
            AlertsHandler.showMessage((Activity) getContext(), R.string.whoops_no_internet, false);
            return;
        }

        if (isMuted()) {
            AlertsHandler.showConfirmDialog(aContext, R.string.video_is_mute, R.string.continue_string, R.string.cancel,
                    new ConfirmDialogInterface() {
                        @Override
                        public void onPositiveButtonSelected() {
                            finishVideoTrimmer();
                        }

                        @Override
                        public void onNegativeButtonSelected() {
                        }
                    });
        } else {
            finishVideoTrimmer();
        }

    }

    private void finishVideoTrimmer() {

        HashMap<String, String> eventInfo = new HashMap<>();
        eventInfo.put("original_video_length_ms", String.valueOf(mDuration));
        eventInfo.put("muted", String.valueOf(isMuted));
        eventInfo.put("source", videoSource);
        if (Utility.isSet(camera) && Utility.stringCompare(videoSource, VideoUtils.VIDEO_SOURCE_CAMERA)) {
            eventInfo.put("camera", camera);
        }
        if (mStartPosition <= 0 && mEndPosition >= mDuration) {
            eventInfo.put("final_video_length_ms", String.valueOf(mDuration));
            eventInfo.put("start_time", "0");
            eventInfo.put("end_time", String.valueOf(mDuration));
        } else {
            eventInfo.put("final_video_length_ms", String.valueOf(mEndPosition - mStartPosition));
            eventInfo.put("start_time", String.valueOf(mStartPosition));
            eventInfo.put("end_time", String.valueOf(mEndPosition));
        }

        TrulyMadlyTrackEvent.trackEvent(aContext, photo, TrulyMadlyEvent.TrulyMadlyEventTypes.video_save, 0,
                null, eventInfo, true);
        if (mStartPosition <= 0 && mEndPosition >= mDuration) {
            mOnTrimVideoListener.getResult(mSrc);
        } else {
            togglePlayButton(false);

            mVideoView.pause();

            MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
            mediaMetadataRetriever.setDataSource(getContext(), mSrc);
            long METADATA_KEY_DURATION = Long.parseLong(mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION));
            mediaMetadataRetriever.release();
            final File file = new File(mSrc.getPath());

            if (mTimeVideo < MIN_TIME_FRAME) {

                if ((METADATA_KEY_DURATION - mEndPosition) > (MIN_TIME_FRAME - mTimeVideo)) {
                    mEndPosition += (MIN_TIME_FRAME - mTimeVideo);
                } else if (mStartPosition > (MIN_TIME_FRAME - mTimeVideo)) {
                    mStartPosition -= (MIN_TIME_FRAME - mTimeVideo);
                }
            }

            BackgroundExecutor.execute(
                    new BackgroundExecutor.Task("", 0L, "") {
                        @Override
                        public void execute() {
                            try {
                                TrimVideoUtils.startTrim(file, getDestinationPath(), mStartPosition, mEndPosition, mOnTrimVideoListener);
                            } catch (Throwable e) {
                                e.printStackTrace();
                                Crashlytics.logException(e);
                                mOnTrimVideoListener.errorOnTrim();
                            }
                        }
                    }
            );
        }
    }

    @OnClick(R.id.icon_play_button)
    void onClickVideoPlayPause() {
        if (mVideoView.isPlaying()) {
            togglePlayButton(false);
            mMessageHandler.removeMessages(SHOW_PROGRESS);
            mVideoView.pause();
        } else {
            togglePlayButton(true);
            if (mResetSeekBar) {
                mResetSeekBar = false;
                togglePlayButton(false);
                mVideoView.seekTo(mStartPosition);

            }

            mMessageHandler.sendEmptyMessage(SHOW_PROGRESS);
            mVideoView.start();
        }
    }

    @OnClick(R.id.cancel_tv)
    void onCancelClicked() {
        mOnTrimVideoListener.cancelAction();
    }

    private String getDestinationPath() {
        if (mFinalPath == null) {
            File folder = new File(FilesHandler.getVideoDirectoryPath(true));
//            File folder = Environment.getExternalStorageDirectory();
            mFinalPath = folder.getPath() + File.separator;
            Log.d(TAG, "Using default path " + mFinalPath);
        }
        return mFinalPath;
    }

    /**
     * Sets the path where the trimmed video will be saved
     * Ex: /storage/emulated/0/MyAppFolder/
     *
     * @param finalPath the full path
     */
    @SuppressWarnings("unused")
    public void setDestinationPath(final String finalPath) {
        mFinalPath = finalPath;
        Log.d(TAG, "Setting custom path " + mFinalPath);
    }

    private void onPlayerIndicatorSeekChanged(int progress, boolean fromUser) {

        int duration = (int) ((mDuration * progress) / 1000L);

        if (fromUser) {
            if (duration < mStartPosition) {
                setProgressBarPosition(mStartPosition);
                duration = mStartPosition;
            } else if (duration > mEndPosition) {
                setProgressBarPosition(mEndPosition);
                duration = mEndPosition;
            }
            setTimeVideo(duration);
        }
    }

    private void onPlayerIndicatorSeekStart() {
        mMessageHandler.removeMessages(SHOW_PROGRESS);
        mVideoView.pause();
        togglePlayButton(false);
        notifyProgressUpdate(false);
    }

    private void onPlayerIndicatorSeekStop(@NonNull SeekBar seekBar) {
        mMessageHandler.removeMessages(SHOW_PROGRESS);
        mVideoView.pause();
        togglePlayButton(false);
        int duration = (int) ((mDuration * seekBar.getProgress()) / 1000L);
        mVideoView.seekTo(duration);
        setTimeVideo(duration);
        notifyProgressUpdate(false);
    }

    private void onVideoPrepared(@NonNull MediaPlayer mp) {
        // note time

        // Adjust the size of the video
        // so it fits on the screen
        mediaPlayer = mp;
        int videoWidth = mp.getVideoWidth();
        int videoHeight = mp.getVideoHeight();
        float videoProportion = (float) videoWidth / (float) videoHeight;
//        float screenProportion = (float) screenWidth / (float) screenHeight;
        ViewGroup.LayoutParams lp = mVideoView.getLayoutParams();

        int computedVideoWidth = (int) (screenWidth * 0.99);
        int computedVideoHeight = (int) ((float) computedVideoWidth / videoProportion);
        lp.width = computedVideoWidth;
        lp.height = computedVideoHeight;
//        if (videoProportion > screenProportion) {
//            lp.width = screenWidth;
//            lp.height = (int) ((float) screenWidth / videoProportion);
//        } else {
//            lp.width = (int) (videoProportion * (float) screenHeight);
//            lp.height = screenHeight;
//        }

        mVideoView.setDimensions(computedVideoWidth, computedVideoHeight);
        mVideoView.setLayoutParams(lp);
        mVideoView.getHolder().setFixedSize(computedVideoWidth, computedVideoHeight);

        int translateY = (computedVideoHeight - computedVideoWidth) / 2;
        TmLogger.d(TAG, "computedVideoHeight:" + computedVideoHeight + ",computedVideoWidth:" + computedVideoWidth + ",translateY:" + translateY + "");
        mVideoView.setTranslationY(-1 * translateY);

        ViewGroup.LayoutParams params = mLinearVideo.getLayoutParams();
        params.height = computedVideoWidth;
        mLinearVideo.setLayoutParams(params);

        mVideoView.requestLayout();

        mPlayView.setVisibility(View.VISIBLE);


        mDuration = mVideoView.getDuration();
        if (mDuration >= VIDEO_DURATION_MAX_SECS_DEFAULT * 1000)
            mRangeSeekBarView.setRatio((float) VIDEO_DURATION_MAX_SECS_DEFAULT / (float) VIDEO_DURATION_MIN_SECS_DEFAULT);
        else if (mDuration >= VIDEO_DURATION_MIN_SECS_DEFAULT * 1000)
            mRangeSeekBarView.setRatio((float) mDuration / (float) (VIDEO_DURATION_MIN_SECS_DEFAULT * 1000));
        else
            mRangeSeekBarView.setRatio(1);


        setSeekBarPosition();

        setTimeFrames();
        setTimeVideo(0);
        if (!flag) {
            flag = true;
            onClickVideoPlayPause();
            onClickVideoPlayPause();
        }
    }

    private void setSeekBarPosition() {

        if (mDuration >= VIDEO_DURATION_MAX_SECS_DEFAULT * 1000) {
            mStartPosition = 0;
            mEndPosition = VIDEO_DURATION_MAX_SECS_DEFAULT * 1000;

            mRangeSeekBarView.setThumbValue(0, (mStartPosition * 100) / mDuration);
            mRangeSeekBarView.setThumbValue(1, (mEndPosition * 100) / mDuration);

        } else {
            mStartPosition = 0;
            mEndPosition = mDuration;
        }

        setProgressBarPosition(mStartPosition);
        mVideoView.seekTo(mStartPosition);

        mTimeVideo = mDuration;
        mRangeSeekBarView.initMaxWidth();
    }

    private void setTimeFrames() {
        String seconds = getContext().getString(R.string.short_seconds);
//        mTextTimeFrame.setText(String.format("%s %s - %s %s", stringForTime(mStartPosition), seconds, stringForTime(mEndPosition), seconds));
        int timeInSeconds = (int) Math.ceil(((double) (mEndPosition - mStartPosition)) / 1000.00);
        if (timeInSeconds < VIDEO_DURATION_MIN_SECS_DEFAULT) {
            timeInSeconds = VIDEO_DURATION_MIN_SECS_DEFAULT;
        }
        if (timeInSeconds > VIDEO_DURATION_MAX_SECS_DEFAULT) {
            timeInSeconds = VIDEO_DURATION_MAX_SECS_DEFAULT;
        }

        mTextTimeFrame.setText(timeInSeconds + "s");
    }

    private void setTimeVideo(int position) {
        String seconds = getContext().getString(R.string.short_seconds);
        mTextTime.setText(String.format("%s %s", stringForTime(position), seconds));
    }

    public void setVideoSource(String videoSource) {
        this.videoSource = videoSource;
    }

    public void setCamera(String camera) {
        this.camera = camera;
    }

    private void onSeekThumbs(int index, float value) {
        switch (index) {
            case Thumb.LEFT: {
                mStartPosition = (int) ((mDuration * value) / 100L);
                mVideoView.seekTo(mStartPosition);
                break;
            }
            case Thumb.RIGHT: {
                mEndPosition = (int) ((mDuration * value) / 100L);
                break;
            }
        }
        setProgressBarPosition(mStartPosition);

        setTimeFrames();
        mTimeVideo = mEndPosition - mStartPosition;
    }

    private void onStopSeekThumbs() {
        mMessageHandler.removeMessages(SHOW_PROGRESS);
        mVideoView.pause();
        togglePlayButton(false);

    }

    private void onVideoCompleted() {
        mVideoView.seekTo(mStartPosition);
        togglePlayButton(false);
    }

    private void togglePlayButton(boolean isPlaying) {
        mPlayView.setImageDrawable(getResources().getDrawable(
                isPlaying ? R.drawable.ic_pause_circle_filled_black : R.drawable.ic_play_circle_filled_black));
    }

    private void notifyProgressUpdate(boolean all) {
        if (mDuration == 0) return;

        int position = mVideoView.getCurrentPosition();
        if (all) {
            for (OnProgressVideoListener item : mListeners) {
                Preconditions.checkNotNull(item).updateProgress(position, mDuration, ((position * 100) / mDuration));
            }
        } else {
            mListeners.get(1).updateProgress(position, mDuration, ((position * 100) / mDuration));
        }
    }

    private void updateVideoProgress(int time) {
        if (mVideoView == null) {
            return;
        }

        if (time >= mEndPosition) {
            mMessageHandler.removeMessages(SHOW_PROGRESS);
            mVideoView.pause();
            togglePlayButton(false);
            mResetSeekBar = true;
            return;
        }

        if (mHolderTopView != null) {
            // use long to avoid overflow
            setProgressBarPosition(time);
        }
        setTimeVideo(time);
    }

    private void setProgressBarPosition(int position) {
        if (mDuration > 0) {
            long pos = 1000L * position / mDuration;
            mHolderTopView.setProgress((int) pos);
        }
    }

    /**
     * Set video information visibility.
     * For now this is for debugging
     *
     * @param visible whether or not the videoInformation will be visible
     */
    public void setVideoInformationVisibility(boolean visible) {
        mTimeInfoContainer.setVisibility(visible ? VISIBLE : GONE);
    }

    /**
     * Listener for events such as trimming operation success and cancel
     *
     * @param onTrimVideoListener interface for events
     */
    @SuppressWarnings("unused")
    public void setOnTrimVideoListener(OnTrimVideoListener onTrimVideoListener) {
        mOnTrimVideoListener = onTrimVideoListener;
    }

    /**
     * Cancel all current operations
     */
    public void destroy() {
        TmLogger.d(TAG, "destroying");
        BackgroundExecutor.cancelAll("", true);
        UiThreadExecutor.cancelAll("");
        if (unbinder != null) {
            mVideoView.setOnPreparedListener(null);
            mVideoView.setOnCompletionListener(null);
            unbinder.unbind();
        }
    }

    /**
     * Sets the uri of the video to be trimmer
     *
     * @param videoURI Uri of the video
     */
    @SuppressWarnings("unused")
    public void setVideoURI(final Uri videoURI) {
        mSrc = videoURI;

        if (mOriginSizeFile == 0) {
            File file = new File(mSrc.getPath());

            mOriginSizeFile = file.length();
            long fileSizeInKB = mOriginSizeFile / 1024;

            if (fileSizeInKB > 1000) {
                long fileSizeInMB = fileSizeInKB / 1024;
                mTextSize.setText(String.format("%s %s", fileSizeInMB, getContext().getString(R.string.megabyte)));
            } else {
                mTextSize.setText(String.format("%s %s", fileSizeInKB, getContext().getString(R.string.kilobyte)));
            }
        }

        mVideoView.setVideoURI(mSrc);
        mVideoView.requestFocus();

        mTimeLineView.setVideo(mSrc);
    }

    public boolean isMuted() {
        return isMuted;
    }

    private static class MessageHandler extends Handler {

        @NonNull
        private final WeakReference<VideoTrimmer> mView;

        MessageHandler(VideoTrimmer view) {
            mView = new WeakReference<>(view);
        }

        @Override
        public void handleMessage(Message msg) {
            VideoTrimmer view = mView.get();
            if (view == null || view.mVideoView == null) {
                return;
            }

            view.notifyProgressUpdate(true);
            if (view.mVideoView.isPlaying()) {
                sendEmptyMessageDelayed(0, 10);
            }
        }
    }
}
