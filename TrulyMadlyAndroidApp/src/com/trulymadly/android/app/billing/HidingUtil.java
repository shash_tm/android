package com.trulymadly.android.app.billing;


import android.util.Base64;
import android.util.Log;

/**
 * A simple utility to manage the XOR hiding routines.
 * <p/>
 * Created by Michael Ramirez on 7/27/15. Copyright 2015, Apothesource, Inc. All Rights Reserved.
 * <p/>
 * Based on
 * https://github.com/pillfill/hiding-passwords-android
 * https://rammic.github.io/2015/07/28/hiding-secrets-in-android-apps/
 */
public class HidingUtil {
    private static final String TAG = "SparksBillingHandler";

    /**
     * 'Hide' a message using a password using XOR operations.
     * <p/>
     * Note: Please do not depend on XOR for real encryption. We're using it here as a very
     * simple hiding scheme.
     *
     * @param msg The message that we want to hide/unhide- The XOR is done-in place.
     * @param pwd The password to use as our OTP
     * @return the number of bytes that were processed
     */
    private static int xorValues(byte[] msg, byte[] pwd) {
        int i;
        for (i = 0; i < msg.length; i++) {
            int keyOffset = i % pwd.length;
            msg[i] = (byte) (msg[i] ^ pwd[keyOffset]);
        }
        return i;
    }

    /**
     * A reusable method to make our simple XOR hiding method. Since the interesting part is
     * how we get the hiding key, we've moved everything else into this reusable method.
     *
     * @param msg      The message to hide/unhide
     * @param pwd      Our password key to use in the XOR process
     * @param isHidden whether we're encrypting or unencrypting (relevant only for logging)
     */
    private static void doHiding(byte[] msg, byte[] pwd, boolean isHidden) {
        xorValues(msg, pwd);

        if (!isHidden) {
            String hiddenMessage = Base64.encodeToString(msg, 0);
            Log.i(TAG, String.format("Hidden Message: %s", hiddenMessage));
            doHiding(msg, pwd, true);
        } else {
            Log.i(TAG, String.format("Unhidden Message: %s", new String(msg)));
        }
    }

    private static byte[] getXorKeyFromParts(String[] compositeKey) {
        byte[] xorParts0 = Base64.decode(compositeKey[0], 0);
        byte[] xorParts1 = Base64.decode(compositeKey[1], 0);

        byte[] xorKey = new byte[xorParts0.length];
        for (int i = 0; i < xorParts1.length; i++) {
            xorKey[i] = (byte) (xorParts0[i] ^ xorParts1[i]);
        }
        return xorKey;
    }

    /**
     * A convenience method that generates a XOR key pair for a given key. It was used to generate
     * the key for {@link MainActivity#useXorStringHiding(String)} method.
     *
     * @param key The source key to use in generating the XOR key halves
     * @return a two-value string array containing both parts of the XOR key
     */
    private static String[] generateKeyXorParts(String key) {
        String[] keyParts = new String[2];

        byte[] xorRandom = new byte[key.length()];
        byte[] xorMatch = new byte[key.length()];
        byte[] keyBytes = key.getBytes();
        for (int i = 0; i < key.length(); i++) {
            xorRandom[i] = (byte) (256 * Math.random());
            xorMatch[i] = (byte) (xorRandom[i] ^ keyBytes[i]);
        }
        keyParts[0] = Base64.encodeToString(xorRandom, 0);
        keyParts[1] = Base64.encodeToString(xorMatch, 0);
        Log.i(TAG, "XOR Key Part 0: " + keyParts[0]);
        Log.i(TAG, "XOR Key Part 1: " + keyParts[1]);

        return keyParts;
    }

    /**
     * Hide/unhide a message using a key and print the components. DEBUG METHOD.
     *
     * @param myHiddenMessage The message we want to hide or unhide
     * @param key             The key used in hiding
     */
    public static void useXorStringHiding(String myHiddenMessage, String key) {
        String[] myCompositeKey = HidingUtil.generateKeyXorParts(key);

        byte[] xorKey = HidingUtil.getXorKeyFromParts(myCompositeKey);

        doHiding(myHiddenMessage.getBytes(), xorKey, false);
    }

    /**
     * A reusable method to make our simple XOR hiding method. Since the interesting part is
     * how we get the hiding key, we've moved everything else into this reusable method.
     *
     * @param msg The message to hide/unhide
     * @param pwd Our password key to use in the XOR process
     */
    public static String unhide(String encryptedKey, String[] compositeKey) {
        byte[] xorKey = getXorKeyFromParts(compositeKey);
        byte[] msg = Base64.decode(encryptedKey, 0);
        xorValues(msg, xorKey);
        return new String(msg);
    }

}
