package com.trulymadly.android.app.billing;

import com.trulymadly.android.analytics.TrulyMadlyEvent;

/**
 * Created by avin on 02/11/16.
 */

public enum PaymentFor {
    select(1), spark(0);

    private int mKey;

    PaymentFor(int mKey) {
        this.mKey = mKey;
    }

    public static PaymentFor createFromKey(int key) {
        switch (key) {
            case 0:
                return spark;

            case 1:
                return select;

            default:
                return null;
        }
    }

    public int getmKey() {
        return mKey;
    }

    public String getTrackingString() {
        switch (this) {
            case spark:
                return TrulyMadlyEvent.TrulyMadlyActivities.sparks;

            case select:
                return TrulyMadlyEvent.TrulyMadlyActivities.select;
        }

        return null;
    }
}
