package com.trulymadly.android.app.billing;

/**
 * Created by avin on 20/07/16.
 */
public enum PaymentMode {
    google(1), paytm(2);//, paytm_wallet(3), paytm_nb(4), paytm_cc(5), paytm_dc(6);

    private int mKey;
    private PaytmBillingHandler.PAYTM_MODE mMode;

    PaymentMode(int mKey) {
        this.mKey = mKey;
    }

    public int getmKey() {
        return mKey;
    }

    public void setmKey(int mKey) {
        this.mKey = mKey;
    }

    public PaytmBillingHandler.PAYTM_MODE getmMode() {
        return mMode;
    }

    public void setmMode(PaytmBillingHandler.PAYTM_MODE mMode) {
        this.mMode = mMode;
    }
}
