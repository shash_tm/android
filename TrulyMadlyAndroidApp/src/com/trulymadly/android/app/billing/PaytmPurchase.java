package com.trulymadly.android.app.billing;

import android.os.Bundle;

/**
 * Created by avin on 26/07/16.
 */
public class PaytmPurchase {
    /*
    STATUS = TXN_SUCCESS,
    BANKNAME = ,
    ORDERID = TM2016_41064_1469515284 .1111,
    TXNAMOUNT = 199.00,
    TXNDATE = 2016 - 07 - 26 12: 12: 08.0,
    MID = Trulym23299004084955,
    TXNID = 2074767,
    RESPCODE = 01,
    PAYMENTMODE = PPI,
    BANKTXNID = 228925,
    CURRENCY = INR,
    GATEWAYNAME = WALLET,
    IS_CHECKSUM_VALID = Y,
    RESPMSG = Txn Successful.
    * */
    private String mOrderId;
    private String mDeveloperPayload;
    private String mSku;
    private String mToken;

    public String getmOrderId() {
        return mOrderId;
    }

    public void setmOrderId(String mOrderId) {
        this.mOrderId = mOrderId;
    }

    public String getmDeveloperPayload() {
        return mDeveloperPayload;
    }

    public void setmDeveloperPayload(String mDeveloperPayload) {
        this.mDeveloperPayload = mDeveloperPayload;
    }

    public String getmSku() {
        return mSku;
    }

    public void setmSku(String mSku) {
        this.mSku = mSku;
    }

    public String getmToken() {
        return mToken;
    }

    public void setmToken(String mToken) {
        this.mToken = mToken;
    }

    public static PaytmPurchase createPaytmPurchase(String sku, Bundle bundle){
        PaytmPurchase paytmPurchase = null;
        if(bundle != null){
            paytmPurchase = new PaytmPurchase();
            paytmPurchase.setmSku(sku);
            paytmPurchase.setmOrderId(bundle.getString("TXNID"));
            paytmPurchase.setmDeveloperPayload(bundle.getString("ORDERID"));
            paytmPurchase.setmToken(bundle.getString("TXNID"));
        }

        return paytmPurchase;
    }
}
