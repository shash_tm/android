package com.trulymadly.android.app.billing;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.crashlytics.android.Crashlytics;
import com.paytm.pgsdk.PaytmMerchant;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.fragments.BuySparkEventListener;
import com.trulymadly.android.app.json.ConstantsDB;
import com.trulymadly.android.app.json.ConstantsRF;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.listener.OnBuyPackageRequestComplete;
import com.trulymadly.android.app.modal.MySelectData;
import com.trulymadly.android.app.modal.PurchaseModal;
import com.trulymadly.android.app.sqlite.RFHandler;
import com.trulymadly.android.app.sqlite.SparksDbHandler;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.TmLogger;
import com.trulymadly.android.app.utility.Utility;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by avin on 19/07/16.
 */
public class PaytmBillingHandler extends SparksBillingHandler {

    /**
     * Parses the server response to get the params object
     * Right now, assuming that the response will come in the following format:
     */
    /*
    * {
          "order_id": "order_12345",
          "mid": "mid_value",
          "cust_id": "customer_id",
          "channel_id": "channel_id",
          "indtype_id": "indtype_id",
          "website": "website",
          "txn_amt": "txn_amt",
          "theme": "theme",
          "email": "email",
          "mob": "mob"
      }
    * */

    public static int payment = 1; // 1 : NB, 2 : CC, 3 : DC, 4 : Wallet
    /**
     * Parses the server response to get the params object
     * Right now, assuming that the response will come in the following format:
     */
    /*
    * {
          "order_id": "order_12345",
          "mid": "mid_value",
          "cust_id": "customer_id",
          "channel_id": "channel_id",
          "indtype_id": "indtype_id",
          "website": "website",
          "txn_amt": "txn_amt",
          "theme": "theme",
          "email": "email",
          "mob": "mob"
      }
    * */

    private final String TAG = "PaytmBillingHandler";
    private PaytmPGService mPaytmPGService;
    private WeakReference<Activity> mWeakActivity;
    private String mCurrentlyLaunchedSku = null;
    private int restoreRequestsLeft;


    public PaytmBillingHandler(Activity aActivity, View view, BuySparkEventListener mBuySparkEventListener,
                               PaymentFor paymentFor) {
        super(aActivity, view, PaymentMode.paytm, paymentFor, mBuySparkEventListener);
        mWeakActivity = new WeakReference<>(aActivity);
    }

    public static void togglePayment(Activity activity) {
        switch (payment) {
            case 1:
                RFHandler.insert(activity, Utility.getMyId(activity), ConstantsRF.RIGID_FIELD_IS_PAYTM_NB_PAYMENT_ENABLED, false);
                RFHandler.insert(activity, Utility.getMyId(activity), ConstantsRF.RIGID_FIELD_IS_PAYTM_CC_PAYMENT_ENABLED, true);
                RFHandler.insert(activity, Utility.getMyId(activity), ConstantsRF.RIGID_FIELD_IS_PAYTM_DC_PAYMENT_ENABLED, false);
                RFHandler.insert(activity, Utility.getMyId(activity), ConstantsRF.RIGID_FIELD_IS_PAYTM_WALLET_PAYMENT_ENABLED, false);
                payment = 2;
                break;
            case 2:
                RFHandler.insert(activity, Utility.getMyId(activity), ConstantsRF.RIGID_FIELD_IS_PAYTM_NB_PAYMENT_ENABLED, false);
                RFHandler.insert(activity, Utility.getMyId(activity), ConstantsRF.RIGID_FIELD_IS_PAYTM_CC_PAYMENT_ENABLED, false);
                RFHandler.insert(activity, Utility.getMyId(activity), ConstantsRF.RIGID_FIELD_IS_PAYTM_DC_PAYMENT_ENABLED, true);
                RFHandler.insert(activity, Utility.getMyId(activity), ConstantsRF.RIGID_FIELD_IS_PAYTM_WALLET_PAYMENT_ENABLED, false);
                payment = 3;
                break;
            case 3:
                RFHandler.insert(activity, Utility.getMyId(activity), ConstantsRF.RIGID_FIELD_IS_PAYTM_NB_PAYMENT_ENABLED, false);
                RFHandler.insert(activity, Utility.getMyId(activity), ConstantsRF.RIGID_FIELD_IS_PAYTM_CC_PAYMENT_ENABLED, false);
                RFHandler.insert(activity, Utility.getMyId(activity), ConstantsRF.RIGID_FIELD_IS_PAYTM_DC_PAYMENT_ENABLED, false);
                RFHandler.insert(activity, Utility.getMyId(activity), ConstantsRF.RIGID_FIELD_IS_PAYTM_WALLET_PAYMENT_ENABLED, true);
                payment = 4;
                break;
            case 4:
                RFHandler.insert(activity, Utility.getMyId(activity), ConstantsRF.RIGID_FIELD_IS_PAYTM_NB_PAYMENT_ENABLED, true);
                RFHandler.insert(activity, Utility.getMyId(activity), ConstantsRF.RIGID_FIELD_IS_PAYTM_CC_PAYMENT_ENABLED, false);
                RFHandler.insert(activity, Utility.getMyId(activity), ConstantsRF.RIGID_FIELD_IS_PAYTM_DC_PAYMENT_ENABLED, false);
                RFHandler.insert(activity, Utility.getMyId(activity), ConstantsRF.RIGID_FIELD_IS_PAYTM_WALLET_PAYMENT_ENABLED, false);
                payment = 1;
                break;
        }
        AlertsHandler.showMessage(activity, "Payment Mode : " + payment, false);

    }

    @Override
    public void restorePurchases(boolean isSilent) {
        Activity activity = mWeakActivity.get();
        if (activity == null) {
            return;
        }
        ArrayList<PurchaseModal> purchaseModals = SparksDbHandler.getPurchasesInPurchaseState(activity,
                ConstantsDB.PURCHASES.PurchaseState.CONSUMED_STORE, PaymentMode.paytm, getmPaymentFor());
        restoreRequestsLeft = 0;
        mRestoreRequestsLeft = 0;
        mRestoredSparksCount = 0;
        if (purchaseModals != null && purchaseModals.size() > 0) {
            restoreRequestsLeft = purchaseModals.size();
            mRestoreRequestsLeft = purchaseModals.size();
            for (final PurchaseModal purchaseModal : purchaseModals) {
                if (!isSilent) {
                    showProgressBar(true, true);
                }
                OnBuyPackageRequestComplete onBuyPackageRequestSuccess = new OnBuyPackageRequestComplete() {
                    @Override
                    public void onSuccess(String developer_payload, boolean isConsumedServer,
                                          int newSparksAdded, int totalSparks, boolean isRelationshipExpertAdded, MySelectData mySelectData) {
                        restoreRequestsLeft--;
                        if (restoreRequestsLeft <= 0) {
                            hideProgressBar();
                        }
                        handlePostPurchase(purchaseModal.getDeveloperPayload(), purchaseModal.getToken(),
                                purchaseModal.getOrderId(), purchaseModal.getSku(), isConsumedServer,
                                newSparksAdded, totalSparks, isRelationshipExpertAdded, mySelectData);
                    }
                };
                logOnServer(purchaseModal.getDeveloperPayload(), purchaseModal.getToken(),
                        purchaseModal.getOrderId(),
                        purchaseModal.getSku(), ConstantsDB.PURCHASES.PurchaseState.CONSUMED_STORE,
                        true, true, onBuyPackageRequestSuccess);
            }
        } else {
            if (!isSilent) {
                View view = getRootView();

                if (view == null) {
                    AlertsHandler.showMessage(activity, R.string.no_purchases_found, false);
                } else {
                    AlertsHandler.showMessageInDialog(view, R.string.no_purchases_found, false);
                }
            }
        }
    }

    ///////////////////////////////////////////////////////
    //Utility functions
    ///////////////////////////////////////////////////////

    @Override
    public void launchPurchaseFlow(String sku, String matchId, String price, PAYTM_MODE paytmMode) {
        super.launchPurchaseFlow(sku, matchId, price, paytmMode);
        mCurrentlyLaunchedSku = sku;
        //TODO : SPARKS: Restore needed?

        fetchPurchaseOrderDetails(price);
    }

    private void fetchPurchaseOrderDetails(final String price) {

        OnBuyPackageRequestComplete onBuyPackageRequestSuccess = new OnBuyPackageRequestComplete() {
            @Override
            public void onSuccess(String developer_payload, boolean isConsumedServer, int newSparksAdded, int totalSparks, boolean isRelationshipExpertAdded, MySelectData mySelectData) {
                Activity aActivity = mWeakActivity.get();
                if (aActivity == null) {
                    purchaseFailed(null, false, null, true, mCurrentlyLaunchedSku,
                            ERROR_ACTIVITY_DOESNT_EXISTS);
                    return;
                }

                if (Utility.isSet(developer_payload)) {
                    if (SparksDbHandler.createPurchase(aActivity, Utility.getMyId(aActivity),
                            mCurrentlyLaunchedSku, developer_payload, getmPaymentMode(), getmPaymentFor())) {
                        developerPayloadForPurchaseInProgress = developer_payload;
                        String purchasingSku = mCurrentlyLaunchedSku;
                        addToSkusList(purchasingSku);
                        try {
                            startTransaction(parsePurchaseOrderParams(developer_payload, Utility.getMyId(aActivity), price));
                        } catch (IllegalStateException e) {
                            Crashlytics.logException(e);
                        }
                    } else {
                        purchaseFailed(aActivity.getResources().getString(R.string.purchase_error_unknown),
                                true, developer_payload, true, mCurrentlyLaunchedSku, ERROR_DB_ROW_CREATE);
                    }
                } else {
                    purchaseFailed(aActivity.getResources().getString(R.string.purchase_error_unknown),
                            true, null, true, mCurrentlyLaunchedSku, ERROR_DEVELOPER_PAYLOAD_NOT_SET);
                }
            }
        };
        makeBuyPackageRequest(new PurchaseModal(mCurrentlyLaunchedSku), onBuyPackageRequestSuccess, false, false, checkingForUnConsumedStorePurchasesInProgress);

    }

    ///////////////////////////////////////////////////////
    //Utility functions
    ///////////////////////////////////////////////////////

    public void startTransaction(Map<String, String> paramMap) {

        PaytmPGService service = PaytmConfig.getPaytmPGService();
//        if(Constants.isLive){
//            service = PaytmPGService.getProductionService();
//        }else{
//            service = PaytmPGService.getStagingService();
//        }

        PaytmOrder Order = new PaytmOrder(paramMap);

        PaytmMerchant Merchant = new PaytmMerchant(
                ConstantsUrls.getPaytmChecksumGenerationUrl(),
                ConstantsUrls.getPaytmChecksumVerificationUrl());

        service.initialize(Order, Merchant, null);

        service.startPaymentTransaction(mWeakActivity.get(), true, true,
                new PaytmPaymentTransactionCallback() {
                    @Override
                    public void someUIErrorOccurred(String inErrorMessage) {
                        // Some UI Error Occurred in Payment Gateway Activity.
                        // // This may be due to initialization of views in
                        // Payment Gateway Activity or may be due to //
                        // initialization of webview. // Error Message details
                        // the error occurred.
                        TmLogger.d(TAG, "someUIErrorOccurred : " + inErrorMessage);
                        String errorMessage = null;
                        Activity activity = mWeakActivity.get();
                        if (activity != null) {
                            errorMessage = activity.getResources().getString(R.string.purchase_error_unknown);
                        }
                        purchaseFailed(errorMessage, true, developerPayloadForPurchaseInProgress, true,
                                mCurrentlyLaunchedSku, inErrorMessage);
                        developerPayloadForPurchaseInProgress = null;
                        mCurrentlyLaunchedSku = null;
                    }

                    @Override
                    public void onTransactionSuccess(Bundle inResponse) {
                        // After successful transaction this method gets called.
                        // // Response bundle contains the merchant response
                        // parameters.
                        TmLogger.d(TAG, "onTransactionSuccess : " + inResponse);
//                        Toast.makeText(getApplicationContext(), "Payment Transaction is successful ", Toast.LENGTH_LONG).show();

                        final PaytmPurchase paytmPurchase = PaytmPurchase.createPaytmPurchase(mCurrentlyLaunchedSku, inResponse);

                        developerPayloadForPurchaseInProgress = null;
                        TmLogger.d(TAG, "Purchase successful.");
//                        logPurchaseDetails(purchase);

                        OnBuyPackageRequestComplete onBuyPackageRequestSuccess = new OnBuyPackageRequestComplete() {
                            @Override
                            public void onSuccess(String developer_payload, boolean isConsumedServer,
                                                  int newSparksAdded, int totalSparks, boolean isRelationshipExpertAdded, MySelectData mySelectData) {

                                handlePostPurchase(checkNotNull(paytmPurchase).getmDeveloperPayload(),
                                        checkNotNull(paytmPurchase).getmToken(),
                                        checkNotNull(paytmPurchase).getmOrderId(),
                                        checkNotNull(paytmPurchase).getmSku(), isConsumedServer,
                                        newSparksAdded, totalSparks, isRelationshipExpertAdded, mySelectData);

//                                handlePostPurchase(paytmPurchase.getmDeveloperPayload(), paytmPurchase.getmToken(),
//                                        paytmPurchase.getmOrderId(), paytmPurchase.getmSku(), isConsumedServer,
//                                        newSparksAdded, totalSparks);
                            }
                        };
                        logOnServer(checkNotNull(paytmPurchase).getmDeveloperPayload(),
                                checkNotNull(paytmPurchase).getmToken(),
                                checkNotNull(paytmPurchase).getmOrderId(),
                                mCurrentlyLaunchedSku, ConstantsDB.PURCHASES.PurchaseState.CONSUMED_STORE,
                                false, false, onBuyPackageRequestSuccess);

                        mCurrentlyLaunchedSku = null;

                    }

                    @Override
                    public void onTransactionFailure(String inErrorMessage,
                                                     Bundle inResponse) {
                        /*
                        * {
                            STATUS = TXN_FAILURE,
                            ORDERID = TM2016_40143_1469186729 .9321,
                            TXNAMOUNT = 99.00,
                            MID = Trulym23299004084955,
                            RESPCODE = 330,   ← Paytm response code
                            IS_CHECKSUM_VALID = N,
                            RESPMSG = Invalid Checksum
                        }
                        * */
                        // This method gets called if transaction failed. //
                        // Here in this case transaction is completed, but with
                        // a failure. // Error Message describes the reason for
                        // failure. // Response bundle contains the merchant
                        // response parameters.
                        TmLogger.d(TAG, "onTransactionFailure : " + inErrorMessage + " :: " + inResponse);

                        StringBuilder errorBuffer = new StringBuilder(inErrorMessage);
                        if (inResponse != null && inResponse.containsKey("RESPCODE") && inResponse.get("RESPCODE") != null) {
                            errorBuffer.append(", RESPCODE :: ").append(String.valueOf(inResponse.get("RESPCODE")));
                        }

                        String errorMessage = null;
                        Activity activity = mWeakActivity.get();
                        if (activity != null) {
                            errorMessage = activity.getResources().getString(R.string.transaction_error);
                        }

                        //TODO : SPARKS: final parameters?
                        purchaseFailed(errorMessage, true, developerPayloadForPurchaseInProgress, true, mCurrentlyLaunchedSku, errorBuffer.toString());
                        developerPayloadForPurchaseInProgress = null;
                        mCurrentlyLaunchedSku = null;

                    }

                    @Override
                    public void networkNotAvailable() { // If network is not
                        // available, then this
                        // method gets called.
                        TmLogger.d(TAG, "networkNotAvailable");
                        String errorMessage = null;
                        Activity activity = mWeakActivity.get();
                        if (activity != null) {
                            errorMessage = activity.getResources().getString(R.string.whoops_no_internet);
                        }

                        purchaseFailed(errorMessage, true, developerPayloadForPurchaseInProgress, true,
                                mCurrentlyLaunchedSku, "No network");
                        developerPayloadForPurchaseInProgress = null;
                        mCurrentlyLaunchedSku = null;
                    }

                    @Override
                    public void clientAuthenticationFailed(String inErrorMessage) {
                        // This method gets called if client authentication
                        // failed. // Failure may be due to following reasons //
                        // 1. Server error or downtime. // 2. Server unable to
                        // generate checksum or checksum response is not in
                        // proper format. // 3. Server failed to authenticate
                        // that client. That is value of payt_STATUS is 2. //
                        // Error Message describes the reason for failure.
                        TmLogger.d(TAG, "clientAuthenticationFailed : " + inErrorMessage);

                        String errorMessage = null;
                        Activity activity = mWeakActivity.get();
                        if (activity != null) {
                            errorMessage = activity.getResources().getString(R.string.purchase_error_unknown);
                        }

                        purchaseFailed(errorMessage, true, developerPayloadForPurchaseInProgress, true,
                                mCurrentlyLaunchedSku, inErrorMessage);
                        mCurrentlyLaunchedSku = null;
                        developerPayloadForPurchaseInProgress = null;
                    }

                    @Override
                    public void onErrorLoadingWebPage(int iniErrorCode,
                                                      String inErrorMessage, String inFailingUrl) {
                        TmLogger.d(TAG, "onErrorLoadingWebPage : " + iniErrorCode + " :: " + inErrorMessage + " :: " + inFailingUrl);
                        String errorMessage = null;
                        Activity activity = mWeakActivity.get();
                        if (activity != null) {
                            errorMessage = activity.getResources().getString(R.string.purchase_error_unknown);
                        }

                        purchaseFailed(errorMessage, true, developerPayloadForPurchaseInProgress, true,
                                mCurrentlyLaunchedSku, inErrorMessage);
                        mCurrentlyLaunchedSku = null;
                        developerPayloadForPurchaseInProgress = null;
                    }

                    // had to be added: NOTE
                    @Override
                    public void onBackPressedCancelTransaction() {
                        purchaseFailed(null, true, developerPayloadForPurchaseInProgress, true,
                                mCurrentlyLaunchedSku, "OnBackPressed");
                        developerPayloadForPurchaseInProgress = null;
                        mCurrentlyLaunchedSku = null;
                        TmLogger.d(TAG, "onBackPressedCancelTransaction");
                    }

                });
    }

    @Override
    public void onDestroy() {

    }

    private boolean isNetBankingEnabled(Context context) {
        return RFHandler.getBool(context, Utility.getMyId(context), ConstantsRF.RIGID_FIELD_IS_PAYTM_NB_PAYMENT_ENABLED);
    }

    private boolean isCreditEnabled(Context context) {
        return RFHandler.getBool(context, Utility.getMyId(context), ConstantsRF.RIGID_FIELD_IS_PAYTM_CC_PAYMENT_ENABLED);
    }

    private boolean isDebitEnabled(Context context) {
        return RFHandler.getBool(context, Utility.getMyId(context), ConstantsRF.RIGID_FIELD_IS_PAYTM_DC_PAYMENT_ENABLED);
    }

    private boolean isWalletEnabled(Context context) {
        return RFHandler.getBool(context, Utility.getMyId(context), ConstantsRF.RIGID_FIELD_IS_PAYTM_WALLET_PAYMENT_ENABLED);
    }

    private Map<String, String> parsePurchaseOrderParams(String orderId, String customerId, String txnAmt) {

        Map<String, String> paramMap = new HashMap<>();

        PAYTM_MODE paytmMode = getmPaymentMode().getmMode();
        if (paytmMode != null) {
            switch (paytmMode) {
                case netbanking:
                    paramMap.put("AUTH_MODE", "USRPWD");
                    paramMap.put("PAYMENT_TYPE_ID", "NB");
                    paramMap.put("PAYMENT_MODE_ONLY", "YES");
                    break;
                case credit_card:
                    paramMap.put("AUTH_MODE", "3D");
                    paramMap.put("PAYMENT_TYPE_ID", "CC");
                    paramMap.put("PAYMENT_MODE_ONLY", "YES");
                    break;
                case debit_card:
                    paramMap.put("AUTH_MODE", "3D");
                    paramMap.put("PAYMENT_TYPE_ID", "DC");
                    paramMap.put("PAYMENT_MODE_ONLY", "YES");
                    break;
                case wallet:
                    paramMap.put("AUTH_MODE", "USRPWD");
                    paramMap.put("PAYMENT_TYPE_ID", "PPI");
                    paramMap.put("PAYMENT_MODE_ONLY", "YES");
                    break;
            }
        }

        paramMap.put("ORDER_ID", orderId);
        paramMap.put("MID", PaytmConfig.getMID());
        paramMap.put("CUST_ID", customerId);
        paramMap.put("CHANNEL_ID", PaytmConfig.getChannelID());
        paramMap.put("INDUSTRY_TYPE_ID", PaytmConfig.getIndustryID());
        paramMap.put("WEBSITE", PaytmConfig.getWebsite());
        paramMap.put("TXN_AMOUNT", txnAmt);
        paramMap.put("THEME", PaytmConfig.getTheme());

        return paramMap;
    }

    public enum PAYTM_MODE {
        all, wallet, netbanking, credit_card, debit_card
    }

}
