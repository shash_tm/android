package com.trulymadly.android.app.billing;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.crashlytics.android.Crashlytics;
import com.trulymadly.android.app.BuildConfig;
import com.trulymadly.android.app.IABUtil.IabHelper;
import com.trulymadly.android.app.IABUtil.IabResult;
import com.trulymadly.android.app.IABUtil.Inventory;
import com.trulymadly.android.app.IABUtil.Purchase;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.fragments.BuySparkEventListener;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.ConstantsDB;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.listener.CustomOkHttpResponseHandler;
import com.trulymadly.android.app.listener.OnBuyPackageRequestComplete;
import com.trulymadly.android.app.modal.MySelectData;
import com.trulymadly.android.app.modal.PurchaseModal;
import com.trulymadly.android.app.sqlite.SparksDbHandler;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.OkHttpHandler;
import com.trulymadly.android.app.utility.TmLogger;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by udbhav on 19/07/16.
 */
public class GooglePlayBillingHandler extends SparksBillingHandler {

    public static final String TEST_SKU_PURCHASED = "android.test.purchased";
    public static final String TEST_SKU_CANCELLED = "android.test.canceled";
    public static final String TEST_SKU_ITEM_UNAVILABLE = "android.test.item_unavailable";
    private static final String TAG = "GooglePlayBillingHandler";
    private final IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener;
    private final IabHelper.QueryInventoryFinishedListener mGotInventoryListener;
    private final IabHelper.OnConsumeFinishedListener mConsumeFinishedListener;
    String[] compositeKeyDev = new String[]{"SQncsfz8mmBuok54b76diLwAX6A9G9260DEvoh0mn/I=", "KV+6xcLIt1IJ5y9AAe7YtfwpB+MVSbvo6wQb+3lOq6o="};
    String[] compositeKeyT1 = new String[]{"8XsA4h0NSDjxi7dfquBDlkT52udmWlcJ2sdOoGNBuoE=", "yClxylpGfFar//k78Ycw0i6fkdsRaBIz+7wIxhlp6rk="};
    String[] compositeKeyLive = new String[]{"NZbWyApi1bMEAntnljJX2rf5a3i6BZoqKtziaN3lMbU=", "EdPwiX05gf9IJxge3BM9uJ6kFUbZcPxtfYiRX/avYu0="};
    // The helper object
    private IabHelper mHelper;
    private int restoreRequestsLeft;
    private boolean restorePurchasesOnSetup = false, restorePurchasesOnSetupIsSilent = true;
    private Inventory inventory = null;
    private Context aContext;
    private Activity aActivity;

    public GooglePlayBillingHandler(Activity aActivity, View view, BuySparkEventListener mBuySparkEventListener,
                                    PaymentFor paymentFor) {

        super(aActivity, view, PaymentMode.google, paymentFor, mBuySparkEventListener);
        this.aContext = aActivity;
        this.aActivity = aActivity;

        String encryptedPublicKeyDev = "LR8vNndebHwlIgpJBjssenleaAFpAyMUenR7GiU5DBktHy82fVNmcSYUJHkWAg1FK0A2cG4lCBgLb1A0CQJ7FyZ9Ny1NB1RGBCYjSSo0CU0nQzoGbzUHGm10DTEVRx8CJyQTDWZ3fQASCTJaFmk3S3hlPwxsBycrEH8DLlcOXxIZAyEncUV9Akg9NXMjJn1/ElArKRgWIDxQTF9gBRBmDDUbUT8GRUx8ERINYkEpIWcKTTAzYyRNJEFcdzRXRxsBUSdUAVNdWWEecSBoAn8RTBkQETRNBAIjVHtaOjwBGx81PzwzagdYBDAkF0xeGxRUI14pO3prFjEQA04VNThkPAgFNRZGdkp6UgElDTQcHQ8YaD8zSRcPJFQBQQEzJQU7Iz4/QFlfZ1kCEipaIzhqWQRuPAUDNhADT01SEB0uegkXBDAzWW1vW1cdSg42AXRIKQIUAkQIAxt0AWNrKxtfbgJnDjFOZXoHAzZZfQg+cVp2QTQIaR9WIAJnXRoxWVMQKAcvMH9lbHA=";
        String encryptedPublicKeyT1 = "dBs4ag4hdSAYEyUVMwwaA1MRe342YwB8YDoJJTt5aHl0GzhqBCx/LRslCyU1KyclLComRA5QK29MAQkPEwMCUVsEAR0mDnknADwbVDkSPycpDXlvHlAXdXYUM149RBticQtBfSw7Ww8AOB1TPUwbPiM0AlE1Cx12Z1QfFTlPZH9bCCBgAHxTLxwkAzAJICAQU149XRkdF1gTGjIuNHwFdwhnAEoMAFMUFzYEExERChQTHwQFDV8oa1AqKjQbYSEOVgIIfA4ZWDtjPgIiOBQWFjIgHE4BABVdTTcnAjlyJHYSYBxYLBJXLD4lFy4aXykzHlB5VDB/CApDPDQ1D3ozS2p9CVouD0NeI0I3CggDFQJbUB5FE0AkTEYTBx9VYT5tCD4beQJ5VRwYPTsNbRYKKzk+PQo5RCpjExQcABlbI1F9BzgbFh5eBx9GFDMyAAMiBQokEzxqcWpzM3VUK0E/QlVhEkEEP1A3OAA5NB5eHB4LEzJPFUovaEQ0MSoMHwpMWyU4bAYadSw=";
        String encryptedPublicKeyLive = "aQxvAz4xFQIOQggIIkoDJRAqTnwiJCMBFhU8dGobaxlpDG8DNDwfDw10Jjg/U1sNQygceSYHXykxDERdexICPEt8ZHABKGE0JUYzOx9sHDptcjoJEyENDhxlFABMCykxQAJ1IyMQGiYbUDAwBWlSNVAfCBUoQVINbzhKYXowNW9AAGk3Rg4HGS5gUw4ZFw8nAhovdAs3BXc6BQR0EnsCNUMNRSAbGQUgA2pVAwJPDCpiNBRmKy8VBSU1PlZ5OBAeVnB3ah0sGyh1F1EbLEsgAVE8FHsCIy0zBA0XeU8gZWxldX8kGBxhBBtPExY5YAIyfTYweQ07Ng4vMT1kXghmbHVuUTMiFAc5NEZXCAFIJ1UGNBBVJ0EnFiFiAX9HejYtVjYReRYiJn0hSCRNLwo5MmN2EX8sRBd/GBciYm0dH2lgMn84Oh0nNnp2BhEJbhMMYhEralQNIS0aJh9DYWEdb0hwHws1PhctNE0KMARxAiBGJSZxJjY1MmAQEEJ9HxwzHTJvBTYKFQ4=";

        // Create the helper, passing it our context and the public key to verify signatures with
        TmLogger.d(TAG, "Creating IAB helper.");
        mHelper = new IabHelper(aContext, Constants.isDev ? HidingUtil.unhide(encryptedPublicKeyDev, compositeKeyDev) :
                (Constants.isT1 ? HidingUtil.unhide(encryptedPublicKeyT1, compositeKeyT1) :
                        HidingUtil.unhide(encryptedPublicKeyLive, compositeKeyLive)));

        // enable debug logging (for a production application, you should set this to false).
        mHelper.enableDebugLogging(!Utility.isLiveRelease(), TAG);

        // Listener that's called when we finish querying the items and subscriptions we own
        mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
            public void onQueryInventoryFinished(IabResult result, Inventory i) {
                TmLogger.d(TAG, "Query inventory finished.");

                // Have we been disposed of in the meantime? If so, quit.
                if (mHelper == null) {
                    TmLogger.d(TAG, "Returning : mHelper is null");
                    return;
                }

                // Is it a failure?
                if (result.isFailure()) {
                    TmLogger.e(TAG, "Failed to query inventory: " + result);
                    return;
                }

                TmLogger.d(TAG, "Query inventory was successful.");
                inventory = i;

                addToSkusList(TEST_SKU_PURCHASED);
                addToSkusList(SparksDbHandler.getUniqueSkus(aContext, getmPaymentFor()));

                CustomOkHttpResponseHandler customOkHttpResponseHandler = new CustomOkHttpResponseHandler(aContext) {
                    @Override
                    public void onRequestSuccess(JSONObject responseJSON) {
                        hideProgressBar();
                        TmLogger.d(TAG, responseJSON.toString());

                        switch (responseJSON.optInt("responseCode")) {
                            case 200:
                                String packagesString = responseJSON.optString("packages");
                                if (Utility.isSet(packagesString)) {
                                    List<String> items = Arrays.asList(packagesString.split("\\s*,\\s*"));
                                    for (String item :
                                            items) {
                                        addToSkusList(item);
                                    }
                                }
                                consumeSparkInventory();
                                break;

                            default:
                                consumeSparkInventory();
                                break;

                        }
                    }

                    @Override
                    public void onRequestFailure(Exception exception) {
                        hideProgressBar();
                        consumeSparkInventory();
                    }
                };
                Map<String, String> params = new HashMap<>();
                params.put("action", "user_spark_package_history");
                params.put("type", getmPaymentFor().name());
                OkHttpHandler.httpPost(aContext, ConstantsUrls.getSparksApiUrl(), params, customOkHttpResponseHandler);

                TmLogger.d(TAG, "Initial inventory query finished; enabling main UI.");
            }
        };

        // Called when consumption is complete
        mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
            public void onConsumeFinished(final Purchase purchase, IabResult result) {
                TmLogger.d(TAG, "Consumption finished. Purchase: " + purchase + ", result: " + result);

                if (!validatePurchase(result, purchase)) {
                    return;
                }

                TmLogger.d(TAG, "Consumption successful.");
                logPurchaseDetails(purchase);
                OnBuyPackageRequestComplete onBuyPackageRequestSuccess = new OnBuyPackageRequestComplete() {
                    @Override
                    public void onSuccess(String developer_payload, boolean isConsumedServer,
                                          int newSparksAdded, int totalSparks, boolean isRelationshipExpertAdded, MySelectData mySelectData) {
                        boolean consumeOnServer = handlePostPurchase(purchase.getDeveloperPayload(), purchase.getToken(),
                                purchase.getOrderId(), purchase.getSku(), isConsumedServer,
                                newSparksAdded, totalSparks, isRelationshipExpertAdded, mySelectData);
                        if(consumeOnServer){
                            logConsumeOnStore(purchase);
                        }
                    }
                };
                logOnServer(purchase.getDeveloperPayload(), purchase.getToken(), purchase.getOrderId(),
                        purchase.getSku(), ConstantsDB.PURCHASES.PurchaseState.CONSUMED_STORE,
                        false, false, onBuyPackageRequestSuccess);
            }
        };

        // Callback for when a purchase is finished
        mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
            public void onIabPurchaseFinished(IabResult result, final Purchase purchase) {
                TmLogger.d(TAG, "Purchase finished: " + result + ", purchase: " + purchase);

                if (!validatePurchase(result, purchase)) {
                    return;
                }

                developerPayloadForPurchaseInProgress = null;
                TmLogger.d(TAG, "Purchase successful.");
                logPurchaseDetails(purchase);
                OnBuyPackageRequestComplete onBuyPackageRequestSuccess = new OnBuyPackageRequestComplete() {
                    @Override
                    public void onSuccess(String developer_payload, boolean isConsumedServer, int newSparksAdded, int totalSparks, boolean isRelationshipExpertAdded, MySelectData mySelectData) {
                        boolean consumeOnServer = handlePostPurchase(purchase.getDeveloperPayload(), purchase.getToken(),
                                purchase.getOrderId(), purchase.getSku(), isConsumedServer,
                                newSparksAdded, totalSparks, isRelationshipExpertAdded, mySelectData);
                        if (consumeOnServer || TEST_SKU_PURCHASED.equals(purchase.getSku())) {
                            logConsumeOnStore(purchase);
                        }
                    }
                };
                logOnServer(purchase.getDeveloperPayload(), purchase.getToken(), purchase.getOrderId(),
                        purchase.getSku(), ConstantsDB.PURCHASES.PurchaseState.PURCHASED_STORE,
                        true, false, onBuyPackageRequestSuccess);

            }
        };

        // Start setup. This is asynchronous and the specified listener
        // will be called once setup completes.
        TmLogger.d(TAG, "Starting setup.");

        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                TmLogger.d(TAG, "Setup finished.");

                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    TmLogger.e(TAG, "**** SETUP Error: " + "Problem setting up in-app billing: \" + result");
                    return;
                }

                // Have we been disposed of in the meantime? If so, quit.
                if (mHelper == null) return;

                TmLogger.d(TAG, "Setup successful. Querying inventory.");

                if (restorePurchasesOnSetup) {
                    restorePurchasesOnSetup = false;
                    restorePurchasesOnSetupIsSilent = true;
                    restorePurchases(restorePurchasesOnSetupIsSilent);
                }
            }
        });
    }

    @Override
    public void restorePurchases(boolean isSilent) {
        if (!mHelper.isSetupDone()) {
            restorePurchasesOnSetup = true;
            restorePurchasesOnSetupIsSilent = isSilent;
            return;
        }
        if (!isSilent) {
            checkingForUnConsumedStorePurchasesInProgress = true;
            showProgressBar(true, true);
        }
        ArrayList<PurchaseModal> unconsumedServerPurchases = SparksDbHandler.getPurchasesInPurchaseState(
                aContext, ConstantsDB.PURCHASES.PurchaseState.CONSUMED_STORE, getmPaymentMode(), getmPaymentFor());
        restoreRequestsLeft = 0;
        mRestoreRequestsLeft = 0;
        mRestoredSparksCount = 0;
        if (unconsumedServerPurchases.size() > 0) {
            restoreRequestsLeft = unconsumedServerPurchases.size();
            mRestoreRequestsLeft = unconsumedServerPurchases.size();
            for (final PurchaseModal p :
                    unconsumedServerPurchases) {
                if (p.getPurchaseState() == ConstantsDB.PURCHASES.PurchaseState.CONSUMED_STORE) {
                    OnBuyPackageRequestComplete onBuyPackageRequestComplete = new OnBuyPackageRequestComplete() {
                        @Override
                        public void onSuccess(String developer_payload, boolean isConsumedStore, int newSparksAdded, int totalSparks, boolean isRelationshipExpertAdded, MySelectData mySelectData) {
                            checkingForUnConsumedStorePurchasesInProgress = false;
                            purchaseSuccessfull(p.getSku(), p.getDeveloperPayload(), p.getToken(),
                                    p.getOrderId(), newSparksAdded, totalSparks,
                                    isRelationshipExpertAdded, mySelectData);
                            onSingleRestoreItemDone();
                        }
                    };
                    makeBuyPackageRequest(p, onBuyPackageRequestComplete, false, true, true);
                } else {
                    onSingleRestoreItemDone();
                }
            }
        } else {
            mHelper.queryInventoryAsync(mGotInventoryListener);
        }
    }

    private void onSingleRestoreItemDone() {
        restoreRequestsLeft--;
        if (restoreRequestsLeft == 0) {
            //All unconsumed server purchases complete. Moving on to unconsumed store purchases.
            mHelper.queryInventoryAsync(mGotInventoryListener);
        }
    }


    private boolean validatePurchase(IabResult result, Purchase purchase) {


        if (mHelper == null) {
            // if we were disposed of in the meantime, quit.
            purchaseFailed(aActivity.getResources().getString(R.string.purchase_error_unknown), true, purchase, ERROR_HELPER_DISPOSED);
            return false;
        }

        if (result.isFailure()) {
            TmLogger.e(TAG, "Error purchasing: " + result);
            switch (result.getResponse()) {
                //https://developer.android.com/google/play/billing/billing_reference.html#billing-codes
                case 1: //BILLING_RESPONSE_RESULT_USER_CANCELED
                case IabHelper.IABHELPER_USER_CANCELLED:
                    trackPurchaseError("USER_CANCELLED", (purchase != null) ? purchase.getSku() : "");
                    // User pressed back or canceled a dialog
                    hideProgressBar();
                    TmLogger.e(TAG, "User pressed back or canceled a dialog: " + result);
                    break;
                case 2: //Network connection is down : BILLING_RESPONSE_RESULT_SERVICE_UNAVAILABLE
                case 3: //Billing API version is not supported for the type requested : BILLING_RESPONSE_RESULT_BILLING_UNAVAILABLE
                    hideProgressBar();
                    purchaseFailed(aActivity.getResources().getString(R.string.ERROR_NETWORK_FAILURE), true, purchase, "NETWORK_ERROR_CODE_" + result.getResponse());
                    break;
                case 7: //Failure to purchase since item is already owned : BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED
                    trackPurchaseError("ALREADY_PURCHASED", (purchase != null) ? purchase.getSku() : "");
                    TmLogger.d(TAG, "Failure to purchase since item is already owned. Retrying consume");
                    mHelper.queryInventoryAsync(mGotInventoryListener);
                    break;
                case 4: //Requested product is not available for purchase : BILLING_RESPONSE_RESULT_ITEM_UNAVAILABLE
                case 5: //Invalid arguments provided to the API. : BILLING_RESPONSE_RESULT_DEVELOPER_ERROR
                case 6: //Fatal error during the API action : BILLING_RESPONSE_RESULT_ERROR
                case 8: //Failure to consume since item is not owned : BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED
                default:
                    hideProgressBar();
                    purchaseFailed(aActivity.getResources().getString(R.string.purchase_error_unknown), true, purchase, "BILLING_SDK_ERROR_CODE" + result.getResponse());
                    break;
            }
            return false;
        }

        return true;
    }

    protected void purchaseFailed(String error, boolean showError, Purchase purchase, String errorInfo) {
        String developerPayload = purchase != null ? purchase.getDeveloperPayload() : null;
        String sku = purchase != null ? purchase.getSku() : null;
        purchaseFailed(error, showError, developerPayload, false, sku, errorInfo);
    }

    private void logConsumeOnStore(Purchase purchase) {
        if (!mHelper.ismAsyncInProgress()) {
            showProgressBar(true, false);
            mHelper.consumeAsync(purchase, mConsumeFinishedListener);
        } else if (!Utility.isSet(mHelper.getmAsyncOperation())) {
            showProgressBar(true, false);
            mHelper.flagEndAsync();
            mHelper.consumeAsync(purchase, mConsumeFinishedListener);
        } else {
            hideProgressBar();
            String message = "Can't Consume, Already async operation " + mHelper.getmAsyncOperation() + "in progress";
            TmLogger.d(TAG, message);
            Crashlytics.logException(new IOException(message));
        }
    }

    public void consumeSparkInventory() {

        TmLogger.d(TAG, "Spark Unique SKUs " + skusList.toString());
        boolean anyPurchaseConsumed = false;
        if (skusList != null) {
            for (String sku : skusList) {
                anyPurchaseConsumed = anyPurchaseConsumed || checkAndConsumeSparkInventory(sku);
            }
        }
        if (!anyPurchaseConsumed) {
            ArrayList<PurchaseModal> unconsumedStorePurchases = SparksDbHandler.getPurchasesInPurchaseState(aContext,
                    ConstantsDB.PURCHASES.PurchaseState.PURCHASED_SERVER, getmPaymentMode(), getmPaymentFor());
            if (SparksDbHandler.getIncompletePurchasesCount(aContext, getmPaymentMode(), getmPaymentFor()) > 0) {
                for (final PurchaseModal p :
                        unconsumedStorePurchases) {
                    OnBuyPackageRequestComplete onBuyPackageRequestComplete = new OnBuyPackageRequestComplete() {
                        @Override
                        public void onSuccess(String developer_payload, boolean isConsumedStore,
                                              int newSparksAdded, int totalSparks, boolean isRelationshipExpertAdded,
                                              MySelectData mySelectData) {
                            checkingForUnConsumedStorePurchasesInProgress = false;
                            purchaseSuccessfull(p.getSku(), p.getDeveloperPayload(), p.getToken(),
                                    p.getOrderId(), newSparksAdded, totalSparks,
                                    isRelationshipExpertAdded, mySelectData);
                        }
                    };
                    makeBuyPackageRequest(p, onBuyPackageRequestComplete, false, true, true);
                }

            } else if (checkingForUnConsumedStorePurchasesInProgress) {
                View view = getRootView();
                if(view == null) {
                    AlertsHandler.showMessage(aActivity, R.string.no_purchases_found, false);
                }else {
                    AlertsHandler.showMessageInDialog(view, R.string.no_purchases_found, false);
                }
                checkingForUnConsumedStorePurchasesInProgress = false;
            }
        }
    }

    private boolean checkAndConsumeSparkInventory(String skuName) {
        if (inventory == null) {
            TmLogger.d(TAG, "NO INVENTORY FOUND : " + skuName);
            return false;
        }
        boolean hasPurchase = inventory.hasPurchase(skuName);
        if (hasPurchase) {
            TmLogger.d(TAG, "FOUND : " + skuName);
            final Purchase inventoryPurchase = inventory.getPurchase(skuName);
            if (inventoryPurchase != null) {
                logPurchaseDetails(inventoryPurchase);
                OnBuyPackageRequestComplete onBuyPackageRequestSuccess = new OnBuyPackageRequestComplete() {
                    @Override
                    public void onSuccess(String developer_payload, boolean isConsumedServer,
                                          int newSparksAdded, int totalSparks, boolean isRelationshipExpertAdded, MySelectData mySelectData) {
                        boolean consumeOnServer = handlePostPurchase(inventoryPurchase.getDeveloperPayload(), inventoryPurchase.getToken(),
                                inventoryPurchase.getOrderId(), inventoryPurchase.getSku(),
                                isConsumedServer, newSparksAdded, totalSparks, isRelationshipExpertAdded, mySelectData);
                        if (consumeOnServer || TEST_SKU_PURCHASED.equals(inventoryPurchase.getSku())) {
                            logConsumeOnStore(inventoryPurchase);
                        }
                    }
                };
                logOnServer(inventoryPurchase.getDeveloperPayload(), inventoryPurchase.getToken(),
                        inventoryPurchase.getOrderId(), inventoryPurchase.getSku(),
                        ConstantsDB.PURCHASES.PurchaseState.PURCHASED_STORE, true,
                        false, onBuyPackageRequestSuccess);
                return true;
            } else {
                TmLogger.d(TAG, "PURCHASE NULL");
                return false;
            }
        } else {
            TmLogger.d(TAG, "NOT FOUND : " + skuName);
            return false;
        }
    }

    private void logPurchaseDetails(Purchase inventoryPurchase) {
        TmLogger.d(TAG, "getDeveloperPayload : " + inventoryPurchase.getDeveloperPayload());
        TmLogger.d(TAG, "getItemType : " + inventoryPurchase.getItemType());
        TmLogger.d(TAG, "getOrderId : " + inventoryPurchase.getOrderId());
        TmLogger.d(TAG, "getOriginalJson : " + inventoryPurchase.getOriginalJson());
        TmLogger.d(TAG, "getPackageName : " + inventoryPurchase.getPackageName());
        TmLogger.d(TAG, "getPurchaseState : " + inventoryPurchase.getPurchaseState());
        TmLogger.d(TAG, "getPurchaseTime : " + inventoryPurchase.getPurchaseTime());
        TmLogger.d(TAG, "getSignature : " + inventoryPurchase.getSignature());
        TmLogger.d(TAG, "getSku : " + inventoryPurchase.getSku());
        TmLogger.d(TAG, "getToken : " + inventoryPurchase.getToken());
    }

    public boolean handleActivityResult(int requestCode, int resultCode, Intent data) {
        if (mHelper == null) return false;

        // Pass on the activity result to the helper for handling
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            return false;
        } else {
            TmLogger.d(TAG, "onActivityResult handled by IABUtil.");
            return true;
        }
    }

    @Override
    public void launchPurchaseFlow(final String sku, String matchId, String price, PaytmBillingHandler.PAYTM_MODE paytmMode) {
        super.launchPurchaseFlow(sku, matchId, price, paytmMode);
        if (SparksDbHandler.getIncompletePurchasesCount(aContext, getmPaymentMode(), getmPaymentFor()) > 0) {
            restorePurchases(false);
            return;
        }
        OnBuyPackageRequestComplete onBuyPackageRequestSuccess = new OnBuyPackageRequestComplete() {
            @Override
            public void onSuccess(String developer_payload, boolean isConsumedServer, int newSparksAdded, int totalSparks, boolean isRelationshipExpertAdded, MySelectData mySelectData) {
                if (Utility.isSet(developer_payload)) {
                    if (SparksDbHandler.createPurchase(aContext, Utility.getMyId(aContext), sku,
                            developer_payload, getmPaymentMode(), getmPaymentFor())) {
                        if (mHelper != null && !mHelper.isDisposed()) {
                            developerPayloadForPurchaseInProgress = developer_payload;
                            String purchasingSku = BuildConfig.DEBUG ? TEST_SKU_PURCHASED : sku;
                            addToSkusList(purchasingSku);
                            try {
                                mHelper.launchPurchaseFlow(aActivity, purchasingSku, Constants.PURCHASE_SPARKS_REQUEST_CODE,
                                        mPurchaseFinishedListener, developer_payload);
                            } catch (IllegalStateException e) {
                                Crashlytics.logException(e);
                            }
                        } else {
                            purchaseFailed(null, false, developer_payload, true, sku, ERROR_HELPER_DISPOSED);
                        }
                    } else {
                        purchaseFailed(aActivity.getResources().getString(R.string.purchase_error_unknown), true, developer_payload, true, sku, ERROR_DB_ROW_CREATE);
                    }
                } else {
                    purchaseFailed(aActivity.getResources().getString(R.string.purchase_error_unknown), true, null, true, sku, ERROR_DEVELOPER_PAYLOAD_NOT_SET);
                }
            }
        };
        makeBuyPackageRequest(new PurchaseModal(sku), onBuyPackageRequestSuccess, false, false, false);

    }

    @Override
    public void onDestroy() {
        // very important:
        TmLogger.d(TAG, "Destroying helper.");
        if (mHelper != null) {
            mHelper.dispose();
            mHelper = null;
        }
    }
}
