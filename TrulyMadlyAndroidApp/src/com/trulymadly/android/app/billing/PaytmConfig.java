package com.trulymadly.android.app.billing;

import com.paytm.pgsdk.PaytmPGService;
import com.trulymadly.android.app.json.Constants;

/**
 * Created by avin on 11/08/16.
 */
public class PaytmConfig {

    public static PaytmPGService getPaytmPGService(){
        if(Constants.isLive){
            return PaytmPGService.getProductionService();
        }else{
            return PaytmPGService.getStagingService();
        }
    }

    public static String getMID(){
        if(Constants.isLive){
            return "Trulym01422119955319";
        }else{
            return "Trulym23299004084955";
        }
    }

    public static String getChannelID(){
        if(Constants.isLive){
            return "WEB";
        }else{
            return "WAP";
        }
    }

    public static String getIndustryID(){
        if(Constants.isLive){
            return "Retail109";
        }else{
            return "Retail";
        }
    }

    public static String getWebsite(){
        if(Constants.isLive){
            return "TrulymadlyWAP";
        }else{
            return "TrulymadlyWAP";
        }
    }

    public static String getTheme(){
        if(Constants.isLive){
            return "merchant";
        }else{
            return "merchant";
        }
    }

}
