package com.trulymadly.android.app.billing;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.trulymadly.android.app.R;
import com.trulymadly.android.app.fragments.BuySparkEventListener;
import com.trulymadly.android.app.json.ConstantsRF;
import com.trulymadly.android.app.sqlite.RFHandler;
import com.trulymadly.android.app.sqlite.SparksDbHandler;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.Utility;

/**
 * Created by avin on 20/07/16.
 */
public class SparksBillingController {
    private PaymentMode mPaymentMode;
    private GooglePlayBillingHandler mGooglePlayBillingHandler;
    private PaytmBillingHandler mPaytmBillingHandler;
    private View.OnClickListener mOnPaymentSelectedClickListener;
    private Context mContext;
    private PaymentFor mPaymentFor;

    public SparksBillingController(Activity aActivity, View view, BuySparkEventListener mBuySparkEventListener) {
        mContext = aActivity;
        mPaymentFor = PaymentFor.spark;
        mPaytmBillingHandler = new PaytmBillingHandler(aActivity, view, mBuySparkEventListener, mPaymentFor);
        mGooglePlayBillingHandler = new GooglePlayBillingHandler(aActivity, view, mBuySparkEventListener, mPaymentFor);
    }

    public SparksBillingController(Activity aActivity, View view, PaymentFor paymentFor, BuySparkEventListener mBuySparkEventListener) {
        mContext = aActivity;
        mPaymentFor = paymentFor;
        mPaytmBillingHandler = new PaytmBillingHandler(aActivity, view, mBuySparkEventListener, mPaymentFor);
        mGooglePlayBillingHandler = new GooglePlayBillingHandler(aActivity, view, mBuySparkEventListener, mPaymentFor);
    }

    public void setRootView(View view){
        mPaytmBillingHandler.setRootView(view);
        mGooglePlayBillingHandler.setRootView(view);
    }

    public void askForPaymentOption(String sku, String matchId, int sparks, String price){
        launchPaymentModeOptionsDialog(sku, matchId, sparks, price);
    }

    private void launchPaymentModeOptionsDialog(final String sku, final String matchId, int sparks, final String price){

        boolean isAnyPaymentEnabled = false, isMoreThanTwoModesEnabled = false;
        boolean isGooglePaymentEnabled = RFHandler.getBool(mContext, Utility.getMyId(mContext),
                ConstantsRF.RIGID_FIELD_IS_GOOGLE_PAYMENT_ENABLED);
        boolean isPaytmPaymentEnabled = RFHandler.getBool(mContext, Utility.getMyId(mContext),
                ConstantsRF.RIGID_FIELD_IS_PAYTM_PAYMENT_ENABLED);
        boolean isPaytmWalletEnabled = RFHandler.getBool(mContext, Utility.getMyId(mContext),
                ConstantsRF.RIGID_FIELD_IS_PAYTM_WALLET_PAYMENT_ENABLED);
        boolean isPaytmNBEnabled = RFHandler.getBool(mContext, Utility.getMyId(mContext),
                ConstantsRF.RIGID_FIELD_IS_PAYTM_NB_PAYMENT_ENABLED);
        boolean isPaytmCCEnabled = RFHandler.getBool(mContext, Utility.getMyId(mContext),
                ConstantsRF.RIGID_FIELD_IS_PAYTM_CC_PAYMENT_ENABLED);
        boolean isPaytmDCEnabled = RFHandler.getBool(mContext, Utility.getMyId(mContext),
                ConstantsRF.RIGID_FIELD_IS_PAYTM_DC_PAYMENT_ENABLED);

        isAnyPaymentEnabled = isGooglePaymentEnabled || isPaytmPaymentEnabled || isPaytmWalletEnabled ||
            isPaytmNBEnabled || isPaytmCCEnabled || isPaytmDCEnabled;

        int enabled = ((isGooglePaymentEnabled)?1:0) + ((isPaytmPaymentEnabled)?1:0) +
                ((isPaytmWalletEnabled)?1:0) + ((isPaytmNBEnabled)?1:0) + ((isPaytmCCEnabled)?1:0)
                + ((isPaytmDCEnabled)?1:0);
        isMoreThanTwoModesEnabled = enabled >= 2;

        if(!isAnyPaymentEnabled){
            isGooglePaymentEnabled = true;
        }

        if(isMoreThanTwoModesEnabled){
//            if(mOnPaymentSelectedClickListener == null){
                mOnPaymentSelectedClickListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        switch (v.getId()){
                            case R.id.google_payment_button:
                                launchPurchaseFlow(PaymentMode.google, sku, matchId, price, null);
                                break;
                            case R.id.paytm_payment_button:
                                launchPurchaseFlow(PaymentMode.paytm, sku, matchId, price, PaytmBillingHandler.PAYTM_MODE.all);
                                break;
                            case R.id.paytm_wallet_payment_button:
                                launchPurchaseFlow(PaymentMode.paytm, sku, matchId, price, PaytmBillingHandler.PAYTM_MODE.wallet);
                                break;
                            case R.id.paytm_nb_payment_button:
                                launchPurchaseFlow(PaymentMode.paytm, sku, matchId, price, PaytmBillingHandler.PAYTM_MODE.netbanking);
                                break;
                            case R.id.paytm_cc_payment_button:
                                launchPurchaseFlow(PaymentMode.paytm, sku, matchId, price, PaytmBillingHandler.PAYTM_MODE.credit_card);
                                break;
                            case R.id.paytm_dc_payment_button:
                                launchPurchaseFlow(PaymentMode.paytm, sku, matchId, price, PaytmBillingHandler.PAYTM_MODE.debit_card);
                                break;
                        }
                    }
                };
//            }


            String headerText = null;
            if (mPaymentFor == PaymentFor.spark) {
                headerText = String.format(mContext.getString(R.string.sparks_payment_selection_header), String.valueOf(sparks), price);
            }
            AlertsHandler.showPaymentModeDialog(mContext, headerText,
                    mPaymentFor == PaymentFor.select,
                    mOnPaymentSelectedClickListener,
                    isGooglePaymentEnabled, isPaytmPaymentEnabled, isPaytmWalletEnabled,
                    isPaytmNBEnabled, isPaytmCCEnabled, isPaytmDCEnabled);
        }else if(isGooglePaymentEnabled){
            launchPurchaseFlow(PaymentMode.google, sku, matchId, price, null);
        }else{
            launchPurchaseFlow(PaymentMode.paytm, sku, matchId, price, PaytmBillingHandler.PAYTM_MODE.all);
        }
    }

    private void launchPurchaseFlow(PaymentMode paymentMode, String sku, String matchId, String price, PaytmBillingHandler.PAYTM_MODE paytmMode){
        mPaymentMode = paymentMode;
        switch (mPaymentMode){
            case google:
                mGooglePlayBillingHandler.launchPurchaseFlow(sku, matchId, price, paytmMode);
                break;

            case paytm:
                mPaytmBillingHandler.launchPurchaseFlow(sku, matchId, price, paytmMode);
                break;
        }
    }

    public void restorePurchases(boolean isSilent){
        if(isSilent) {
            mGooglePlayBillingHandler.restorePurchases(isSilent);
            mPaytmBillingHandler.restorePurchases(isSilent);
        } else if (SparksDbHandler.getIncompletePurchasesCount(mContext, PaymentMode.paytm, mPaymentFor) > 0) {
            mPaytmBillingHandler.restorePurchases(isSilent);
        }else{
            mGooglePlayBillingHandler.restorePurchases(isSilent);
        }
    }

    public boolean handleActivityResult(int requestCode, int resultCode, Intent data){
        return mGooglePlayBillingHandler.handleActivityResult(requestCode, resultCode, data);
    }

    public void onDestroy(){
        mGooglePlayBillingHandler.onDestroy();
        mPaytmBillingHandler.onDestroy();
    }
}
