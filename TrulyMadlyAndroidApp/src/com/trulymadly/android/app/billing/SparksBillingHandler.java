package com.trulymadly.android.app.billing;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.facebook.appevents.AppEventsConstants;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.asynctasks.TrackEventToFbAsyncTask;
import com.trulymadly.android.app.bus.ShowRestorePurchasesEvent;
import com.trulymadly.android.app.fragments.BuySparkEventListener;
import com.trulymadly.android.app.json.ConstantsDB.PURCHASES.PurchaseState;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.listener.CustomOkHttpResponseHandler;
import com.trulymadly.android.app.listener.OnBuyPackageRequestComplete;
import com.trulymadly.android.app.modal.MySelectData;
import com.trulymadly.android.app.modal.PurchaseModal;
import com.trulymadly.android.app.sqlite.SparksDbHandler;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.MoEHandler;
import com.trulymadly.android.app.utility.OkHttpHandler;
import com.trulymadly.android.app.utility.SparksHandler;
import com.trulymadly.android.app.utility.TmLogger;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventStatus.buy_error;
import static com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes.buy;

/**
 * Created by udbhav on 08/06/16.
 */
public abstract class SparksBillingHandler {

    protected static final String ERROR_HELPER_DISPOSED = "HELPER_DISPOSED";
    protected static final String ERROR_DB_ROW_CREATE = "DB_ROW_CREATE";
    protected static final String ERROR_DEVELOPER_PAYLOAD_NOT_SET = "DEVELOPER_PAYLOAD_NOT_SET";
    protected static final String ERROR_ACTIVITY_DOESNT_EXISTS = "ACTIVITY_DOESNT_EXISTS";
    private static final String TAG = "SparksBillingHandler";
    private final Context aContext;

    private final Activity aActivity;
    private final BuySparkEventListener mBuySparkEventListener;
    public String developerPayloadForPurchaseInProgress = null;
    protected ArrayList<String> skusList = null;
    protected boolean checkingForUnConsumedStorePurchasesInProgress = false;
    protected int mRestoreRequestsLeft = 0;
    protected int mRestoredSparksCount = 0;
    private WeakReference<View> mWeakRootView;
    private ProgressDialog mProgressBar = null;
    private String matchId;
    private PaymentMode mPaymentMode;
    private PaymentFor mPaymentFor = PaymentFor.spark;

    public SparksBillingHandler(final Activity aActivity,
                                View view, PaymentMode paymentMode,
                                BuySparkEventListener mBuySparkEventListener) {
        aContext = aActivity;
        this.aActivity = aActivity;
        this.mPaymentMode = paymentMode;
        mWeakRootView = new WeakReference<>(view);
        this.mBuySparkEventListener = mBuySparkEventListener;
    }

    public SparksBillingHandler(final Activity aActivity,
                                View view, PaymentMode paymentMode,
                                PaymentFor paymentFor,
                                BuySparkEventListener mBuySparkEventListener) {
        aContext = aActivity;
        this.aActivity = aActivity;
        this.mPaymentMode = paymentMode;
        this.mPaymentFor = paymentFor;
        mWeakRootView = new WeakReference<>(view);
        this.mBuySparkEventListener = mBuySparkEventListener;
    }

    protected boolean handlePostPurchase(String developerPayload, String token, String orderId, String sku,
                                         boolean isConsumedServer, int newSparksAdded, int totalSparks,
                                         boolean isRelationshipExpertAdded, MySelectData mySelectData) {
        if (!isConsumedServer) {
            SparksDbHandler.setSparkPurchaseState(aContext, Utility.getMyId(aContext),
                    developerPayload, PurchaseState.PURCHASED_SERVER, token, orderId, sku, mPaymentMode,
                    mPaymentFor);
            return true;
        } else {
            purchaseSuccessfull(sku, developerPayload, token, orderId, newSparksAdded,
                    totalSparks, isRelationshipExpertAdded, mySelectData);
            return false;
        }
    }

    protected void purchaseSuccessfull(String sku, String developerPayload, String token, String orderId,
                                       int newSparksAdded, int totalSparks,
                                       boolean isRelationshipExpertAdded, MySelectData mySelectData) {
        hideProgressBar();
        SparksDbHandler.setSparkPurchaseState(aContext, Utility.getMyId(aContext),
                developerPayload, PurchaseState.CONSUMED_SERVER, token, orderId, sku,
                mPaymentMode, mPaymentFor);
        if (totalSparks > 0) {
            SparksHandler.insertSparks(aContext, totalSparks);
        }
        mRestoredSparksCount += newSparksAdded;
        if (mRestoreRequestsLeft <= 0) {
            mBuySparkEventListener.onBuySparkSuccess(mPaymentMode, sku, mRestoredSparksCount,
                    totalSparks, isRelationshipExpertAdded, mySelectData);
            mRestoredSparksCount = 0;
        }
    }

    protected void logOnServer(String developerPayload, String token, String orderId, String sku, PurchaseState purchaseState,
                               boolean continueLoaderOnSuccess, boolean isRestore, OnBuyPackageRequestComplete onBuyPackageRequestSuccess) {
        SparksDbHandler.setSparkPurchaseState(aContext, Utility.getMyId(aContext),
                developerPayload, purchaseState, token, orderId, sku, mPaymentMode, mPaymentFor);
        PurchaseModal purchaseModal = new PurchaseModal(sku, token, orderId, developerPayload, purchaseState);
        makeBuyPackageRequest(purchaseModal, onBuyPackageRequestSuccess, continueLoaderOnSuccess, true, isRestore);
    }

    public abstract void restorePurchases(boolean isSilent);

    public abstract void onDestroy();

    public void launchPurchaseFlow(final String sku, String matchId, String price, PaytmBillingHandler.PAYTM_MODE paytmMode) {
        this.matchId = matchId;
        this.mPaymentMode.setmMode(paytmMode);
    }

    protected void showProgressBar(boolean isAfterPurchase, boolean isRestore) {
        mProgressBar = UiUtils.showProgressBar(aContext, mProgressBar, isRestore ? R.string.restoring_purchase : isAfterPurchase ? R.string.completing_purchase : R.string.purchasing_in_progress);
    }

    public void hideProgressBar() {
        mProgressBar = UiUtils.hideProgressBar(mProgressBar);
    }

    public void addToSkusList(String sku) {
        if (skusList == null) {
            skusList = new ArrayList<>();
        }
        if (Utility.isSet(sku) && !skusList.contains(sku)) {
            skusList.add(sku);
        }
    }

    public void addToSkusList(ArrayList<String> skus) {
        if (skus != null) {
            for (String sku : skus) {
                addToSkusList(sku);
            }
        }
    }

    protected void purchaseFailed(String error, boolean showError, String developerPayload,
                                  boolean setStateCancelled, String sku, String errorInfo) {
        if (setStateCancelled) {
            if (developerPayload == null && developerPayloadForPurchaseInProgress != null) {
                developerPayload = developerPayloadForPurchaseInProgress;
            }
            SparksDbHandler.setSparkPurchaseState(aContext, Utility.getMyId(aContext), developerPayload,
                    PurchaseState.CANCELLED, null, null, sku, mPaymentMode, mPaymentFor);
        }

        if (showError && Utility.isSet(error)) {
            View view = getRootView();

            if (view == null) {
                AlertsHandler.showMessage(aActivity, error, false);
            } else {
                AlertsHandler.showMessageInDialog(view, error, false);
            }

            Utility.fireBusEvent(aContext, true, new ShowRestorePurchasesEvent());
        }

        developerPayloadForPurchaseInProgress = null;
        trackPurchaseError(errorInfo, sku);
    }

    protected View getRootView() {
        View view = null;
        if (mWeakRootView != null) {
            view = mWeakRootView.get();
        }

        return view;
    }

    public void setRootView(View view) {
        mWeakRootView = new WeakReference<>(view);
    }

    public PaymentMode getmPaymentMode() {
        return mPaymentMode;
    }

    public PaymentFor getmPaymentFor() {
        return mPaymentFor;
    }

    protected void trackPurchaseError(String errorInfo, String sku) {
        Map<String, String> eventInfo = new HashMap<>();
        eventInfo.put("match_id", matchId);
        eventInfo.put("error", errorInfo);
        eventInfo.put("pg", mPaymentMode.name());
        if (mPaymentMode.getmMode() != null) {
            eventInfo.put("paytm_mode", mPaymentMode.getmMode().name());
        }
        TrulyMadlyTrackEvent.trackEvent(aContext, getmPaymentFor().getTrackingString(), buy, 0, buy_error, eventInfo, true, true);

        MoEHandler.trackEvent(aContext, MoEHandler.Events.PURCHASE_NOT_COMPLETED + " " + sku);
    }

    public void makeBuyPackageRequest(final PurchaseModal purchase, final OnBuyPackageRequestComplete onBuyPackageRequestComplete,
                                      final boolean continueLoaderOnSuccess, boolean isAfterPurchase, final boolean isRestore) {

        showProgressBar(isAfterPurchase, false);

        CustomOkHttpResponseHandler customOkHttpResponseHandler = new CustomOkHttpResponseHandler(aContext) {
            @Override
            public void onRequestSuccess(JSONObject responseJSON) {
                TmLogger.d(TAG, responseJSON.toString());
                if (isRestore) {
                    mRestoreRequestsLeft--;
                }
                int code = responseJSON.optInt("responseCode");
                switch (code) {
                    case 200:
                        if (!continueLoaderOnSuccess) {
                            hideProgressBar();
                        }
                        int newSparks = responseJSON.optInt("new_sparks_added", 0);
                        boolean sendFbTracking = false;
                        Bundle params = new Bundle();
                        if (getmPaymentFor() == PaymentFor.spark) {
                            if (newSparks > 0) {
                                params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, "Sparks");
                                params.putInt(AppEventsConstants.EVENT_PARAM_NUM_ITEMS, newSparks);
                                sendFbTracking = true;
                            }
                        } else {
                            params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, getmPaymentFor().getTrackingString());
                            sendFbTracking = true;
                        }
                        if (sendFbTracking) {
                            params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_ID, purchase.getSku());
                            TrackEventToFbAsyncTask.trackEvent(aActivity, AppEventsConstants.EVENT_NAME_PURCHASED, params);
                        }
                        JSONObject mySelectDataJSONObj = responseJSON.optJSONObject("my_select");
                        MySelectData mySelectData = null;
                        if (mySelectDataJSONObj != null) {
                            mySelectData = new MySelectData(mySelectDataJSONObj);
                        }
                        onBuyPackageRequestComplete.onSuccess(responseJSON.optString("developer_payload"),
                                responseJSON.optBoolean("consumptionState"), newSparks, responseJSON.optInt("sparks", 0),
                                responseJSON.optBoolean("chat_assist", false), mySelectData);

                        break;

                    case 403:
                    default:
                        checkingForUnConsumedStorePurchasesInProgress = false;
                        hideProgressBar();
                        String errorMessage = responseJSON.optString("error");
                        if (!Utility.isSet(errorMessage)) {
                            errorMessage = aActivity.getResources().getString(R.string.purchase_error_unknown);
                        }
                        purchaseFailed(errorMessage, true, purchase.getDeveloperPayload(), false,
                                purchase.getSku(), "BUY_PACKAGE_REQUEST_ERROR_CODE_" + code + "_" + errorMessage);
                        break;

                }
            }

            @Override
            public void onRequestFailure(Exception exception) {
                if (isRestore) {
                    mRestoreRequestsLeft--;
                }
                checkingForUnConsumedStorePurchasesInProgress = false;
                hideProgressBar();
                purchaseFailed(aActivity.getResources().getString(R.string.ERROR_NETWORK_FAILURE), true,
                        purchase.getDeveloperPayload(), false, purchase.getSku(), "BUY_PACKAGE_NETWORK_ERROR");
            }
        };
        Map<String, String> params = new HashMap<>();
        params.put("action", "buy_package");
        params.put("google_sku", purchase.getSku());
        if (Utility.isSet(purchase.getToken())) {
            switch (mPaymentMode) {
                case google:
                    params.put("google_token", purchase.getToken());
                    break;

                case paytm:
                    params.put("transaction_id", purchase.getToken());
                    break;
            }
        }
        if (Utility.isSet(purchase.getOrderId())) {
            params.put("order_id", purchase.getOrderId());
        }

        if (mPaymentMode == PaymentMode.paytm && Utility.isSet(purchase.getDeveloperPayload())) {
            params.put("developer_payload", purchase.getDeveloperPayload());
        }
        params.put("pg", mPaymentMode.name());
        params.put("type", mPaymentFor.name());
        OkHttpHandler.httpPost(aContext, ConstantsUrls.getSparksApiUrl(), params, customOkHttpResponseHandler);
    }

}
