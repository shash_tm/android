/**
 *
 */
package com.trulymadly.android.app.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import com.trulymadly.android.app.R;
import com.trulymadly.android.app.utility.UiUtils;

/**
 * @author udbhav
 */
@SuppressLint("DrawAllocation")
public class TrustScoreCurve extends View {

    private int score = 0;

    public TrustScoreCurve(Context context) {
        super(context);
    }

    public TrustScoreCurve(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public TrustScoreCurve(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (isInEditMode()) {
            return;
        }
        if (canvas == null) {
            canvas = new Canvas();
        }
        super.onDraw(canvas);

        Paint paint = new Paint();
        final RectF rect = new RectF();

        // HACK to converting score to angle by using max value as 175 instead
        // of 180
        int angle = score * 175 / 100;

        int curveWidthInDp = 12;
        int curveWidthInPx = UiUtils.dpToPx(curveWidthInDp);

        int widthInDp = 80;
        rect.set(curveWidthInPx / 2, curveWidthInPx / 2,
                UiUtils.dpToPx(widthInDp) - curveWidthInPx
                        / 2, UiUtils.dpToPx(widthInDp)
                        - curveWidthInPx / 2);
        paint.setColor(getResources().getColor(R.color.final_blue_3));
        paint.setStrokeWidth(curveWidthInPx);
        paint.setAntiAlias(true);
        paint.setStrokeCap(Paint.Cap.SQUARE);
        paint.setStyle(Paint.Style.STROKE);
        canvas.drawArc(rect, 180 + angle, 180 - angle, false, paint);

        rect.set(curveWidthInPx / 2, curveWidthInPx / 2,
                UiUtils.dpToPx(widthInDp) - curveWidthInPx
                        / 2, UiUtils.dpToPx(widthInDp)
                        - curveWidthInPx / 2);
        paint.setColor(getResources().getColor(R.color.final_blue_1));
        paint.setStrokeWidth(curveWidthInPx);
        paint.setAntiAlias(true);
        paint.setStrokeCap(Paint.Cap.SQUARE);
        paint.setStyle(Paint.Style.STROKE);
        canvas.drawArc(rect, 180, angle, false, paint);

    }

    public void reDraw(int trust_score) {
        this.score = trust_score;
        this.invalidate();
    }

}
