package com.trulymadly.android.app.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.VideoView;

/**
 * Created by udbhav on 14/09/16.
 * using this answer
 * http://stackoverflow.com/questions/4434027/android-videoview-orientation-change-with-buffered-video/4452597#4452597
 */
public class CustomVideoView extends VideoView {

    private int mForceHeight = 0;
    private int mForceWidth = 0;

    public CustomVideoView(Context context) {
        super(context);
    }

    public CustomVideoView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomVideoView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setDimensions(int w, int h) {
        this.mForceHeight = h;
        this.mForceWidth = w;

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (isInEditMode()) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        } else {
            setMeasuredDimension(mForceWidth, mForceHeight);
        }
    }
}
