package com.trulymadly.android.app.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.View;

import com.trulymadly.android.app.R;

/**
 * Created by avin on 22/02/16.
 */
public class ArcLoader extends View implements Animatable {
    // =============================================================================================
    // CONSTANTS
    // =============================================================================================

    private static final int MARGIN_MEDIUM = 5;
    private static final int SPEED_MEDIUM = 5;
    private static final long FRAME_DURATION = 1000 / 60;

    // =============================================================================================
    // FIELDS
    // =============================================================================================
    private ArcDrawable mArcDrawable;

    // =============================================================================================
    // CONSTRUCTORS
    // =============================================================================================

    public ArcLoader(Context context) {
        super(context);
    }

    public ArcLoader(Context context, AttributeSet attrs) {
        super(context, attrs);

        init(attrs);
    }

    public ArcLoader(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init(attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ArcLoader(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        init(attrs);
    }

    // =============================================================================================
    // METHODS
    // =============================================================================================

    private void init(AttributeSet attributeSet) {
        ArcConfiguration configuration = readFromAttributes(attributeSet);

        // Load Drawable for this View
        mArcDrawable = new ArcDrawable(configuration);
        setBackgroundDrawable(mArcDrawable);

        // Start the Animation
//        start();
    }

    private ArcConfiguration readFromAttributes(AttributeSet attributeSet) {
        ArcConfiguration configuration = new ArcConfiguration(getContext());

        TypedArray array = getContext().obtainStyledAttributes(attributeSet, R.styleable.ArcLoader);

        for (int i = 0; i < array.length(); i++) {
            int type = array.getIndex(i);

            if (type == R.styleable.ArcLoader_arc_color) {
                int colors_resourse_id = array.getColor(R.styleable.ArcLoader_arc_color, Color.BLUE);
                configuration.setColor(colors_resourse_id);
            }

            if (type == R.styleable.ArcLoader_arc_speed) {
                String value = array.getString(R.styleable.ArcLoader_arc_speed);

                if (value != null)
                    configuration.setAnimationSpeedWithIndex(Integer.parseInt(value));
            }

            if (type == R.styleable.ArcLoader_arc_margin) {
                Float value = array.getDimension(R.styleable.ArcLoader_arc_margin, MARGIN_MEDIUM);

                configuration.setArcMargin(value.intValue());
            }

            if (type == R.styleable.ArcLoader_arc_thickness) {
                Float value = array.getDimension(R.styleable.ArcLoader_arc_thickness,
                        getContext().getResources().getDimension(R.dimen.default_stroke_width));
                configuration.setArcWidthInPixel(value.intValue());
            }
        }

        array.recycle();

        return configuration;
    }

    @Override
    public void start() {
        if (mArcDrawable != null)
            mArcDrawable.start();
    }

    @Override
    public void stop() {
        if (mArcDrawable != null)
            mArcDrawable.stop();
    }

    @Override
    public boolean isRunning() {

        return mArcDrawable != null && mArcDrawable.isRunning();

    }

    public void refreshArcLoaderDrawable(ArcConfiguration configuration) {
        if (isRunning())
            stop();

        // Load Drawable for this View
        mArcDrawable = new ArcDrawable(configuration);
        setBackgroundDrawable(mArcDrawable);

        // Start the Animation
        start();
    }

    // =============================================================================================
    // CLASS
    // =============================================================================================

    /**
     * Created by prathamesh on 16/01/16.
     */
    public static class ArcConfiguration {
        // =============================================================================================
        // CONSTANTS
        // =============================================================================================


        // =============================================================================================
        // FIELDS
        // =============================================================================================

        private Typeface mTypeFace = null;
        private String mText = "Loading..";
        private int mTextSize = 0;
        private int mTextColor = Color.WHITE;

        private int mArcMargin;
        private int mAnimationSpeed;
        private int mStrokeWidth;
        private boolean mArcCircle;
        private int color = Color.BLUE;

        // =============================================================================================
        // CONSTRUCTOR
        // =============================================================================================

        private ArcConfiguration() {
        }

        public ArcConfiguration(Context context) {

            // Default Values
            mArcMargin = MARGIN_MEDIUM;
            mAnimationSpeed = SPEED_MEDIUM;
            mStrokeWidth = (int) context.getResources().getDimension(R.dimen.default_stroke_width);
        }

        //    public ArcConfiguration(Context context) {
        //        this(context);
        //
        //    }

        // =============================================================================================
        // METHODS
        // =============================================================================================

        public int getArcMargin() {
            return mArcMargin;
        }

        public void setArcMargin(int mArcMargin) {
            this.mArcMargin = mArcMargin;
        }

        public int getAnimationSpeed() {
            return mAnimationSpeed;
        }

        public void setAnimationSpeed(int mAnimationSpeed) {
            this.mAnimationSpeed = mAnimationSpeed;
        }

        public void setAnimationSpeedWithIndex(int mAnimationIndex) {

            switch (mAnimationIndex) {
                case 0:
                    this.mAnimationSpeed = 1;
                    break;

                case 1:
                    this.mAnimationSpeed = SPEED_MEDIUM;
                    break;

                case 2:
                    this.mAnimationSpeed = 8;
                    break;

                case 3:
                    this.mAnimationSpeed = 10;
                    break;
            }
        }

        public int getArcWidth() {
            return mStrokeWidth;
        }

        public void setArcWidthInPixel(int mStrokeWidth) {
            this.mStrokeWidth = mStrokeWidth;
        }

        public Typeface getTypeFace() {
            return mTypeFace;
        }

        public void setTypeFace(Typeface typeFace) {
            mTypeFace = typeFace;
        }

        public String getText() {
            return mText;
        }

        public void setText(String mText) {
            this.mText = mText;
        }

        public int getTextSize() {
            return mTextSize;
        }

        public void setTextSize(int size) {
            mTextSize = size;
        }

        public int getTextColor() {
            return mTextColor;
        }

        public void setTextColor(int mTextColor) {
            this.mTextColor = mTextColor;
        }

        public boolean drawCircle() {
            return this.mArcCircle;
        }

        public void drawCircle(boolean drawCircle) {
            this.mArcCircle = drawCircle;
        }

        public int getColor() {
            return color;
        }

        public void setColor(int color) {
            this.color = color;
        }
    }

    private class ArcDrawable extends Drawable implements Animatable {
        final ArcConfiguration mConfiguration;
        Paint mPaint;
        int mStrokeWidth, mArcMargin, mArcAnglePosition, mAnimationSpeed;
        int mArcColor;
        boolean isRunning;
        boolean mDrawCirle;
        boolean isDrawnFully;
        int drawanAngle = 0;
        final Runnable updater = new Runnable() {
            @Override
            public void run() {
                if (!isDrawnFully) {
                    mArcAnglePosition = 0;
                    drawanAngle += mAnimationSpeed;

                    if (drawanAngle >= 360) {
                        isDrawnFully = true;
                    }
                }

                if (isDrawnFully) {
                    drawanAngle -= mAnimationSpeed;
                    mArcAnglePosition += mAnimationSpeed;

                    if (drawanAngle <= 0) {
                        isDrawnFully = false;
                    }
                }

                scheduleSelf(updater, FRAME_DURATION + SystemClock.uptimeMillis());
                invalidateSelf();
            }
        };

        public ArcDrawable(ArcConfiguration configuration) {
            mConfiguration = configuration;

            initComponents();
        }

        private void initComponents() {
            mStrokeWidth = mConfiguration.getArcWidth();
            mArcMargin = mConfiguration.getArcMargin();
            mArcColor = mConfiguration.getColor();
            mAnimationSpeed = mConfiguration.getAnimationSpeed();
            mDrawCirle = mConfiguration.drawCircle();

            mPaint = new Paint();
            mPaint.setAntiAlias(true);
            mPaint.setStrokeWidth(mStrokeWidth);
            mPaint.setStyle(Paint.Style.STROKE);

        }

        @Override
        public void start() {
            if (!isRunning()) {
                // Set the flag
                isRunning = true;

                scheduleSelf(updater, FRAME_DURATION + SystemClock.uptimeMillis());
                invalidateSelf();
            }
        }

        @Override
        public void stop() {
            if (isRunning()) {
                // Set the flag
                isRunning = false;

                unscheduleSelf(updater);
                invalidateSelf();
            }
        }

        @Override
        public boolean isRunning() {
            return isRunning;
        }

        @Override
        public void draw(Canvas canvas) {

            int w = getWidth();
            int h = getHeight();

            int arc1_bound_start = mArcMargin + mStrokeWidth * 2;
            int arc_padding = 0;

            if (mDrawCirle) {
                mPaint.setStyle(Paint.Style.FILL);
                mPaint.setColor(Color.WHITE);
                canvas.drawCircle(w / 2, h / 2, w / 2, mPaint);

                mPaint.setStyle(Paint.Style.STROKE);

                arc_padding += 3;
            }

            RectF arc1_bound = new RectF(arc1_bound_start + arc_padding, arc1_bound_start + arc_padding, ((w - (mStrokeWidth * 2)) - mArcMargin) - arc_padding, ((h - (mStrokeWidth * 2)) - mArcMargin) - arc_padding);
            int i = 0;
            int startangle = (90 * i);
            mPaint.setColor(mArcColor);
            canvas.drawArc(arc1_bound, startangle + mArcAnglePosition, drawanAngle, false, mPaint);
        }

        @Override
        public void setAlpha(int i) {
        }

        @Override
        public void setColorFilter(ColorFilter colorFilter) {
        }

        @Override
        public int getOpacity() {
            return 0;
        }
    }
}
