package com.trulymadly.android.app.facebookalbum;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.crashlytics.android.Crashlytics;
import com.moe.pushlibrary.MoEHelper;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes;
import com.trulymadly.android.app.GetOkHttpRequestParams;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.adapter.FacebookPhotoAdapter;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.Constants.HttpRequestType;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.json.FacebookAlbumResponseParser;
import com.trulymadly.android.app.listener.CustomOkHttpResponseHandler;
import com.trulymadly.android.app.listener.OnActionBarClickedInterface;
import com.trulymadly.android.app.modal.FacebookPhotoItem;
import com.trulymadly.android.app.utility.ActionBarHandler;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.OkHttpHandler;
import com.trulymadly.android.app.utility.UiUtils;

import org.json.JSONObject;

import java.util.Vector;

public class FacebookPhotoActivity extends AppCompatActivity {
    private String access_token = null;
    private String album_id = null;
    private String album_name = null;
    private GridView facebook_album_gridview_id;
    private Vector<FacebookPhotoItem> photos = null;
    private Context aContext;
    private Activity aActivity;
    private ProgressDialog mProgressDialog = null;

    private String trkActivity;
    private MoEHelper mhelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        try {
            setContentView(R.layout.facebook_photo);
        } catch (OutOfMemoryError e) {
            finish();
            return;
        }
        mhelper = new MoEHelper(this);
        facebook_album_gridview_id = (GridView) findViewById(R.id.facebook_photo_gridview_id);

        aContext = this;
        aActivity = this;
        Intent i = getIntent();
        Bundle b = i.getExtras();
        if (b != null) {
            access_token = b.getString("ACCESS_TOKEN");
            album_id = b.getString("ALBUM_ID");
            album_name = b.getString("ALBUM_NAME");
            trkActivity = b.getString("trkActivity");
        }

        if (album_id != null && access_token != null) {
            new ActionBarHandler(this, album_name, null,
                    new OnActionBarClickedInterface() {
                        @Override
                        public void onBackClicked() {
                            onBackPressed();
                        }

                        @Override
                        public void onUserProfileClicked() {

                        }

                        @Override
                        public void onConversationsClicked() {
                        }


                        @Override
                        public void onCuratedDealsClicked() {

                        }

                        @Override
                        public void onLocationClicked() {
                        }

                        @Override
                        public void onTitleClicked() {

                        }

                        @Override
                        public void onTitleLongClicked() {

                        }

                        @Override
                        public void onSearchViewOpened() {

                        }

                        @Override
                        public void onSearchViewClosed() {

                        }

                        @Override
                        public void onCategoriesClicked() {

                        }
                    }, false,
                    false, false, false, false);
            fetchFacebookPhotos();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void fetchFacebookPhotos() {
        mProgressDialog = UiUtils.showProgressBar(aContext, mProgressDialog);

        CustomOkHttpResponseHandler responseHandler = new CustomOkHttpResponseHandler(aContext) {

            @Override
            public void onRequestSuccess(JSONObject response) {
                mProgressDialog = UiUtils.hideProgressBar(mProgressDialog);
                if (response != null) {
                    showFacebookPhotos(response);
                }
            }

            @Override
            public void onRequestFailure(Exception exception) {
                mProgressDialog = UiUtils.hideProgressBar(mProgressDialog);
                AlertsHandler.showNetworkError(aActivity, exception);

            }
        };
        OkHttpHandler.httpGet(aContext,
                (Constants.FACEBOOK_URL_PHOTOS).replace("[ACCESS_TOKEN]", access_token).replace("[ALBUM_ID]", album_id),
                responseHandler);
    }

    private void showFacebookPhotos(JSONObject response) {
        try {
            photos = FacebookAlbumResponseParser.parsePhotos(response);
            FacebookPhotoAdapter adapter = new FacebookPhotoAdapter(aContext, photos);
            facebook_album_gridview_id.setAdapter(adapter);
            facebook_album_gridview_id.setOnItemClickListener(new OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                    int w, h;
                    w = Integer.parseInt(photos.elementAt(position).getWidth());
                    h = Integer.parseInt(photos.elementAt(position).getHeight());
                    if (w < 200 || h < 200) {
                        AlertsHandler.showMessage(aActivity, R.string.min_photos_size);
                    } else {
                        showPopUpForFacebookPhotoSelect(photos.elementAt(position).getSourceUrl() + " " + w + " " + h);
                    }
                }
            });
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
    }

    private void showPopUpForFacebookPhotoSelect(String url) {
        sendFBPhotoToServer(url);
    }

    private void sendFBPhotoToServer(String clickedUrl) {
        mProgressDialog = UiUtils.showProgressBar(aContext, mProgressDialog);

        CustomOkHttpResponseHandler responseHandler = new CustomOkHttpResponseHandler(
                aContext, trkActivity, TrulyMadlyEventTypes.fb_photo_save) {

            @Override
            public void onRequestSuccess(JSONObject response) {
                mProgressDialog = UiUtils.hideProgressBar(mProgressDialog);
                finishWithResult(response.toString());

            }

            @Override
            public void onRequestFailure(Exception exception) {
                mProgressDialog = UiUtils.hideProgressBar(mProgressDialog);
                AlertsHandler.showNetworkError(aActivity, exception);
            }
        };

        OkHttpHandler.httpPost(aContext, ConstantsUrls.get_photos_url(),
                GetOkHttpRequestParams.getHttpRequestParams(HttpRequestType.FB_PHOTO_SELECT, clickedUrl),
                responseHandler);

    }

    private void finishWithResult(String response) {
        Bundle conData = new Bundle();
        conData.putString("response", response);
        Intent intent = new Intent();
        intent.putExtras(conData);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

}
