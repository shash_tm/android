package com.trulymadly.android.app.facebookalbum;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.crashlytics.android.Crashlytics;
import com.moe.pushlibrary.MoEHelper;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.adapter.FacebookAlbumAdapter;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.FacebookAlbumResponseParser;
import com.trulymadly.android.app.listener.CustomOkHttpResponseHandler;
import com.trulymadly.android.app.listener.OnActionBarClickedInterface;
import com.trulymadly.android.app.modal.FacebookAlbumItem;
import com.trulymadly.android.app.utility.ActionBarHandler;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.OkHttpHandler;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import static com.google.common.base.Preconditions.checkNotNull;

public class FacebookAlbumActivity extends AppCompatActivity {
    final private int GET_FACEBOOK_PHOTO = 1;
    private String access_token = null;
    private GridView facebook_album_gridview_id;
    private Vector<FacebookAlbumItem> albums = null;
    private Context aContext;
    private Activity aActivity;
    private ProgressDialog mProgressDialog = null;

    private String trkActivity;
    private MoEHelper mhelper = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        mhelper = new MoEHelper(this);
        setContentView(R.layout.facebook_album);
        facebook_album_gridview_id = (GridView) findViewById(R.id.facebook_album_gridview_id);

        aContext = this;
        aActivity = this;
        Intent i = getIntent();
        Bundle b = i.getExtras();
        if (b != null) {
            access_token = b.getString("ACCESS_TOKEN");
            trkActivity = b.getString("trkActivity");
        }

        new ActionBarHandler(this, getResources().getString(R.string.facebook_albums), null,
                new OnActionBarClickedInterface() {
                    @Override
                    public void onBackClicked() {
                        onBackPressed();
                    }

                    @Override
                    public void onUserProfileClicked() {

                    }

                    @Override
                    public void onConversationsClicked() {
                    }

                    @Override
                    public void onLocationClicked() {
                    }


                    @Override
                    public void onCuratedDealsClicked() {

                    }

                    @Override
                    public void onTitleClicked() {

                    }

                    @Override
                    public void onTitleLongClicked() {

                    }

                    @Override
                    public void onSearchViewOpened() {

                    }

                    @Override
                    public void onSearchViewClosed() {

                    }

                    @Override
                    public void onCategoriesClicked() {

                    }
                }, false,
                false, false, false, false);

        if (access_token != null)
            fetchFacebookAlbums();

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void fetchFacebookAlbums() {
        mProgressDialog = UiUtils.showProgressBar(aContext, mProgressDialog);

        CustomOkHttpResponseHandler okHttpResponseHandler = new CustomOkHttpResponseHandler(aContext) {
            @Override
            public void onRequestSuccess(JSONObject response) {
                getCoverPics(response);
            }

            @Override
            public void onRequestFailure(Exception exception) {
                mProgressDialog = UiUtils.hideProgressBar(mProgressDialog);
                AlertsHandler.showNetworkError(aActivity, exception);
            }
        };
        OkHttpHandler.httpGet(aContext, (Constants.FACEBOOK_URL_ALBUMS).replace("[ACCESS_TOKEN]", access_token),
                okHttpResponseHandler);
    }

    private void getCoverPics(JSONObject response) {
        try {
            albums = FacebookAlbumResponseParser.parseAlbums(response);
            JSONArray batchRequest = new JSONArray();
            for (FacebookAlbumItem album : albums) {
                String coverPhotoId = checkNotNull(album).getCover_photo_id();
                if (Utility.isSet(coverPhotoId)) {
                    JSONObject batchItem = new JSONObject();
                    batchItem.put("method", "GET");
                    batchItem.put("relative_url", coverPhotoId);
                    batchRequest.put(batchItem);
                }
            }

            CustomOkHttpResponseHandler responseHandler = new CustomOkHttpResponseHandler(aContext) {

                @Override
                public void onRequestSuccess(JSONObject responseJSON) {

                }

                @Override
                public void onRequestFailure(Exception exception) {
                    mProgressDialog = UiUtils.hideProgressBar(mProgressDialog);
                    showFacebookAlbums();
                }

                @Override
                public void onRequestSuccess(JSONArray response) {
                    mProgressDialog = UiUtils.hideProgressBar(mProgressDialog);
                    for (int i = 0; i < response.length(); i++) {
                        JSONObject obj;
                        try {
                            obj = response.getJSONObject(i);
                            JSONObject body = new JSONObject(obj.getString("body"));

                            String id = body.optString("id");
                            String picture_url = body.optString("source");
                            for (FacebookAlbumItem album : albums) {
                                if (album.getCover_photo_id() != null
                                        && album.getCover_photo_id().equalsIgnoreCase(id)) {
                                    album.setCover_url(picture_url);
                                    break;
                                }
                            }
                        } catch (JSONException e) {
                            Crashlytics.logException(e);
                        }
                    }
                    showFacebookAlbums();
                }
            };

            Map<String, String> params = new HashMap<>();
            params.put("access_token", access_token);
            params.put("batch", batchRequest.toString());
            params.put("include_headers", "false");
            OkHttpHandler.httpPost(aContext, "https://graph.facebook.com", params, responseHandler);

        } catch (JSONException e) {
            Crashlytics.logException(e);
        }

    }

    private void showFacebookAlbums() {
        FacebookAlbumAdapter adapter = new FacebookAlbumAdapter(aContext, albums);
        facebook_album_gridview_id.setAdapter(adapter);
        facebook_album_gridview_id.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                Intent i = new Intent(FacebookAlbumActivity.this, FacebookPhotoActivity.class);
                i.putExtra("trkActivity", trkActivity);
                i.putExtra("ACCESS_TOKEN", access_token);
                i.putExtra("ALBUM_ID", albums.elementAt(position).getId());
                i.putExtra("ALBUM_NAME", albums.elementAt(position).getName());
                startActivityForResult(i, GET_FACEBOOK_PHOTO);
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GET_FACEBOOK_PHOTO) {
            if (resultCode == Activity.RESULT_OK) {
                finishWithResult(data.getExtras().getString("response"));
            }
        }
    }

    private void finishWithResult(String response) {
        Bundle conData = new Bundle();
        conData.putString("response", response);
        Intent intent = new Intent();
        intent.putExtras(conData);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
