/**
 * 
 */
package com.trulymadly.android.app;

import android.os.Bundle;

import com.trulymadly.android.app.json.Constants.EditProfilePageType;

/**
 * @author udbhav
 *
 */
public interface EditProfileActionInterface {
	void processFragmentSaved(EditProfilePageType currentPageType, Bundle responseData);

	void setHeaderText(String headerText);

	void onViewCreated();
}
