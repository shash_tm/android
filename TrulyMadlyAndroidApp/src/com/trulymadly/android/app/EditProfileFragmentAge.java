/**
 *
 */
package com.trulymadly.android.app;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.moe.pushlibrary.MoEHelper;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.Constants.EditProfilePageType;
import com.trulymadly.android.app.json.ConstantsCities;
import com.trulymadly.android.app.listener.OnHashtagSelected;
import com.trulymadly.android.app.modal.Height;
import com.trulymadly.android.app.modal.UserData;
import com.trulymadly.android.app.utility.AlertsHandler;
import com.trulymadly.android.app.utility.CityUtils;
import com.trulymadly.android.app.utility.TimeUtils;
import com.trulymadly.android.app.utility.UiUtils;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import butterknife.Unbinder;

/**
 * @author udbhav
 */
public class EditProfileFragmentAge extends Fragment {

    private static final EditProfilePageType currentPageType = EditProfilePageType.AGE;
    public boolean popularCitiesVisible;
    @BindView(R.id.register_fname)
    TextView register_fname;
    @BindView(R.id.register_lname)
    TextView register_lname;
    @BindView(R.id.birthdate_text_edit)
    TextView dob_tv;
    @BindView(R.id.continue_age)
    Button continueButton;
    @BindView(R.id.height_spinner)
    Spinner heightSpinner;
    @BindView(R.id.city_autocomplete_tv)
    AutoCompleteTextView cityAutocompleteTv;
    @BindView(R.id.popular_cities_container)
    LinearLayout popular_cities_container;
    @BindView(R.id.popular_cities_layout)
    LinearLayout popular_cities_layout;
    @BindView(R.id.non_city_items_container)
    View non_city_items_container;
    private String fname, lname;
    private String countryId = null, stateId = null, cityId = null, height = null;
    private boolean isAuthentic = false;
    private Activity aActivity;
    private EditProfileActionInterface editProfileActionInterface;
    private String status;
    private long startTime;
    private UserData userData;
    private OnHashtagSelected citySelectedListener;
    private int dob_d, dob_m, dob_y;
    private Unbinder unbinder;
    private boolean isCityViewExpanded = false;
    private ArrayAdapter cityAutoCompleteAdapterNew;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            editProfileActionInterface = (EditProfileActivity) activity;
        } catch (ClassCastException e) {
            Crashlytics.logException(e);
            /** The activity does not implement the listener. */
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        aActivity = getActivity();

        if (savedInstanceState != null) {
            return;
        }

        Bundle data = getArguments();
        fname = data.getString("fname");
        lname = data.getString("lname");
        status = data.getString("status");
        dob_d = data.getInt("dob_d");
        dob_m = data.getInt("dob_m");
        dob_y = data.getInt("dob_y");
        height = data.getString("height");
        countryId = data.getString("location_country_id");
        stateId = data.getString("location_state_id");
        cityId = data.getString("location_city_id");
        if (!Utility.isSet(cityId)) {
            cityId = Constants.LOCATION_OTHER;
        }
        userData = (UserData) data.getSerializable("userData");

        JSONObject newJson = new JSONObject();
        try {
            newJson.put("User ID", Utility.getMyId(aActivity));

        } catch (JSONException ignored) {
        }
        MoEHelper.getInstance(aActivity).trackEvent("Age Screen Launched", newJson);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        editProfileActionInterface.setHeaderText(getResources().getString(R.string.header_age));
        View view = null;
        try {
            view = inflater.inflate(R.layout.edit_profile_age, container, false);
        } catch (OutOfMemoryError e) {
            System.gc();
            aActivity.finish();
            return view;
        }
        unbinder = ButterKnife.bind(this, view);


        if (dob_d != 0) {
            Date date = new Date(dob_y - 1900, dob_m - 1, dob_d);
            dob_tv.setText(TimeUtils.getFormattedDate(date));
        }

        continueButton.setText(R.string.save);
        register_fname.setText(fname);
        register_lname.setText(lname);
        isAuthentic = (status != null && status.equalsIgnoreCase("authentic"));


        cityAutocompleteTv.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                expandCityView();
                return false;
            }
        });


        citySelectedListener = new OnHashtagSelected() {
            @Override
            public void onHashtagSelected(String s) {

                if (s == null || s.trim().length() == 0) {
                    cityAutocompleteTv.setText("");
                    cityAutocompleteTv.dismissDropDown();
                } else {
                    popularCitiesVisible = CityUtils.hidePopularCities(popularCitiesVisible, popular_cities_container);
                    cityAutocompleteTv.setText(s);
                    cityAutocompleteTv.setSelection(s.length());
                    cityAutocompleteTv.dismissDropDown();
                    HashMap<String, String> map = CityUtils.getCityAndStateId(s);
                    cityId = map.get("cityId");
                    stateId = map.get("stateId");
                    closeExpandedCityView();
                }

            }
        };

        cityAutocompleteTv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                citySelectedListener.onHashtagSelected(parent.getAdapter().getItem(position).toString());
            }
        });

        cityAutoCompleteAdapterNew = new ArrayAdapter<>(aActivity, R.layout.city_suggestion_item, R.id.city_suggestion_item_tv, ConstantsCities.cityNames);
        cityAutocompleteTv.setAdapter(cityAutoCompleteAdapterNew);
        cityAutocompleteTv.setThreshold(1);

        createCityName();

        cityAutocompleteTv.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_ENTER) {
                        if (Utility.isSet(cityAutocompleteTv.getText().toString()) && cityAutoCompleteAdapterNew.getCount() > 0) {
                            citySelectedListener.onHashtagSelected(cityAutoCompleteAdapterNew.getItem(0).toString());
                        } else {
                            citySelectedListener.onHashtagSelected(null);
                        }
                        return true;
                    } else {
                        if (keyCode != KeyEvent.KEYCODE_BACK) {
                            expandCityView();
                        }
                        return false;
                    }
                }
                return false;
            }
        });
        cityAutocompleteTv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 0) {
                    popularCitiesVisible = CityUtils.showPopularCities(popularCitiesVisible, isCityViewExpanded, aActivity, popular_cities_container, cityAutocompleteTv);
                } else {
                    popularCitiesVisible = CityUtils.hidePopularCities(popularCitiesVisible, popular_cities_container);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        cityAutocompleteTv.setDropDownHorizontalOffset(UiUtils.dpToPx(-70));
        cityAutocompleteTv.setDropDownVerticalOffset(UiUtils.dpToPx(32));
        cityAutocompleteTv.setDropDownWidth((int) UiUtils.getScreenWidth(aActivity));


        View.OnClickListener cityClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                citySelectedListener.onHashtagSelected(view.getTag().toString());
            }
        };
        popular_cities_layout.removeAllViews();
        for (String city : ConstantsCities.popularCityNames) {
            View cityView;
            cityView = inflater.inflate(R.layout.city_suggestion_item, null);
            ((TextView) cityView.findViewById(R.id.city_suggestion_item_tv)).setText(city);
            popular_cities_layout.addView(cityView);
            cityView.setTag(city);
            cityView.setOnClickListener(cityClickListener);
        }


        View.OnTouchListener onHeightTouchListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                popularCitiesVisible = CityUtils.hidePopularCities(popularCitiesVisible, popular_cities_container);
                if (cityAutocompleteTv != null)
                    cityAutocompleteTv.dismissDropDown();
                return false;
            }
        };

        String heightId = "0";
        if (Utility.isSet(height)) {
            heightId = "" + (Integer.parseInt(height) - 48);
        }
        CityUtils.createHeightSpinner(Height.getHeightArray(getResources()), aActivity, heightSpinner, heightId, onHeightTouchListener);

        setStartTime();
        return view;
    }

    private void expandCityView() {
        if (!isCityViewExpanded) {
            isCityViewExpanded = true;
            continueButton.setVisibility(View.GONE);
            non_city_items_container.setVisibility(View.GONE);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    popularCitiesVisible = CityUtils.showPopularCities(popularCitiesVisible, isCityViewExpanded, aActivity, popular_cities_container, cityAutocompleteTv);
                }
            }, 500);

        }
    }

    private void closeExpandedCityView() {
        if (isCityViewExpanded) {
            isCityViewExpanded = false;
            HashMap<String, String> map = CityUtils.getCityAndStateId(cityAutocompleteTv.getText().toString());
            cityId = map.get("cityId");
            stateId = map.get("stateId");
            validateAgeData(false);
            continueButton.setVisibility(View.VISIBLE);
            non_city_items_container.setVisibility(View.VISIBLE);
            popularCitiesVisible = CityUtils.hidePopularCities(popularCitiesVisible, popular_cities_container);
            cityAutocompleteTv.dismissDropDown();
            UiUtils.hideKeyBoard(aActivity);
            cityAutocompleteTv.setHint(R.string.city);
        }
    }

    @OnItemSelected(R.id.height_spinner)
    public void onHeightSelected() {
        popularCitiesVisible = CityUtils.hidePopularCities(popularCitiesVisible, popular_cities_container);
        height = ((Height) heightSpinner.getSelectedItem()).getId();
        validateAgeData(false);
    }

    @OnItemSelected(value = R.id.height_spinner, callback = OnItemSelected.Callback.NOTHING_SELECTED)
    public void onNothingSelected() {
        popularCitiesVisible = CityUtils.hidePopularCities(popularCitiesVisible, popular_cities_container);
    }

    private boolean validateAgeData(Boolean showError) {
        Boolean isValid = true;
        int errorMessage = 0;
        if (!Utility.isSet(fname)) {
            isValid = false;
            errorMessage = R.string.enter_first_name;
        }
        if (!isAuthentic && !Utility.isSet(lname)) {
            isValid = false;
            errorMessage = R.string.enter_last_name;
        }
        if (countryId == null || countryId.equalsIgnoreCase("-1") || countryId.equalsIgnoreCase("0")) {
            isValid = false;
            errorMessage = R.string.please_select_country;
        } else if (stateId == null || stateId.equalsIgnoreCase("-1") || stateId.equalsIgnoreCase("0")) {
            isValid = false;
            errorMessage = R.string.please_select_state;
        } else if (cityId == null || cityId.equalsIgnoreCase("-1") || cityId.equalsIgnoreCase("0") || cityId.equalsIgnoreCase(Constants.LOCATION_OTHER)) {
            isValid = false;
            errorMessage = R.string.please_select_city;
        }
        if (height == null || height.equals("-1") || height.equalsIgnoreCase("Height")) {
            isValid = false;
            errorMessage = R.string.enter_height;
        }
        if (!isValid && showError) {
            AlertsHandler.showMessage(aActivity, errorMessage);
        }
        continueButton.setEnabled(isValid);
        return isValid;
    }

    public void onBackPressed() {
        if (isCityViewExpanded) {
            closeExpandedCityView();
        }
    }

    public boolean isCityViewExpanded() {
        return isCityViewExpanded;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        editProfileActionInterface.onViewCreated();
    }

    private void setStartTime() {
        startTime = (new java.util.Date()).getTime();
    }

    private void setEndTime() {
        long endTime = (new Date()).getTime();
        int time_diff = (int) (endTime - startTime) / 1000;
        TrulyMadlyTrackEvent.trackEvent(aActivity, TrulyMadlyActivities.edit_profile,
                TrulyMadlyEventTypes.regiter_basics, time_diff, null, null, true);
    }


    @OnClick(R.id.continue_age)
    public void onContinueClick() {
        if (validateAgeData(false)) {
            Bundle responseData = new Bundle();
            responseData.putString("fname", fname);
            responseData.putString("lname", lname);
            responseData.putInt("dob_d", dob_d);
            responseData.putInt("dob_m", dob_m);
            responseData.putInt("dob_y", dob_y);
            responseData.putString("location_country_id", countryId);
            responseData.putString("location_state_id", stateId);
            responseData.putString("location_city_id", cityId);
            responseData.putString("height", height);
            responseData.putSerializable("userData", userData);

            setEndTime();
            editProfileActionInterface.processFragmentSaved(currentPageType, responseData);
        }
    }


    private void createCityName() {


        if (Utility.isSet(cityId)) {


            if (cityId.equalsIgnoreCase(Constants.LOCATION_OTHER)) {
                cityAutocompleteTv.setText(aActivity.getResources().getString(R.string.others));
                cityAutocompleteTv.setSelection(cityAutocompleteTv.getText().length());
                cityAutocompleteTv.dismissDropDown();
                AlertsHandler.showMessage(aActivity, R.string.select_city_message);
            } else {
                int index = Arrays.asList(ConstantsCities.cityIds).indexOf(cityId);
                if (index != -1) {
                    cityAutocompleteTv.setText(ConstantsCities.cityNames[index]);
                    cityAutocompleteTv.setSelection(cityAutocompleteTv.getText().length());
                    cityAutocompleteTv.dismissDropDown();
                }
            }
        }

    }

}
