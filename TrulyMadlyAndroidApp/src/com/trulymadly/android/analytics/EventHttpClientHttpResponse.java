package com.trulymadly.android.analytics;

public class EventHttpClientHttpResponse {

	private int code;
	private boolean badPayload;

	public EventHttpClientHttpResponse() {
		code = -1;
		badPayload = false;
	}

	public boolean isSuccessful() {
		return code >= 200 && code < 300;
	}

	public boolean isRejectedPermanently() {
		return code >= 400 && code < 500;
	}

	public boolean isRejectedTemporarily() {
		return !(isSuccessful() || isRejectedPermanently());
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public boolean isBadPayload() {
		return badPayload;
	}

	public void setBadPayload(boolean badPayload) {
		this.badPayload = badPayload;
	}
}
