package com.trulymadly.android.analytics;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.HitBuilders.EventBuilder;
import com.google.android.gms.analytics.HitBuilders.TimingBuilder;
import com.google.android.gms.analytics.Tracker;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.TrulyMadlyApplication;
import com.trulymadly.android.app.TrulyMadlyApplication.TrackerName;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.Constants.AlarmRequestStatus;
import com.trulymadly.android.app.json.ConstantsRF;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.modal.GaEventModal;
import com.trulymadly.android.app.receivers.GaDeQueueAlarmReceiver;
import com.trulymadly.android.app.sqlite.GaQueueDBHandler;
import com.trulymadly.android.app.sqlite.RFHandler;
import com.trulymadly.android.app.utility.ABHandler;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.Utility;

import java.util.Calendar;
import java.util.ConcurrentModificationException;

public class GAEventTrackerV4 {

    private static final String PROPERTY_ID = Constants.GA_API_ID;
    private static final int GA_QUEUE_DELAY_SECONDS = 60;
    public static final int GA_QUEUE_MAX_ITEMS_TO_SENT = GA_QUEUE_DELAY_SECONDS
            / 2;
    private static final int GA_QUEUE_DELAY = GA_QUEUE_DELAY_SECONDS * 1000;

    public static void createGaEvent(Context aContext, String event_category,
                                     String event_action, String event_label, long time_taken_millis) {

        String gender = SPHandler.getString(aContext,
                ConstantsSP.SHARED_KEYS_USER_GENDER);
        String city = SPHandler.getString(aContext,
                ConstantsSP.SHARED_KEYS_USER_CITY);
        String age = SPHandler.getString(aContext,
                ConstantsSP.SHARED_KEYS_USER_AGE);
        String userId = Utility.getMyId(aContext);
        String networkClass = Utility.getNetworkClass(aContext);

        String ab_prefix = ABHandler.getABPrefix(aContext);

        GaEventModal gaEventModal = new GaEventModal(event_category,
                event_action, event_label, time_taken_millis, gender, city, age,
                userId, networkClass, ab_prefix);

        GaQueueDBHandler.insertGaItem(aContext, gaEventModal);

        checkGAQueueAlarmAndSet(aContext);
        //sendEventToGA(aContext, gaEventModal);

    }

    public static void sendEventToGA(Context aContext,
                                     GaEventModal gaEventModal) {
        TimingBuilder timedEvent = null;
        try {

            String gender = gaEventModal.get("gender");
            String city = gaEventModal.get("city");
            String age = gaEventModal.get("age");
            String userId = gaEventModal.get("userId");
            String networkClass = gaEventModal.get("networkClass");
            String ab_prefix = gaEventModal.get("ab_prefix");

            // Build and send a Timing hit
            int time_taken_millis = 0;
            try {
                time_taken_millis = Integer
                        .parseInt(gaEventModal.get("time_taken_millis"));
            } catch (NumberFormatException ignored) {

            }
            if (time_taken_millis > 0) {
                timedEvent = new HitBuilders.TimingBuilder()
                        .setValue(time_taken_millis)
                        .setCategory(gaEventModal.get("event_category"))
                        .setVariable(gaEventModal.get("event_action"))
                        .setLabel(gaEventModal.get("event_label"));
            }

            // Build and send an Event.
            EventBuilder event = new HitBuilders.EventBuilder(
                    gaEventModal.get("event_category"),
                    gaEventModal.get("event_action"))
                    .setLabel(gaEventModal.get("event_label"));

            if (time_taken_millis > 0) {
                event.setValue((long) (time_taken_millis / 1000));
            }

            Tracker t = getTracker(aContext);
            if (Utility.isSet(gender)) {
                event.setCustomDimension(1, gender);
                if (timedEvent != null)
                    timedEvent.setCustomDimension(1, gender);
            }

            if (Utility.isSet(city)) {
                event.setCustomDimension(2, city);
                if (timedEvent != null)
                    timedEvent.setCustomDimension(2, city);
            }

            if (Utility.isSet(age)) {
                event.setCustomDimension(3, age);
                if (timedEvent != null)
                    timedEvent.setCustomDimension(3, age);
            }

            if (Utility.isSet(userId)) {
                t.set("&uid", userId);
                event.setCustomDimension(4, userId);
                if (timedEvent != null)
                    timedEvent.setCustomDimension(4, userId);
            }

            if (Utility.isSet(networkClass)) {
                event.setCustomDimension(5, networkClass);
                if (timedEvent != null)
                    timedEvent.setCustomDimension(5, networkClass);
            }


            if (Utility.isSet(ab_prefix)) {
                event.setCustomDimension(6, ab_prefix);
                if (timedEvent != null)
                    timedEvent.setCustomDimension(6, ab_prefix);
            }

            t.send(event.build());
            if (timedEvent != null) {
                t.send(timedEvent.build());
            }
        } catch (ConcurrentModificationException | IncompatibleClassChangeError e) {
            Crashlytics.logException(e);
        }
    }

    private synchronized static Tracker getTracker(Context aContext) {
        TrackerName trackerId = TrackerName.APP_TRACKER;
        if (!TrulyMadlyApplication.mTrackers.containsKey(trackerId)) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(aContext);
            Tracker t = (trackerId == TrackerName.APP_TRACKER)
                    ? analytics
                    .newTracker(Constants.isLive ? R.xml.app_tracker_live
                            : (Constants.isT1 ? R.xml.app_tracker_t1
                            : R.xml.app_tracker_dev))
                    : analytics.newTracker(PROPERTY_ID);
            TrulyMadlyApplication.mTrackers.put(trackerId, t);
        }
        return TrulyMadlyApplication.mTrackers.get(trackerId);
    }

    public static void checkGAQueueAlarmAndSet(Context ctx) {
        if (TrulyMadlyApplication.lastGaStartTime == 0 || Calendar.getInstance()
                .getTimeInMillis()
                - TrulyMadlyApplication.lastGaStartTime > GA_QUEUE_DELAY) {
            AlarmRequestStatus alarmStatus = Utility.getAlarmRequestStatus(ctx,
                    Constants.NULL_USER_ID, ConstantsRF.RIGID_FIELD_GA_QUEUE_TIMESTAMP,
                    GA_QUEUE_DELAY);
            switch (alarmStatus) {
                case EXPIRED:
                case NOT_SET:
                    GAEventTrackerV4.generateGaQueueAlarm(ctx);
                case IN_PROGRESS:
                default:
                    break;
            }
        }
    }

    private static void generateGaQueueAlarm(Context ctx) {
        long currentTime = Calendar.getInstance()
                .getTimeInMillis();
        TrulyMadlyApplication.lastGaStartTime = currentTime;
        RFHandler.insert(ctx, Constants.NULL_USER_ID,
                ConstantsRF.RIGID_FIELD_GA_QUEUE_TIMESTAMP,
                String.valueOf(currentTime));
        PendingIntent alarmIntent;
        Intent intent = new Intent(ctx, GaDeQueueAlarmReceiver.class);
        alarmIntent = PendingIntent.getBroadcast(ctx, 0, intent, 0);
        Utility.generateAlarm(ctx, alarmIntent, GA_QUEUE_DELAY);
    }

}
