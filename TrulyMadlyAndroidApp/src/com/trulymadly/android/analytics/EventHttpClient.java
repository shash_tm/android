package com.trulymadly.android.analytics;

import android.content.Context;

import com.trulymadly.android.app.utility.OkHttpHandler;
import com.trulymadly.android.app.utility.TmLogger;
import com.trulymadly.android.app.utility.Utility;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.trulymadly.android.app.utility.OkHttpHandler.createHttpPostFormBody;

public class EventHttpClient {

    private static final String USER_AGENT_STRING = "NETWORK : ";
    private static final int DEFAULT_HTTP_CONNECT_TIMEOUT = 30;
    private static final int DEFAULT_HTTP_SOCKET_TIMEOUT = 30;

    public static EventHttpClientHttpResponse performHttpRequest(String uri, String body, Context appContext) {

        EventHttpClientHttpResponse ret = new EventHttpClientHttpResponse();
        OkHttpClient client = new OkHttpClient();


        String tag = "EVENT_HTTP_CLIENT";
        try {
            OkHttpClient.Builder clientBuilder = OkHttpHandler.getOkHttpBuilder(DEFAULT_HTTP_CONNECT_TIMEOUT, DEFAULT_HTTP_SOCKET_TIMEOUT, 0);
            client = clientBuilder.build();

            //connection = (HttpURLConnection) (new URL(uri)).openConnection();
            Request.Builder builder = new Request.Builder().url(uri);
            builder.addHeader("login_mobile", "true");
//			connection.setRequestProperty("User-Agent",
//                    USER_AGENT_STRING + appContext != null ? Utility.getNetworkClass(appContext) : "N/A");
            builder.addHeader("User-Agent", USER_AGENT_STRING + appContext != null ? Utility.getNetworkClass(appContext) : "N/A");
            //connection.setRequestProperty("Accept", "application/json");
            builder.addHeader("Accept", "application/json");

//            connection.setRequestMethod("POST");
//            connection.setDoOutput(true);
//            String urlParameters = "data=" + URLEncoder.encode(body, "UTF-8") + "&content_type="
//                    + URLEncoder.encode("application/json", "UTF-8");
//            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
//            wr.writeBytes(urlParameters);
//            wr.flush();
//            wr.close();
            Map<String, String> params = new HashMap<>();
            params.put("data", body);
            params.put("content_type", "application/json");
            Request request = builder.post(createHttpPostFormBody(params)).build();

            //connection.connect();
            Response response = client.newCall(request).execute();

            //int code = connection.getResponseCode();
            int code = response.code();

            ret.setCode(code);

            if (response.body() != null) {
                response.body().close();
            }

        } catch (IllegalStateException | IllegalArgumentException e) {
            TmLogger.e(tag, "Error communicating with server.", e);
        } catch (SocketTimeoutException e) {
            TmLogger.e(tag, "Timeout communicating with server.");
        } catch (IOException e) {
            TmLogger.e(tag, "Error communicating with server.", e);
        }
        return ret;
    }

    public enum Method {
        GET, PUT, POST
    }

}
