package com.trulymadly.android.analytics;

import android.app.Activity;
import android.content.Context;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.trulymadly.android.app.asynctasks.TrackEventToFbAsyncTask;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.sqlite.EventTrackingDBHandler;
import com.trulymadly.android.app.utility.TmLogger;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONException;

import java.util.HashMap;
import java.util.Map;

public class TrulyMadlyTrackEvent {

	private static final String tag = "TRULY_MADLY_TRACK_EVENT";
	// private MixpanelAPI mixpanel;
	// private Context context;
	private static boolean initialized = false;

	private TrulyMadlyTrackEvent() {
		// this.context=context;
		// mixpanel=MixpanelAPI.getInstance(context, Constants.MIXPANEL_API_ID);
	}

	public static void onStop(Context context) {

		// For GA
		try {
			GoogleAnalytics.getInstance(context)
					.reportActivityStop((Activity) context);
		} catch (Exception e) {
            TmLogger.w(tag, "Error stopping GATracker.", e);
        }

		try {
			EventSendWorker.activityStopped();
		} catch (Exception e) {
            TmLogger.w(tag, "Error stopping Activity.", e);
        }
    }

	public static void onStart(Context context) {

		// for GA
		try {
			GoogleAnalytics.getInstance(context)
					.reportActivityStart((Activity) context);

		} catch (Exception e) {
            TmLogger.w(tag, "Error starting GATracker.", e);
        }

		try {
			init();
			EventSendWorker.activityStarted(context.getApplicationContext());
		} catch (Exception e) {
            TmLogger.w(tag, "Error starting  Activity.", e);
        }

	}

	private static void init() {
		if (!initialized) {
			synchronized (TrulyMadlyTrackEvent.class) {
				if (!initialized) {
					initialized = true;
				}
			}
		}
	}

	/**
	 * Track event in action log and GA
	 */
//	public static void trackEvent(Context context, String activity,
//                                  String event_type, long time_taken_millis, String event_status,
//                                  Map<String, String> event_info, boolean sendToActionLog) {
//		trackEvent(context, activity, event_type, time_taken_millis,
//				event_status, event_info, true);
//	}

	/**
	 * Track event in action log optional and GA with label optional
	 */
	public static void trackEvent(Context context, String activity,
			String event_type, long time_taken_millis, String event_status,
								  Map<String, String> event_info,
								  boolean sendToActionLog) {
		trackEvent(context, activity, event_type, time_taken_millis,
				event_status, event_info, sendToActionLog, true, true);
	}

	public static void trackEvent(Context context, String activity,
								  String event_type, long time_taken_millis, String event_status,
								  Map<String, String> event_info,
								  boolean sendToActionLog, boolean sendToGAOptional) {
		trackEvent(context, activity, event_type, time_taken_millis,
				event_status, event_info, sendToActionLog, true, sendToGAOptional);
	}

	/**
	 * Track event in action log optional and GA and to facebook optional
	 */
	public static void trackEvent(final Context context, String activity,
			final String event_type, long time_taken_millis,
								   String event_status, Map<String, String> event_info, boolean sendToActionLog,
								  boolean sendToFacebook, boolean sendToGA) {
		Event event = null;
		try {
			final float time_taken_sec = (float) (time_taken_millis / 1000.0);
			// for Google Tracker
			if (sendToGA) {
				if (Utility.isSet(event_status)) {
					trackEventGA(context, activity, event_type, event_status,
							time_taken_millis);
				} else {
					trackEventGA(context, activity, event_type, Constants.GA_LABEL,
							time_taken_millis);
				}
			}

			if (sendToFacebook) {
				TrackEventToFbAsyncTask.trackEvent(context, event_type, activity, event_status, time_taken_sec);
			}

			if (sendToActionLog && event_type != null) {
				// for server side logging
                TmLogger.v(tag,
                        "Tracking Event " + " : " + activity + " : "
                                + event_type + " : " + time_taken_sec + " : "
								+ event_status);
				if (event_info == null) {
					event_info = new HashMap<>();
				}
				event_info.put("NetworkClass",
						Utility.getNetworkClass(context));
				event = new Event(context, activity, event_type, event_status,
						event_info, time_taken_sec);
				EventTrackingDBHandler.getInstance(context).addPayLoad(event);
				EventSendWorker.ensureRunning(context);
			}

		} catch (JSONException ignored) {
		}

	}

	/**
	 * Track event to GA with label
	 */
	private static void trackEventGA(Context context, String activity,
			String event_type, String event_status, long time_taken_millis) {
		GAEventTrackerV4.createGaEvent(context, activity, event_type, event_status,
				time_taken_millis);
	}

}
