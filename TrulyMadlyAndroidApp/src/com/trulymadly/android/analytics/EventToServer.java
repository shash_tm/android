package com.trulymadly.android.analytics;

public class EventToServer {

	private final Long _id;
	private final String event_info;


	public EventToServer(Long _id, String event_info) {
		this._id = _id;
		this.event_info=event_info;
	    }
	    
	    public Long key()   { return this._id; }
	    public String value() { return this.event_info; }
	    
	    
}
