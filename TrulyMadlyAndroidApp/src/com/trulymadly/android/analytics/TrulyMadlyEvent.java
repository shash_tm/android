package com.trulymadly.android.analytics;

import com.trulymadly.android.app.TrulyMadlyApplication;

public class TrulyMadlyEvent {

    public static String socketConnectionEventType(){
        return TrulyMadlyApplication.isApplicationVisible() ? TrulyMadlyEventTypes.socket_connection : TrulyMadlyEventTypes.socket_connection_bg;
    }

    public class TrulyMadlyActivities {

        public static final String index = "index";
        public static final String GCM = "GCM";
        public static final String push = "push";
        public static final String install = "install";
        public static final String update = "update";
        public static final String login = "login";
        public static final String logout = "logout";
        // public static final String sign_up = "signUp";
        public static final String register_basics = "register_basics";
        public static final String preview = "preview";
        public static final String edit_profile = "edit_profile";
        public static final String register_photo = "register_photo";
        public static final String photo = "photo";
        public static final String trustbuilder_register = "trustbuilder_register";
        public static final String trustbuilder = "trustbuilder";

        public static final String hobby = "hobby";

        public static final String edit_preference = "edit_preference";

        public static final String matches = "matches";
        public static final String photo_flow = "photo_flow";
        public static final String voucher = "vouchers";
        public static final String profile = "profile";

        public static final String app_rate = "app_rate";
        public static final String contact_us = "contact_us";
        public static final String message_full_conversation = "message_full_conversation";
        public static final String emoticons = "emoticons";
        public static final String ask_friends = "ask_friends";
        public static final String shorten_api = "shorten_api";
        public static final String Moengage_notification = "Moengage_notification";
        public static final String settings = "settings";
        public static final String Socket = "socket";
        public static final String conversation_list = "conversation_list";
        public static final String quiz = "quiz";
        public static final String sticker = "sticker";
        public static final String competitors = "competitors";
        public static final String menu = "menu";
        public static final String faq = "faq";
        public static final String my_profile_fav_click = "my_profile_fav_click";
        public static final String mutual_match_fav_click = "mutual_match_fav_click";
        public static final String match_fav_click = "match_fav_click";
        public static final String datespot_list = "datespot_list";
        public static final String datespots = "datespots";
        public static final String photo_fail = "photo_fail";

        public static final String campaigns = "campaigns";
        public static final String scenes = "scenes";

        public static final String photo_share = "photo_share";

        //Sparks Related
        public static final String sparks = "sparks";

        public static final String delete = "delete";
        public static final String favorites = "favorites";

        public static final String my_profile = "my_profile";
        public static final String photo_full = "photo_full";

        //Select Related
        public static final String select = "select";
        public static final String referrer = "referrer";
    }

    public class TrulyMadlyEventTypes {
        /**
         * MAX LIMIT 1000 for facebook analytics
         */

        public static final String login = "login";
        public static final String logout = "logout";
        public static final String install = "install";
        public static final String update = "update";
        public static final String signup_via_email = "signup_via_email";
        public static final String signup_via_facebook = "signup_via_facebook";
        public static final String login_via_email = "login_via_email";
        public static final String logout_server_call = "click";
        public static final String install_referrer = "install_referrer";
        public static final String push_open = "push_open";
        public static final String push_received = "push_received";
        public static final String fb_click = "fb_click";
        public static final String fb_popup = "fb_popup";
        public static final String fb_popup_cancel = "fb_popup_cancel";
        public static final String fb_server_call = "fb_server_call";
        public static final String page_load = "page_load";
        public static final String register_server_save_call = "register_server_save_call";
        public static final String regiter_basics = "basics";
        public static final String register_interest = "interest";
        public static final String add_own_hashtag_launched = "add_own_hashtag_launched";
        public static final String hashtag_selected = "own_hashtag_selected";
        public static final String register_habit = "habit";
        public static final String register_other_details = "other_details";
        public static final String linkedin_popup = "linkedin_popup";
        public static final String linkedin_server_call = "linkedin_server_call";
        public static final String linkedin_failed = "linkedin_failed";
        public static final String linkedin_cancel = "linkedin_cancel";
        public static final String register_work = "work";
        public static final String register_education = "education";
        public static final String register_hashtag = "hashtag";
        public static final String register_start = "register_start";
        public static final String photo_skip = "photo_skip";
        public static final String add_from_computer_clicked = "add_from_computer_clicked";
        public static final String take_from_camera_clicked = "take_from_camera_clicked";
        public static final String add_from_computer_upload = "add_from_computer_upload";
        public static final String take_from_camera_upload = "take_from_camera_upload";
        public static final String add_from_computer_cancel = "add_from_computer_cancel";
        public static final String take_from_camera_cancel = "take_from_camera_cancel";
        public static final String fb_photo_save = "fb_photo_save";
        public static final String photo_save_call = "photo_save_call";
        public static final String skip = "skip";
        public static final String phone_click = "phone_click";
        public static final String phone_server_call = "phone_server_call";
        public static final String id_click = "id_click";
        public static final String id_server_call = "id_server_call";
        public static final String endorsement_click = "endorsement_click";
        public static final String employment_click = "employment_click";
        public static final String uploadpic_click = "uploadpic_click";
        public static final String uploadpic_server_call = "uploadpic_server_save_call";
        public static final String view_profile_clicked = "view_profile_clicked";
        public static final String page_load_without_cache = "page_load_without_cache";
        public static final String save_call = "save_call";
        public static final String introCancel = "introCancel";
        public static final String abort = "abort";
        public static final String like = "like";
        public static final String hide = "hide";
        public static final String rate_app = "rate_app";
        public static final String feedback_app = "feedback_app";
        public static final String available_vouchers = "available_vouchers";
        public static final String fetch_voucher_code = "fetch_voucher_code";
        public static final String fetching_images = "fetching_images";
        public static final String forget_password_server_call = "forget_password_server_call";
        public static final String ask_friend_click = "ask_friend_click";
        public static final String ask_friend_confirm = "ask_friend_confirm";
        public static final String ask_friend_cancel = "ask_friend_cancel";
        public static final String ask_friend_liked_after_nope = "ask_friend_liked_after_nope";
        public static final String ask_friend_noped = "ask_friend_noped";
        public static final String shorten = "shorten";
        public static final String discovery_option_toggled = "discovery_option_toggled";
        public static final String sound_option_toggled = "sound_option_toggled";
        public static final String vibration_option_toggled = "vibration_option_toggled";
        public static final String socket_connection = "connection";
        public static final String socket_connection_bg = "connection_bg";
        public static final String socket_disconnection = "disconnect";
        public static final String quiz_icon_click = "quiz_icon_click_call";
        public static final String quiz_status = "quiz_status_call";
        public static final String fetch_quiz = "fetch_quiz";
        public static final String save_quiz_answers = "save_quiz_answers";
        public static final String api_call = "api_call";
        public static final String send_nudge = "send_nudge";
        public static final String send_quiz_message = "send_quiz_message";
        public static final String sticker_sent = "sticker_sent";
        public static final String gallery_shown = "gallery_shown";
        public static final String quiz_item_in_chat_clicked = "quiz_item_in_chat_clicked_call";
        public static final String quiz_fetch_common_answers = "quiz_fetch_common_answers";
        public static final String quiz_item_in_drawer_clicked = "quiz_item_in_drawer_clicked";
        public static final String login_privacy_view = "login_privacy_view";
        public static final String sign_up_on_email_page_clicked = "sign_up_on_email_page_clicked";
        public static final String fb_birthday_permission_shown = "fb_birthday_permission_shown";
        public static final String email_instead_clicked = "email_instead_clicked";
        public static final String home_page_slider_changed = "home_page_slider_changed";
        public static final String fetch_static_data = "fetch_static_data";
        public static final String otp_verify = "otp_number_verify";
        public static final String otp_send = "otp_number_send";
        public static final String phone_call_number_send = "phone_call_number_send";
        public static final String otp_number_verify_403 = "otp_number_verify_403";
        public static final String otp_number_verify_success = "otp_number_verify_success";
        public static final String otp_number_verify_incorrect = "otp_number_verify_incorrect";
        public static final String why_verify_clicked = "why_verify_clicked";
        public static final String otp_number_send_success = "otp_number_send_success";
        public static final String otp_number_send_403 = "otp_number_send_403";

        public static final String delete_click = "delete_click";
        public static final String delete_cancel_click = "delete_cancel_click";
        public static final String delete_continue_click = "delete_continue_click";
        public static final String discovery_option_toggled_delete = "discovery_option_toggled_delete";
        public static final String conversation_passcode = "conversation_passcode";

        public static final String send_nudge_common_answers = "send_nudge_common_answers";
        public static final String send_nudge_chat = "send_nudge_chat";
        public static final String quiz_item_in_chat_clicked_call_view_answers = "quiz_item_in_chat_clicked_call_view_answers";
        public static final String quiz_item_in_chat_clicked_call_play_quiz = "quiz_item_in_chat_clicked_call_play_quiz";
        public static final String get_played_quiz_flare = "get_played_quiz_flare";


        public static final String ad_native = "ad_native";
        public static final String ad_interstitial = "ad_interstitial";
        public static final String terms_of_service_clicked = "terms_of_service_clicked";
        public static final String my_profile_clicked = "my_profile_clicked";
        public static final String matches_clicked = "matches_clicked";
        public static final String preferences_clicked = "preferences_clicked";
        public static final String conversations_clicked = "conversations_clicked";
        public static final String share_clicked = "share_clicked";
        public static final String refer_earn_clicked = "refer_earn_clicked";
        public static final String settings_clicked = "settings_clicked";
        public static final String faq_clicked = "faq_clicked";

        public static final String movies_click = "movies_click";
        public static final String music_click = "music_click";
        public static final String food_click = "food_click";
        public static final String books_click = "books_click";
        public static final String others_click = "others_click";
        public static final String menu_viewed = "menu_viewed";
        public static final String suggest_viewed = "suggest_viewed";
        public static final String suggest_clicked = "suggest_clicked";
        public static final String suggest_confirmed = "suggest_confirm";
        public static final String awaiting_response = "awaiting_response";
        public static final String voucher_code_viewed = "voucher_code_viewed";
        public static final String lets_go_clicked = "lets_go_clicked";
        public static final String lets_go_viewed = "lets_go_viewed";
        public static final String lets_go_confirmed = "lets_go_confirm";

        public static final String save_number = "number_saved";
        public static final String icon_clicked = "icon_clicked";
        public static final String tutorial = "tutorial";
        public static final String photo_retry = "photo_retry";


        public static final String permission = "permission";
        public static final String pi_activity_continue_photo = "pi_activity_continue_photo";
        public static final String pi_activity_shown = "pi_activity_shown";

        public static final String trustscore = "trustscore";
        public static final String impression = "impression";
        public static final String click = "click";
        public static final String save_datespot_preferences = "save_datespot_preferences";

        public static final String matches_last = "matches_last";
        public static final String crobo_postback = "crobo_postback";


        public static final String compress = "compress";
        public static final String compress_success = "compress_success";
        public static final String compress_fail = "compress_fail";
        public static final String upload = "upload";
        public static final String upload_success = "upload_success";
        public static final String upload_fail = "upload_fail";
        public static final String upload_cancel = "upload_cancel";
        public static final String download_thumbnail = "download_thumbnail";
        public static final String download_thumbnail_success = "download_thumbnail_success";
        public static final String download_thumbnail_fail = "download_thumbnail_fail";
        public static final String download_image = "download_image";
        public static final String download_image_success = "download_image_success";
        public static final String download_image_fail = "download_image_fail";
        public static final String list_category = "list_category";
        public static final String list_event = "list_event";
        public static final String list_following = "list_following";
        public static final String details_event_followed = "details_event_followed";
        public static final String details_event = "details_event";
        public static final String like_matchpage = "like_matchpage";
        public static final String hide_matchpage = "hide_matchpage";
        public static final String get_tickets = "get_tickets";
        public static final String get_tickets_followed = "get_tickets_followed";
        public static final String follow_fb = "follow_fb";
        public static final String follow_fb_followed = "follow_fb_followed";
        public static final String phone_called = "phone_called";
        public static final String phone_called_followed = "phone_called_followed";
        public static final String follow_event = "follow_event";
        public static final String unfollow_event = "unfollow_event";
        public static final String chat_match = "chat_match";
        public static final String chat_match_followed = "chat_match_followed";
        public static final String view_match = "view_match";
        public static final String like_match = "like_match";
        public static final String await_response = "await_response";
        public static final String hide_match = "hide_match";
        public static final String likeback_event = "likeback_event";
        public static final String disabled_match = "disabled_match";
        public static final String filter_applied = "filter_applied";
        public static final String clear_chat = "clear_chat";
        public static final String share_event = "share_event";
        public static final String share_event_followed = "share_event_followed";
        public static final String share_match = "share_match";
        public static final String share_match_followed = "share_match_followed";
        public static final String share_outside = "share_outside";
        public static final String share_outside_followed = "share_outside_followed";
        public static final String filter_clicked = "filter_clicked";
        public static final String nearby_filter_clicked = "nearby_filter_clicked";
        public static final String filter = "filter_clicked";

        public static final String scenes_tutorial = "scenes_tutorial";
        public static final String profile_ads = "profile_ads";
        public static final String image_link_click = "image_link_click";
        public static final String rating = "rating";

        //Sparks Related
        public static final String intro = "intro";
        public static final String buy = "buy";
        public static final String send = "send";
        public static final String conv_list = "conv_list";
        public static final String chat = "chat";
        public static final String share_instagram_shown = "share_instagram_shown";
        public static final String share_instagram_clicked = "share_instagram_clicked";
        public static final String like_facebook_clicked = "like_facebook_clicked";
        public static final String refer_a_friend = "refer_a_friend";
        public static final String favorites_save_server_call = "favorites_save_server_call";
        public static final String favorites_suggestion_server_call = "favorites_suggestion_server_call";
        public static final String favorites_autocomplete_server_call = "favorites_autocomplete_server_call";
        public static final String select = "select";
        public static final String deselect = "deselect";
        public static final String show_less = "show_less";
        public static final String show_more = "show_more";

        //Video Profiles
        public static final String download_video_thumbnail = "download_video_thumbnail";
        public static final String video_previewed = "video_previewed";
        public static final String video_mute = "video_mute";
        public static final String video_unmute = "video_unmute";
        public static final String video_completed = "video_completed";
        public static final String video_closed = "video_closed";
        public static final String play_clicked = "play_clicked";

        public static final String already_sparked = "already_sparked";


        public static final String why_not_spark = "why_not_spark";
        public static final String preview_video = "preview_video_clicked";
        public static final String delete_video = "delete_video";
        public static final String video_create = "video_create";
        public static final String video_save = "video_save";
        public static final String video_clicked = "video_clicked";
        public static final String video_upload = "video_upload";
        public static final String video_record = "video_record";
        public static final String video_cancel = "video_cancel";
        public static final String post_video_server_call = "post_video_server_call";
        public static final String favorites_popup_matches = "favorites_popup_matches";
        public static final String add_video_clicked = "add_video_clicked";
        public static final String tm_select_menu_clicked = "tm_select_menu_clicked";
        public static final String tm_scenes_clicked = "tm_scenes_clicked";
        public static final String select_serious_intent_nudge = "select_serious_intent_nudge";
        public static final String select_detail_card_profile = "select_detail_card_profile";
        public static final String select_on_action_nudge = "select_on_action_nudge";
        public static final String buy_trial = "buy_trial";

        public static final String quiz = "quiz";

        public static final String sdk_init = "sdk_init";
        public static final String sdk_init_req = "sdk_init_req";
        public static final String constraints_check = "constraints_check";
        public static final String webview_open = "webview_open";

        /** MAX LIMIT 300 for facebook analytics */
    }

    public class TrulyMadlyEventStatus {
        public static final String success = "success";
        public static final String not_matched = "not_matched";
        public static final String matched = "matched";
        public static final String matches = "matches";
        public static final String profile = "profile";

        public static final String unlock = "unlock";
        public static final String setup = "setup";

        public static final String ad_request_made = "ad_request_made";
        public static final String ad_request_success = "ad_request_success";
        public static final String ad_mediation_success_admob = "ad_mediation_success_admob";
        public static final String ad_mediation_success_fb = "ad_mediation_success_fb";
        public static final String ad_request_failure = "ad_request_failure";
        public static final String ad_mediation_failed_admob = "ad_mediation_failed_admob";
        public static final String ad_mediation_failed_fb = "ad_mediation_failed_fb";
        public static final String ad_shown = "ad_shown";
        public static final String ad_mediation_shown_admob = "ad_mediation_shown_admob";
        public static final String ad_mediation_shown_fb = "ad_mediation_shown_fb";
        public static final String ad_clicked = "ad_clicked";
        public static final String ad_interacted = "ad_interacted";
        public static final String ad_closed = "ad_closed";
        public static final String webview_closed = "webview_closed";

        public static final String shown = "shown";
        public static final String denied = "denied";
        public static final String granted = "granted";
        public static final String try_again = "try_again";

        public static final String open = "open";
        public static final String close = "close";

        public static final String fail = "fail";
        public static final String cancel = "cancel";
        public static final String thumbnail = "thumbnail";

        public static final String started = "started";
        public static final String stopped = "stopped";
        public static final String error = "error";

        public static final String ad_error = "ad_error";


        //Sparks Related
        public static final String tryit_clicked = "tryit_clicked";
        public static final String view = "view";
        public static final String buy = "buy";
        public static final String tm_error = "tm_error";
        public static final String buy_error = "buy_error";
//        public static final String success = "success";
        public static final String send = "send";
        public static final String send_error = "send_error";
        public static final String interests_error = "interests_error";
        public static final String delete_clicked = "delete_clicked";
        public static final String delete_confirmed = "delete_confirmed";
        public static final String say_hi = "say_hi";
        public static final String seen = "seen";
//        public static final String error = "error";
        public static final String clicked = "clicked";
        public static final String view_profile = "view_profile";
        public static final String message_sent = "message_sent";

        public static final String autocomplete = "autocomplete";
        public static final String suggestion = "suggestion";
        public static final String my_favorite = "my_favorite";
        public static final String viewed = "viewed";
        public static final String maybe_later = "maybe_later";
        public static final String send_spark = "send_spark";

        public static final String clicked_yes = "clicked_yes";
        public static final String clicked_no = "clicked_no";

        public static final String quiz_started = "quiz_started";
        public static final String quiz_ended = "quiz_ended";

        public static final String fetch_success = "fetch_success";
        public static final String fetch_fail = "fetch_fail";
    }

    public class TrulyMadlyEventInfoKeys {
        public static final String ad_feasible = "ad_feasible";
    }
}
