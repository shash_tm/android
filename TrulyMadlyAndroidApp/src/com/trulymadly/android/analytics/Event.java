package com.trulymadly.android.analytics;

import android.content.Context;

import com.trulymadly.android.app.utility.ABHandler;
import com.trulymadly.android.app.utility.TimeUtils;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

public class Event {

	private JSONObject system_info;

	public Event(Context context, String activity, String event_type,
			String event_status, Map<String, String> event_info,
			Float time_taken) throws JSONException {
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(3);
		if (Utility.isSet(activity)) {
			this.system_info = new JSONObject();
			this.system_info.put("activity", activity);
			this.system_info.put("event_type", event_type);
			this.system_info.put("event_status", event_status);
			if (time_taken != null) {
				this.system_info.put("time_taken",
						df.format(time_taken.floatValue()));
			}
			this.system_info.put("source", "android_app");
			this.system_info.put("os_version", Utility.getOsVersion());
			this.system_info.put("device_id", Utility.getDeviceId(context));
			String sdk_version = Utility.getAppVersionName(context);
            String ab_prefix = ABHandler.getABPrefix(context);
            if (Utility.isSet(ab_prefix)) {
				sdk_version += "_" + ab_prefix;
			}
			this.system_info.put("sdk_version", sdk_version);
			this.system_info.put("tstamp", currentGMTTime());
			this.system_info.put("user_id", Utility.getMyId(context));
			if (event_info != null) {
				JSONObject event_info_new = new JSONObject();
				for (String key : event_info.keySet()) {
					event_info_new.put(key, event_info.get(key));
				}
				this.system_info.put("event_info", event_info_new);
			}
		}
	}

	private String currentGMTTime() {
		SimpleDateFormat dateFormatGmt = new SimpleDateFormat(
				TimeUtils.TIME_FORMAT, Locale.ENGLISH);
		dateFormatGmt.setTimeZone(TimeZone.getTimeZone("GMT"));
		return dateFormatGmt.format(new Date());
	}

	public JSONObject payLoad() {
		return system_info;
	}

}
