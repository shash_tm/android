package com.trulymadly.android.analytics;


import android.content.Context;

import com.crashlytics.android.Crashlytics;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.ConstantsUrls;
import com.trulymadly.android.app.sqlite.EventTrackingDBHandler;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

class EventSendWorker {

    public static String tag = "EVENT_SEND_WORKER";
    private static boolean running = false;
    private static WeakReference<Context> appContext;
    private static long runningActivities = 0;

    private static void doStart(Context context) {
        if (!running) {
            synchronized (EventSendWorker.class) {
                if (!running) {
					//Crashlytics.log(Log.DEBUG, "Starting the EventSendWorker");
                    appContext = new WeakReference<>(context.getApplicationContext());

					running = true;
					Thread payloadRunner = new PayloadRunner();
					Thread.UncaughtExceptionHandler handler = new Thread.UncaughtExceptionHandler() {
						@Override
						public void uncaughtException(Thread thread, Throwable throwable) {
							//Crashlytics.log(Log.DEBUG, "EventSendWorker : Error in the thread "+throwable.getMessage());
						}
					};
					payloadRunner.setUncaughtExceptionHandler(handler);
					payloadRunner.setName("Trulymadly-PayloadSendWorker");
					payloadRunner.start();
				}
			}
		}
	}


    private static EventTrackingDBHandler getDBHandler(Context context) {
		return EventTrackingDBHandler.getInstance(context);
	}

    private static void pause(int millis) {
        try {
            //Crashlytics.log(Log.DEBUG, "Pausing the EventSendWorker Thread");
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            // Happens during normal operation.
        }
    }

	public static void ensureRunning(Context context) {
		doStart(context);
	}

	public static void activityStarted(Context context) {
		runningActivities++;
		doStart(context);
	}

	public static void activityStopped() {
		runningActivities--;

		if (runningActivities < 0) {
			runningActivities = 0;
        }
    }

    /*
     * this will run only when user is on the app
     */
    private static class PayloadRunner extends Thread {
        public void run() {
            try {
                synchronized (this) {
                    //Crashlytics.log(Log.DEBUG, "Starting the EventSendWorker Thread");
                    if (appContext == null || appContext.get() == null) {
                        return;
                    }
                    Context ctx = appContext.get();
                    EventTrackingDBHandler db = getDBHandler(ctx);
                    while (runningActivities > 0) {

                        //Crashlytics.log(Log.DEBUG, "EventSendWorker Thread: Checking runningActivity Count:"+runningActivities);

                        if (!Utility.isNetworkAvailable(ctx)) {
                            break;
                        }
                        List<EventToServer> data_to_send = db.getPayloadToSent();
                        //Crashlytics.log(Log.DEBUG, "EventSendWorker Thread: Payload to send: size"+data_to_send.size());
                        if (data_to_send == null || data_to_send.size() == 0) {
                            // There is no payload in the db.
                            pause(Constants.EMPTY_QUEUE_SLEEP_TIME);
                            continue;
                        }
                        List<Long> ids_sent_successfully = new ArrayList<>();

                        EventHttpClientHttpResponse response = null;
                        JSONArray dataToSend = new JSONArray();
                        for (EventToServer s : data_to_send) {
                            ids_sent_successfully.add(s.key());
                            try {
                                JSONObject event_info = new JSONObject(s.value());
                                dataToSend.put(event_info);
                            } catch (JSONException ignored) {
                            }
                        }


                        response = EventHttpClient.performHttpRequest(ConstantsUrls.get_EVENT_TRK_URL(), dataToSend.toString(), ctx);

                        // Each Payload type is handled by the appropriate handler, but if sent correctly, or failed permanently to send, it should be removed from the queue.
                        if (response != null) {
                            if (response.isSuccessful()) {
                                //Crashlytics.log(Log.DEBUG, "Payload submission successful. Removing from send queue.");
                                //ids to deleted
                                db.deletePayload(ids_sent_successfully);
                            } else if (response.isRejectedPermanently() || response.isBadPayload()) {
                                Crashlytics.logException(new IOException("Tracking response failed - code : " + response.getCode()));
                                //Crashlytics.log(Log.DEBUG, "Payload rejected. Removing from send queue.");
                                //db.deletePayload(ids_sent_successfully);
                            } else if (response.isRejectedTemporarily()) {
                                //Crashlytics.logException(new IOException("Tracking response failed - Rejected Temporarily - code : " + response.getCode()));
                                //Crashlytics.log(Log.DEBUG, "Unable to send JSON. Leaving in queue.");
                                // Break the loop. Restart when network is reachable.
                                break;
                            }
                        } else {
                            break;
                        }
                        //sleep the thread so that we don't make continue calls
                        pause(Constants.QUEUE_SLEEP_TIME);
                    }
                }
            } catch (Exception e) {
                Crashlytics.logException(e);
            } finally {
                running = false;
            }
        }
    }
}
