package com.trulymadly.android.Exception;

/**
 * Created by udbhav on 20/11/15.
 */
public class DbIsNullException extends Exception {
    public DbIsNullException() {
        super();
    }

    public DbIsNullException(String message) {
        super(message);
    }

    public DbIsNullException(String message, Throwable cause) {
        super(message, cause);
    }

    public DbIsNullException(Throwable cause) {
        super(cause);
    }

}