/**
 * 
 */
package com.trulymadly.android.socket;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.bus.ServiceEvent;
import com.trulymadly.android.app.bus.ServiceEvent.SERVICE_EVENT_TYPE;
import com.trulymadly.android.app.json.ConstantsSocket;
import com.trulymadly.android.app.json.ConstantsSocket.SOCKET_EMITS;
import com.trulymadly.android.app.utility.TimeUtils;
import com.trulymadly.android.app.utility.TmLogger;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;

import io.socket.client.Ack;


/**
 * @author udbhav
 *
 */
public class AckCustom implements Ack {

    private static final String TAG = "SOCKET_ACK";
	private final HashMap<String, String> map;
	private final Object data;
	private String event_name;
	private String ack_event_status = "ack_failed";
	private Handler handler = null;
	//private  Runnable runnable = null;
	private AckTimeoutRunnable ackTimeoutRunnable = null;
	private Context aContext;
	private Ack ack = null;
	private boolean isRequestComplete = false, isFirstTime, trackInActionLog;
	private Date startTime = null;
	private boolean isManualRetry = false;
	private int emitAttemptCounter = 1;
	
	public AckCustom(Context ctx, String event_name, Ack ack, Object data, boolean isFirstTime, boolean isDebug, int socketConnectionAttemptCounter) {
		this.map = new HashMap<>();
		this.data = data;
		this.emitAttemptCounter = socketConnectionAttemptCounter;
		if (data instanceof JSONArray) {
			this.isManualRetry = false;
			map.put("socketId", ((JSONArray) data).optJSONObject(0).optString("socketId"));
			map.put("unique_id", ((JSONArray) data).optJSONObject(0).has("unique_id") ? ((JSONArray) data).optJSONObject(0).optString("unique_id") : Utility.generateUniqueRandomId());
		} else if (data instanceof JSONObject) {
			this.isManualRetry  = ((JSONObject) data).optBoolean("isRetry");
			map.put("unique_id", ((JSONObject) data).has("unique_id") ? ((JSONObject) data).optString("unique_id") : Utility.generateUniqueRandomId());
			map.put("socketId", ((JSONObject) data).optString("socketId"));
		}
		map.put("local_tstamp", TimeUtils.getFormattedTime());
		createAckCustom(ctx, event_name, ack, isFirstTime, isDebug);
	}

	private void createAckCustom(Context ctx, String event_name, Ack ack, boolean isFirstTime, boolean isDebug) {
		aContext = ctx;
		this.event_name = event_name;
		this.ack = ack;
		this.isFirstTime = isFirstTime;
		
		//special case logging
		String event_status = "sent"; 
		
		if (SOCKET_EMITS.chat_sent.equalsIgnoreCase(event_name) || SOCKET_EMITS.get_missed_messages.equalsIgnoreCase(event_name)) {
			event_status += "_" + emitAttemptCounter;
		}
		if (SOCKET_EMITS.chat_sent.equalsIgnoreCase(event_name) && isManualRetry) {
			event_status = "manual_retry--" +  event_status;
		}

		trackInActionLog = SOCKET_EMITS.chat_sent.equalsIgnoreCase(event_name);

		if (isDebug) {
			trackInActionLog = true;	
		}

		if(!SOCKET_EMITS.chat_typing.equals(event_name)) {
			TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyActivities.Socket, event_name, 0, event_status, map, trackInActionLog);
		}
        TmLogger.log(Log.DEBUG, TAG, "request " + event_name + " : " + event_status);
		onStart();
	}

	private void onStart() {
		if (handler == null) {
			handler = new Handler(Looper.getMainLooper());
		}
		if (ackTimeoutRunnable != null) {
			handler.removeCallbacks(ackTimeoutRunnable);
			ackTimeoutRunnable = null;
		}
		ackTimeoutRunnable = new AckTimeoutRunnable() {

			@Override
			public void myRun(Date myStartTime) {
				startTime = myStartTime;
				ack_event_status = "ack_timed_out";
				call((Object) null);
			}
		};
		
		// setting timeouts
        long timeout = ConstantsSocket.EMIT_THRESHOLD_TIMEOUT;
        if(SOCKET_EMITS.ping.equals(event_name)) {
			if(Utility.getNetworkClass(aContext).equals("2G")){
                timeout = ConstantsSocket.EMIT_THRESHOLD_TIMEOUT_FOR_PING_2G;
            } else {
                timeout = ConstantsSocket.EMIT_THRESHOLD_TIMEOUT_FOR_PING;
            }
		}
		if(SOCKET_EMITS.chat_sent.equals(event_name)) {
            timeout = ConstantsSocket.EMIT_THRESHOLD_TIMEOUT_CHAT_SENT;
        }
		if(SOCKET_EMITS.get_missed_messages.equals(event_name)) {
            timeout = ConstantsSocket.EMIT_THRESHOLD_TIMEOUT_GMM;
        }
		if(SOCKET_EMITS.chat_typing.equals(event_name)) {
            timeout = ConstantsSocket.EMIT_THRESHOLD_TIMEOUT_CHAT_TYPING;
        }
		handler.postDelayed(ackTimeoutRunnable, timeout);
		startTime = new Date();
	}
	
	@Override
	public void call(Object... args) {
		
		boolean isSuccess = (args != null && args.length > 0 && args[0] != null);
		String event_status = isSuccess?"ack_received":ack_event_status;
		if (SOCKET_EMITS.chat_sent.equalsIgnoreCase(event_name) || SOCKET_EMITS.get_missed_messages.equalsIgnoreCase(event_name)) {
			event_status += "_" + emitAttemptCounter;
		}
		if (isManualRetry) event_status = "manual_retry--" +  event_status;
		map.put("local_tstamp", TimeUtils.getFormattedTime());
		long time_taken = 0;
		if (startTime != null) {
			time_taken = TimeUtils.getSystemTimeInMiliSeconds() - startTime.getTime();
		}
		
		//TRACKING
		if(!SOCKET_EMITS.chat_typing.equals(event_name)) {
			TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyActivities.Socket, event_name, time_taken, event_status, map, trackInActionLog);
		}
        TmLogger.log(isSuccess?Log.DEBUG:Log.ERROR, TAG, "request " + event_name + " : " + event_status);
		//=======
		
		// cancelling runnable on response
		if (handler != null && ackTimeoutRunnable != null) {
			handler.removeCallbacks(ackTimeoutRunnable);
			ackTimeoutRunnable = null;
		}
		
		if (!isRequestComplete) {
			isRequestComplete = true;

			// If isFailed && isFirstTime && (chat_sent or getBool missed messages) then retry.
			if(!isSuccess && isFirstTime && SOCKET_EMITS.chat_sent.equals(event_name)){
				if (data instanceof JSONArray) {
					Utility.fireBusEvent(aContext, true, new ServiceEvent(SERVICE_EVENT_TYPE.EMIT, event_name, (JSONArray) data, ack, false));
				} else if (data instanceof JSONObject) {
					//DEBUG: Fail status for chat_sent
					// ((JSONObject) data).put("fail", false);
					Utility.fireBusEvent(aContext, true, new ServiceEvent(SERVICE_EVENT_TYPE.EMIT, event_name, (JSONObject) data, ack, false, emitAttemptCounter+1));
				}
			} else if(!isSuccess && isFirstTime && SOCKET_EMITS.get_missed_messages.equals(event_name) && ((JSONObject) data).optBoolean("isOnConnect")){
				//DEBUG: Fail status for get_missed_messages
				// ((JSONObject) data).put("fail", false);
				Utility.fireBusEvent(aContext, true, new ServiceEvent(SERVICE_EVENT_TYPE.EMIT_DIRECT, event_name, (JSONObject) data, ack, false, emitAttemptCounter+1));
			} else {
				if (ack != null) {
					ack.call(args);
				}
			}
		}
	}
	
	private abstract class AckTimeoutRunnable implements Runnable {

		private Date myStartTime = null;
		public AckTimeoutRunnable() {
			myStartTime = new Date();
		}

        public abstract void myRun(Date myStartTime);

        @Override
		public void run() {
			myRun(myStartTime);
		}
	}

}
