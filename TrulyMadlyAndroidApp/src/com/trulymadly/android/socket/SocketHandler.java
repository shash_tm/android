/**
 *
 */
package com.trulymadly.android.socket;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyActivities;
import com.trulymadly.android.analytics.TrulyMadlyEvent.TrulyMadlyEventTypes;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.MessageModal;
import com.trulymadly.android.app.MessageOneonOneConversionActivity;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.TrulyMadlyApplication;
import com.trulymadly.android.app.bus.ChatMessageReadEvent;
import com.trulymadly.android.app.bus.ChatMessageReceivedEvent;
import com.trulymadly.android.app.bus.ChatTypingReceivedEvent;
import com.trulymadly.android.app.bus.ServiceEvent;
import com.trulymadly.android.app.bus.TriggerChatUpdateEvent;
import com.trulymadly.android.app.bus.TriggerConnectingEvent;
import com.trulymadly.android.app.bus.TriggerConversationListUpdateEvent;
import com.trulymadly.android.app.bus.TriggerQuizStatusRefreshEvent;
import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.json.ConstantsRF;
import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.json.ConstantsSocket;
import com.trulymadly.android.app.json.ConstantsSocket.SOCKET_EMITS;
import com.trulymadly.android.app.json.ConstantsSocket.SOCKET_END;
import com.trulymadly.android.app.json.ConstantsSocket.SOCKET_START_TYPE;
import com.trulymadly.android.app.json.ConstantsSocket.SOCKET_STATE;
import com.trulymadly.android.app.listener.CustomOkHttpResponseHandler;
import com.trulymadly.android.app.listener.GetMetaDataCallbackInterface;
import com.trulymadly.android.app.listener.OnSocketConnectedCallbackInterface;
import com.trulymadly.android.app.modal.MatchMessageMetaData;
import com.trulymadly.android.app.modal.NotificationModal;
import com.trulymadly.android.app.sqlite.MessageDBHandler;
import com.trulymadly.android.app.sqlite.RFHandler;
import com.trulymadly.android.app.utility.NotificationHandler;
import com.trulymadly.android.app.utility.OkHttpHandler;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.TimeUtils;
import com.trulymadly.android.app.utility.TmLogger;
import com.trulymadly.android.app.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.net.ssl.SSLException;
import javax.net.ssl.SSLHandshakeException;

import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.client.Manager;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import io.socket.engineio.client.Transport;
import io.socket.engineio.client.transports.WebSocket;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author udbhav
 */
public class SocketHandler {

    // http://chat.trulymadly.com
    // http://chat1.trulymadly.com http://52.74.209.59
    // https://chat2.trulymadly.com:443
    // t1 : 52.74.15.38 -> 52.74.130.184 -> 52.76.19.195 -> https://t2chat.trulymadly.com:443 (http://54.179.170.231:80)
    // dev : 52.74.40.23 -> 52.74.103.245 -> 54.169.28.196 -> 54.255.149.203

    public final static String tag = "socketio";
    private static SocketHandler socketHandler = null;
    private final Context aContext;
    private final Emitter.Listener onChatRead;
    private final Emitter.Listener onChatReceived;
    private final Emitter.Listener onChatTyping;
    private final Emitter.Listener onQuizRefresh;
    private final Emitter.Listener onDebug;
    private final Emitter.Listener onRestart;
    private final Emitter.Listener onSocketConnected;
    private final Emitter.Listener onSocketDisconnected;
    private final Emitter.Listener onSocketConnectionError;
    private final Emitter.Listener onSocketError;
    private final Emitter.Listener onSocketConnectionTimeout;
    private final Emitter.Listener onSocketReconnectionFailed;
    private final Emitter.Listener onSocketReconnecting;
    private boolean isPingInProgress = false;
    private Socket socket = null;
    private int socketConnectionAttemptCounter = 1;
    private ArrayList<OnSocketConnectedCallbackInterface> onSocketConnectedCallbackList = null;
    private boolean isDebug = false;
    private boolean toReconnectAfterDisconnect = false, isDestroying = false;
    private Date connectStartTime, totalConnectStartTime = null;
    private boolean isClearingInProgress = false;
    private SOCKET_STATE socketState;
    private String lastPingEventStatus;
    //default true - in case switching to polling
    private boolean lastPingSuccessStatus = true;
    private SOCKET_START_TYPE socketStartType = SOCKET_START_TYPE.NORMAL;

    private SocketHandler(Context ctx) {
        aContext = ctx;
        ConstantsSocket.setUpConstants(ctx);
        setCtr(1);
        setSocketState(SOCKET_STATE.POLLING);
        TmLogger.log(Log.DEBUG, tag, "creating new instance");
        setDebug(SPHandler.getBool(aContext,
                ConstantsSP.SHARED_KEYS_SOCKET_DEBUG));

        onSocketConnected = new Emitter.Listener() {

            @Override
            public void call(Object... paramVarArgs) {
                TmLogger.log(Log.DEBUG, tag, "connected");
                String socketId = socket != null && Utility.isSet(socket.id())
                        ? socket.id() : "null";
                TmLogger.log(Log.ERROR, tag, "SOCKET ID : " + socketId);
                String event_status = null;
                if (paramVarArgs != null && paramVarArgs.length > 0
                        && paramVarArgs[0] != null) {
                    event_status = paramVarArgs[0].toString();
                    TmLogger.log(Log.DEBUG, tag, event_status);
                }
                long time_taken = 0;
                if (connectStartTime != null) {
                    time_taken = TimeUtils.getSystemTimeInMiliSeconds()
                            - connectStartTime.getTime();
                }
                TrulyMadlyTrackEvent.trackEvent(aContext,
                        TrulyMadlyActivities.Socket,
                        TrulyMadlyEvent.socketConnectionEventType(), time_taken,
                        "ack_received" + getCtrWithSuffix(), getEventInfoMap(),
                        isDebug);
                TmLogger.log(Log.DEBUG, tag,
                        TrulyMadlyEvent.socketConnectionEventType()
                                + "::ack_received" + getCtrWithSuffix());
                getMissedMessages(null, true, true);
            }
        };
        onSocketDisconnected = new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                setSocketState(SOCKET_STATE.POLLING);
                resetCtr();
                TmLogger.log(Log.ERROR, tag, "DISCONNECTED "
                        + getErrorInEventStatusFromArgs(0, args));
                if (!isDestroying) {
                    // Not using TrulyMadlyApplication.isApplicationVisible() as it is causing repeated connect attempts when app is open.
//                    if (toReconnectAfterDisconnect
//                            || TrulyMadlyApplication.isApplicationVisible())
                    if (toReconnectAfterDisconnect) {
                        connect();
                        toReconnectAfterDisconnect = false;
                    }
                } else {
                    socket = null;
                    isDestroying = false;
                }
            }
        };
        onSocketConnectionTimeout = new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                // not needed as this is the same call being sent with
                // onSocketConnectionError as timeout error
                // connectionEnd("connection timeout", args);
                TmLogger.log(Log.ERROR, tag,
                        "TIMEOUT " + getErrorInEventStatusFromArgs(0, args));
            }
        };
        onSocketConnectionError = new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                String event_status = getErrorInEventStatusFromArgs(getCtr(),
                        args);
                TmLogger.log(Log.ERROR, tag,
                        "CONNECTION ERROR " + event_status);
                boolean isCurrentSocketAttemptTimedOut = event_status.startsWith("ack_timed_out");
                if (!isCurrentSocketAttemptTimedOut) {
                    TrulyMadlyTrackEvent.trackEvent(aContext,
                            TrulyMadlyActivities.Socket,
                            TrulyMadlyEvent.socketConnectionEventType(), 0, event_status,
                            getEventInfoMap(), true);
                }

                handleSocketConnectionError(isCurrentSocketAttemptTimedOut);
            }

        };
        onSocketError = new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                String event_status = "", event_type = "";
                boolean connectionEndWhileConnecting = false;
                //if (isSocketConnecting && !isSocketConnected) {
                if (socketState == SOCKET_STATE.CONNECTING) {
                    connectionEndWhileConnecting = true;
                    event_status = getErrorInEventStatusFromArgs(getCtr(),
                            args);
                    event_type = TrulyMadlyEvent.socketConnectionEventType();
                } else {
                    connectionEndWhileConnecting = false;
                    event_status = getErrorInEventStatusFromArgs(0, args);
                    event_type = "error_socket";
                }
                TmLogger.log(Log.ERROR, tag, "SOCKET ERROR " + event_status);

                boolean isCurrentSocketAttemptTimedOut = event_status.startsWith("ack_timed_out");
                if (!isCurrentSocketAttemptTimedOut) {
                    TrulyMadlyTrackEvent.trackEvent(aContext,
                            TrulyMadlyActivities.Socket, event_type, 0,
                            event_status, getEventInfoMap(), true);
                }

//                if (checkStatusForIpFallback(event_status)) {
//                    connectionFailed(SOCKET_END.error_1);
//                }
                if (connectionEndWhileConnecting) {
                    handleSocketConnectionError(isCurrentSocketAttemptTimedOut);
                } else {
                    setSocketState(SOCKET_STATE.POLLING);
                    resetCtr();
                }
            }
        };

        onSocketReconnecting = new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                //TmLogger.log(Log.ERROR, tag, "INCREMENTING CTR");
                //incCtr();

                connectStartTime = new Date();
                setSocketState(SOCKET_STATE.CONNECTING);
                TmLogger.log(Log.DEBUG, tag, "reconnectiang");
                if (!Utility.isNetworkAvailable(aContext)) {
                    connectionEnd(SOCKET_END.no_network, SOCKET_STATE.FAILED, false);
                } else if (!Utility.isUserLoggedIn(aContext)) {
                    connectionEnd("stopping_after_not_logged_in", SOCKET_STATE.POLLING, false);
                } else {
                    TrulyMadlyTrackEvent.trackEvent(aContext,
                            TrulyMadlyActivities.Socket,
                            TrulyMadlyEvent.socketConnectionEventType(), 0,
                            "sent" + getCtrWithSuffix(), getEventInfoMap(),
                            isDebug);
                    TmLogger.log(Log.DEBUG, tag,
                            TrulyMadlyEvent.socketConnectionEventType() + "::sent"
                                    + getCtrWithSuffix());
                    initPing();
                }
            }

        };
        onSocketReconnectionFailed = new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                TmLogger.log(Log.ERROR, tag, "Failed Reconnections");
                //setSocketState(SOCKET_STATE.POLLING);

                //commenting this out for case of 0 reconnections.
                //connectionFailed(SOCKET_END.failed_reconnections_limit_reached, false);
            }

        };
        onChatReceived = new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                TrulyMadlyApplication
                        .updateLastEmittedOrReceivedTimestamp(aContext);
                TmLogger.log(Log.DEBUG, tag, "chat message");
                if (args != null && args.length > 0 && args[0] != null) {
                    try {
                        TmLogger.log(Log.DEBUG, tag,
                                args[0].toString());
                        callChatMessageReceived((JSONObject) args[0]);
                        triggerConversationViewUpdate();
                        TrulyMadlyTrackEvent.trackEvent(aContext,
                                TrulyMadlyActivities.Socket, "chat_received", 0,
                                "received", getEventInfoMap(), isDebug);
                    } catch (ClassCastException e) {
                        TmLogger.log(Log.DEBUG, tag, args[0].toString());
                    }
                }
            }
        };
        onChatRead = new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                TrulyMadlyApplication
                        .updateLastEmittedOrReceivedTimestamp(aContext);
                TmLogger.log(Log.DEBUG, tag, "chat read");
                if (args != null && args.length > 0 && args[0] != null) {
                    try {
                        TmLogger.log(Log.DEBUG, tag,
                                args[0].toString());
                        callChatMessageRead((JSONObject) args[0]);
                        triggerConversationViewUpdate();
                        TrulyMadlyTrackEvent.trackEvent(aContext,
                                TrulyMadlyActivities.Socket,
                                "chat_read_listener", 0, "received",
                                getEventInfoMap(), isDebug);
                    } catch (ClassCastException e) {
                        TmLogger.log(Log.DEBUG, tag, args[0].toString());
                    }
                }
            }
        };
        onChatTyping = new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                TmLogger.log(Log.DEBUG, tag, "chat typing listener");
                if (args != null && args.length > 0 && args[0] != null) {
                    try {
                        TmLogger.log(Log.DEBUG, tag,
                                args[0].toString());
                        callChatTypingReceived((JSONObject) args[0]);
                    } catch (ClassCastException e) {
                        TmLogger.log(Log.DEBUG, tag, args[0].toString());
                    }
                }
            }
        };
        onQuizRefresh = new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                TmLogger.log(Log.DEBUG, tag, "quiz refresh received");
                if (args != null && args.length > 0 && args[0] != null) {
                    try {
                        TmLogger.log(Log.DEBUG, tag,
                                args[0].toString());
                        callQuizStatusRefresh((JSONObject) args[0]);
                    } catch (ClassCastException e) {
                        TmLogger.log(Log.DEBUG, tag, args[0].toString());
                    }
                }
            }
        };
        onDebug = new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                if (args != null && args.length > 0 && args[0] != null) {
                    try {
                        TmLogger.log(Log.DEBUG, tag,
                                args[0].toString());
                        setDebug(((JSONObject) args[0]).optBoolean("debug"));
                    } catch (ClassCastException e) {
                        TmLogger.log(Log.DEBUG, tag, args[0].toString());
                    }
                }
            }
        };
        onRestart = new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                TmLogger.log(Log.DEBUG, tag, "onRestart received");
                TrulyMadlyApplication.forceRestartService(aContext,
                        SOCKET_END.force_restart_on_server_emit_restart);
            }
        };
    }

    synchronized public static SocketHandler get(Context ctx) {
        if (socketHandler == null) {
            socketHandler = new SocketHandler(ctx.getApplicationContext());
        }
        return socketHandler;
    }

    public static boolean isSocketEnabled(Context aContext) {

        boolean isSocket = false;

        if (TimeUtils.isTimeoutExpired(aContext,
                ConstantsSP.SHARED_KEYS_LAST_TSTAMP_SOCKET_SWITCHED_TO_PHP_POLLING,
                ConstantsSocket.POLLING_EXPIRATION_TIMEOUT)) {
            SPHandler.remove(aContext,
                    ConstantsSP.SHARED_KEYS_LAST_TSTAMP_SOCKET_SWITCHED_TO_PHP_POLLING);
            isSocket = true;
        }
        return isSocket && Utility.isSet(SPHandler.getString(aContext,
                ConstantsSP.SHARED_KEYS_SOCKET_ENABLED));
    }

    public static void destroy() {
        if (socketHandler != null) {
            socketHandler.destroySocket();
            socketHandler = null;
        }
    }

    public static JSONObject prepareSocketParams(String message, MessageModal.MessageType messageType,
                                                 String messageId, String matchId, int quizId,
                                                 boolean isFromFeedbackPage, boolean isRetry,
                                                 JSONObject metadata) {
        JSONObject paramsSocket = new JSONObject();
        try {
            paramsSocket.put("msg", message);
            paramsSocket.put("message_type", messageType.name());
            paramsSocket.put("unique_id", messageId);
            paramsSocket.put("receiver_id", matchId);
            paramsSocket.put("is_admin", isFromFeedbackPage);
            paramsSocket.put("isRetry", isRetry);
            paramsSocket.put("quiz_id", quizId);

            if (metadata != null) {
                paramsSocket.put("metadata", metadata);
            }

            // DEBUG: Fail status for chat_sent
            // paramsSocket.put("fail", true);
        } catch (JSONException ignored) {
        }

        return paramsSocket;
    }

    private void destroySocket() {
        clearSocketConnectedCallbackList();
        isDestroying = true;
        if (socket != null) {
            socket.disconnect();
        } else {
            isDestroying = false;
        }
    }

    public void setSocketStartType(SOCKET_START_TYPE socketStartType) {
        this.socketStartType = socketStartType;
    }

    private void handleSocketConnectionError(boolean isCurrentSocketAttemptTimedOut) {
        boolean isPingSuccess = false;
        //isIpFallback
        //isAckTimedOut

        if (socketStartType == SOCKET_START_TYPE.NORMAL) {
            // First attempt when there is no error or ack_timed_out
            if (isCurrentSocketAttemptTimedOut) {
                sendLastPingEventStatus();
                connectionEnd(SOCKET_END.ack_timed_out_1, SOCKET_STATE.POLLING, true);
            } else {
                //event_status.endsWith("java.io.IOException: 502") || event_status.contains("java.net.ProtocolException")
                //connectionFailed(SOCKET_END.exception502);
                //checkStatusForIpFallback(event_status)
                connectionEnd(SOCKET_END.error_1, SOCKET_STATE.POLLING, true);
            }
        } else {
            //Second attempt when there is an error or ack_timed_out
            if (isCurrentSocketAttemptTimedOut) {
                sendLastPingEventStatus();
                if (socketStartType == SOCKET_START_TYPE.IP_FALLBACK_ON_ERROR) {
                    if (lastPingSuccessStatus) {
                        TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyActivities.Socket,
                                TrulyMadlyEventTypes.socket_disconnection, 0, SOCKET_END.failed_reconnections_limit_reached,
                                getEventInfoMap(), true);
                        connectionFailed(SOCKET_END.ack_timed_out_2_error, true);
                    } else {
                        connectionEnd(SOCKET_END.ack_timed_out_2_error, SOCKET_STATE.FAILED, true);
                    }
                } else {
                    if (lastPingSuccessStatus) {
                        TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyActivities.Socket,
                                TrulyMadlyEventTypes.socket_disconnection, 0, SOCKET_END.failed_reconnections_limit_reached,
                                getEventInfoMap(), true);
                        connectionFailed(SOCKET_END.ack_timed_out_2_ack_timed_out, true);
                    } else {
                        connectionEnd(SOCKET_END.ack_timed_out_2_ack_timed_out, SOCKET_STATE.FAILED, true);
                    }
                }
            } else {
                TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyActivities.Socket,
                        TrulyMadlyEventTypes.socket_disconnection, 0, SOCKET_END.failed_reconnections_limit_reached,
                        getEventInfoMap(), true);
                if (socketStartType == SOCKET_START_TYPE.IP_FALLBACK_ON_ERROR) {
                    connectionFailed(SOCKET_END.error_2_error, true);
                } else {
                    connectionFailed(SOCKET_END.error_2_ack_timed_out, true);
                }
//                connectionFailed(
//                        SOCKET_END.failed_reconnections_limit_reached, true);
            }
        }
//        if (isAckTimeOut
//                && ctr >= (MAX_RETRY_ATTEMPT + 1)) {
//            //Will not happen as per above else-if check added.
//            connectionFailed(
//                    SOCKET_END.failed_reconnections_limit_reached);
//        }

    }

    private boolean checkStatusForIpFallback(String event_status) {
        return event_status.contains("javax.net.ssl.SSLHandshakeException")
                || event_status.contains("javax.net.ssl.SSLException")
                || event_status.contains("javax.net.ssl.SSLPeerUnverifiedException")
                || event_status.contains("java.net.UnknownHostException");
    }

    private void incCtr() {
        socketConnectionAttemptCounter++;
    }

    private void resetCtr() {
        setCtr(0);
        socketStartType = SOCKET_START_TYPE.NORMAL;
    }

    private int getCtr() {
        if (socketConnectionAttemptCounter == 0) {
            setCtr(1);
        }
        if (socketConnectionAttemptCounter >= 1 && socketStartType != SOCKET_START_TYPE.NORMAL) {
            setCtr(2);
        }
        return socketConnectionAttemptCounter;
    }

    private void setCtr(int c) {
        socketConnectionAttemptCounter = c;
    }

    private String getCtrWithSuffix() {
        int ctr = getCtr();
        String suffix = "_" + ctr;
        if (ctr > 1) {
            if (socketStartType == SOCKET_START_TYPE.IP_FALLBACK_ON_ERROR)
                suffix += "_error";
            if (socketStartType == SOCKET_START_TYPE.ACK_TIMED_OUT)
                suffix += "_ack_timed_out";
        }
        return suffix;
    }

    private String getErrorInEventStatusFromArgs(int ctr, Object... args) {
        String event_status = "";
        if (args != null && args.length > 0 && args[0] != null) {
            event_status = args[0].toString();
            if (event_status.endsWith("websocket error")) {
                event_status = "websocket error";
            } else if (event_status.endsWith("xhr poll error")) {
                event_status = "xhr poll error";
            } else if (event_status.endsWith("xhr post error")) {
                event_status = "xhr post error";
            } else if (event_status.endsWith("timeout")) {
                event_status = "ack_timed_out";
            } else if (event_status.endsWith("server error")) {
                event_status = "server error";
            }
            if (ctr != 0) {
                event_status += getCtrWithSuffix();
            }
            try {
                Throwable e = ((Exception) args[0]).getCause();
                if (e instanceof UnknownHostException) {
                    event_status += " -- " + e.toString();
                } else if (e instanceof ConnectException) {
                    event_status += " -- " + e.toString();
                } else if (e instanceof SocketTimeoutException) {
                    event_status += " -- " + e.toString();
                } else if (e instanceof SocketException) {
                    event_status += " -- " + e.toString();
                } else if (e instanceof IOException) {
                    event_status += " -- " + e.toString();
                } else if (e instanceof SSLHandshakeException) {
                    event_status += " -- " + e.toString();
                } else if (e instanceof SSLException) {
                    event_status += " -- " + e.toString();
                } else {
                    event_status += " -- " + e.toString();
                }
            } catch (NullPointerException | ClassCastException ignored) {
            }

        }
        return event_status;
    }

    private HashMap<String, String> getEventInfoMap() {
        HashMap<String, String> map = new HashMap<>();
        map.put("local_tstamp", TimeUtils.getFormattedTime());
        String socketId = socket != null && Utility.isSet(socket.id())
                ? socket.id() : "null";
        map.put("socketId", socketId);
        return map;
    }

    private void triggerConversationViewUpdate() {
        Utility.fireBusEvent(aContext, true,
                new TriggerConversationListUpdateEvent());
    }

    synchronized private void setSocketState(SOCKET_STATE socketState) {
        this.socketState = socketState;
        Utility.fireBusEvent(aContext, true,
                new TriggerConnectingEvent(socketState));
    }

    private void addToOnSocketConnectedCallbackList(
            OnSocketConnectedCallbackInterface onSocketConnectedCallback) {
        if (this.onSocketConnectedCallbackList == null) {
            this.onSocketConnectedCallbackList = new ArrayList<>();
        }
        if (onSocketConnectedCallback != null) {
            onSocketConnectedCallbackList.add(onSocketConnectedCallback);
        }
    }

    private synchronized void callSocketConnectedCallbackList() {
        if (onSocketConnectedCallbackList != null
                && onSocketConnectedCallbackList.size() > 0) {
            for (OnSocketConnectedCallbackInterface onSocketConnectedCallback : onSocketConnectedCallbackList) {
                checkNotNull(onSocketConnectedCallback).connected();
            }
        }
        onSocketConnectedCallbackList.clear();
    }

    private synchronized void clearSocketConnectedCallbackList() {
        if (onSocketConnectedCallbackList != null
                && onSocketConnectedCallbackList.size() > 0
                && !isClearingInProgress) {
            isClearingInProgress = true;
            for (OnSocketConnectedCallbackInterface onSocketConnectedCallback : onSocketConnectedCallbackList) {
                checkNotNull(onSocketConnectedCallback).failed();
            }
            onSocketConnectedCallbackList.clear();
            isClearingInProgress = false;
        }
    }

    private void setDebug(boolean isSocketDebug) {
        isDebug = isSocketDebug;
    }

    public void getMetaData(final JSONArray match_ids,
                            final GetMetaDataCallbackInterface onMetaDataReceived) {
        final String myId = Utility.getMyId(aContext);
        MatchMessageMetaData matchMessageMetaData_currentId = null;
        JSONArray match_ids_stripped = new JSONArray();
        for (int i = 0; i < match_ids.length(); i++) {
            matchMessageMetaData_currentId = MessageDBHandler
                    .getMatchMessageMetaData(myId, match_ids.optString(i),
                            aContext);
            if (matchMessageMetaData_currentId == null
                    || matchMessageMetaData_currentId
                    .getLastUpdateTstamp() == null
                    || matchMessageMetaData_currentId.getLastUpdateTstamp()
                    .before(TimeUtils.getParsedTime(getTstamp(1)))
                    || !Utility.isSet(matchMessageMetaData_currentId.getFName())
                    || !Utility.isSet(
                    matchMessageMetaData_currentId.getProfileUrl())) {
                match_ids_stripped.put(match_ids.optString(i));
            }
        }

        if (match_ids_stripped.length() > 0) {
            try {
                JSONObject params = new JSONObject();
                params.put("match_ids", match_ids_stripped);
                Ack ack = new Ack() {
                    @Override
                    public void call(Object... paramVarArgs) {
                        if (paramVarArgs != null && paramVarArgs.length > 0
                                && paramVarArgs[0] != null) {
                            TmLogger.log(Log.DEBUG, "socketResponse",
                                    paramVarArgs[0].toString());
                            // saving meta data in db.
                            JSONArray metaDataArray;
                            try {
                                metaDataArray = new JSONArray(
                                        paramVarArgs[0].toString());
                                MatchMessageMetaData matchMessageMetaData_thread;
                                int l = metaDataArray.length();
                                for (int i = 0; i < l; i++) {
                                    JSONObject metaDataObject = metaDataArray
                                            .optJSONObject(i);
                                    matchMessageMetaData_thread = MessageDBHandler
                                            .getMatchMessageMetaData(myId,
                                                    metaDataObject.optString(
                                                            "profile_id"),
                                                    aContext);
                                    matchMessageMetaData_thread
                                            .generateMatchMessageMetaDataFromServer(
                                                    metaDataObject);
                                    MessageDBHandler.insertMessageDetails(
                                            matchMessageMetaData_thread,
                                            aContext);
                                }
                                if (l > 0) {
                                    getChatMetaData(match_ids,
                                            onMetaDataReceived);
                                }
                            } catch (JSONException e) {
                                Crashlytics.logException(e);
                            }
                        }
                    }
                };
                emitSocketMessage(SOCKET_EMITS.get_user_metadata, params, ack,
                        true, 1);
            } catch (JSONException e) {
                Crashlytics.logException(e);
            }
        } else {
            getChatMetaData(match_ids, onMetaDataReceived);
        }

    }

    private void getChatMetaData(JSONArray match_ids,
                                 final GetMetaDataCallbackInterface onMetaDataReceived) {
        try {
            JSONObject data = new JSONObject();
            data.put("match_ids", match_ids);
            Ack ack = new Ack() {
                @Override
                public void call(Object... paramVarArgs) {
                    if (paramVarArgs != null && paramVarArgs.length > 0
                            && paramVarArgs[0] != null) {
                        TmLogger.log(Log.DEBUG, "socketResponse",
                                paramVarArgs[0].toString());
                        // saving meta data in db.
                        JSONArray metaDataArray;
                        try {
                            metaDataArray = new JSONArray(
                                    paramVarArgs[0].toString());
                            int l = metaDataArray.length();
                            for (int i = 0; i < l; i++) {
                                JSONObject metaDataObject = metaDataArray
                                        .optJSONObject(i);
                                // [{"last_seen_msg_id":19886,"last_seen_msg_time":"2015-03-09
                                // 05:51:17","match_id":18110}]
                                MessageDBHandler.updateMessageMetaDataValue(
                                        aContext,
                                        Utility
                                                .getMyId(aContext),
                                        metaDataObject.optString("match_id"),
                                        "last_seen_receiver_tstamp",
                                        metaDataObject.optString(
                                                "last_seen_msg_time"));
                                MessageDBHandler.setChatReadOutgoing(aContext,
                                        Utility
                                                .getMyId(aContext),
                                        metaDataObject.optString("match_id"),
                                        metaDataObject.optString(
                                                "last_seen_msg_time"));
                            }
                        } catch (JSONException e) {
                            Crashlytics.logException(e);
                        }
                        if (onMetaDataReceived != null) {
                            onMetaDataReceived.onMetaDataReceived();
                        }
                    }
                }
            };
            emitSocketMessage(SOCKET_EMITS.get_chat_metadata, data, ack, true,
                    1);
        } catch (JSONException e) {
            Crashlytics.logException(e);
        }

    }

    public void getMissedMessages(String tstamp, final Boolean showNotification,
                                  boolean isOnConnect) {
        Ack ack = new Ack() {
            @Override
            public void call(Object... paramVarArgs) {
                if (paramVarArgs != null && paramVarArgs.length > 0
                        && paramVarArgs[0] != null) {
                    connectionSuccessFull();
                    processMissedMessages(paramVarArgs[0].toString(),
                            showNotification);
                    Utility.fireBusEvent(aContext, true,
                            new TriggerChatUpdateEvent());
                    getCDASKMessagesHack();
                    getFirstTimeMessages();
                } else {
                    connectionFailed(SOCKET_END.get_missed_messages_failed, false);
                }
            }
        };
        try {
            JSONObject params = new JSONObject();
            params.put("timestamp", getGmmTstamp(tstamp));
            params.put("isOnConnect", isOnConnect);
            // DEBUG: Fail status for get_missed_messages
            // params.put("fail", true);
            if (isOnConnect) {
                // Directly emitting as GMM is part of connection now.
                emitNow(SOCKET_EMITS.get_missed_messages, params, ack, true, 1);
            } else {
                emitSocketMessage(SOCKET_EMITS.get_missed_messages, params, ack,
                        true, 1);
            }
        } catch (JSONException ignored) {
        }
    }

    private String getGmmTstamp(String tstamp) {
        String maxTstamp = null;
        if (Utility.isSet(tstamp)) {
            maxTstamp = tstamp;
        } else {
            maxTstamp = MessageDBHandler.getLastMessageReceivedTstamp(
                    Utility.getMyId(aContext),
                    aContext);
        }
        // getting last 1 day messages in case of no tstamp.
        return maxTstamp != null ? maxTstamp : getTstamp(1);
    }

    private void getCDASKMessagesHack() {

        if (RFHandler.getBool(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_CURATED_DEALS_CD_ASK_UPDATE_CHECKED)) {
            return;
        }
        String firstCDASKMessageTstamp = MessageDBHandler
                .getFirstCDASKMessageTstamp(Utility.getMyId(aContext),
                        aContext);
        if (!Utility.isSet(firstCDASKMessageTstamp)) {
            //setString rigid field as true as there is not need to this hack
            RFHandler.insert(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_CURATED_DEALS_CD_ASK_UPDATE_CHECKED, true);
            return;
        }
        TmLogger.log(Log.DEBUG, tag, "getting CD ASK messages hack messages");
        Ack ack = new Ack() {
            @Override
            public void call(Object... paramVarArgs) {
                if (paramVarArgs != null && paramVarArgs.length > 0
                        && paramVarArgs[0] != null) {
                    try {
                        processMissedMessages(paramVarArgs[0].toString(),
                                false);
                        //setString rigid field as true as data has been processed
                        RFHandler.insert(aContext, Utility.getMyId(aContext), ConstantsRF.RIGID_FIELD_CURATED_DEALS_CD_ASK_UPDATE_CHECKED, true);
                        Utility.fireBusEvent(aContext, true,
                                new TriggerChatUpdateEvent());
                    } catch (OutOfMemoryError ignored) {

                    }
                }
            }
        };
        try {
            JSONObject params = new JSONObject();
            params.put("timestamp", firstCDASKMessageTstamp);
            emitSocketMessage(SOCKET_EMITS.get_missed_messages, params, ack,
                    true, 1);
        } catch (JSONException ignored) {
        }

    }

    private void getFirstTimeMessages() {
        if (Utility.isSet(SPHandler.getString(aContext,
                ConstantsSP.SHARED_KEYS_SOCKET_FIRST_TIME_MESSAGES))) {
            return;
        }
        TmLogger.log(Log.DEBUG, tag, "getting first time messages");
        Ack ack = new Ack() {
            @Override
            public void call(Object... paramVarArgs) {
                if (paramVarArgs != null && paramVarArgs.length > 0
                        && paramVarArgs[0] != null) {
                    try {
                        processMissedMessages(paramVarArgs[0].toString(),
                                false);
                        SPHandler.setString(aContext,
                                ConstantsSP.SHARED_KEYS_SOCKET_FIRST_TIME_MESSAGES,
                                "yes");
                        Utility.fireBusEvent(aContext, true,
                                new TriggerChatUpdateEvent());
                    } catch (OutOfMemoryError ignored) {

                    }
                }
            }
        };
        try {
            JSONObject params = new JSONObject();
            params.put("timestamp", getTstamp(10));
            emitSocketMessage(SOCKET_EMITS.get_missed_messages, params, ack,
                    true, 1);
        } catch (JSONException ignored) {
        }

    }

    private void processMissedMessages(String messagesJsonArrayString,
                                       boolean showNotification) {
        try {
            JSONArray missedMessages = new JSONArray(messagesJsonArrayString);
            int len = missedMessages.length();
            TmLogger.log(Log.DEBUG, "socketResponse",
                    "got " + len + " messages");
            String my_id = Utility.getMyId(aContext);
            MessageModal messageObj;
            HashMap<String, NotificationModal> matchNotificationsMap = new HashMap<>();
            for (int i = 0; i < len; i++) {
                messageObj = new MessageModal(my_id,
                        missedMessages.getJSONObject(i));
                messageObj = MessageOneonOneConversionActivity
                        .processSingleMessage(messageObj, aContext);
                if (showNotification && messageObj != null) {
                    NotificationModal nModal = prepareNotification(messageObj);
                    if (nModal != null) {
                        matchNotificationsMap.put(messageObj.getMatch_id(),
                                nModal);
                    }
                }
            }
            if (showNotification) {
                getMetaDataAndShowNotifications(my_id, matchNotificationsMap);
            }
        } catch (JSONException e) {
            Crashlytics.logException(e);
        }

    }

    private NotificationModal prepareNotification(MessageModal messageObj) {
        String matchId = messageObj.getMatch_id();
        MatchMessageMetaData matchMessageMetaData_thread = MessageDBHandler
                .getMatchMessageMetaData(messageObj.getMy_id(), matchId,
                        aContext);
        String content_text = "";
        switch (messageObj.getMessageType()) {
            case STICKER:
                content_text = matchMessageMetaData_thread.getFName()
                        + " has sent you a sticker";
                break;
            case TEXT:
            default:
                content_text = messageObj.getMessage();
                content_text = content_text.length() > 30
                        ? content_text.substring(0, 29) + "..." : content_text;
                break;
        }

        boolean isAdmin = matchId.equalsIgnoreCase("admin");
        if (!Utility.isSet(matchMessageMetaData_thread.getFName())) {
            return null;
        }

        String ticker_text = "Message from "
                + matchMessageMetaData_thread.getFName();
        String collapse_key = NotificationHandler.COLLAPSE_KEY_PREFIX + matchId;

        String title_text = matchMessageMetaData_thread.getFName();

        if (messageObj.isSparkAccepted()) {
            title_text = aContext.getResources().getString(R.string.sparks_accepted_notification_title_text);
            content_text = matchMessageMetaData_thread.getFName() + " " + aContext.getResources().getString(R.string.sparks_accepted_notification_content_text_suffix);
        }

        NotificationModal nModal = new NotificationModal(NotificationModal.NotificationType.MESSAGE,
                title_text, content_text, ticker_text, collapse_key, matchId, isAdmin, matchMessageMetaData_thread.getProfilePic());
        try {
            JSONObject metadata = new JSONObject(messageObj.getmMetadataString());
            if (Utility.isSet(metadata.optString("event_status"))) {
                nModal.setEvent_status(metadata.optString("event_status"));
            }
        } catch (JSONException | NullPointerException ignored) {
        }


        return nModal;
    }

    private void getMetaDataAndShowNotifications(final String user_id,
                                                 final HashMap<String, NotificationModal> matchNotificationsMap) {
        ArrayList<String> match_ids = new ArrayList<>();

        for (Entry<String, NotificationModal> pair : matchNotificationsMap
                .entrySet()) {
            String matchId = pair.getKey();
            NotificationModal nModal = pair.getValue();
            if ((!Utility.isSet(nModal.getTitle_text())
                    || !Utility.isSet(nModal.getPic_url()))
                    && !match_ids.contains(matchId)) {
                match_ids.add(matchId);
            }
        }

        if (match_ids.size() > 0) {
            JSONArray match_ids_json = new JSONArray();
            for (String matchId : match_ids) {
                match_ids_json.put(matchId);
            }
            GetMetaDataCallbackInterface onMetaDataReceived = new GetMetaDataCallbackInterface() {
                @Override
                public void onMetaDataReceived() {
                    showNotifications(user_id, matchNotificationsMap);
                }
            };
            getMetaData(match_ids_json, onMetaDataReceived);
        } else {
            showNotifications(user_id, matchNotificationsMap);
        }

    }

    private void showNotifications(String user_id,
                                   HashMap<String, NotificationModal> matchNotificationsMap) {
        for (Entry<String, NotificationModal> pair : matchNotificationsMap
                .entrySet()) {
            NotificationModal nModal = pair.getValue();
            if (!Utility.isSet(nModal.getTitle_text())
                    || !Utility.isSet(nModal.getPic_url())) {
                MatchMessageMetaData matchMessageMetaData_thread = MessageDBHandler
                        .getMatchMessageMetaData(user_id, pair.getKey(),
                                aContext);
                nModal.setTitle_text(matchMessageMetaData_thread.getFName());
                nModal.setPic_url(matchMessageMetaData_thread.getProfilePic());
            }
            NotificationHandler.createLocalNotification(aContext, nModal);
        }
    }

    private String getTstamp(int dayDiff) {
        Date referenceDate = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(referenceDate);
        c.add(Calendar.DAY_OF_YEAR, 0 - dayDiff);
        return TimeUtils.getFormattedTime(c.getTime());
    }

    private void createSocket() {
        if (socket != null) {
            TmLogger.log(Log.DEBUG, tag,
                    "already created socket - aborting createSocket");
            return;
        }
        if (!Utility.isUserLoggedIn(aContext)) {
            TmLogger.log(Log.DEBUG, tag,
                    "user not logged in - aborting createSocket");
            return;
        }
        if (!Utility.isNetworkAvailable(aContext)) {
            setSocketState(SOCKET_STATE.FAILED);
            TmLogger.log(Log.DEBUG, tag,
                    "no network available - aborting createSocket");
            return;
        }
        try {
            IO.Options opts = new IO.Options();
            opts.forceNew = true;
            //This is being ignored because of the new socket connection logic with ip fallback situation.
            opts.reconnection = false;
            //opts.reconnectionAttempts = MAX_RETRY_ATTEMPT;
            //opts.reconnectionAttempts = -1;
            //opts.reconnectionDelay = ConstantsSocket.SOCKET_RECONNECTION_DELAY;
            //opts.reconnectionDelayMax = ConstantsSocket.SOCKET_RECONNECTION_DELAY_MAX;
            Crashlytics.log(Log.DEBUG, tag, "SOCKET_CONNECTION_TIMEOUT " + ConstantsSocket.SOCKET_CONNECTION_TIMEOUT);
            opts.timeout = ConstantsSocket.SOCKET_CONNECTION_TIMEOUT;

            opts.query = ConstantsSocket.getQueryHeader(aContext);
            opts.upgrade = true;
            // opts.rememberUpgrade = true;

			/* TRANSPORTS */
            if (ConstantsSocket.SOCKET_TRANPORT_ONLY_WEBSOCKET_BOOL) {
                opts.transports = new String[]{WebSocket.NAME};
                // opts.transports = new String[] {Polling.NAME};
            }

            String socketUrlForConnecting = (socketStartType != SOCKET_START_TYPE.IP_FALLBACK_ON_ERROR) ? ConstantsSocket.SOCKET_URL : ConstantsSocket.getRandomSocketIpFromCommaString(ConstantsSocket.SOCKET_IP_LIST);

            TmLogger.log(Log.DEBUG, tag,
                    "SOCKET_URL : " + socketUrlForConnecting);
            socket = IO.socket(socketUrlForConnecting, opts);
            socket.on(Socket.EVENT_CONNECT, onSocketConnected)
                    .on(Socket.EVENT_DISCONNECT, onSocketDisconnected)
                    .on(Socket.EVENT_CONNECT_ERROR, onSocketConnectionError)
                    .on(Socket.EVENT_CONNECT_TIMEOUT, onSocketConnectionTimeout)
                    .on(Socket.EVENT_ERROR, onSocketError)
                    .on(Socket.EVENT_RECONNECT_FAILED,
                            onSocketReconnectionFailed)
                    .on(Socket.EVENT_RECONNECTING, onSocketReconnecting)
                    .on("chat_received", onChatReceived)
                    .on("chat_read_listener", onChatRead)
                    .on("chat_typing_listener", onChatTyping)
                    .on("debug", onDebug).on("QUIZ_REFRESH", onQuizRefresh)
                    .on("restart", onRestart);

            socket.io().on(Manager.EVENT_TRANSPORT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Transport transport = (Transport) args[0];
                    if (transport != null && socket != null
                            && socket.io() != null) {
                        TmLogger.log(Log.DEBUG, tag, transport.name
                                + " -- TIMEOUT : " + socket.io().timeout());
                    }
                }
            }).on(Manager.EVENT_RECONNECT, new Emitter.Listener() {

                @Override
                public void call(Object... args) {
                    logArgs("EVENT_RECONNECT", args);

                }
            }).on(Manager.EVENT_CONNECT_ERROR, new Emitter.Listener() {

                @Override
                public void call(Object... args) {
                    logArgs("EVENT_CONNECT_ERROR", args);

                }
            }).on(Manager.EVENT_CONNECT_TIMEOUT, new Emitter.Listener() {

                @Override
                public void call(Object... args) {
                    logArgs("EVENT_CONNECT_TIMEOUT", args);

                }
            }).on(Manager.EVENT_RECONNECT_ATTEMPT, new Emitter.Listener() {

                @Override
                public void call(Object... args) {
                    logArgs("EVENT_RECONNECT_ATTEMPT", args);

                }
            }).on(Manager.EVENT_RECONNECT_ERROR, new Emitter.Listener() {

                @Override
                public void call(Object... args) {
                    logArgs("EVENT_RECONNECT_ERROR", args);

                }
            }).on(Manager.EVENT_RECONNECT_FAILED, new Emitter.Listener() {

                @Override
                public void call(Object... args) {
                    logArgs("EVENT_RECONNECT_FAILED", args);

                }
            }).on(Manager.EVENT_RECONNECTING, new Emitter.Listener() {

                @Override
                public void call(Object... args) {
                    logArgs("EVENT_RECONNECTING", args);

                }
            });
        } catch (URISyntaxException e) {
            Crashlytics.logException(e);
        }
        TmLogger.log(Log.DEBUG, tag, "created");
    }

    private void logArgs(String title, Object... args) {
        String message = title;
        if (args != null && args.length > 0 && args[0] != null) {
            message += " -- " + args[0].toString();
        }
        TmLogger.log(Log.DEBUG, tag, message);
    }

    private void callChatTypingReceived(JSONObject data) {
        TmLogger.log(Log.DEBUG, tag,
                "chat typing listener " + data.toString());
        String matchId = data.optString("sender_id");
        String myId = data.optString("receiver_id");
        Utility.fireBusEvent(aContext,
                MessageOneonOneConversionActivity.isChatActiveForId(aContext,
                        matchId)
                        && Utility.stringCompare(Utility.getMyId(aContext), myId),
                new ChatTypingReceivedEvent());

    }

    private void callQuizStatusRefresh(JSONObject data) {
        String matchId = data.optString("sender_id");
        Utility.fireBusEvent(
                aContext, MessageOneonOneConversionActivity
                        .isChatActiveForId(aContext, matchId),
                new TriggerQuizStatusRefreshEvent(matchId));

    }

    private void callChatMessageRead(JSONObject data) {
        String matchId = data.optString("receiver_id");
        String last_seen_msg_id = data.optString("last_seen_msg_id");
        String last_seen_msg_tstamp = data.optString("tstamp");

        MessageDBHandler.updateMessageMetaDataValue(aContext,
                Utility.getMyId(aContext), matchId,
                "last_seen_receiver_tstamp", last_seen_msg_tstamp);
        MessageDBHandler.setChatReadOutgoing(aContext,
                Utility.getMyId(aContext), matchId,
                null);
        Utility.fireBusEvent(aContext,
                MessageOneonOneConversionActivity.isChatActiveForId(aContext,
                        matchId),
                new ChatMessageReadEvent(matchId, last_seen_msg_id,
                        last_seen_msg_tstamp));
    }

    private void callChatMessageReceived(JSONObject data) {
        String my_id = Utility.getMyId(aContext);
        String sender_id = data.optString("sender_id");
        String receiver_id = data.optString("receiver_id");
        String matchId = Utility.stringCompare(my_id, sender_id) ? receiver_id
                : sender_id;

        if (MessageOneonOneConversionActivity.isChatActiveForId(aContext,
                matchId)) {
            Utility.fireBusEvent(aContext, true,
                    new ChatMessageReceivedEvent(data, matchId));
        } else {
            MessageModal messageObj = new MessageModal(my_id, data);
            messageObj = MessageOneonOneConversionActivity
                    .processSingleMessage(messageObj, aContext);
            if (messageObj != null) {
                NotificationModal nModal = prepareNotification(messageObj);
                if (nModal != null) {
                    HashMap<String, NotificationModal> matchNotificationsMap = new HashMap<>();
                    matchNotificationsMap.put(matchId, nModal);
                    getMetaDataAndShowNotifications(my_id,
                            matchNotificationsMap);
                } else if (messageObj.isSparkAccepted()) {
                    JSONArray match_ids_json = new JSONArray();
                    match_ids_json.put(messageObj.getMatch_id());
                    final MessageModal finalMessageObj = messageObj;
                    GetMetaDataCallbackInterface onMetaDataReceived = new GetMetaDataCallbackInterface() {
                        @Override
                        public void onMetaDataReceived() {
                            NotificationModal sparkAcceptedNotificationModal = prepareNotification(finalMessageObj);
                            if (sparkAcceptedNotificationModal != null) {
                                HashMap<String, NotificationModal> sparkAcceptedNotificationsMap = new HashMap<>();
                                sparkAcceptedNotificationsMap.put(finalMessageObj.getMatch_id(), sparkAcceptedNotificationModal);
                                showNotifications(finalMessageObj.getMy_id(), sparkAcceptedNotificationsMap);
                            }
                        }
                    };
                    getMetaData(match_ids_json, onMetaDataReceived);
                }
            }
        }
    }

    public void connect() {
        connect(null);
    }

    private synchronized void connect(
            OnSocketConnectedCallbackInterface onSocketConnectedCallback) {
        if (!isSocketEnabled(aContext)) {
            TmLogger.log(Log.DEBUG, tag,
                    "connecting starting -- SOCKET DISABLED");
            setSocketState(SOCKET_STATE.POLLING);
            return;
        }
        if (!Utility.isNetworkAvailable(aContext)) {
            TmLogger.log(Log.DEBUG, tag,
                    "no network available - aborting socket connect");
            setSocketState(SOCKET_STATE.FAILED);
            return;
        }
        if (isConnected()) {
            TmLogger.log(Log.DEBUG, tag, "connected already");
            addToOnSocketConnectedCallbackList(onSocketConnectedCallback);
            callSocketConnectedCallbackList();
            setSocketState(SOCKET_STATE.CONNECTED);
            return;
        }
        if (socketState == SOCKET_STATE.CONNECTING) {
            addToOnSocketConnectedCallbackList(onSocketConnectedCallback);
            TmLogger.log(Log.DEBUG, tag, "connecting in progress already");
            setSocketState(SOCKET_STATE.CONNECTING);
            return;
        }
        if (!Utility.isUserLoggedIn(aContext)) {
            TmLogger.log(Log.DEBUG, tag, "user not logged in. aborting");
            setSocketState(SOCKET_STATE.FAILED);
            return;
        }
        addToOnSocketConnectedCallbackList(onSocketConnectedCallback);
        TmLogger.log(Log.DEBUG, tag, "connecting");

        // connecting socket
        if (socket == null) {
            createSocket();
        }
        if (socket != null) {
            TrulyMadlyTrackEvent.trackEvent(aContext,
                    TrulyMadlyActivities.Socket,
                    TrulyMadlyEvent.socketConnectionEventType(), 0,
                    "sent" + getCtrWithSuffix(), getEventInfoMap(), isDebug);
            TmLogger.log(Log.DEBUG, tag,
                    TrulyMadlyEvent.socketConnectionEventType() + "::sent"
                            + getCtrWithSuffix());
            connectStartTime = new Date();
            setSocketState(SOCKET_STATE.CONNECTING);
            if (getCtr() == 1) {
                totalConnectStartTime = new Date();
            }
            socket.connect();
            initPing();
        } else {
            setSocketState(SOCKET_STATE.POLLING);
            TmLogger.log(Log.DEBUG, tag, "aborting connect");
        }
    }

    private void connectionFailed(String failReason, boolean isConnection) {
        setSocketState(SOCKET_STATE.POLLING);
        TmLogger.log(Log.ERROR, tag, "Switching to PHP");
        SPHandler.setString(aContext,
                ConstantsSP.SHARED_KEYS_LAST_TSTAMP_SOCKET_SWITCHED_TO_PHP_POLLING,
                TimeUtils.getFormattedTime());
        connectionEnd(failReason, SOCKET_STATE.POLLING, isConnection);
    }

    private void connectionSuccessFull() {
        setSocketState(SOCKET_STATE.CONNECTED);
        callSocketConnectedCallbackList();
        if (totalConnectStartTime != null) {
            long time_taken = TimeUtils.getSystemTimeInMiliSeconds()
                    - totalConnectStartTime.getTime();
            totalConnectStartTime = null;
            TrulyMadlyTrackEvent.trackEvent(aContext,
                    TrulyMadlyActivities.Socket,
                    TrulyMadlyEvent.socketConnectionEventType(), time_taken,
                    "total_connection_time", getEventInfoMap(), isDebug);
        }
        resetCtr();
        SPHandler.remove(aContext,
                ConstantsSP.SHARED_KEYS_LAST_TSTAMP_SOCKET_SWITCHED_TO_PHP_POLLING);
    }

    public void connectionEnd(String message, SOCKET_STATE endState, boolean isConnection) {
        connectionEnd(message, endState, isConnection, (Object) null);
    }

    private synchronized void connectionEnd(String message, SOCKET_STATE endState, boolean isConnection, Object... args) {
        setSocketState(endState);
        TmLogger.log(Log.DEBUG, tag, "connection end because of " + message);
        TmLogger.log(Log.DEBUG, tag, "setting socket state " + endState.toString());
        if (args != null && args.length > 0 && args[0] != null) {
            TmLogger.log(Log.DEBUG, tag,
                    getErrorInEventStatusFromArgs(0, args));
        }

        toReconnectAfterDisconnect = SOCKET_END.force_restart_on_ping_failed.equals(message)
                || SOCKET_END.network_class_change.equals(message)
                || toReconnectAfterDisconnect;

        TrulyMadlyTrackEvent.trackEvent(aContext, TrulyMadlyActivities.Socket,
                isConnection ? TrulyMadlyEvent.socketConnectionEventType() : TrulyMadlyEventTypes.socket_disconnection, 0, message,
                getEventInfoMap(), true);

        if (SOCKET_END.service_idle_timeout.equals(message)
                || SOCKET_END.logout.equals(message)
                || SOCKET_END.failed_reconnections_limit_reached.equals(message)
                || SOCKET_END.get_missed_messages_failed.equals(message)
                || SOCKET_END.no_network.equals(message)
                || SOCKET_END.restart_on_login.equals(message)
                || SOCKET_END.disabled.equals(message)
                || SOCKET_END.exception502.equals(message)
                || SOCKET_END.ack_timed_out_2_ack_timed_out.equals(message)
                || SOCKET_END.ack_timed_out_2_error.equals(message)
                || SOCKET_END.error_2_ack_timed_out.equals(message)
                || SOCKET_END.error_2_error.equals(message)) {
            TmLogger.log(Log.DEBUG, tag, "Clearing socket");
            destroy();
            // socket = null;
        } else if (SOCKET_END.error_1.equals(message)) {
            TmLogger.log(Log.DEBUG, tag, "error_1");
            destroy();
            startRetrySocketAttempt(ServiceEvent.SERVICE_EVENT_TYPE.RESTART_SOCKET_ON_ERROR);
        } else if (SOCKET_END.ack_timed_out_1.equals(message)) {
            TmLogger.log(Log.DEBUG, tag, "ack_timed_out_1");
            destroy();
            startRetrySocketAttempt(ServiceEvent.SERVICE_EVENT_TYPE.RESTART_ON_ACK_TIMED_OUT);
        } else {
            if (socket != null && socket.connected()) {
                socket.disconnect();
            }
        }
    }

    private void startRetrySocketAttempt(ServiceEvent.SERVICE_EVENT_TYPE serviceEventType) {
        TmLogger.log(Log.DEBUG, tag, "starting retry with type : " + serviceEventType.toString());

        Utility.fireServiceBusEvent(aContext,
                new ServiceEvent(serviceEventType), 1500);
    }

    public void onActivityResumed() {
        if (!isConnected()) {
            connect();
        } else {
            // emitPing();
        }
    }

    private void emitPing() {
        Ack ack = new Ack() {

            @Override
            public void call(Object... args) {
                isPingInProgress = false;
                if (args != null && args.length > 0 && args[0] != null) {
                } else {
                    TmLogger.log(Log.ERROR, tag, "ping failed");
                    connectionEnd(SOCKET_END.force_restart_on_ping_failed, SOCKET_STATE.POLLING, false);
                    // TrulyMadlyApplication.forceRestartService(aContext,
                    // SOCKET_END.force_restart_on_ping_failed);
                }
            }
        };
        if (!isPingInProgress && socketState != SOCKET_STATE.CONNECTING) {
            isPingInProgress = true;
            emitSocketMessage(SOCKET_EMITS.ping, new JSONObject(), ack, true,
                    1);
        }
    }

    synchronized public void emitNow(String event_name, Object data, Ack ack,
                                     boolean isFirstTime, int emitAttemptCounter) {
        if (socket == null) {
            return;
        }
        try {
            if (data instanceof JSONArray) {
                int length = ((JSONArray) data).length();
                for (int i = 0; i < length; i++) {
                    if (!((JSONArray) data).optJSONObject(i).has("unique_id")) {
                        ((JSONArray) data).optJSONObject(i).put("unique_id",
                                Utility.generateUniqueRandomId());
                    }
                    ((JSONArray) data).optJSONObject(i).put("socketId",
                            socket != null ? socket.id() : "null");
                }
            } else if (data instanceof JSONObject) {
                if (!((JSONObject) data).has("unique_id")) {
                    ((JSONObject) data).put("unique_id",
                            Utility.generateUniqueRandomId());
                }
                ((JSONObject) data).put("socketId",
                        socket != null ? socket.id() : "null");
            }
        } catch (JSONException ignored) {
        }
        TmLogger.log(Log.DEBUG, tag,
                "emitting '" + event_name + "' -- data : " + data.toString());
        AckCustom ackCustom = new AckCustom(aContext, event_name, ack, data,
                isFirstTime, isDebug, emitAttemptCounter);
        if (socket != null) {
            socket.emit(event_name, data, ackCustom);
        }
    }

    private void emitFailed(String event_name, Ack ack) {
        if (socketState == SOCKET_STATE.CONNECTING && !isConnected()) {
            TmLogger.log(Log.DEBUG, tag, "Couldn't emit " + event_name
                    + " - SOCKET CONNECTING IN PROGRESS");
        } else {
            TmLogger.log(Log.DEBUG, tag,
                    "Couldn't emit " + event_name + " - SOCKET NOT CONNECTED");
            if (ack != null) {
                ack.call((Object) null);
            }
            connectionEnd("socket_not_connected_while_emitting", SOCKET_STATE.POLLING, false);
        }
    }

    synchronized public void emitSocketMessage(final String event_name,
                                               final Object data, final Ack ack, final boolean isFirstTime,
                                               final int emitAttemptCounter) {
        if (!SOCKET_EMITS.get_missed_messages.equals(event_name)
                && !SOCKET_EMITS.ping.equals(event_name)) {
            TrulyMadlyApplication
                    .updateLastEmittedOrReceivedTimestamp(aContext);
        }
        OnSocketConnectedCallbackInterface onSocketConnectedCallback = new OnSocketConnectedCallbackInterface() {
            @Override
            public void connected() {
                if (!isSocketEnabled(aContext)) {
                    TmLogger.log(Log.DEBUG, tag,
                            "emitting '" + event_name + "' -- SOCKET DISABLED");
                    return;
                }
                if (isConnected()) {
                    emitNow(event_name, data, ack, isFirstTime,
                            emitAttemptCounter);
                } else {
                    emitFailed(event_name, ack);
                }
            }

            @Override
            public void failed() {
                if (ack != null) {
                    ack.call((Object) null);
                }
            }
        };
        connect(onSocketConnectedCallback);
    }

    public boolean isConnected() {
        return socketState == SOCKET_STATE.CONNECTED && socket != null && socket.connected();
    }

    /**
     * Sent the status of the saved ping status (in global vars) to the server
     */
    private void sendLastPingEventStatus() {
        if (Utility.isSet(lastPingEventStatus)) {
            TrulyMadlyTrackEvent.trackEvent(aContext,
                    TrulyMadlyActivities.Socket,
                    TrulyMadlyEvent.socketConnectionEventType(), 0,
                    lastPingEventStatus, getEventInfoMap(),
                    isDebug);
            TmLogger.log(Log.DEBUG, tag, "lastPingEventStatus : " + lastPingEventStatus);
        }
    }

    private void initPing() {
        if (socketStartType == SOCKET_START_TYPE.ACK_TIMED_OUT) {
            (new PingAsyncTask("ack_timed_out", 2)).execute();
        } else if (socketStartType == SOCKET_START_TYPE.IP_FALLBACK_ON_ERROR) {
            (new PingAsyncTask("error", 2)).execute();
        } else {
            (new PingAsyncTask("", 1)).execute();
        }
    }

    private class PingAsyncTask extends AsyncTask<Void, Void, Void> {


        final String suffix;
        private final String pingType;
        private final int ctr;

        public PingAsyncTask(String pingType, int ctr) {
            this.pingType = pingType;
            this.ctr = ctr;
            //do not reset lastPingEventStatus. causing issue in tracking
            //lastPingEventStatus = "";
            lastPingSuccessStatus = false;
            TmLogger.log(Log.DEBUG, tag, "Pinging : " + Constants.ping_url);
            suffix = Utility.isSet(pingType) ? ("_" + pingType) : "";
        }

        @Override
        protected Void doInBackground(Void... voids) {
            CustomOkHttpResponseHandler responseHandler = new CustomOkHttpResponseHandler(
                    aContext) {

                @Override
                public void onRequestSuccess(JSONObject responseJSON) {
                    lastPingEventStatus = "ping_success_" + ctr + suffix;
                    lastPingSuccessStatus = true;
                    TmLogger.log(Log.DEBUG, tag, "Pinging : " + lastPingEventStatus);
                }

                @Override
                public void onRequestFailure(Exception exception) {
                    lastPingEventStatus = "ping_fail_" + ctr + suffix;
                    lastPingSuccessStatus = false;
                    TmLogger.log(Log.DEBUG, tag, "Pinging : " + lastPingEventStatus);
                }
            };

            OkHttpHandler.httpGet(aContext, Constants.ping_url, null,
                    responseHandler, OkHttpHandler.NET_TIMEOUT.SMALL);

            return null;
        }
    }

}
