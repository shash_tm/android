package com.trulymadly.android.app;

import android.content.Context;
import android.test.InstrumentationTestCase;

import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.json.ProfileNewResponseParser;
import com.trulymadly.android.app.json.UserFlagsParser;
import com.trulymadly.android.app.modal.MatchesLatestModal2;
import com.trulymadly.android.app.modal.UserFlags;
import com.trulymadly.android.app.utility.SPHandler;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by avin on 04/12/15.
 */
public class ParsersTest extends InstrumentationTestCase{

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        Context context = getInstrumentation().getTargetContext();
        clearTempData();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        clearTempData();
    }

    private void clearTempData() {

    }

    @Test
    public void test_parseUserFlags(){
        Context context = getInstrumentation().getTargetContext();
        InputStream stream = context.getResources().openRawResource(R.raw.userflags_without_chat);
        String xml = null;
        try {
            byte[] buffer = new byte[stream.available()];
            stream.read(buffer);
            stream.close();
            xml = new String(buffer, "UTF-8"); // you just need to specify the charsetName
        } catch (IOException e) {
            // Error handling
        }

        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(xml);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        UserFlags userFlags = UserFlagsParser.parseUserFlags(context, jsonObject);
        assertNotNull(userFlags);
        assertNull(userFlags.getmChatsConfig());
//        assertNull(userFlags.getmInterstitialNativeAdsConfig());

//        assertFalse(AdsHandler.isAdAllowed(context, AdsHandler.ADS.conversationItemAd));
//        assertFalse(AdsHandler.isAdAllowed(context, AdsHandler.ADS.interstitialConversationAd));
//        assertFalse(AdsHandler.isAdAllowed(context, AdsHandler.ADS.interstitialMatchesAd));

        String USER_ID = "123456";
        SPHandler.setString(context, ConstantsSP.SHARED_KEYS_USER_ID, USER_ID);
//        AdsFlagsParser.processInterstitialNativeAdsFlags(context, USER_ID, userFlags.getmInterstitialNativeAdsConfig());
//        assertFalse(AdsHandler.isAdAllowed(context, AdsHandler.ADS.conversationItemAd));
//        assertFalse(AdsHandler.isAdAllowed(context, AdsHandler.ADS.interstitialConversationAd));
//        assertFalse(AdsHandler.isAdAllowed(context, AdsHandler.ADS.interstitialMatchesAd));

        //With Chats config and AdsFlags
        stream = context.getResources().openRawResource(R.raw.userflags);
        xml = null;
        try {
            byte[] buffer = new byte[stream.available()];
            stream.read(buffer);
            stream.close();
            xml = new String(buffer, "UTF-8"); // you just need to specify the charsetName
        } catch (IOException e) {
            // Error handling
        }

        jsonObject = null;
        try {
            jsonObject = new JSONObject(xml);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        userFlags = UserFlagsParser.parseUserFlags(context, jsonObject);
        assertNotNull(userFlags);
        assertNotNull(userFlags.getmChatsConfig());
//        assertNotNull(userFlags.getmInterstitialNativeAdsConfig());

//        AdsFlagsParser.processInterstitialNativeAdsFlags(context, USER_ID, userFlags.getmInterstitialNativeAdsConfig());
//        assertTrue(AdsHandler.isAdAllowed(context, AdsHandler.ADS.conversationItemAd));
//        assertTrue(AdsHandler.isAdAllowed(context, AdsHandler.ADS.interstitialConversationAd));
//        assertTrue(AdsHandler.isAdAllowed(context, AdsHandler.ADS.interstitialMatchesAd));
    }

    @Test
    public void test_parseProfileNewResponse(){
        Context context = getInstrumentation().getTargetContext();
        InputStream stream = context.getResources().openRawResource(R.raw.matches_0);
        String xml = null;
        try {
            byte[] buffer = new byte[stream.available()];
            stream.read(buffer);
            stream.close();
            xml = new String(buffer, "UTF-8");      // you just need to specify the charsetName
        } catch (IOException e) {
            // Error handling
        }

        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(xml);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ProfileNewResponseParser profileNewResponseParser = new ProfileNewResponseParser();
        MatchesLatestModal2 matchesLatestModal2 = (profileNewResponseParser.parseProfileResponse(jsonObject, context, true, false, false)).getmMatchesLatestModal2();
        assertNotNull(matchesLatestModal2);
        assertNull(matchesLatestModal2.getProfileNewModalList());

        //matches response with matches
        stream = context.getResources().openRawResource(R.raw.matches);
        xml = null;
        try {
            byte[] buffer = new byte[stream.available()];
            stream.read(buffer);
            stream.close();
            xml = new String(buffer, "UTF-8");      // you just need to specify the charsetName
        } catch (IOException e) {
            // Error handling
        }

        jsonObject = null;
        try {
            jsonObject = new JSONObject(xml);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        profileNewResponseParser = new ProfileNewResponseParser();
        matchesLatestModal2 = (profileNewResponseParser.parseProfileResponse(jsonObject, context, true, false, false)).getmMatchesLatestModal2();
        assertNotNull(matchesLatestModal2);
        assertNotNull(matchesLatestModal2.getProfileNewModalList());
    }

}
