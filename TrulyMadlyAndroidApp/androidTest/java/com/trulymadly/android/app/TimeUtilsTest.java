package com.trulymadly.android.app;

import android.content.Context;
import android.test.InstrumentationTestCase;

import com.trulymadly.android.app.json.ConstantsSP;
import com.trulymadly.android.app.sqlite.RFHandler;
import com.trulymadly.android.app.utility.SPHandler;
import com.trulymadly.android.app.utility.TimeUtils;

import java.util.Calendar;

/**
 * Created by avin on 01/12/15.
 */
public class TimeUtilsTest extends InstrumentationTestCase {

    private final String KEY_TIMESTAMP_1 = "key_timestamp_1";
    private final long VALUE_TIMESTAMP = 1447131960000L;//Tue, 10 Nov 2015 05:06:00 GMT
    private final long TIME_OUT = 3600000L; //1 hour

    protected void setUp() throws Exception {
        super.setUp();
        clearSharedPrefs();
    }

    protected void tearDown() throws Exception {
        super.tearDown();
        clearSharedPrefs();
    }

    private void clearSharedPrefs() {
        Context context = getInstrumentation().getTargetContext();
        SPHandler.remove(context, KEY_TIMESTAMP_1);
    }

    /*
    * This is to test TimeUtils#isTimeoutExpired
    * */
    public void test_isTimeoutExpired(){
        //Test first time case
        Context context = getInstrumentation().getTargetContext();
        assertTrue(TimeUtils.isTimeoutExpired(context, KEY_TIMESTAMP_1, TIME_OUT));

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, -30);
        SPHandler.setString(context, KEY_TIMESTAMP_1, String.valueOf(calendar.getTimeInMillis()));
        assertFalse(TimeUtils.isTimeoutExpired(context, KEY_TIMESTAMP_1, TIME_OUT));

        calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, -61);
        SPHandler.setString(context, KEY_TIMESTAMP_1, String.valueOf(calendar.getTimeInMillis()));
        assertTrue(TimeUtils.isTimeoutExpired(context, KEY_TIMESTAMP_1, TIME_OUT));
    }

    /*
    * This is to test TimeUtils#isDifferenceGreater
    * */
    public void test_isDifferenceGreater(){
        //Test first time case
        Context context = getInstrumentation().getTargetContext();
        int USER_ID = 123456;
        SPHandler.setString(context, ConstantsSP.SHARED_KEYS_USER_ID, String.valueOf(USER_ID));
        String KEY_TIMESTAMP_2 = "key_timestamp_2";
        assertTrue(TimeUtils.isDifferenceGreater(context, KEY_TIMESTAMP_2, TIME_OUT));

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, -30);
        RFHandler.insert(context, String.valueOf(USER_ID), KEY_TIMESTAMP_2, calendar.getTimeInMillis());
        assertFalse(TimeUtils.isDifferenceGreater(context, KEY_TIMESTAMP_2, TIME_OUT));

        calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, -61);
        RFHandler.insert(context, String.valueOf(USER_ID), KEY_TIMESTAMP_2, calendar.getTimeInMillis());
        assertTrue(TimeUtils.isTimeoutExpired(context, KEY_TIMESTAMP_2, TIME_OUT));
    }


}
