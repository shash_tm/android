package com.trulymadly.android.app.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.longtailvideo.jwplayer.JWPlayerView;
import com.longtailvideo.jwplayer.configuration.PlayerConfig;
import com.longtailvideo.jwplayer.core.PlayerState;
import com.longtailvideo.jwplayer.events.listeners.VideoPlayerEvents;
import com.longtailvideo.jwplayer.media.playlists.PlaylistItem;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.trulymadly.android.analytics.TrulyMadlyEvent;
import com.trulymadly.android.analytics.TrulyMadlyTrackEvent;
import com.trulymadly.android.app.R;
import com.trulymadly.android.app.bus.NetworkChangeEvent;
import com.trulymadly.android.app.custom.TMJWPlayerView;
import com.trulymadly.android.app.listener.ActivityLifecycleListener;
import com.trulymadly.android.app.listener.NetworkChangedListener;
import com.trulymadly.android.app.listener.VideoControlListener;
import com.trulymadly.android.app.modal.ProfileViewPagerModal;
import com.trulymadly.android.app.utility.Utility;

import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by udbhav on 07/01/16.
 */
public class ImagePagerAdapter extends PagerAdapter implements VideoControlListener,
        ViewPager.OnPageChangeListener, ActivityLifecycleListener, NetworkChangedListener {

    private final Context mContext;
    private ProfileViewPagerModal[] mProfileViewPagerModals;
    private HashMap<Integer, JWPlayerView> mJWPlayersMap;
    private HashMap<Integer, View> mLoaderViewsMap, mPlayViewMap;
    private HashMap<Integer, ViewGroup> mContainerViewMap;
    private HashMap<Integer, Boolean> mVideoMuteMap, mVideoSeenMap;
    private int mCurrentPosition = 0;
    private Animation mScaleRepeatAnimation;
    private HashMap<Integer, View> mCircleImageViewsMap;
    private HashMap<Integer, Long> mDurationMap;
    private HashMap<Integer, Integer> mPlayCountMap;
    private int mPreplayPosition = -1;
    private String mSourceActivity, mMatchId;
    private HashMap<Integer, Boolean> mPlaySuccessTrackingSentTracker;

    public ImagePagerAdapter(Context mContext,
                             ProfileViewPagerModal[] profileViewPagerModals,
                             String matchId,
                             String mSourceActivity) {
        this.mContext = mContext;
        this.mSourceActivity = mSourceActivity;
        this.mMatchId = matchId;
        this.mProfileViewPagerModals = profileViewPagerModals;

        mJWPlayersMap = new HashMap<>();
        mVideoMuteMap = new HashMap<>();
        mDurationMap = new HashMap<>();
        mPlayCountMap = new HashMap<>();
        mVideoSeenMap = new HashMap<>();
        mLoaderViewsMap = new HashMap<>();
        mPlayViewMap = new HashMap<>();
        mContainerViewMap = new HashMap<>();
        mPlaySuccessTrackingSentTracker = new HashMap<>();
        mCircleImageViewsMap = new HashMap<>();
        mScaleRepeatAnimation = AnimationUtils.loadAnimation(mContext, R.anim.scale_animation_repeat_80);
        mScaleRepeatAnimation.setRepeatMode(Animation.REVERSE);
        mScaleRepeatAnimation.setRepeatCount(Animation.INFINITE);
    }

    @Override
    public int getCount() {
        return mProfileViewPagerModals == null ? 0 : mProfileViewPagerModals.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        final ProfileViewPagerModal profileViewPagerModal = mProfileViewPagerModals[position];

        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.album_viewpager_item,
                container, false);
        itemView.setTag(position);
        ImageView album_pic_id = (ImageView) itemView.findViewById(R.id.album_pic_id);
        View play_button_view = itemView.findViewById(R.id.play_button_view);

        ViewGroup playerContainer = (ViewGroup) itemView.findViewById(R.id.playerview_container);
        mContainerViewMap.put(position, playerContainer);

        final ProgressBar custom_prog_bar_photo_id = (ProgressBar) itemView
                .findViewById(R.id.custom_prog_bar_photo_id);

        String sourceUrl = profileViewPagerModal.getmSource();
        String imageUrl = (profileViewPagerModal.isVideo())?profileViewPagerModal.getmThumbnailUrl():
                profileViewPagerModal.getmSource();

        Picasso.with(mContext).load(imageUrl).config(Bitmap.Config.RGB_565)
                .into(album_pic_id, new Callback.EmptyCallback() {
                    @Override
                    public void onSuccess() {
                        custom_prog_bar_photo_id.setVisibility(View.GONE);
                        if (profileViewPagerModal.isVideo()) {
                            HashMap<String, String> eventInfo = new HashMap<>();
                            eventInfo.put("video_length_ms", String.valueOf(profileViewPagerModal.getmDuration() * 1000));
                            eventInfo.put("photos", String.valueOf(getCount()));
                            eventInfo.put("position", String.valueOf(position));
                            if (Utility.isSet(profileViewPagerModal.getmId())) {
                                eventInfo.put("video_id", profileViewPagerModal.getmId());
                            }
                            if (Utility.isSet(mMatchId)) {
                                eventInfo.put("user_id", String.valueOf(mMatchId));
                            }
                            if (Utility.isSet(mSourceActivity)) {
                                eventInfo.put("source", mSourceActivity);
                            }

                            TrulyMadlyTrackEvent.trackEvent(mContext,
                                    TrulyMadlyEvent.TrulyMadlyActivities.photo_full,
                                    TrulyMadlyEvent.TrulyMadlyEventTypes.download_video_thumbnail, 0,
                                    TrulyMadlyEvent.TrulyMadlyEventStatus.success, eventInfo, true);
                        }
                    }

                    @Override
                    public void onError() {
                        custom_prog_bar_photo_id.setVisibility(View.GONE);
                        if (profileViewPagerModal.isVideo()) {
                            HashMap<String, String> eventInfo = new HashMap<>();
                            eventInfo.put("video_length_ms", String.valueOf(profileViewPagerModal.getmDuration() * 1000));
                            eventInfo.put("photos", String.valueOf(getCount()));
                            eventInfo.put("position", String.valueOf(position));
                            if (Utility.isSet(profileViewPagerModal.getmId())) {
                                eventInfo.put("video_id", profileViewPagerModal.getmId());
                            }
                            if (Utility.isSet(mMatchId)) {
                                eventInfo.put("user_id", String.valueOf(mMatchId));
                            }
                            if (Utility.isSet(mSourceActivity)) {
                                eventInfo.put("source", mSourceActivity);
                            }

                            TrulyMadlyTrackEvent.trackEvent(mContext,
                                    TrulyMadlyEvent.TrulyMadlyActivities.photo_full,
                                    TrulyMadlyEvent.TrulyMadlyEventTypes.download_video_thumbnail, 0,
                                    TrulyMadlyEvent.TrulyMadlyEventStatus.fail, eventInfo, true);
                        }
                    }
                });

        if(profileViewPagerModal.isVideo()){
            PlayerConfig playerConfig = new PlayerConfig.Builder()
                    .build();
            TMJWPlayerView jwPlayerView = new TMJWPlayerView(mContext, playerConfig);
            mJWPlayersMap.put(position, jwPlayerView);
            mContainerViewMap.get(position).addView(jwPlayerView);
            mPlayViewMap.put(position, play_button_view);
            togglePlayerPlayButton(position, isPositionVideoSeen(position));

            final ImageView loader_iv = (ImageView) itemView.findViewById(R.id.loader_iv);
            Picasso.with(mContext).load(R.drawable.video).into(loader_iv);
            ImageView circle_iv = (ImageView) itemView.findViewById(R.id.circle_iv);
            circle_iv.setImageResource(R.drawable.white_circle_loader);
            final ImageView mute_iv = (ImageView) itemView.findViewById(R.id.toggle_mute_iv);
            mute_iv.setVisibility((profileViewPagerModal.isMuted())?View.GONE:View.VISIBLE);
            mCircleImageViewsMap.put(position, itemView.findViewById(R.id.circle_iv));
            mLoaderViewsMap.put(position, itemView.findViewById(R.id.loader_view));

            PlaylistItem item = new PlaylistItem.Builder()
                    .file(sourceUrl)
                    .build();
            mJWPlayersMap.get(position).setControls(false);
            mJWPlayersMap.get(position).setFullscreenHandler(null);

            mJWPlayersMap.get(position).getConfig().setRepeat(true);
            mJWPlayersMap.get(position).load(item);

            if(!profileViewPagerModal.isMuted()) {
                if (!mVideoMuteMap.containsKey(position) ||
                        mVideoMuteMap.get(position) == null || mVideoMuteMap.get(position)) {
                    mute_iv.setImageResource(R.drawable.mute_video_icon);
                    mJWPlayersMap.get(position).setMute(true);
                } else {
                    mute_iv.setImageResource(R.drawable.unmute_video_icon);
                    mJWPlayersMap.get(position).setMute(false);
                }
            }else{
                mJWPlayersMap.get(position).setMute(true);
            }

            mPlayViewMap.get(position).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    togglePlayerPlayButton(position, false);
                    play(position);

                    HashMap<String, String> eventInfo = new HashMap<>();
                    eventInfo.put("video_length_ms", String.valueOf(profileViewPagerModal.getmDuration() * 1000));
                    eventInfo.put("photos", String.valueOf(getCount()));
                    eventInfo.put("position", String.valueOf(position));
                    if (Utility.isSet(profileViewPagerModal.getmId())) {
                        eventInfo.put("video_id", profileViewPagerModal.getmId());
                    }
                    if (Utility.isSet(mMatchId)) {
                        eventInfo.put("user_id", String.valueOf(mMatchId));
                    }
                    if (Utility.isSet(mSourceActivity)) {
                        eventInfo.put("source", mSourceActivity);
                    }

                    TrulyMadlyTrackEvent.trackEvent(mContext,
                            TrulyMadlyEvent.TrulyMadlyActivities.photo_full,
                            TrulyMadlyEvent.TrulyMadlyEventTypes.play_clicked, 0,
                            TrulyMadlyEvent.TrulyMadlyEventStatus.success, eventInfo, true);
                }
            });

            mJWPlayersMap.get(position).addOnBufferListener(new VideoPlayerEvents.OnBufferListener() {
                @Override
                public void onBuffer(PlayerState playerState) {
                    if(mCurrentPosition == position) {
                        togglePlayerPlayButton(position, false);
                        togglePlayerLoader(position, true);
                    }else{
                        pause(position);
                    }
                }
            });

            mJWPlayersMap.get(position).addOnPauseListener(new VideoPlayerEvents.OnPauseListener() {
                @Override
                public void onPause(PlayerState playerState) {
                    if(position != mCurrentPosition) {
                        View view = mLoaderViewsMap.get(position);
                        if (view != null) {
                            view.setVisibility(View.GONE);
                        }
                    }
                }
            });

            mJWPlayersMap.get(position).addOnFirstFrameListener(new VideoPlayerEvents.OnFirstFrameListener() {

                @Override
                public void onFirstFrame(int i) {
                    JWPlayerView jwPlayerView = mJWPlayersMap.get(position);
                    if(mCurrentPosition != position) {
                        if(jwPlayerView != null) {
                            jwPlayerView.pause();
                        }
                    }
                }
            });

            if(!profileViewPagerModal.isMuted()) {
                mute_iv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        HashMap<String, String> eventInfo = new HashMap<>();
                        eventInfo.put("video_length_ms", String.valueOf(profileViewPagerModal.getmDuration() * 1000));
                        eventInfo.put("photos", String.valueOf(getCount()));
                        eventInfo.put("position", String.valueOf(position));
                        if (Utility.isSet(profileViewPagerModal.getmId())) {
                            eventInfo.put("video_id", profileViewPagerModal.getmId());
                        }
                        if (Utility.isSet(mMatchId)) {
                            eventInfo.put("user_id", String.valueOf(mMatchId));
                        }
                        if (Utility.isSet(mSourceActivity)) {
                            eventInfo.put("source", mSourceActivity);
                        }


                        JWPlayerView jwPlayerView = mJWPlayersMap.get(position);
                        if (!mVideoMuteMap.containsKey(position) ||
                                mVideoMuteMap.get(position) == null || !mVideoMuteMap.get(position)) {
                            mVideoMuteMap.put(position, true);
                            if(jwPlayerView != null) {
                                jwPlayerView.setMute(true);
                            }
                            mute_iv.setImageResource(R.drawable.mute_video_icon);

                            TrulyMadlyTrackEvent.trackEvent(mContext,
                                    TrulyMadlyEvent.TrulyMadlyActivities.photo_full,
                                    TrulyMadlyEvent.TrulyMadlyEventTypes.video_mute, 0,
                                    TrulyMadlyEvent.TrulyMadlyEventStatus.success, eventInfo, true);
                        } else {
                            mVideoMuteMap.put(position, false);
                            if(jwPlayerView != null) {
                                jwPlayerView.setMute(false);
                            }
                            mute_iv.setImageResource(R.drawable.unmute_video_icon);

                            TrulyMadlyTrackEvent.trackEvent(mContext,
                                    TrulyMadlyEvent.TrulyMadlyActivities.photo_full,
                                    TrulyMadlyEvent.TrulyMadlyEventTypes.video_unmute, 0,
                                    TrulyMadlyEvent.TrulyMadlyEventStatus.success, eventInfo, true);
                        }
                    }
                });
            }

            mJWPlayersMap.get(position).addOnPlayListener(new VideoPlayerEvents.OnPlayListener() {
                @Override
                public void onPlay(PlayerState playerState) {
                    JWPlayerView jwPlayerView = mJWPlayersMap.get(position);
                    if(mCurrentPosition == position) {
                        togglePlayer(position, true);
                        togglePlayerLoader(position, false);
                        togglePlayerPlayButton(position, false);

                    }else{
                        if(jwPlayerView != null) {
                            jwPlayerView.pause();
                        }
                    }

                    Long startedTime = mDurationMap.get(position);
                    int seconds = -1;
                    if (startedTime != null) {
                        long currentTime = Calendar.getInstance().getTimeInMillis();
                        long diff = currentTime - startedTime;
                        seconds = (int) (diff / 1000);
                        mDurationMap.remove(position);
                    }

                    if (mPlaySuccessTrackingSentTracker.get(position) == null ||
                            !mPlaySuccessTrackingSentTracker.get(position)) {
                        mPlaySuccessTrackingSentTracker.put(position, true);
                        HashMap<String, String> eventInfo = new HashMap<>();
                        eventInfo.put("video_length_ms", String.valueOf(profileViewPagerModal.getmDuration() * 1000));
                        eventInfo.put("photos", String.valueOf(getCount()));
                        eventInfo.put("position", String.valueOf(position));
                        if (Utility.isSet(profileViewPagerModal.getmId())) {
                            eventInfo.put("video_id", profileViewPagerModal.getmId());
                        }
                        if (Utility.isSet(mMatchId)) {
                            eventInfo.put("user_id", String.valueOf(mMatchId));
                        }
                        if (Utility.isSet(mSourceActivity)) {
                            eventInfo.put("source", mSourceActivity);
                        }

                        TrulyMadlyTrackEvent.trackEvent(mContext,
                                TrulyMadlyEvent.TrulyMadlyActivities.photo_full,
                                TrulyMadlyEvent.TrulyMadlyEventTypes.video_previewed, 0,
                                TrulyMadlyEvent.TrulyMadlyEventStatus.success, eventInfo, true);
                    }
                }
            });

            mJWPlayersMap.get(position).addOnCompleteListener(new VideoPlayerEvents.OnCompleteListener() {
                @Override
                public void onComplete() {

                    Integer currentCount = mPlayCountMap.get(position);
                    if (currentCount == null) {
                        currentCount = 0;
                    }
                    mPlayCountMap.put(position, currentCount + 1);

                    mVideoSeenMap.put(position, true);
                    togglePlayer(position, false);
                    togglePlayerPlayButton(position, true);
//                    play(position);
//                    JWPlayerView jwPlayerView = mJWPlayersMap.get(position);
//                    if(jwPlayerView != null) {
//                        jwPlayerView.setVisibility(View.INVISIBLE);
//                    }

                    HashMap<String, String> eventInfo = new HashMap<>();
                    eventInfo.put("video_length_ms", String.valueOf(profileViewPagerModal.getmDuration() * 1000));
                    eventInfo.put("photos", String.valueOf(getCount()));
                    eventInfo.put("position", String.valueOf(position));
                    if (Utility.isSet(profileViewPagerModal.getmId())) {
                        eventInfo.put("video_id", profileViewPagerModal.getmId());
                    }
                    if (Utility.isSet(mMatchId)) {
                        eventInfo.put("user_id", String.valueOf(mMatchId));
                    }
                    if (Utility.isSet(mSourceActivity)) {
                        eventInfo.put("source", mSourceActivity);
                    }

                    TrulyMadlyTrackEvent.trackEvent(mContext,
                            TrulyMadlyEvent.TrulyMadlyActivities.photo_full,
                            TrulyMadlyEvent.TrulyMadlyEventTypes.video_completed, 0,
                            TrulyMadlyEvent.TrulyMadlyEventStatus.success, eventInfo, true);
                }
            });

            mJWPlayersMap.get(position).addOnErrorListener(new VideoPlayerEvents.OnErrorListener() {
                @Override
                public void onError(String s) {
//                    JWPlayerView jwPlayerView = mJWPlayersMap.get(position);
//                    if(jwPlayerView != null){
//                        jwPlayerView.setVisibility(View.INVISIBLE);
//                    }
                    togglePlayer(position, false);
                    pause(position);

                    if(position == mCurrentPosition){
                        togglePlayer(mCurrentPosition, false);
                        togglePlayerLoader(position, false);
                        togglePlayerPlayButton(position, true);
                    }

                    Long startedTime = mDurationMap.get(position);
                    int seconds = -1;
                    if (startedTime != null) {
                        long currentTime = Calendar.getInstance().getTimeInMillis();
                        long diff = currentTime - startedTime;
                        seconds = (int) (diff / 1000);
                    }

                    HashMap<String, String> eventInfo = new HashMap<>();
                    eventInfo.put("video_length_ms", String.valueOf(profileViewPagerModal.getmDuration() * 1000));
                    eventInfo.put("photos", String.valueOf(getCount()));
                    eventInfo.put("position", String.valueOf(position));
                    eventInfo.put("error", s);
                    if (Utility.isSet(profileViewPagerModal.getmId())) {
                        eventInfo.put("video_id", profileViewPagerModal.getmId());
                    }
                    if (Utility.isSet(mMatchId)) {
                        eventInfo.put("user_id", String.valueOf(mMatchId));
                    }
                    if (Utility.isSet(mSourceActivity)) {
                        eventInfo.put("source", mSourceActivity);
                    }

                    TrulyMadlyTrackEvent.trackEvent(mContext,
                            TrulyMadlyEvent.TrulyMadlyActivities.photo_full,
                            TrulyMadlyEvent.TrulyMadlyEventTypes.video_previewed, 0,
                            TrulyMadlyEvent.TrulyMadlyEventStatus.fail, eventInfo, true);

                }

            });

            mJWPlayersMap.get(position).addOnSetupErrorListener(new VideoPlayerEvents.OnSetupErrorListener() {
                @Override
                public void onSetupError(String s) {
                    if (position == mCurrentPosition) {
                        togglePlayer(mCurrentPosition, false);
                        togglePlayerLoader(position, false);
                        togglePlayerPlayButton(position, true);
                    }

                    Long startedTime = mDurationMap.get(position);
                    int seconds = -1;
                    if (startedTime != null) {
                        long currentTime = Calendar.getInstance().getTimeInMillis();
                        long diff = currentTime - startedTime;
                        seconds = (int) (diff / 1000);
                    }

                    HashMap<String, String> eventInfo = new HashMap<>();
                    eventInfo.put("video_length_ms", String.valueOf(profileViewPagerModal.getmDuration() * 1000));
                    eventInfo.put("photos", String.valueOf(getCount()));
                    eventInfo.put("position", String.valueOf(position));
                    eventInfo.put("error", s);
                    if (Utility.isSet(profileViewPagerModal.getmId())) {
                        eventInfo.put("video_id", profileViewPagerModal.getmId());
                    }
                    if (Utility.isSet(mMatchId)) {
                        eventInfo.put("user_id", String.valueOf(mMatchId));
                    }
                    if (Utility.isSet(mSourceActivity)) {
                        eventInfo.put("source", mSourceActivity);
                    }

                    TrulyMadlyTrackEvent.trackEvent(mContext,
                            TrulyMadlyEvent.TrulyMadlyActivities.photo_full,
                            TrulyMadlyEvent.TrulyMadlyEventTypes.video_previewed, 0,
                            TrulyMadlyEvent.TrulyMadlyEventStatus.fail, eventInfo, true);
                }
            });

        }

        container.addView(itemView);
        if(position == mPreplayPosition){
            mCurrentPosition = position;
            mPreplayPosition = -1;
            play(position);
        }

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        stop(mCurrentPosition);
    }

    @Override
    public void onPause() {
        trackCloseVideo(mCurrentPosition);
        pause(mCurrentPosition);
        togglePlayerLoader(mCurrentPosition, false);
        togglePlayerPlayButton(mCurrentPosition, true);
    }

    @Override
    public void onResume() {
        resume(mCurrentPosition);
        resume(mCurrentPosition - 1);
        resume(mCurrentPosition + 1);
    }

    @Override
    public void onDestroy() {
        destroy(mCurrentPosition);
        destroy(mCurrentPosition-1);
        destroy(mCurrentPosition+1);
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (position != mCurrentPosition) {
            trackCloseVideo(mCurrentPosition);
        }

        mCurrentPosition = position;
        if (!isPositionVideoSeen(position)) {
            play(position);
        }
        togglePlayerPlayButton(position, isPositionVideoSeen(position));
        pause(position-1);
        destroy(position-2);
        pause(position+1);
        destroy(position+2);
    }

    private void trackCloseVideo(int position) {
        if (position < 0 || position >= getCount() || mProfileViewPagerModals == null ||
                !mProfileViewPagerModals[position].isVideo()) {
            return;
        }

        Long startedTime = mDurationMap.get(position);
        int seconds = -1;
        if (startedTime != null) {
            long currentTime = Calendar.getInstance().getTimeInMillis();
            long diff = currentTime - startedTime;
            seconds = (int) (diff / 1000);
        }

        ProfileViewPagerModal profileViewPagerModal = mProfileViewPagerModals[position];
        HashMap<String, String> eventInfo = new HashMap<>();
        eventInfo.put("video_length_ms", String.valueOf(profileViewPagerModal.getmDuration() * 1000));
        Integer playCount = mPlayCountMap.get(position);
        if (playCount != null) {
            playCount = 0;
        }
        eventInfo.put("play_count", String.valueOf(playCount));
        eventInfo.put("duration", String.valueOf((mJWPlayersMap.get(position) != null) ?
                mJWPlayersMap.get(position).getPosition() : 0));
        eventInfo.put("photos", String.valueOf(getCount()));
        eventInfo.put("muted", String.valueOf(profileViewPagerModal.isMuted()));

        eventInfo.put("position", String.valueOf(position));

        if (seconds > 0) {
            eventInfo.put("time_taken", String.valueOf(seconds));
        }

        if (Utility.isSet(mMatchId)) {
            eventInfo.put("user_id", String.valueOf(mMatchId));
        }

        if (Utility.isSet(profileViewPagerModal.getmId())) {
            eventInfo.put("video_id", profileViewPagerModal.getmId());
        }
        if (Utility.isSet(mSourceActivity)) {
            eventInfo.put("source", mSourceActivity);
        }

        TrulyMadlyTrackEvent.trackEvent(mContext,
                TrulyMadlyEvent.TrulyMadlyActivities.photo_full,
                TrulyMadlyEvent.TrulyMadlyEventTypes.video_closed, 0,
                TrulyMadlyEvent.TrulyMadlyEventStatus.success, eventInfo, true);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private boolean isPositionVideo(int position) {
        boolean isVideo = true;
        if (mProfileViewPagerModals == null || position < 0 || mProfileViewPagerModals.length <= position ||
                !mProfileViewPagerModals[position].isVideo()) {
            isVideo = false;
        }

        return isVideo;
    }

    private boolean isPositionVideoSeen(int position) {
        boolean isVideoSeen = false;
        if (mVideoSeenMap != null && mVideoSeenMap.containsKey(position)) {
            isVideoSeen = mVideoSeenMap.get(position);
        }

        return isVideoSeen;
    }

    @Override
    public void prePlay(int position) {
        mPreplayPosition = position;
    }

    @Override
    public void play(int position) {
        boolean isVideo = isPositionVideo(position);
        if (isVideo) {
            JWPlayerView jwPlayerView = mJWPlayersMap.get(position);
            if (jwPlayerView != null) {
                jwPlayerView.onResume();
                togglePlayerLoader(position, true);
                jwPlayerView.play();

                mDurationMap.put(position, Calendar.getInstance().getTimeInMillis());
            }
        }
    }

    @Override
    public void pause(int position) {
        JWPlayerView jwPlayerView = mJWPlayersMap.get(position);
        if(jwPlayerView != null) {
            jwPlayerView.pause();
            jwPlayerView.onPause();
        }

        boolean isVideo = isPositionVideo(position);
        if (isVideo) {
            togglePlayerLoader(position, false);
            mDurationMap.remove(position);
        }
    }

    @Override
    public void resume(int position) {
        JWPlayerView jwPlayerView = mJWPlayersMap.get(position);
        if (jwPlayerView != null) {
            jwPlayerView.onResume();
        }
    }

    @Override
    public void stop(int position) {
        JWPlayerView jwPlayerView = mJWPlayersMap.get(position);
        if(jwPlayerView != null) {
            jwPlayerView.pause();
            jwPlayerView.stop();
            jwPlayerView.onPause();
//            jwPlayerView.setVisibility(View.INVISIBLE);
            togglePlayer(position, false);
        }

        boolean isVideo = isPositionVideo(position);
        if (isVideo) {
            togglePlayerLoader(position, false);
        }

        mDurationMap.remove(position);
    }

    @Override
    public void destroy(int position) {
        JWPlayerView jwPlayerView = mJWPlayersMap.get(position);
        if(jwPlayerView != null) {
            jwPlayerView.pause();
            jwPlayerView.stop();
            jwPlayerView.onPause();
            togglePlayer(position, false);
            mContainerViewMap.get(position).removeView(jwPlayerView);
            jwPlayerView.onDestroy();
            mJWPlayersMap.remove(position);
        }

        boolean isVideo = isPositionVideo(position);
        if (isVideo) {
            togglePlayerLoader(position, false);
            mLoaderViewsMap.remove(position);
            mCircleImageViewsMap.remove(position);
            mPlayViewMap.remove(position);
            mContainerViewMap.remove(position);
            mDurationMap.remove(position);
        }
    }

    @Override
    public void onNetworkChanged(NetworkChangeEvent networkChangeEvent) {
        if(networkChangeEvent != null) {
            if (networkChangeEvent.isConnected()) {
                if(mProfileViewPagerModals != null && mCurrentPosition >= 0 &&
                        mProfileViewPagerModals.length > mCurrentPosition){
                    ProfileViewPagerModal profileViewPagerModal = mProfileViewPagerModals[mCurrentPosition];
                    if(profileViewPagerModal.isVideo()){
                        if (!isPositionVideoSeen(mCurrentPosition)) {
                            play(mCurrentPosition);
                        } else {
                            togglePlayerLoader(mCurrentPosition, false);
                            togglePlayer(mCurrentPosition, false);
                            togglePlayerPlayButton(mCurrentPosition, true);
                        }
                    }
                }
            }
        }
    }

    private void togglePlayerLoader(int position, boolean makeVisible) {
        View loaderView = mLoaderViewsMap.get(position);
        if (loaderView != null) {
            loaderView.setVisibility(makeVisible ? View.VISIBLE : View.GONE);
        }

        View circleView = mCircleImageViewsMap.get(position);
        if (circleView != null) {
            if (makeVisible) {
                circleView.startAnimation(mScaleRepeatAnimation);
            } else {
                circleView.clearAnimation();
            }
        }
    }

    private void togglePlayerPlayButton(int position, boolean makeVisible) {
        View playView = mPlayViewMap.get(position);
        if (playView != null) {
            playView.setVisibility(makeVisible ? View.VISIBLE : View.GONE);
        }
    }

    private void togglePlayer(int position, boolean makePlayerVisible) {
        ViewGroup jwPlayerViewContainer = mContainerViewMap.get(position);
        JWPlayerView jwPlayerView = mJWPlayersMap.get(position);
        if (jwPlayerView != null) {
            jwPlayerViewContainer.setVisibility(makePlayerVisible ? View.VISIBLE : View.INVISIBLE);
            jwPlayerView.setVisibility(makePlayerVisible ? View.VISIBLE : View.INVISIBLE);
        }
    }
}
