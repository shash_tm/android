package com.trulymadly.android.app;

import android.test.InstrumentationTestRunner;
import android.test.InstrumentationTestSuite;

import junit.framework.TestSuite;

/**
 * Created by avin on 01/12/15.
 */

//@RunWith(Suite.class)
//@Suite.SuiteClasses({ TimeUtilsTest.class})
//Not working right now
class ApiInstrumentationTestSuite extends InstrumentationTestRunner {
    @Override
    public TestSuite getAllTests() {
        TestSuite suite = new InstrumentationTestSuite(this);
        suite.addTestSuite(CacheTest.class);
        suite.addTestSuite(TimeUtilsTest.class);
        suite.addTestSuite(ParsersTest.class);
        return suite;
    }


}
