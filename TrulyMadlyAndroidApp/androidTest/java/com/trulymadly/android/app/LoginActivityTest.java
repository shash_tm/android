package com.trulymadly.android.app;

import android.test.ActivityInstrumentationTestCase2;
import android.widget.EditText;

import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.utility.FilesHandler;

import org.junit.Test;

import java.io.File;

/**
 * Created by avin on 30/11/15.
 */
public class LoginActivityTest extends ActivityInstrumentationTestCase2<LoginActivity> {
    EditText mEditText;
    private LoginActivity mLoginActivity;

    public LoginActivityTest() {
        super(LoginActivity.class);
    }

    public LoginActivityTest(Class<LoginActivity> activityClass) {
        super(activityClass);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        // Starts the activity under test using the default Intent with:
        // action = {@link Intent#ACTION_MAIN}
        // flags = {@link Intent#FLAG_ACTIVITY_NEW_TASK}
        // All other fields are null or empty.
        mLoginActivity = getActivity();
//        mEditText = (EditText) mLoginActivity.findViewById(R.id.et_userid_login);
    }

    /**
     * Test if your test fixture has been set up correctly. You should always implement a test that
     * checks the correct setup of your test fixture. If this tests fails all other tests are
     * likely to fail as well.
     */
    public void testPreconditions() {
        //Try to add a message to add context to your assertions. These messages will be shown if
        //a tests fails and make it easy to understand why a test failed
        assertNotNull("mLoginActivity is null", mLoginActivity);
//        assertNotNull("mFirstTestText is null", mEditText);
    }

    /**
     * Tests the correctness of the initial text.
     */
    public void testMyFirstTestTextView_labelText() {
        //It is good practice to read the string from your resources in order to not break
        //multiple tests when a string changes.
        final String expected = mLoginActivity.getString(R.string.upload);
        final String actual = "Upload";//mFirstTestText.getText().toString();
        assertEquals("mFirstTestText contains wrong text", expected, actual);
    }

    /*
    * Tests the FIlesHandler#createFolder
    * */
    @Test
    public void test_createFolder(){
        String path = "/temp";
        File folder = new File(Constants.disk_path + path);
        assertFalse(folder.exists());

        FilesHandler.createFolder(path);
        assertTrue(folder.exists());
    }


}
