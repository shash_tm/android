package com.trulymadly.android.app;

import android.content.Context;
import android.test.InstrumentationTestCase;

import com.trulymadly.android.app.sqlite.CachingDBHandler;
import com.trulymadly.android.app.utility.TimeUtils;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by avin on 16/12/15.
 */
public class AppCacheTest extends InstrumentationTestCase {
    private final String mUrl = "some_random_url";
    private final String mUserId = "123456";
    private final String mTimeStamp = String.valueOf(TimeUtils.getSystemTimeInMiliSeconds());

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        Context context = getInstrumentation().getTargetContext();
        clearTempData();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        clearTempData();
    }

    private void clearTempData() {
    }

    /*
    * Tests the CachingDBHandler#createFolder
    * */
    @Test
    public void test_getResponse() {
        Context context = getInstrumentation().getTargetContext();
        InputStream stream = context.getResources().openRawResource(R.raw.userflags_without_chat);
        String xml = null;
        try {
            byte[] buffer = new byte[stream.available()];
            stream.read(buffer);
            stream.close();
            xml = new String(buffer, "UTF-8"); // you just need to specify the charsetName
        } catch (IOException e) {
            // Error handling
        }

        JSONObject response = CachingDBHandler.getResponse(context, mUrl, mUserId);
        assertNull(response);
    }

    /*
    * Tests the CachingDBHandler#insertResponse
    * */
    @Test
    public void test_insertResponse() {
        Context context = getInstrumentation().getTargetContext();
        InputStream stream = context.getResources().openRawResource(R.raw.userflags_without_chat);
        String xml = null;
        try {
            byte[] buffer = new byte[stream.available()];
            stream.read(buffer);
            stream.close();
            xml = new String(buffer, "UTF-8"); // you just need to specify the charsetName
        } catch (IOException e) {
            // Error handling
        }

        String mHashKey = "12345678123456781234567812345678";
        CachingDBHandler.insertResponse(context, mUrl, mHashKey, xml, mUserId, mTimeStamp);
        JSONObject response = CachingDBHandler.getResponse(context, mUrl, mUserId);
        assertNotNull(response);
        try {
            String responseS = response.toString();
            JSONObject object = new JSONObject(xml);
//            assertEquals(removeWhiteSpaces(responseS), removeWhiteSpaces(xml));
            assertEquals(response, object);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String removeWhiteSpaces(String input) {
        return input.replace("\\s+", "");
    }
}
