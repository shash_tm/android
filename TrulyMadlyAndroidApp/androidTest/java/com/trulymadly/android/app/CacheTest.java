package com.trulymadly.android.app;

import android.support.test.runner.AndroidJUnit4;
import android.test.ActivityTestCase;

import com.trulymadly.android.app.json.Constants;
import com.trulymadly.android.app.utility.FilesHandler;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.IOException;

/**
 * Created by avin on 02/12/15.
 */
@RunWith(AndroidJUnit4.class)
public class CacheTest extends ActivityTestCase {

    private final String mTempFolder = "temp";

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        clearTempData();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        clearTempData();
    }

    private void clearTempData() {
        String path = "/" + mTempFolder;
        File folder = new File(Constants.disk_path + path);
        if(folder.exists())
            folder.delete();
    }

    private void createDummyFolderAndData(boolean addData){
        String path = "/" + mTempFolder;
        FilesHandler.createFolder(path);

        if(addData){
            for(int i = 0; i < 5; i++) {
                File noMediaFile = new File(FilesHandler.getFilePath(Constants.disk_path, mTempFolder
                ), "hello" + i +  ".text");
                try {
                    noMediaFile.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            File noMediaFile = new File(FilesHandler.getFilePath(Constants.disk_path,
                    mTempFolder), "hello" + 1 +  ".text");
            assertTrue(noMediaFile.exists());
        }
    }

    /*
    * Tests the FilesHandler#createFolder
    * */
    @Test
    public void test_createFolder(){
        String path = "/" + mTempFolder;
        File folder = new File(Constants.disk_path + path);
        FilesHandler.deleteFileOrDirectory(Constants.disk_path + path);
        assertFalse(folder.exists());

        FilesHandler.createFolder(path);
        assertTrue(folder.exists());
    }

    /*
    * Tests the FilesHandler#createImageCacheFolder
    * */
    @Test
    public void test_createImageCacheFolder(){
        File folder = new File(Constants.disk_path + "/" + Constants.IMAGES_ASSETS_FOLDER);
        FilesHandler.createImageCacheFolder();
        assertTrue(folder.exists());

        File noMediaFile = new File(FilesHandler.getFilePath(Constants.disk_path,
                Constants.IMAGES_ASSETS_FOLDER), ".nomedia");
        assertTrue(noMediaFile.exists());
    }

    /*
    * Tests the FilesHandler#deleteFileOrDirectory
    * */
    @Test
    public void test_deleteFileOrDirectory(){
        String path = "/" + mTempFolder;

        FilesHandler.deleteFileOrDirectory(Constants.disk_path + path);
        File deletedFolder = new File(Constants.disk_path + path);
        assertFalse(deletedFolder.exists());

        createDummyFolderAndData(false);
        assertTrue(deletedFolder.exists());
        FilesHandler.deleteFileOrDirectory(Constants.disk_path + path);
        assertFalse(deletedFolder.exists());
    }

    /*
    * Tests the FilesHandler#deleteContents
    * */
    @Test
    public void test_deleteContents(){
        String path = "/" + mTempFolder;
        File folder = new File(Constants.disk_path + path);

        createDummyFolderAndData(true);
        assertTrue(folder.exists());
        FilesHandler.deleteContents(Constants.disk_path + path);
        File noMediaFile = new File(FilesHandler.getFilePath(Constants.disk_path,
                Constants.IMAGES_ASSETS_FOLDER), "hello" + 1 +  ".text");
        assertFalse(noMediaFile.exists());
    }

    /*
    * Tests the FilesHandler#deleteContents
    * */
    @Test
    public void test_deleteContentsWithInclude(){
        String path = "/" + mTempFolder;
        File folder = new File(Constants.disk_path + path);

        createDummyFolderAndData(true);
        assertTrue(folder.exists());
        FilesHandler.deleteContents(Constants.disk_path + path, "hello1.text", true);
        File hello1 = new File(FilesHandler.getFilePath(Constants.disk_path,
                mTempFolder), "hello" + 1 +  ".text");
        assertFalse(hello1.exists());

        File hello2 = new File(FilesHandler.getFilePath(Constants.disk_path,
                mTempFolder), "hello" + 2 +  ".text");
        assertTrue(hello2.exists());

        FilesHandler.deleteContents(Constants.disk_path + path, "hello2.text", false);
        File hello3 = new File(FilesHandler.getFilePath(Constants.disk_path,
                mTempFolder), "hello" + 1 +  ".text");
        assertFalse(hello3.exists());
        assertTrue(hello2.exists());
    }


}
