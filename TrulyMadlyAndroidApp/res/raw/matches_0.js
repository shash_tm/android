{
	"responseCode": 200,
	"my_data": {
		"values_filled": false,
		"favourite_filled": false,
		"interpersonal_filled": false,
		"trustmeter": {
			"fb": 30,
			"photo_id": 30,
			"linkedin": 15,
			"employment": 15,
			"phone": 10,
			"endorsements": 15
		},
		"profile_pic": "https:\/\/s3-ap-southeast-1.amazonaws.com\/trulymadlytestbucketnew\/files\/images\/profiles\/1448274567562.6_862003309186547968_8621140308.jpg",
		"profile_pic_full": "https:\/\/s3-ap-southeast-1.amazonaws.com\/trulymadlytestbucketnew\/files\/images\/profiles\/1448274567562.6_862003309186547968.jpg",
		"age": "29",
		"fname": "Udbhav",
		"gender": "M",
		"city": "Delhi",
		"state": "Delhi \/ NCR ",
		"mutual_like_count": 0,
		"message_count": 0,
		"conversation_count": 0,
		"status": "authentic",
		"user_id": "2136",
		"show_hash": false
	},
	"system_messages": {
		"last_tile_msg": "Oh no! We don't have any matches to show you as of now. Come back later, will ya? ",
		"last_tile_link": "invite_friends",
		"non_mutual_like_msg": "You can view the name and chat only when both of you have liked each other. Patience my dear, patience."
	},
	"system_flags": {
		"socket_debug_flag": false,
		"chat_ip": "http:\/\/54.169.28.196:80",
		"is_socket_enabled": true
	},
	"abString": {
		"RecommendationAB": {
			"RecommendationEngine": "100"
		},
		"AestheticsAB": {
			"Aesthetics_None": [null, null]
		}
	},
	"sessionId": "11"
}