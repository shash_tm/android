{
	"responseCode": 200,
	"isProfileComplete": false,
	"isTrustVerified": true,
	"trustScore": 85,
	"messages": {
		"trustIncompleteMessage": "A high trust score goes a long way with the ladies!",
		"profileIncompleteMessage": "Complete your profile, you could be missing out on a lot of great matches!"
	},
	"counter": {
		"likeCount": -1,
		"hideCount": -1
	},
	"chat_configurations": {
		"show_passcode": true,
		"chat_domain": "",
		"websocket_only": false,
		"connection_retry_attempts": 5,
		"chat_send_timeout": 5,
		"connection_timeout": 4,
		"get_missed_messages_timeout": 8,
		"socket_debug_flag": true,
		"ab_prefix": "hello"
	},
	"native_ad_flags": {
		"global_insterstitial_counter": 10,
		"matches_insterstitial_counter": 10,
		"conversation_insterstitial_counter": 10,
		"interstitial_time_interval": 10,
		"native_time_interval": 10
	}
}