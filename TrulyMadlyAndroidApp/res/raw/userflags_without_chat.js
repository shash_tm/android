{
	"responseCode": 200,
	"isProfileComplete": false,
	"isTrustVerified": true,
	"trustScore": 85,
	"messages": {
		"trustIncompleteMessage": "A high trust score goes a long way with the ladies!",
		"profileIncompleteMessage": "Complete your profile, you could be missing out on a lot of great matches!"
	},
	"counter": {
		"likeCount": -1,
		"hideCount": -1
	},
	"chat_configurations": [],
	"native_ad_flags": []
}