Redex Setup Guides

1. Setting up .bash_profile

vi ~/.bash_profile

Please use the relevant local path
export ANDROID_SDK=$HOME/Documents/androidsdk [Modify for local path]
export PATH=$PATH:$ANDROID_SDK/platform-tools
export PATH=$PATH:$ANDROID_SDK/tools
export GRADLE_HOME=/Applications/Android\ Studio.app/Contents/gradle/gradle-2.14.1 [Modify for local path]
export PATH=$PATH:$GRADLE_HOME/bin
export REDEX_HOME=/Users/udbhav/git/redex [Modify for local path]
export PATH=$PATH:$REDEX_HOME

2. Steps for Build REDEX
Redex
     1. Built on commit no c5fe210
     2. Updated to commit no 4d4fa8a (25/05/2016)
     3. Updated to commit no 4eea580 (28/06/2016)
     4. Updated to commit no 1cd504e (06/07/2016)
     5. Updated to commit no a9b787f (11/07/2016)
     6. Updated to commit no 20c50b7 (14/07/2016)
     7. Updated to commit no 6c9ad6c (20/07/2016)
     8. Updated to commit no 0440aba (26/07/2016)
     9. Updated to commit no 1962ec3 (10/08/2016)
    10. Updated to commit no 6e0d1d5 (25/08/2016)
    11. Updated to commit no 18fdc1a (24/11/2016)
    12. Updated to commit no 9076ad0 (06/12/2016)
    13. Updated to commit no 9bd9c88 (05/01/2017)

Steps to build Redex everytime after updating from git.
    1. sudo make clean all
        1.1 This step can delete a file called "apkutil". If that file is deleted, undo the delete from git and then continue with next step.
    2. sudo autoreconf -ivf && sudo ./configure && sudo make
    3. sudo make install

3. For Building APK
Run the command build.sh.

build.sh -h : To print the help file
build.sh -a <buildVariant> : To build the selected variant (Both normal and redexed version)

Please read "build.sh -h" help documentation for more deails

4. For uploading files to google drive
    a. Install gdrive using the following command:
        brew install gdrive
    b. Once installed, run the following command before running any other command:
        gdrive about
       It will give you a link and ask you for Google drive access key. Open the given link in the browser
       and give the gdrive access to your google drive files. It will give you the access key amd put it in the terminal.
    c. Done. Now run the build.sh with the following parameter passing the parent folder id (check point d) as shown below
        ./build.sh -a assembleRelease -u 0B3VsjPhMST-ZWWRteXptWHJ6TWc
        -u will upload the generated files and folders to the given parent folder
    d. How to get parent folder id? - There are multiple ways to get it:
        i) Open the folder in the browser and get the id from the url:
            e.g. https://drive.google.com/drive/u/1/folders/0B5bImYel7mXtdklDOFU1WnNqZjQ -> id is: 0B5bImYel7mXtdklDOFU1WnNqZjQ
        ii) Select the folder and open the sharing link and get the id from that link:
            e.g. https://drive.google.com/a/trulymadly.com/folderview?id=0B5bImYel7mXtdklDOFU1WnNqZjQ&usp=sharing -> id is: 0B5bImYel7mXtdklDOFU1WnNqZjQ

    Parent folder? - Parent folder is the version code folder that we create inside TrulyMadlyApp folder in google drive.
    For current release (3.5.0) its id is: "0B3VsjPhMST-ZWWRteXptWHJ6TWc" and hence the command.
